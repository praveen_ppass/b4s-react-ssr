export const sideTabList = {
	dashboard: {
		profile: [
			{ name: "My Profile", id: "PersonalInfo", classes: "My-Profile" },
			{ name: "Matched Scholarships", id: "MatchedScholarships", classes: "matched-scholarships" },
			{ name: "Applied Scholarships", id: "ApplicationStatus", classes: "applied-scholarships" },
			{ name: "Awarded Scholarships", id: "AwardeesScholarships", classes: "awarded-scholarships" },
			{ name: "My Favorites", id: "MyFavorites", classes: "my-fav" },
			{ name: "Questions & Answers", id: "QuestionsAndAnswers", classes: "questions-answers" }
		]
	}
};
