export const breadCrumObj = {
  home: {
    title: `India's Largest Scholarship Platform`,
    bgImage: [""],
    subTitle: "Making Education Affordable",
    breadCrum: [
      {
        url: "#",
        name: "Home"
      }
    ]
  },
  contact_us: {
    title: `CONTACT US`,
    bgImage: ["backcontact"],
    subTitle: "We are always striving to help you",
    breadCrum: [
      {
        url: "#",
        name: "Contact Us"
      }
    ]
  },
  collegeboard_payment: {
    title: `payment`,
    bgImage: ["backcontact"],
    subTitle: "We are always striving to help you",
    breadCrum: [
      {
        url: "#",
        name: "Payment"
      }
    ]
  },
  brand_page: {
    title: ``,
    bgImage: [""],
    subTitle: "",
    breadCrum: [
      {
        url: "#",
        name: "Page"
      }
    ]
  },
  premium_membership: {
    title: `PREMIUM MEMBERSHIP`,
    bgImage: ["bgpremiummembership"],
    subTitle: "Get access to premium features",
    breadCrum: [
      {
        url: "#",
        name: "Premium Membership"
      }
    ]
  },

  media_partners: {
    title: `MEDIA PARTNERS`,
    bgImage: ["bgmediapartners"],
    subTitle:
      "Fastest growing network of media partners for spreading awareness about scholarships",
    breadCrum: [
      {
        url: "#",
        name: "Media Partners"
      }
    ]
  },

  terms_conditions: {
    title: `Terms and Conditions`,
    bgImage: ["bgtnc"],
    breadCrum: [
      {
        url: "#",
        name: "Terms & Conditions"
      }
    ]
  },
  team: {
    title: "WELCOME TO <span>BUDDY4STUDY</span>",
    subTitle:
      "India's largest student empowerment network",
    bgImage: ["bgteam"],
    breadCrum: [
      {
        url: "#",
        name: "Team"
      }
    ]
  },
  studentlist: {
    title: "",
    subTitle: "",
    bgImage: [],
    hideBanner: true,
    breadCrum: [
      {
        url: "#",
        name: "Student List"
      }
    ]
  },

  addStudent: {
    title: "",
    subTitle: "",
    bgImage: [],
    hideBanner: true,
    breadCrum: [
      {
        url: "#",
        name: "Add Student"
      }
    ]
  },
  download: {
    title: "",
    subTitle: "",
    bgImage: [],
    hideBanner: true,
    breadCrum: [
      {
        url: "#",
        name: "Download"
      }
    ]
  },
  tutorials: {
    title: "",
    subTitle: "",
    bgImage: [],
    hideBanner: true,
    breadCrum: [
      {
        url: "#",
        name: "Tutorials"
      }
    ]
  },
  upgrade: {
    title: "",
    subTitle: "",
    bgImage: [],
    hideBanner: true,
    breadCrum: [
      {
        url: "#",
        name: "Upgrade"
      }
    ]
  },
  forgotpassword: {
    bgImage: [""],
    hideBanner: true,
    breadCrum: [
      {
        url: "#",
        name: "Forgot Password"
      }
    ]
  },

  about: {
    title: "INDIA'S LARGEST SCHOLARSHIP PLATFORM",
    subTitle: "Connecting Scholarship Seekers with Scholarship Providers",
    bgImage: ["bgabout"],
    breadCrum: [
      {
        url: "#",
        name: "About"
      }
    ]
  },
  scholarship_detail: {
    breadCrum: [
      {
        url: "/scholarships",
        name: "Scholarship"
      },
      {
        url: "#",
        name:
          "Jawahar Navodaya Vidyalaya Selection Test Against Vacant Seats 2018"
      }
    ]
  },
  scholar_result: {
    title: "",
    subTitle: "",
    bgImage: [""],
    hideBanner: true,
    breadCrum: [
      {
        url: "/scholarship-result",
        name: "scholarship result"
      }
    ]
  },
  careers: {
    title: "JOIN THE GANG",
    subTitle:
      "<span>Ready to be part of an enterprising, creative, young and growth-oriented work environment? <br/>Send us your resume at <a href='mailto:careers@buddy4study.com'>careers@buddy4study.com</a> <br/></span> <a href='/career-opportunities' className='btn'> Explore good opportunities </a>",
    bgImage: ["bgcareers"],
    breadCrum: [
      {
        url: "#",
        name: "careers"
      }
    ]
  },
  qna: {
    title: "Question & Answer",
    subTitle:
      "<span>Get Your Queries Answered on Scholarships, Exams, Admissions, Study Abroad and More..</span",
    bgImage: ["bgQNA"],
    breadCrum: [
      {
        url: "#",
        name: "Question & Answers"
      }
    ]
  },
  qcategories: {
    bgImage: [""],
    breadCrum: [
      {
        url: "/qna",
        name: "Question & Answers"
      }
    ]
  },

  "career-opportunities": {
    title: "CAREERS",
    subTitle: "Choose your best career path",
    bgImage: ["bgcareers"],
    breadCrum: [
      {
        url: "#",
        name: "career-opportunities"
      }
    ]
  },
  "career-details": {
    title: "CAREERS",
    subTitle: "Choose your best career path",
    bgImage: ["bgcareers"],
    breadCrum: [
      {
        url: "#",
        name: "career-details"
      }
    ]
  },
  privacy_policy: {
    title: `Privacy Policy`,
    bgImage: ["bgtnc"],
    breadCrum: [
      {
        url: "#",
        name: "Privacy Policy"
      }
    ]
  },
  disclaimer: {
    title: ``,
    subTitle: "",
    bgImage: [""],
    breadCrum: [
      {
        url: "/disclaimer",
        name: "Disclaimer"
      }
    ]
  },

  city: {
    title: ``,
    subTitle: "",
    bgImage: [""],
    breadCrum: [
      {
        url: "/scholarship-result",
        name: "scholar result"
      },
      {
        url: "/city",
        name: "Merck India Charitable Trust (MICT) Scholarship Program 2018-19"
      }
    ]
  },

  faqs: {
    title: ``,
    subTitle: "",
    bgImage: [""],
    breadCrum: [
      {
        url: "/faqs",
        name: "FAQs"
      }
    ]
  },

  "faqs-details": {
    title: ``,
    subTitle: "",
    bgImage: [""],
    breadCrum: [
      {
        url: "/faqs",
        name: "FAQs"
      }
    ]
  },

  results: {
    title: "",
    subTitle: "",
    bgImage: [""],
    hideBanner: true,
    breadCrum: [
      {
        url: "/results",
        name: "scholarship result"
      }
    ]
  },

  dashboard: {
    title: `Dashboard`,
    subTitle: "",
    bgImage: ["bgdashboard"],
    breadCrum: []
  },
  myScholarships: {
    title: `My Scholarships`,
    subTitle: "",
    bgImage: ["bgdashboard"],
    breadCrum: []
  },
  mySubscribers: {
    title: `My Subscribers`,
    subTitle: "",
    bgImage: ["bgdashboard"],
    breadCrum: []
  },
  scholarships: {
    title: "All Scholarships for Students",
    breadCrum: [
      {
        url: "/scholarships",
        name: "Scholarships"
      }
    ]
  },
  cscPayment: {
    title: ``,
    bgImage: [],
    subTitle: "",
    breadCrum: [
      {
        url: "#",
        name: "Payment"
      }
    ]
  },
  vleMatchedSch: {
    title: "",
    subTitle: "",
    bgImage: [],
    hideBanner: true,
    breadCrum: [
      {
        url: "#",
        name: "Matched Scholarships"
      }
    ]
  },
  vlePaymentPlans: {
    title: "",
    subTitle: "",
    bgImage: [],
    breadCrum: [
      {
        url: "#",
        name: "Payment Plans"
      }
    ]
  },
  vleDocuments: {
    title: "",
    subTitle: "",
    bgImage: [],
    hideBanner: true,
    breadCrum: [
      {
        url: "#",
        name: "Documents"
      }
    ]
  },
  videos: {
    title: ``,
    bgImage: [""],
    subTitle: "",
    breadCrum: [
      {
        url: "#",
        name: "Videos"
      }
    ]
  }
};
