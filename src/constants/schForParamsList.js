export const paramsList = {
  "class-10": {
    TYPE: "EDUCATION",
    TITLE: "Class 10 Scholarships",
    FILTER: [
      {
        label: "Class 10",
        value: 11,
        TYPE: "EDUCATION"
      }
    ]
  },
  "class-12": {
    TYPE: "EDUCATION",
    TITLE: "Class 12 Scholarships",
    FILTER: [
      {
        label: "Class 12",
        value: 16,
        TYPE: "EDUCATION"
      }
    ]
  },
  graduation: {
    TYPE: "EDUCATION",
    TITLE: "College Scholarships",
    FILTER: [
      {
        label: "Graduation",
        value: 22,
        TYPE: "EDUCATION"
      }
    ]
  },
  international: {
    TYPE: "FOREIGN",
    TITLE: "International Scholarships",
    FILTER: [
      {
        label: "International",
        value: 698,
        TYPE: "FOREIGN"
      }
    ]
  },
  "all-india": {
    TYPE: "NATIONAL",
    TITLE: "National Scholarships",
    FILTER: [
      {
        label: "All India",
        value: 447,
        TYPE: "NATIONAL"
      }
    ]
  },
  "west-bengal": {
    TYPE: "NATIONAL",
    TITLE: "National Scholarships",
    FILTER: [
      {
        label: "West Bengal",
        value: 482,
        TYPE: "NATIONAL"
      }
    ]
  },
  "uttar-pradesh": {
    TYPE: "NATIONAL",
    TITLE: "National Scholarships",
    FILTER: [
      {
        label: "Uttar Pradesh",
        value: 480,
        TYPE: "NATIONAL"
      }
    ]
  },
  bihar: {
    TYPE: "NATIONAL",
    TITLE: "National Scholarships",
    FILTER: [
      {
        label: "Bihar",
        value: 452,
        TYPE: "NATIONAL"
      }
    ]
  },
  "madhya-pradesh": {
    TYPE: "NATIONAL",
    TITLE: "National Scholarships",
    FILTER: [
      {
        label: "Madhya Pradesh",
        value: 467,
        TYPE: "NATIONAL"
      }
    ]
  },
  engineering: {
    TYPE: "EDUCATION",
    TITLE: "Engineering Scholarships",
    FILTER: [
      {
        label: "Engineering",
        value: 676,
        TYPE: "EDUCATION"
      }
    ]
  },
  medical: {
    TYPE: "EDUCATION",
    TITLE: "Medical Scholarships",
    FILTER: [
      {
        label: "Medical",
        value: 677,
        TYPE: "EDUCATION"
      }
    ]
  },
  business: {
    TYPE: "EDUCATION",
    TITLE: "Business Scholarships",
    FILTER: [
      {
        label: "Business",
        value: 385,
        TYPE: "EDUCATION"
      }
    ]
  },
  arts: {
    TYPE: "EDUCATION",
    TITLE: "Arts Scholarships",
    FILTER: [
      {
        label: "Arts",
        value: 253,
        TYPE: "EDUCATION"
      }
    ]
  },
  law: {
    TYPE: "EDUCATION",
    TITLE: "Law Scholarships",
    FILTER: [
      {
        label: "Law",
        value: 278,
        TYPE: "EDUCATION"
      }
    ]
  },
  science: {
    TYPE: "EDUCATION",
    TITLE: "Science Scholarships",
    FILTER: [
      {
        label: "Science",
        value: 414,
        TYPE: "EDUCATION"
      }
    ]
  },
  "class-1-to-9": {
    TYPE: "EDUCATION",
    TITLE: "Class 1-9 Scholarships",
    FILTER: [
      {
        label: "Class 1",
        value: 2,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 2",
        value: 3,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 3",
        value: 4,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 4",
        value: 5,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 5",
        value: 6,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 6",
        value: 7,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 7",
        value: 8,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 8",
        value: 9,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 9",
        value: 10,
        TYPE: "EDUCATION"
      }
    ]
  },
  cultural: {
    TYPE: "SCHOLARSHIPS",
    TITLE: "Cultural Scholarships",
    FILTER: [
      {
        label: "Cultural",
        value: 122,
        TYPE: "SCHOLARSHIPS"
      }
    ]
  },
  sports: {
    TYPE: "SCHOLARSHIPS",
    TITLE: "Sports Scholarships",
    FILTER: [
      {
        label: "Sports",
        value: 125,
        TYPE: "SCHOLARSHIPS"
      }
    ]
  },
  "science-maths": {
    TYPE: "SCHOLARSHIPS",
    TITLE: "Science/Maths Scholarships",
    FILTER: [
      {
        label: "Science/Maths",
        value: 126,
        TYPE: "SCHOLARSHIPS"
      }
    ]
  },
  science: {
    TYPE: "EDUCATION",
    TITLE: "Science Scholarships",
    FILTER: [
      {
        label: "Science",
        value: 414,
        TYPE: "EDUCATION"
      }
    ]
  },
  school: {
    TYPE: "EDUCATION",
    TITLE: "School Scholarships",
    //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 16],
    // Class 4 Class 5 Class 6 Class 7 Class 8 Class 9 Class 10 Class 11 Class 12",
    FILTER: [
      {
        label: "KG",
        value: 1,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 1",
        value: 2,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 2",
        value: 3,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 3",
        value: 4,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 4",
        value: 5,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 5",
        value: 6,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 6",
        value: 7,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 7",
        value: 8,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 8",
        value: 9,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 9",
        value: 10,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 10",
        value: 11,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 11",
        value: 12,
        TYPE: "EDUCATION"
      },
      {
        label: "Class 12",
        value: 16,
        TYPE: "EDUCATION"
      }
    ]
  },
  "post-graduation": {
    TYPE: "EDUCATION",
    TITLE: "College Scholarships",
    FILTER: [
      {
        label: "Post Graduation",
        value: 23,
        TYPE: "EDUCATION"
      }
    ]
  },
  phd: {
    TYPE: "EDUCATION",
    TITLE: "Phd Scholarships",
    FILTER: [
      {
        label: "Phd",
        value: 24,
        TYPE: "EDUCATION"
      }
    ]
  },
  iti: {
    TYPE: "EDUCATION",
    TITLE: "ITI Scholarships",
    FILTER: [
      {
        label: "ITI",
        value: 840,
        TYPE: "EDUCATION"
      }
    ]
  },
  "polytechnique-diploma": {
    TYPE: "EDUCATION",
    TITLE: "Polytechnique/Diploma Scholarships",
    FILTER: [
      {
        label: "Polytechnique/Diploma",
        value: 21,
        TYPE: "EDUCATION"
      }
    ]
  },

  "means-based-scholarships": {
    TYPE: "SCHOLARSHIPS",
    TITLE: "Means Based Scholarships",
    FILTER: [
      {
        label: "Means based (Low family income)",
        value: 121,
        TYPE: "SCHOLARSHIPS"
      }
    ]
  },
  "talent-based-scholarships": {
    TYPE: "SCHOLARSHIPS",
    TITLE: "Talent Based Scholarships",
    FILTER: [
      {
        label: "Cultural talent (singing ,dancing & music)",
        value: 122,
        TYPE: "SCHOLARSHIPS"
      },
      {
        label: "Sports talent (individual/team sports)",
        value: 125,
        TYPE: "SCHOLARSHIPS"
      }
    ]
  },
  "merit-based-scholarships": {
    TYPE: "SCHOLARSHIPS",
    TITLE: "Merit Based Scholarships",
    FILTER: [
      {
        label: "Merit based (Competition & academic performance)",
        value: 120,
        TYPE: "SCHOLARSHIPS"
      }
    ]
  },
  "physically-challenged-scholarships": {
    TYPE: "SCHOLARSHIPS",
    TITLE: "Need Based Scholarships",
    FILTER: [
      {
        label: "Physically challenged",
        value: 130,
        TYPE: "SCHOLARSHIPS"
      }
    ]
  },
  "minorities-scholarships": {
    TYPE: "SCHOLARSHIPS",
    TITLE: "Minorities Scholarships",
    FILTER: [
      {
        label: "Minority Based",
        value: 132,
        TYPE: "SCHOLARSHIPS"
      }
    ]
  },
  "talent-based-scholarships": {
    TYPE: "SCHOLARSHIPS",
    TITLE: "Talent Based Scholarships",
    FILTER: [
      {
        label: "Talent Based",
        value: [125, 122],
        TYPE: "SCHOLARSHIPS"
      }
    ]
  }
};
