import gblFunc from "../globals/globalFunctions";
import moment from "moment";
export const prodEnv =
	typeof window !== "undefined"
		? window.location.hostname == "www.buddy4study.com"
			? 1 //live
			: 0 //qa or staging
		: 1;

const endPoint = {
	new:
		typeof process.env !== "undefined"
			? process.env.API_URL
			: typeof window !== "undefined"
				? window.location.hostname === "www.buddy4study.com"
					? "https://api.buddy4study.com/api/v1.0/"
					: window.location.hostname === "staging.ssr.b4s.in"
						? "http://103.12.135.80:8200/api/v1.0/"
						: "https://devapi.buddy4study.com:8444/api/v1.0/"
				: "https://api.buddy4study.com/api/v1.0/"
};

export const imgBaseUrl =
	"https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/";

export const imgBaseUrlDev = "http://img.b4s.in/static/images/";

export const getApi = function (apiKey, dynamicVals) {
	//method to replace dynamic values in the api url@params - (apiKey in the constants, array of dynamic values)

	var FormattedUrl = apiUrl[apiKey]["url"];
	for (var i = 0; i < dynamicVals.length; i++) {
		FormattedUrl = FormattedUrl.replace(
			apiUrl[apiKey]["dynamicValues"][i],
			dynamicVals[i]
		);
	}
	return FormattedUrl;
};

export const baseUrls = {
	ums: `${endPoint.new}ums`,
	ssms: `${endPoint.new}ssms`,
	utilms: `${endPoint.new}utilms`,
	qnams: `${endPoint.new}qnams`,
	admissionms: `${endPoint.new}admissionms`,
	uaa: `${endPoint.new}uaa`
};

//All dynamic values will be started from D...
export const apiUrl = {
	scholarshipsList: {
		url: "Dhost/ssms/scholarship",
		dynamicValues: ["Dhost"]
	},
	authService: `${endPoint.new}uaa/oauth/token`,
	userRules: `${endPoint.new}ums/user`,
	// userRules: `http://103.12.135.75:8887/user`, //Direct endpoint uncomment for testing
	dynamic_page_data: `${endPoint.new}utilms/page`,
	contactUs: `${endPoint.new}utilms/contactUs`,
	team: `${endPoint.new}utilms/teams`,
	//dynamicPageData: `${endPoint.new}pagedata/getNodeDataBySlug`,
	rulesList: `${endPoint.new}utilms/rule`,
	// rulesList: "http://103.12.135.75:8701/rule",// Direct endpoint uncomment for testing
	featuredScholarships: `${baseUrls.ssms}/scholarship?featured`,
	closedScholarships: "ScholarShip/upcomingScholarship",
	scholarshipDetail: `${endPoint.new}ssms/scholarship/`,
	faqsList: `${endPoint.new}utilms/faq`,
	faqDetails: `${endPoint.new}utilms/faq`,
	contentData: `${endPoint.new}utilms/page`,
	dashDetails: `${endPoint.new}utilms/scholarship/stats`, //to be used on home page
	careerOpportunities: `${endPoint.new}utilms/careerJob`,
	careerDetails: `${endPoint.new}utilms/careerJob`,
	cultureDetails: `${endPoint.new}utilms/career-event`,
	getUserList: `${endPoint.new}ums/user`,
	getUnsubscribed: `${endPoint.new}ums/user`,
	// loginUser: `http://103.12.135.75:8082/oauth/token`, //Direct endpoint uncomment for testing
	loginUser: `${endPoint.new}uaa/oauth/token`,
	/*   loginUser: "http://10.0.1.63:9000/services/oauth/token", */
	registerUser: `${endPoint.new}ums/user/register`,
	educationLoan: `${endPoint.new}ums/loanLead`,
	addUserRules: "Users/addUserRulesByUserId",
	updateUser: `${endPoint.new}ums/user`,
	getOccupation: `${endPoint.new}utilms/occupation`,
	familyEarningDetails: `${endPoint.new}ums/user`, // Dynamic URL
	referenceDetail: `${endPoint.new}ums/user`,
	getRules: `${endPoint.new}utilms/rule`,
	entranceExamination: `${endPoint.new}ums/user`,
	scholarshipHistory: `${endPoint.new}ums/user`,
	educationInfo: `${endPoint.new}ums/user`,
	occupationData: `${endPoint.new}utilms/occupation`,
	tokenGeneration: `${endPoint.new}uaa/oauth/token`,
	// tokenGeneration: `http://103.12.135.75:8082/oauth/token`, //Direct endpoint uncomment for testings
	changeUserPassword: `${endPoint.new}ums/user`,
	ruleFilter: `${endPoint.new}utilms/rule/filterRule`,
	documentGroupsAndTypes: `${baseUrls.ums}/documentGroup`,
	documentTypeCategories: `${baseUrls.ums}/documentTypeCategory`,
	documentTypeCategoryOptions: `${baseUrls.ums}/documentTypeCategoryOption`,
	documents: `${baseUrls.ums}/user`,
	mediaPartner: `${baseUrls.utilms}/mediaPartner/zone`, // media partner api
	mediaZone: `${baseUrls.utilms}/zone`,
	studentDetails: `${endPoint.new}ums/user`,
	matchUserRules: `${endPoint.new}ums/user`,
	matchingScholarships: `${endPoint.new}evalms`,
	mediaUrl: `${endPoint.new}utilms/shortUrl/`,
	favScholarships: `${endPoint.new}ums/user`,
	verifyUserRegistration: `${baseUrls.ums}/user/email/verify`,
	forgotUserPassword: `${baseUrls.ums}/user/password/forget`,
	resetUserPassword: `${baseUrls.ums}/user/password/reset`,
	applicationStatus: `${baseUrls.ssms}/user`,
	applicationStatusDoc: `${baseUrls.ums}/user`,
	awardeeDocApi: `${baseUrls.ums}/user`,
	docIssue: `${baseUrls.ums}/user`,
	disbursalAcknowledgement: `${baseUrls.ums}/user/`,
	awardeesList: `${endPoint.new}ssms/scholarshipResult`,
	awardeesDetails: `${endPoint.new}ssms/scholarshipResult`,
	docIssue: `${baseUrls.ums}/user`,
	qnaDash: `${baseUrls.qnams}/qna-question/user`,
	qnaNotification: `${baseUrls.qnams}`,
	seoTags: `${endPoint.new}utilms/metaTag/`,
	uploadUserPic: `${baseUrls.ums}/user`,
	slotBook: `${baseUrls.ssms}/scholarship`,
	preferredLanguages: `${baseUrls.utilms}/language`,
	hitCounterSchDetail: `${baseUrls.ssms}/scholarship`,
	getBoardList: `${baseUrls.utilms}/boards`,
	getCscUrl: `${baseUrls.ums}/csc/loginUrl`,
	cscUserDetails: `${baseUrls.ums}/csc/userDetails`,
	cscUserPayment: `${baseUrls.ums}/csc/paymentDetails`,
	scholarshipSlugByBsid: `${baseUrls.ssms}/scholarship`,
	scholarshipListByUsid: `${baseUrls.ssms}/scholarship/usid`,
	applicationPersonalInfo: `${baseUrls.ums}/user`,
	getApplicationConfig: `${baseUrls.ums}/scholarship`,
	marchentCall: `${baseUrls.ums}/user/`,
	applicationStepCall: `${baseUrls.ums}/user/`,
	internationalStudies: `${baseUrls.ums}/lead/international`,
	getInternationalStudy: `${baseUrls.ssms}/scholarship`,
	getEbooks: `${baseUrls.utilms}/ebook`,
	interestForm: `${baseUrls.utilms}/product/interest`,
	vlePaymentPlans: `${baseUrls.ums}/paymentplan`,
	getRelatedScholarship: `${baseUrls.ssms}/scholarship/related`,
	getRelatedArticle: `${baseUrls.utilms}/tag/article/related`,
	dmdcList: `${baseUrls.ums}`,
	dmdcApi: `${baseUrls.ums}/user`,
	userFavScholarship: `${baseUrls.ums}/user`,
	countryList: `${endPoint.new}utilms/get/country/list`,
	scholarshipFAQ: `${baseUrls.ssms}/scholarship`,
	getArchiveScholarship: `${baseUrls.ssms}/scholarship`,
	getArchiveScholars: `${baseUrls.ssms}/scholarshipResult`,
	addLikeFollow: `${baseUrls.ums}/user`,
	bookSlotDate: `${baseUrls.ssms}/telephonic`,
	footerBanner: `${baseUrls.utilms}/banner`,
	mediaUrlScholarship: `${endPoint.new}ssms/scholarship/`,
	promotionalBannerApi: `${baseUrls.utilms}/promotion/content?filter=`,
	cscPaymntSuccTransactionId: `${baseUrls.ums}/userpayment`,
	getQuestAnsw: `${baseUrls.qnams}`,
	getQuestAnsw1: `${baseUrls.qnams}`,
	postQuestAnsw: `${baseUrls.qnams}`,
	scholarshipVideoApi: `${baseUrls.utilms}/videolibrary?filter=`,
	resetPasswordTokenCheck: `${baseUrls.ums}/user/password/token/checkValid`,
	brandPage: `${baseUrls.ssms}/organisation`,
	scholarshipUrl: `${baseUrls.ssms}/organisation`,
	scholarshipUrlOrganisation: `${baseUrls.ssms}/organisation`,
	brandPageAboutUs: `${baseUrls.ums}/organisation`,
	faqPageUrl: `${baseUrls.ums}/organisation`,
	getFollower: `${baseUrls.ums}/organisation`,
	brandFollower: `${baseUrls.ums}/user`,
	registerAdmissionUser: `${baseUrls.admissionms}/user/register`,
	qualificationAdmissionUser: `${baseUrls.admissionms}/user`,
	resetPasswordTokenCheck: `${baseUrls.ums}/user/password/token/checkValid`,
	paymentStatus: `${baseUrls.ums}/user`,
	resultLookUpUrl: `${baseUrls.ssms}/scholar/result-lookup`,
	currentSchLookUp: `${baseUrls.ssms}/scholarship/current-year`,
	hulUserCode: `${baseUrls.utilms}/hul/employee`,
	likeFollow: `${baseUrls.qnams}`,
	scholarApi: `${baseUrls.ssms}/user`,
	onlineApplicationFormListApi: `${endPoint.new}ssms/scholarship/applications`,
	sendApi: `${baseUrls.ums}/user/otp/send`,
	verifyApi: `${baseUrls.ums}/user/otp/verify`,
	bankDetail: `${baseUrls.utilms}/bank-detail`,
	UpdateMobile: `${baseUrls.ums}/user`,
	userInfoByToken: `${baseUrls.ums}/user`,
	countingOTP: `${baseUrls.ums}/user/otp/send`,
	otpLogin: `${baseUrls.uaa}/user/login`,
	submitCvPopup: `${baseUrls.utilms}/career-job-application`,
	otpMobEmailApi: `${baseUrls.ums}/user`,
	deleteAnswer: `${baseUrls.qnams}`,
	editAnswer: `${baseUrls.qnams}`,
	presentClassInfo: `${baseUrls.ums}/user`,
	latestQue: `${baseUrls.qnams}/qna-question`,
	otpPasswordChange: `${baseUrls.ums}/user/password/otp/verify`,
	awardeesDetails2: `${endPoint.new}ssms/scholarship`,
	paymentAckt: `${baseUrls.ums}/user/payment/acknowledgement`,
	checkStatus: `${baseUrls.ums}/user`,
    donationUrl:`${endPoint.new}ums/donations`
}; //to be used on home page"

export const apiKeys = {
	FAMILYEARNINGINFO: "FAMILYEARNINGINFO",
	PERSONAL_INFO: "PERSONAL_INFO"
};

export const apiEndPoints = (key, ...params) => {
	const endPoints = {
		[apiKeys.FAMILYEARNINGINFO]: `${baseUrls.ums}/user/${params[0]}/application/${params[1]}/familyInfo`
	};

	return endPoints[key];
};

// /user/{userId}/application/{scholarshipId}/familyInfo

export const dependantApiUrl = {
	state: `${endPoint.new}utilms/district`,
	caState: `${endPoint.new}utilms/district`
};

export const utmSourceUrls = {
	GET_MY_UNI: "https://www.getmyuni.com/pixel?client_id=27"
};

export const defaultHomePageStats = {
	scholarshipFundDisbursed: "65",
	totalScholarshipFund: "45000",
	totalUser: "1700000",
	totalLegacyUsers: "130665",
	usersHelped: "75000"
};

//Query string for education loan pages api call
export const utmSources = { GET_MY_UNI: "getmyuni", SVG: "svg" };

export const applicationFormsUrl = {
	qa: "https://qa.form.b4s.in",
	prod: "https://www.buddy4study.com"
};

export const PRODUCTION_URL = "https://www.buddy4study.com/";
export const STAGING_URL = "https://staging.ssr.b4s.in/";

export const ADMISSION_PRODUCTION_URL = "https://admission.buddy4study.com/";
export const ADMISSION_STAGING_URL = "https://staging.admission.b4s.in:8090/";
export const INSTAMOJO_PEARSON =
	"https://www.instamojo.com/buddy4study/pearson-mepro-english-scholar-programme/";
export const INSTAMOJO_COLLEGE_BOARD_LIVE =
	"https://www.instamojo.com/buddy4study/sat-discount-voucher-fee/";
export const INSTAMOJO_COLLEGE_BOARD_TEST =
	"https://test.instamojo.com/atul_yadav/smp-premium-application/";

export const apiCallTypes = {
	GET: "GET",
	POST: "POST",
	PUT: "PUT",
	DELETE: "DELETE"
};
export const upgradedApplicationBSID = [
	"MIM4",
	"FAC1",
	"FAL9",
	"CSS22",
	"RHS1",
	"SGA3",
	"IS73",
	"SDS5",
	"SDS6",
	"CIS10",
	"LFL2",
	"CSP1",
	"CHO1",
	"TGSP1",
	"MIM5",
	"HUS2",
	"GGI6",
	"CRS11",
	"CRS9",
	"SMS10",
	"CAL2",
	"SBA1",
	"BKS1",
	"CBI2",
	"CBS3",
	"CES7",
	"CES8",
	"HEC6",
	"SCE3",
	"CKS1",
	"GMM2",
	"IGA1",
	"GMM2",
	"HUL1",
	"SSE4",
	"ASF1",
	"DB1",
	"LIF9",
	"LIF2",
	"UBS9",
	"HCL2"

];
export const headerClasses = {
	"": "black",
	scholarships: "black",
	scholarship: "black",
	register: "black",
	"scholarship-for": "black",
	"contact-us": "transparent",
	"terms-and-conditions": "transparent",
	"privacy-policy": "transparent",
	"scholarship-conclave": "white",
	collegeboard: "transparent",
	colgate: "transparent",
	team: "transparent",
	media: "black",
	"online-application-form": "black",
	"premium-membership": "transparent",
	"about-us": "black",
	careers: "black",
	faqs: "black",
	faq: "black",
	disclaimer: "black",
	university: "black",
	school: "black",
	student: "black",
	mediaoffer: "black",
	foundation: "black",
	corporation: "black",
	"mict-result": "black",
	"confirm-newsletter-subscription": "black",
	"payment-cancelled": "black",
	"payment-successful": "black",
	"payment-complete": "black",
	"thank-your-subscription": "black",
	"scholarship-result": "black",
	verifyuser: "black",
	resetPassword: "black",
	unsubscribed: "black",
	myprofile: "black",
	myscholarship: "black",
	subscribers: "black",
	scholar: "black",
	"404": "black",
	csc: "black",
	vle: "black",
	application: "black",
	qna: "transparent",
	"qna/": "black",
	videos: "black",
	payment: "transparent",
	page: "black",
	results: "black",
	"scholar-profile": "black"
};

/* Sticky Header Banner Position */
export const headerBannerPosClass = {
	"": "transparent",
	scholarships: "",
	scholarship: "",
	"scholarship-for": "",
	"contact-us": "topHeaderBanner",
	"terms-and-conditions": "topHeaderBanner",
	"privacy-policy": "topHeaderBanner",
	"scholarship-conclave": "",
	collegeboard: "topHeaderBanner",
	colgate: "topHeaderBanner",
	team: "topHeaderBanner",
	media: "topHeaderBanner",
	"online-application-form": "",
	"premium-membership": "topHeaderBanner",
	"about-us": "topHeaderBanner",
	careers: "topHeaderBanner",
	"career-opportunities": "topHeaderBanner",
	faqs: "",
	faq: "",
	disclaimer: "",
	"mict-result": "",
	"confirm-newsletter-subscription": "",
	"payment-cancelled": "",
	"payment-successful": "",
	"thank-your-subscription": "",
	verifyuser: "",
	resetPassword: "",
	unsubscribed: "",
	myprofile: "",
	myscholarship: "",
	subscribers: "",
	scholar: "",
	"404": "",
	csc: "",
	vle: "",
	qna: "topHeaderBanner",
	qcategories: "",
	videos: "topHeaderBanner",
	payment: "topHeaderBanner",
	"brand-page": "",
	"scholarship-result": ""
};

export const myScholarshipsRoute = "myScholarship";

export const screenWidth = {
	MAX_TABLET_WIDTH: 913
};

export const apiRequestData = {
	RULETYPES: [
		"class",
		"subject",
		"course",
		"state",
		"board",
		"orphan",
		"religion",
		"special",
		"gender",
		"quota",
		"family_income",
		"foreign",
		"orphan",
		"subscriber_role",
		"subscriber_relation",
		"entrance_exams",
		"level",
		"scholarship_provider",
		"disabled"
	]
};

export const loginRegisterRuleTypes = {
	RULETYPES: ["class", "religion", "gender", "quota"]
};

export const privateRoutes = ["/scholarship-for", "/myprofile"];

export const loginRegisterRequiredRules = [1, 5, 4, 6];

// export const requiredRuleKeys = ["class", "gender", "religion", "quota"];
export const requiredRuleKeys = {
	1: "class",
	4: "religion",
	5: "gender",
	6: "quota"
};

export const requiredRulesMap = new Map([
	["class", 1],
	["gender", 5],
	["religion", 4],
	["quota", 6]
]);
export const bookSlotInfo = {
	MIM4: {
		minDate: "2018-11-22",
		maxDate: "2018-12-07",
		excludes: [moment("2018-12-01"), moment("2018-12-02")]
	},
	FAL8: {
		minDate: "2018-12-04",
		maxDate: "2018-12-26",
		excludes: [
			moment("2018-12-08"),
			moment("2018-12-09"),
			moment("2018-12-15"),
			moment("2018-12-16"),
			moment("2018-12-22"),
			moment("2018-12-23")
		]
	}
};

export const seoPages = ["favicon.ico", "assets"];

export const noFollowPages = ["media-url"];
export const dependantRequests = ["state"];

export const scholarshipHistoryTypes = ["Private", "Government", "NGO"];

export const entranceExaminationLevels = ["State", "National", "International"];

export const yearArray = gblFunc.createRangeArray(
	parseInt(
		moment()
			.subtract(30, "years")
			.year()
	),
	parseInt(moment().year() + 1)
);

export const camelCase = obj => {
	let rtn = obj;
	if (typeof obj === "object") {
		if (obj instanceof Array) {
			rtn = obj.map(camelCase);
		} else {
			for (let key in obj) {
				if (obj.hasOwnProperty(key)) {
					const newKey = key.replace(/(_\w)/g, k => k[1].toUpperCase());
					rtn[newKey] = camelCase(obj[key]);

					if (newKey != key) {
						delete obj[key];
					}
				}
			}
		}
	}
	return rtn;
};

export const snakeCase = obj => {
	//return false;
	if (typeof obj != "object") return obj;

	for (var oldName in obj) {
		// Camel to underscore
		let newName = oldName.replace(/([A-Z])/g, function ($1) {
			return "_" + $1.toLowerCase();
		});
		// Only process if names are different
		if (newName != oldName) {
			// Check for the old property name to avoid a ReferenceError in strict mode.
			if (obj.hasOwnProperty(oldName)) {
				obj[newName] = obj[oldName];
				delete obj[oldName];
			}
		}
		// Recursion
		if (typeof obj[newName] == "object") {
			obj[newName] = snakeCase(obj[newName]);
		}
	}
	return obj;
};

export const allowedDocumentExtensions = [
	".bmp",
	".gif",
	".jpeg",
	".jpg",
	".png",
	".tif",
	".tiff",
	".doc",
	".docx",
	".odt",
	".pdf",
	".rtf",
	".7z",
	".zip",
	".rar",
	".BMP",
	".GIF",
	".JPEG",
	".JPG",
	".PNG",
	".TIF",
	".TIFF",
	".DOC",
	".DOCX",
	".ODT",
	".PDF",
	".RTF",
	".7Z",
	".ZIP",
	".RAR"
];

export const imageFileExtension = [
	".jpeg",
	".jpg",
	".png",
	".bmp",
	".tif",
	".JPEG",
	".JPG",
	".PNG",
	".BMP",
	".TIF"
];

const scholarshipLabelsArray = [
	[120, "Merit based"],
	[121, "Means based"],
	[122, "Cultural talent"],
	[125, "Sports talent"],
	[124, "Literary art"],
	[123, "Visual art"]
];

export const scholarshipLabelsMap = new Map(scholarshipLabelsArray);

export const defaultSeoTags = {
	robots: function (reqUrl) {
		var pathname = reqUrl.split("/").splice(1, 1)[0];
		return noFollowPages.indexOf(pathname) > -1 ? "noFollow" : "";
	},
	tags: {
		title: "India's largest scholarship platform | Buddy4Study",
		description:
			"Buddy4Study is India's largest scholarship platform which connects scholarship providers with seekers.",
		keywords:
			"scholar, scholarships in India, scholarship in India, scholarship portal, scholarship, scholarship India, scholarships, scholarship applications, online scholarship, scholarship websites, online scholarship application, scholarship search, study abroad scholarships"
	}
};

export const checkForSeoPage = function (reqUrl) {
	var pathname = reqUrl.split("/").splice(1, 1)[0];
	var reqUrlUpdated = reqUrl.split("?")[0];
	return seoPages.indexOf(pathname) > -1
		? "noIndex"
		: reqUrlUpdated.charAt(0) == "/" && reqUrlUpdated.length > 1
			? reqUrlUpdated.slice(1)
			: reqUrlUpdated;
};

export const confirmationMessages = {
	DASHBOARD: {
		DOCUMENTS: "Are you sure you want to delete this document?",
		REFERENCES: "Are you sure you want to delete this reference?",
		DMDC: "Successfully saved DM / DC"
	},
	GENERIC: "Are you sure you want to delete this record?"
};

//This json will be transferred to the external json file..to be put in cdn and call after page load as async.....
export const metaTagsJson = {
	"/": "Scholarship portal for Indian students | Find scholarships info online",
	"scholarship-for/class-10":
		"Scholarship for 10th class students for higher studies in India",
	"scholarship-for/class-12":
		"Scholarship for class 12 and 12th passed students for graduation",
	"scholarship-for/graduation":
		"Scholarships for college students to study in India or abroad",
	"scholarship-for/post-graduation":
		"Scholarships for postgraduate students in India - MBA, MTech etc",
	"scholarship-for/phd": "PhD scholarships and PhD fellowships for students",
	"scholarship-for/school":
		"Scholarships for school students in India looking for financial aid",
	"scholarship-for/International":
		"Study Abroad Scholarships for students pursuing higher studies",
	"scholarship-for/means-based-scholarships":
		"Means based scholarships for students who need financial aid",
	"scholarship-for/physically-challenged-scholarships":
		"Scholarships for students with disabilities - government and private",
	"scholarship-for/minorities-scholarships":
		"Minority scholarships for students belonging to SC, ST, OBC, muslims",
	"scholarship-for/talent-based-scholarships":
		"Talent scholarships for students talented in music, art, sports",
	"about-us": "India's Largest Scholarship Platform for students",
	article: "Latest scholarship Information for students education updates",
	careers: "Best Platform to Get Ahead With the Career",
	scholarships:
		"Scholarships for students in India - National and International",
	"education-loan": "Apply for Education Loan that suits your requirement",
	"contact-us": "Contact us for Scholarships Details",
	team: "Team behind the mission of making scholarship accessible",
	"user/login": "Register with us to access the list of scholarships",
	faqs: "Ask questions related to scholarship process, government scholarship",
	"scholarship-result": "Scholarship Results [Meet the winners]",
	"premium-membership": " Premium Membership [Get access to premium features]",
	"buddy4study-registration": "How to Register on Buddy4Study",
	"media/partners":
		"Our media partners [Gaining footprints across print, online, and digital platforms]",
	"terms-and-conditions": "Terms and Conditions to register",
	"contact-us": "Contact us for Scholarships Details"
};

//popup messages .....................................

export const messages = {
	login: {
		success: "You have successfully logged in!",
		error: "",
		confirmation: "",
		acknowledgement: ""
	},
	otp: {
		success: "OTP has been sent to your registered mobile number.",
		verified: "Your mobile number has been verified successfully.",
		verifyError: "Incorrect OTP entered.",
		alreadyVerifiedMobile:
			"This mobile number is verified with us already. Please use different one."
	},
	educationLoan: {
		success: "Your record submitted successfully!",
		error: "Sorry Something is Missing",
		confirmation: "",
		acknowledgement: ""
	},
	register: {
		success:
			"Your registration is successful. Please check your email for verification.",
		error: {
			702: "",
			sameRecord: ""
		},

		confirmation: "",
		acknowledgement: ""
	},

	changePassword: {
		success: "Your password changed successfully",
		failed: "Your old password is incorrect",
		confirmation: "",
		acknowledgement: ""
	},
	generic: {
		success: "Thank you for contacting with us",
		error: "Something went wrong. Please try again later.",

		confirmation: "",
		acknowledgement: "",
		generic: "",
		httpErrors: {
			400: "Oops! Something went wrong. We are working on fixing the problem.",
			500: "Oops! Something went wrong. We are working on fixing the problem.",
			404: "Oops! Something went wrong. We are working on fixing the problem.",
			403: "You don't have permission to access this resource",
			409: "Can not save same record",
			401: "You are not authorized",
			503: "Service unavailable",
			801: "No data found",
			803: "Incorrect password",
			702: "Record already exists",
			701: "Missing validation"
		},
		networkError:
			"Oops! Something went wrong. Please check your network connection and try again."
	},
	forgotPssword: {
		success: "Password verification email has been sent to you",
		error: "Oops! We couldn't locate this email. Please register.",
		confirmation: "",
		acknowledgement: ""
	},
	resetPassword: {
		success: "Your password has been reset successfully",
		error: "",
		confirmation: "",
		acknowledgement: ""
	},
	addMyFavScholarship: {
		success: "Scholarship has been added to your favorites!",
		error: "",
		confirmation: "",
		acknowledgement: ""
	},
	bookSlot: {
		success: "Slot has been booked successfully!",
		error: "",
		confirmation: "",
		acknowledgement: ""
	},
	admission: {
		registerSuccess: "You have registered successfully.",
		recordSaved: "Your details has been saved successfully."
	},
	resetPasswordTokenExpire: {
		success:
			"*Your link to reset password has expired. Please re-apply from ‘Forgot Password’ link to try again!",
		error: "",
		confirmation: "",
		acknowledgement: ""
	}
};

export const getYearOrMonth = (type, value) => {
	if (type == "year") {
		let difference = value[1] - value[0];
		let year = [];
		while (difference >= 0) {
			year.push(value[0]);
			value[0] += 1;
			difference--;
		}
		return year.reverse();
	} else {
		return ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
	}
};

export const setCookie = (cname, cvalue, hours) => {
	let d = new Date();
	d.setTime(d.getTime() + hours * 60 * 60 * 1000);
	let expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};

export const getCookie = cname => {
	let name = cname + "=";
	let decodedCookie = decodeURIComponent(document.cookie);
	let ca = decodedCookie.split(";");
	for (let i = 0; i < ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) == " ") {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
};

export const LOGIN_PAGE_ROUTE = "/user/login";
export const LOGIN_VLE_ROUTE = "/vle/login";

export const LOGOUT_PAGE_ROUTE = "/user/logout";

export const defaultImageLogo =
	"https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/logo-placeholder.jpg";

const Base64 = {
	keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	encode: encode,
	decode: decode
};

export const encode = input => {
	let output = "";
	let chr1,
		chr2,
		chr3 = "";
	let enc1,
		enc2,
		enc3,
		enc4 = "";
	let i = 0;

	do {
		chr1 = input.charCodeAt(i++);
		chr2 = input.charCodeAt(i++);
		chr3 = input.charCodeAt(i++);

		enc1 = chr1 >> 2;
		enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
		enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
		enc4 = chr3 & 63;

		if (isNaN(chr2)) {
			enc3 = enc4 = 64;
		} else if (isNaN(chr3)) {
			enc4 = 64;
		}

		output =
			output +
			Base64.keyStr.charAt(enc1) +
			Base64.keyStr.charAt(enc2) +
			Base64.keyStr.charAt(enc3) +
			Base64.keyStr.charAt(enc4);
		chr1 = chr2 = chr3 = "";
		enc1 = enc2 = enc3 = enc4 = "";
	} while (i < input.length);

	return output;
};

export const decode = input => {
	let output = "";
	let chr1,
		chr2,
		chr3 = "";
	let enc1,
		enc2,
		enc3,
		enc4 = "";
	let i = 0;

	// remove all characters that are not A-Z, a-z, 0-9, +, /, or =
	let base64test = /[^A-Za-z0-9\+\/\=]/g;
	if (base64test.exec(input)) {
		window.alert(
			"There were invalid base64 characters in the input text.\n" +
			"Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
			"Expect errors in decoding."
		);
	}
	input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

	do {
		enc1 = Base64.keyStr.indexOf(input.charAt(i++));
		enc2 = Base64.keyStr.indexOf(input.charAt(i++));
		enc3 = Base64.keyStr.indexOf(input.charAt(i++));
		enc4 = Base64.keyStr.indexOf(input.charAt(i++));

		chr1 = (enc1 << 2) | (enc2 >> 4);
		chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
		chr3 = ((enc3 & 3) << 6) | enc4;

		output = output + String.fromCharCode(chr1);

		if (enc3 != 64) {
			output = output + String.fromCharCode(chr2);
		}
		if (enc4 != 64) {
			output = output + String.fromCharCode(chr3);
		}

		chr1 = chr2 = chr3 = "";
		enc1 = enc2 = enc3 = enc4 = "";
	} while (i < input.length);

	return output;
};
export const UrlsToRedirect = new Map([
	[
		"/scholarship/west-bengal-post-martic-scholarship-for-sc-st-obc-2017",
		"/scholarship/west-bengal-post-matric-scholarship-for-sc-st-obc-2017"
	],
	[
		"/scholarship/serb-ramanujam-fellowships-2018",
		"/scholarship/serb-ramanujan-fellowships-2018"
	]
]);
export const pagesToNotFound = [
	"/scholarship/university-of-west-london-international-ambassador-scholarship-2019",
	"/scholarship/india-foundation-nursing-scholarship-programme-2018"
];
export const IMButtonJS = "https://js.instamojo.com/v1/button.js";
export const paymentPages = ["/premium-membership"];

// export const messages = {
//   loginSuccess: "You have successfully logged in!",
//   forgotPasswordSuccess: "Password verification email has been sent to you.",
//   registerSuccess:
//     "User has been registered successfully, click link in your email",
//   resetPassword: "Your password has been reset successfully",
//   contactSuccess: "Thank You! for Contacting with Us",
//   apiFailed: "Something went wrong! Please try again."
// };

export const Offer = {
	corp: "corporation",
	found: "foundation",
	media: "mediaOffer",
	sch: "school",
	univ: "university"
};

export const MediaClass = {
	"3": "radioStations",
	"1": "newsPaper",
	"2": "ePaper",
	"5": "radioStations"
};
export const scholarshipConclaveForms = [
	"register",
	"otp",
	"qualification",
	"futureStudyPlans",
	"competitionScore",
	"budget",
	"thanks"
];
export const formsWithProgressBar = [
	"qualification",
	"futureStudyPlans",
	"competitionScore",
	"budget"
];
function scholarshipConclaveConfig() {
	this.getNextForm = currentForm => {
		const currentFormIndex = scholarshipConclaveForms.indexOf(currentForm);
		return scholarshipConclaveForms[
			currentFormIndex > -1 ? currentFormIndex + 1 : 0
		];
	};
	this.defaultForm = this.getNextForm("default");
}
export const scholarshipConclaveConfigservice = new scholarshipConclaveConfig();

export const defaultProfilePic =
	"https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/profile-img.png";

export const fromNow = dateCreated => {
	let myDate = dateCreated
		.split(" ")[0]
		.split("-")
		.reverse()
		.join("-");

	let time = dateCreated.split(" ")[1];
	let getDate = new Date(myDate + " " + time);
	let change_version = Math.floor(getDate.getTime() / 1000);
	let dayToGo = moment.unix(change_version, "YYYYMMDD").fromNow();

	return dayToGo;
};

export const getCurrentDate = () => {
	let today = new Date();
	let dd = today.getDate();
	let mm = today.getMonth() + 1; //January is 0!

	let yyyy = today.getFullYear();
	if (dd < 10) {
		dd = "0" + dd;
	}
	if (mm < 10) {
		mm = "0" + mm;
	}
	return dd + "-" + mm + "-" + yyyy;
};

export const dateProcess = date => {
	let parts = date.split("-");
	return new Date(parts[2], parts[1] - 1, parts[0]);
};

export const imgError = ev => {
	return (ev.target.src = defaultProfilePic);
};

export const getDocStatus = id => {
	switch (id) {
		case 1:
			return "Correct";
		case 2:
			return "InCorrect";
		case 3:
			return "Missing";
		case 4:
			return "Blurred";
		case 5:
			return "Not Verified";
	}
};

export const getSemester = () => {
	let arr = [];
	for (let i = 1; i <= 12; i++) {
		arr.push({ id: i, year: `${i} Semester` });
	}
	return arr;
};
export const getAcadmicYear = () => {
	let arr = [];
	for (let i = 1; i <= 4; i++) {
		arr.push({ id: i, year: `${i} Year` });
	}
	return arr;
};
export const relationById = rId => {
	const relation = [
		{ id: "585", rulevalue: "Father" },
		{ id: "586", rulevalue: "Mother" },
		{ id: "587", rulevalue: "Brother" },
		{ id: "588", rulevalue: "Sister" },
		{ id: "757", rulevalue: "Grand Father" },
		{ id: "807", rulevalue: "Grand Mother" },
		{ id: "755", rulevalue: "Uncle" },
		{ id: "756", rulevalue: "Aunty" },
		{ id: "754", rulevalue: "Guardian" },
		{ id: "583", rulevalue: "Self" }
	];
	if (rId) {
		let rel = relation.find(x => x.id == rId);
		return rel.rulevalue;
	}
};
export const getPassingYear = () => {
	let arr = [];
	let date = new Date().getFullYear();
	for (let i = date - 10; i <= date + 10; i++) {
		arr.push({ id: i, year: i });
	}
	return arr;
};
