import {
	FETCH_RULES_REQUESTED,
	FETCH_RULES_SUCCEEDED,
	FETCH_RULES_FAILED,
	FETCH_USERLIST_SUCCEEDED,
	FETCH_USERLIST_REQUESTED,
	FETCH_USERLIST_FAILED,
	FETCH_DEPENDANT_DATA,
	FETCH_DEPENDANT_DATA_SUCCEEDED,
	FETCH_DEPENDANT_DATA_FAILED,
	FETCH_CONTACTUS_REQUESTED,
	FETCH_CONTACTUS_SUCCEEDED,
	FETCH_CONTACTUS_FAILED,
	FETCH_USER_RULES_REQUEST,
	FETCH_USER_RULES_SUCCESS,
	UPDATE_USER_RULES_SUCCESS,
	UPDATE_USER_RULES_FAILURE,
	UPDATE_USER_RULES_REQUEST,
	FETCH_FILTER_RULES_REQUESTED,
	FETCH_FILTER_RULES_SUCCEEDED,
	FETCH_FILTER_RULES_FAILED,
	FETCH_USER_MATCHING_RULES_REQUESTED,
	FETCH_USER_MATCHING_RULES_SUCCEEDED,
	FETCH_USER_MATCHING_RULES_FAILED,
	FETCH_BOARD_LIST_REQUESTED,
	FETCH_BOARD_LIST_SUCCEEDED,
	FETCH_BOARD_LIST_FAILED,
	FETCH_SCHOLARSHIP_APPLY_REQUEST,
	FETCH_SCHOLARSHIP_APPLY_SUCCESS,
	FETCH_SCHOLARSHIP_APPLY_FAILURE,
	FETCH_DEPENDANT_OTHER_DATA_SUCCEEDED,
	FETCH_DEPENDANT_OTHER_DATA,

	FETCH_OTP_MOBILE_CHANGE_REQUESTED,
	FETCH_OTP_MOBILE_CHANGE_SUCCEEDED,
	FETCH_OTP_MOBILE_CHANGE_FAILED,

	FETCH_OTP_EMAIL_CHANGE_REQUESTED,
	FETCH_OTP_EMAIL_CHANGE_SUCCEEDED,
	FETCH_OTP_EMAIL_CHANGE_FAILED,

	SUBMIT_OTP_MOB_EMAIL_CHANGE_REQUESTED,
	SUBMIT_OTP_MOB_EMAIL_CHANGE_SUCCEEDED,
	SUBMIT_OTP_MOB_EMAIL_CHANGE_FAILED
} from "./commonActions";

const initialState = {
	showLoader: true,
	rulesList: null,
	district: [],
	userRulesData: null,
	filterRules: []
};

const commonReducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case FETCH_RULES_REQUESTED:
			return Object.assign({}, state, { showLoader: true, isError: false });

		case FETCH_RULES_SUCCEEDED:
			return Object.assign({}, state, {
				showLoader: false,
				isError: false,
				type,
				rulesList: payload
			});

		case FETCH_RULES_FAILED:
			return Object.assign({}, state, {
				showLoader: false,
				isError: true,
				type,
				error: payload
			});
		case FETCH_USERLIST_REQUESTED:
			return payload;

		case FETCH_USERLIST_SUCCEEDED:
			return Object.assign({}, state, {
				userList: payload,
				showLoader: false,
				type
			});

		case FETCH_USERLIST_FAILED:
			return Object.assign({}, state, {
				showLoader: false,
				isError: true,
				userDetails: payload
			});
		case FETCH_CONTACTUS_REQUESTED:
			return { ...state, payload, type };
		case FETCH_CONTACTUS_SUCCEEDED:
			return Object.assign({}, state, {
				contactUs: payload,
				showLoader: false,
				type
			});

		case FETCH_CONTACTUS_FAILED:
			return Object.assign({}, state, {
				showLoader: false,
				isError: true,
				type,
				contactUs: payload
			});
		case FETCH_DEPENDANT_DATA:
			return Object.assign({}, state, { showLoader: true, isError: true });

		case FETCH_DEPENDANT_DATA_SUCCEEDED:
			return Object.assign({}, state, {
				showLoader: false,
				isError: false,
				district: payload
			});
		case FETCH_DEPENDANT_OTHER_DATA:
			return Object.assign({}, state, { showLoader: true, isError: true });
		case FETCH_DEPENDANT_OTHER_DATA_SUCCEEDED:
			return Object.assign({}, state, {
				showLoader: false,
				isError: false,
				otherdistrict: payload
			});

		case FETCH_USER_RULES_REQUEST:
			return { ...state, showLoader: true, type, isError: false };

		case FETCH_USER_RULES_SUCCESS:
			return {
				...state,
				showLoader: false,
				userRulesData: payload,
				type,
				isError: false
			};

		case UPDATE_USER_RULES_REQUEST:
			return { ...state, showLoader: true };

		case UPDATE_USER_RULES_SUCCESS:
			return { ...state, showLoader: false, updatedUserRulesData: payload };

		case UPDATE_USER_RULES_FAILURE:
			return { ...state, showLoader: false, isError: true };

		case FETCH_DEPENDANT_DATA_FAILED:
			return Object.assign({}, state, { showLoader: false, isError: true });

		case FETCH_FILTER_RULES_REQUESTED:
			return Object.assign({}, state, { showLoader: true, isError: false });

		case FETCH_FILTER_RULES_SUCCEEDED:
			return Object.assign({}, state, {
				showLoader: false,
				isError: false,
				filterRules: payload
			});

		case FETCH_FILTER_RULES_FAILED:
			return Object.assign({}, state, {
				showLoader: false,
				isError: true,
				error: payload
			});

		case FETCH_USER_MATCHING_RULES_REQUESTED:
			return Object.assign({}, state, { showLoader: true, isError: false });

		case FETCH_USER_MATCHING_RULES_SUCCEEDED:
			return Object.assign({}, state, {
				showLoader: false,
				isError: false,
				matchingRules: payload
			});

		case FETCH_USER_MATCHING_RULES_FAILED:
			return Object.assign({}, state, {
				showLoader: false,
				isError: true,
				error: payload
			});

		case FETCH_BOARD_LIST_REQUESTED:
			return Object.assign({}, state, { showLoader: true, isError: false });

		case FETCH_BOARD_LIST_SUCCEEDED:
			return Object.assign({}, state, {
				showLoader: false,
				isError: false,
				boardList: payload
			});

		case FETCH_BOARD_LIST_FAILED:
			return Object.assign({}, state, {
				showLoader: false,
				isError: true,
				error: payload
			});

		case FETCH_SCHOLARSHIP_APPLY_REQUEST:
			return Object.assign({}, state, { showLoader: true, isError: false });

		case FETCH_SCHOLARSHIP_APPLY_SUCCESS:
			return Object.assign({}, state, {
				showLoader: false,
				isError: false,
				type,
				schApply: payload
			});

		case FETCH_SCHOLARSHIP_APPLY_FAILURE:
			return Object.assign({}, state, {
				showLoader: false,
				type,
				isError: true,
				error: payload
			});

		case FETCH_OTP_MOBILE_CHANGE_REQUESTED:
			return { ...state, showLoader: true };

		case FETCH_OTP_MOBILE_CHANGE_SUCCEEDED:
			return { ...state, showLoader: false, otpMobileChange: payload, type };

		case FETCH_OTP_MOBILE_CHANGE_FAILED:
			return { ...state, showLoader: false, isError: true, type: type, error: payload };

		case FETCH_OTP_EMAIL_CHANGE_REQUESTED:
			return { ...state, showLoader: true };

		case FETCH_OTP_EMAIL_CHANGE_SUCCEEDED:
			return { ...state, showLoader: false, otpEmailChange: payload, type: type };

		case FETCH_OTP_EMAIL_CHANGE_FAILED:
			return { ...state, showLoader: false, isError: true, type: type, error: payload };

		case SUBMIT_OTP_MOB_EMAIL_CHANGE_REQUESTED:
			return { ...state, showLoader: true };

		case SUBMIT_OTP_MOB_EMAIL_CHANGE_SUCCEEDED:
			return { ...state, showLoader: false, submitOtp: payload, type: type };

		case SUBMIT_OTP_MOB_EMAIL_CHANGE_FAILED:
			return { ...state, showLoader: false, isError: true, type: type, error: payload };
		default:
			return state;
	}
};

export default commonReducer;
