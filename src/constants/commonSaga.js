import { call, put, takeEvery } from "redux-saga/effects";

import {
	FETCH_RULES_REQUESTED,
	FETCH_RULES_SUCCEEDED,
	FETCH_RULES_FAILED,
	FETCH_USERLIST_SUCCEEDED,
	FETCH_USERLIST_REQUESTED,
	FETCH_USERLIST_FAILED,
	FETCH_DEPENDANT_DATA,
	FETCH_DEPENDANT_DATA_SUCCEEDED,
	FETCH_DEPENDANT_DATA_FAILED,
	FETCH_CONTACTUS_REQUESTED,
	FETCH_CONTACTUS_SUCCEEDED,
	FETCH_CONTACTUS_FAILED,
	FETCH_GUEST_AUTH_DETAILS_SUCCESS,
	FETCH_GUEST_AUTH_DETAILS_FAILURE,
	FETCH_GUEST_AUTH_DETAILS_REQUEST,
	//User rules actions
	FETCH_USER_RULES_REQUEST,
	FETCH_USER_RULES_SUCCESS,
	UPDATE_USER_RULES_SUCCESS,
	UPDATE_USER_RULES_FAILURE,
	FETCH_USER_RULES_FAILURE,
	UPDATE_USER_RULES_REQUEST,
	FETCH_FILTER_RULES_REQUESTED,
	FETCH_FILTER_RULES_SUCCEEDED,
	FETCH_FILTER_RULES_FAILED,
	FETCH_USER_MATCHING_RULES_SUCCEEDED,
	FETCH_USER_MATCHING_RULES_REQUESTED,
	FETCH_USER_MATCHING_RULES_FAILED,
	FETCH_BOARD_LIST_REQUESTED,
	FETCH_BOARD_LIST_SUCCEEDED,
	FETCH_BOARD_LIST_FAILED,
	FETCH_SCHOLARSHIP_APPLY_REQUEST,
	FETCH_SCHOLARSHIP_APPLY_SUCCESS,
	FETCH_SCHOLARSHIP_APPLY_FAILURE,
	FETCH_DEPENDANT_OTHER_DATA_SUCCEEDED,
	FETCH_DEPENDANT_OTHER_DATA_FAILED,
	FETCH_DEPENDANT_OTHER_DATA,

	FETCH_OTP_MOBILE_CHANGE_REQUESTED,
	FETCH_OTP_MOBILE_CHANGE_SUCCEEDED,
	FETCH_OTP_MOBILE_CHANGE_FAILED,

	FETCH_OTP_EMAIL_CHANGE_REQUESTED,
	FETCH_OTP_EMAIL_CHANGE_SUCCEEDED,
	FETCH_OTP_EMAIL_CHANGE_FAILED,

	SUBMIT_OTP_MOB_EMAIL_CHANGE_REQUESTED,
	SUBMIT_OTP_MOB_EMAIL_CHANGE_SUCCEEDED,
	SUBMIT_OTP_MOB_EMAIL_CHANGE_FAILED,
} from "./commonActions";

import {
	apiRequestData,
	apiUrl,
	dependantApiUrl,
	apiCallTypes
} from "./constants";
import fetchClient from "../api/fetchClient";

//   API_URL: RuleType/list'
//   RULETYPES_VALUES: "RULETYPES": ["class", "subject", "course", "state", "board", "orphan", "religion", "special", "gender", "quota", "family_income", "foreign", "orphan", "subscriber_role", "subscriber_relation"]

const fetchAllRulesApi = input =>
	fetchClient
		.get(apiUrl.rulesList, {
			params: { filter: JSON.stringify(input) }
		})
		.then(res => {
			return res.data;
		});

function* fetchAllRules() {
	try {
		const { RULETYPES } = apiRequestData;
		const rules = yield call(fetchAllRulesApi, RULETYPES);

		yield put({
			type: FETCH_RULES_SUCCEEDED,
			payload: rules
		});
	} catch (error) {
		yield put({
			type: FETCH_RULES_FAILED,
			payload: error
		});
	}
}
/**
 * GET USER DETAILS API CALL
 *
 */

const fetchUserDetailUrl = userid =>
	fetchClient
		.get(apiUrl.getUserList + `/${userid}/personalInfo`, {})
		.then(res => {
			return res.data;
		});

function* fetchUserList(input) {
	try {
		const userList = yield call(fetchUserDetailUrl, input.payload.userid);

		yield put({
			type: FETCH_USERLIST_SUCCEEDED,
			payload: userList
		});
	} catch (error) {
		yield put({
			type: FETCH_USERLIST_FAILED,
			payload: error.errorMessage
		});
	}
}

/*
    DEPENDANT DATA API REQUESTS
    THESE ARE FIRED ON THE BASIS OF EXISTING VALUES
    (KEY AND VALUE PAIRS)
    KEY:DISTRICT -> URL: http://qa.b4s.in:8080/api/v1.0/mobile/getDistrict/454

*/

const fetchDependantDataApi = ({ KEY, DATA }) =>
	fetchClient.get(`${dependantApiUrl[KEY]}/${DATA}`).then(res => {
		return res.data;
	});

function* fetchDependantData(input) {
	try {
		const { payload } = input;
		const dependantData = yield call(fetchDependantDataApi, payload);

		yield put({
			type: FETCH_DEPENDANT_DATA_SUCCEEDED,
			payload: dependantData
		});
	} catch (error) {
		yield put({
			type: FETCH_DEPENDANT_DATA_FAILED,
			payload: error
		});
	}
}

const fetchOtherDependantDataApi = ({ KEY, DATA }) =>
	fetchClient.get(`${dependantApiUrl[KEY]}/${DATA}`).then(res => {
		return res.data;
	});

function* fetchOtherDependantData(input) {
	try {
		const { payload } = input;
		const dependantData = yield call(fetchOtherDependantDataApi, payload);

		yield put({
			type: FETCH_DEPENDANT_OTHER_DATA_SUCCEEDED,
			payload: dependantData
		});
	} catch (error) {
		yield put({
			type: FETCH_DEPENDANT_OTHER_DATA_FAILED,
			payload: error
		});
	}
}

const fetchContactUsUrl = input =>
	fetchClient.post(apiUrl.contactUs, input).then(res => {
		return res.data;
	});

function* submitContactUs(input) {
	let obj = {
		comment: input.payload["contact_data"].COMMENT,
		email: input.payload["contact_data"].EMAIL,
		mobile: input.payload["contact_data"].MOBILE,
		name: input.payload["contact_data"].NAME,
		subject: "",
		type: "need-help"
	};

	try {
		const contact_us = yield call(fetchContactUsUrl, obj);
		yield put({
			type: FETCH_CONTACTUS_SUCCEEDED,
			payload: contact_us
		});
	} catch (error) {
		yield put({
			type: FETCH_CONTACTUS_FAILED,
			payload: error.errorMessage
		});
	}
}

/*  Guest authentication saga*/
const guestAuthApiCall = inputData => {
	let guestAuthParams = new FormData();

	guestAuthParams.append("grant_type", "client_credentials");
	guestAuthParams.append("portalId", 1);

	return fetchClient.post(apiUrl.tokenGeneration, guestAuthParams).then(res => {
		return res.data;
	});
};

function* guestAuthentication(input) {
	try {
		const guestAuthData = yield call(guestAuthApiCall);

		yield put({
			type: FETCH_GUEST_AUTH_DETAILS_SUCCESS,
			payload: guestAuthData
		});
	} catch (error) {
		yield put({
			type: FETCH_GUEST_AUTH_DETAILS_FAILURE,
			payload: error.response
		});
	}
}

/*  User rules saga */

const userRulesApi = ({ inputData }, TYPE) => {
	const adminLite = sessionStorage.getItem("adminLite"); //TO differentiate lite call for admin and normal usetr
	let url = `${apiUrl.userRules}/${
		inputData.userid
		}/lite?adminLite=${adminLite}`;

	switch (TYPE) {
		case apiCallTypes.GET:
			url = `${apiUrl.userRules}/${
				inputData.userid
				}/lite?adminLite=${adminLite}`;
			return fetchClient.get(url).then(res => {
				return res.data;
			});

		case apiCallTypes.POST:
			url = `${apiUrl.userRules}/${inputData.userid}/rule`;
			return fetchClient.post(url, inputData.params).then(res => {
				return res.data;
			});
	}
};

function* userRules(input) {
	try {
		switch (input.type) {
			case FETCH_USER_RULES_REQUEST:
				const userRulesData = yield call(
					userRulesApi,
					input.payload,
					apiCallTypes.GET
				);

				yield put({
					type: FETCH_USER_RULES_SUCCESS,
					payload: userRulesData
				});

				break;

			case UPDATE_USER_RULES_REQUEST:
				const updatedUserRules = yield call(
					userRulesApi,
					input.payload,
					apiCallTypes.POST
				);

				yield put({
					type: UPDATE_USER_RULES_SUCCESS,
					payload: updatedUserRules
				});

				break;
		}
	} catch (error) {
		switch (input.type) {
			case FETCH_USER_RULES_REQUEST:
				yield put({
					type: FETCH_USER_RULES_FAILURE,
					payload: error
				});
				break;

			case UPDATE_USER_RULES_REQUEST:
				yield put({
					type: UPDATE_USER_RULES_SUCCESS,
					payload: error
				});
				break;
		}
	}
}

/* FILTER RULES */

const fetchRuleByRuleID = ({ inputData }) =>
	fetchClient.get(`${apiUrl.ruleFilter}/${inputData}`).then(res => {
		return res.data;
	});

function* fetchFilterRulesById(input) {
	try {
		const { payload } = input;
		const filterRule = yield call(fetchRuleByRuleID, payload);

		yield put({
			type: FETCH_FILTER_RULES_SUCCEEDED,
			payload: filterRule
		});
	} catch (error) {
		yield put({
			type: FETCH_FILTER_RULES_FAILED,
			payload: error
		});
	}
}

/* MATCHING USER RULES */

const fetchMatchUserRules = ({ inputData }) =>
	fetchClient
		.get(`${apiUrl.matchUserRules}/${inputData.userId}/matchingrules`)
		.then(res => {
			return res.data;
		});

function* fetchFilterRules(input) {
	try {
		const { payload } = input;
		const filterRule = yield call(fetchMatchUserRules, payload);

		yield put({
			type: FETCH_USER_MATCHING_RULES_SUCCEEDED,
			payload: filterRule
		});
	} catch (error) {
		yield put({
			type: FETCH_USER_MATCHING_RULES_FAILED,
			payload: error
		});
	}
}

/* BOARD LIST  */
const fetchBoardListApi = ({ inputData }) =>
	fetchClient.get(`${apiUrl.getBoardList}`).then(res => {
		return res.data;
	});

function* fetchBoardList(input) {
	try {
		const { payload } = input;
		const boardList = yield call(fetchBoardListApi, payload);

		yield put({
			type: FETCH_BOARD_LIST_SUCCEEDED,
			payload: boardList
		});
	} catch (error) {
		yield put({
			type: FETCH_BOARD_LIST_FAILED,
			payload: error
		});
	}
}

const fetchSchApplyApi = ({ scholarshipId, userId }) => {

	return fetchClient.get(
		`${
		apiUrl.scholarshipSlugByBsid
		}/${scholarshipId}/user/${userId}/scholarshipapply`
	)
		.then(res => {
			return res.data;
		});
}

function* fetchSchApply(input) {
	try {
		const schApplyList = yield call(fetchSchApplyApi, input.payload);

		yield put({
			type: FETCH_SCHOLARSHIP_APPLY_SUCCESS,
			payload: schApplyList
		});
	} catch (error) {
		yield put({
			type: FETCH_SCHOLARSHIP_APPLY_FAILURE,
			payload: error
		});
	}
}

const fetchOtpMobileChangeCall = payload =>
	fetchClient.post(`${apiUrl.otpMobEmailApi}/${payload.inputData.userId}/mobile/otp`, payload.inputData)
		.then(res => {
			return res.data;
		});

function* fetchOtpMobileChange(input) {
	try {
		const contact_us = yield call(fetchOtpMobileChangeCall, input.payload);
		yield put({
			type: FETCH_OTP_MOBILE_CHANGE_SUCCEEDED,
			payload: contact_us
		});
	} catch (error) {
		yield put({
			type: FETCH_OTP_MOBILE_CHANGE_FAILED,
			payload: error.response && error.response.data
		});
	}
}

const fetchOtpEmailChangeCall = payload => {
	let userId = parseInt(window.localStorage.getItem("userId"));
	return fetchClient
		.post(`${apiUrl.otpMobEmailApi}/${userId}/email/otp`, payload.inputData)
		.then(res => {
			//debugger
			return res.data;
		});
}

function* fetchOtpEmailChange(input) {
	try {
		//debugger
		const contact_us = yield call(fetchOtpEmailChangeCall, input.payload);
		yield put({
			type: FETCH_OTP_EMAIL_CHANGE_SUCCEEDED,
			payload: contact_us
		});
	} catch (error) {
		yield put({
			type: FETCH_OTP_EMAIL_CHANGE_FAILED,
			payload: error.response && error.response.data
		});
	}
}

const submitOtpMobEmailApi = payload => {
	let userId = parseInt(window.localStorage.getItem("userId"));
	return fetchClient
		.post(`${apiUrl.otpMobEmailApi}/${userId}/email-mobile/otp/verify`, payload.inputData)
		.then(res => {
			return res.data;
		});
}

function* submitOtpMobEmail(input) {
	try {
		const contact_us = yield call(submitOtpMobEmailApi, input.payload);
		yield put({
			type: SUBMIT_OTP_MOB_EMAIL_CHANGE_SUCCEEDED,
			payload: contact_us
		});
	} catch (error) {
		yield put({
			type: SUBMIT_OTP_MOB_EMAIL_CHANGE_FAILED,
			payload: error.response && error.response.data
		});
	}
}

export default function* commonSaga() {
	yield takeEvery(FETCH_RULES_REQUESTED, fetchAllRules);
	yield takeEvery(FETCH_USERLIST_REQUESTED, fetchUserList);
	yield takeEvery(FETCH_DEPENDANT_DATA, fetchDependantData);
	yield takeEvery(FETCH_CONTACTUS_REQUESTED, submitContactUs);

	yield takeEvery(FETCH_GUEST_AUTH_DETAILS_REQUEST, guestAuthentication);

	/* User rules saga */
	yield takeEvery(FETCH_USER_RULES_REQUEST, userRules);
	yield takeEvery(UPDATE_USER_RULES_REQUEST, userRules);

	/* Filter Rule */

	yield takeEvery(FETCH_FILTER_RULES_REQUESTED, fetchFilterRulesById);

	/* USER MATCHING RULE*/
	yield takeEvery(FETCH_USER_MATCHING_RULES_REQUESTED, fetchFilterRules);

	/* BOARD LIST */
	yield takeEvery(FETCH_BOARD_LIST_REQUESTED, fetchBoardList);

	/* SCHOLARSHIP APPLY */
	yield takeEvery(FETCH_SCHOLARSHIP_APPLY_REQUEST, fetchSchApply);

	yield takeEvery(FETCH_DEPENDANT_OTHER_DATA, fetchOtherDependantData);
	yield takeEvery(FETCH_OTP_MOBILE_CHANGE_REQUESTED, fetchOtpMobileChange);
	yield takeEvery(FETCH_OTP_EMAIL_CHANGE_REQUESTED, fetchOtpEmailChange);
	yield takeEvery(SUBMIT_OTP_MOB_EMAIL_CHANGE_REQUESTED, submitOtpMobEmail);

}
