export const formInputs = {
  need_help: {
    NAME: {
      elementType: "input",
      label: "Your Name",
      classes: ["col-md-12"],
      elementConfig: {
        type: "text",
        placeholder: "Name",
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { lable: false }
    },
    EMAIL: {
      elementType: "input",
      label: "Email",
      classes: ["col-md-12"],
      elementConfig: {
        type: "email",
        placeholder: "Email",
        className: "form-control"
      },
      value: "",
      validation: { required: true, isEmail: true },
      valid: false,
      touched: false,
      hide: { lable: false }
    },
    MOBILE: {
      elementType: "input",
      label: "Mobile",
      classes: ["col-md-12"],
      elementConfig: {
        type: "text",
        placeholder: "Mobile",
        maxLength: "10",
        minLength: "10",
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { lable: false }
    },
    COMMENT: {
      elementType: "textarea",
      label: "Comment",
      classes: ["col-md-12"],
      elementConfig: { placeholder: "Comment", className: "form-control" },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { lable: false }
    }
  },
  contact_form: {
    NAME: {
      elementType: "input",
      label: "Your Name",
      classes: ["col-md-12"],
      elementConfig: {
        type: "text",
        placeholder: "Name",
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    EMAIL: {
      elementType: "input",
      label: "Email",
      classes: ["col-md-12"],
      elementConfig: {
        type: "email",
        placeholder: "E-Mail",
        className: "form-control"
      },
      value: "",
      validation: { required: true, isEmail: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    MOBILE: {
      elementType: "input",
      label: "Mobile",
      classes: ["col-md-12"],
      elementConfig: {
        type: "text",
        placeholder: "Mobile",
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    SUBJECT: {
      elementType: "input",
      label: "Subject",
      classes: ["col-md-12"],
      elementConfig: {
        type: "text",
        placeholder: "Subject",
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    COMMENT: {
      elementType: "textarea",
      label: "Comment",
      classes: ["col-md-12"],
      elementConfig: {
        placeholder: "Comment",
        className: "form-control",
        rows: "4"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    }
  },
  Personal_Info: {
    FIRST_NAME: {
      elementType: "input",
      label: "First Name*",
      classes: ["col-md-6"],
      elementConfig: {
        type: "text",
        placeholder: "Enter your first name",
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    LAST_NAME: {
      elementType: "input",
      label: "Last Name",
      classes: ["col-md-6"],
      elementConfig: {
        type: "text",
        placeholder: "Enter your last name",
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    EMAIL: {
      elementType: "input",
      label: "Email*",
      classes: ["col-md-6"],
      elementConfig: {
        type: "email",
        placeholder: "Enter your email",
        className: "form-control"
      },
      value: "",
      validation: { required: true, isEmail: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    MOBILE: {
      elementType: "input",
      label: "Mobile Number*",
      classes: ["col-md-6"],
      elementConfig: {
        type: "text",
        placeholder: "Enter your mobile",
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    Aadhaar: {
      elementType: "input",
      label: "Aadhaar Number",
      classes: ["col-md-6"],
      elementConfig: {
        type: "text",
        placeholder: "Enter your adhaar number",
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    DOB: {
      elementType: "input",
      label: "Date of Birth",
      classes: ["col-md-6"],
      elementConfig: {
        type: "text",
        placeholder: "Enter your date of birth",
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    POSTAL_ADDRESS: {
      elementType: "input",
      label: "Postal Address*",
      classes: ["col-md-6"],
      elementConfig: {
        type: "text",
        placeholder: "Enter your postal address",
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    PINCODE: {
      elementType: "input",
      label: "Pincode*",
      classes: ["col-md-6"],
      elementConfig: {
        type: "text",
        placeholder: "Enter your pincode",
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    STATE: {
      elementType: "select",
      label: "State*",
      classes: ["col-md-6"],
      elementConfig: {
        options: [],
        className: "form-control",
        optionTitle: "--Select State--"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    DISTRICT: {
      elementType: "select",
      label: "District*",
      classes: ["col-md-6"],
      isDependant: true,
      dependsOn: "STATE",
      elementConfig: {
        options: [],
        className: "form-control",
        optionTitle: "--Select District--"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    CITY: {
      elementType: "input",
      label: "City*",
      classes: ["col-md-6"],
      elementConfig: {
        type: "text",
        placeholder: "Enter City",
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    RELIGION: {
      elementType: "select",
      label: "Religion",
      classes: ["col-md-6"],
      elementConfig: {
        options: [],
        className: "form-control",
        optionTitle: "--Select Religion--"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    CATEGORY: {
      elementType: "select",
      label: "Category",
      classes: ["col-md-6"],
      elementConfig: {
        options: [],
        className: "form-control",
        optionTitle: "--Select Category--"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    GENDER: {
      elementType: "select",
      label: "Gender*",
      classes: ["col-md-6"],
      elementConfig: {
        options: [],
        className: "form-control",
        optionTitle: "--Select Gender--"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    DISABLED: {
      elementType: "select",
      label: "Physically Challenged*",
      classes: ["col-md-6"],
      elementConfig: {
        options: [
          { ID: "Yes", RULEVALUE: "Yes" },
          { ID: "No", RULEVALUE: "No" }
        ],
        className: "form-control",
        optionTitle: "--Physically Challenged--"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    },
    ANNUAL_FAM_INCOME: {
      elementType: "input",
      label: "Annual Family Income*",
      classes: ["col-md-6"],
      elementConfig: {
        type: "text",
        placeholder: "Annual Family Income",
        className: "form-control",
        "aria-label": "Amount (to the nearest dollar)"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false, span: false }
    },
    ARE_YOU_LOOKING: {
      elementType: "select",
      label: "Are you looking for a scholarship to study abroad?",
      classes: ["col-md-6"],
      elementConfig: {
        options: [
          { ID: "820", RULEVALUE: "No" },
          { ID: "698", RULEVALUE: "Yes" }
        ],
        className: "form-control"
      },
      value: "",
      validation: { required: true },
      valid: false,
      touched: false,
      hide: { label: false }
    }
  },
  formIsValid: false
};
