import React, { Component } from "react";
import { Link } from "react-router-dom";
import { breadCrumObj } from "../../../constants/breadCrum";
import { Helmet } from "react-helmet";
import gblFunc from "../../../globals/globalFunctions";
import BreadCrum from "../../components/bread-crum/breadCrum";
import Loader from "../../common/components/loader";
import { imgBaseUrl } from "../../../constants/constants";
import ServerError from "../../common/components/serverError";

class City extends Component {
  constructor() {
    super();
    this.state = {
      activeTab: "bangaloreScholar"
    };

    this.toggleTab = this.toggleTab.bind(this);
  }
  toggleTab(e) {
    if (e.target.nodeName === "LI") {
      const { id } = e.target;
      this.setState({
        activeTab: id
      });
    }
  }
  render() {
    if (this.props.isError) {
      return (
        <section>
          <section className="gray">
            <section className="tnc topPos">
              <BreadCrum
                classes={breadCrumObj["disclaimer"]["bgImage"]}
                listOfBreadCrum={breadCrumObj["disclaimer"]["breadCrum"]}
                title={breadCrumObj["disclaimer"]["title"]}
              />
              <ServerError errorMessage={this.props.errorMessage} />
            </section>
          </section>
        </section>
      );
    }

    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <section className="gray">
          <section className="tnc topPos">
            <Helmet> </Helmet>
            <BreadCrum
              classes={breadCrumObj["city"]["bgImage"]}
              listOfBreadCrum={breadCrumObj["city"]["breadCrum"]}
              title={breadCrumObj["city"]["title"]}
            />
            <article className="city">
              <section className="conatiner-fluid graybg">
                <article className="container">
                  <article className="row">
                    <h1 className="title">
                      Merck India Charitable Trust (MICT) Scholarship Program
                      2018-19
                    </h1>
                    <section className="cityTabWrapper">
                      <ul onClick={this.toggleTab}>
                        <li
                          id="bangaloreScholar"
                          className={
                            this.state.activeTab === "bangaloreScholar"
                              ? "active"
                              : ""
                          }
                        >
                          Bangalore
                        </li>
                        <li
                          id="mumbaiScholar"
                          className={
                            this.state.activeTab === "mumbaiScholar"
                              ? "active"
                              : ""
                          }
                        >
                          Mumbai
                        </li>
                      </ul>
                      <article className="tabContent">
                        {this.state.activeTab === "bangaloreScholar" ? (
                          <Link
                            to={`${imgBaseUrl}bangalore-scholar.jpg`}
                            target="_blank"
                          >
                            <img
                              src={`${imgBaseUrl}bangalore-scholar.jpg`}
                              alt="MICT Scholarship result Bangalore"
                              className="img-responsive"
                            />
                          </Link>
                        ) : (
                            <Link
                              to={`${imgBaseUrl}mumbai-scholar.jpg`}
                              target="_blank"
                            >
                              <img
                                src={`${imgBaseUrl}mumbai-scholar.jpg`}
                                alt="MICT Scholarship result Mumbai"
                                className="img-responsive"
                              />
                            </Link>
                          )}
                      </article>
                    </section>
                  </article>
                </article>
              </section>
            </article>
          </section>
        </section>
      </section>
    );
  }
}

export default City;
