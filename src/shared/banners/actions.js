export const FOOTER_BANNER_REQUESTED = "FOOTER_BANNER_REQUESTED";
export const FOOTER_BANNER_SUCCEEDED = "FOOTER_BANNER_SUCCEEDED";
export const FOOTER_BANNER_FAILED = "FOOTER_BANNER_FAILED";

export const footerBanner = data => ({
  type: FOOTER_BANNER_REQUESTED,
  payload: { inputData: data }
});
