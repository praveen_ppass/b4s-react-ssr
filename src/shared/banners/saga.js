import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FOOTER_BANNER_REQUESTED,
  FOOTER_BANNER_SUCCEEDED,
  FOOTER_BANNER_FAILED
} from "./actions";

const footerBannerApi = ({ inputData }) => {
  return fetchClient.get(apiUrl.footerBanner).then(res => {
    return res.data;
  });
};

function* footerBanner(input) {
  try {
    const bannerData = yield call(footerBannerApi, input.payload);
    yield put({
      type: FOOTER_BANNER_SUCCEEDED,
      payload: bannerData
    });
  } catch (error) {
    yield put({
      type: FOOTER_BANNER_FAILED,
      payload: error.errorMessage
    });
  }
}

export default function* footerBannerSaga() {
  yield takeEvery(FOOTER_BANNER_REQUESTED, footerBanner);
}
