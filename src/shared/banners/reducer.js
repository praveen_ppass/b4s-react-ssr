import {
  FOOTER_BANNER_REQUESTED,
  FOOTER_BANNER_SUCCEEDED,
  FOOTER_BANNER_FAILED
} from "./actions";

const initialState = {
  showLoader: false,
  isError: false,
  errorMessage: "",
  bannerData: ""
};

const footerBannerReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FOOTER_BANNER_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type,
        errorMessage: ""
      };

    case FOOTER_BANNER_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type: "FOOTER_BANNER_SUCCEEDED",
        bannerData: payload
      };

    case FOOTER_BANNER_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        type: "FOOTER_BANNER_FAILED",
        errorMessage: payload
      };
    default:
      return state;
  }
};
export default footerBannerReducer;
