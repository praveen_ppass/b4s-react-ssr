import React, { Component } from "react";
import DesktopFooterBanner from "./desktopFooterBanner";
import MobileFooterBanner from "./mobileFooterBanner";
import {
  FOOTER_BANNER_REQUESTED,
  FOOTER_BANNER_SUCCEEDED,
  FOOTER_BANNER_FAILED
} from "../actions";
import { getCookie } from "../../../constants/constants";

export default class FooterBanner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bannerData: [],
      bannerSize: 2,
      isMobileFooterBanner: false
    };
    this.trackUrlLeftComponent = this.trackUrlLeftComponent.bind(this);
    this.updateMobileDimensions = this.updateMobileDimensions.bind(this);
  }

  trackUrlLeftComponent(link) {
    window.open(link);
  }
  componentDidMount() {
    this.props.footerBannerApi();
    this.updateMobileDimensions();
    window.addEventListener("resize", this.updateMobileDimensions.bind(this));
  }

  componentWillReceiveProps(nextProps) {
    const { type, bannerData } = nextProps;
    switch (type) {
      case FOOTER_BANNER_SUCCEEDED:
        this.setState({ bannerData: bannerData });
        break;
    }
  }

  updateMobileDimensions() {
    if (window.innerWidth <= 767) {
      this.setState(
        {
          isMobileFooterBanner: true
        },
        () => this.mobileSliderHandler()
      );
    } else {
      this.setState({
        isMobileFooterBanner: false
      });
    }
  }

  mobileSliderHandler() {
    let current = 0;
    let time = 15000;

    if (typeof window !== "undefined" && !getCookie("schFooterName")) {
      let messageSlides = document.getElementsByClassName(
        "fixed-banner-slider"
      );
      setInterval(() => {
        for (let i = 0; i < messageSlides.length; i++) {
          messageSlides[i].style.bottom = "-120px";
        }
        messageSlides[current].style.bottom = "0px";
        current = current != messageSlides.length - 1 ? current + 1 : 0;
      }, time);
    }
  }

  render() {
    const { bannerData, isMobileFooterBanner } = this.state;
    return (
      <section>
        {typeof window !== "undefined" && isMobileFooterBanner ? (
          //&& /Android/i.test(navigator.userAgent)
          <MobileFooterBanner
            bannerData={bannerData}
            trackUrlLeftComponent={this.trackUrlLeftComponent}
            footerBanner={this.props.footerBanner}
            isMobileFooterBanner={isMobileFooterBanner}
          />
        ) : (
          <DesktopFooterBanner
            bannerData={bannerData}
            trackUrlLeftComponent={this.trackUrlLeftComponent}
            footerBanner={this.props.footerBanner}
          />
        )}
      </section>
    );
  }
}
