import React from "react";

const mobileFooterBanner = props => {
  const bannersData = props.bannerData.filter((list, index) => index <= 2);
  return (
    <article className="mobile">
      {bannersData.length <= 3
        ? bannersData.map(banner => (
            <section className="fixed-banner-slider" key={banner.id}>
              <span
                className="cross"
                onClick={() =>
                  props.footerBanner("schFooterName", "schBanner", 24)
                }
              >
                &times;
              </span>
              <article className="bannerWrapper">
                <article className="tblCell">
                  <span
                    onClick={() => props.trackUrlLeftComponent(banner.link)}
                  >
                    {banner.description} <i>Click here</i>
                  </span>
                </article>
              </article>
            </section>
          ))
        : ""}
    </article>
  );
};

export default mobileFooterBanner;
