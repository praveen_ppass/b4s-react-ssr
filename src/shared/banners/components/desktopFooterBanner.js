import React from "react";

const desktopFooterBanner = props => {
  let bannrData = [];
  let classes = ["fixed-banner"];
  if (props.bannerData.length) {
    if (props.bannerData.length >= 3)
      bannrData = props.bannerData.filter((list, index) => index <= 2);
    else bannrData = props.bannerData;

    const bnnerLength = bannrData.length;
    switch (bnnerLength) {
      case 1:
        classes.push("bannerOne");
        break;
      case 2:
        classes.push("bannerTwo");
        break;
      case 3:
        break;
    }
  }
  return <Banner classes={classes} bannrData={bannrData} {...props} />;
};

const Banner = props => {
  return (
    <article>
      <section className={props.classes.join(" ")}>
        <span
          className="cross"
          onClick={() => props.footerBanner("schFooterName", "schBanner", 24)}
        >
          &times;
        </span>
        {props.bannrData
          ? props.bannrData.map(banner => (
              <article className="bannerWrapper" key={banner.id}>
                <article className="tblCell">
                  <span
                    onClick={() => props.trackUrlLeftComponent(banner.link)}
                  >
                    {banner.description} <i>Click here</i>
                  </span>
                </article>
              </article>
            ))
          : ""}
      </section>
    </article>
  );
};

export default desktopFooterBanner;
