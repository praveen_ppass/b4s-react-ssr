import React from "react";
import FooterBanner from "../components/footerBanner";

import { footerBanner as footerBannerAction } from "../actions";

import { connect } from "react-redux";

const mapStateToProps = ({ footerBanner }) => ({
  showLoader: footerBanner.showLoader,
  type: footerBanner.type,
  isError: footerBanner.isError,
  errorMessage: footerBanner.errorMessage,
  bannerData: footerBanner.bannerData
});

const mapDispatchToProps = dispatch => ({
  footerBannerApi: banner => dispatch(footerBannerAction(banner))
});

export default connect(mapStateToProps, mapDispatchToProps)(FooterBanner);
