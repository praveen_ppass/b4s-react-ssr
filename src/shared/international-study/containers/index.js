import { connect } from "react-redux";
import {
  submitInternationalStudies as internationStudyAction,
  fetchInternationalStudy as fetchInternationalStudyAction,
  fetchEbooks as fetchEbooksAction
} from "../actions";
import studyAbroadPage from "../../international-study/components/";

const mapStateToProps = ({ internationalStd }) => ({
  showLoader: internationalStd.showLoader,
  internationalStudy: internationalStd.internationStudy,
  internationalError: internationalStd.internationalError,
  type: internationalStd.type,
  ebooks: internationalStd.ebooks
});
const mapDispatchToProps = dispatch => ({
  internationStudyLoad: data => dispatch(internationStudyAction(data)),
  getInternationStudyLoad: data =>
    dispatch(fetchInternationalStudyAction(data)),
  getEbooks: data => dispatch(fetchEbooksAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(studyAbroadPage);
