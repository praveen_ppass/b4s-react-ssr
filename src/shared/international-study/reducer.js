import {
  SUBMIT_INTERNATIONAL_STUDIES_REQUESTED,
  SUBMIT_INTERNATIONAL_STUDIES_SUCCEEDED,
  SUBMIT_INTERNATIONAL_STUDIES_FAILED,
  FETCH_INTERNATION_STUDY_LIST_SUCCEEDED,
  FETCH_INTERNATION_STUDY_LIST_REQUESTED,
  FETCH_INTERNATION_STUDY_LIST_FAILED,
  FETCH_INTERNATION_EBOOK_SUCCEEDED,
  FETCH_INTERNATION_EBOOK_FAILED,
  FETCH_INTERNATION_EBOOK_REQUESTED
} from "./actions";

const initialState = {
  showLoader: false,
  internationalError: ""
};
const internationalStudyReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SUBMIT_INTERNATIONAL_STUDIES_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        internationalError: "",
        isError: false
      };
    case SUBMIT_INTERNATIONAL_STUDIES_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        internationalError: "",
        isError: false
      };
    case SUBMIT_INTERNATIONAL_STUDIES_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        internationalError: payload,
        isError: true
      };
    case FETCH_INTERNATION_STUDY_LIST_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        internationStudy: "",
        isError: false
      };
    case FETCH_INTERNATION_STUDY_LIST_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        internationStudy: payload,
        isError: false
      };
    case FETCH_INTERNATION_STUDY_LIST_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true
      };

    case FETCH_INTERNATION_EBOOK_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        ebooks: "",
        isError: false
      };
    case FETCH_INTERNATION_EBOOK_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        ebooks: payload,
        isError: false
      };
    case FETCH_INTERNATION_EBOOK_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true
      };
    default:
      return state;
  }
};
export default internationalStudyReducer;
