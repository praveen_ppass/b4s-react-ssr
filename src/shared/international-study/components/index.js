import React, { Component } from "react";
import Slider from "react-slick";
import querystring from "query-string";
import { Link } from "react-router-dom";
import "url-search-params-polyfill";
import Loader from "../../common/components/loader";
import Testimonials from "./testimonials";
import { ruleRunner } from "../../../validation/ruleRunner";
import {
  required,
  isEmail,
  minLength,
  isNumeric,
  isMobileNumber,
  alphabetsOnly
} from "../../../validation/rules";
import AlertMessage from "../../common/components/alertMsg";
import {
  SUBMIT_EDUCATIONLOAN_SUCCEEDED,
  SUBMIT_EDUCATIONLOAN_FAILED,
  SUBMIT_INTERNATIONAL_STUDIES_SUCCEEDED,
  SUBMIT_INTERNATIONAL_STUDIES_FAILED
} from "../../international-study/actions";
import { messages, imgBaseUrl, utmSources } from "../../../constants/constants";
import gblFunc from "../../../globals/globalFunctions";
const ebooksUrls = {
  uk:
    "https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/SAS/UK-E-Book.pdf",
  us:
    "https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/SAS/US-E-Book.pdf"
};
class studyAbroadPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activity: "",
      country: "",
      exams: "",
      programs: "",
      email: "",
      mobile: "",
      name: "",
      proOthers: "",
      conOthers: "",
      ebookId: "",
      downldName: "",
      downldEmail: "",
      downldMobile: "",
      downldCountry: "",
      downldConOthers: "",
      downldProgram: "",
      downldExam: "",
      downldProOthers: "",
      downldActivity: "",
      utmSource: "",
      utmMedium: "",
      ebookUrl: {},
      ebookFor: "",
      source: "international-studies",
      statusAlert: "",
      isShowAlert: false,
      internationalStudyPopup: true,
      alertMsg: "",
      validations: {
        name: null,
        email: null,
        mobile: null,
        country: null,
        exams: null,
        programs: null,
        proOthers: null,
        conOthers: null
      },
      downloadValidation: {
        downldName: null,
        downldEmail: null,
        downldMobile: null,
        downldCountry: null,
        downldProgram: null,
        downldExam: null,
        downldProOthers: null,
        downldConOthers: null
      }
    };

    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.submitEducation = this.submitEducation.bind(this);
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.onChangeEbook = this.onChangeEbook.bind(this);
    this.onCloseEbook = this.onCloseEbook.bind(this);
    this.onSubmitPDFHandler = this.onSubmitPDFHandler.bind(this);
    this.onFormPdfFieldChange = this.onFormPdfFieldChange.bind(this);
  }

  hideAlert() {
    //  close popup

    this.setState({
      isShowAlert: false
    });
  }

  gtmEventHandler(attrArr) {
    //category ,//label, //event
    gblFunc.gaTrack.trackEvent(attrArr);
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "name":
        validationObject.name = "* Name";
        validationObject.validationFunctions = [
          required,
          alphabetsOnly,
          minLength(1)
        ];
        return validationObject;
      case "email":
        validationObject.name = "* Email";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;
      case "mobile":
        validationObject.name = "* Mobile number ";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;
      case "downldName":
        validationObject.name = "* Name";
        validationObject.validationFunctions = [
          required,
          alphabetsOnly,
          minLength(3)
        ];
        return validationObject;
      case "downldEmail":
        validationObject.name = "* Email";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;
      case "downldMobile":
        validationObject.name = "* Mobile number ";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;
      case "downldProgram":
        validationObject.name = "* Program";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "downldProOthers":
        validationObject.name = "* Other";
        validationObject.validationFunctions = [
          required,
          alphabetsOnly,
          minLength(3)
        ];
        return validationObject;
      case "downldCountry":
        validationObject.name = "* Country";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "downldConOthers":
        validationObject.name = "* Other";
        validationObject.validationFunctions = [
          required,
          alphabetsOnly,
          minLength(3)
        ];
        return validationObject;
      case "downldExam":
        validationObject.name = "* Exam";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "country":
        validationObject.name = "* Country";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "exams":
        validationObject.name = "* Exam";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "programs":
        validationObject.name = "* Program";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "proOthers":
        validationObject.name = "* Other";
        validationObject.validationFunctions = [
          required,
          alphabetsOnly,
          minLength(3)
        ];
        return validationObject;
      case "conOthers":
        validationObject.name = "* Other";
        validationObject.validationFunctions = [
          required,
          alphabetsOnly,
          minLength(3)
        ];
        return validationObject;
    }
  }

  checkFormValidations(type) {
    const { programs, country, downldProgram, downldCountry } = this.state;
    let validations = null;
    if (type === "counselling") {
      validations = { ...this.state.validations };
    } else {
      validations = { ...this.state.downloadValidation };
    }
    if (programs !== "Others") {
      delete validations.proOthers;
    } else {
      validations.proOthers = null;
    }
    if (downldProgram !== "Others") {
      delete validations.downldProOthers;
    } else {
      validations.downldProOthers = null;
    }
    if (country !== "Others") {
      delete validations.conOthers;
    } else {
      validations.conOthers = null;
    }
    if (downldCountry !== "Others") {
      delete validations.downldConOthers;
    } else {
      validations.downldConOthers = null;
    }
    let isFormValid = true;

    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }

    if (type == "counselling") {
      this.setState({
        validations,
        isFormValid
      });
    } else {
      this.setState({
        downloadValidation: validations,
        isFormValid
      });
    }
    return isFormValid;
  }

  submitEducation(event, activities) {
    event.preventDefault();
    const {
      country,
      email,
      mobile,
      name,
      programs,
      exams,
      proOthers,
      conOthers,
      source,
      activity,
      utmMedium,
      utmSource
    } = this.state;
    if (this.checkFormValidations("counselling")) {
      let apiParams = {
        country: country === "Others" ? conOthers : country,
        email,
        mobile,
        name,
        program: programs === "Others" ? proOthers : programs,
        exam: exams,
        source,
        activity: "counselling",
        utmMedium,
        utmSource
      };
      //Don't add key if not present in utm source
      // if (utmSource) {
      //   apiParams.utmSource = utmSource;
      // }
      this.props.internationStudyLoad(apiParams);
      // this.setState({
      //   activity: "counselling"
      // });
    }
  }

  onSubmitPDFHandler(event, ebookTitle) {
    event.preventDefault();
    const {
      downldEmail,
      downldName,
      downldMobile,
      downldCountry,
      downldConOthers,
      downldProgram,
      downldProOthers,
      downldExam,
      source,
      ebookId,
      ebookUrl,
      utmMedium,
      utmSource
    } = this.state;

    if (this.checkFormValidations("download")) {
      let apiParams = {
        activity: "download",
        email: downldEmail,
        country: downldCountry === "Others" ? downldConOthers : downldCountry,
        mobile: downldMobile,
        name: downldName,
        program: downldProgram === "Others" ? downldProOthers : downldProgram,
        exam: downldExam,
        source,
        ebookId,
        utmMedium,
        utmSource
      };
      this.props.internationStudyLoad(apiParams);

      if (typeof window !== "undefined") {
        let setBookPDF = sessionStorage.getItem("ebookDownload")
          ? JSON.parse(sessionStorage.getItem("ebookDownload"))
          : [];
        setBookPDF.push({ title: ebookTitle, url: { ...ebookUrl } });
        sessionStorage.setItem("ebookDownload", JSON.stringify(setBookPDF));
      }
    }
  }

  onFormFieldChange(event) {
    const { id, value } = event.target;
    if (id === "programs" && value !== "Others")
      this.setState({ proOthers: "" });
    if (id === "country" && value !== "Others")
      this.setState({ conOthers: "" });
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );
    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];
    this.setState({
      [id]: value,
      validations
    });
  }

  onFormPdfFieldChange(event) {
    const { id, value } = event.target;
    if (id === "downldProgram" && value !== "Others")
      this.setState({ downldProOthers: "" });
    if (id === "downldCountry" && value !== "Others")
      this.setState({ downldConOthers: "" });
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );
    let validations = { ...this.state.downloadValidation };
    validations[id] = validationResult[id];
    this.setState({
      [id]: value,
      downloadValidation: validations
    });
  }

  componentDidMount() {
    this.props.getInternationStudyLoad();
    this.props.getEbooks();
    gblFunc.scrollPage(this.refs.downloadPDF);

    const paramsMedium = new URLSearchParams(location.search);
    const utm = paramsMedium.get("utm_medium");
    this.setState({ utmMedium: utm });
    const paramsSource = new URLSearchParams(location.search);
    const uts = paramsSource.get("utm_source");
    this.setState({ utmSource: uts });
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    const registrationAlertMsg =
      "Thank you for registering with us! Our expert counsellor will get in touch with you to guide on 'International Scholarships' & admission processes to top ranked universities/courses abroad. For any query, write to us at <a href='mailto:info@buddy4study.com'>info@buddy4study.com</a>";
    const downloadAlertMsg =
      "Thank you for downloading this e-book! Our expert counsellor will get in touch with you to guide on 'International Scholarships' & admission processes to top ranked universities/courses abroad. For any query, write to us at <a href='mailto:info@buddy4study.com'>info@buddy4study.com</a>";
    switch (type) {
      case SUBMIT_INTERNATIONAL_STUDIES_SUCCEEDED:
        this.setState(
          {
            isShowAlert: true,
            statusAlert: 200,
            alertMsg:
              this.state.activity === "download"
                ? downloadAlertMsg
                : registrationAlertMsg,
            name: "",
            mobile: "",
            country: "",
            email: "",
            programs: "",
            proOthers: "",
            conOthers: "",
            exams: "",
            downldName: "",
            downldEmail: "",
            downldMobile: "",
            downldCountry: "",
            downldConOthers: "",
            downldProOthers: "",
            downldProgram: "",
            downldExam: "",
            downldActivity: "",
            showModal: false,
            ebookTitle: ""
          },
          () => {
            if (
              typeof window !== "undefined" &&
              this.state.activity === "download"
            ) {
              window.open(this.state.ebookUrl[this.state.ebookFor], "_blank");
            }
          }
        );
        //For bifurcating event action for pdf download and formSubmission
        const attrArr =
          this.state.activity === "download"
            ? ["Lead", "international", "PDF Downloaded"]
            : ["Lead", "international", "Lead Submitted"];
        this.gtmEventHandler(attrArr); // calling.... gtm...
        break;
      case SUBMIT_INTERNATIONAL_STUDIES_FAILED:
        sessionStorage.removeItem("ebookDownload");
        this.setState({
          isShowAlert: true,
          statusAlert: "",
          alertMsg: "Opps! Something went wrong, please try again!",
          name: "",
          mobile: "",
          country: "",
          email: "",
          showModal: false,
          ebookTitle: ""
        });

        break;

      default:
        break;
    }
  }
  markup(val) {
    return { __html: val };
  }

  onChangeEbook(country, ebookTitle, url, id) {
    if (typeof window !== "undefined") {
      let showModal = true;
      let isDownloaded = JSON.parse(sessionStorage.getItem("ebookDownload"));
      if (isDownloaded && isDownloaded.length > 0) {
        isDownloaded.map(downLink => {
          if (downLink.title == ebookTitle && window) {
            window.open(downLink.url[country], "_blank");
            showModal = false;
            return;
          }
        });
      }
      this.setState({
        showModal: showModal,
        country,
        activity: "download",
        ebookFor: country,
        ebookTitle,
        ebookUrl: { [country]: url },
        ebookId: id
      });
    }
  }
  onCloseEbook() {
    this.setState({
      showModal: false
    });
  }

  render() {
    const { internationalStudy, ebooks } = this.props;
    let settings = {
      dots: false,
      className: "center",
      infinite: true,
      autoplay: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      cssEase: "linear",
      speed: 500,
      pauseOnHover: false,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            dots: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true
          }
        },
        {
          breakpoint: 853,
          settings: {
            dots: true,
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 603,
          settings: {
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };

    return (
      <section className="abroadStudy">
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          isShow={this.state.isShowAlert}
          msg={this.state.alertMsg}
          close={this.hideAlert}
          status={this.state.statusAlert}
          internationalStudyPopup={this.state.internationalStudyPopup}
        />
        <section>
          <section className="container-fluid eduTopBg ">
            <section className="container containerWidth positionRelative containerHeight">
              <Link to="/">
                <img
                  src={`${imgBaseUrl}white-logo.png`}
                  alt="buddy4study-logo"
                  className="eduLogo"
                />
              </Link>
              <article className="graphicsObj">
                <p>
                  Study in UK, US, Canada, Australia, Ireland and other
                  countries
                </p>
                {/*  <a
                  href="https://buddy4study.com/application/CAL2/instruction"
                  target="_blank"
                  className="but"
                >
                  Apply for international scholarships
                </a> */}

                <a
                  href="https://www.buddy4study.com/scholarship-conclave?utm_source=IntPageTab&utm_medium=Web"
                  target="_blank"
                  className="but singleBtn"
                >
                  International Scholarship Exam and Fair
                </a>
              </article>
              <section className="eduFormWrapper">
                <form
                  name="loanEduForm"
                  onSubmit={event => this.submitEducation(event, "counselling")}
                >
                  <p>Register for Free Counselling Session</p>

                  <article className="row positionRelative">
                    <input
                      type="text"
                      name="fullName"
                      placeholder="Name"
                      id="name"
                      onChange={this.onFormFieldChange}
                      value={this.state.name}
                    />
                    {this.state.validations.name ? (
                      <i
                        classname="error errorred"
                        style={{
                          backgroundColor: "#fff",
                          color: "#e5260e",
                          padding: "2px 5px"
                        }}
                      >
                        {this.state.validations.name}
                      </i>
                    ) : null}
                  </article>
                  <article className="row positionRelative">
                    <input
                      type="email"
                      name="email"
                      placeholder="Email ID"
                      id="email"
                      onChange={this.onFormFieldChange}
                      value={this.state.email}
                    />
                    {this.state.validations.email ? (
                      <i className="error">{this.state.validations.email}</i>
                    ) : null}
                  </article>
                  <article className="row positionRelative">
                    <input
                      type="text"
                      name="mobile"
                      placeholder="Contact Number"
                      id="mobile"
                      maxLength="10"
                      onChange={this.onFormFieldChange}
                      value={this.state.mobile}
                    />
                    {this.state.validations.mobile ? (
                      <i className="error">{this.state.validations.mobile}</i>
                    ) : null}
                  </article>

                  <article className="row positionRelative">
                    <article>
                      <label> Looking for programs</label>
                      <dd
                        className={`select-container ${
                          this.state.programs === "Others" ? "width50" : ""
                          }`}
                      >
                        <span
                          className={`select-arrow ${
                            this.state.programs === "Others" ? "top11" : ""
                            }`}
                        />
                        <select
                          name="programs"
                          id="programs"
                          onChange={this.onFormFieldChange}
                          value={this.state.programs}
                        >
                          <option value="" defaultValue="selected">
                            --Select--
                          </option>
                          <option value="Graduation">Graduation</option>
                          <option value="Post Graduation">
                            Post Graduation
                          </option>
                          <option value="PhD">PhD</option>
                          <option value="Others">Others</option>
                        </select>
                        {this.state.validations.programs ? (
                          <i className="error selecterror">
                            {this.state.validations.programs}
                          </i>
                        ) : null}
                      </dd>
                      {this.state.programs === "Others" ? (
                        <dd className="width50">
                          <input
                            type="text"
                            name="proOthers"
                            placeholder="Other"
                            id="proOthers"
                            className="inputother"
                            onChange={this.onFormFieldChange}
                            value={this.state.proOthers}
                          />
                          {this.state.validations.proOthers ? (
                            <i className="error selecterror others">
                              {this.state.validations.proOthers}
                            </i>
                          ) : null}
                        </dd>
                      ) : (
                          ""
                        )}
                    </article>
                  </article>

                  <article className="row positionRelative">
                    <article>
                      <label> Looking for universities in</label>
                      <dd
                        className={`select-container ${
                          this.state.country === "Others" ? "width50" : ""
                          }`}
                      >
                        <span
                          className={`select-arrow ${
                            this.state.country === "Others" ? "top11" : ""
                            }`}
                        />
                        <select
                          name="country"
                          id="country"
                          onChange={this.onFormFieldChange}
                          value={this.state.country}
                        >
                          <option value="" defaultValue="selected">
                            --Select--
                          </option>
                          <option value="UK">UK</option>
                          <option value="Australia">Australia</option>
                          <option value="US">US</option>
                          <option value="Canada">Canada</option>
                          <option value="New Zealand">New Zealand</option>
                          <option value="Ireland">Ireland</option>
                          <option value="Israel">Israel</option>
                          <option value="Others">Others</option>
                        </select>
                        {this.state.validations.country ? (
                          <i className="error selecterror">
                            {this.state.validations.country}
                          </i>
                        ) : null}
                      </dd>
                      {this.state.country === "Others" ? (
                        <dd className="width50">
                          <input
                            type="text"
                            name="conOthers"
                            placeholder="Other"
                            id="conOthers"
                            className="inputother"
                            onChange={this.onFormFieldChange}
                            value={this.state.conOthers}
                          />
                          {this.state.validations.conOthers ? (
                            <i className="error selecterror others">
                              {this.state.validations.conOthers}
                            </i>
                          ) : null}
                        </dd>
                      ) : (
                          ""
                        )}
                    </article>
                  </article>
                  <article className="row">
                    <article>
                      <label> Have you attempted any of these exams</label>
                      <dd className="select-container">
                        <span className="select-arrow" />
                        <select
                          name="familyIncome"
                          id="exams"
                          onChange={this.onFormFieldChange}
                          value={this.state.exams}
                        >
                          <option value="" defaultValue="selected">
                            --Select--
                          </option>
                          <option value="IELTS">IELTS</option>
                          <option value="TOEFL">TOEFL</option>
                          <option value="GMAT">GMAT</option>
                          <option value="GRE">GRE</option>
                          <option value="SAT">SAT</option>
                          <option value="NONE">NONE</option>
                        </select>
                        {this.state.validations.exams ? (
                          <i className="error selecterror">
                            {this.state.validations.exams}
                          </i>
                        ) : null}
                      </dd>
                    </article>
                  </article>
                  <article className="row">
                    <article>
                      <label>
                        By clicking on this submit button, you are agreeing to
                        our{" "}
                        <Link to="/terms-and-conditions">
                          Terms and Conditions
                        </Link>
                      </label>
                    </article>
                  </article>

                  <button type="submit">Submit</button>
                </form>
              </section>
            </section>
          </section>

          <section className="container-fluid paddingLR greybg">
            <section className="container containerWidth paddingLR">
              <section className="row">
                <h1 className="titleText">Study Abroad with us</h1>
                <section className="row eduyoutube">
                  <section className="col-md-7 paddingL">
                    <p>
                      Do you dream of a career out of India? Are you looking for
                      the right opportunities to study abroad in popular
                      destinations like US, UK, Canada, Australia, Ireland? We
                      have partnered with leading international universities
                      across the globe who are offering scholarships for Indian
                      students. These study abroad scholarships will help you
                      secure admission in your preferred college.
                    </p>
                    <p>
                      We also help Indian students to find external scholarships
                      and education loans to fund their education abroad. This
                      along with end-to-end scholarship application support,
                      counseling, and guidance to choose, apply, and get
                      admission in our partner colleges.
                    </p>
                  </section>
                  <section className="col-md-5 paddingL paddingR">
                    <article className="edustep">
                      <ul>
                        <li>
                          International scholarships by top ranked universities
                        </li>
                        <li>Application guidance</li>
                        <li>
                          Assistance in applying for external scholarships
                          <img
                            src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/logo1.png"
                            className="img-responsive"
                          />
                          {/* (e.g.
                          JN Tata Endowment for Higher Education of Indians, KC
                          Mahindra Scholarships for Post-Graduate Studies
                          Abroad, Narotam Sekhsaria Scholarship Programme and
                          more) */}
                        </li>
                        <li>
                          Access to education loans
                          <img
                            src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/payment.png"
                            className="img-responsive"
                          />
                        </li>
                        <li>Free international scholarship alerts</li>
                        <li>Query handling support</li>
                      </ul>
                    </article>
                  </section>
                </section>
              </section>
            </section>
          </section>

          <section className="container-fluid paddingLR paddingbottom30">
            <section className="container containerWidth paddingLR">
              <section className="row">
                <h2 className="titleText ">
                  Common Admission, Loan and Scholarship Application (CALSA)
                </h2>
                <article className="redefining-student-friendly-box">
                  <article className="col-md-6 col-sm-12 col-xs-12">
                    <p>
                      One of the most common problems faced by students
                      interested in studying abroad is the burden of applying
                      for multiple scholarships. There are hundreds of forms to
                      be filled and you often miss out on the ones that are
                      important. To address this problem, Buddy4Study has come
                      up with an innovative product - Common Admission, Loan and
                      Scholarship Application (CALSA). CALSA is a common
                      application form valid for application to multiple
                      universities in one go. Buddy4Study partnered
                      International universities offer many scholarships to
                      Indian students. To participate and avail these university
                      scholarships, all you need to do is fill out the CALSA
                      form.
                      <br />
                      <br />
                      <a
                        href="https://buddy4study.com/application/CAL2/instruction"
                        target="blank_"
                        className="btn margin0"
                      >
                        Apply Now
                      </a>
                    </p>
                  </article>
                  <article className="col-md-6 col-sm-12 col-xs-12">
                    <h2>One Application form for multiple benefits</h2>

                    <img
                      src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/graph.png"
                      className="img-responsive"
                    />
                    {/* <ul>
                    <li>
                      <dd>
                        Access to partner university scholarships and
                        application assistance
                      </dd>
                    </li>
                    <li>
                      <dd>Access to international scholarships</dd>
                    </li>
                    <li>
                      <dd>Access to education loan</dd>
                    </li>
                    <li>
                      <dd>
                        Free international scholarship alerts via SMS/Email
                      </dd>
                    </li>
                  </ul> */}
                  </article>
                </article>
              </section>
            </section>
          </section>
          <section className="container-fluid paddingLR padding30">
            <section className="container containerWidth paddingLR">
              <section className="row">
                <h1 className="titleText">
                  Partner International Universities
                </h1>

                <section className="col-md-12 paddingL topuniversity">
                  <Slider {...settings}>
                    {internationalStudy && internationalStudy.length > 0
                      ? internationalStudy.map((list, index) => (
                        <article key={index}>
                          <img
                            src={list.logoUrl}
                            className="img-responsive"
                          />
                        </article>
                      ))
                      : null}
                  </Slider>
                </section>
              </section>
            </section>
          </section>

          <section className="container-fluid">
            <section className="container containerWidth paddingLR">
              <section className="row">
                <h2 className="titleText">University Scholarships</h2>
                <article className="table-responsive">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>University Name </th>
                        <th>Country Name</th>
                        <th>Deadline</th>
                        <th>Apply Now</th>
                      </tr>
                    </thead>
                    <tbody>
                      {internationalStudy && internationalStudy.length > 0
                        ? internationalStudy.map((list, index) => (
                          <tr key={index}>
                            <td>
                              {" "}
                              <a
                                href={`https://www.buddy4study.com/scholarship/${
                                  list.slug
                                  }`}
                                target="_blank"
                              >
                                {list.universityName}
                              </a>
                            </td>
                            <td>{list.countryName}</td>
                            <td>{list.deadline}</td>
                            <td>
                              <Link
                                to={`${list.applyNow}`}
                                target="blank_"
                                className="btn margin0"
                              >
                                Apply Now
                                </Link>
                            </td>
                          </tr>
                        ))
                        : ""}
                    </tbody>
                  </table>
                </article>
              </section>
            </section>
          </section>
          <section className="container-fluid paddingLR  eduTopBorder eduBottomBorder paddingbottom30">
            <section className="container containerWidth paddingLR ">
              <section className="row">
                <section className="col-md-8 speech">
                  <h1 className="titleheading" ref="downloadPDF">
                    Testimonials
                  </h1>
                  <Testimonials />
                </section>
                <section className="col-md-4 col-sm-12">
                  <article className="ebook">
                    <h1 className="titleheading">Download Ebooks </h1>

                    <ul>
                      {/* <li>
                        <a
                          onClick={() =>
                            this.onChangeEbook(
                              "us",
                              "Admission Guidelines for US universities",
                              "https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/SAS/US-E-Book.pdf"
                            )
                          }
                        >
                          Admission Guidelines for US universities
                        </a>
                      </li> */}

                      {ebooks && ebooks.length
                        ? ebooks.map(ebook => (
                          <li key={ebook.id}>
                            <a
                              onClick={() =>
                                this.onChangeEbook(
                                  ebook.university,
                                  ebook.title,
                                  ebook.url,
                                  ebook.id
                                )
                              }
                            >
                              {ebook.title}
                            </a>
                          </li>
                        ))
                        : null}

                      {/* <li>
                        <a
                          onClick={() =>
                            this.onChangeEbook(
                              "uk",
                              "Admission Guidelines for UK universities",
                              "https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/SAS/UK-E-Book.pdf"
                            )
                          }
                        >
                          Study in UK: All you need to know
                        </a>
                      </li> */}
                    </ul>
                  </article>
                </section>
              </section>
            </section>
          </section>
          <section className="bgcolor">
            <article className="container ">
              <article className="col-md-12 text-center">
                <h1 className="titleheading whitecolor">Contact Us</h1>
                <p>
                  For queries to study abroad, write to us at&nbsp;&nbsp;
                  <a href="mailto:institute@buddy4study.com">
                    institute@buddy4study.com
                  </a>
                </p>
              </article>
            </article>
          </section>
        </section>
        {this.state.showModal ? (
          <EbookModal
            onCloseEbook={this.onCloseEbook}
            validations={this.state.downloadValidation}
            downldName={this.state.downldName}
            downldMobile={this.state.downldMobile}
            downldEmail={this.state.downldEmail}
            onFormPdfFieldChange={this.onFormPdfFieldChange}
            onSubmitPDFHandler={this.onSubmitPDFHandler}
            ebookTitle={this.state.ebookTitle}
            downldCountry={this.state.downldCountry}
            downldProgram={this.state.downldProgram}
            downldExam={this.state.downldExam}
            downldProOthers={this.state.downldProOthers}
            downldConOthers={this.state.downldConOthers}
          />
        ) : null}
      </section>
    );
  }
}

const EbookModal = ({
  onCloseEbook,
  validations,
  onFormPdfFieldChange,
  downldName,
  downldEmail,
  downldMobile,
  onSubmitPDFHandler,
  ebookTitle,
  downldCountry,
  downldConOthers,
  downldProgram,
  downldProOthers,
  downldExam
}) => {
  return (
    <article className="popup">
      <article className="popupsms">
        <article className="popup_inner loginpopup">
          <article className="LoginRegPopup">
            <article className="modal fade modelAuthPopup1">
              <article className="modal-dialog">
                <article
                  className={
                    downldProgram === "Others"
                      ? "modal-content modelBg emailverification internal othersBoxHeight"
                      : "modal-content modelBg emailverification internal"
                  }
                >
                  <article className="modal-header">
                    <h3 className="modal-title">
                      Fill the form to Download Ebook
                    </h3>
                    <button
                      type="button"
                      className="close btnPos"
                      onClick={onCloseEbook}
                    >
                      <i>&times;</i>
                    </button>
                  </article>
                  <article className="modal-body forgot">
                    <article className="row">
                      <form
                        name="forgotForm"
                        onSubmit={event =>
                          onSubmitPDFHandler(event, ebookTitle)
                        }
                        className="formelemt"
                      >
                        <article className="row">
                          <section className="col-md-12">
                            <section className="ctrl-wrapper">
                              <article className="form-group">
                                <input
                                  type="text"
                                  name="name"
                                  className="form-control"
                                  placeholder="Full Name"
                                  id="downldName"
                                  value={downldName}
                                  onChange={event =>
                                    onFormPdfFieldChange(event)
                                  }
                                />
                                {validations.downldName ? (
                                  <span className="error1">
                                    {validations.downldName}
                                  </span>
                                ) : null}
                              </article>
                            </section>
                            <section className="ctrl-wrapper">
                              <article className="form-group">
                                <input
                                  type="email"
                                  name="contactno"
                                  className="form-control"
                                  placeholder="Email"
                                  id="downldEmail"
                                  onChange={event =>
                                    onFormPdfFieldChange(event)
                                  }
                                  value={downldEmail}
                                />
                                {validations.downldEmail ? (
                                  <span className="error1">
                                    {validations.downldEmail}
                                  </span>
                                ) : null}
                              </article>
                            </section>
                            <section className="ctrl-wrapper">
                              <article className="form-group">
                                <input
                                  type="text"
                                  name="mobile"
                                  className="form-control"
                                  placeholder="Contact Number"
                                  id="downldMobile"
                                  maxLength="10"
                                  onChange={event =>
                                    onFormPdfFieldChange(event)
                                  }
                                  value={downldMobile}
                                />
                                {validations.downldMobile ? (
                                  <span className="error1">
                                    {validations.downldMobile}
                                  </span>
                                ) : null}
                              </article>
                            </section>

                            <section className="ctrl-wrapper">
                              <article className="form-group">
                                <dd
                                  className={`select-container ${
                                    downldProgram === "Others"
                                      ? "width50"
                                      : ""
                                    }`}
                                >
                                  <span
                                    className={`select-arrow ${
                                      downldProgram === "Others"
                                        ? "top11"
                                        : ""
                                      }`}
                                  />
                                  <select
                                    name="downldProgram"
                                    className="form-control"
                                    id="downldProgram"
                                    onChange={event =>
                                      onFormPdfFieldChange(event)
                                    }
                                    value={downldProgram}
                                  >
                                    <option value="" defaultValue="selected">
                                      Select program
                                      </option>
                                    <option value="Graduation">
                                      Graduation
                                      </option>
                                    <option value="Post Graduation">
                                      Post Graduation
                                      </option>
                                    <option value="PhD">PhD</option>
                                    <option value="Others">Others</option>
                                  </select>
                                  {validations.downldProgram ? (
                                    <i className="error1 selecterror">
                                      {validations.downldProgram}
                                    </i>
                                  ) : null}
                                </dd>
                              </article>
                              {downldProgram === "Others" ? (
                                <article className="form-group">
                                  <dd className="width50">
                                    <input
                                      type="text"
                                      name="downldProOthers"
                                      placeholder="Enter the other program name"
                                      id="downldProOthers"
                                      className="form-control"
                                      onChange={event =>
                                        onFormPdfFieldChange(event)
                                      }
                                      value={downldProOthers}
                                    />
                                    {validations.downldProOthers ? (
                                      <i className="error1 selecterror others">
                                        {validations.downldProOthers}
                                      </i>
                                    ) : null}
                                  </dd>
                                </article>
                              ) : (
                                  ""
                                )}
                            </section>
                            <section className="ctrl-wrapper">
                              <article className="form-group">
                                <dd
                                  className={`select-container ${
                                    downldCountry === "Others"
                                      ? "width50"
                                      : ""
                                    }`}
                                >
                                  <span
                                    className={`select-arrow ${
                                      downldCountry === "Others"
                                        ? "top11"
                                        : ""
                                      }`}
                                  />
                                  <select
                                    name="downldCountry"
                                    className="form-control"
                                    id="downldCountry"
                                    onChange={event =>
                                      onFormPdfFieldChange(event)
                                    }
                                    value={downldCountry}
                                  >
                                    <option value="" defaultValue="selected">
                                      Select university in
                                      </option>
                                    <option value="UK">UK</option>
                                    <option value="Australia">
                                      Australia
                                      </option>
                                    <option value="US">US</option>
                                    <option value="Canada">Canada</option>
                                    <option value="New Zealand">
                                      New Zealand
                                      </option>
                                    <option value="Ireland">Ireland</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Others">Others</option>
                                  </select>
                                  {validations.downldCountry ? (
                                    <i className="error1 selecterror">
                                      {validations.downldCountry}
                                    </i>
                                  ) : null}
                                </dd>
                              </article>
                              {downldCountry === "Others" ? (
                                <article className="form-group">
                                  <dd className="width50">
                                    <input
                                      type="text"
                                      name="downldConOthers"
                                      placeholder="Enter the other university name"
                                      id="downldConOthers"
                                      className="form-control"
                                      onChange={event =>
                                        onFormPdfFieldChange(event)
                                      }
                                      value={downldConOthers}
                                    />
                                    {validations.downldConOthers ? (
                                      <i className="error1 selecterror others">
                                        {validations.downldConOthers}
                                      </i>
                                    ) : null}
                                  </dd>
                                </article>
                              ) : (
                                  ""
                                )}
                            </section>
                            <section className="ctrl-wrapper">
                              <article className="form-group">
                                <dd className="select-container">
                                  <span className="select-arrow" />
                                  <select
                                    name="familyIncome"
                                    className="form-control"
                                    id="downldExam"
                                    onChange={event =>
                                      onFormPdfFieldChange(event)
                                    }
                                    value={downldExam}
                                  >
                                    <option value="" defaultValue="selected">
                                      Have you attempted any of these exams
                                      </option>
                                    <option value="IELTS">IELTS</option>
                                    <option value="TOEFL">TOEFL</option>
                                    <option value="GMAT">GMAT</option>
                                    <option value="GRE">GRE</option>
                                    <option value="SAT">SAT</option>
                                    <option value="NONE">NONE</option>
                                  </select>
                                  {validations.downldExam ? (
                                    <i className="error1 selecterror">
                                      {validations.downldExam}
                                    </i>
                                  ) : null}
                                </dd>
                              </article>
                            </section>
                          </section>
                        </article>
                        <section className="row">
                          <button
                            type="submit"
                            className="btn-block greenBtn recover"
                          >
                            Download Now
                            </button>
                        </section>
                      </form>
                    </article>
                  </article>
                </article>
              </article>
            </article>
          </article>
        </article>
      </article>
    </article>
  );
};

export default studyAbroadPage;
