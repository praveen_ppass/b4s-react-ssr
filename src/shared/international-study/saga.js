import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  SUBMIT_INTERNATIONAL_STUDIES_REQUESTED,
  SUBMIT_INTERNATIONAL_STUDIES_SUCCEEDED,
  SUBMIT_INTERNATIONAL_STUDIES_FAILED,
  FETCH_INTERNATION_STUDY_LIST_REQUESTED,
  FETCH_INTERNATION_STUDY_LIST_FAILED,
  FETCH_INTERNATION_STUDY_LIST_SUCCEEDED,
  FETCH_INTERNATION_EBOOK_REQUESTED,
  FETCH_INTERNATION_EBOOK_SUCCEEDED,
  FETCH_INTERNATION_EBOOK_FAILED
} from "./actions";

const submitInterantionalStudyApi = inputData => {
  return fetchClient.post(apiUrl.internationalStudies, inputData).then(res => {
    return res.data;
  });
};

function* submitInterantionalStudy(input) {
  try {
    const response = yield call(
      submitInterantionalStudyApi,
      input.payload.inputData
    );

    yield put({
      type: SUBMIT_INTERNATIONAL_STUDIES_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    yield put({
      type: SUBMIT_INTERNATIONAL_STUDIES_FAILED,
      payload: error
    });
  }
}

const fetchInterantionalStudyApi = inputData => {
  return fetchClient
    .get(`${apiUrl.getInternationalStudy}/international-study`)
    .then(res => {
      return res.data;
    });
};

function* fetchInterantionalStudy(input) {
  try {
    const fetchInternationStudy = yield call(
      fetchInterantionalStudyApi,
      input.payload.inputData
    );

    yield put({
      type: FETCH_INTERNATION_STUDY_LIST_SUCCEEDED,
      payload: fetchInternationStudy
    });
  } catch (error) {
    yield put({
      type: FETCH_INTERNATION_STUDY_LIST_FAILED,
      payload: error
    });
  }
}

const fetchEbookRequestApi = inputData => {
  let url = apiUrl.getEbooks;
  if (inputData && inputData.country) {
    url = `${url}?country=${inputData.country}`;
  }
  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchEbookRequest(input) {
  try {
    const fetchInternationStudy = yield call(
      fetchEbookRequestApi,
      input.payload.inputData
    );

    yield put({
      type: FETCH_INTERNATION_EBOOK_SUCCEEDED,
      payload: fetchInternationStudy
    });
  } catch (error) {
    yield put({
      type: FETCH_INTERNATION_EBOOK_FAILED,
      payload: error
    });
  }
}

export default function* internationStudySaga() {
  yield takeEvery(
    SUBMIT_INTERNATIONAL_STUDIES_REQUESTED,
    submitInterantionalStudy
  );
  yield takeEvery(
    FETCH_INTERNATION_STUDY_LIST_REQUESTED,
    fetchInterantionalStudy
  );
  yield takeEvery(FETCH_INTERNATION_EBOOK_REQUESTED, fetchEbookRequest);
}
