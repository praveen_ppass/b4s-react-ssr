import {
  SUBMIT_CONTACT_US_SUCCEEDED,
  SUBMIT_CONTACT_US_REQUESTED,
  SUBMIT_CONTACT_US_FAILED
} from "./actions";

const initialState = {
  showLoader: false,
  isError: false,
  errorMessage: ""
};

const contactUsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SUBMIT_CONTACT_US_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type,
        errorMessage: ""
      };

    case SUBMIT_CONTACT_US_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type
      };

    case SUBMIT_CONTACT_US_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        type,
        errorMessage: payload
      };
    default:
      return state;
  }
};
export default contactUsReducer;
