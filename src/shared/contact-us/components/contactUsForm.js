import React, { Component } from "react";
import BreadCrum from "../../components/bread-crum/breadCrum";
import { ruleRunner } from "../../../validation/ruleRunner";
import AlertMessage from "../../common/components/alertMsg";
import {
  required,
  isEmail,
  minLength,
  isNumeric,
  isMobileNumber,
  lengthRange
} from "../../../validation/rules";
import { breadCrumObj } from "../../../constants/breadCrum";
import { SUBMIT_CONTACT_US_SUCCEEDED } from "../actions";
import Loader from "../../common/components/loader";
import { messages } from "../../../constants/constants";

class ContactUsForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: "",
      email: "",
      mobile: "",
      name: "",
      subject: "",
      isShowAlert: false,
      validations: {
        email: null,
        mobile: null,
        name: null,
        subject: null
      },
      isFormValid: false
    };
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
  }

  /*

  {

  "comment": "string",
  "email": "string",
  "mobile": 0,
  "name": "string",
  "subject": "string"
  }

  */

  hideAlert() {
    this.setState({
      showAlertMsg: "",
      isShowAlert: false,
      name: "",
      email: "",
      mobile: "",
      subject: ""
    });
  }

  onFormFieldChange(event) {
    const { id, value } = event.target;
    let validations = { ...this.state.validations };
    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      const validationResult = ruleRunner(
        value,
        id,
        name,
        ...validationFunctions
      );
      validations[id] = validationResult[id];
    }
    this.setState({
      [id]: value,
      validations
    });
  }

  onSubmitHandler(event) {
    event.preventDefault();
    const { comment, email, mobile, name, subject } = this.state;
    if (this.checkFormValidations()) {
      this.props.submitContactUs({
        comment,
        email,
        mobile,
        name,
        subject,
        type: "contactUS"
      });
    }
  }
  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "name":
        validationObject.name = "* Name";
        validationObject.validationFunctions = [required, lengthRange(3, 60)];
        return validationObject;
      case "email":
        validationObject.name = "* Email ";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;
      case "mobile":
        validationObject.name = "* Mobile number ";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;
      case "subject":
        validationObject.name = "* Subject ";
        validationObject.validationFunctions = [required, lengthRange(6, 60)];
        return validationObject;
    }
  }
  /************ end - validation part - get message and required validation******************* */

  /************ start - validation part - check form submit validation******************* */
  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      if (validations.hasOwnProperty(key)) {
        let { name, validationFunctions } = this.getValidationRulesObject(key);
        let validationResult = ruleRunner(
          this.state[key],
          key,
          name,
          ...validationFunctions
        );
        validations[key] = validationResult[key];
        if (validationResult[key] !== null) {
          isFormValid = false;
        }
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  /************ start - validation part - check form submit validation******************* */
  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    switch (type) {
      case SUBMIT_CONTACT_US_SUCCEEDED:
        this.setState({
          isShowAlert: true
        });
        break;

      default:
        break;
    }
  }

  render() {
    return (
      <section className="contact">
        <AlertMessage
          isShow={this.state.isShowAlert}
          msg={messages.generic.success}
          status={true}
          close={this.hideAlert}
        />

        <Loader isLoader={this.props.showLoader} />
        <BreadCrum
          classes={breadCrumObj["contact_us"].bgImage}
          listOfBreadCrum={breadCrumObj["contact_us"].breadCrum}
          subTitle={breadCrumObj["contact_us"].subTitle}
          title={breadCrumObj["contact_us"].title}
        />
        {/* form section */}

        <section className="bgcontact">
          <h2>Reach out to Buddy4Study</h2>
          <section className="container">
            <article className="row">
              <article className="col-md-6 vdivider">
                <h4>SUBMIT YOUR QUERY</h4>
                <form autoCapitalize="off">
                  <article className="form-group">
                    <label htmlFor="number">Your Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="name"
                      name="name"
                      placeholder="Enter your full Name"
                      value={this.state.name}
                      onChange={this.onFormFieldChange}
                      required
                    />
                    {this.state.validations.name ? (
                      <span className="error animated bounce">
                        {this.state.validations.name}
                      </span>
                    ) : null}
                  </article>
                  <article className="form-group">
                    <label htmlFor="email">Email</label>
                    <input
                      type="text"
                      id="email"
                      name="email"
                      className="form-control"
                      placeholder="abc@xyz.com"
                      value={this.state.email}
                      onChange={this.onFormFieldChange}
                      required=""
                    />
                    {this.state.validations.email ? (
                      <span className="error animated bounce">
                        {this.state.validations.email}
                      </span>
                    ) : null}
                  </article>
                  <article className="form-group">
                    <label htmlFor="number">Your Number</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Enter your mobile number"
                      id="mobile"
                      name="mobile"
                      onChange={this.onFormFieldChange}
                      value={this.state.mobile}
                      maxLength={10}
                      required=""
                    />
                    {this.state.validations.mobile ? (
                      <span className="error animated bounce">
                        {this.state.validations.mobile}
                      </span>
                    ) : null}
                  </article>
                  <article className="form-group">
                    <label htmlFor="subject">Subject</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="What is your concern?"
                      id="subject"
                      name="subject"
                      onChange={this.onFormFieldChange}
                      value={this.state.subject}
                      required=""
                    />
                    {this.state.validations.subject ? (
                      <span className="error animated bounce">
                        {this.state.validations.subject}
                      </span>
                    ) : null}
                  </article>
                  <article className="form-group textAreaCrtl">
                    <label htmlFor="comment">Comment:</label>
                    <textarea
                      className="form-control"
                      rows="4"
                      placeholder="How can we help you?"
                      id="comment"
                      name="comment"
                      onChange={this.onFormFieldChange}
                      value={this.state.comment}
                      required=""
                    />
                  </article>
                  <article className="pull-right">
                    <input
                      type="submit"
                      className="btn"
                      onClick={this.onSubmitHandler}
                    />
                  </article>
                  {this.props.isError ? (
                    <span className="error animated bounce">
                      {this.props.errorMessage}
                    </span>
                  ) : null}
                </form>
              </article>
              <article className="col-md-6 addressformat">
                <h3>CONTACT US</h3>
                <address>
                  <p>Smiling Star Advisory Pvt. Ltd.</p>
                  <p>
                    Technopolis IT Hub, C-56 A&#47;12, 4th Floor, Sector 62{" "}
                    <br />Noida &ndash; 201301 Uttar Pradesh, India
                  </p>
                  <p>
                    <a href="mailto:info@buddy4study.com">
                      info@buddy4study.com
                    </a>
                  </p>
                  <p>www.buddy4study.com</p>
                </address>
              </article>
            </article>
          </section>
        </section>
      </section>
    );
  }
}

export default ContactUsForm;
