import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  SUBMIT_CONTACT_US_REQUESTED,
  SUBMIT_CONTACT_US_SUCCEEDED,
  SUBMIT_CONTACT_US_FAILED
} from "./actions";

const contactUsApi = ({ inputData }) => {
  return fetchClient.post(apiUrl.contactUs, inputData).then(res => {
    return res.data;
  });
};

function* submitContactUs(input) {
  try {
    const contact_us = yield call(contactUsApi, input.payload);

    yield put({
      type: SUBMIT_CONTACT_US_SUCCEEDED,
      payload: contact_us
    });
  } catch (error) {
    yield put({
      type: SUBMIT_CONTACT_US_FAILED,
      payload: error.errorMessage
    });
  }
}

export default function* contactUsSaga() {
  yield takeEvery(SUBMIT_CONTACT_US_REQUESTED, submitContactUs);
}
