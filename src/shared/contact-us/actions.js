export const SUBMIT_CONTACT_US_REQUESTED = "SUBMIT_CONTACT_US_REQUESTED";
export const SUBMIT_CONTACT_US_SUCCEEDED = "SUBMIT_CONTACT_US_SUCCEEDED";
export const SUBMIT_CONTACT_US_FAILED = "SUBMIT_CONTACT_US_FAILED";

export const submitContactUs = data => ({
  type: SUBMIT_CONTACT_US_REQUESTED,
  payload: { inputData: data }
});
