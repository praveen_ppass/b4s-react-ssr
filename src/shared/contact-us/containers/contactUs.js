import React, { Component } from "react";
import ContactUsForm from "../components/contactUsForm";

import { submitContactUs as submitContactUsAction } from "../actions";
import { connect } from "react-redux";

const mapStateToProps = ({ contactUs }) => ({
  showLoader: contactUs.showLoader,
  type: contactUs.type,
  isError: contactUs.isError,
  errorMessage: contactUs.errorMessage
});

const mapDispatchToProps = dispatch => ({
  submitContactUs: contactData => dispatch(submitContactUsAction(contactData))
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactUsForm);
