import React, { Component } from "react";
import { connect } from "react-redux";

import PremiumMembershipPage from "../components/premiumMembershipPage";
import { merchantId as getMerchantIdAction } from "../../vle/actions";

const mapStateToProps = ({ loginOrRegister, marchant }) => ({
  isAuthenticated: loginOrRegister.isAuthenticated,
  marchantID: marchant.marchantId,
  type: marchant.type
});

const mapDispatchToProps = dispatch => ({
  getMerchantId: inputData => dispatch(getMerchantIdAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  PremiumMembershipPage
);
