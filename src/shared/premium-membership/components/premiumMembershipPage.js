import React, { Component } from "react";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import { Helmet } from "react-helmet";
import gblFunc from "../../../globals/globalFunctions";
import { PayNowPopUP } from "../../vle/components/pages/payNow";
import { FETCH_MARCHANT_ID_SUCCESS } from "../../vle/actions";
import UserLoginRegistrationPopup from "../../login/containers/userLoginRegistrationContainer";

class PremiumMembership extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLogged: false,
      paypop: false,
      selectedProduct: null,
      studentPaymentConfig: {
        url: "",
        text: "PAY NOW"
      },
      showLoginPopup: false
    };
    this.openpaypop = this.openpaypop.bind(this);
    this.closepaypop = this.closepaypop.bind(this);
    this.closeLoginPopup = this.closeLoginPopup.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case FETCH_MARCHANT_ID_SUCCESS:
        if (nextProps.isAuthenticated && this.state.selectedProduct) {
          const updateStudentPayment = { ...this.state.studentPaymentConfig };
          const userData = { ...this.state.userData };
          switch (this.state.selectedProduct) {
            case "S": //Annual membership
              updateStudentPayment.url = `https://www.instamojo.com/buddy4study/annual-membership-4c345/?data_name=${
                userData.firstName
                }${userData.lastName}&data_email=${userData.email}&data_phone=${
                userData.mobile
                }&data_Field_74346=${
                nextProps.marchantID.merchantTransactionNumber
                }&data_hidden=data_Field_74346`;
              break;
            case "A": //Six month membership
              updateStudentPayment.url = `https://www.instamojo.com/buddy4study/premium-membership-431a7/?data_name=${
                userData.firstName
                }${userData.lastName}&data_email=${userData.email}&data_phone=${
                userData.mobile
                }&data_Field_44258=${
                nextProps.marchantID.merchantTransactionNumber
                }&data_hidden=data_Field_44258`;
              break;
            default:
              return false;
          }

          this.setState({
            paypop: true,
            selectedProduct: null,
            studentPaymentConfig: updateStudentPayment
          });
        }
        break;
    }
  }

  openpaypop(selectedProduct) {
    const userList = gblFunc.getStoreUserDetails();
    this.setState({ selectedProduct: selectedProduct, userData: userList });
    this.props.getMerchantId({
      userId: userList.userId,
      marchentObj: {
        amount: selectedProduct == "A" ? 365 : 199,
        currency: "inr",
        paymentPartner: "B4S",
        transactionMode: "D",
        transactionType: "D",
        initiatorUserId: userList.userId,
        paidForUserId: userList.userId
      }
    });
  }
  closepaypop() {
    this.setState({
      paypop: false
    });
  }
  closeLoginPopup() {
    this.setState({ showLoginPopup: false });
  }

  render() {
    const isAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;

    return (
      <section className="gray">
        <section className="premiummembership">
          <Helmet> </Helmet>
          <BreadCrum
            classes={breadCrumObj["premium_membership"]["bgImage"]}
            listOfBreadCrum={breadCrumObj["premium_membership"]["breadCrum"]}
            title={breadCrumObj["premium_membership"]["title"]}
            subTitle={breadCrumObj["premium_membership"]["subTitle"]}
          />
          {this.state.paypop ? (
            <PayNowPopUP
              closepaypop={this.closepaypop}
              paymentConfig={this.state.studentPaymentConfig}
              htmlContent={`Access Matched Scholarships<br/>Receive Timely Alerts<br/>Dedicated Application Support`}
              payment_type="PREMIUM PROFILE"
            />
          ) : null}
          {this.state.showLoginPopup ? (
            <UserLoginRegistrationPopup closePopup={this.closeLoginPopup} />
          ) : null}

          <section className="container-fluid premium-row-steps">
            <section className="container equial-padding">
              <span id="payPreFormSubs" />
              <section className="row text-center">
                <section className="col-md-4 col-sm-4">
                  <h3>Step-1</h3>
                  <p>
                    Register at <span>Buddy4Study</span>
                  </p>
                </section>
                <section className="col-md-4 col-sm-4">
                  <h3>Step-2</h3>
                  <p>
                    Provide <span>Your Details</span>
                  </p>
                </section>
                <section className="col-md-4 col-sm-4">
                  <h3>Step-3 </h3>
                  <p>
                    Get Best-fit <span>Scholarships</span>
                  </p>
                </section>
              </section>
            </section>
          </section>

          <section className="container-fluid premium-row-details common-align equial-padding">
            <section className="container">
              <section className="row">
                <article className="col-md-12 text-center buddy-heading noPadding premium-membership">
                  <h1 className="premium">PREMIUM MEMBERSHIP</h1>
                  <p>
                    Access best-fit scholarship | Timely alerts | Application
                    support
                  </p>
                  <article className="col-md-6 col-sm-12 ">
                    <article>
                      <i>199</i>
                      <p>
                        6 Months <span>Membership</span>
                      </p>
                    </article>
                    <article className="btn-ctrl">
                      {isAuthenticated ? (
                        <button
                          type="button"
                          onClick={() => this.openpaypop("S")}
                        >
                          Starter Pack @ Rs 199 only
                        </button>
                      ) : (
                          <button
                            type="button"
                            onClick={() => {
                              this.setState({ showLoginPopup: true });
                            }}
                          >
                            Become Premium Member
                        </button>
                        )}
                    </article>
                  </article>
                  <article className="col-md-6 col-sm-12">
                    <article>
                      <i>365</i>
                      <p>
                        1 Year <span>Membership</span>
                      </p>
                    </article>

                    <article className="btn-ctrl">
                      {isAuthenticated ? (
                        <button
                          type="button"
                          onClick={() => this.openpaypop("A")}
                        >
                          Annual Pack @ Rs 1 per day only
                        </button>
                      ) : (
                          <button
                            type="button"
                            onClick={() => {
                              this.setState({ showLoginPopup: true });
                            }}
                          >
                            Become Premium Member
                        </button>
                        )}
                    </article>
                  </article>
                </article>
              </section>
              <section className="row margin-top">
                <article className="col-sm-12 col-md-6 cell-padding col-margin-about">
                  <article className="cell-bg cell-height">
                    <article className="col-md-12">
                      <article className="details">
                        <h5 className="ap">
                          APPLY FOR MATCHING SCHOLARSHIPS FASTER
                        </h5>
                        <p>
                          Premium membership helps you to find the best-fit
                          scholarship and enables you to apply for the right
                          scholarships at the right time.
                        </p>
                      </article>
                    </article>

                    <article className="col-md-12 col-margin-about">
                      <article className="details">
                        <h5 className="dm">ACCENTUATE YOUR PROFILE</h5>
                        <p>
                          Premium membership takes you a notch ahead of others
                          by highlighting your profile to scholarship providers.
                        </p>
                      </article>
                    </article>
                    <article className="col-md-12 col-margin-about">
                      <article className="details">
                        <h5 className="ps">SMS / EMAIL ALERTS</h5>
                        <p>
                          Never miss an opportunity. Regular updates through SMS
                          and emails help you stay up-to-date with scholarships
                          matching your profile.
                        </p>
                      </article>
                    </article>
                  </article>
                </article>
                <article className="col-sm-12 col-md-6 cell-padding col-margin-about">
                  <article className="cell-bg cell-height">
                    <article className="col-md-12">
                      <article className="details">
                        <h5 className="ps">GET EXPERT SUPPORT</h5>
                        <p>
                          Premium members receive personal scholarship
                          counsellor and support for application documents to
                          help through the process.
                        </p>
                      </article>
                    </article>

                    <article className="col-md-12">
                      <article className="details">
                        <h5 className="ap">GET PRIORITY SUPPORT SERVICE</h5>
                        <p>
                          With Premium membership, your queries are handled on
                          priority, saving you time.
                        </p>
                      </article>
                    </article>
                  </article>
                </article>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default PremiumMembership;
