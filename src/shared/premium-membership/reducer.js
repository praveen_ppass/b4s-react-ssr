import {
  FETCH_PRIVACY_POLICY_SUCCEEDED,
  FETCH_PRIVACY_POLICY_REQUESTED,
  FETCH_PRIVACY_POLICY_FAILED
} from "./actions";

const initialState = [];
const premiumMembershipReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_PRIVACY_POLICY_SUCCEEDED:
      return payload.BODY.DATA.NODE_BODY.BODY;
    case FETCH_PRIVACY_POLICY_REQUESTED:
      return payload;
    case FETCH_PRIVACY_POLICY_FAILED:
      return payload;
    default:
      return state;
  }
};
export default privacyConditionReducer;
