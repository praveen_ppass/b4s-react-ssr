import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_PRIVACY_POLICY_REQUESTED,
  FETCH_PRIVACY_POLICY_SUCCEEDED,
  FETCH_PRIVACY_POLICY_FAILED
} from "./actions";

const fetchUrl = () =>
  fetchClient
    .post(apiUrl.dynamic_page_data, { SLUG: "page/disclaimer" })
    .then(res => {
      return res.data;
    });
function* fetchPrivacyCondition(input) {
  try {
    const privacy_policy = yield call(fetchUrl);

    yield put({
      type: FETCH_PRIVACY_POLICY_SUCCEEDED,
      payload: privacy_policy
    });
  } catch (error) {
    yield put({
      type: FETCH_PRIVACY_POLICY_FAILED,
      payload: error
    });
  }
}

export default function* fetchPrivacyConditionSaga() {
  yield takeEvery(FETCH_PRIVACY_POLICY_REQUESTED, fetchPrivacyCondition);
}
