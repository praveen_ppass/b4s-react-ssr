import React from "react";
import { connect } from "react-redux";
import { fetchTeam as fetchTeamAction } from "../actions";
import Team from "../components/teamPage";

const mapStateToProps = ({ team }) => ({
  team: team.team,
  showLoader: team.showLoader,
  isError: team.isError,
  errorMessage: team.errorMessage
});

const mapDispatchToProps = dispatch => ({
  loadTeam: () => dispatch(fetchTeamAction("pageData"))
});

export default connect(mapStateToProps, mapDispatchToProps)(Team);
