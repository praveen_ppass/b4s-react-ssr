import React, { Component, Fragment } from "react";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import { Helmet } from "react-helmet";
import Loader from "../../common/components/loader";
import ServerError from "../../common/components/serverError";
import TeamData from "./../team.json";

import Slider from "react-slick";

class Team extends Component {
	constructor(props) {
		super(props);
		this.state = {
			teamTab: "Manjeet_Singh",
			tabType: "",
			isTabStatus: false,
			teamIndex: "team_one"
		};
		this.ChangeTab = this.ChangeTab.bind(this);
	}

	ChangeTab(tabName) {
		const teamName = tabName.split(" ").join("_");
		switch (teamName) {
			case "Manjeet_Singh":
				this.setState({
					teamTab: "Manjeet_Singh",
					teamIndex: "team_one",
					isTabStatus: true
				});
				return <TabInnerData />;
			case "Raj_Kishor":
				this.setState({
					teamTab: "Raj_Kishor",
					teamIndex: "team_one",
					isTabStatus: true
				});
				return <TabInnerData />;
			case "Ankur_Dhawan":
				this.setState({
					teamTab: "Ankur_Dhawan",
					teamIndex: "team_one",
					isTabStatus: true
				});
				return <TabInnerData />;
			case "Ashutosh_Kumar_Burnwal":
				this.setState({
					teamTab: "Ashutosh_Kumar_Burnwal",
					teamIndex: "team_one",
					isTabStatus: true
				});
				return <TabInnerData />;
			case "Ravi_Kant":
				this.setState({
					teamTab: "Ravi_Kant",
					teamIndex: "team_two",
					isTabStatus: true
				});
				return <TabInnerData />;
			case "Ashish_Jha":
				this.setState({
					teamTab: "Ashish_Jha",
					teamIndex: "team_two",
					isTabStatus: true
				});
				return <TabInnerData />;
			case "Navdeep_Saklani":
				this.setState({
					teamTab: "Navdeep_Saklani",
					teamIndex: "team_two",
					isTabStatus: true
				});
				return <TabInnerData />;
			case "Neha_Agarwal":
				this.setState({
					teamTab: "Neha_Agarwal",
					teamIndex: "team_two",
					isTabStatus: true
				});
				return <TabInnerData />;
			case "V_Karthik":
				this.setState({
					teamTab: "V_Karthik",
					teamIndex: "team_three",
					isTabStatus: true
				});
				return <TabInnerData />;
			case "Neha_Sharma":
				this.setState({
					teamTab: "Neha_Sharma",
					teamIndex: "team_three",
					isTabStatus: true
				});
				return <TabInnerData />;
			case "Dishant_Kharbanda":
				this.setState({
					teamTab: "Dishant_Kharbanda",
					teamIndex: "team_three",
					isTabStatus: true
				});
				return <TabInnerData />;
			case "Shiv_Tiwary":
				this.setState({
					teamTab: "Shiv_Tiwary",
					teamIndex: "team_three",
					isTabStatus: true
				});
				return <TabInnerData />;
		}
	}
	componentDidMount() {
		this.setState({
			teamTab: "Manjeet_Singh",
			teamIndex: "team_one",
			isTabStatus: true
		});
	}

	render() {
		const { teamTab, isTabStatus, teamIndex } = this.state;
		if (this.props.isError) {
			return (
				<section>
					<section className="teamnew">
						<BreadCrum
							classes={breadCrumObj["team"]["bgImage"]}
							listOfBreadCrum={breadCrumObj["team"]["breadCrum"]}
							title={breadCrumObj["team"]["title"]}
							subTitle={breadCrumObj["team"]["subTitle"]}
						/>
						<ServerError errorMessage={this.props.errorMessage} />
					</section>
				</section>
			);
		}
		const settings = {
			className: "center",
			centerMode: false,
			infinite: true,
			autoplay: false,
			centerPadding: "0px",
			slidesToShow: 4,
			speed: 2000,
			autoplaySpeed: 4000,

			responsive: [
				{
					breakpoint: 799,
					settings: {
						autoplay: false,
						slidesToShow: 1,
						slidesToScroll: 1,
						initialSlide: 1
					}
				},
				{
					breakpoint: 480,
					settings: {
						centerMode: true,
						infinite: true,
						autoplay: false,
						centerPadding: "0px",
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		};
		return (
			<section>
				<Loader isLoader={this.props.showLoader} />
				<section className="teamnew">
					<Helmet> </Helmet>

					<BreadCrum
						classes={breadCrumObj["team"]["bgImage"]}
						listOfBreadCrum={breadCrumObj["team"]["breadCrum"]}
						title={breadCrumObj["team"]["title"]}
						subTitle={breadCrumObj["team"]["subTitle"]}
					/>
					{/* team section */}
					<section className="container text-center">
						<h5>
							THE LEADERSHIP <span>TEAM</span>
						</h5>
						<p>
							Meet the young and dynamic leadership team at Buddy4Study.  Together, they bind the entire team to lead a single mission – Making scholarships accessible and empowering student community towards nation building.
						</p>
					</section>
					<section className="bg-tab">
						<article>
							<ul>
								<li>Meet Our Leaders</li>
							</ul>
						</article>
					</section>
					{/* No Slider Theme */}
					<section className="container-fluid scholarship-buddyteam marginTop desktopView">
						<section className="row marginBottom">
							<section className="container">
								<section className="row">
									{TeamData &&
										TeamData.team_one.map(team => {
											return (
												<article
													className="col-md-3"
													key={team.id}
													onClick={() => this.ChangeTab(team.name)}
												>
													<img src={team.pic} className={teamTab == team.name.split(" ").join("_") ? "img-style colorActive" : "img-style gray"} />
													<span className={teamTab === team.name.split(" ").join("_") ? "arrow" : ""}></span>
												</article>
											);
										})}
								</section>
							</section>
							<section className={isTabStatus && teamIndex === "team_one" ? "bg-inner show" : "bg-inner hide"}>
								<section className="container">
									<TabData teamTab={teamTab} />
								</section>
							</section>
						</section>

						<section className="row marginBottom">
							<section className="container">
								<section className="row">
									{TeamData &&
										TeamData.team_two.map(team => {
											return (
												<article
													className="col-md-3"
													key={team.id}
													onClick={() => this.ChangeTab(team.name)}
												>
													<img src={team.pic} className={teamTab == team.name.split(" ").join("_") ? "img-style colorActive" : "img-style gray"} />
													<span className={teamTab === team.name.split(" ").join("_") ? "arrow" : ""}></span>
												</article>
											);
										})}
								</section>
							</section>
							<section className={isTabStatus && teamIndex === "team_two" ? "bg-inner show" : "bg-inner hide"}>
								<section className="container">
									<TabData teamTab={teamTab} />
								</section>
							</section>
						</section>

						<section className="row marginBottom">
							<section className="container">
								<section className="row">
									{TeamData &&
										TeamData.team_three.map(team => {
											return (
												<article
													className="col-md-3"
													key={team.id}
													onClick={() => this.ChangeTab(team.name)}
												>
													<img src={team.pic} className={teamTab == team.name.split(" ").join("_") ? "img-style colorActive" : "img-style gray"} />
													<span className={teamTab === team.name.split(" ").join("_") ? "arrow" : ""}></span>
												</article>
											);
										})}
								</section>
							</section>
							<section className={isTabStatus && teamIndex === "team_three" ? "desktopSlider-content bg-inner show" : "desktopSlider-content bg-inner hide"}>
								<section className="container">
									<TabData teamTab={teamTab} />
								</section>
							</section>
						</section>
					</section>

					{/* Slider Theme */}
					<section className="container-fluid scholarship-buddyteam marginTop mobileView">
						<section className="row marginBottom">
							<section className="container">
								<section className="row">
									<Slider {...settings}>
										{TeamData &&
											TeamData.team_one.map(team => {
												return (
													<Fragment>
														<article
															className="col-md-3"
															key={team.id}
														>
															<img src={team.pic} className="img-style colorActive" />
															<span className="arrow"></span>
														</article>
														<section className="bg-inner">
															<section className="container">
																<h1>{team.name} <article className="socialIconWraper"><a href={team.linkedinUrl} target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></article></h1>
																<h2>{team.designation}</h2>
																<article dangerouslySetInnerHTML={{
																	__html: team.description
																}} />
																<article dangerouslySetInnerHTML={{
																	__html: team.aboutMe
																}} />
															</section>
														</section>
													</Fragment>
												);
											})}
									</Slider>
								</section>
							</section>
						</section>
						<section className="row marginBottom">
							<section className="container">
								<section className="row">
									<Slider {...settings}>
										{TeamData &&
											TeamData.team_two.map(team => {
												return (
													<Fragment>
														<article
															className="col-md-3"
															key={team.id}
														>
															<img src={team.pic} className="img-style colorActive" />
															<span className="arrow"></span>
														</article>
														<section className="bg-inner">
															<section className="container">
																<h1>{team.name} <article className="socialIconWraper"><a href={team.linkedinUrl} target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></article></h1>
																<h2>{team.designation}</h2>
																<article dangerouslySetInnerHTML={{
																	__html: team.description
																}} />
																<article dangerouslySetInnerHTML={{
																	__html: team.aboutMe
																}} />
															</section>
														</section>
													</Fragment>
												);
											})}
									</Slider>
								</section>
							</section>
						</section>
						<section className="row marginBottom">
							<section className="container">
								<section className="row">
									<Slider {...settings}>
										{TeamData &&
											TeamData.team_three.map(team => {
												return (
													<Fragment>
														<article
															className="col-md-3"
															key={team.id}
														>
															<img src={team.pic} className="img-style colorActive" />
															<span className="arrow"></span>
														</article>
														<section className="bg-inner">
															<section className="container">
																<h1>{team.name} <article className="socialIconWraper"><a href={team.linkedinUrl} target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></article></h1>
																<h2>{team.designation}</h2>
																<article dangerouslySetInnerHTML={{
																	__html: team.description
																}} />
																<article dangerouslySetInnerHTML={{
																	__html: team.aboutMe
																}} />
															</section>
														</section>
													</Fragment>
												);
											})}
									</Slider>
								</section>
							</section>
						</section>
					</section>
				</section>
			</section>
		);
	}
}
const TabData = props => {
	switch (props.teamTab) {
		case "Manjeet_Singh":
			return <TabInnerData data={TeamData && TeamData.team_one[0]} />;
			break;
		case "Raj_Kishor":
			return <TabInnerData data={TeamData && TeamData.team_one[1]} />;
			break;
		case "Ankur_Dhawan":
			return <TabInnerData data={TeamData && TeamData.team_one[2]} />;
			break;
		case "Ashutosh_Kumar_Burnwal":
			return <TabInnerData data={TeamData && TeamData.team_one[3]} />;
			break;
		case "Ravi_Kant":
			return <TabInnerData data={TeamData && TeamData.team_two[0]} />;
			break;
		case "Ashish_Jha":
			return <TabInnerData data={TeamData && TeamData.team_two[1]} />;
			break;
		case "Navdeep_Saklani":
			return <TabInnerData data={TeamData && TeamData.team_two[2]} />;
			break;
		case "Neha_Agarwal":
			return <TabInnerData data={TeamData && TeamData.team_two[3]} />;
			break;
		case "V_Karthik":
			return <TabInnerData data={TeamData && TeamData.team_three[0]} />;
			break;
		case "Neha_Sharma":
			return <TabInnerData data={TeamData && TeamData.team_three[1]} />;
			break;
		case "Dishant_Kharbanda":
			return <TabInnerData data={TeamData && TeamData.team_three[2]} />;
			break;
		case "Shiv_Tiwary":
			return <TabInnerData data={TeamData && TeamData.team_three[3]} />;
			break;
		default:
			return <article />;
	}
};

export default Team;

const TabInnerData = props => {
	const { data } = props;
	return (
		<article className="col-md-12 tabs">
			<h1>{data.name} <article className="socialIconWraper"><a href={data.linkedinUrl} target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></article></h1>
			<h2>{data.designation}</h2>
			<article dangerouslySetInnerHTML={{
				__html: data.description
			}} />
			<article dangerouslySetInnerHTML={{
				__html: data.aboutMe
			}} />
		</article>
	);
};
