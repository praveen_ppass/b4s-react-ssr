import {
  FETCH_ONLINEAPPLICATIONFORMLIST_REQUESTED,
  FETCH_ONLINEAPPLICATIONFORMLIST_SUCCEEDED,
  FETCH_ONLINEAPPLICATIONFORMLIST_FAILED,
  FETCH_FEATURED_SCHOLARSHIPSLIST_REQUESTED,
  FETCH_FEATURED_SCHOLARSHIPSLIST_SUCCEEDED,
  FETCH_FEATURED_SCHOLARSHIPSLIST_FAILED
} from "./actions";

const initialState = {
  showLoader: true,
  isError: false,
  errorMessage: ""
};

const onlineApplicationFormListReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case FETCH_ONLINEAPPLICATIONFORMLIST_REQUESTED:
      return {
        ...state,
        onlineApplicationFormListItems: [],
        showLoader: true,
        type
      };
    case FETCH_ONLINEAPPLICATIONFORMLIST_SUCCEEDED:
      return {
        ...state,
        onlineApplicationFormListItems: payload,
        showLoader: false,
        type
      };

    case FETCH_ONLINEAPPLICATIONFORMLIST_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        errorMessage: payload,
        isError: true
      };

    case FETCH_FEATURED_SCHOLARSHIPSLIST_REQUESTED:
      return {
        ...state,
        fetcherScholarshipsListItems: [],
        showLoader: true,
        type
      };

    case FETCH_FEATURED_SCHOLARSHIPSLIST_SUCCEEDED:
      return {
        ...state,
        fetcherScholarshipsListItems: payload,
        showLoader: false,
        type
      };

    case FETCH_FEATURED_SCHOLARSHIPSLIST_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        errorMessage: payload,
        isError: true
      };

    default:
      return state;
  }
};
export default onlineApplicationFormListReducer;
