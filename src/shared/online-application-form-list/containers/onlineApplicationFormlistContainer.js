import { connect } from "react-redux";

import {
  fetchOnlineApplicationFormList as fetchOnlineApplicationFormListAction,
  fetchFeaturedScholarshipsList as fetchFeaturedScholarshipsListAction
} from "../actions";

import OnlineApplicationFormList from "../components/onlineApplicationFormlistPage";

const mapStateToProps = ({ onlineApplicationFormList }) => ({
  onlineApplicationFormListItems:
    onlineApplicationFormList.onlineApplicationFormListItems,
  fetcherScholarshipsListItems:
    onlineApplicationFormList.fetcherScholarshipsListItems
});

const mapDispatchToProps = dispatch => ({
  loadOnlineApplicationFormList: inputData =>
    dispatch(fetchOnlineApplicationFormListAction(inputData)),
  loadFeaturedScholarshipsList: inputData =>
    dispatch(fetchFeaturedScholarshipsListAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  OnlineApplicationFormList
);
