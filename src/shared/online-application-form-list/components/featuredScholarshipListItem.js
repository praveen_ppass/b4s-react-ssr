import React from "react";
import { Link } from "react-router-dom";
import gblFunc from "../../../globals/globalFunctions";
import moment from "moment";
const FeaturedScholarshipListItem = props => {
  const {
    fetcherScholarshipsListItems,
    classes,
    headerTitle,
    fromListPage
  } = props;

  if (fromListPage) {
    let premiumScholarships = fetcherScholarshipsListItems.filter(
      scholarshipsListItems =>
        scholarshipsListItems.status == 1 && scholarshipsListItems.slug != ""
    );

    return (
      <article className="box listFeaturedScholarships">
        <h4>{headerTitle}</h4>
        {premiumScholarships.map(scholarshipsListItems => (
          <p key={scholarshipsListItems.id}>
            <Link
              to={`/scholarship/${
                scholarshipsListItems.slug
              }?utm_source=FeaturedList&utm_medium=WebDetailPage`}
            >
              <img
                property="image"
                src={scholarshipsListItems.logoFid}
                alt="buddy4study-features"
              />
            </Link>
            <span>
              <Link
                to={`/scholarship/${
                  scholarshipsListItems.slug
                }?utm_source=FeaturedList&utm_medium=WebDetailPage`}
                dangerouslySetInnerHTML={{
                  __html: scholarshipsListItems.scholarshipName
                    ? gblFunc.replaceWithLoreal(
                        scholarshipsListItems.scholarshipName
                      )
                    : ""
                }}
              />
              <i>
                {moment(
                  scholarshipsListItems.deadlineDate,
                  "YYYY-MM-DD"
                ).format("DD-MMM-YYYY")}
              </i>
            </span>
          </p>
        ))}
      </article>
    );
  } else {
    return (
      <article className={classes.join(" ")}>
        <article className="cellBox-mobo">
          <article>
            <dd>{headerTitle}</dd>
            {fetcherScholarshipsListItems &&
            fetcherScholarshipsListItems.length > 0
              ? fetcherScholarshipsListItems
                  .filter(scholarshipsListItems => {
                    return (
                      scholarshipsListItems.status == 1 &&
                      scholarshipsListItems.slug != ""
                    );
                  })
                  .map(scholarshipsListItems => (
                    <ScholarshipInfo
                      key={scholarshipsListItems.id}
                      scholarshipsListItems={scholarshipsListItems}
                      fromListPage={fromListPage}
                    />
                  ))
              : null}
          </article>
        </article>
      </article>
    );
  }
};

const ScholarshipInfo = props => {
  return (
    <p>
      <Link
        to={`/scholarship/${
          props.scholarshipsListItems.slug
        }?utm_source=FeaturedList&utm_medium=WebDetailPage`}
      >
        <img
          property="image"
          src={props.scholarshipsListItems.logoFid}
          alt="buddy4study-features"
        />
      </Link>

      <span>
        <Link
          to={`/scholarship/${
            props.scholarshipsListItems.slug
          }?utm_source=FeaturedList&utm_medium=WebDetailPage`}
          dangerouslySetInnerHTML={{
            __html: props.scholarshipsListItems.scholarshipName
              ? gblFunc.replaceWithLoreal(
                  props.scholarshipsListItems.scholarshipName
                )
              : ""
          }}
        />
        <i>
          {moment(
            props.scholarshipsListItems.deadlineDate,
            "YYYY-MM-DD"
          ).format("DD-MMM-YYYY")}
        </i>
      </span>
    </p>
  );
};

export default FeaturedScholarshipListItem;
