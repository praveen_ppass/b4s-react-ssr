import React from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import gblFunc from "../../../globals/globalFunctions";
import { imgBaseUrl, defaultImageLogo } from "../../../constants/constants";
const OnlineApplicationFormlistItem = props => {
  return props.onlineApplicationFormListItems.map((item, index) => {
    let daysToGO;
    daysToGO =
      parseInt(
        moment(item.deadline, "YYYY-MM-DD").diff(
          moment().startOf("day"),
          "days"
        )
      ) + 1;
    // }

    let daysLabel;
    if (daysToGO === 1) {
      daysLabel = "Last";
    } else if (daysToGO === 2) {
      daysLabel = "1";
    } else {
      daysLabel = daysToGO;
    }

    return (
      <article className="box" key={index}>
        <article className="data-row flex-container">
          <article className="col-md-2 col-sm-12 flex-item">
            <article className="tablecell">
              <article className="logotext">
                <Link to={`/scholarship/${item.slug}`}>
                  <img
                    src={
                      item.logoUrl !== null ? item.logoUrl : defaultImageLogo
                    }
                    className="img-responsive"
                    alt={item.title}
                  />
                </Link>
                {/* <p>
                  {parseInt(item.views)
                    ? `${
                    parseInt(item.views) > 1
                      ? parseInt(item.views)
                      : " view"
                    } views`
                    : 0 + " view"}
                </p> */}
              </article>
            </article>
          </article>
          <article className="col-md-7 col-sm-9 flex-item">
            <article className="tablecell">
              <article className="contentdisplay">
                {item.title !== null && (
                  <h2>
                    <Link to={`/scholarship/${item.slug}`} className="ellipsis">
                      <span
                        dangerouslySetInnerHTML={{
                          __html: item.title
                            ? gblFunc.replaceWithLoreal(item.title)
                            : ""
                        }}
                      />
                    </Link>
                    <article className="namelistTooltip">
                      <span
                        dangerouslySetInnerHTML={{
                          __html: item.title
                            ? gblFunc.replaceWithLoreal(item.title)
                            : ""
                        }}
                      />
                      <i className="arrow" />
                    </article>
                  </h2>
                )}
                <p>
                  <img
                    src={`${imgBaseUrl}scholarship-icon.png`}
                    alt="scholarship-icon"
                  />
                  {item.applicableFor}
                </p>
                <p>
                  <img src={`${imgBaseUrl}awards-icon.png`} alt="awards-icon" />
                  {item.purposeAward}
                </p>
              </article>
            </article>
          </article>
          {daysToGO >= 1 ? (
            item.deadline ? (
              <article className="col-md-3 col-sm-3 flex-item">
                <article className="tablecell">
                  <article className="btnCell">
                    <span
                      onClick={() => {
                        props.gtmEventHandler([
                          "Application-Listing",
                          item.title
                        ]);
                        window.open(item.applyLink);
                      }}
                    >
                      Apply Now
                    </span>
                  </article>
                  {daysToGO < 17 ? (
                    <article
                      className={daysToGO < 9 ? "daysgo pink" : "daysgo yellow"}
                    >
                      <span>
                        {daysLabel}
                        <p>{`day${daysToGO > 2 ? "s" : ""} to go`}</p>
                      </span>
                    </article>
                  ) : (
                    <article className="calender">
                      <p className="text-center">
                        Last Date to apply
                        <span>
                          {moment(item.deadline).format("D")}{" "}
                          {moment(item.deadline).format("MMM, YY")}
                        </span>
                      </p>
                    </article>
                  )}
                </article>
              </article>
            ) : (
              ""
            )
          ) : item.deadline ? (
            <article className="col-md-3 col-sm-3 flex-item">
              <article className="tablecell">
                <article className="btnCell">
                  <span
                    onClick={() => {
                      props.gtmEventHandler([
                        "Application-Listing",
                        item.title
                      ]);
                      window.open(item.applyLink);
                    }}
                  >
                    Apply Now
                  </span>
                </article>
                <article className="daysgo gray">
                  <span>Closed</span>
                </article>
              </article>
            </article>
          ) : (
            <article className="col-md-3 col-sm-3 flex-item">
              <article className="tablecell">
                <article className="btnCell">
                  <span
                    onClick={() => {
                      props.gtmEventHandler([
                        "Application-Listing",
                        item.title
                      ]);
                      window.open(item.applyLink);
                    }}
                  >
                    Apply Now
                  </span>
                </article>
                <article className="daysgo pink always">
                  <p>Always Open</p>
                </article>
              </article>
            </article>
          )}
        </article>
      </article>
    );
  });
};

export default OnlineApplicationFormlistItem;
