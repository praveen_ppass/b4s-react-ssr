import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import OnlineApplicationFormlistItem from "../components/onlineApplicationFormlistItem";
import gblFunc from "../../../globals/globalFunctions";
import FeaturedScholarshipListItem from "../components/featuredScholarshipListItem";
import Loader from "../../common/components/loader";
import AlertMessage from "../../common/components/alertMsg";
import Pagination from "rc-pagination";

class OnlineApplicationFormList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isShowAlert: false,
      alertMsg: "",
      statusAlert: false
    };
    this.hideAlert = this.hideAlert.bind(this);
    this.gtmEventHandler = this.gtmEventHandler.bind(this);
  }

  hideAlert() {
    this.setState({
      isShowAlert: false
    });
  }

  componentWillMount() {
    this.props.loadOnlineApplicationFormList();
    this.props.loadFeaturedScholarshipsList();
  }

  gtmEventHandler(gtm) {
    gblFunc.gaTrack.trackEvent(gtm);
  }

  render() {
    return (
      <section className="onlineApplicationForm-list">
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          isShow={this.state.isShowAlert}
          msg={this.state.alertMsg}
          close={this.hideAlert}
          status={this.state.statusAlert}
        />

        {/* breacrum */}
        <section className="container-fluid row-bg font-family list">
          <section className="container">
            <ul className="breadcrumb">
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <a href="javascript:void(0);">Online Application Form</a>
              </li>
            </ul>
          </section>
        </section>

        {/* End */}
        <section className="container-fluid titleNTRow">
          <section className="container">
            <section className="row">
              <article className="col-md-12">
                <article>
                  <h1>
                    Live Scholarship Applications
                    <span>
                      Find below scholarships for which applications are live
                      now! You are advised to check eligibility criteria for
                      each scholarship and apply before deadline.
                    </span>
                  </h1>
                </article>
              </article>
            </section>
          </section>
        </section>
        {/* list of online application form */}
        <section className="list marginBox">
          <article className="container">
            <article className="row flex-container-wrap">
              <article className="col-md-9 col-sm-9 col-xs-12 marginRight flex-container-list">
                {this.props.onlineApplicationFormListItems &&
                  this.props.onlineApplicationFormListItems.length > 0 ? (
                    <OnlineApplicationFormlistItem
                      onlineApplicationFormListItems={
                        this.props.onlineApplicationFormListItems || []
                      }
                      gtmEventHandler={this.gtmEventHandler}
                    />
                  ) : (
                    "No Online Application Form Found"
                  )}
              </article>
              {/* right panel */}
              <article className="col-md-3 col-sm-3 col-xs-12 marginLeft flex-container-list">
                {this.props.fetcherScholarshipsListItems &&
                  this.props.fetcherScholarshipsListItems.length > 0 ? (
                    <FeaturedScholarshipListItem
                      headerTitle="Featured scholarships"
                      classes="mobo-hide"
                      fromListPage
                      fetcherScholarshipsListItems={
                        this.props.fetcherScholarshipsListItems || []
                      }
                    />
                  ) : null}
              </article>
            </article>
          </article>
        </section>
      </section>
    );
  }
}
export default OnlineApplicationFormList;
