import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
// import { showLoading, hideLoading } from "react-redux-loading-bar";

import { apiUrl, getApi, endPoint, baseUrls } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_ONLINEAPPLICATIONFORMLIST_REQUESTED,
  FETCH_ONLINEAPPLICATIONFORMLIST_SUCCEEDED,
  FETCH_ONLINEAPPLICATIONFORMLIST_FAILED,
  FETCH_FEATURED_SCHOLARSHIPSLIST_REQUESTED,
  FETCH_FEATURED_SCHOLARSHIPSLIST_SUCCEEDED,
  FETCH_FEATURED_SCHOLARSHIPSLIST_FAILED
} from "./actions";

const fetchOnlineApplicationFormListUrl = () =>
  fetchClient.get(apiUrl.onlineApplicationFormListApi).then(res => res.data);

function* fetchOnlineApplicationFormList(input) {
  try {
    const onlineApplicationFormListItems = yield call(
      fetchOnlineApplicationFormListUrl,
      input.payload
    );
    yield put({
      type: FETCH_ONLINEAPPLICATIONFORMLIST_SUCCEEDED,
      payload: onlineApplicationFormListItems
    });
  } catch (err) {
    yield put({
      type: FETCH_ONLINEAPPLICATIONFORMLIST_FAILED,
      payload: err
    });
  }
}

// fetch Featured Scholarships call
const fetchFeaturedScholarshipsListFunction = () =>
  fetchClient.get(`${apiUrl.featuredScholarships}=true`, {}).then(res => {
    return res.data;
  });

function* fetchFeaturedScholarshipsList(input) {
  try {
    const fetcherScholarshipsListItems = yield call(
      fetchFeaturedScholarshipsListFunction
    );

    yield put({
      type: FETCH_FEATURED_SCHOLARSHIPSLIST_SUCCEEDED,
      payload: fetcherScholarshipsListItems
    });
  } catch (error) {
    yield put({
      type: FETCH_FEATURED_SCHOLARSHIPSLIST_FAILED,
      payload: error
    });
  }
}

export default function* fetchOnlineApplicationFormListSaga() {
  yield takeEvery(
    FETCH_ONLINEAPPLICATIONFORMLIST_REQUESTED,
    fetchOnlineApplicationFormList
  );
  yield takeEvery(
    FETCH_FEATURED_SCHOLARSHIPSLIST_REQUESTED,
    fetchFeaturedScholarshipsList
  );
}
