export const FETCH_ONLINEAPPLICATIONFORMLIST_REQUESTED =
  "FETCH_ONLINEAPPLICATIONFORMLIST_REQUESTED";
export const FETCH_ONLINEAPPLICATIONFORMLIST_SUCCEEDED =
  "FETCH_ONLINEAPPLICATIONFORMLIST_SUCCEEDED";
export const FETCH_ONLINEAPPLICATIONFORMLIST_FAILED =
  "FETCH_ONLINEAPPLICATIONFORMLIST_FAILED";

export const FETCH_FEATURED_SCHOLARSHIPSLIST_REQUESTED =
  "FETCH_FEATURED_SCHOLARSHIPSLIST_REQUESTED";
export const FETCH_FEATURED_SCHOLARSHIPSLIST_SUCCEEDED =
  "FETCH_FEATURED_SCHOLARSHIPSLIST_SUCCEEDED";
export const FETCH_FEATURED_SCHOLARSHIPSLIST_FAILED =
  "FETCH_FEATURED_SCHOLARSHIPSLIST_FAILED";

export const fetchOnlineApplicationFormList = data => ({
  type: FETCH_ONLINEAPPLICATIONFORMLIST_REQUESTED,
  payload: { inputData: data }
});

export const fetchFeaturedScholarshipsList = data => ({
  type: FETCH_FEATURED_SCHOLARSHIPSLIST_REQUESTED,
  payload: { inputData: data }
});
