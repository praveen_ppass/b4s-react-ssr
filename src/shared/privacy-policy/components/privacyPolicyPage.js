import React, { Component } from "react";
import { breadCrumObj } from "../../../constants/breadCrum";
import { Helmet } from "react-helmet";
import gblFunc from "../../../globals/globalFunctions";
import BreadCrum from "../../components/bread-crum/breadCrum";
import Loader from "../../common/components/loader";
import { imgBaseUrl } from "../../../constants/constants";
import ServerError from "../../common/components/serverError";

class PrivacyPolicy extends Component {
  componentDidMount() {
    this.props.loadPrivacyPolicy();
  }

  getTitle() {
    if (this.props.privacyPolicyList != null) {
      return this.props.privacyPolicyList.title;
    }
  }

  rawMarkUp() {
    if (this.props.privacyPolicyList != null) {
      let rawMarkup = this.props.privacyPolicyList.body;
      return { __html: rawMarkup };
    }
  }

  render() {
    if (this.props.isError) {
      return (
        <section>
          <section className="gray">
            <section className="tnc">
              <BreadCrum
                classes={breadCrumObj["privacy_policy"]["bgImage"]}
                listOfBreadCrum={breadCrumObj["privacy_policy"]["breadCrum"]}
                title={breadCrumObj["privacy_policy"]["title"]}
              />
              <ServerError errorMessage={this.props.errorMessage} />
            </section>
          </section>
        </section>
      );
    }

    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <section className="gray">
          <section className="tnc">
            <Helmet> </Helmet>
            <BreadCrum
              classes={breadCrumObj["privacy_policy"]["bgImage"]}
              listOfBreadCrum={breadCrumObj["privacy_policy"]["breadCrum"]}
              title={breadCrumObj["privacy_policy"]["title"]}
            />
            <article className="privacy">
              <section className="conatiner-fluid graybg">
                <article className="container">
                  <article className="row ">
                    <article className="col-md-4 col-sm-4 col-xs-12 right">
                      <h1>{this.getTitle()}</h1>
                      <img
                        src={`${imgBaseUrl}arrowr.png`}
                        alt="arrow"
                        alt="buddy4study-arrow"
                      />
                    </article>
                    <article className="col-md-8 col-sm-8 col-xs-12 left">
                      <article
                        className="row"
                        dangerouslySetInnerHTML={this.rawMarkUp()}
                      />
                    </article>
                  </article>
                </article>
              </section>
            </article>
          </section>
        </section>
      </section>
    );
  }
}

export default PrivacyPolicy;
