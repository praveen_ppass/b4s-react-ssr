import React, { Component } from "react";

class ikiGai extends Component {
  render() {
    return (
      <section className="container-fluid details-page-wrapper-ikigai">
        <section className="container">
          <section className="row">
            <section className="col-md-12">
              <ul className="breadcrumb">
                <li>
                  <a href="/">Home</a>
                </li>
                <li>
                  <a href="/scholarships">Scholarship</a>
                </li>
                <li>
                  <a property="url">Ikigai Scholarship 2019</a>
                </li>
              </ul>
            </section>
          </section>
          <section className="row">
            <section className="col-md-12">
              <section className="row">
                <section className="moboCellOrderikigai">
                  <article className="row flex-item">
                    <section className="col-md-9 col-sm-9">
                      <h1 className="titleText">Ikigai Scholarship 2019</h1>
                    </section>
                    <section className="col-md-3 col-sm-3">
                      <article className="col-md-12 Date">
                        Last updated on&nbsp;
                        <br />
                        <span>
                          <i className="fa fa-calendar" /> &nbsp;21-Feb-2019
                        </span>
                      </article>
                    </section>
                  </article>
                  <article className="row flex-item">
                    <article className="moboCellOrderIn">
                      <section className="col-md-9 col-sm-9 flex-item">
                        <article className="panel boxPadding posR">
                          <article className="data-row">
                            <h2>What is Ikigai Scholarship 2019?</h2>
                            <div>
                              <p>
                                The Ikigai Scholarship empowers outstanding
                                students in India to pursue college degrees and
                                certification programs to develop their
                                professional abilities. The scholarship is open
                                to a broad range of disciplines across science &
                                engineering, arts, sports and many more; and
                                provides tuition and lodging expenses along with
                                a stipend over the duration of eligible
                                programs. Ikigai Scholars strive to not only
                                excel in their chosen fields but also to become
                                socially responsible and financially
                                independent.
                              </p>
                            </div>
                          </article>

                          <article className="data-row">
                            <h2>Who can apply for this scholarship ?</h2>
                            <div>
                              <p>
                                To be eligible for the Ikigai scholarship 2019,
                                applicants must meet each of the following
                                criteria:
                              </p>

                              <ul>
                                <li>
                                  Current or former student of Pardada Pardadi
                                  Educational Society
                                </li>
                                <li>
                                  Currently pursuing Class 12 or completed Class
                                  12 in 2018
                                </li>
                                <li>
                                  Scored a minimum of 60% marks in the last
                                  examination
                                </li>
                                <li>
                                  Demonstrated high performance in at least one
                                  subject/field/sport
                                </li>
                                <li>
                                  Obtained admission or applying for a college
                                  degree or certification program
                                </li>
                              </ul>
                            </div>
                          </article>
                          <article className="data-row">
                            <h2>What are the benefits ?</h2>
                            <div>
                              <p>
                                Up to 4 students will be selected for the Ikigai
                                scholarship in 2019. Ikigai scholars will
                                receive the following benefits over the entire
                                course of the program:
                              </p>

                              <ul>
                                <li>
                                  Up to <strong>INR 1.0 lakhs per annum</strong>{" "}
                                  towards tuition fee, admission fee and hostel
                                  fee
                                </li>
                                <li>
                                  <strong>INR 1,500 per month</strong> towards
                                  personal expenses
                                </li>
                              </ul>
                            </div>
                          </article>

                          <article className="data-row">
                            <h2>
                              What are the documents required for the Ikigai
                              Scholarship 2019?
                            </h2>
                            <div>
                              <ul>
                                <li>
                                  Student ID proof (ID Card/Bonafide Student
                                  Certificate/Letter from School)
                                </li>
                                <li>
                                  Address Proof/Domicile/Residential
                                  certificate/Telephone bill/ Ration Card
                                </li>
                                <li>Birth certificate</li>
                                <li>11th mark sheet</li>
                                <li>
                                  12th Pre-board mark sheet (or 12th mark sheet
                                  if Class 12th Passed)
                                </li>
                                <li>Admission letter (if available)</li>
                              </ul>
                            </div>
                          </article>
                          <article className="data-row">
                            <h2>Who is offering this scholarship ?</h2>
                            <div>
                              <p>
                                The scholarship is offered by a group of
                                professionals who believe in equal opportunity
                                for all.
                              </p>
                            </div>
                          </article>

                          <article className="data-row">
                            <article className="col-md-6 nopadding">
                              <h2>Important Links</h2>
                              <ul className="optional-links-text">
                                <li>
                                  <a
                                    href="https://buddy4study.com/application/IS73/instruction?block=1"
                                    target="_blank"
                                  >
                                    <strong>Apply online link</strong>
                                  </a>
                                </li>
                              </ul>
                            </article>
                            <article className="col-md-6 nopadding">
                              <h2>Contact details</h2>
                              <article>
                                <p>Email: info@buddy4study.com</p>
                              </article>
                            </article>
                          </article>
                          <article className="data-row">
                            <article className="matchingscho">
                              <article className="col-md-6 nopadding">
                                <a
                                  href="https://buddy4study.com/application/IS73/instruction?block=1"
                                  target="_blank"
                                >
                                  <button>Apply Now</button>
                                </a>
                              </article>
                            </article>
                          </article>
                        </article>
                        <article className="panel boxPadding disclaimer">
                          <article className="data-row">
                            <h2>Disclaimer</h2>
                            <p>
                              All the information provided here is for reference
                              purpose only. While we strive to list all
                              scholarships for benefit of students, Buddy4Study
                              does not guarantee the accuracy of the data
                              published here. For official information, please
                              refer to the official website.{" "}
                              <a href="/disclaimer">read more</a>
                            </p>
                          </article>
                        </article>
                      </section>
                      <section className="col-md-3 col-sm-3 flex-item">
                        <section className="panel ">
                          <section className="left-cell-1">
                            <img
                              property="image"
                              src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/scholarship_logo/logo_cd412eff-471c-49f0-b5dc-b7f69369d706_Pardada.png"
                              alt="IKIGAI Scholarship Program 2018-19"
                              title=""
                              className="school-logo"
                            />

                            <article className="deadlineBox">
                              <dd>
                                <span>Deadline:</span> 31-March-2019
                              </dd>
                              <a
                                href="https://buddy4study.com/application/IS73/instruction?block=1"
                                target="_blank"
                              >
                                <button>Apply Now</button>
                              </a>
                            </article>
                          </section>
                          <section className="left-cell-2">
                            <article className="cellBox">
                              <article>
                                <dd>
                                  <i>Award</i>Up to INR 1.18 lakhs per annum
                                </dd>
                                <dd>
                                  <i>Eligibility</i>Class 12 pursuing/passed
                                  students
                                </dd>
                                <dd>
                                  <i>Region</i>India
                                  <li>All India</li>
                                </dd>
                              </article>
                            </article>
                          </section>
                        </section>
                      </section>
                    </article>
                  </article>
                </section>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default ikiGai;
