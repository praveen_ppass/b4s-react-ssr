import React, { Component } from "react";
import { Route, Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import moment from "moment";
import AlertMessage from "../../../../src/shared/common/components/alertMsg";
import {
  getAllSummery as getAllSummeryAction,
  fetchSchApply as fetchSchApplyAction
} from "../../application/actions/applicationSummeryAction";
import {
  fetchPresentClass as fetchPresentClassAction,
  PRESENT_CLASS_INFO_SUCCESS
} from "../../application/actions/educationInfoAction";
import gblFunc from "../../../globals/globalFunctions";
import { smsApplicationDetailConfig } from "../../../shared/application/formconfig";
import Loader from "../../../shared/common/components/loader";

class Summary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      documentDetails: [],
      bankDetails: null,
      questionsDetails: null,
      summaryFetching: true,
      isRedirect: false,
      msg: "",
      status: "",
      showLoader: false,
      isCompleted: false,
      decHtml: "",
      data: []
    };
    // this.rec = this.rec.bind(this);
    this.rec2 = this.rec2.bind(this);
    this.rec3 = this.rec3.bind(this);
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this.onEditApplicationHandler = this.onEditApplicationHandler.bind(this);
  }
  // gblFunc.getStoreApplicationScholarshipId()
  componentDidMount() {
    if (this.props.history.location.state &&
      this.props.history.location.state.bsid
    ) {
      // let scholarshipId = "";
      // if (window != undefined) {
      //   scholarshipId = parseInt(
      //     window.localStorage.getItem("applicationSchID")
      //   );
      // }
      // this.props.getAllSummery({
      //   scholarshipId: scholarshipId,
      //   presentClass: 11
      // });
      const scholarshipId = gblFunc.getStoreApplicationScholarshipId();
      if (scholarshipId) {
        this.props.fetchPresentClass(scholarshipId);
      }
    } else {
      this.props.history.push(
        `/application/${this.props.match.params.bsid.toUpperCase()}/instruction`
      );
    }
  }
  componentWillReceiveProps(nextProps) {
    const { type } = nextProps.applicationSummery;
    switch (type) {
      case "FETCH_SCHOLARSHIP_APPLY_SUCCESS_SUMMARY":
        this.setState({
          isRedirect: true
        });
        break;
      case "FETCH_SCHOLARSHIP_APPLY_FAILURE_SUMMARY":
        this.setState({
          status: false,
          msg: "Sorry!! Server Error",
          showLoader: true
        });
        break;

      case "GET_ALL_SUMMERY_RULES_SUCCESS":
        // this.setState(
        //   {
        //     personalDetails:
        //       nextProps.applicationSummery.summeryData.summary.personalInfo,
        //     educationDetails:
        //       nextProps.applicationSummery.summeryData.summary.educationInfos,
        //     familyDetails:
        //       nextProps.applicationSummery.summeryData.summary.familyInfos,
        //     // documentDetails:
        //     //   nextProps.applicationSummery.summeryData.summary
        //     //     .userScholarshipDocuments,
        //     bankDetails:
        //       nextProps.applicationSummery.summeryData.summary.bankDetails,
        //     questionsDetails:
        //       nextProps.applicationSummery.summeryData.summary
        //         .scholarshipQuestions,
        //     summaryFetching: false
        //   },
        //   () =>
        //     console.log(
        //       "Updated the state",
        //       nextProps.applicationSummery.summeryData.summary
        //     ),
        this.rec2(
          nextProps.applicationSummery.summeryData.summary.stepSummary
        ) &&
          this.rec3(nextProps.applicationSummery.summeryData.summary.documents);
        // );

        break;
      default:
        break;
    }
    if (
      (type == undefined || type == "FETCH_APPLICATION_STEP_SUCCESS") &&
      nextProps.type1 === PRESENT_CLASS_INFO_SUCCESS
    ) {
      if (
        nextProps.presentClassInfo &&
        nextProps.presentClassInfo.presentClass
        // nextProps.presentClassInfo.length > 0 &&
        // nextProps.presentClassInfo[0].userAcademicInfo &&
        // nextProps.presentClassInfo[0].userAcademicInfo.academicClass
      ) {
        // return this.setState({ verifiedPresentClass: nextProps.presentClassInfo.presentClass },)
        let scholarshipId = "";
        if (window != undefined) {
          scholarshipId = parseInt(
            window.localStorage.getItem("applicationSchID")
          );
        }
        const rsType = gblFunc.getStoreHulAuthType("hulType");
        const hul3PUser = (rsType == "3P");
        this.props.getAllSummery({
          scholarshipId: scholarshipId,
          presentClass: nextProps.presentClassInfo.presentClass,
          hul3PUser
        });
      }
    }
  }

  onEditApplicationHandler() {
    this.props.history.replace(
      `/application/${this.props.match.params.bsid.toUpperCase()}/form/personalInfo`
    );
  }

  onSubmitHandler() {
    let scholarshipId = "";
    if (window != undefined) {
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    // this.setState({ submitted: false }, () =>
    //   this.props.fetchSchApply({
    //     scholarshipId: scholarshipId
    //   })
    // );
    this.props.fetchSchApply({
      scholarshipId: scholarshipId
    });
  }
  rec2(fo) {
    var f = fo;

    var data = [],
      data3 = [];
    const that = this;
    // this.rec=this.rec2.rec.bind(this);

    function rec(f) {
      for (var k in f) {
        for (var key in f[k]) {
          if (f[k].hasOwnProperty(key)) {
            if (key == "heading") {
              data.push({ label: key, value: f[k][key] });
            }
            if (key == "data") {
              if (Array.isArray(f[k][key])) {
                f[k][key].map((item, index2) => {
                  data.push({ label: item.label, value: item.value });
                });
              } else {
                for (var key2 in f[k][key]) {
                  if (f[k][key].hasOwnProperty(key2)) {
                    if (Array.isArray(f[k][key][key2])) {
                      f[k][key][key2].map((item, index2) => {
                        // this.setState(prevState => ({
                        //     data: [...prevState.data,{"label":item.label,"value":item.value}]
                        //   }))
                        data.push({ label: item.label, value: item.value });
                      });
                    } else {
                      // data.push({"label":"Subheading","value":"awai"})
                      rec(f[k][key][key2]);
                    }
                  }
                }
              }
            }
          }
        }
      }
      // rec().then((data)=>{
      //   this.setState(prevState => ({
      //     data: [...prevState.data,data]
      //   }))

      // })
      that.setState({
        data: data
      });

      return data;
    }
    rec.bind(this);
    return rec(f);
  }

  rec3(fo) {
    var f = fo;

    var data = [],
      data3 = [];
    const that = this;
    // this.rec=this.rec2.rec.bind(this);

    function rec(f) {
      // for (var k in f) {
      for (var key in f) {
        if (f.hasOwnProperty(key)) {
          if (key == "heading") {
            data.push({ label: key, value: f[key] });
          }
          if (key == "data") {
            if (Array.isArray(f[key])) {
              f[key].map((item, index2) => {
                data.push({ label: item.label, value: item.value });
              });
            } else {
              for (var key2 in f[key]) {
                if (f[key].hasOwnProperty(key2)) {
                  if (Array.isArray(f[key][key2])) {
                    f[key][key2].map((item, index2) => {
                      // this.setState(prevState => ({
                      //     data: [...prevState.data,{"label":item.label,"value":item.value}]
                      //   }))
                      data.push({ label: item.label, value: item.value });
                    });
                  } else {
                    // data.push({"label":"Subheading","value":"awai"})
                    rec(f[key][key2]);
                  }
                }
              }
            }
          }
        }
      }
      // }
      // rec().then((data)=>{
      //   this.setState(prevState => ({
      //     data: [...prevState.data,data]
      //   }))

      // })
      that.setState(
        {
          documentDetails: data
        });

      return data;
    }
    rec.bind(this);
    return rec(f);
  }

  close() {
    // if (this.state.redirectToEducation) {
    //   this.setState({ redirectToEducation: false }, () =>
    //     this.props.history.push(
    //       `/application/${this.props.match.params.bsid.toUpperCase()}/form/educationInfo`
    //     )
    //   );
    // } else {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
    // }
  }

  render() {
    const { match, applicationSummery, location, history } = this.props;
    const {
      documentDetails,
      isRedirect
    } = this.state;
    let isSubmitArray = [];
    let redirecTo = null;
    if (!gblFunc.getCscStoreUserDetails()) {
      this.props.history.push(
        `/application/${match.params.bsid.toUpperCase()}/instruction`
      );
      return;
    }
    if (
      isRedirect &&
      gblFunc.getStoreUserDetails()["vleUser"] == "true"
    ) {
      redirecTo = <Redirect to={`/vle/student-list`} />;
    } else if (
      isRedirect &&
      ["CBS3", "CBI2"].includes(location.state.bsid)
    ) {
      redirecTo = (
        <Redirect
          to={`/payment/${location.state.bsid}/collegeboard`}
        />
      );
    } else if (
      isRedirect &&
      ["PMES01"].includes(location.state.bsid.toUpperCase())
    ) {
      redirecTo = (
        <Redirect to={`/payment/${location.state.bsid}/pearson`} />
      );
    } else if (isRedirect) {
      redirecTo = (
        <Redirect
          to={`/${location.state.bsid.toUpperCase()}/application-submitted`}
        />
      );
    } else if (
      isRedirect &&
      ["SKKS1", "CES7", "CES8", "SCE3", "CKS1", "SSE4", "HUL1", "FAL9", "HCL2"].includes(
        location.state.bsid.toUpperCase()
      )
    ) {
      redirecTo = (
        <Redirect
          to={{
            pathname: `/application/${ocation.state.bsid.toUpperCase()}/instruction`,
            state: {
              from: "summary",
              isChildForm:
                childEduBSID[location.state.bsid.toUpperCase()] ||
                  ["FAL9"].includes(location.state.bsid.toUpperCase())
                  ? true
                  : false
            }
          }}
        />
      );
    } else if (isRedirect) {
      redirecTo = (
        <Redirect
          to={`/${location.state.bsid.toUpperCase()}/application-submitted`}
        />
      );
    }
    //  else if (
    //   isRedirect &&
    //   this.props.instructions.redirectUrl != null
    // ) {
    //   if (
    //     this.props.instructions.redirectUrl.includes("http") ||
    //     this.props.instructions.redirectUrl.includes("https")
    //   ) {
    //     window.open(this.props.instructions.redirectUrl, "_blank");
    //   } else {
    //     redirecTo = <Redirect to={this.props.instructions.redirectUrl} />;
    //   }
    // }
    else if (isRedirect) {
      redirecTo = <Redirect to={`/myscholarship/?tab=application-status`} />;
    }

    // const stepsArray=["Personal Info step","Education info step","Family Info step","Reference Info step","Bank Info step","Family Info step"]
    if (applicationSummery.summeryData) {
      var stepsArray = applicationSummery.summeryData.summary.applicableSteps;
    }
    return (
      <section className="summary">
        {!smsApplicationDetailConfig.isSMSAdmin() && redirecTo}
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        {/* <section className="banner">
          <section className="container-fluid">
            <section className="container">
              <section className="row">
                <section className="col-md-12">
                  <Link to={"/"}>
                    <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/white-logo.png" />
                  </Link>
                </section>
              </section>
            </section>
          </section>
        </section> */}
        <section className="container-fluid equial-padding">
          <section className="container">
            <section className="row">
              <section className="col-md-12">
                {

                  applicationSummery &&
                    applicationSummery.summeryData &&
                    applicationSummery.summeryData.summary &&
                    applicationSummery.summeryData.summary.scholarshipDetail ?
                    <article className="scholarshipScheme">
                      <article className="summary-logo pull-left">
                        <img
                          src={
                            applicationSummery.summeryData.summary.scholarshipDetail.logoUrl
                              ? applicationSummery.summeryData.summary.scholarshipDetail.logoUrl
                              : ""
                          }
                          className="img-responsive"
                        />
                      </article>
                      <article className="display-content">
                        <h1>
                          {applicationSummery.summeryData.summary.scholarshipDetail.title
                            ? applicationSummery.summeryData.summary.scholarshipDetail.title
                            : ""}
                        </h1>
                        <article className="date">
                          <dd>
                            <span>
                              Deadline &nbsp;<i className="fa fa-calendar"></i>&nbsp;
                          &nbsp;
                          {applicationSummery.summeryData.summary.scholarshipDetail.deadline
                                ? applicationSummery.summeryData.summary.scholarshipDetail.deadline
                                : ""}
                            </span>
                          </dd>
                        </article>
                      </article>
                    </article>
                    : ""
                }
              </section>
            </section>
          </section>
        </section>
        <section className="container-fluid">
          <section className="container">
            <section className="row">
              <section className="col-md-12">
                <article className="details">
                  {/* <button onClick={()=>this.rec2(this.state.poc.summary)}>Click</button> */}
                  {/* applicationSummery.summeryData.summary */}
                  {/* <button onClick={()=>this.rec2(this.props.applicationSummery.summeryData.summary.stepSummary)}>Click</button> */}

                  {/* <h2>Personal Details</h2> */}
                  {/* <article></article> */}
                  {/* <article className="col-md-12"> */}
                  {this.state.data
                    ? this.state.data.map((item, index) =>
                      item.label == "heading" ? (
                        //  <div className="row col-md-12">
                        stepsArray.includes(item.value) ? (
                          <div className="col-md-12" key={"data_" + index}>
                            <h2>{item.value}</h2>
                          </div>
                        ) : (
                          <div className="col-md-12">
                            <h3>{item.value}</h3>
                          </div>
                        )
                      ) : (
                        //  </div>
                        // item.label="Subheading"?<h3>{item.value}</h3>:(
                        // <article className="col-md-12">

                        <article className="form-label col-md-3 col-sm-6 col-xs-12">
                          <p>{item.label}</p>
                          <span>{item.value}</span>
                        </article>
                        // </article>

                        // )
                      )
                    )
                    : ""}

                  {/* </article> */}
                </article>
              </section>
            </section>
          </section>

          <section className="container">
            <section className="row">
              <section className="col-md-12">
                <article className="details">
                  {/* <button onClick={()=>this.rec2(this.state.poc.summary)}>Click</button> */}
                  {/* applicationSummery.summeryData.summary */}
                  {/* <button onClick={()=>this.rec2(this.props.applicationSummery.summeryData.summary.stepSummary)}>Click</button> */}

                  {/* <h2>Personal Details</h2> */}
                  {/* <article></article> */}
                  {/* <article className="col-md-12"> */}
                  {documentDetails
                    ? documentDetails.map((item, index) =>
                      item.label == "heading" ? (
                        //  <div className="row col-md-12">
                        stepsArray.includes(item.value) ? (
                          <div className="col-md-12" key={'doc_' + index}>
                            <h2>{item.value}</h2>
                          </div>
                        ) : (
                          <div className="col-md-12">
                            <h3>{item.value}</h3>
                          </div>
                        )
                      ) : (
                        //  </div>
                        // item.label="Subheading"?<h3>{item.value}</h3>:(
                        // <article className="col-md-12">

                        <article className="customize">
                          <section className="row">
                            <section className="col-md-9">
                              {item.label}
                            </section>
                            <section className="col-md-3">
                              <article className="uploadgp">
                                <span
                                  className={
                                    item.value == "UPLOADED"
                                      ? "upload"
                                      : "upload missing"
                                  }
                                >
                                  {item.value}
                                </span>
                              </article>
                            </section>
                          </section>
                        </article>
                        // </article>

                        // )
                      )
                    )
                    : ""}

                  {/* </article> */}
                </article>
              </section>
            </section>
          </section>

          {/* <section className="container">
          <section className="whitebg">
            <section className="row">
              <h2>Documents Details</h2>
              {documentDetails && documentDetails.length > 0 && documentDetails.map(d => (<article className="col-md-12 pt20">
                <article className="pull-left text">{d.label}</article>
                <article className="pull-right">
                  <span className={d.value!="Missing" ? "upload-btn" : "upload-btn error"}>Uploaded</span>
                </article>
              </article>))}
            </section>
          </section>
        </section> */}

          {
            applicationSummery && applicationSummery.summeryData ?
              <section className="container">
                <section className="row">
                  <section className="col-md-12">
                    <article className="details">
                      <h2>
                        {applicationSummery.summeryData.summary && applicationSummery.summeryData.summary.questions ? applicationSummery.summeryData.summary.questions.heading
                          : ""}
                      </h2>
                      <article className="col-md-12">
                        {applicationSummery.summeryData.summary && applicationSummery.summeryData.summary.questions && applicationSummery.summeryData.summary.questions.data.labelAndValue &&
                          applicationSummery.summeryData.summary.questions.data.labelAndValue.map(
                            (d, i) => {
                              return (
                                <article className="textlopp" key={i}>
                                  <p>{d.label}</p>
                                  <span>Response: {d.value ? d.value : ""}</span>
                                </article>

                              )
                            }
                          )}
                      </article>
                    </article>
                  </section>
                </section>
              </section>
              : ""
          }
          <section className="container">
            <section className="row">
              <article className="col-md-12">
                {history && history.location && history.location.state && history.location.state.bsid && (!history.location.state.editBtn || (history.location.state.bsid == "CBI2" || history.location.state.bsid == "CBS3")) &&
                  <article className="status">
                    <p style={{ background: '#ffffff', padding: '1%', color: 'red', fontSize: '13px', textAlign: 'left' }}>Dear student, You can not edit the details after submission. Please check and verify your summary details. If all the entries are correct then submit the application.Without submission your application will not be qualified for next round.</p>

                    {/* Application Status: <span>Submitted</span> */}
                    <button
                      onClick={this.onEditApplicationHandler}
                      className="btn"
                    >
                      Edit Application Form
                  </button>
                    <button onClick={this.onSubmitHandler} className="btn green">
                      Submit
                  </button>
                  </article>
                }
              </article>
            </section>
          </section>

        </section>
      </section>
    );
  }
}

const mapStateToProps = ({ applicationSummery, applicationEducation }) => ({
  applicationSummery: applicationSummery,
  showLoader: applicationSummery.showLoader,
  type1: applicationEducation.type,
  presentClassInfo: applicationEducation.presentClassInfo
});

const mapDispatchToProps = dispatch => ({
  getAllSummery: input => dispatch(getAllSummeryAction(input)),
  fetchSchApply: inputData => dispatch(fetchSchApplyAction(inputData)),
  fetchPresentClass: inputData => dispatch(fetchPresentClassAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Summary);
