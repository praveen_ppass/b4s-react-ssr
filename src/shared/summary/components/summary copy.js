import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import moment from "moment";
import {
  getAllSummery as getAllSummeryAction,
  fetchSchApply as fetchSchApplyAction
} from "../../application/actions/applicationSummeryAction";
import gblFunc from "../../../globals/globalFunctions";
class Summary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      personalDetails: null,
      educationDetails: null,
      familyDetails: null,
      documentDetails: null,
      bankDetails: null,
      questionsDetails: null,
      summaryFetching: true,
      data: [],
      poc: {
        summary: {
          personal: {
            heading: "Personal Details",
            data: {
              labelAndValue: [
                {
                  label: "First Name*",
                  value: "TBI"
                },
                {
                  label: "DOB* (DD/MM/YYYY)",
                  value: "1888-11-22"
                },
                {
                  label: "Annual Family Income*",
                  value: 56546
                },
                {
                  label: "Email*",
                  value: "ravi.kant@buddy4study.com"
                },
                {
                  label: "Mobile*",
                  value: "--missing--"
                },
                {
                  label: "Alternate Mobile*",
                  value: 8979798799
                }
              ],
              nested: {
                userAddress: {
                  heading: "yKm2XG",
                  data: {
                    labelAndValue: [
                      {
                        label: "Address*",
                        value: "hegde the following the same 3"
                      },
                      {
                        label: "City*",
                        value: "Noida"
                      },
                      {
                        label: "District*",
                        value: "Chirang"
                      },
                      {
                        label: "Pincode*",
                        value: "201556"
                      },
                      {
                        label: "State*",
                        value: "Assam"
                      }
                    ]
                  }
                }
              }
            }
          }
        }
      }
    };
    // this.rec = this.rec.bind(this);
    this.rec2 = this.rec2.bind(this);
  }
  // gblFunc.getStoreApplicationScholarshipId()
  componentDidMount() {
    this.props.getAllSummery({
      scholarshipId: "17156",
      presentClass: 11
    });
  }
  componentWillReceiveProps(nextProps) {
    if (
      this.state.summaryFetching &&
      nextProps.applicationSummery &&
      nextProps.applicationSummery.type === "GET_ALL_SUMMERY_RULES_SUCCESS" &&
      nextProps.applicationSummery.summeryData &&
      nextProps.applicationSummery.summeryData.summary
    ) {
      this.setState(
        {
          personalDetails:
            nextProps.applicationSummery.summeryData.summary.personalInfo,
          educationDetails:
            nextProps.applicationSummery.summeryData.summary.educationInfos,
          familyDetails:
            nextProps.applicationSummery.summeryData.summary.familyInfos,
          documentDetails:
            nextProps.applicationSummery.summeryData.summary
              .userScholarshipDocuments,
          bankDetails:
            nextProps.applicationSummery.summeryData.summary.bankDetails,
          questionsDetails:
            nextProps.applicationSummery.summeryData.summary
              .scholarshipQuestions,
          summaryFetching: false
        },
        () =>
          console.log(
            "Updated the state",
            nextProps.applicationSummery.summeryData.summary
          ),
        this.rec2(nextProps.applicationSummery.summeryData.summary.stepSummary)
      );
    }
  }
  rec2(fo) {
    var f = fo;

    var data = [],
      data3 = [];
    const that = this;
    // this.rec=this.rec2.rec.bind(this);

    function rec(f) {
      for (var k in f) {
        for (var key in f[k]) {
          if (f[k].hasOwnProperty(key)) {
            if (key == "heading") {
              data.push({ label: key, value: f[k][key] });
            }
            if (key == "data") {
              if (Array.isArray(f[k][key])) {
                f[k][key].map((item, index2) => {
                  data.push({ label: item.label, value: item.value });
                });
              } else {
                for (var key2 in f[k][key]) {
                  if (f[k][key].hasOwnProperty(key2)) {
                    if (Array.isArray(f[k][key][key2])) {
                      f[k][key][key2].map((item, index2) => {
                        // this.setState(prevState => ({
                        //     data: [...prevState.data,{"label":item.label,"value":item.value}]
                        //   }))
                        data.push({ label: item.label, value: item.value });
                      });
                    } else {
                      // data.push({"label":"Subheading","value":"awai"})
                      rec(f[k][key][key2]);
                    }
                  }
                }
              }
            }
          }
        }
      }
      // rec().then((data)=>{
      //   this.setState(prevState => ({
      //     data: [...prevState.data,data]
      //   }))

      // })
      that.setState({
        data: data
      });

      return data;
    }
    rec.bind(this);
    return rec(f);
  }

  render() {
    // const stepsArray=["Personal Info step","Education info step","Family Info step","Reference Info step","Bank Info step","Family Info step"]
    if (this.props.applicationSummery.summeryData) {
      var stepsArray = this.props.applicationSummery.summeryData.summary
        .applicableSteps;
    }
    const {
      personalDetails,
      educationDetails,
      familyDetails,
      documentDetails,
      bankDetails,
      questionsDetails
    } = this.state;
    return (
      <section className="summary">
        <section className="banner">
          <section className="container">
            <section className="row">
              <div className="col-md-12">
                <Link to={"/"}>
                  <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/white-logo.png" />
                </Link>
              </div>
            </section>
          </section>
        </section>
        {/* logo banner start here */}
        <section className="container">
          <section className="whitebg">
            <section className="row">
              <section className="col-md-12">
                <article className="summary-logo pull-left">
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/scholarship_logo/logo_17067_1570195843.jpg"
                    className="img-responsive"
                  />
                </article>
                <article className="pull-left">
                  <h1>
                    Technip India Scholarship Program for Class 11 Students
                  </h1>
                  <article class="date">
                    <dd>
                      <span>
                        Deadline &nbsp;<i class="fa fa-calendar"></i>&nbsp;
                        &nbsp;15-01-2020
                      </span>
                    </dd>
                  </article>
                </article>
              </section>
            </section>
          </section>
        </section>
        {/* logo banner end here */}

        {/* personal details start */}
        {/* <section className="container">
          <section className="whitebg">
            <section className="row">
              <button onClick={()=>this.rec2(this.state.poc.summary)}>Click</button>
              <h2>Personal Details</h2>
              {personalDetails && <article className="col-md-12">
                {(personalDetails.firstName || personalDetails.lastName) && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Name</p>
                  <span>{`${personalDetails.firstName? personalDetails.firstName : ''} ${personalDetails.lastName ? personalDetails.lastName : ''}`}</span>
                </article>}
                {personalDetails.email && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Email</p>
                  <span>{personalDetails.email}</span>
                </article>}
                {personalDetails.mobile && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Mobile</p>
                  <span>{`${personalDetails.countryCode ? personalDetails.countryCode : '+'} ${personalDetails.mobile}`}</span>
                </article>}
                {personalDetails.dob && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>DOB</p>
                  <span>{moment(personalDetails.dob, "YYYY-MM-DD").format("DD/MM/YYYY")}</span>
                </article>}
                {personalDetails.familyIncome && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Annual Family Income (INR)</p>
                  <span>{personalDetails.familyIncome}</span>
                </article>}
                {personalDetails.aadharCard && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Aadhar Card</p>
                  <span>{personalDetails.aadharCard}</span>
                </article>}
              </article>}
            </section>
          </section>
        </section> */}

        <section className="container">
          <section className="whitebg">
            <section className="row">
              {/* <button onClick={()=>this.rec2(this.state.poc.summary)}>Click</button> */}
              {/* applicationSummery.summeryData.summary */}
              {/* <button onClick={()=>this.rec2(this.props.applicationSummery.summeryData.summary.stepSummary)}>Click</button> */}

              {/* <h2>Personal Details</h2> */}
              {/* <article></article> */}
              {/* <article className="col-md-12"> */}
              {this.state.data
                ? this.state.data.map((item, index) =>
                    item.label == "heading" ? (
                      //  <div className="row col-md-12">
                      stepsArray.includes(item.value) ? (
                        <div className="col-md-12">
                          <h2>{item.value}</h2>
                        </div>
                      ) : (
                        <div className="col-md-12">
                          <h3>{item.value}</h3>
                        </div>
                      )
                    ) : (
                      //  </div>
                      // item.label="Subheading"?<h3>{item.value}</h3>:(
                      // <article className="col-md-12">

                      <article className="form-label col-md-3 col-sm-6 col-xs-12">
                        <p>{item.label}</p>
                        <span>{item.value}</span>
                      </article>
                      // </article>

                      // )
                    )
                  )
                : ""}

              {/* </article> */}
            </section>
          </section>
        </section>
        {/* personal details end */}

        {/* personal details start */}
        {/* <section className="container">
          <section className="whitebg">
            <section className="row">
              <h2>Education Details</h2>
              <article className="col-md-12 bottomborder">
                <h3>Class 12th Details</h3>
                <article className="form-label col-md-3 col-sm-6 col-xs-12 col-sm-6">
                  <p>Name</p>
                  <span>Gazala Fatima</span>
                </article>
                <article className="form-label col-md-3 col-sm-6 col-xs-12 col-sm-6">
                  <p>Email</p>
                  <span>gazala.fatima@gmail.com</span>
                </article>
                <article className="form-label col-md-3 col-sm-6 col-xs-12  col-sm-6">
                  <p>Mobile</p>
                  <span>+919911919214</span>
                </article>
                <article className="form-label col-md-3 col-sm-6 col-xs-12  col-sm-6">
                  <p>DOB</p>
                  <span>28/11/1996</span>
                </article>
                <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Annual Family Income (INR)</p>
                  <span>77K</span>
                </article>
                <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Name</p>
                  <span>Gazala Fatima</span>
                </article>
                <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Name</p>
                  <span>Gazala Fatima</span>
                </article>
              </article>
              
              <article className="col-md-12">
                <h3>Class 12th Details</h3>
                <article className="form-label col-md-3 col-sm-6 col-xs-12 col-sm-6">
                  <p>Name</p>
                  <span>Gazala Fatima</span>
                </article>
                <article className="form-label col-md-3 col-sm-6 col-xs-12 col-sm-6">
                  <p>Email</p>
                  <span>gazala.fatima@gmail.com</span>
                </article>
                <article className="form-label col-md-3 col-sm-6 col-xs-12  col-sm-6">
                  <p>Mobile</p>
                  <span>+919911919214</span>
                </article>
                <article className="form-label col-md-3 col-sm-6 col-xs-12  col-sm-6">
                  <p>DOB</p>
                  <span>28/11/1996</span>
                </article>
                <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Annual Family Income (INR)</p>
                  <span>77K</span>
                </article>
                <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Name</p>
                  <span>Gazala Fatima</span>
                </article>
                <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Name</p>
                  <span>Gazala Fatima</span>
                </article>
              </article>
            </section>
          </section>
        </section> */}
        {/* eduaction details end */}

        {/* documents details start */}
        {/* <section className="container">
          <section className="whitebg">
            <section className="row">
              <h2>Documents Details</h2>
              {documentDetails && documentDetails.length > 0 && documentDetails.map(d => (<article className="col-md-12 pt20">
                <article className="pull-left text">{d.docNameLabel}</article>
                <article className="pull-right">
                  <span className={d.verificationStatus ? "upload-btn" : "upload-btn error"}>Uploaded</span>
                </article>
              </article>))}
            </section>
          </section>
        </section> */}
        {/* doc details end */}
        {/* BAnk details start */}
        {/* <section className="container">
          <section className="whitebg">
            <section className="row">
              <h2>Bank Details</h2>
              {bankDetails && bankDetails.length > 0 && bankDetails.map(d => (<article className="col-md-12">
                {d.accountHolderName && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Account Holder Name</p>
                  <span>{d.accountHolderName}</span>
                </article>}
                {d.accountNumber && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Account No.</p>
                  <span>{d.accountNumber}</span>
                </article>}
                {d.bankName && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Bank Name</p>
                  <span>{d.bankName}</span>
                </article>}
                {d.ifscCode && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>IFSC Code</p>
                  <span>{d.ifscCode}</span>
                </article>}
                {d.branchName && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>Branh Name</p>
                  <span>{d.branchName}</span>
                </article>}
                {d.district && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>District</p>
                  <span>{d.district}</span>
                </article>}
                {d.state && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                  <p>State</p>
                  <span>{d.state}</span>
                </article>}
              </article>))}
            </section>
          </section>
        </section> */}
        {/* Bank details end */}
        {/* <section className="container">
          <section className="whitebg">
            <section className="row">
              <h2>Family Details</h2>
              {familyDetails && familyDetails.length > 0 && familyDetails.map(d => (
                <section className="whitebg">
                <section className="row">
                {d.name && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                <p>Name</p>
                <span>{d.name}</span>
              </article>}
              {d.mobile && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                <p>Mobile</p>
                <span>{d.mobile}</span>
              </article>}
              {d.alternateMobile && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                <p>Alternate Mobile Number</p>
                <span>{d.alternateMobile}</span>
              </article>}
              {d.email && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                <p>Email</p>
                <span>{d.email}</span>
              </article>}
              {d.relationName && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                <p>Relation</p>
                <span>{d.relationName}</span>
              </article>}
              {d.occupationName && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                <p>Occupation</p>
                <span>{d.occupationName}</span>
              </article>}
              {d.panNumber && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                <p>Pan Number</p>
                <span>{d.panNumber}</span>
              </article>}
              {d.employer && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                <p>Employer</p>
                <span>{d.employer}</span>
              </article>}
              {d.customData && d.customData.employeeId && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                <p>Employee Id</p>
                <span>{d.customData.employeeId}</span>
              </article>}
              {d.addressLine && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                <p>Address</p>
                <span>{d.addressLine}</span>
              </article>}
              {d.absoluteIncome && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                <p>Income</p>
                <span>{d.absoluteIncome}</span>
              </article>}
              {d.otherOccupation && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                <p>Other Occupation</p>
                <span>{d.otherOccupation}</span>
              </article>}
              {d.otherRelation && <article className="form-label col-md-3 col-sm-6 col-xs-12">
                <p>Other Relation</p>
                <span>{d.otherRelation}</span>
              </article>}
              </section>
              </section>
              ))}
            </section>
          </section>
        </section> */}
        <section className="container">
          <section className="whitebg">
            <section className="row">
              <h2>
                {this.props.applicationSummery.summeryData
                  ? this.props.applicationSummery.summeryData.summary.questions
                      .heading
                  : ""}
              </h2>
              <article className="col-md-12">
                {this.props.applicationSummery.summeryData &&
                  this.props.applicationSummery.summeryData.summary.questions
                    .data.labelAndValue &&
                  this.props.applicationSummery.summeryData.summary.questions.data.labelAndValue.map(
                    d => (
                      <article className="form-label col-md-12">
                        <p>{d.label}</p>
                        <span>Response: {d.value ? d.value : ""}</span>
                      </article>
                    )
                  )}
              </article>
            </section>
          </section>
        </section>
        <section className="container">
          <section className="row">
            <article className=" status col-md-12">
              <article className=" pull-right">
                Status: <span>Submitted</span>
              </article>
            </article>
          </section>
        </section>
      </section>
    );
  }
}

const mapStateToProps = ({ applicationSummery }) => ({
  applicationSummery: applicationSummery,
  showLoader: applicationSummery.showLoader
});

const mapDispatchToProps = dispatch => ({
  getAllSummery: input => dispatch(getAllSummeryAction(input)),
  fetchSchApply: inputData => dispatch(fetchSchApplyAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Summary);
