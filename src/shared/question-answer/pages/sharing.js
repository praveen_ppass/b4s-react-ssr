import {
  FacebookShareButton,
  TwitterShareButton,
  LinkedinShareButton,
  FacebookShareCount,
  LinkedinShareCount,
  WhatsappShareButton
} from "react-share";

import React from "react";

export const Sharing = props => {
  return props.id == props.uniqueId ? (
    <ul className={props.isAni ? "social-icons hide" : "social-icons show"}>
      <li>
        {" "}
        <FacebookShareButton url={props.pageUrl}>
          <i className="fa fa-facebook" aria-hidden="true" />
        </FacebookShareButton>
        {/* <a className="social-icon" href="">
          {" "}
          <i className="fa fa-facebook" />
        </a>{" "} */}
      </li>
      <li>
        <TwitterShareButton url={props.pageUrl}>
          <i className="fa fa-twitter" aria-hidden="true" />
        </TwitterShareButton>
      </li>
      <li>
        <LinkedinShareButton url={props.pageUrl}>
          <i className="fa fa-linkedin" aria-hidden="true" />
        </LinkedinShareButton>
      </li>
      {/* <li>
        {" "}
        <a className="social-icon" href="">
          {" "}
          <i className="fa fa-instagram" />
        </a>{" "}
      </li> */}
    </ul>
  ) : null;
};
