import React from "react";
import { Link } from "react-router-dom";

export const Tags = props => {
  const { tags } = props;
  let tag = null;
  if (tags && tags.length) {
    tag = tags.map(tag => (
      <li key={tag.id}>
        <Link
          to={{
            pathname: `/qna/categories/${tag.slug}`
          }}
        >
          {tag.text}
        </Link>
      </li>
    ));
  }
  return <ul className="tag">{tag}</ul>;
};
