import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Sharing } from "./sharing";
import { Tags } from "./tags";
import AutoSuggestSearch from "./search";
import gblFunc from "../../../globals/globalFunctions";
import { fromNow, imgBaseUrl } from "../../../constants/constants";
import Pagination from "rc-pagination";
import {
  POST_COMMENT_SUCCEEDED,
  DELETE_COMMENT_SUCCEEDED,
  SAVE_ANSWERS_TO_QUEST_SUCCEEDED,
  DELETE_ANSWER_SUCCEEDED,
  POST_ANSWER_SUCCEEDED
} from "../actions";
import { LOGIN_USER_SUCCEEDED } from "../../../shared/login/actions";

let CKEditor;

class SinglePageQuestion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAni: true,
      ITEMS_PER_PAGE: 5,
      currentPage: 1,
      uniqueId: null,
      replyState: {
        authorUserId: null,
        commentText: "",
        level: null,
        parentCommentId: null,
        qnaAnswerId: null
      },
      editState: {
        authorUserId: null,
        commentText: "",
        level: null,
        parentCommentId: null,
        qnaAnswerId: null,
        id: null
      },
      answereditState: {
        authorUserId: null,
        commentText: "",
        level: null,
        parentCommentId: null,
        qnaAnswerId: null,
        id: null,
        answerId: null,
        questionId: null
      },
      isReplyShowed: "",
      replyId: null,
      editId: null,
      isEditShowed: "",
      editCommentId: "",
      replyCommentId: "",
      editorMode: false,
      pendingEditor: false,
      editorMode1: -1,
      pendingEditor1: -1,
      editorModeAnswer: -1
    };
    this.socialIconHandler = this.socialIconHandler.bind(this);
    this.onPageChange = this.onPageChange.bind(this);
    this.onSubmitCommentHandler = this.onSubmitCommentHandler.bind(this);
    this.handleReplyToComment = this.handleReplyToComment.bind(this);
    this.onClickReplyHandler = this.onClickReplyHandler.bind(this);
    this.onClickReplyCancelHandler = this.onClickReplyCancelHandler.bind(this);
    this.onClickEditCommentHandler = this.onClickEditCommentHandler.bind(this);
    this.onClickEditCommentCancelHandler = this.onClickEditCommentCancelHandler.bind(this);
    this.onEditCommentHandler = this.onEditCommentHandler.bind(this);
    this.onEditAnswerHandler = this.onEditAnswerHandler.bind(this);
    this.handleReplyEnter = this.handleReplyEnter.bind(this);
    this.handleEditEnter = this.handleEditEnter.bind(this);
    this.changeEditor1State = this.changeEditor1State.bind(this);
    this.changeEditor1StateToCancel = this.changeEditor1StateToCancel.bind(this);
    this.changePendingModeState = this.changePendingModeState.bind(this);
    this.onClickDeleteCommentHandler = this.onClickDeleteCommentHandler.bind(this);
    this.onClickDeleteAnswerHandler = this.onClickDeleteAnswerHandler.bind(this);
    this.handleEditAnswerEnter = this.handleEditAnswerEnter.bind(this);
    this.handleEditAnswerCancel = this.handleEditAnswerCancel.bind(this);
    this.onSubmitAnswerHandler = this.onSubmitAnswerHandler.bind(this);
    this.OnChangeEditorModeAnswer = this.OnChangeEditorModeAnswer.bind(this);
    // this.onSubmitAnswerHandler = this.onSubmitAnswerHandler.bind(this);


  }
  componentDidMount() {


    if (window !== undefined) {
      if (!!this.props.location.search) {
        let search = this.props.location.search;
        let urlPath = window.location.href;
        urlPath = urlPath.replace(search, "");
        window.history.pushState({ path: urlPath }, "", urlPath);
      }
      CKEditor = require("ckeditor4-react");
    }
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    switch (type) {
      case DELETE_ANSWER_SUCCEEDED:
        this.setState(
          {
            answereditState: {
              authorUserId: null,
              commentText: "",
              level: null,
              parentCommentId: null,
              qnaAnswerId: null,
              id: null,
              answerId: null,
              questionId: null
            },
          },
          () => this.onPageChange(this.state.currentPage)
        );
        break;
      case POST_COMMENT_SUCCEEDED:
        this.setState(
          {
            replyState: {
              authorUserId: null,
              commentText: "",
              level: null,
              parentCommentId: null,
              qnaAnswerId: null
            },
            editState: {
              authorUserId: null,
              commentText: "",
              level: null,
              parentCommentId: null,
              qnaAnswerId: null,
              id: null
            },
            replyId: null,
            isReplyShowed: "",
            editId: null,
            isEditShowed: "",
            editCommentId: "",
            replyCommentId: "",
            editorMode1: -1
          },
          () => this.onPageChange(this.state.currentPage)
        );
        break;
      case SAVE_ANSWERS_TO_QUEST_SUCCEEDED:
        if (this.state.editorMode) {
          this.setState({ editorMode: false });
        }
        // if(this.state.editorMode1>){
        //   this.setState({
        //     editorMode1:false
        //   })
        // }
        break;
      case DELETE_COMMENT_SUCCEEDED:
        this.onPageChange(this.state.currentPage);
        break;
      case LOGIN_USER_SUCCEEDED:
        if (this.state.pendingEditor) {
          this.setState({ pendingEditor: false, editorMode: true });
        }
        // if(this.state.pendingEditor1){
        //   this.setState({
        //     pendingEditor1:false,
        //     editorMode1:true
        //   })
        // }
        break;
      case POST_ANSWER_SUCCEEDED:
        this.setState(
          {
            answereditState: {
              authorUserId: null,
              commentText: "",
              level: null,
              parentCommentId: null,
              qnaAnswerId: null,
              id: null,
              answerId: null,
              questionId: null
            },
            editorModeAnswer: -1
          },
          () => this.onPageChange(this.state.currentPage)
        );
        break;

    }
  }
  async OnChangeEditorModeAnswer(index, answer, props) {
    //  await this.setState(prevState => {
    //     let answereditState = Object.assign({}, prevState.answereditState);  // creating copy of state variable jasper
    //     answereditState.commentText = answer.answerText;                     // update the name property, assign a new value                 
    //     return { answereditState };                                 // return new object jasper object
    //   })
    await this.setState({
    })
    const updateTheEditComment = {
      ...this.state.updateTheEditComment,
      authorUserId: gblFunc.getStoreUserDetails()["userId"],
      qnaQuestionId: props.data.id,
      id: answer.id,
      status: 1
    };

    updateTheEditComment["answerText"] = answer.answerText;
    await this.setState({
      answereditState: updateTheEditComment,
      editorModeAnswer: index,

    });
  }
  socialIconHandler(id) {
    this.setState({
      isAni: !this.state.isAni,
      uniqueId: id
    });
  }

  onSubmitCommentHandler(e, type) {
    e.preventDefault();

    const isAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;

    if (isAuthenticated) {
      if (type == "EDIT") {
        const { editState } = this.state;
        this.props.saveComments(editState);
      } else {
        const { replyState } = this.state;
        this.props.saveComments(replyState);
      }
    } else {
      this.props.showLoginPopup();
    }
  }

  onSubmitAnswerHandler(e) {
    e.preventDefault();

    const isAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;

    if (isAuthenticated) {
      const { answereditState } = this.state;
      this.props.saveEditAnswer(answereditState);
    } else {
      this.props.showLoginPopup();
    }
  }

  getOffset(page) {
    return page - 1;
  }
  onPageChange(currentPage) {
    const { ITEMS_PER_PAGE } = this.state;
    const { getAnswerFromSPQ } = this.props;

    let page = this.getOffset(currentPage);

    const length = ITEMS_PER_PAGE;
    getAnswerFromSPQ({
      questionId: this.props.questionId,
      page,
      length
    });
    let urlPath = window.location.href;
    let urlNew = null;
    urlNew = urlPath.includes(`?`) ? urlPath.split(`?`) : null;
    window.history.pushState({ path: !!urlNew ? urlNew[0] + `?page=${page + 1}` : urlPath + `?page=${page + 1}` }, '', !!urlNew ? urlNew[0] + `?page=${page + 1}` : urlPath + `?page=${page + 1}`);
    this.setState({
      currentPage
    });
  }
  handleReplyToComment(target, commentObj, updateAnswerFromEditor = "",
    type = "") {
    const { parentCommentId, qnaAnswerId } = commentObj;
    if (type == "editor1") {
      const updateReplyState = {
        ...this.state.replyState,
        parentCommentId,
        qnaAnswerId,
        authorUserId: gblFunc.getStoreUserDetails()["userId"],
        level: 1
      };

      updateReplyState["commentText"] = updateAnswerFromEditor;
      this.setState({
        replyState: updateReplyState
      });

    }
  }

  handleReplyEnter(e) {
    // if (e.key === "Enter" && !e.shiftKey) {
    // e.preventDefault();
    const { replyState } = this.state;
    if (
      replyState &&
      replyState.commentText &&
      replyState.commentText.trim().length > 0
    ) {
      this.onSubmitCommentHandler(e, "REPLY");
    }
    // }
  }

  onClickReplyHandler({ id, authorUserId }) {
    this.setState({
      isReplyShowed: authorUserId + id,
      replyId: id,
      editId: null,
      isEditShowed: "",
      editState: {
        authorUserId: null,
        commentText: "",
        level: null,
        parentCommentId: null,
        qnaAnswerId: null,
        id: null
      },
      replyCommentId: authorUserId + id
    });
  }

  onClickReplyCancelHandler(e) {
    this.setState({
      isReplyShowed: "",
      replyId: "",
      editId: null,
      isEditShowed: "",
      editState: {
        authorUserId: null,
        commentText: "",
        level: null,
        parentCommentId: null,
        qnaAnswerId: null,
        id: null
      },
      replyCommentId: ""
    });
  }

  onClickEditCommentHandler(comment, id) {
    let updateEditState = { ...this.state.editState };

    updateEditState = comment;
    this.setState({
      isEditShowed: comment.authorUserId + comment.id,
      editId: id,
      editState: updateEditState,
      isReplyShowed: "",
      replyId: null,
      replyState: {
        authorUserId: null,
        commentText: "",
        level: null,
        parentCommentId: null,
        qnaAnswerId: null
      },
      editCommentId: comment.authorUserId + comment.id
    });
  }

  onClickEditCommentCancelHandler(e) {
    // let updateEditState = { ...this.state.editState };

    // updateEditState = comment;
    this.setState({
      isEditShowed: "",
      editId: "",
      editState: "",
      isReplyShowed: "",
      replyId: null,
      replyState: {
        authorUserId: null,
        commentText: "",
        level: null,
        parentCommentId: null,
        qnaAnswerId: null
      },
      editCommentId: "",
    });
  }

  onClickDeleteCommentHandler(id) {
    this.props.deletedComment({ commentId: id });
  }
  onClickDeleteAnswerHandler(id) {
    this.props.deleteAnswer({ answerId: id });
  }

  onEditCommentHandler({ target }, editObj, updateAnswerFromEditor = "", type = "") {
    //commentText
    const { authorUserId, id, parentCommentId, qnaAnswerId, level } = editObj;
    if (type = "editor2") {
      const updateTheEditComment = {
        ...this.state.editState,
        authorUserId: gblFunc.getStoreUserDetails()["userId"],
        id,
        parentCommentId,
        qnaAnswerId,
        level
      };

      updateTheEditComment["commentText"] = updateAnswerFromEditor;
      // updateReplyState["commentText"] = updateAnswerFromEditor;


      this.setState({
        editState: updateTheEditComment
      });

    }

  }

  onEditAnswerHandler({ target }, editObj, updateAnswerFromEditor = "", type = "", questionId, answerId) {
    //commentText
    if (type = "editor3") {
      const updateTheEditComment = {
        ...this.state.updateTheEditComment,
        authorUserId: gblFunc.getStoreUserDetails()["userId"],
        qnaQuestionId: questionId,
        id: answerId,
        status: 1
      };

      updateTheEditComment["answerText"] = updateAnswerFromEditor;
      this.setState({
        answereditState: updateTheEditComment
      });

    }

  }


  handleEditEnter(e) {
    // if (e.key === "Enter" && !e.shiftKey) {
    // e.preventDefault();
    const { editState } = this.state;
    if (
      editState &&
      editState.commentText &&
      editState.commentText.trim().length > 0
    ) {
      this.onSubmitCommentHandler(e, "EDIT");
    }
    // }
  }

  handleEditAnswerEnter(e) {
    // if (e.key === "Enter" && !e.shiftKey) {
    // e.preventDefault();
    const { answereditState } = this.state;
    if (
      answereditState &&
      answereditState.commentText &&
      answereditState.commentText.trim().length > 0
    ) {
      this.onSubmitAnswerHandler(e);
    }
    // }
  }

  async handleEditAnswerCancel(e) {
    // if (e.key === "Enter" && !e.shiftKey) {
    // e.preventDefault();
    const { answereditState } = this.state;
    await this.setState({
      ...this.state,
      editorModeAnswer: -1,
      answereditState: {
        authorUserId: null,
        commentText: "",
        level: null,
        parentCommentId: null,
        qnaAnswerId: null,
        id: null,
        answerId: null,
        questionId: null
      },

    })
    // }
  }


  changeEditor1State(index) {
    this.setState({
      editorMode1: index
    })
  }

  changeEditor1StateToCancel() {
    this.setState({
      editorMode1: -1
    })
  }
  changePendingModeState(index) {
    this.setState({
      pendingEditor1: index
    })
  }

  render() {
    const {
      ITEMS_PER_PAGE,
      currentPage,
      editCommentId,
      replyCommentId
    } = this.state;
    const {
      socialIconHandler,
      onSubmitCommentHandler,
      onSubmitAnswerHandler,
      handleReplyToComment,
      onClickReplyHandler,
      onClickReplyCancelHandler,
      onClickEditCommentHandler,
      onClickEditCommentCancelHandler,
      onEditCommentHandler,
      onEditAnswerHandler,
      handleEditEnter,
      handleEditAnswerEnter,
      handleEditAnswerCancel,
      handleReplyEnter,
      onClickDeleteCommentHandler,
      onClickDeleteAnswerHandler,
      OnChangeEditorModeAnswer,
    } = this;
    const fns = {
      socialIconHandler,
      onSubmitCommentHandler,
      onSubmitAnswerHandler,
      onClickReplyHandler,
      onClickReplyCancelHandler,
      handleReplyToComment,
      onClickEditCommentHandler,
      onClickEditCommentCancelHandler,
      onEditCommentHandler,
      onEditAnswerHandler,
      handleEditEnter,
      handleEditAnswerEnter,
      handleEditAnswerCancel,
      handleReplyEnter,
      onClickDeleteCommentHandler,
      onClickDeleteAnswerHandler,
      OnChangeEditorModeAnswer
    };
    let data = null,
      tags = [],
      answersLength = 0,
      answers = [],
      relatedQuestions = [],
      likeTemplate,
      followTemplate;
    const userIdUser = gblFunc.getStoreUserDetails()["userId"];
    if (this.props.data && Object.keys(this.props.data).length) {
      data = this.props.data;
      tags = this.props.data.qnaCategories;
    }

    if (
      this.props.singlePageQuesAnswers &&
      this.props.singlePageQuesAnswers.data &&
      this.props.singlePageQuesAnswers.data.length > 0
    ) {
      answersLength = this.props.singlePageQuesAnswers.total;
      answers = this.props.singlePageQuesAnswers.data;
    }

    if (this.props.relatedQuestions && this.props.relatedQuestions.length) {
      relatedQuestions = this.props.relatedQuestions;
    }
    if (this.props.isAuthenticated && this.props.questionId) {
      likeTemplate = (
        <button
          onClick={() => this.props.onLikeFollowHandler("Like")}
          className={`btn-yellow paddingbtn ${
            this.props.isLiked ? "active" : ""
            }`}
        >
          {this.props.isLiked ? "Liked" : "Like"}
        </button>
      );
      followTemplate = (
        <button
          onClick={() => this.props.onLikeFollowHandler("Follow")}
          className={`btn-yellow paddingbtn ${
            this.props.isFollowed ? "active" : ""
            }`}
        >
          {this.props.isFollowed ? "Followed" : "Follow"}
        </button>
      );
    } else {
      likeTemplate = (
        <button
          onClick={this.props.onLoginPopUpHandler}
          className="btn-yellow paddingbtn"
        >
          Liked
        </button>
      );
      followTemplate = (
        <button
          onClick={this.props.onLoginPopUpHandler}
          className="btn-yellow paddingbtn"
        >
          Follow
        </button>
      );
    }

    return (
      <section className="qna qnacate">
        <section className="container-fluid qnaWrapper singlePage">
          <section className="container">
            <section className="row">
              <section className="col-md-8 col-sm-8 col-xs-12 ">
                <section className="search-wraper">
                  <form
                    onSubmit={this.props.onQuestionPopUpHandler}
                    autoCapitalize="off"
                  >
                    <input
                      type="text"
                      value={this.props.postQuestionVal}
                      name="search"
                      id="search"
                      placeholder="Type your question"
                      autoComplete={"off"}
                      onChange={this.props.onPostChangeHandler}
                      onFocus={this.props.onPostChangeHandler}
                    />
                    <button type="submit">
                      <span>POST</span>
                      <i className="fa fa-search" aria-hidden="true" />
                    </button>
                  </form>
                </section>
              </section>

              <section className="col-md-4 col-sm-4 col-xs-12 search-single">
                <article className="followWrapper">
                  {/* active */}
                  {followTemplate}
                  {likeTemplate}
                  <ul>
                    <li>
                      {" "}
                      {this.props.likeFollowCount.totalLiked
                        ? this.props.likeFollowCount.totalLiked + " Likes"
                        : this.props.likeFollowCount.totalLiked + " Like"}
                    </li>
                    <li>|</li>
                    <li>
                      {this.props.likeFollowCount.totalFollowed
                        ? this.props.likeFollowCount.totalFollowed + " Followed"
                        : this.props.likeFollowCount.totalFollowed + " Follow"}
                    </li>
                  </ul>
                </article>
                <AutoSuggestSearch {...this.props} />
              </section>
            </section>

            <section className="row">
              <section className="col-md-8 col-sm-8 col-xs-12 posReltv top">
                <article className="cateWrapper transparent">
                  <h4>
                    {data && data.questionText ? data.questionText : ""}
                    {data && data.authorName ?
                      <section>
                        <p className="author"><span>Posted by</span> {data.authorName} <span>on</span> {data.createdAt && data.createdAt.substr(0, data.createdAt.indexOf(' '))}</p>
                      </section>
                      : ""
                    }
                    {data && data.viewCount ? (
                      <div className="views">
                        <i className="fa fa-eye" aria-hidden="true" />
                        {data.viewCount + " views"}
                      </div>
                    ) : null}
                  </h4>

                  <Tags tags={tags} />
                  {/* <form
                    onSubmit={this.props.onPostAnswerSubmitHandler}
                    autoCapitalize="off"
                    className="formelemt"
                  > */}
                  <section className="ctrl-wrapper ckeditor">
                    <article className="form-group textAreaCrtl">
                      {!this.state.editorMode && (
                        <textarea
                          className="form-control"
                          // rows={`${
                          //   this.props.answerPost.answerText &&
                          //     this.props.answerPost.answerText.length > 0
                          //     ? "5"
                          //     : "1"
                          //   }`}
                          placeholder="Write your Answer"
                          id="answerText"
                          name="answerText"
                          required=""
                          // value={this.props.answerPost.answerText}
                          onFocus={() => {
                            const isAuthenticated =
                              gblFunc.isUserAuthenticated() ||
                              this.props.isAuthenticated;
                            if (isAuthenticated) {
                              this.setState({ editorMode: true });
                            } else {
                              this.setState({ pendingEditor: true }, () =>
                                this.props.showLoginPopup()
                              );
                            }
                          }}
                        />
                      )}
                      {window !== undefined && this.state.editorMode && (
                        <CKEditor
                          data={this.props.answerPost.answerText}
                          config={{
                            toolbar: [
                              ["Bold", "Italic", "Underline"],
                              ["Link"],
                              ["NumberedList", "BulletedList"]
                            ]
                          }}
                          // config={{
                          //   toolbar: [
                          //     [ 'Bold', 'Italic', 'Underline', 'Strike' ],
                          //     [ 'Cut', 'Copy' ],
                          //     [ 'Link', 'Unlink', 'Anchor' ],
                          //     [ 'Styles', 'Format', 'Font', 'FontSize' ],
                          //     [ 'TextColor', 'BGColor' ]
                          //   ]
                          // }}
                          onChange={e => {
                            this.props.onAnswerChangeHandler(
                              e,
                              this.props.questionId,
                              e.editor.getData(),
                              "editor"
                            );
                          }}
                        />
                      )}
                      {/* <span className="error animated bounce">
                          hehhehehhe
                        </span> */}
                    </article>
                  </section>
                  {this.state.editorMode ? (
                    <article className="postckeditor">
                      <input
                        type="button"
                        className="post"
                        value="Post"
                        onClick={this.props.onPostAnswerSubmitHandler}
                      />
                      <input
                        type="button"
                        className="post"
                        value="Cancel"
                        onClick={() => {

                          this.setState({ editorMode: false });

                        }}
                      />
                    </article>
                  ) : null}
                </article>

                <article className="headingWrapper">
                  <article className="heading">
                    <span>
                      <article className="textanswer">
                        {answersLength} Answer
                      </article>
                    </span>
                  </article>
                </article>

                {answers && answers.length ? (
                  <Answers
                    {...this.props}
                    answers={answers}
                    {...fns}
                    replyState={this.state.replyState}
                    isAni={this.state.isAni}
                    uniqueId={this.state.uniqueId}
                    isReplyShowed={this.state.isReplyShowed}
                    replyId={this.state.replyId}
                    isEditShowed={this.state.isEditShowed}
                    editId={this.state.editId}
                    editState={this.state.editState}
                    answereditState={this.state.answereditState}
                    questionText={data && data.questionText}
                    editCommentId={editCommentId}
                    replyCommentId={replyCommentId}
                    currentLoggedIn={gblFunc.getStoreUserDetails()["userId"]}
                    editorMode={this.state.editorMode1}
                    pendingEditor={this.state.pendingEditor1}
                    showLoginPopup={this.props.showLoginPopup}
                    changeEditor1State={this.changeEditor1State}
                    changeEditor1StateToCancel={this.changeEditor1StateToCancel}
                    changePendingModeState={this.changePendingModeState}
                    isAuthenticated={this.props.isAuthenticated}
                    userIdUser={userIdUser}
                    authorUserId={this.state.authorUserId}
                    editorModeAnswer={this.state.editorModeAnswer}

                  />
                ) : (
                    <article className="cateWrapper answer">
                      <p>
                        No information found. please go through other questions{" "}
                        <Link to="/qna">link</Link>.
                    </p>
                    </article>
                  )}

                {/* <a target="_blank" href="/" className="view">
                  View all comments
                </a> */}

                <article className="pagination">
                  {answersLength > 5 ? (
                    <Pagination
                      defaultPageSize={ITEMS_PER_PAGE}
                      pageSize={ITEMS_PER_PAGE}
                      defaultCurrent={1}
                      current={currentPage}
                      onChange={this.onPageChange}
                      total={answersLength}
                      showTitle={false}
                    />
                  ) : null}
                </article>
              </section>

              <section className="col-md-4 col-sm-4 col-xs-12">
                {relatedQuestions.length ? (
                  <article className="boxleft box">
                    <h4>Related Questions</h4>

                    {relatedQuestions.map(list => (
                      <p key={list.id}>
                        <Link
                          to={{
                            pathname: `/qna/${list.slug}`
                          }}
                        >
                          {list.questionText}
                        </Link>
                      </p>
                    ))}
                  </article>
                ) : (
                    ""
                  )}

                {/* <article className="boxleft box">
                  <h4>Related Articles</h4>

                  <p>
                    {" "}
                    My native place is Bihar, can I get admission to engineering
                    at Jharkhand state college
                  </p>
                  <p>
                    My native place is Bihar, can I get admission to engineering
                    at Jharkhand state college
                  </p>
                  <p>
                    My native place is Bihar, can I get admission to engineering
                    at Jharkhand state college
                  </p>
                  <Link target="_blank" to="">
                    View all questions
                  </Link>
                </article> */}

                {/* <article className="boxleft box">
                  <h4>Related Scholarships</h4>

                  <p>
                    {" "}
                    My native place is Bihar, can I get admission to engineering
                    at Jharkhand state college
                  </p>
                  <p>
                    My native place is Bihar, can I get admission to engineering
                    at Jharkhand state college
                  </p>
                  <p>
                    My native place is Bihar, can I get admission to engineering
                    at Jharkhand state college
                  </p>
                  <Link target="_blank" to="">
                    View all questions
                  </Link>
                </article> */}
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

const Answers = props => {
  let DisplayAnswers = null;
  if (props.answers && props.answers.length) {
    DisplayAnswers = props.answers.map((answer, index) => {
      let dayToGo = fromNow(answer.createdAt);
      return (
        <article key={answer.id} className="cateWrapper answer newedit">
          <article className="imgWrapper">
            <div className="left">
              <img
                src={
                  answer.authorPic
                    ? answer.authorPic
                    : `${imgBaseUrl}profile-img.png`
                }
              />
              <h5>{answer.authorName}
                <span>{dayToGo} </span>
              </h5>
            </div>

            <i
              className="share-icon"
              onClick={() => props.socialIconHandler(index)}
            >


              <Sharing
                id={index}
                isAni={props.isAni}
                pageUrl={
                  typeof window !== "undefined" ? window.location.href : ""
                }
                uniqueId={props.uniqueId}
              />
            </i>
            <article className="editdelgp">
              {parseInt(props.currentLoggedIn) == answer.authorUserId ?
                <button onClick={() => props.OnChangeEditorModeAnswer(index, answer, props)}>
                  <i
                    className="fa fa-edit"
                    aria-hidden="true"
                    title="Edit"
                  />
                </button>
                : null
              }
              {parseInt(props.currentLoggedIn) == answer.authorUserId ? (
                <button
                  onClick={() =>
                    props.onClickDeleteAnswerHandler(answer.id)
                  }
                >
                  {" "}
                  <i
                    className="fa fa-trash trash"
                    aria-hidden="true"
                    title="Delete"
                  />
                </button>
              ) : null}

            </article>

            {props.editorModeAnswer === index ?
              parseInt(props.currentLoggedIn) == answer.authorUserId ?

                <AnswerEdit
                  editState={props.answereditState} {...props}
                  answerId={answer.id}
                  index={index}
                  answer={answer}
                /> : null

              : ""}


            {


            }
          </article>

          <article className="para">
            <p
              dangerouslySetInnerHTML={{
                __html: answer.answerText
              }}
              className="wr"
            />

            <article className="comments">
              {" "}
              {answer.comment && answer.comment.length > 0 ? (
                <span>
                  Comments<i> {answer.comment && answer.comment.length}</i>
                </span>
              ) : (
                  <p>Be the first to comment</p>
                )}
            </article>
          </article>

          <article className="commentWrapper">
            <div className="left">
              <img
                src={
                  gblFunc.getStoreUserDetails().pic
                    ? gblFunc.getStoreUserDetails().pic
                    : `${imgBaseUrl}profile-img.png`
                }
              />
              <article className="comform">
                <form
                  onSubmit={props.onSubmitCommentHandle}
                  autoCapitalize="off"
                >
                  <section className="ctrl-wrapper">
                    <article className="form-group textAreaCrtl">
                      {/* <textarea
                        className="form-control"
                        placeholder="Write your comment"
                        id="commentText"
                        name="commentText"
                        rows="1"
                        value={
                          props.commentList &&
                            props.commentList.commentText &&
                            props.commentList.qnaAnswerId == answer.id
                            ? props.commentList.commentText
                            : ""
                        }
                        onChange={e =>
                          props.onCommentChangeHandler(e, answer.id)
                        }
                        onKeyPress={e => props.onCommitEnter(e)}
                      /> */}


                      {props.editorMode != index && (
                        <textarea
                          className="form-control"
                          // rows={`${
                          //   props.commentList.commentText &&
                          //     props.commentList.commentText.length > 0
                          //     ? "5"
                          //     : "1"
                          //   }`}
                          placeholder="Write your Comment"
                          id="commentText"
                          name="commentText"
                          // required=""
                          // value={
                          //   props.commentList &&
                          //     props.commentList.commentText &&
                          //     props.commentList.qnaAnswerId == answer.id
                          //     ? props.commentList.commentText
                          //     : ""
                          // }
                          onFocus={() => {
                            const isAuthenticated =
                              gblFunc.isUserAuthenticated() ||
                              props.isAuthenticated;
                            if (isAuthenticated) {
                              props.changeEditor1State(index);
                            } else {
                              props.changePendingModeState(index),
                                props.showLoginPopup()

                            }
                          }}
                        />
                      )}
                      {window !== undefined && props.editorMode == index && (
                        <CKEditor
                          data={props.commentList.commentText}
                          config={{
                            toolbar: [
                              ["Bold", "Italic", "Underline"],
                              ["Link"],
                              ["NumberedList", "BulletedList"]
                            ]
                          }}
                          // config={{
                          //   toolbar: [
                          //     [ 'Bold', 'Italic', 'Underline', 'Strike' ],
                          //     [ 'Cut', 'Copy' ],
                          //     [ 'Link', 'Unlink', 'Anchor' ],
                          //     [ 'Styles', 'Format', 'Font', 'FontSize' ],
                          //     [ 'TextColor', 'BGColor' ]
                          //   ]
                          // }}
                          onChange={e => {
                            // props.onCommentChangeHandler(e, answer.id)
                            props.onCommentChangeHandler(
                              e,
                              answer.id,
                              e.editor.getData(),
                              "editor4"
                            );
                          }}
                        />
                      )}
                    </article>
                    <article className="postckeditor">
                      {window !== undefined && props.editorMode == index ? <button
                        className="post"
                        onClick={e => props.onCommitEnter(e)}
                      >
                        Post
                      </button> : ""}

                      {window !== undefined && props.editorMode == index ? <button
                        className="post"
                        onClick={e => props.changeEditor1StateToCancel(e)}
                      >
                        Cancel
                      </button> : ""}

                    </article>
                  </section>
                  {/* {props.commentList.qnaAnswerId == answer.id &&
                  props.commentList &&
                  props.commentList.commentText &&
                  props.commentList.commentText.length ? (
                    <article className="pull-right">
                      <input type="submit" className="post" value="Post" />
                    </article>
                  ) : null} */}
                </form>
              </article>
            </div>
            {answer.comment && answer.comment.length > 0
              ? answer.comment.map(c => (
                <div className="left leftM15">
                  <img
                    src={
                      c.authorPic
                        ? c.authorPic
                        : `${imgBaseUrl}profile-img.png`
                    }
                  />
                  {c.authorUserId + c.id !== props.editCommentId ? (
                    <p className="replycomment">
                      <span style={{ color: "blue" }}>{c.authorName}: </span>{" "}
                      {/* {c.commentText} */}
                      <p dangerouslySetInnerHTML={{
                        __html: c.commentText
                      }} />

                      {/* <a target="_blank" href="/" className="readmore">
                    read more
                  </a> */}
                    </p>
                  ) : null}

                  {props.isReplyShowed === c.authorUserId + c.id &&
                    props.replyId == c.id ? (
                      <ReplyToComment commentObj={c} {...props} />
                    ) : null}

                  {props.isEditShowed === c.authorUserId + c.id &&
                    props.currentLoggedIn == c.authorUserId &&
                    props.editId == c.id ? (
                      <EditComment editState={props.editState} {...props} />
                    ) : null}
                  <p className="replygp">
                    {props.currentLoggedIn &&
                      props.currentLoggedIn != c.authorUserId &&
                      c.authorUserId + c.id !== props.replyCommentId ? (
                        <button onClick={() => props.onClickReplyHandler(c)}>
                          <i
                            className="fa fa-reply"
                            aria-hidden="true"
                            title="Reply"
                          />{" "}
                          Reply
                        </button>
                      ) : null}
                    {props.currentLoggedIn == c.authorUserId ? (
                      <button
                        onClick={() =>
                          props.onClickEditCommentHandler(c, c.id)
                        }
                      >
                        {" "}
                        {c.authorUserId + c.id !== props.editCommentId && (
                          <i
                            className="fa fa-edit"
                            aria-hidden="true"
                            title="Edit"
                          />
                        )}
                      </button>
                    ) : null}
                    {props.currentLoggedIn == c.authorUserId ? (
                      <button
                        onClick={() =>
                          props.onClickDeleteCommentHandler(c.id)
                        }
                      >
                        {" "}
                        <i
                          className="fa fa-trash"
                          aria-hidden="true"
                          title="Delete"
                        />
                      </button>
                    ) : null}
                  </p>
                  {c.childComments &&
                    c.childComments.map(child => (
                      <div className="subleft ">
                        <article className="reply">
                          <img
                            src={
                              child.authorPic
                                ? child.authorPic
                                : `${imgBaseUrl}profile-img.png`
                            }
                          />
                          {child.authorUserId + child.id !==
                            props.editCommentId ? (
                              <p className="replycomment">
                                <span style={{ color: "blue" }}>
                                  {child.authorName}:{" "}
                                </span>{" "}
                                {/* {child.commentText} */}
                                <p dangerouslySetInnerHTML={{
                                  __html: child.commentText
                                }} />
                                {/* <a target="_blank" href="/" className="readmore">
                            read more
                          </a> */}
                              </p>
                            ) : null}
                          {props.isReplyShowed === c.authorUserId + c.id &&
                            props.replyId == child.id ? (
                              <ReplyToComment commentObj={child} {...props} />
                            ) : null}

                          {props.isEditShowed ===
                            child.authorUserId + child.id &&
                            props.currentLoggedIn == child.authorUserId &&
                            props.editId == child.id ? (
                              <EditComment
                                editState={props.editState}
                                {...props}
                              />
                            ) : null}
                          <p className="replygp">
                            {/* <button>
                            {" "}
                            <i
                              className="fa fa-thumbs-up"
                              aria-hidden="true"
                            />{" "}
                            Like
                          </button>{" "} */}
                            {/* <button
                                onClick={() =>
                                  props.onClickReplyHandler(child.id)
                                }
                              >
                                <i className="fa fa-reply" aria-hidden="true" />{" "}
                                Reply
                              </button> */}
                            {props.currentLoggedIn == child.authorUserId ? (
                              <button
                                onClick={() =>
                                  props.onClickEditCommentHandler(
                                    child,
                                    child.id
                                  )
                                }
                              >
                                {" "}
                                {child.authorUserId + child.id !==
                                  props.editCommentId && (
                                    <i
                                      className="fa fa-edit"
                                      aria-hidden="true"
                                      title="Edit"
                                    />
                                  )}
                              </button>
                            ) : null}
                            {props.currentLoggedIn == child.authorUserId ? (
                              <button
                                onClick={() =>
                                  props.onClickDeleteCommentHandler(child.id)
                                }
                              >
                                {" "}
                                <i
                                  className="fa fa-trash"
                                  aria-hidden="true"
                                  title="Delete"
                                />
                              </button>
                            ) : null}
                          </p>
                        </article>
                      </div>
                    ))}
                </div>
              ))
              : null}
          </article>
        </article>
      );
    });
  }
  return DisplayAnswers;
};

const AnswerEdit = props => {
  return (
    <article className="comform">
      <form
        onSubmit={props.onSubmitAnswerHandler}
        autoCapitalize="off"
      >
        <section className="ctrl-wrapper">
          <article className="form-group textAreaCrtl">
            <CKEditor
              data={props.editState.commentText ? props.editState.commentText : props.answer.answerText}

              config={{
                toolbar: [
                  ["Bold", "Italic", "Underline"],
                  ["Link"],
                  ["NumberedList", "BulletedList"]
                ],

              }}

              onChange={(e) => {
                props.onEditAnswerHandler(
                  e,
                  props.editState,
                  e.editor.getData(),

                  "editor3",
                  props.data.id,
                  props.answerId

                );
              }}


            />
          </article>
          <article className="postckeditor">
            <button
              className="post"
              onClick={e => props.handleEditAnswerEnter(e)}
            >
              Post
                      </button>

            <button
              className="post"
              onClick={e => props.handleEditAnswerCancel(e)}
            >
              Cancel
                      </button>
          </article>
        </section>
        {/* {props.editState &&
        props.editState.commentText &&
        props.editState.commentText.length > 0 &&
        props.editId == props.editState.id ? (
          <article className="pull-right">
            <input type="submit" className="post" value="Post" />
          </article>
        ) : null}{" "} */}
      </form>
    </article>
  );
};

const EditComment = props => {
  return (
    <article className="comform">
      <form
        onSubmit={e => props.onSubmitCommentHandler(e, "EDIT")}
        autoCapitalize="off"
      >
        <section className="ctrl-wrapper">
          <article className="form-group textAreaCrtl">
            <CKEditor
              data={props.editState.commentText}
              config={{
                toolbar: [
                  ["Bold", "Italic", "Underline"],
                  ["Link"],
                  ["NumberedList", "BulletedList"]
                ],

              }}
              // config={{
              //   toolbar: [
              //     [ 'Bold', 'Italic', 'Underline', 'Strike' ],
              //     [ 'Cut', 'Copy' ],
              //     [ 'Link', 'Unlink', 'Anchor' ],
              //     [ 'Styles', 'Format', 'Font', 'FontSize' ],
              //     [ 'TextColor', 'BGColor' ]
              //   ]
              // }}
              // onChange={(e)=>{
              //   props.handleReplyToComment(
              //   e, props.commentObj
              // )}
              onChange={e => {
                props.onEditCommentHandler(
                  e,
                  props.editState,
                  e.editor.getData(),
                  "editor2"
                );
              }}


            />
          </article>
          <article className="postckeditor">
            <button
              className="post"
              onClick={e => props.handleEditEnter(e)}
            >
              Post
               </button>

            <button
              className="post"
              onClick={e => props.onClickEditCommentCancelHandler(e)}
            >
              Cancel
             </button>
          </article>
        </section>
        {/* {props.editState &&
        props.editState.commentText &&
        props.editState.commentText.length > 0 &&
        props.editId == props.editState.id ? (
          <article className="pull-right">
            <input type="submit" className="post" value="Post" />
          </article>
        ) : null}{" "} */}
      </form>
    </article>
  );
};

const ReplyToComment = props => {
  // CKEditor = require("ckeditor4-react");
  return (
    <article className="comform subComment">
      <form
        onSubmit={e => props.onSubmitCommentHandler(e, "REPLY")}
        autoCapitalize="off"
        className=""
      >
        <section className="ctrl-wrapper">
          <article className="form-group textAreaCrtl">
            <CKEditor
              data={props.replyState.commentText}
              config={{
                toolbar: [
                  ["Bold", "Italic", "Underline"],
                  ["Link"],
                  ["NumberedList", "BulletedList"]
                ],

              }}
              onChange={e => {
                props.handleReplyToComment(
                  e,
                  props.commentObj,
                  e.editor.getData(),
                  "editor1"
                );
              }}


            />
          </article>
          <article className="postckeditor">
            <button
              className="post"
              onClick={e => props.handleReplyEnter(e)}
            >
              Post
                      </button>
            <button
              className="post"
              onClick={e => props.onClickReplyCancelHandler(e)}
            >
              Cancel
                      </button>
          </article>
        </section>
        {/* {props.replyState &&
        props.replyState.commentText &&
        props.replyState.commentText.length > 0 &&
        props.replyId == props.commentObj.id ? (
          <article className="pull-right">
            <input type="submit" className="post" value="Post" />
          </article>
        ) : null}{" "} */}
      </form>
    </article>
  );
};

export default SinglePageQuestion;
