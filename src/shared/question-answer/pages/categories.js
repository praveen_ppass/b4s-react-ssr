import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { Sharing } from "./sharing";
import { Tags } from "./tags";
import AutoSuggestSearch from "./search";
import InfiniteScroll from "react-infinite-scroller";
import { fromNow, imgBaseUrl } from "../../../constants/constants";
import Pagination from "rc-pagination";
class Categories extends Component {
  constructor() {
    super();
    this.state = {
      isAni: true,
      isTotal: false,
      uniqueId: null,
      hasMoreItems: true,
      currentPage:1
    };
    this.socialIconHandler = this.socialIconHandler.bind(this);
    this.onMsgHandler = this.onMsgHandler.bind(this);
    this.onReadMoreHandler = this.onReadMoreHandler.bind(this);
    this.onPageChange = this.onPageChange.bind(this);

  }

  componentDidMount() {


    if (window !== undefined) {
      if (!!this.props.location.search) {
        let search = this.props.location.search;
        let urlPath = window.location.href;
        urlPath = urlPath.replace(search, "");
        window.history.pushState({ path: urlPath }, "", urlPath);
      }
    }
  }
  getOffset(page) {
    return page - 1;
  }

  onPageChange(currentPage) {
    const { match } = this.props;

    let page = this.getOffset(currentPage);

    const length = 30;
    if (match && match.params) {
      this.props.fetchCategoryBySlug({
        slug: match.params.routes,
        page: page,
        length: length + 30
      });
    }
    let urlPath = window.location.href;
    let urlNew = null;
    urlNew = urlPath.includes(`?`) ? urlPath.split(`?`) : null;
    window.history.pushState({ path: !!urlNew ? urlNew[0] + `?page=${page + 1}` : urlPath + `?page=${page + 1}` }, '', !!urlNew ? urlNew[0] + `?page=${page + 1}` : urlPath + `?page=${page + 1}`);
    this.setState({
      currentPage
    });
  }
  socialIconHandler(id) {
    this.setState({
      isAni: !this.state.isAni,
      uniqueId: id
    });
  }

  onMsgHandler(total, length) {
    const ctgLength = length + 5;

    this.setState(
      {
        isTotal: total == length ? true : false
      },
      () =>
        this.props.fetchCategoryList({
          page: 0,
          length: ctgLength
        })
    );
  }
  loadItems(length, total) {
    const { match } = this.props;

    if (match && match.params && total > length) {
      this.props.fetchCategoryBySlug({
        slug: match.params.routes,
        page: 0,
        length: length + 10
      });
    }
  }

  onReadMoreHandler() { }

  render() {
    let loader = <article className="qnaloading">Loading&#8230;</article>;
    let data = [],
      total = null,
      text = null,
      categories = [],
      categoryTotal = 0,
      items = [],
      likeTemplate,
      followTemplate;
    if (this.props.data && Object.keys(this.props.data).length) {
      data = this.props.data.category.questions.data;
      total = this.props.data.category.questions.total;
      text = this.props.data.category.text;
      categories =
        this.props.categoryTags &&
          this.props.categoryTags.data &&
          this.props.categoryTags.data.length
          ? this.props.categoryTags.data
          : [];
      categoryTotal =
        this.props.categoryTags && this.props.categoryTags.total
          ? this.props.categoryTags.total
          : 0;

      data.length > 0 ? (
        data.map(list => {
          let dayToGo = fromNow(list.createdAt);
          items.push(
            <article key={list.id} className="cateWrapper">
              <UserInfo
                socialIconHandler={this.socialIconHandler}
                isAni={this.state.isAni}
                authorName={list.authorName}
                id={list.id}
                uniqueId={this.state.uniqueId}
                authorPic={
                  list.authorPic
                    ? list.authorPic
                    : `${imgBaseUrl}/profile-img.png`
                }
                dayToGo={dayToGo}
              />
              <Tags tags={list.qnaCategories} />

              <h4> {list.questionText} </h4>
              <form
                onSubmit={this.props.onPostAnswerSubmitHandler}
                autoCapitalize="off"
                className="formelemt"
              >
                <section className="ctrl-wrapper">
                  <article className="form-group textAreaCrtl">
                    <textarea
                      className="form-control"
                      rows={`${
                        list.id == this.props.setUniqueQuestId &&
                          this.props.answerPost.answerText &&
                          this.props.answerPost.answerText.length > 0
                          ? "5"
                          : "1"
                        }`}
                      placeholder="Write your Answer"
                      id="answerText"
                      name="answerText"
                      required=""
                      value={
                        list.id == this.props.setUniqueQuestId &&
                          this.props.answerPost.answerText
                          ? this.props.answerPost.answerText
                          : ""
                      }
                      onChange={e =>
                        this.props.onAnswerChangeHandler(e, list.id)
                      }
                    />
                    {/* <span className="error animated bounce">
                          hehhehehhe
                        </span> */}
                  </article>
                </section>
                {this.props.showAnswerPostBtn &&
                  list.id == this.props.setUniqueQuestId &&
                  this.props.isAuthenticated ? (
                    <article className="pull-right">
                      <input type="submit" className="post" value="Post" />
                    </article>
                  ) : this.props.showAnswerPostBtn &&
                    !this.props.isAuthenticated ? (
                      <article className="pull-right">
                        <input
                          onClick={this.props.onLoginPopUpHandler}
                          type="button"
                          className="post"
                          value="Post"
                        />
                      </article>
                    ) : null}
              </form>
              {/* <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Aenean euismod bibendum laoreet.
              </p> */}
              <article className="headingWrapper">
                <article className="heading">
                  <span>
                    <article className="textanswer">
                      {list.answers.total} Answer
                    </article>
                  </span>
                </article>
              </article>

              <Answers
                slug={list.slug}
                answers={list.answers}
                onReadMoreHandler={this.onReadMoreHandler}
              />
            </article>
          );
        })
      ) : (
          <article className="cateWrapper">
            <p>
              No information available on this category. You can go through other
            categories <Link to="/qna">link</Link>.{" "}
            </p>
            {/* <a target="_blank" href="/" className="view">
              View all answers
          </a> */}
          </article>
        );
    }

    if (this.props.isAuthenticated && this.props.categoryIdBySlug) {
      likeTemplate = (
        <button
          onClick={() => this.props.onLikeFollowHandler("Like")}
          className={`btn-yellow paddingbtn ${
            this.props.isLiked ? "active" : ""
            }`}
        >
          {this.props.isLiked ? "Liked" : "Like"}
        </button>
      );
      followTemplate = (
        <button
          onClick={() => this.props.onLikeFollowHandler("Follow")}
          className={`btn-yellow paddingbtn ${
            this.props.isFollowed ? "active" : ""
            }`}
        >
          {this.props.isFollowed ? "Followed" : "Follow"}
        </button>
      );
    } else {
      likeTemplate = (
        <button
          onClick={this.props.onLoginPopUpHandler}
          className="btn-yellow paddingbtn"
        >
          Liked
        </button>
      );
      followTemplate = (
        <button
          onClick={this.props.onLoginPopUpHandler}
          className="btn-yellow paddingbtn"
        >
          Follow
        </button>
      );
    }

    return (
      <section className="qna qnacate">
        <section className="container-fluid qnaWrapper singlePage">
          <section className="container">
            <section className="row">
              <section className="col-md-8 col-sm-8 col-xs-12 posReltv">
                <section className="search-wraper">
                  <form
                    onSubmit={this.props.onQuestionPopUpHandler}
                    autoCapitalize="off"
                  >
                    <input
                      type="text"
                      value={this.props.postQuestionVal}
                      name="search"
                      id="search"
                      placeholder="Type your question"
                      autoComplete={"off"}
                      onChange={this.props.onPostChangeHandler}
                      onFocus={this.props.onPostChangeHandler}
                    />
                    <button type="submit">
                      <span>POST</span>
                      <i className="fa fa-search" aria-hidden="true" />
                    </button>
                  </form>
                </section>
              </section>

              <section className="col-md-4 col-sm-4 col-xs-12 search-single">
                <article className="followWrapper">
                  {/* active */}
                  {followTemplate}
                  {likeTemplate}
                  <ul>
                    <li>
                      {" "}
                      {this.props.likeFollowCount.totalLiked
                        ? this.props.likeFollowCount.totalLiked + " Likes"
                        : this.props.likeFollowCount.totalLiked + " Like"}
                    </li>
                    <li>|</li>
                    <li>
                      {this.props.likeFollowCount.totalFollowed
                        ? this.props.likeFollowCount.totalFollowed + " Followed"
                        : this.props.likeFollowCount.totalFollowed + " Follow"}
                    </li>
                  </ul>
                </article>
                <AutoSuggestSearch {...this.props} />
              </section>
            </section>
            <section className="row">
              <section className="col-md-8 col-sm-8 col-xs-12 posReltv top">
                <article className="title">
                  <h2>
                    {text
                      ? text.replace(/\b[a-z]/g, function () {
                        return arguments[0].toUpperCase();
                      })
                      : ""}
                  </h2>
                  {total ? <p>{total ? total : 0} Questions</p> : null}
                </article>

                {/* <InfiniteScroll
                  pageStart={0}
                  loadMore={this.loadItems.bind(this, items.length, total)}
                  hasMore={this.state.hasMoreItems}
                  loader={total > items.length ? loader : ""}
                  threshold={150}
                > */}
                {items && items.length > 0 ? (
                  items
                ) : (
                    <article className="cateWrapper">
                      <p>
                        No information available on this category. You can go
                        through other categories.
                      </p>
                      {/* <a target="_blank" href="/" className="view">
              View all answers
          </a> */}
                    </article>
                  )}
                {/* </InfiniteScroll> */}
                <article className="pagination">
                  {total>30 ? (
                    <Pagination
                      defaultPageSize={30}
                      pageSize={30}
                      defaultCurrent={1}
                      current={this.state.currentPage}
                      onChange={this.onPageChange}
                      total={total}
                      showTitle={false}
                    />
                  ) : null}
                </article>
              </section>
              <section className="col-md-4 col-sm-4 col-xs-12">
                {categories.length ? (
                  <article className="boxleft box">
                    <h4>Categories</h4>
                    <article className="scrollbar scrollbarColor">
                      {categories.map(catg => (
                        <p key={catg.id}>
                          {" "}
                          <Link
                            to={{
                              pathname: `/qna/categories/${catg.slug}`
                            }}
                          >
                            {" "}
                            {catg.text}
                          </Link>
                        </p>
                      ))}
                      {this.state.isTotal ? null : (
                        <span>
                          <i
                            onClick={() =>
                              this.onMsgHandler(
                                categoryTotal,
                                categories.length
                              )
                            }
                            className="fa fa-angle-double-down"
                          />
                        </span>
                      )}
                    </article>
                  </article>
                ) : null}
                {/* <article className="boxleft box">
                  <h4>Related Questions</h4>

                  <p>
                    {" "}
                    My native place is Bihar, can I get admission to engineering
                    at Jharkhand state college
                  </p>
                  <p>
                    {" "}
                    My native place is Bihar, can I get admission to engineering
                    at Jharkhand state college
                  </p>
                  <p>
                    {" "}
                    My native place is Bihar, can I get admission to engineering
                    at Jharkhand state college
                  </p>
                  <Link target="_blank" to="" className="view">
                    View all questions
                  </Link>
                </article> */}
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

const rawMarkUp = contact => {
  let rawMarkup = contact;
  return { __html: rawMarkup };
};

const Answers = props => {
  let DisplayAnswer = null;

  if (props.answers.data && props.answers.data.length > 0) {
    DisplayAnswer = props.answers.data.map(answer => {
      let dayToGo = fromNow(answer.createdAt);
      return (
        <article className="cateWrapper answer">
          <article className="imgWrapper">
            <div className="left">
              <img
                src={
                  answer.authorPic
                    ? answer.authorPic
                    : `${imgBaseUrl}/profile-img.png`
                }
              />
              <h5>{answer.authorName}</h5>
              <p>{dayToGo}</p>
            </div>
          </article>

          <article className="para">
            <article
              className="catg"
              dangerouslySetInnerHTML={rawMarkUp(answer.answerText)}
            />
            <p className="wr">
              {/* <a onChange={() => dotAnswerText(answer.answerText, 'read')} href="#">Readmore</a> */}
              {props.answers.total &&
                props.answers.total > 1 &&
                props.answers.data &&
                props.answers.data.length ? (
                  <Link
                    target="_blank"
                    to={`/qna/${props.slug}`}
                    className="viewanswer"
                  >
                    View all answers
                </Link>
                ) : null}
              {props.answers.total &&
                props.answers.total == 1 &&
                props.answers.data &&
                props.answers.data.length ? (
                  <Link
                    target="_blank"
                    to={`/qna/${props.slug}`}
                    className="viewanswer"
                  >
                    Full answer
                </Link>
                ) : null}
            </p>

            {/* <span>121 Comment</span> */}
          </article>
        </article>
      );
    });
  } else {
    DisplayAnswer = (
      <article className="cateWrapper answer">
        <p>No Answer Found.</p>
      </article>
    );
  }
  return DisplayAnswer;
};

const UserInfo = props => {
  return (
    <article className="imgWrapper">
      <div className="left">
        <img src={props.authorPic} />
        <h5>{props.authorName}</h5>
        <p>{props.dayToGo}</p>
      </div>
      <i
        className="share-icon"
        onClick={() => props.socialIconHandler(props.id)}
      >
        <Sharing
          isAni={props.isAni}
          id={props.id}
          uniqueId={props.uniqueId}
          pageUrl={typeof window !== "undefined" ? window.location.href : ""}
        />
      </i>
    </article>
  );
};

export default Categories;
