import React from "react";
import Autosuggest from "react-autosuggest";
import { FETCH_AUTO_SUGGEST_SUCCEEDED } from "../actions";

// https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

function getSuggestions(value, lists) {
  const escapedValue = escapeRegexCharacters(value.trim());

  if (escapedValue === "") {
    return [];
  }

  const regex = new RegExp(escapedValue, "ig");

  // return lists.filter(list => regex.test(list.questionText));
  return lists;
}

function getSuggestionValue(suggestion) {
  return suggestion.questionText;
}

function renderSuggestion(suggestion) {
  return <span>{suggestion.questionText}</span>;
}

class AutoSuggestSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: "",
      suggestions: []
    };
    this.onChange = this.onChange.bind(this);
    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(
      this
    );
    this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(
      this
    );
    this.onSuggestionSelected = this.onSuggestionSelected.bind(this);
    this.onSearchHandler = this.onSearchHandler.bind(this);
    this.shouldRenderSuggestions = this.shouldRenderSuggestions.bind(this);
  }

  onChange(event, { newValue, method }) {
    this.setState({
      value: newValue
    });
  }

  onSuggestionsFetchRequested({ value }) {
    setTimeout(
      () =>
        this.props.loadAutoSuggestList({
          term: value,
          page: 0,
          size: 10
        }),
      500
    );
  }

  onSuggestionsClearRequested() {
    this.setState({
      suggestions: []
    });
  }

  onSuggestionSelected(
    event,
    { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }
  ) {
    if (
      this.props.autoSuggestQuests &&
      this.props.autoSuggestQuests.data &&
      this.props.autoSuggestQuests.data.length &&
      (method == "enter" || method == "click")
    ) {
      const selectedQuestion = this.props.autoSuggestQuests.data.filter(
        list => {
          if (list.questionText == suggestionValue) {
            return list;
          }
        }
      );

      this.props.history.push({
        pathname: `/qna/${selectedQuestion[0].slug}`
      });
    }
  }

  onSearchHandler() {
    if (
      this.props.autoSuggestQuests &&
      this.props.autoSuggestQuests.data &&
      this.props.autoSuggestQuests.data.length
    ) {
      const selectedQuestion = this.props.autoSuggestQuests.data.filter(
        list => {
          if (list.questionText == this.state.value) {
            return list;
          }
        }
      );

      this.props.history.push({
        pathname: `/qna/${selectedQuestion[0].slug}`
      });
    }
  }

  shouldRenderSuggestions(value) {
    return value.trim().length >= 3;
  }

  componentDidMount() {
    // console.log(this.props.autoSuggestQuests);
    // console.log(this.state.suggestions);
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case FETCH_AUTO_SUGGEST_SUCCEEDED:
        if (
          nextProps.autoSuggestQuests &&
          nextProps.autoSuggestQuests.data &&
          nextProps.autoSuggestQuests.data.length
        ) {
          this.setState({
            suggestions: getSuggestions(
              this.state.value,
              nextProps.autoSuggestQuests.data
            )
          });
        }

        break;
    }
  }

  render() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: "Type your question",
      value,
      onChange: this.onChange
    };

    return (
      <section className="search-ctrl">
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          onSuggestionSelected={this.onSuggestionSelected}
          shouldRenderSuggestions={this.shouldRenderSuggestions}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          inputProps={inputProps}
        />
        <button type="button" onClick={this.onSearchHandler}>
          <i className="fa fa-search" aria-hidden="true" />
        </button>
      </section>
    );
  }
}

export default AutoSuggestSearch;

// <section className="search-wraper">
//           <form autoCapitalize="off">
//             <input
//               type="text"
//               value=""
//               name="search"
//               id="search"
//               placeholder="Type your question"
//               autoComplete={"off"}
//             />
//             <button type="submit">
//               <span>Search</span>
//               <i className="fa fa-search" aria-hidden="true" />
//             </button>
//           </form>
//         </section>
