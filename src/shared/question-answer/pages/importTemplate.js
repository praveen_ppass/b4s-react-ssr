import SinglePageQuestion from "../pages/singlePageQuestions";
import Categories from "../pages/categories";

export const pageTemplate = {
  SinglePageQuestion,
  Categories
};
