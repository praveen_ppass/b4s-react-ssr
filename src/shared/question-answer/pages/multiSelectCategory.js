import React from "react";
import Select from "react-select";

const multiSelectCatg = props => {
  console.log(props.options)
  return (
    <Select
      value={props.selectedOptions}
      onChange={props.onSelectHandler}
      options={props.options}
      valueKey="id"
      labelKey="text"
      placeholder="Type your categories"
      className="form-control"
      multi
      backspaceToRemoveMessage=""
      id="selectCatgory"
      closeOnSelect={true}
    />
  );
};

export default multiSelectCatg;
