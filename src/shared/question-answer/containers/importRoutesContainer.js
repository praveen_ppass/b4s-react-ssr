import { connect } from "react-redux";
import importRoutesComponent from "../components/importRoutes";

import {
  fetchQuesAns as getQuesAnswAction,
  fetchCatgSlugData as fetchCategorySlugAction,
  fetchRelatedQues as relatedQuestionAction,
  fetchAutoSuggestList as fetchAutoSuggestListAction,
  fetchViewCounter as viewCounterAction,
  fetchCatgList as fetchCategoryListAction,
  fetchLikeAndFollow as fetchLikeAndFollowAction,
  fetchAllLikeAndFollow as fetchAllLikeAndFollowAction,
  saveLikeAndFollow as saveLikeAndFollowAction,
  fetchCatgList as fetchCatgListAction,
  saveQuestion as saveQuestionAction,
  saveAnsToQues as saveAnsToQuesAction,
  fetchAnsSpq as fetchAnsSpqAction,
  postComment as postCommentAction,
  postAnswer as postAnswerAction,
  deleteComment as deleteCommentAction,
  deleteAnswer as deleteAnswerAction,
} from "../actions";

const mapStateToProps = ({ quesAnswerData, loginOrRegister }) => ({
  singlePageQues: quesAnswerData.quesAnswer,
  categories: quesAnswerData.categoryData,
  type: quesAnswerData.type,
  relatedQuestions: quesAnswerData.relatedData,
  autoSuggestQuests: quesAnswerData.autoSuggestQuestList,
  categoryTags: quesAnswerData.categoryTags,
  showLoader: quesAnswerData.showLoader,
  likeFollowDts: quesAnswerData.likeFollowDts,
  allLikeFollowDts: quesAnswerData.allLikeFollowDts,
  errorMessage: quesAnswerData.errorMessage,
  isAuthenticated: loginOrRegister.isAuthenticated,
  savedLikeFollow: quesAnswerData.savedLikeFollow,
  singlePageQuestAnsws: quesAnswerData.answers,
  savedComments: quesAnswerData.savedComments,
  deleteComment: quesAnswerData.deleteComment,
  savedAnswer:quesAnswerData.savedAnswer,
  data1:quesAnswerData.data1,

});
const mapDispatchToProps = dispatch => ({
  getQuesAnsw: inputData => dispatch(getQuesAnswAction(inputData)),
  fetchCategoryBySlug: inputData =>
    dispatch(fetchCategorySlugAction(inputData)),
  relatedQuestion: inputData => dispatch(relatedQuestionAction(inputData)),
  loadAutoSuggestList: inputData =>
    dispatch(fetchAutoSuggestListAction(inputData)),
  viewCounter: inputData => dispatch(viewCounterAction(inputData)),
  fetchCategoryList: inputData => dispatch(fetchCategoryListAction(inputData)),
  loadLikeAndFollow: inputData => dispatch(fetchLikeAndFollowAction(inputData)),
  loadAllLikeAndFollow: inputData =>
    dispatch(fetchAllLikeAndFollowAction(inputData)),
  setLikeAndFollow: inputData => dispatch(saveLikeAndFollowAction(inputData)),
  loadCatgList: inputData => dispatch(fetchCatgListAction(inputData)),
  postQuestion: inputData => dispatch(saveQuestionAction(inputData)),
  postAnsToQues: inputData => dispatch(saveAnsToQuesAction(inputData)),
  getAnswerFromSPQ: inputData => dispatch(fetchAnsSpqAction(inputData)),
  saveComments: inputData => dispatch(postCommentAction(inputData)),
  deletedComment: inputData => dispatch(deleteCommentAction(inputData)),
  deleteAnswer: inputData => dispatch(deleteAnswerAction(inputData)),
  saveEditAnswer: inputData => dispatch(postAnswerAction(inputData)),


});

export default connect(mapStateToProps, mapDispatchToProps)(
  importRoutesComponent
);
