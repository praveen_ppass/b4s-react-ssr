import { connect } from "react-redux";
import questionAnswerComponent from "../components/questionAnswer";

import {
  fetchQuesAnsTypes as questAnswTypeAction,
  fetchAutoSuggestList as fetchAutoSuggestListAction,
  fetchCatgList as fetchCatgListAction,
  saveQuestion as saveQuestionAction,
  deleteAnswer as deleteAnswerAction,
  fetchQues as fetchQuesListAction
} from "../actions";
import { fetchScholarships as fetchScholarshipsAction } from "../../scholarship/actions";
const mapStateToProps = ({ quesAnswerData, scholarship, loginOrRegister }) => ({
  homeQuesAnswer: quesAnswerData.homeQuesAnswer,
  type: quesAnswerData.type,
  scholarshipList: scholarship.scholarshipList,
  autoSuggestQuests: quesAnswerData.autoSuggestQuestList,
  showLoader: quesAnswerData.showLoader,
  categoryTags: quesAnswerData.categoryTags,
  isAuthenticated: loginOrRegister.isAuthenticated,
  errorMessage: quesAnswerData.errorMessage,
  data1:quesAnswerData.data1,
  deleteAnswerDts:quesAnswerData.deleteAnswerDts,
  relatedQusetionData:quesAnswerData.relatedQusetionData
});
const mapDispatchToProps = dispatch => ({
  fetchQuesAnsType: inputData => dispatch(questAnswTypeAction(inputData)),
  loadScholarships: inputData => dispatch(fetchScholarshipsAction(inputData)),
  loadAutoSuggestList: inputData =>
    dispatch(fetchAutoSuggestListAction(inputData)),
  loadCatgList: inputData => dispatch(fetchCatgListAction(inputData)),
  postQuestion: inputData => dispatch(saveQuestionAction(inputData)),
  deleteAnswer: inputData => dispatch(deleteAnswerAction(inputData)),
  fetchQues : inputData => dispatch(fetchQuesListAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  questionAnswerComponent
);
