import {
  FETCH_QUESTION_ANSWER_REQUESTED,
  FETCH_QUESTION_ANSWER_SUCCEEDED,
  FETCH_QUESTION_ANSWER_FAILED,
  FETCH_HOME_QUES_ANS_REQUESTED,
  FETCH_HOME_QUES_ANS_SUCCEEDED,
  FETCH_HOME_QUES_ANS_FAILED,
  FETCH_CATEGORY_SLUG_DATA_REQUESTED,
  FETCH_CATEGORY_SLUG_DATA_SUCCEEDED,
  FETCH_CATEGORY_SLUG_DATA_FAILED,
  FETCH_RELATED_QUESTION_REQUESTED,
  FETCH_RELATED_QUESTION_SUCCEEDED,
  FETCH_RELATED_QUESTION_FAILED,
  FETCH_AUTO_SUGGEST_REQUESTED,
  FETCH_AUTO_SUGGEST_SUCCEEDED,
  FETCH_AUTO_SUGGEST_FAILED,
  FETCH_VIEW_COUNTER_SUCCEEDED,
  FETCH_VIEW_COUNTER_FAILED,
  FETCH_VIEW_COUNTER_REQUESTED,
  FETCH_CATEGORY_LIST_FAILED,
  FETCH_CATEGORY_LIST_SUCCEEDED,
  FETCH_CATEGORY_LIST_REQUESTED,
  SAVE_LIKE_QA_FOLLOW_REQUESTED,
  SAVE_LIKE_QA_FOLLOW_SUCCEEDED,
  SAVE_LIKE_QA_FOLLOW_FAILED,
  FETCH_LIKE_FOLLOW_REQUESTED,
  FETCH_QA_LIKE_FOLLOW_REQUESTED,
  FETCH_QA_LIKE_FOLLOW_SUCCEEDED,
  FETCH_QA_LIKE_FOLLOW_FAILED,
  FETCH_ALL_QA_LIKE_FOLLOW_REQUESTED,
  FETCH_ALL_QA_LIKE_FOLLOW_SUCCEEDED,
  FETCH_ALL_QA_LIKE_FOLLOW_FAILED,
  SAVE_QUESTION_REQUESTED,
  SAVE_QUESTION_SUCCEEDED,
  SAVE_QUESTION_FAILED,
  DELETE_ANSWER_REQUESTED,
  DELETE_ANSWER_SUCCEEDED,
  DELETE_ANSWER_FAILED,
  SAVE_ANSWERS_TO_QUEST_REQUESTED,
  SAVE_ANSWERS_TO_QUEST_SUCCEEDED,
  SAVE_ANSWERS_TO_QUEST_FAILED,
  FETCH_ANSWERS_SPQ_REQUESTED,
  FETCH_ANSWERS_SPQ_SUCCEEDED,
  FETCH_ANSWERS_SPQ_FAILED,
  POST_COMMENT_REQUESTED,
  POST_COMMENT_SUCCEEDED,
  POST_COMMENT_FAILED,
  POST_ANSWER_REQUESTED,
  POST_ANSWER_SUCCEEDED,
  POST_ANSWER_FAILED,
  DELETE_COMMENT_FAILED,
  DELETE_COMMENT_SUCCEEDED,
  DELETE_COMMENT_REQUESTED,
  FETCH_QUESTION_REQUESTED,
  FETCH_QUESTION_SUCCEEDED,
  FETCH_QUESTION_FAILED
} from "./actions";
import {
  FETCH_SCHOLARSHIPS_REQUESTED,
  FETCH_SCHOLARSHIPS_SUCCEEDED,
  FETCH_SCHOLARSHIPS_FAILED
} from "../scholarship/actions";
import {
  LOGIN_USER_SUCCEEDED,
  SOCIAL_LOGIN_USER_SUCCEEDED,
  LOGIN_USER_FAILED,
  SOCIAL_LOGIN_USER_FAILED
} from "../login/actions";

const initialState = {
  showLoader: true,
  isError: false,
  errorMessage: "",
  quesAnswer: null,
  homeQuesAnswer: null,
  autoSuggestQuestList: [],
  savedLikeFollow: null,
  likeFollowDts: null,
  allLikeFollowDts: null,
  postedQuestionDts: null,
  deleteAnswerDts:null,
  answers: [],
  relatedQusetionData:null
};

const quesAnsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_QUESTION_ANSWER_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case FETCH_QUESTION_ANSWER_SUCCEEDED:
      return {
        ...state,
        quesAnswer: payload,
        showLoader: false,
        isError: false,
        type
      };
    case FETCH_QUESTION_ANSWER_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };

    case FETCH_HOME_QUES_ANS_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case FETCH_HOME_QUES_ANS_SUCCEEDED:
      return {
        ...state,
        homeQuesAnswer: payload,
        showLoader: false,
        isError: false,
        type
      };
    case FETCH_HOME_QUES_ANS_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };

      case FETCH_RELATED_QUESTION_REQUESTED:
        return {
          ...state,
          showLoader: true,
          isError: false,
          type
        };
  
      case FETCH_RELATED_QUESTION_SUCCEEDED:
        return {
          ...state,
          relatedQuestions: payload,
          showLoader: false,
          isError: false,
          type
        };
      case FETCH_RELATED_QUESTION_FAILED:
        return {
          ...state,
          showLoader: false,
          type,
          isError: true,
          errorMessage: payload
        };

    case FETCH_SCHOLARSHIPS_REQUESTED:
      return Object.assign({}, state, {
        scholarshipList: null,
        showLoader: true,
        isError: false,
        type
      });

    case FETCH_SCHOLARSHIPS_SUCCEEDED:
      const { scholarships } = payload;
      let scholarshipList = scholarships;

      let newState = {
        scholarshipList,
        showLoader: false,
        isError: false,
        type
      };
      return Object.assign({}, state, newState);

    case FETCH_SCHOLARSHIPS_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };

    case FETCH_CATEGORY_SLUG_DATA_REQUESTED:
      return {
        ...state,
        isError: false,
        type
      };

    case FETCH_CATEGORY_SLUG_DATA_SUCCEEDED:
      return {
        ...state,
        categoryData: payload,
        isError: false,
        type
      };
    case FETCH_CATEGORY_SLUG_DATA_FAILED:
      return {
        ...state,
        type,
        isError: true,
        errorMessage: payload
      };

    case FETCH_QUESTION_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case FETCH_QUESTION_SUCCEEDED:
      return {
        ...state,
        relatedQusetionData: payload,
        showLoader: false,
        isError: false,
        type
      };
    case FETCH_QUESTION_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };

    case FETCH_AUTO_SUGGEST_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case FETCH_AUTO_SUGGEST_SUCCEEDED:
      return {
        ...state,
        autoSuggestQuestList: payload,
        showLoader: false,
        isError: false,
        type
      };
    case FETCH_AUTO_SUGGEST_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };
    case FETCH_VIEW_COUNTER_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case FETCH_VIEW_COUNTER_SUCCEEDED:
      return {
        ...state,
        viewCounter: payload,
        showLoader: false,
        isError: false,
        type
      };
    case FETCH_VIEW_COUNTER_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };
    case FETCH_CATEGORY_LIST_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case FETCH_CATEGORY_LIST_SUCCEEDED:
      return {
        ...state,
        categoryTags: payload,
        showLoader: false,
        isError: false,
        type
      };
    case FETCH_CATEGORY_LIST_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };

    case SAVE_LIKE_QA_FOLLOW_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case SAVE_LIKE_QA_FOLLOW_SUCCEEDED:
      return {
        ...state,
        savedLikeFollow: payload,
        showLoader: false,
        isError: false,
        type
      };
    case SAVE_LIKE_QA_FOLLOW_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };

    case FETCH_QA_LIKE_FOLLOW_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case FETCH_QA_LIKE_FOLLOW_SUCCEEDED:
      return {
        ...state,
        likeFollowDts: payload,
        showLoader: false,
        isError: false,
        type
      };
    case FETCH_QA_LIKE_FOLLOW_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };

    case FETCH_ALL_QA_LIKE_FOLLOW_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case FETCH_ALL_QA_LIKE_FOLLOW_SUCCEEDED:
      return {
        ...state,
        allLikeFollowDts: payload,
        showLoader: false,
        isError: false,
        type
      };
    case FETCH_ALL_QA_LIKE_FOLLOW_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };
    case LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload
      };
    case "VERIFY_TOKEN":
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload,
        isAuthenticated: true,
        userRulesPresent: false
      };
    case "ADD_MOBILE":
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload,
        isAuthenticated: true,
        userRulesPresent: false
      };
    case LOGIN_USER_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload
      };
    case SOCIAL_LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type,
        userLoginData: payload,
        isAuthenticated: true
      };
    case SOCIAL_LOGIN_USER_FAILED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type,
        userLoginData: payload,
        isAuthenticated: true
      };

    case SAVE_QUESTION_REQUESTED:
    return {
        ...state,
        showLoader: true,
        isError: false,
        type,
        data1:payload
      };

    case SAVE_QUESTION_SUCCEEDED:
      return {
        ...state,
        postedQuestionDts: payload,
        showLoader: false,
        isError: false,
        type,
        data1:payload
      };
    case SAVE_QUESTION_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };

      case DELETE_ANSWER_REQUESTED:
        return {
            ...state,
            showLoader: true,
            isError: false,
            type,
            deleteAnswerDts:payload
          };
    
        case DELETE_ANSWER_SUCCEEDED:
          return {
            ...state,
            deleteAnswerDts: payload,
            showLoader: false,
            isError: false,
            type,
            deleteAnswerDts:payload
          };
        case DELETE_ANSWER_FAILED:
          return {
            ...state,
            showLoader: false,
            type,
            isError: true,
            errorMessage: payload
          };
    case SAVE_ANSWERS_TO_QUEST_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case SAVE_ANSWERS_TO_QUEST_SUCCEEDED:
      return {
        ...state,
        savedAnswS: payload,
        showLoader: false,
        isError: false,
        type
      };
    case SAVE_ANSWERS_TO_QUEST_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };

    case FETCH_ANSWERS_SPQ_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case FETCH_ANSWERS_SPQ_SUCCEEDED:
      return {
        ...state,
        answers: payload,
        showLoader: false,
        isError: false,
        type
      };
    case FETCH_ANSWERS_SPQ_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };

    case POST_COMMENT_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case POST_COMMENT_SUCCEEDED:
      return {
        ...state,
        savedComments: payload,
        showLoader: false,
        isError: false,
        type
      };
    case POST_COMMENT_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };
      case POST_ANSWER_REQUESTED:
        return {
          ...state,
          showLoader: true,
          isError: false,
          type
        };
  
      case POST_ANSWER_SUCCEEDED:
        return {
          ...state,
          savedAnswer: payload,
          showLoader: false,
          isError: false,
          type
        };
      case POST_ANSWER_FAILED:
        return {
          ...state,
          showLoader: false,
          type,
          isError: true,
          errorMessage: payload
        };
    case DELETE_COMMENT_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case DELETE_COMMENT_SUCCEEDED:
      return {
        ...state,
        deleteComment: payload,
        showLoader: false,
        isError: false,
        type
      };
    case DELETE_COMMENT_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };
    default:
      return state;
  }
};

export default quesAnsReducer;
