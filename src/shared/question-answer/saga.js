import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

/************   All actions required in this module starts *********** */
import {
  FETCH_QUESTION_ANSWER_REQUESTED,
  FETCH_QUESTION_ANSWER_SUCCEEDED,
  FETCH_QUESTION_ANSWER_FAILED,
  FETCH_HOME_QUES_ANS_REQUESTED,
  FETCH_HOME_QUES_ANS_SUCCEEDED,
  FETCH_HOME_QUES_ANS_FAILED,
  FETCH_CATEGORY_SLUG_DATA_REQUESTED,
  FETCH_CATEGORY_SLUG_DATA_SUCCEEDED,
  FETCH_CATEGORY_SLUG_DATA_FAILED,
  FETCH_RELATED_QUESTION_SUCCEEDED,
  FETCH_RELATED_QUESTION_FAILED,
  FETCH_RELATED_QUESTION_REQUESTED,
  FETCH_AUTO_SUGGEST_REQUESTED,
  FETCH_AUTO_SUGGEST_SUCCEEDED,
  FETCH_AUTO_SUGGEST_FAILED,
  FETCH_VIEW_COUNTER_REQUESTED,
  FETCH_VIEW_COUNTER_SUCCEEDED,
  FETCH_VIEW_COUNTER_FAILED,
  FETCH_CATEGORY_LIST_REQUESTED,
  FETCH_CATEGORY_LIST_SUCCEEDED,
  FETCH_CATEGORY_LIST_FAILED,
  SAVE_LIKE_QA_FOLLOW_REQUESTED,
  SAVE_LIKE_QA_FOLLOW_SUCCEEDED,
  SAVE_LIKE_QA_FOLLOW_FAILED,
  FETCH_QA_LIKE_FOLLOW_REQUESTED,
  FETCH_ALL_QA_LIKE_FOLLOW_REQUESTED,
  FETCH_QA_LIKE_FOLLOW_SUCCEEDED,
  FETCH_QA_LIKE_FOLLOW_FAILED,
  FETCH_ALL_QA_LIKE_FOLLOW_SUCCEEDED,
  FETCH_ALL_QA_LIKE_FOLLOW_FAILED,
  SAVE_QUESTION_REQUESTED,
  SAVE_QUESTION_SUCCEEDED,
  SAVE_QUESTION_FAILED,
  DELETE_ANSWER_REQUESTED,
  DELETE_ANSWER_SUCCEEDED,
  DELETE_ANSWER_FAILED,
  SAVE_ANSWERS_TO_QUEST_REQUESTED,
  SAVE_ANSWERS_TO_QUEST_SUCCEEDED,
  SAVE_ANSWERS_TO_QUEST_FAILED,
  FETCH_ANSWERS_SPQ_REQUESTED,
  FETCH_ANSWERS_SPQ_SUCCEEDED,
  FETCH_ANSWERS_SPQ_FAILED,
  POST_COMMENT_REQUESTED,
  POST_COMMENT_SUCCEEDED,
  POST_COMMENT_FAILED,
  POST_ANSWER_REQUESTED,
  POST_ANSWER_SUCCEEDED,
  POST_ANSWER_FAILED,
  DELETE_COMMENT_REQUESTED,
  DELETE_COMMENT_SUCCEEDED,
  DELETE_COMMENT_FAILED,
  FETCH_QUESTION_REQUESTED,
  FETCH_QUESTION_SUCCEEDED,
  FETCH_QUESTION_FAILED
} from "./actions";
const fetchQuestAnsAPI = ({ slug }) =>
  fetchClient.get(apiUrl.getQuestAnsw + "/qna-question/" + slug).then(res => {
    return res.data;
  });

function* fetchQuestAns(input) {
  try {
    const questAnsw = yield call(fetchQuestAnsAPI, input.payload);
    yield put({
      type: FETCH_QUESTION_ANSWER_SUCCEEDED,
      payload: questAnsw
    });
  } catch (error) {
    yield put({
      type: FETCH_QUESTION_ANSWER_FAILED,
      payload: error
    });
  }
}

const fetchQuestTypeAPI = () =>
  fetchClient.get(apiUrl.getQuestAnsw + "/category/top/question").then(res => {
    return res.data;
  });

const fetchQuesCatgCallAPI = ({ page, length }) => {
  let url;
  if (page == 0 && length) {
    url = `${apiUrl.getQuestAnsw}/category/?page=${page}&length=${length}`;
  } else {
    url = `${apiUrl.getQuestAnsw}/category`;
  }

  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchQuestType(input) {
  try {
    const { questAnsType, questAnsCatg } = yield all({
      questAnsType: call(fetchQuestTypeAPI),
      questAnsCatg: call(fetchQuesCatgCallAPI, input.payload)
    });

    let data = {
      questAnsType,
      questAnsCatg
    };

    yield put({
      type: FETCH_HOME_QUES_ANS_SUCCEEDED,
      payload: data
    });
  } catch (error) {
    yield put({
      type: FETCH_HOME_QUES_ANS_FAILED,
      payload: error
    });
  }
}

/* Fetch Questions List */

const fetchLatestQuestionAPI = ({ page, length }) => {
  let url;
  url = `${apiUrl.latestQue}?page=${page}&size=${length}`;
  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchLatestQuestion(input) {
  try {
    const questAnsw = yield call(fetchLatestQuestionAPI, input.payload);
    yield put({
      type: FETCH_QUESTION_SUCCEEDED,
      payload: questAnsw
    });
  } catch (error) {
    yield put({
      type: FETCH_QUESTION_FAILED,
      payload: error
    });
  }
}
const fetchCatgSlugCallAPI = ({ slug, page, length }) =>
  fetchClient
    .get(
      `${
      apiUrl.getQuestAnsw
      }/category/${slug}/question?page=${page}&length=${length}`
    )
    .then(res => {
      return res.data;
    });

function* fetchCatgSlugCall(input) {
  try {
    const { category } = yield all({
      category: call(fetchCatgSlugCallAPI, input.payload)
      // quesAnsCatg: call(fetchQuesCatgCallAPI, input.payload)
    });
    let data = {
      category
      // quesAnsCatg
    };
    // const category = yield call(fetchCatgSlugCallAPI, input.payload);
    yield put({
      type: FETCH_CATEGORY_SLUG_DATA_SUCCEEDED,
      payload: data
    });
  } catch (error) {
    yield put({
      type: FETCH_CATEGORY_SLUG_DATA_FAILED,
      payload: error
    });
  }
}

const fetchRelatedQuestionApi = ({ slug, categories, page, length }) =>
  fetchClient
    .get(
      `${
      apiUrl.getQuestAnsw
      }/category/question/related?categories=${categories}&excludeSlug=${slug}&page=${page}&length=${length}`
    )
    .then(res => {
      return res.data;
    });

function* fetchRelatedQuestion(input) {
  try {
    const relatedQuestion = yield call(fetchRelatedQuestionApi, input.payload);

    yield put({
      type: FETCH_RELATED_QUESTION_SUCCEEDED,
      payload: relatedQuestion
    });
  } catch (error) {
    yield put({
      type: FETCH_RELATED_QUESTION_FAILED,
      payload: error
    });
  }
}

const fetchAutoSuggestQuestionApi = ({ term, page, size }) =>
  fetchClient
    .get(
      `${
      apiUrl.getQuestAnsw
      }/qna-question/suggest?term=${term}&page=${page}&size=${size}`
    )
    .then(res => {
      return res.data;
    });

function* fetchAutoSuggestQuestion(input) {
  try {
    const autoSuggestQues = yield call(
      fetchAutoSuggestQuestionApi,
      input.payload
    );

    yield put({
      type: FETCH_AUTO_SUGGEST_SUCCEEDED,
      payload: autoSuggestQues
    });
  } catch (error) {
    yield put({
      type: FETCH_AUTO_SUGGEST_FAILED,
      payload: error
    });
  }
}

const fetchViewCounterApi = input => {
  let url = `${apiUrl.getQuestAnsw}/qna-question/activity`;

  if (input.userId && input.qnaQuestionId) {
    url = url + `?qnaQuestionId=${input.qnaQuestionId}&userId=${input.userId}`;
  }
  if (!input.userId && input.qnaQuestionId) {
    url = url + `?qnaQuestionId=${input.qnaQuestionId}`;
  }
  return fetchClient.post(url, input).then(res => {
    return res.data;
  });
};

function* fetchViewCounterCall(input) {
  try {
    const viewCounter = yield call(fetchViewCounterApi, input.payload);

    yield put({
      type: FETCH_VIEW_COUNTER_SUCCEEDED,
      payload: viewCounter
    });
  } catch (error) {
    yield put({
      type: FETCH_VIEW_COUNTER_FAILED,
      payload: error
    });
  }
}

function* fetchCategoryApiCall(input) {
  try {
    const categoryList = yield call(fetchQuesCatgCallAPI, input.payload);

    yield put({
      type: FETCH_CATEGORY_LIST_SUCCEEDED,
      payload: categoryList
    });
  } catch (error) {
    yield put({
      type: FETCH_CATEGORY_LIST_FAILED,
      payload: error
    });
  }
}

const saveLikeFollowCallApi = (input, methodType) => {
  const { routeName } = input;
  let likeFollowType =
    routeName === "question" ? "qna-question" : "qna/category";
  let url = `${apiUrl.likeFollow}/${likeFollowType}/${input.id}/user/${
    input.userId
    }/follow`;

  switch (methodType) {
    case "POST":
      return fetchClient.post(url, input.qnaCategoryFollowDto).then(res => {
        return res.data;
      });
    case "GET":
      return fetchClient.get(url).then(res => {
        return res.data;
      });
  }
};

function* saveLikeFollowCall(input) {
  try {
    const saveLikeFollow = yield call(
      saveLikeFollowCallApi,
      input.payload,
      "POST"
    );

    yield put({
      type: SAVE_LIKE_QA_FOLLOW_SUCCEEDED,
      payload: saveLikeFollow
    });
  } catch (error) {
    yield put({
      type: SAVE_LIKE_QA_FOLLOW_FAILED,
      payload: error
    });
  }
}

function* fetchLikeFollowCall(input) {
  try {
    const likeFollow = yield call(saveLikeFollowCallApi, input.payload, "GET");

    yield put({
      type: FETCH_QA_LIKE_FOLLOW_SUCCEEDED,
      payload: likeFollow
    });
  } catch (error) {
    yield put({
      type: FETCH_QA_LIKE_FOLLOW_FAILED,
      payload: error
    });
  }
}

function allLikeFollowCallApi({ id, routeName }) {
  let likeFollowType =
    routeName === "question" ? "qna-question" : "qna/category";
  let url = `${apiUrl.likeFollow}/${likeFollowType}/${id}/follow`;

  return fetchClient.get(url).then(res => res.data);
}

function* fetchAllLikeFollowCall(input) {
  try {
    const allLikeFollow = yield call(allLikeFollowCallApi, input.payload);

    yield put({
      type: FETCH_ALL_QA_LIKE_FOLLOW_SUCCEEDED,
      payload: allLikeFollow
    });
  } catch (error) {
    yield put({
      type: FETCH_ALL_QA_LIKE_FOLLOW_FAILED,
      payload: error.response
    });
  }
}

function saveQuestionDtsCallApi(input) {
  return fetchClient
    .post(`${apiUrl.postQuestAnsw}/qna-question`, input)
    .then(res => res.data);
}

function* saveQuestionDtsCall(input) {
  try {
    const savedQuest = yield call(saveQuestionDtsCallApi, input.payload);

    yield put({
      type: SAVE_QUESTION_SUCCEEDED,
      payload: savedQuest
    });
  } catch (error) {
    yield put({
      type: SAVE_QUESTION_FAILED,
      payload: error.response
    });
  }
}
function deleteAnswerApi(input) {
  return fetchClient
    .post(
      `${apiUrl.deleteAnswer}/qna-answer/${input.answerId}/status/${0}`
    )
    .then(res => res.data);
}

function* deleteAnswerCall(input) {
  try {
    const savedQuest = yield call(deleteAnswerApi, input.payload);

    yield put({
      type: DELETE_ANSWER_SUCCEEDED,
      payload: savedQuest
    });
  } catch (error) {
    yield put({
      type: DELETE_ANSWER_FAILED,
      payload: error.response
    });
  }
}
function saveAnswersCallApi(input) {
  return fetchClient
    .post(
      `${apiUrl.likeFollow}/qna-question/${input.questionId}/answer`,
      input.answerPost
    )
    .then(res => res.data);
}

function* saveAnswersCall(input) {
  try {
    const savedAnswToQuest = yield call(saveAnswersCallApi, input.payload);

    yield put({
      type: SAVE_ANSWERS_TO_QUEST_SUCCEEDED,
      payload: savedAnswToQuest
    });
  } catch (error) {
    yield put({
      type: SAVE_ANSWERS_TO_QUEST_FAILED,
      payload: error.response
    });
  }
}

function fetchAnswersSpqCallApi(input) {
  let url = `${apiUrl.likeFollow}/qna-question/${
    input.slug
    }/answer/?page=${input.page}&length=${input.length}`;

  return fetchClient.get(url).then(res => res.data);
}

function* fetchAnswersSpqCall(input) {
  try {
    const SPQ_ANSWERS = yield call(fetchAnswersSpqCallApi, input.payload);

    yield put({
      type: FETCH_ANSWERS_SPQ_SUCCEEDED,
      payload: SPQ_ANSWERS
    });
  } catch (error) {
    yield put({
      type: FETCH_ANSWERS_SPQ_FAILED,
      payload: error.response
    });
  }
}

function saveCommentsCallApi(input) {
  let url = `${apiUrl.likeFollow}/qna-answer/${input.qnaAnswerId}/comment`;

  return fetchClient.post(url, input).then(res => res.data);
}

function* saveCommentsCall(input) {
  try {
    const POST_COMMENTS = yield call(saveCommentsCallApi, input.payload);

    yield put({
      type: POST_COMMENT_SUCCEEDED,
      payload: POST_COMMENTS
    });
  } catch (error) {
    yield put({
      type: POST_COMMENT_FAILED,
      payload: error.response
    });
  }
}
function saveEditAnswerCallApi(input) {
  let url = `${apiUrl.editAnswer}/qna-question/${input.qnaQuestionId}/answer/${input.id}`


  return fetchClient.post(url, input).then(res => res.data);
}

function* saveEditAnswerCall(input) {
  try {
    const POST_ANSWER = yield call(saveEditAnswerCallApi, input.payload);


    yield put({
      type: POST_ANSWER_SUCCEEDED,
      payload: POST_ANSWER
    });
  } catch (error) {
    yield put({
      type: POST_ANSWER_FAILED,
      payload: error.response
    });
  }
}

function deleteCommentApi(input) {
  let url = `${apiUrl.likeFollow}/qna-answer/comment/${input.commentId}`;

  return fetchClient.delete(url, input).then(res => res.data);
}

function* deleteComment(input) {
  try {
    const DELETE_COMMENT = yield call(deleteCommentApi, input.payload);

    yield put({
      type: DELETE_COMMENT_SUCCEEDED,
      payload: DELETE_COMMENT
    });
  } catch (error) {
    yield put({
      type: DELETE_COMMENT_FAILED,
      payload: error.response
    });
  }
}

export default function* fetchQuestAnswSaga() {
  yield takeEvery(FETCH_QUESTION_ANSWER_REQUESTED, fetchQuestAns);
  yield takeEvery(FETCH_HOME_QUES_ANS_REQUESTED, fetchQuestType);
  yield takeEvery(FETCH_CATEGORY_SLUG_DATA_REQUESTED, fetchCatgSlugCall);
  yield takeEvery(FETCH_RELATED_QUESTION_REQUESTED, fetchRelatedQuestion);
  yield takeEvery(FETCH_AUTO_SUGGEST_REQUESTED, fetchAutoSuggestQuestion);
  yield takeEvery(FETCH_VIEW_COUNTER_REQUESTED, fetchViewCounterCall);
  yield takeEvery(FETCH_CATEGORY_LIST_REQUESTED, fetchCategoryApiCall);
  yield takeEvery(SAVE_LIKE_QA_FOLLOW_REQUESTED, saveLikeFollowCall);
  yield takeEvery(FETCH_QA_LIKE_FOLLOW_REQUESTED, fetchLikeFollowCall);
  yield takeEvery(FETCH_ALL_QA_LIKE_FOLLOW_REQUESTED, fetchAllLikeFollowCall);
  yield takeEvery(SAVE_QUESTION_REQUESTED, saveQuestionDtsCall);
  yield takeEvery(DELETE_ANSWER_REQUESTED, deleteAnswerCall);
  yield takeEvery(SAVE_ANSWERS_TO_QUEST_REQUESTED, saveAnswersCall);
  yield takeEvery(FETCH_ANSWERS_SPQ_REQUESTED, fetchAnswersSpqCall);
  yield takeEvery(POST_COMMENT_REQUESTED, saveCommentsCall);
  yield takeEvery(POST_ANSWER_REQUESTED, saveEditAnswerCall);
  yield takeEvery(DELETE_COMMENT_REQUESTED, deleteComment);
  yield takeEvery(FETCH_QUESTION_REQUESTED, fetchLatestQuestion);
}
