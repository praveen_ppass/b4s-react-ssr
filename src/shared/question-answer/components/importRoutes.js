import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import { imgBaseUrl, messages } from "../../../constants/constants";
import Loader from "../../common/components/loader";
import { pageTemplate } from "../pages/importTemplate";
import gblFunc from "../../../globals/globalFunctions";
import {
  FETCH_CATEGORY_SLUG_DATA_SUCCEEDED,
  FETCH_QUESTION_ANSWER_SUCCEEDED,
  FETCH_RELATED_QUESTION_SUCCEEDED,
  FETCH_CATEGORY_LIST_SUCCEEDED,
  FETCH_ALL_QA_LIKE_FOLLOW_SUCCEEDED,
  FETCH_ALL_QA_LIKE_FOLLOW_FAILED,
  SAVE_LIKE_FOLLOW_SUCCEEDED,
  SAVE_LIKE_QA_FOLLOW_SUCCEEDED,
  FETCH_QA_LIKE_FOLLOW_SUCCEEDED,
  FETCH_QA_LIKE_FOLLOW_FAILED,
  SAVE_QUESTION_SUCCEEDED,
  SAVE_QUESTION_FAILED,
  SAVE_ANSWERS_TO_QUEST_SUCCEEDED,
  FETCH_ANSWERS_SPQ_SUCCEEDED,
  POST_COMMENT_SUCCEEDED,
  SAVE_ANSWERS_TO_QUEST_FAILED,
} from "../actions";
import UserLoginRegistrationPopup from "../../login/containers/userLoginRegistrationContainer";
import moment from "moment";
import {
  LOGIN_USER_SUCCEEDED,
  SOCIAL_LOGIN_USER_SUCCEEDED
} from "../../login/actions";

import QuestionPopup from "./questionPopup";
import { required } from "../../../validation/rules";
import { ruleRunner } from "../../../validation/ruleRunner";
import AlertMessage from "../../common/components/alertMsg";

class ImportRoutes extends Component {
  constructor() {
    super();
    this.state = {
      routeName: "",
      slug: "",
      text: "",
      data: null,
      questionId: null,
      initialLength: 30,
      hasMore: true,
      relatedQuestions: [],
      isCounter: false,
      categories: [],
      categoryIdBySlug: null,
      likeFollowCount: {
        totalFollowed: 0,
        totalLiked: 0
      },
      isLiked: false,
      isFollowed: false,
      showLoginPopup: false,
      hideCloseButton: false,
      hideForgetCloseBtn: false,
      isLikeFollow: false,
      showQuestionPopUp: false,
      postQuestionVal: "",
      selectedOptions: [],
      options: [],
      questData: {
        selectedOptions: null,
        // additionalDts: null,
        questionText: null
      },
      validations: {
        // additionalDetail: null,
        questionText: null,
        qnaCategories: null
      },
      showLoader: false,
      isShow: false,
      msg: "",
      quesStatusErr: false,
      showAnswerPostBtn: false,
      setUniqueQuestId: null,
      answerPost: {
        answerText: null
      },
      singlePageQuesAnswers: [],
      commentList: {
        authorUserId: null,
        commentText: "",
        level: 0,
        parentCommentId: 0,
        qnaAnswerId: null
      }
    };

    this.apiCallHandler = this.apiCallHandler.bind(this);
    this.onLikeFollowHandler = this.onLikeFollowHandler.bind(this);
    this.onLoginPopUpHandler = this.onLoginPopUpHandler.bind(this);
    this.closeLoginPopup = this.closeLoginPopup.bind(this);
    this.onQuestionPopUpHandler = this.onQuestionPopUpHandler.bind(this);
    this.onPostChangeHandler = this.onPostChangeHandler.bind(this);
    this.onCanclePostQuestionPopUp = this.onCanclePostQuestionPopUp.bind(this);
    this.onSelectHandler = this.onSelectHandler.bind(this);
    this.onAdditionDtHandler = this.onAdditionDtHandler.bind(this);
    this.onSubmitQuestionHandler = this.onSubmitQuestionHandler.bind(this);
    this.onSearchPopQuesHandler = this.onSearchPopQuesHandler.bind(this);
    this.onLoginPopUpHandler = this.onLoginPopUpHandler.bind(this);
    this.closeLoginPopup = this.closeLoginPopup.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.onAnswerChangeHandler = this.onAnswerChangeHandler.bind(this);
    this.onPostAnswerSubmitHandler = this.onPostAnswerSubmitHandler.bind(this);
    this.onCommentChangeHandler = this.onCommentChangeHandler.bind(this);
    this.onSubmitCommentHandle = this.onSubmitCommentHandle.bind(this);
    this.showLoginPopup = this.showLoginPopup.bind(this);
    this.showQuesPopUpHandler = this.showQuesPopUpHandler.bind(this);

    this.onCommitEnter = this.onCommitEnter.bind(this);
  }

  componentDidMount() {
    this.apiCallHandler();
  }

  apiCallHandler() {
    const { match } = this.props;

    if (
      match &&
      match.params &&
      match.params.pathName &&
      match.params.pathName == "categories"
    ) {
      const { pathName } = match.params;
      this.setState(
        {
          routeName: pathName,
          slug: match.params.routes,
          text: ""
        },
        () => {
          this.props.fetchCategoryBySlug({
            slug: match.params.routes,
            page: 0,
            length: this.state.initialLength
          });
          this.props.fetchCategoryList({
            page: 0,
            length: 5
          });
        }
      );
    } else {
      if (match.params && match.params.routes) {
        this.setState(
          {
            routeName: "question",
            slug: match.params.routes,
            text: ""
          },
          () =>
            this.props.getQuesAnsw({
              slug: match.params.routes
            })
        );
      }
    }
  }

  componentWillMount() {
    if (breadCrumObj["qcategories"]["breadCrum"].length == 2) {
      breadCrumObj["qcategories"]["breadCrum"].pop();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    switch (type) {
      case FETCH_QA_LIKE_FOLLOW_FAILED:
        this.setState({ isLiked: false, isFollowed: false });
        break;
      case LOGIN_USER_SUCCEEDED:
        this.apiCallHandler();

        break;
      case SOCIAL_LOGIN_USER_SUCCEEDED:
        this.apiCallHandler();
        break;
      case FETCH_CATEGORY_SLUG_DATA_SUCCEEDED:
        let categoryId;
        const { routes } = this.props.match.params;
        if (
          nextProps.categories &&
          nextProps.categories.category &&
          nextProps.categories.category.questions &&
          nextProps.categories.category.questions.data &&
          nextProps.categories.category.questions.data.length > 0
        ) {
          const { data } = nextProps.categories.category.questions;

          categoryId = data.map(catg => {
            if (catg.qnaCategories && catg.qnaCategories.length > 0) {
              return catg.qnaCategories;
            }
          });
          if (categoryId.length > 0) {
            categoryId = categoryId[0].filter(c => {
              return c.slug === routes;
            });
          }
        }

        this.setState(
          {
            categoryIdBySlug: categoryId
              ? categoryId[0] && categoryId[0].id
              : nextProps.categories.category.id,
            isLikeFollow: true,
            data: nextProps.categories,
            categories: nextProps.categoryTags
          },
          () =>
            this.props.loadAllLikeAndFollow({
              id:
                this.state.routeName == "question"
                  ? this.state.questionId
                  : this.state.categoryIdBySlug,
              routeName: this.state.routeName
            })
        );
        break;
      case FETCH_QUESTION_ANSWER_SUCCEEDED:
        let filteredCategories = [];
        if (
          nextProps.singlePageQues &&
          nextProps.singlePageQues.qnaCategories &&
          nextProps.singlePageQues.qnaCategories.length
        ) {
          const { qnaCategories } = nextProps.singlePageQues;
          qnaCategories.filter((catg, index) => {
            if (index <= 5) {
              filteredCategories.push(catg.slug);
            }
          });
        }

        this.setState(
          {
            data: nextProps.singlePageQues,
            questionId: nextProps.singlePageQues.id
          },

          () => {
            this.props.relatedQuestion({
              page: 0,
              length: 1,
              slug: this.state.slug,
              categories: filteredCategories
            });

            this.props.viewCounter({
              qnaQuestionId: this.state.questionId,
              userId: gblFunc.getStoreUserDetails()["userId"]
                ? gblFunc.getStoreUserDetails()["userId"]
                : null
            });
            this.props.getAnswerFromSPQ({
              questionId: 101,
              slug: this.state.slug,
              page: 0,
              length: 5
            });
          }
        );
        break;
      case FETCH_RELATED_QUESTION_SUCCEEDED:
        this.setState(
          {
            isLikeFollow: true,
            relatedQuestions: nextProps.relatedQuestions
          },
          () =>
            this.props.loadAllLikeAndFollow({
              id:
                this.state.routeName == "question"
                  ? this.state.questionId
                  : this.state.categoryIdBySlug,
              routeName: this.state.routeName
            })
        );
        break;

      case FETCH_ALL_QA_LIKE_FOLLOW_SUCCEEDED:
        const { allLikeFollowDts } = nextProps;
        this.setState(
          {
            likeFollowCount: allLikeFollowDts
          },
          () => {
            if (this.state.isLikeFollow) {
              this.props.loadLikeAndFollow({
                routeName: this.state.routeName,
                id:
                  this.state.routeName == "question"
                    ? this.state.questionId
                    : this.state.categoryIdBySlug,
                userId: gblFunc.getStoreUserDetails()["userId"]
              });
            }
          }
        );
        break;

      case FETCH_ALL_QA_LIKE_FOLLOW_FAILED:
        break;
      case SAVE_LIKE_QA_FOLLOW_SUCCEEDED:
        this.props.loadLikeAndFollow({
          routeName: this.state.routeName,
          id:
            this.state.routeName == "question"
              ? this.state.questionId
              : this.state.categoryIdBySlug,
          userId: gblFunc.getStoreUserDetails()["userId"]
        });

        break;
      case FETCH_QA_LIKE_FOLLOW_SUCCEEDED:
        const { likeFollowDts } = nextProps;

        this.setState(
          {
            isLikeFollow: false,
            isLiked: likeFollowDts && likeFollowDts.liked,
            isFollowed: likeFollowDts && likeFollowDts.follow
          },
          () =>
            this.props.loadAllLikeAndFollow({
              routeName: this.state.routeName,
              id:
                this.state.routeName == "question"
                  ? this.state.questionId
                  : this.state.categoryIdBySlug
            })
        );

        break;
      case FETCH_CATEGORY_LIST_SUCCEEDED:
        const { categoryTags } = nextProps;

        if (categoryTags.data.length > 0) {
          this.setState({
            options: categoryTags.data
          });
        }

        break;
      case SAVE_QUESTION_SUCCEEDED:
        this.setState({
          msg: "Question posted successfully",
          isShow: true,
          showLoader: true,
          status: true,
          showQuestionPopUp: false,
          postQuestionVal: "",
          selectedOptions: [],
          validations: {
            questionText: null,
            qnaCategories: null,
            // additionalDetail: null
          }
        });
        if (nextProps.data1.slug) {
          this.props.history.push({
            pathname: `/qna/${nextProps.data1.slug}`
          });
        }
        break;
      case SAVE_QUESTION_FAILED:
        const { data } = nextProps.errorMessage;
        const { generic } = messages;

        this.setState({
          msg:
            generic.httpErrors[data.errorCode] && data.errorCode !== 702
              ? generic.httpErrors[data.errorCode]
              : "Question is already posted",
          quesStatusErr: true
          // isShow: true,
          // showLoader: true,
          // status: false
        });

      case SAVE_ANSWERS_TO_QUEST_SUCCEEDED:
        this.setState(
          {
            msg: "Answer Successfully Posted",
            isShow: true,
            showLoader: true,
            status: true,
            answerPost: {
              answerText: ""
            },
            showAnswerPostBtn: false
          },
          () => this.apiCallHandler()
        );
        break;

      case FETCH_ANSWERS_SPQ_SUCCEEDED:
        this.setState({
          singlePageQuesAnswers: nextProps.singlePageQuestAnsws
        });

        break;
      case SAVE_ANSWERS_TO_QUEST_FAILED:
        this.setState({
          msg: generic.httpErrors[data.errorCode],
          isShow: true,
          showLoader: true,
          status: false
        });
        break;
      case POST_COMMENT_SUCCEEDED:
        this.setState({
          commentList: {
            authorUserId: null,
            commentText: "",
            level: 0,
            parentCommentId: 0,
            qnaAnswerId: null
          }
        });
        break;
      default:
        break;
    }
  }

  onLikeFollowHandler(likeOrFollow) {
    switch (likeOrFollow) {
      case "Like":
        if (!this.state.isFollowed && !this.state.isLiked) {
          this.setState(
            {
              isLiked: !this.state.isLiked,
              isFollowed: !this.state.isFollowed
            },
            () =>
              this.props.setLikeAndFollow({
                routeName: this.state.routeName,
                id:
                  this.state.routeName == "categories"
                    ? this.state.categoryIdBySlug
                    : this.state.questionId,
                userId: gblFunc.getStoreUserDetails()["userId"],
                qnaCategoryFollowDto: {
                  liked: this.state.isLiked,
                  follow: this.state.isFollowed
                }
              })
          );
        }
        else {
          this.setState(
            {
              isLiked: !this.state.isLiked
            },

            () =>
              this.props.setLikeAndFollow({
                routeName: this.state.routeName,
                id:
                  this.state.routeName == "categories"
                    ? this.state.categoryIdBySlug
                    : this.state.questionId,
                userId: gblFunc.getStoreUserDetails()["userId"],
                qnaCategoryFollowDto: {
                  liked: this.state.isLiked,
                  // follow: this.state.isFollowed
                }
              })
          )
        }

        break;

      case "Follow":
        this.setState(
          {
            isFollowed: !this.state.isFollowed
          },
          () =>
            this.props.setLikeAndFollow({
              routeName: this.state.routeName,
              id:
                this.state.routeName == "categories"
                  ? this.state.categoryIdBySlug
                  : this.state.questionId,
              userId: gblFunc.getStoreUserDetails()["userId"],
              qnaCategoryFollowDto: {
                follow: this.state.isFollowed
              }
            })
        );
        break;

      // default:
      //   break;
    }
  }

  onLoginPopUpHandler() {
    this.setState({
      showLoginPopup: true
    });
  }

  closeLoginPopup(e) {
    //e.preventDefault();
    this.setState({
      showLoginPopup: false
    });
  }

  onPostChangeHandler({ target }) {
    const questData = { ...this.state };
    questData.questionText = target.value;
    const isAuth = gblFunc.isUserAuthenticated() || this.props.isAuthenticated;

    // if (isAuth) {
    if (target.value) {
      this.setState(
        {
          postQuestionVal: target.value,
          questData
        },
        () => this.showQuesPopUpHandler()
      );
    } else {
      this.showQuesPopUpHandler();
    }
    // }
    //  else if (!isAuth) {
    //   this.setState({
    //     showLoginPopup: true
    //   });
    // } 
    // else {
    //   this.setState({
    //     postQuestionVal: target.value,
    //     questData
    //   });
    // }
  }

  onQuestionPopUpHandler(e) {
    e.preventDefault();
    this.showQuesPopUpHandler();
    // const isAuth = gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    // if (isAuth) {
    //   this.showQuesPopUpHandler();
    // } else {
    //   this.setState({
    //     showLoginPopup: true
    //   });
    // }
    // this.setState({
    //   showLoginPopup: true
    // });
  }

  showQuesPopUpHandler() {
    const { showQuestionPopUp } = this.state;

    if (!showQuestionPopUp) {
      this.setState(
        {
          showQuestionPopUp: true
        },
        () => this.props.loadCatgList({ page: 0, length: 100 })
      );
    }
  }

  onCanclePostQuestionPopUp() {
    this.setState({
      showQuestionPopUp: false,
      selectedOptions: [],
      postQuestionVal: "",
      validations: {
        questionText: null,
        qnaCategories: null,
        // additionalDetail: null
      },
      questData: {
        selectedOptions: null,
        // additionalDts: null,
        questionText: null
      }
    });
  }

  onSelectHandler(selectedOptions) {
    this.setState({
      selectedOptions
    });
  }

  onAdditionDtHandler({ target }) {
    const updatedQuesData = { ...this.state.questData };
    updatedQuesData[target.name] = target.value;
    this.setState({
      questData: updatedQuesData
    });
  }

  onSearchPopQuesHandler(value) {
    const questData = { ...this.state.questData };

    questData["questionText"] = value;

    this.setState({ questData });
  }

  onSubmitQuestionHandler(e) {
    e.preventDefault();
    const { selectedOptions, questData } = this.state;
    var selectedOptionsTag = [];
    const data1 = {
      id: 99,
      text: "Miscellaneous Questions",
      slug: "na",
      status: 1,
      weight: 1,
    };

    // selectedOptionsTag[0]= this.props.categoryTags.data[56];
    selectedOptionsTag[0] = data1;

    const data = {
      qnaCategories: selectedOptionsTag,
      questionText: questData && questData.questionText,
      // additionalDetail: questData && questData.additionalDts
    };
    const isAuth = gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    // if (!isAuth) {
    //   this.setState({
    //     showLoginPopup: true
    //   });
    // }
    // else{
    if (this.checkFormValidations(data)) {
      if (isAuth) {
        data.authorUserId = gblFunc.getStoreUserDetails()["userId"];
        this.props.postQuestion(data);

      }
      else {
        this.setState({
          showLoginPopup: true
        });
      }
    }
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "qnaCategories":
        validationObject.name = "* Category";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "questionText":
        validationObject.name = "* Question";
        validationObject.validationFunctions = [required];
        return validationObject;
      // case "additionalDetail":
      //   validationObject.name = "* Additional Detail";
      //   validationObject.validationFunctions = [required];
      //   return validationObject;
    }
  }

  checkFormValidations(questData) {
    let validations = { ...this.state.validations };

    let isFormValid = true;

    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let value;
      if (
        key == "qnaCategories" &&
        questData[key] &&
        questData[key].length == 0
      ) {
        value = "";
      } else if (
        key == "qnaCategories" &&
        questData[key] &&
        questData[key].length > 0
      ) {
        value = true;
      } else {
        value = questData[key];
      }

      let validationResult = ruleRunner(
        value,
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations
    });
    return isFormValid;
  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
  }

  onAnswerChangeHandler(
    { target },
    questId,
    updateAnswerFromEditor = "",
    type = ""
  ) {
    const updateAnswer = { ...this.state.answerPost };
    if (type == "editor") {
      updateAnswer["answerText"] = updateAnswerFromEditor;
      return this.setState({
        showAnswerPostBtn: true,
        answerPost: updateAnswer,
        setUniqueQuestId: questId
      });
    }
    if (target.value.length > 0) {
      updateAnswer[target.name] = target.value;
      this.setState({
        showAnswerPostBtn: true,
        answerPost: updateAnswer,
        setUniqueQuestId: questId
      });
    } else {
      this.setState({
        setUniqueQuestId: null,
        answerPost: { ...this.state.answerPost, answerText: null },
        showAnswerPostBtn: false
      });
    }
  }

  onPostAnswerSubmitHandler(e, origin = "") {
    e.preventDefault();

    const { answerPost } = this.state;

    answerPost["authorUserId"] = gblFunc.getStoreUserDetails()["userId"];

    answerPost.status = 1;

    answerPost["qnaQuestionId"] = this.state.setUniqueQuestId;

    const answers = {
      answerPost,
      questionId: this.state.setUniqueQuestId
    };
    this.props.postAnsToQues(answers);
  }

  onCommentChangeHandler({ target }, answerId, updateAnswerFromEditor = "", type = "") {
    // const { parentCommentId, qnaAnswerId } = commentObj;
    if (type == "editor4") {
      const updateCommentList = { ...this.state.commentList };
      updateCommentList["commentText"] = updateAnswerFromEditor;
      updateCommentList["qnaAnswerId"] = answerId;

      return this.setState({
        showAnswerPostBtn: true,
        commentList: updateCommentList,
      });
    }

    // if (target.value && target.value.length > 0) {
    //   const updateCommentList = { ...this.state.commentList };
    //   updateCommentList[target.name] = target.value;
    //   updateCommentList["qnaAnswerId"] = answerId;
    //   this.setState({
    //     commentList: updateCommentList
    //   });
    // } 
    // else {
    //   this.setState({
    //     commentList: {
    //       ...this.state.commentList,
    //       commentText: target.value.trim()
    //     }
    //   });
    // }
  }

  onCommitEnter(e) {
    // if (e.key === "Enter" && !e.shiftKey) {
    e.preventDefault();
    const { commentList } = this.state;
    if (
      commentList &&
      commentList.commentText &&
      commentList.commentText.trim().length > 0
    ) {
      this.onSubmitCommentHandle();
    }
    // }
  }

  onSubmitCommentHandle() {
    // e.preventDefault();
    const isAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;

    if (isAuthenticated) {
      const commentList = { ...this.state.commentList };
      commentList["authorUserId"] = gblFunc.getStoreUserDetails()["userId"];
      // commentList[""];

      this.props.saveComments(commentList);
    } else {
      this.setState({
        showLoginPopup: true
      });
    }
  }

  showLoginPopup() {
    this.setState({
      showLoginPopup: true
    });
  }

  render() {
    const { match } = this.props;
    const {
      postQuestionVal,
      selectedOptions,
      options,
      questData,
      validations,
      quesStatusErr,
      msg
    } = this.state;
    const propsStates = {
      postQuestionVal,
      selectedOptions,
      options,
      questData,
      validations,
      quesStatusErr,
      msg
    };
    if (
      breadCrumObj["qcategories"]["breadCrum"].length == 1 &&
      match &&
      match.params &&
      match.params.routes
    ) {
      breadCrumObj["qcategories"]["breadCrum"].push({
        url: "#",
        name: match.params.routes
          ? match.params.routes
            .replace(/\b[a-z]/, function () {
              return arguments[0].toUpperCase();
            })
            .replace(/-/g, " ")
          : ""
      });
    }
    const isAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    return (
      <section>
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <Loader isLoader={this.props.showLoader} />


        <section>
          {this.state.showQuestionPopUp ? (
            <QuestionPopup
              {...this.props}
              onSelectHandler={this.onSelectHandler}
              onCanclePostQuestionPopUp={this.onCanclePostQuestionPopUp}
              {...propsStates}
              onSubmitQuestionHandler={this.onSubmitQuestionHandler}
              onAdditionDtHandler={this.onAdditionDtHandler}
              onSearchPopQuesHandler={this.onSearchPopQuesHandler}
            />
          ) : null}
        </section>
        {this.state.showLoginPopup ? (
          <UserLoginRegistrationPopup
            closePopup={this.closeLoginPopup}
            hideCloseButton={this.state.hideCloseButton}
            hideForgetCloseBtn={this.state.hideForgetCloseBtn}
          />
        ) : null}

        <section className="qna qnacate">
          <BreadCrum
            classes={breadCrumObj["qcategories"]["bgImage"]}
            listOfBreadCrum={breadCrumObj["qcategories"]["breadCrum"]}
            title={breadCrumObj["qcategories"]["title"]}
            hideBanner={breadCrumObj["qcategories"]["hideBanner"]}
            subTitle={breadCrumObj["qcategories"]["subTitle"]}
          />

          <section className="container-fluid qnaWrapper">
            <section className="container">
              <section className="row">
                <ImportsRoutes
                  {...this.state}
                  isAuthenticated={isAuthenticated}
                  onLikeFollowHandler={this.onLikeFollowHandler}
                  onLoginPopUpHandler={this.onLoginPopUpHandler}
                  onPostChangeHandler={this.onPostChangeHandler}
                  onQuestionPopUpHandler={this.onQuestionPopUpHandler}
                  onAnswerChangeHandler={this.onAnswerChangeHandler}
                  onPostAnswerSubmitHandler={this.onPostAnswerSubmitHandler}
                  onCommentChangeHandler={this.onCommentChangeHandler}
                  onCommitEnter={this.onCommitEnter}
                  onSubmitCommentHandle={this.onSubmitCommentHandle}
                  showLoginPopup={this.showLoginPopup}
                  {...this.props}
                />
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

const ImportsRoutes = props => {
  let PageRoute = null;
  switch (props.routeName) {
    case "question":
      PageRoute = pageTemplate["SinglePageQuestion"];
      break;
    case "categories":
      PageRoute = pageTemplate["Categories"];
      break;
    default:
      break;
  }

  return PageRoute ? <PageRoute {...props} /> : null;
};

export default ImportRoutes;
