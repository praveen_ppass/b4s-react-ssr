import React from "react";
import AutoSuggestQuesPopupSearch from "../pages/searchQuestionPopup";
import MultiSelectCatg from "../pages/multiSelectCategory";

// const questionPopup = props => {
  class questionPopup extends React.Component {

    constructor(props) {
      super(props);
  
      this.state = {
       value: "", 
        // suggestions: []
      };
      this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
      console.log(e.target)
      this.setState(
        {
          value: e.target.value
        },
        () => this.props.onSearchPopQuesHandler(this.state.value)
      );
    }


  render(){
      const props=this.props;

  return (
    <article className="popup qnapopup">
      <article className="popupsms">
        <article className="popup_inner loginpopup">
          <article className="LoginRegPopup qnapopup">
            <section className="modal fade modelAuthPopup1">
              <section className="modal-dialog">
                <section className="modal-content modelBg">
                  <article className="modal-header">
                    <button
                      type="button"
                      onClick={props.onCanclePostQuestionPopUp}
                      className="close btnPos"
                    >
                      <i>&times;</i>
                    </button>
                  </article>

                  <article className="modal-body">
                    <article className="row">
                      <article className="col-md-12">
                        <h4>Ask your question</h4>
                        <p>
                          <i className="fa fa-check" aria-hidden="true" />Get
                          answers within 24 hours.
                        </p>

                        <form
                          onSubmit={props.onSubmitQuestionHandler}
                          autoCapitalize="off"
                          className="formelemt"
                        >
                          <section className="ctrl-wrapper">
                            <article className="form-group">
                              {/* <AutoSuggestQuesPopupSearch {...props} />

                              {props.validations["questionText"] ? (
                                <span className="error animated bounce">
                                  {props.validations["questionText"]}
                                </span>
                              ) : null} */}
                               <textarea autocomplete="off" aria-autocomplete="list" aria-controls="react-autowhatever-1" class="react-autosuggest__input"
                               onChange={(e)=>this.onChange(e)} value={this.state.value}
                               placeholder="Type your question"  rows="5"></textarea>
                            </article>
                          </section>
                          {/* <section className="ctrl-wrapper">
                            <article className="form-group">
                              <MultiSelectCatg {...props} />
                              {props.validations["qnaCategories"] ? (
                                <span className="error animated bounce">
                                  {props.validations["qnaCategories"]}
                                </span>
                              ) : null}
                            </article>
                          </section> */}
                          {/* <section className="ctrl-wrapper">
                            <article className="form-group textAreaCrtl">
                              <textarea
                                className="form-control"
                                rows="4"
                                placeholder="Any additional details"
                                id="detail"
                                name="additionalDts"
                                required=""
                                value={props.questData.additionalDts}
                                onChange={props.onAdditionDtHandler}
                              />
                              {props.validations["additionalDetail"] ? (
                                <span className="error animated bounce">
                                  {props.validations["additionalDetail"]}
                                </span>
                              ) : null}
                            </article>
                          </section> */}
                          <article className="pull-right">
                            <input
                              className="btn"
                              value="Close"
                              onClick={props.onCanclePostQuestionPopUp}
                            />
                            <input type="submit" className="btn" value="Post" />
                          </article>
                          {props.quesStatusErr ? (
                            <article className="pull-left already">
                              <p>
                                {" "}
                                {!!props.msg? 
                                <i className="fa fa-exclamation-triangle" />
                                  : ""
                              }
                                {props.msg}
                              </p>{" "}
                            </article>
                          ) : null}
                        </form>
                      </article>
                    </article>
                  </article>
                </section>
              </section>
            </section>
          </article>
        </article>
      </article>
    </article>
  );   
 }
};

export default questionPopup;
