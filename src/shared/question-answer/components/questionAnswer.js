import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import Loader from "../../common/components/loader";
import { imgBaseUrl, messages } from "../../../constants/constants";
import AutoSuggestSearch from "../pages/search";
import gblFunc from "../../../globals/globalFunctions";
import QuestionPopup from "./questionPopup";
import {
  FETCH_CATEGORY_LIST_SUCCEEDED,
  SAVE_QUESTION_SUCCEEDED,
  SAVE_QUESTION_FAILED
} from "../actions";
import UserLoginRegistrationPopup from "../../login/containers/userLoginRegistrationContainer";
import { required } from "../../../validation/rules";
import { ruleRunner } from "../../../validation/ruleRunner";
import AlertMessage from "../../common/components/alertMsg";
class QuestionAnswer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quesAnsResponse: null,
      internationalSch: null,
      latestQuestions: [],
      LatestTotal: null,
      loadPageCount: 0,
      isTotal: false,
      showQuestionPopUp: false,
      postQuestionVal: "",
      selectedOptions: [],
      options: [],
      questData: {
        selectedOptions: null,
        // additionalDts: null,
        questionText: null
      },
      showLoginPopup: false,
      validations: {
        // additionalDetail: null,
        questionText: null,
        qnaCategories: null
      },
      showLoader: false,
      isShow: false,
      msg: "",
      status: false,
      quesStatusErr: false
    };

    this.onMsgHandler = this.onMsgHandler.bind(this);
    this.onQuestionPopUpHandler = this.onQuestionPopUpHandler.bind(this);
    this.onPostChangeHandler = this.onPostChangeHandler.bind(this);
    this.onCanclePostQuestionPopUp = this.onCanclePostQuestionPopUp.bind(this);
    this.onSelectHandler = this.onSelectHandler.bind(this);
    this.onAdditionDtHandler = this.onAdditionDtHandler.bind(this);
    this.onSubmitQuestionHandler = this.onSubmitQuestionHandler.bind(this);
    this.onSearchPopQuesHandler = this.onSearchPopQuesHandler.bind(this);
    this.onLoginPopUpHandler = this.onLoginPopUpHandler.bind(this);
    this.closeLoginPopup = this.closeLoginPopup.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.showQuesPopUpHandler = this.showQuesPopUpHandler.bind(this);
  }

  componentDidMount() {
    const { loadPageCount } = this.state;
    this.props.fetchQuesAnsType({
      page: 0,
      length: 5
    });
    this.props.fetchQues({
      page: loadPageCount,
      length: 10
    })
  }

  componentWillReceiveProps(nextProps) {
    const { type, relatedQusetionData } = nextProps;
    switch (type) {
      case "FETCH_QUESTION_SUCCEEDED":
        var arrayData;
        if (relatedQusetionData && relatedQusetionData.data && relatedQusetionData.data.length > 0) {
          arrayData = this.state.latestQuestions.concat(relatedQusetionData.data);
        }
        this.setState({
          latestQuestions: arrayData,
          LatestTotal: relatedQusetionData.total
        })
        break;
      case "FETCH_HOME_QUES_ANS_SUCCEEDED":
        this.setState(
          {
            quesAnsResponse: nextProps.homeQuesAnswer
          },
          () => {
            this.props.loadScholarships({
              page: 0,
              length: 10,
              rules: [{ rule: [698] }],
              mode: "OPEN"
            });

            // this.props.loadLikeAndFollow({
            //   userId: gblFunc.getStoreUserDetails()["userId"],
            //   categoryId
            // });
          }
        );
        break;
      case "FETCH_SCHOLARSHIPS__SUCCEEDED":
        this.setState({
          internationalSch: nextProps.scholarshipList
        });
        break;
      case FETCH_CATEGORY_LIST_SUCCEEDED:
        const { categoryTags } = nextProps;

        if (categoryTags.data.length > 0) {
          this.setState({
            options: categoryTags.data
          });
        }

        break;
      case SAVE_QUESTION_SUCCEEDED:
        this.setState({
          msg: "Question posted successfully",
          isShow: true,
          showLoader: true,
          status: true,
          showQuestionPopUp: false,
          postQuestionVal: "",
          selectedOptions: [],
          validations: {
            questionText: null,
            qnaCategories: null,
            // additionalDetail: null
          }
        });
        if (nextProps.data1.slug) {
          this.props.history.push({
            pathname: `/qna/${nextProps.data1.slug}`
          });
        }

        break;
      case SAVE_QUESTION_FAILED:
        const { data } = nextProps.errorMessage;
        const { generic } = messages;
        this.setState({
          msg:
            generic.httpErrors[data.errorCode] && data.errorCode !== 702
              ? generic.httpErrors[data.errorCode]
              : "Question is already posted",
          quesStatusErr: true
          // isShow: true,
          // showLoader: true,
          // status: false
        });
        break;
      default:
        break;
    }
  }

  onMsgHandler(total, length) {
    const ctgLength = length + 5;

    this.setState(
      {
        isTotal: total == length ? true : false
      },
      () =>
        this.props.fetchQuesAnsType({
          page: 0,
          length: ctgLength
        })
    );
  }

  onPostChangeHandler({ target }) {
    const questData = { ...this.state };
    questData.questionText = target.value;
    const isAuth = gblFunc.isUserAuthenticated() || this.props.isAuthenticated;

    if (isAuth) {
      if (target.value) {
        this.setState(
          {
            postQuestionVal: target.value,
            questData
          },
          () => this.showQuesPopUpHandler()
        );
      } else {
        this.showQuesPopUpHandler();
      }
    } else if (!isAuth) {
      this.setState({
        showLoginPopup: true
      });
    } else {
      this.setState({
        postQuestionVal: target.value,
        questData
      });
    }
  }

  onQuestionPopUpHandler(e) {
    e.preventDefault();

    const isAuth = gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    if (isAuth) {
      this.showQuesPopUpHandler();
    } else {
      this.setState({
        showLoginPopup: true
      });
    }
  }

  showQuesPopUpHandler() {
    const { showQuestionPopUp } = this.state;

    if (!showQuestionPopUp) {
      this.setState(
        {
          showQuestionPopUp: true
        },
        () => this.props.loadCatgList({ page: 0, length: 100 })
      );
    }
  }

  onCanclePostQuestionPopUp() {
    this.setState({
      showQuestionPopUp: false,
      selectedOptions: [],
      postQuestionVal: "",
      msg: "",
      validations: {
        questionText: null,
        qnaCategories: null,
        // additionalDetail: null
      },
      questData: {
        selectedOptions: null,
        // additionalDts: null,
        questionText: null
      }
    });
  }

  onSelectHandler(selectedOptions) {
    this.setState({
      selectedOptions
    });
  }

  onAdditionDtHandler({ target }) {
    const updatedQuesData = { ...this.state.questData };
    updatedQuesData[target.name] = target.value;
    this.setState({
      questData: updatedQuesData
    });
  }

  onSearchPopQuesHandler(value) {
    const questData = { ...this.state.questData };

    questData["questionText"] = value;

    this.setState({ questData });
  }

  onLoginPopUpHandler() {
    this.setState({
      showLoginPopup: true
    });
  }

  closeLoginPopup(e) {
    // e.preventDefault();
    this.setState({
      showLoginPopup: false
    });
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "qnaCategories":
        validationObject.name = "* Category";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "questionText":
        validationObject.name = "* Question";
        validationObject.validationFunctions = [required];
        return validationObject;
      // case "additionalDetail":
      //   validationObject.name = "* Additional Detail";
      //   validationObject.validationFunctions = [required];
      //   return validationObject;
    }
  }

  checkFormValidations(questData) {
    let validations = { ...this.state.validations };

    let isFormValid = true;

    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);

      let value;
      if (
        // key == "qnaCategories" &&
        questData[key] &&
        questData[key].length == 0
      ) {
        value = "";
      } else if (
        // key == "qnaCategories" &&
        questData[key] &&
        questData[key].length > 0
      ) {
        value = true;
      } else {
        value = questData[key];
      }

      let validationResult = ruleRunner(
        value,
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations
    });
    return isFormValid;
  }

  onSubmitQuestionHandler(e) {
    e.preventDefault();
    const { selectedOptions, questData } = this.state;
    var selectedOptionsTag = [];
    const data1 = {
      id: 99,
      text: "Miscellaneous Questions",
      slug: "na",
      status: 1,
      weight: 1,
    };

    // selectedOptionsTag[0]= this.props.categoryTags.data[56];
    selectedOptionsTag[0] = data1;
    const data = {
      qnaCategories: selectedOptionsTag,
      questionText: questData && questData.questionText,
      // additionalDetail: questData && questData.additionalDts
    };
    const isAuth = gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    if (this.checkFormValidations(data) ) {
      if(isAuth){
      data.authorUserId = gblFunc.getStoreUserDetails()["userId"];
        this.props.postQuestion(data);

      }
      else{
        this.setState({
          showLoginPopup: true
        })

      }
    }
    // if(!isAuth){
      
    // }
    // if (this.checkFormValidations(data)) {
    //   data.authorUserId = gblFunc.getStoreUserDetails()["userId"];
    //   this.props.postQuestion(data);
    // }
  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
  }

  render() {
    const {
      postQuestionVal,
      selectedOptions,
      options,
      questData,
      validations,
      quesStatusErr,
      msg,
      latestQuestions,
      LatestTotal
    } = this.state;
    const propsStates = {
      postQuestionVal,
      selectedOptions,
      options,
      questData,
      validations,
      quesStatusErr,
      msg
    };
    if (this.state.quesAnsResponse &&
      this.state.quesAnsResponse.questAnsType &&
      this.state.quesAnsResponse.questAnsType.length) {
      var popularTopicsList = this.state.quesAnsResponse.questAnsType;
      var index = popularTopicsList.findIndex(x => x.slug === "international-scholarships");
      if (index >= 0) {
        popularTopicsList.splice(index, 1);
      }
    }

    return (
      <section>
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <Loader isLoader={this.props.showLoader} />
        <section>
          {this.state.showQuestionPopUp ? (
            <QuestionPopup
              {...this.props}
              {...propsStates}
              onSelectHandler={this.onSelectHandler}
              onCanclePostQuestionPopUp={this.onCanclePostQuestionPopUp}
              onSubmitQuestionHandler={this.onSubmitQuestionHandler}
              onAdditionDtHandler={this.onAdditionDtHandler}
              onSearchPopQuesHandler={this.onSearchPopQuesHandler}
            />
          ) : null}
        </section>

        {this.state.showLoginPopup ? (
          <UserLoginRegistrationPopup
            closePopup={this.closeLoginPopup}
            hideCloseButton={false}
            hideForgetCloseBtn={false}
          />
        ) : null}
        <section className="qna">
          <section className="container-fluid layerPos">
            <section className="container">
              <section className="row">
                <article className="col-md-12">
                  <section className="search-wraper">
                    <AutoSuggestSearch {...this.props} />
                    <button
                      className="ask"
                      // onClick={() => {
                      //   const isAuth =
                      //     gblFunc.isUserAuthenticated() ||
                      //     this.props.isAuthenticated;
                      //   if (isAuth) {
                      //     this.setState({ showQuestionPopUp: true }, () =>
                      //       this.props.loadCatgList({ page: 0, length: 100 })
                      //     );
                      //   } else {
                      //     this.setState({
                      //       showLoginPopup: true
                      //     });
                      //   }
                      // }}
                      onClick={()=>{
                        this.setState({
                          showQuestionPopUp:true
                        })
                      }}
                    >
                      Ask a Question
                    </button>
                  </section>
                </article>
              </section>
            </section>
          </section>

          <BreadCrum
            classes={breadCrumObj["qna"]["bgImage"]}
            listOfBreadCrum={breadCrumObj["qna"]["breadCrum"]}
            title={breadCrumObj["qna"]["title"]}
            subTitle={breadCrumObj["qna"]["subTitle"]}
          />

          <section className="container-fluid qnaWrapper">
            <section className="container">
              <section className="row">
                <section className="col-md-8 col-sm-8 col-xs-12">
                  <h3>Popular Topics</h3>
                </section>
                <section className="col-md-4 col-sm-4 col-xs-12 search-wraper">
                  <AutoSuggestSearch {...this.props} />
                </section>
              </section>
              <section className="row">
                <section className="col-md-8 col-sm-8 col-xs-12">
                  <section className="row">
                    {this.state.quesAnsResponse &&
                      this.state.quesAnsResponse.questAnsType &&
                      this.state.quesAnsResponse.questAnsType.length
                      ? popularTopicsList.map(
                        (list, i) => (
                          <section
                            key={i}
                            className="col-md-6 col-sm-6 col-xs-12"
                          >
                            <article className="boxleft">
                              <h4>
                                {list.text
                                  ? list.text.replace(/\b[a-z]/g, function () {
                                    return arguments[0].toUpperCase();
                                  })
                                  : ""}{" "}
                              </h4>

                              {list.questions && list.questions.length
                                ? list.questions
                                  .filter((l, i) => i <= 4)
                                  .map(ques => (
                                    <p key={list.id}>
                                      {" "}
                                      <Link
                                        {...this.props}
                                        to={{
                                          pathname: `/qna/${ques.slug}`,
                                          state: {
                                            id: list.id
                                          }
                                        }}
                                      >
                                        {ques.questionText}
                                      </Link>{" "}
                                    </p>
                                  ))
                                : null}

                              <Link
                                {...this.props}
                                to={{
                                  pathname: `/qna/categories/${list.slug}`
                                }}
                                className="view"
                              >
                                View all questions
                                </Link>
                            </article>
                          </section>
                        )
                      )
                      : null}
                  </section>
                </section>
                <section className="col-md-4 col-sm-4 col-xs-12">
                  {this.state.quesAnsResponse &&
                    this.state.quesAnsResponse.questAnsCatg &&
                    this.state.quesAnsResponse.questAnsCatg.data &&
                    this.state.quesAnsResponse.questAnsCatg.data.length ? (
                      <article className="boxleft box">
                        <h4>Categories</h4>
                        <article className="scrollbar scrollbarColor">
                          {this.state.quesAnsResponse.questAnsCatg.data.map(
                            cat => (
                              <p key={cat.id}>
                                {" "}
                                <Link
                                  {...this.props}
                                  to={{
                                    pathname: `/qna/categories/${cat.slug}`
                                  }}
                                >
                                  {" "}
                                  {cat.text}
                                </Link>
                              </p>
                            )
                          )}
                          {this.state.isTotal ? null : (
                            <span>
                              <i
                                onClick={() =>
                                  this.onMsgHandler(
                                    this.state.quesAnsResponse.questAnsCatg.total,
                                    this.state.quesAnsResponse.questAnsCatg.data
                                      .length
                                  )
                                }
                                className="fa fa-angle-double-down"
                              />
                            </span>
                          )}
                        </article>
                      </article>
                    ) : null}

                  {/* {this.state.internationalSch &&
                    this.state.internationalSch.length ? (
                      <article className="boxleft box">
                        <h4>International Scholarships</h4>
                        {this.state.internationalSch.map(intnSch => (
                          <p>
                            <Link
                              {...this.props}
                              to={`/scholarship/${intnSch.slug}`}
                            >
                              {" "}
                              {intnSch.scholarshipName}{" "}
                            </Link>
                          </p>
                        ))}
                        <Link
                          {...this.props}
                          to={`/scholarship-for/International`}
                          className="view"
                        >
                          View all scholarships
                      </Link>
                    </article>
                  ) : null} */}
                </section>
                {/* Latest Questions */}
                <section className="col-md-4 col-sm-4 col-xs-12">
                  {latestQuestions &&
                    latestQuestions.length > 0 ? (
                      <article className="boxleft box">
                        <h4>Latest Questions</h4>
                        <article className="scrollbar la scrollbarColor">
                          {latestQuestions.map(
                            (item, index) => (
                              <p key={`${index}_${item.id}`}>
                                {" "}
                                <Link
                                  {...this.props}
                                  to={{
                                    pathname: `/qna/${item.slug}`
                                  }}
                                >
                                  {" "}
                                  {item.questionText}
                                </Link>
                              </p>
                            )
                          )}
                          {/* {LatestTotal !== latestQuestions.length && LatestTotal > 10 ? (
                            <span onClick={() => {
                              const { loadPageCount, LatestTotal, latestQuestions } = this.state;
                              if (LatestTotal > latestQuestions.length) {
                                this.setState({
                                  loadPageCount: loadPageCount + 1
                                }, () => {
                                  this.props.fetchQues({
                                    page: loadPageCount + 1,
                                    length: 5
                                  })
                                })
                              }
                            }} >
                              <i className="fa fa-angle-double-down"
                              />
                            </span>
                          ) : ""} */}
                        </article>
                      </article>
                    ) : null}

                  {/* {this.state.internationalSch &&
                    this.state.internationalSch.length ? (
                      <article className="boxleft box">
                        <h4>International Scholarships</h4>
                        {this.state.internationalSch.map(intnSch => (
                          <p>
                            <Link
                              {...this.props}
                              to={`/scholarship/${intnSch.slug}`}
                            >
                              {" "}
                              {intnSch.scholarshipName}{" "}
                            </Link>
                          </p>
                        ))}
                        <Link
                          {...this.props}
                          to={`/scholarship-for/International`}
                          className="view"
                        >
                          View all scholarships
                      </Link>
                    </article>
                  ) : null} */}
                </section>

              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default QuestionAnswer;
