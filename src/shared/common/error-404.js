import React from "react";
import { Link } from "react-router-dom";
import { imgBaseUrl } from "../../constants/constants";
const error = () => (
  <section>
    <section className="errorbggreay-new">
      <section className="section">
        <section className="container errorPageContainer">
          <span className="numberText">
            4<i>0</i>4
          </span>
          <h2>Sorry, the page you requested was not found.</h2>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            {/*<li>
              <Link to="/qna">View all questions</Link>
            </li>*/}
            <li>
              <Link to="/faq/10-scholarships-class-10-12-passed-students">
                Scholarships for Class 10 and 12 passed students
              </Link>
            </li>
            <li>
              <Link to="/faq/scholarship-graduate-students">
                Scholarships for graduates
              </Link>
            </li>
          </ul>
          <p>
            You may also like to get free counselling for
            <Link to="/international-study" target="_blank">
              International Scholarships
            </Link>{" "}
            for study abroad options.
          </p>
        </section>
      </section>
      {/* Start Scholarship for every student section Done */}
      <section className="container-fluid scholarship-container equial-padding">
        <section className="container">
          <section className="row">
            <article className="col-md-12 text-center buddy-heading">
              <h2 className="scholarship">Find the best-fit scholarship</h2>
              <p>
                Choosing the right scholarship is a daunting task. Pick relevant
                scholarships and stand a chance to win.
              </p>
            </article>
          </section>

          <section className="row">
            <article className="col-md-3">
              <section className="Scholarship-block">
                <section className="Scholarship-gd-top">
                  <img
                    src={`${imgBaseUrl}income-based-scholarships.jpg`}
                    alt="Means Based Scholarships"
                  />
                  <h2>
                    <Link
                      to="/scholarship-for/means-based-scholarships"
                      onClick={() => this.gtmEvent(7)}
                    >
                      MEANS BASED SCHOLARSHIPS
                    </Link>
                  </h2>
                </section>
              </section>
            </article>

            <article className="col-md-3">
              <section className="Scholarship-block">
                <section className="Scholarship-gd-top">
                  <img
                    src={`${imgBaseUrl}merit-based-scholarship.jpg`}
                    alt="MERIT BASED SCHOLARSHIPS"
                  />
                  <h2>
                    <Link
                      to="/scholarship-for/merit-based-scholarships"
                      onClick={() => this.gtmEvent(8)}
                    >
                      MERIT BASED SCHOLARSHIPS
                    </Link>
                  </h2>
                </section>
              </section>
            </article>

            <article className="col-md-3">
              <section className="Scholarship-block">
                <section className="Scholarship-gd-top">
                  <img
                    src={`${imgBaseUrl}PHYSICALLY-CHALLENGED-SCHOLARSHIPS.jpg`}
                    alt="Need Based Scholarships"
                  />
                  <h2>
                    <Link
                      to="/scholarship-for/physically-challenged-scholarships"
                      onClick={() => this.gtmEvent(9)}
                    >
                      NEED BASED SCHOLARSHIPS
                    </Link>
                  </h2>
                </section>
              </section>
            </article>

            <article className="col-md-3">
              <section className="Scholarship-block">
                <section className="Scholarship-gd-top">
                  <img
                    src={`${imgBaseUrl}school-scholarship.jpg`}
                    alt="SCHOOL SCHOLARSHIPS"
                  />
                  <h2>
                    <Link
                      to="/scholarship-for/school"
                      onClick={() => this.gtmEvent(10)}
                    >
                      SCHOOL SCHOLARSHIPS
                    </Link>
                  </h2>
                </section>
              </section>
            </article>

            <article className="col-md-3">
              <section className="Scholarship-block">
                <section className="Scholarship-gd-top">
                  <img
                    src={`${imgBaseUrl}COLLEGE-SCHOLARSHIPS.jpg`}
                    alt="COLLEGE SCHOLARSHIPS"
                  />
                  <h2>
                    <Link
                      to="/scholarship-for/Graduation"
                      onClick={() => this.gtmEvent(11)}
                    >
                      COLLEGE SCHOLARSHIPS
                    </Link>
                  </h2>
                </section>
              </section>
            </article>

            <article className="col-md-3">
              <section className="Scholarship-block">
                <section className="Scholarship-gd-top">
                  <img
                    src={`${imgBaseUrl}INTERNATIONAL-SCHOLARSHIPS.jpg`}
                    alt="INTERNATIONAL SCHOLARSHIPS"
                  />
                  <h2>
                    <Link
                      to="/scholarship-for/International"
                      onClick={() => this.gtmEvent(12)}
                    >
                      INTERNATIONAL SCHOLARSHIPS
                    </Link>
                  </h2>
                </section>
              </section>
            </article>

            <article className="col-md-3">
              <section className="Scholarship-block">
                <section className="Scholarship-gd-top">
                  <img
                    src={`${imgBaseUrl}MINORITIES-SCHOLARSHIPS.jpg`}
                    alt="MINORITIES SCHOLARSHIPS"
                  />
                  <h2>
                    <Link
                      to="/scholarship-for/minorities-scholarships"
                      onClick={() => this.gtmEvent(13)}
                    >
                      MINORITIES SCHOLARSHIPS
                    </Link>
                  </h2>
                </section>
              </section>
            </article>
            <article className="col-md-3">
              <section className="Scholarship-block">
                <section className="Scholarship-gd-top">
                  <img
                    src={`${imgBaseUrl}TALENT-BASED-SCHOLARSHIPS.jpg`}
                    alt="TALENT BASED SCHOLARSHIPS"
                  />
                  <h2>
                    <Link
                      to="/scholarship-for/talent-based-scholarships"
                      onClick={() => this.gtmEvent(14)}
                    >
                      TALENT BASED SCHOLARSHIPS
                    </Link>
                  </h2>
                </section>
              </section>
            </article>
          </section>

          {/* <UserLoginRegistrationPopup
              text="Close Me"
              closePopup={() => null}
              userIsLoggedIn={() => null}
              logUserOut={() => null}
            /> */}

          <section className="row">
            <section className="col-md-12 text-center">
              <Link
                to="/scholarships"
                className="btn"
                onClick={() => this.gtmEvent(2)}
              >
                Suggested Scholarships
              </Link>
            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
);
export default error;
