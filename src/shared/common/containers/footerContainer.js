import React from "react";
import { connect } from "react-redux";
import { fetchDashData as fetchDashDataAction } from "./../../home/actions";
import Footer from "./../footer";

const mapStateToProps = ({ home }) => ({
  dashData: home.dashData
});
const mapDispatchToProps = dispatch => ({
  loadDashData: () => dispatch(fetchDashDataAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
