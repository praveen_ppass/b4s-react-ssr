import React from "react";
import { imgBaseUrl } from "../../constants/constants";

const ThankYou = () => (
  <section>
    <section className="errorbggreay" />
    <section className="thankspage">
      <img src={`${imgBaseUrl}icon-thanks.jpg`} alt="thankyou-icon" />
      <h2>Payment Successful!</h2>
      <p>
        Thank you! We have successfully received your payment. <br />Your
          Transaction ID is 2321XXXX. <br />
        Check your inbox for the receipt.
        </p>
    </section>
  </section>
);
export default ThankYou;
