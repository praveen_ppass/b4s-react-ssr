import React from "react";
import { imgBaseUrl } from "../../constants/constants";
const ThankYou = () => (
  <section>
    <section className="errorbggreay" />
    <section className="thankspage text-center">
      <img src={`${imgBaseUrl}icon-thanks1.jpg`} alt="thankyou-icon" />
      <h2>Payment Cancelled</h2>
      <p>
        Your payment has been canceled. Your Transaction ID is 2434XXXX.<br />
        Please email us at info@buddy4study.com in case of any query.
        </p>
      <a href="" className="btn-yellow text-center">
        Make another payment
        </a>
    </section>
  </section>
);
export default ThankYou;
