import React, { Component } from "react";

import { Route, NavLink, HashRouter, Link } from "react-router-dom";
import { getCookie, setCookie } from "../../constants/constants";

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initScroll: false,
      name: "",
      email: "",
      isAndorid: false,
      footerBanner: true,
      schFooterBanner: false,
      showLoanAds: true
    };
    this.scrollHandler = this.scrollHandler.bind(this);
    this.validateStickyThumbler = this.validateStickyThumbler.bind(this);
    this.submitSubcription = this.submitSubcription.bind(this);
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.goSubscribe = this.goSubscribe.bind(this);
    this.footerBanner = this.footerBanner.bind(this);
    this.onFooterAppHandler = this.onFooterAppHandler.bind(this);
    this.closeLoanAds = this.closeLoanAds.bind(this);
  }

  validateStickyThumbler() {
    if (typeof window !== "undefined") {
      if (window.scrollY == 0) {
        this.setState({
          initScroll: false
        });
      } else {
        this.setState({
          initScroll: true
        });
      }
    }
  }

  trackUrlLeftComponent() {
    window.open("https://www.buddy4study.com/educationloan", "_blank");
  }
  trackUrlRightComponent() {
    window.open(
      "http://www.buddy4study.com/scholarship/capital-first-mba-scholarships-2018-19",
      "_blank"
    );
  }

  componentDidMount() {
    typeof window !== "undefined"
      ? window.addEventListener("scroll", this.validateStickyThumbler)
      : "";

    this.updateDimensions();

    if (window)
      window.addEventListener("resize", this.updateDimensions.bind(this));
  }

  componentWillUnmount() {
    if (window)
      window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  submitSubcription(event) {
    event.preventDefault();
  }

  onFormFieldChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  }

  scrollHandler() {
    if (typeof window !== "undefined") {
      window.scrollTo({
        top: 0, // could be negative value
        left: 0,
        behavior: "smooth"
      });
    }
  }

  goSubscribe() {
    gblFunc.gaTrack.trackEvent(["Home-button", "Subscribe now"]);
  }

  footerBanner(cname, cvalue, hours) {
    setCookie(cname, cvalue, hours);

    if (hours === 8) {
      this.setState({
        footerBanner: false
      });
    } else {
      this.setState({
        schFooterBanner: false
      });
    }
  }
  closeLoanAds() {
    this.setState({
      showLoanAds: false
    });
  }

  updateDimensions() {
    if (typeof window !== "undefined" && window.screen.width <= 963) {
      if (document && /Android/i.test(navigator.userAgent)) {
        if (getCookie("footerName")) {
          this.setState({
            footerBanner: false
          });
        } else {
          this.setState({
            footerBanner: true
          });
        }
      }
    } else {
      this.setState({
        footerBanner: false
      });
    }

    if (!/Android/i.test(navigator.userAgent)) {
      if (document && getCookie("schFooterName")) {
        this.setState({
          schFooterBanner: false
        });
      } else {
        this.setState({
          schFooterBanner: false
        });
      }
    }
  }

  onFooterAppHandler() {
    if (window) {
      ga("send", {
        hitType: "event",
        eventCategory: "APP",
        eventAction: "Click",
        eventLabel: "pop-up"
      });

      window.location.href =
        "https://play.google.com/store/apps/details?id=com.budy4study.ui&referrer=utm_source%3DWebsiteBottom%26utm_medium%3DWebBottomPopUp%26utm_campaign%3DWebBottomAppInstall%26anid%3Dadmob";
    }
  }
  render() {
    const { footerBanner, schFooterBanner } = this.state;

    return (
      <section>
        <article
          className={
            this.state.showLoanAds == true
              ? "educationLoanSticky"
              : "educationLoanSticky hide"
          }
        >
          <button
            type="button"
            className="close"
            onClick={() => this.closeLoanAds()}
          >
            <i>&times;</i>
          </button>
          <p>
            <a href="https://www.buddy4study.com/educationloan" target="_blank">
              Need Education Loan
            </a>
          </p>
        </article>
        <footer>
          <section className="container-fluid footer equial-padding">
            <section className="container">
              <section className="row">
                {this.state.initScroll ? (
                  <i
                    className="fa fa-arrow-up bottomarrow-thumbler"
                    onClick={this.scrollHandler}
                  />
                ) : (
                    ""
                  )}
                <article className="col-sm-12 col-md-3 borderR footer-min-height cell1">
                  <address>
                    <p>
                      <i className="fa fa-map-pin" /> Stellar IT Park,
                      Tower&ndash;1, Ground Floor, Office No. 8, 9 &amp; 10, C
                      &ndash; 25, Sector &ndash; 62, <br /> Noida &ndash;
                      201301, India
                    </p>
                    <p>
                      <i className="fa fa-envelope" />{" "}
                      <a href="mailto:info@buddy4study.com">
                        info@buddy4study.com
                      </a>
                    </p>
                  </address>
                  <article className="footer-social">
                    <a
                      href="https://www.facebook.com/buddy4study/"
                      data-placement="top"
                      title="Facebook"
                      target="_blank"
                    >
                      <i className="fa fa-facebook" aria-hidden="true" />
                    </a>
                    <a
                      href="https://twitter.com/Buddy4studyOffc"
                      data-placement="top"
                      title="Twitter"
                      target="_blank"
                    >
                      <i className="fa fa-twitter" aria-hidden="true" />
                    </a>
                    <a
                      href="https://www.buddy4study.com/article/"
                      data-placement="top"
                      title="wordpress"
                      target="_blank"
                    >
                      <i className="fa fa-wordpress" aria-hidden="true" />
                    </a>
                    <a
                      href="https://www.linkedin.com/company/buddy4study-com"
                      data-placement="top"
                      title="Linkedin"
                      target="_blank"
                    >
                      <i className="fa fa-linkedin" aria-hidden="true" />
                    </a>
                    <a
                      href="https://www.youtube.com/user/buddy4study"
                      data-placement="top"
                      title="youtube"
                      target="_blank"
                    >
                      <i className="fa fa-youtube" aria-hidden="true" />
                    </a>
                  </article>
                </article>

                <article className="col-sm-3 col-md-3 borderR footer-min-height moboMinHeight cell2">
                  <span className="footer-ul">
                    <h5>
                      <Link to="/about-us">About Us</Link>
                    </h5>
                    <h5>
                      <Link to="/team">Team</Link>
                    </h5>
                    <h5>
                      <Link to="/scholarship-result">Scholarship Result</Link>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/"
                        target="_blank"
                      >
                        Articles
                      </a>
                    </h5>
                    <h5>
                      <Link to="/media/partners">Media Partners</Link>
                    </h5>
                    <h5>
                      <Link to="/careers">Careers</Link>
                    </h5>
                  </span>
                </article>

                <article className="col-sm-3 col-md-3 borderR footer-min-height moboMinHeight cell3">
                  <span className="footer-ul">
                    <h5>
                      <Link to="/faqs">FAQs</Link>
                    </h5>
                    <h5>
                      <Link to="/terms-and-conditions">Terms & Conditions</Link>
                    </h5>
                    <h5>
                      <Link to="/privacy-policy">Privacy Policy</Link>
                    </h5>
                    <h5>
                      <Link to="/contact-us">Contact Us</Link>
                    </h5>
                  </span>
                </article>

                <article className="col-sm-6 col-md-3 contact-form cell4">
                  <h3>Newsletter Subscription </h3>
                  <form
                    name="newsLtrForm"
                    method="post"
                    autoCapitalize="off"
                    action="https://mailer.buddy4study.com/subscribe"
                    acceptCharset="utf-8"
                  >
                    <article className="ctrl-wrapper">
                      <article className="form-group">
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="form-control"
                          maxLength="20"
                          minLength="3"
                          placeholder="Name"
                          required
                        />
                      </article>
                    </article>
                    <article className="ctrl-wrapper">
                      <article className="form-group">
                        <input
                          type="email"
                          maxLength="50"
                          id="email"
                          minLength="1"
                          name="email"
                          className="form-control"
                          placeholder="Email"
                          required
                        />
                      </article>
                    </article>
                    <input
                      value="lc6Vj09u7639foguwkKbxr0w"
                      name="list"
                      type="hidden"
                    />
                    <article style={{ display: "none" }}>
                      <input id="hp" name="hp" type="text" />
                    </article>
                    <input
                      type="reset"
                      value="Reset"
                      style={{ display: "none" }}
                    />
                    <input
                      type="submit"
                      id="submit"
                      name="submit"
                      value="Subscribe Now"
                    />
                  </form>
                </article>
              </section>
            </section>
          </section>
        </footer>
        {/* Start footer banner */}
        {typeof window !== "undefined" ? (
          schFooterBanner && !window.location.pathname.includes("faq") ? (
            <section className="fix-banner">
              <span
                className="cross"
                onClick={() =>
                  this.footerBanner("schFooterName", "schBanner", 24)
                }
                cross-banner
              >
                &times;
              </span>
              <article className="leftPanel">
                <span>
                  <a
                    target="_blank"
                    href="https://www.buddy4study.com/educationloan"
                    id="Loan"
                  // onClick={this.trackUrlLeftComponent}
                  >
                    Easiest way to get a collateral free education loan
                  </a>
                </span>
                <a
                  target="_blank"
                  href="https://www.buddy4study.com/educationloan"
                  id="Loan"
                // onClick={this.trackUrlLeftComponent}
                >
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/banner-apply-btn.png"
                    id="LoanBtn"
                    className="bannerBtn"
                    onClick={this.trackUrlLeftComponent}
                  />
                </a>
              </article>
              <article className="rightPanel hide">
                <span>
                  <a
                    target="_blank"
                    href="https://www.buddy4study.com/educationloan"
                    id="CapitalFirst"
                    onClick={this.trackUrlRightComponent}
                  >
                    Capital First MBA Scholarships 2018-20 ( Win INR 100,000 per
                    annum)
                  </a>
                </span>

                <img
                  src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/banner-apply-btn.png"
                  id="CapitalFirstBtn"
                  className="bannerBtn"
                  onClick={this.trackUrlRightComponent}
                />
              </article>
            </section>
          ) : null
        ) : null}
        {/* App bottom Popup */}
        {typeof window !== "undefined" ? (
          footerBanner &&
            /Android/i.test(navigator.userAgent) &&
            !window.location.pathname.includes("faq") ? (
              <section className="slideAppPopupWrapper open">
                <dd
                  className="crossPopup"
                  onClick={() =>
                    this.footerBanner("footerName", "footerBanner", 8)
                  }
                  id="crossPopupCookies"
                >
                  &times;
              </dd>
                <p>Buddy4study.com</p>
                <span>
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/logo-aap.jpg"
                    alt="buddy4study app"
                  />
                  <i>
                    Make your Scholarship search faster, simpler and stay updated.
                </i>
                </span>
                <a
                  href="javascript:void(0)"
                  className="_getFooterApp"
                  onClick={this.onFooterAppHandler}
                >
                  GET APP
              </a>
              </section>
            ) : null
        ) : null}
      </section>
    );
  }
}

export default Footer;
