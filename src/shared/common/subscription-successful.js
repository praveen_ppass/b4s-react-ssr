import React from "react";
import { imgBaseUrl } from "../../constants/constants";
const ThankYou = () => (
  <section>
    <section className="errorbggreay hide" />
    <section className="thankspage">
      <img src={`${imgBaseUrl}icon-thanks2.jpg`} alt="thankyou-icon" />
      <h2>Subscription successful!</h2>
      <p>Congratulations! You are now signed up for our newsletter.</p>
    </section>
  </section>
);
export default ThankYou;
