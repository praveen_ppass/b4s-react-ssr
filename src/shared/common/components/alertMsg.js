import React from "react";

const AlertMessage = props =>
  props.isShow ? (
    <section className={props.isShow ? "alert-overlay" : "hide"} style={{zIndex:9999999}}>
      <article
        className={
          props.internationalStudyPopup ? "alertPopup popupPos" : "alertPopup"
        }
      >
        <article className="innerContainer">
          <p className={props.status ? "success-icon" : "cancel-icon"}>
            {/* <p className="success-icon">
          <i>{props.msg}</i>
        </p> */}
            <i dangerouslySetInnerHTML={{ __html: props.msg }} />
          </p>
          <input type="button" value="OK" autoFocus onClick={props.myProfileRedirect ? props.redirectMyProfile : props.close} />
        </article>
      </article>
    </section>
  ) : null;
export default AlertMessage;
