import React, { Component } from "react";
import { ruleRunner } from "../../../validation/ruleRunner";
import {
  required,
  isEmail,
  minLength,
  shouldMatch
} from "../../../validation/rules";
import {
  UPDATE_USER_PASSWORD_FAILURE,
  RESET_USER_PASSWORD_SUCCESS,
  RESET_USER_PASSWORD_FAILURE
} from "../../login/actions";
class ChangePasswordPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: "",
      confirmNewPassword: "",
      oldPassword: "",
      isFormValid: false,
      validations: {
        newPassword: null,
        confirmNewPassword: null,
        oldPassword: null
      }
    };
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.updateUserPasswordHandler = this.updateUserPasswordHandler.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
  }

  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "oldPassword":
        validationObject.name = "* Old password";
        validationObject.validationFunctions = [required, minLength(6)];
        return validationObject;
      case "newPassword":
        validationObject.name = "* New password";
        validationObject.validationFunctions = [required, minLength(6)];
        return validationObject;
      case "confirmNewPassword":
        validationObject.name = "* Confirm new password";
        // validationObject.validationFunctions = [required];
        validationObject.validationFunctions = [
          shouldMatch(this.state.newPassword)
        ];
        return validationObject;
    }
  }
  shouldFieldUpdate(id, value) {
    switch (id) {
      case "mobile":
        return value.length <= 10;
      default:
        return true;
    }
  }

  /************ end - validation part - get message and required validation******************* */

  /************ start - validation part - check form submit validation******************* */
  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }
  /************ start - validation part - check form submit validation******************* */

  onFormFieldChange(event) {
    const { id, value } = event.target;
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );
    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];
    this.setState({
      [id]: value,
      validations
    });
  }

  updateUserPasswordHandler(event) {
    event.preventDefault();
    const { newPassword, oldPassword } = this.state;

    if (this.checkFormValidations()) {
      const params = { newPassword, oldPassword };
      const userid =
        typeof window !== "undefined"
          ? window.localStorage.getItem("userId")
          : "";
      this.props.updateUserPassword({ userid, params });
    }
  }

  componentWillReceiveProps(nextProps) {
    const { passwordUpdateError, type } = nextProps;

    switch (type) {
      case RESET_USER_PASSWORD_FAILURE:
        if (passwordUpdateError) {
          const errorMessage = passwordUpdateError;
          const validations = { ...this.state.validations };
          validations.oldPassword = errorMessage;
          this.setState({
            validations
          });
        }
        break;
    }
  }

  render() {
    return (
      <article className="popup">
        <article className="popupsms">
          <article className="popup_inner loginpopup">
            <article className="LoginRegPopup">
              <section className="modal fade modelAuthPopup1">
                <section className="modal-dialog">
                  <section className="modal-content modelBg emailverification">
                    <article className="modal-header">
                      <h3 className="modal-title">Change Password</h3>
                      <button
                        type="button"
                        className="close btnPos"
                        onClick={this.props.closePopup}
                      >
                        <i>&times;</i>
                      </button>
                      <p>{this.props.updatePasswordMessage}</p>
                    </article>

                    <article className="modal-body forgot">
                      <article className="row">
                        <form
                          name="forgotForm"
                          className="formelemt"
                          onSubmit={this.updateUserPasswordHandler}
                          autoCapitalize="off"
                        >
                          <section className="row">
                            <section className="ctrl-wrapper">
                              <article className="form-group">
                                <input
                                  type="password"
                                  name="oldPassword"
                                  className="form-control"
                                  placeholder="Old Password"
                                  id="oldPassword"
                                  value={this.state.oldPassword}
                                  onChange={this.onFormFieldChange}
                                />
                                {this.state.validations.oldPassword ? (
                                  <span className="error1 animated bounce">
                                    {this.state.validations.oldPassword}
                                  </span>
                                ) : null}
                              </article>
                            </section>
                            <section className="ctrl-wrapper">
                              <article className="form-group">
                                <input
                                  type="password"
                                  name="newPassword"
                                  className="form-control"
                                  placeholder="New Password"
                                  value={this.state.newPassword}
                                  onChange={this.onFormFieldChange}
                                  id="newPassword"
                                />
                                {this.state.validations.newPassword ? (
                                  <span className="error1 animated bounce">
                                    {this.state.validations.newPassword}
                                  </span>
                                ) : null}
                              </article>
                            </section>
                            <section className="ctrl-wrapper">
                              <article className="form-group">
                                <input
                                  type="password"
                                  name="confirmNewPassword"
                                  className="form-control"
                                  value={this.state.confirmNewPassword}
                                  onChange={this.onFormFieldChange}
                                  placeholder="Re-Type Password"
                                  id="confirmNewPassword"
                                />
                                {this.state.validations.confirmNewPassword ? (
                                  <span className="error1 animated bounce">
                                    {
                                      this.state.validations
                                        .confirmNewPassword
                                    }
                                  </span>
                                ) : null}
                              </article>
                            </section>
                          </section>

                          <section className="row">
                            <button
                              type="submit"
                              className="btn-block greenBtn recover"
                            >
                              Change password
                              </button>
                          </section>
                        </form>
                      </article>
                    </article>
                  </section>
                </section>
              </section>
            </article>
          </article>
        </article>
      </article>
    );
  }
}

export default ChangePasswordPopup;
