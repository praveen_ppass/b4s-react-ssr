import React from "react";

const ServerError = ({ errorMessage }) => (
  <div className="text-center padding-center">
    <div>
      <i className="fa fa-frown-o fa-5x noData" aria-hidden="true" />
    </div>
    {errorMessage}
  </div>
);

export default ServerError;
