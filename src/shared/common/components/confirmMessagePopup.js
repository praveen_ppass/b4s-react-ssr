import React from "react";

const ConfirmMessagePopup = props => (
  <section className={props.showPopup ? "alert-overlay" : "hide"}>
    <article className="alertPopup">
      <article className="innerContainer">
        <p>{props.message}</p>
        <article className="centerBut">
          <button onClick={props.onConfirmationSuccess}>{props.btnText && props.btnText.btn1 ? props.btnText.btn1 : 'Yes'}</button>
          <button onClick={props.onConfirmationFailure}>{props.btnText && props.btnText.btn2 ? props.btnText.btn2 : 'No'}</button>
        </article>
      </article>
    </article>
  </section>
);

export default ConfirmMessagePopup;
