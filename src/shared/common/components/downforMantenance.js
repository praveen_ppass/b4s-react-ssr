import React from "react";

const DownForMaintenance = props => (
  <section className={`downForMaintenance ${props.isFlag ? "show" : "hide"}`}>
    <article className="overLay" />
    <article className="overLayContent">
      <img
        src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/maintenance.png"
        alt=""
      />
      We are currently upgrading our website to serve you better.During this
      process, our website will be under maintenance mode for <i>3 hours</i>.
    </article>
  </section>
);
export default DownForMaintenance;
