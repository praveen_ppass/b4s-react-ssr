import React, { Component, Fragment } from "react";
import { Link, Redirect } from "react-router-dom";
import { browserHistory } from "react-router";
// import LoadingBar from "react-redux-loading-bar";
import { Helmet } from "react-helmet";
import gblFunc from "../../../globals/globalFunctions";
import Loader from "./loader";
import AlertMessagePopup from "../../../shared/common/components/alertMsg";
import UserLoginRegistrationPopup from "../../login/containers/userLoginRegistrationContainer";
import ChangePasswordPopup from "./changePassword";
import {
  metaTagsJson,
  imgBaseUrl,
  LOGIN_PAGE_ROUTE
} from "../../../constants/constants";

import {
  UPDATE_USER_PASSWORD_SUCCESS,
  UPDATE_USER_PASSWORD_FAILURE,
  LOG_USER_OUT_SUCCEEDED,
  LOG_USER_OUT_FAILED,
  LOG_USER_OUT
} from "../../login/actions";
import {
  UPLOAD_USER_PIC_SUCCESS,
  FETCH_UPDATE_USER_SUCCEEDED
} from "../../dashboard/actions";
import { FETCH_USER_RULES_SUCCESS } from "../../../constants/commonActions";

import {
  messages,
  headerClasses,
  headerBannerPosClass
} from "../../../constants/constants";
import AlertMessage from "../../common/components/alertMsg";
var jwtDecode = require("jwt-decode");
class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectHome: "https://www.buddy4study.com/",
      showAlertMessagePopup: false,
      search: "",
      clicked: false,
      isMin: false,
      showPopup: false,
      showChangePasswordPopup: false,
      menuClicked: false,
      userName: "",
      userProfilePic: "",
      isLoggedIn: false,
      openLoginPagePopup: false,
      updatePasswordMessage: "",
      redirectToNotFound: false,
      showMsg: "",
      isShowMsg: false,
      isShowAlert: false,
      statusAlert: false,
      alertMsg: "",
      isSearch: false,
      upperBanner: false,
      isFlag: false
    };
    this.showAnimateSearch = this.showAnimateSearch.bind(this);
    this.handleStickyLoad = this.handleStickyLoad.bind(this);
    this.togglePopup = this.togglePopup.bind(this);
    this.userIsLoggedIn = this.userIsLoggedIn.bind(this);
    this.logUserOut = this.logUserOut.bind(this);
    this.showChangePasswordPopup = this.showChangePasswordPopup.bind(this);
    this.closeChangePasswordPopup = this.closeChangePasswordPopup.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.userPictureChanged = this.userPictureChanged.bind(this);
    this.updateUserData = this.updateUserData.bind(this);
    this.onSearchSubmit = this.onSearchSubmit.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.renderRedirect = this.renderRedirect.bind(this);
    this.onHeaderAppHandler = this.onHeaderAppHandler.bind(this);
    this.gtmEventHandler = this.gtmEventHandler.bind(this);
    this.urlPush = this.urlPush.bind(this);
    this.closeTabs = this.closeTabs.bind(this);
    //iframe style
    var iframe = {
      visibility: "hidden",
      opacity: "0",
      position: "absolute",
      "z-index": "-1"
    };
    this.getParsedLocation = this.getParsedLocation.bind(this);
  }

  showChangePasswordPopup() {
    this.setState({
      showChangePasswordPopup: true
    });
  }

  userPictureChanged(userProfilePic) {
    this.setState({
      userProfilePic
    });
  }

  closeChangePasswordPopup() {
    this.setState({
      showChangePasswordPopup: false,
      updatePasswordMessage: ""
    });
  }

  // Interest Form
  clickToScrollInterestForm(e) {
    if (typeof window !== "undefined") {
      window.scrollTo({
        top: 0, // could be negative value
        left: 0,
        behavior: "smooth"
      });
    }
  }

  getParsedLocation(currentLocation) {
    const specialCharacter = {
      slash: "/",
      qMark: "?"
    };
    if (currentLocation === "") {
      return currentLocation;
    } else {
      const firstSlash = currentLocation.indexOf(specialCharacter.slash);
      let secondCharacter = currentLocation.indexOf(
        specialCharacter.slash,
        firstSlash + 1
      );
      if (secondCharacter === -1) {
        secondCharacter = currentLocation.indexOf(
          specialCharacter.qMark,
          firstSlash + 1
        );
      }
      if (secondCharacter === -1) {
        return currentLocation.slice(firstSlash + 1);
      } else {
        return currentLocation.slice(firstSlash + 1, secondCharacter);
      }
    }
  }

  componentDidMount() {
    if (typeof window !== "undefined") {
      window.addEventListener('beforeunload', this.closeTabs);
    }

    if (typeof window !== "undefined" && window.navigator.userAgent) {
      if (
        window.navigator.userAgent.indexOf("MSIE") > 0 ||
        window.navigator.userAgent.match(/Trident.*rv\:11\./)
      ) {
        this.setState({ showAlertMessagePopup: true });
      }
    }
    if (
      typeof window !== "undefined" &&
      (this.props.isAuthenticated ||
        window.localStorage.getItem("isAuth") === "1")
    ) {
      this.userIsLoggedIn();
    }
    typeof window !== "undefined"
      ? window.addEventListener("scroll", this.handleStickyLoad)
      : "";
    this.updateDimensions();
    if (window) {
      window.addEventListener("resize", this.updateDimensions.bind(this));
    }
    if (typeof window !== "undefined") {
      this.setState({ redirectHome: window.location.protocol + "//" + window.location.host + '/' })
    }
  }

  togglePopup() {
    //If this is login page route , don't open popup from header
    if (this.props.pageLoc === LOGIN_PAGE_ROUTE) {
      this.props.openLoginPagePopup();
    } else {
      this.setState({
        showPopup: !this.state.showPopup
      });
    }
  }

  componentWillUnmount() {
    if (window)
      window.removeEventListener("resize", this.updateDimensions.bind(this));
    window.removeEventListener('beforeunload', this.closeTabs);
  }
  closeTabs() {
    if (gblFunc.isUserAuthenticated()) {
      const token = jwtDecode(gblFunc.getAuthToken());
      if (token.authorities && token.authorities.length > 0 && token.authorities.includes('ADMIN')) {
        gblFunc.removeUserDetails();
      }
    }
  }
  userIsLoggedIn() {
    const { pic, firstName } = gblFunc.getStoreUserDetails();
    const userProfilePic = gblFunc.isNull(pic) ? null : pic;
    this.setState({
      isLoggedIn: true,
      userName: firstName,
      userProfilePic
    });
  }

  updateUserData(userName, userProfilePic) {
    this.setState({
      isLoggedIn: true,
      userName,
      userProfilePic
    });
  }

  componentWillReceiveProps(nextProps) {
    const { type, pageLoc } = nextProps;
    if (this.props.pageLoc === pageLoc) {
      switch (type) {
        //Since user is logged in only when his details are fetched.
        case FETCH_USER_RULES_SUCCESS:
          this.userIsLoggedIn();
          const { firstName, pic } = nextProps.userRulesData;
          this.updateUserData(firstName, pic);
          break;
        //Update pic in case user changes pic on dashboard.
        case UPLOAD_USER_PIC_SUCCESS:
          const { location } = nextProps.uploadPicData;
          this.userPictureChanged(location);
          break;

        case FETCH_UPDATE_USER_SUCCEEDED:
          //TODO: Add user name updation case here.
          const userName = nextProps.updateUserInfo.firstName;
          this.setState({
            userName
          });

          break;

        case LOG_USER_OUT:
          if (this.state.isLoggedIn) {
            this.setState({
              isLoggedIn: false,
              userName: "",
              userProfilePic: ""
            });
          }
          if (pageLoc.includes('/form/summary')) {
            const url = window.location.href;
            const replaceUrl = url.replace('/form/summary', "/instruction");
            window.open(replaceUrl, "_self");
          }
          break;
        case LOG_USER_OUT_FAILED:
          return <Redirect to={"/404"} />;
        case UPDATE_USER_PASSWORD_SUCCESS:
          this.setState({
            isShowAlert: true,
            statusAlert: true,
            showChangePasswordPopup: false,
            alertMsg: messages.changePassword.success
          });
          break;

        case UPDATE_USER_PASSWORD_FAILURE:
          this.setState({
            isShowAlert: true,
            statusAlert: false,
            showChangePasswordPopup: false,
            alertMsg: messages.changePassword.failed
          });
          break;
      }
    }
  }

  logUserOut() {
    this.setState(
      {
        isLoggedIn: false,
        userName: "",
        userProfilePic: ""
      },
      () => this.props.logUserOut()
    );
    if (!this.props.pageLoc.includes("CES8")) {
      window.open(this.state.redirectHome, "_self")

    }
    //  window.localStorage.removeItem();
  }

  /* Sticky Handler  */
  handleStickyLoad() {
    if (typeof window !== "undefined") {
      /* Sticky Header */
      if (window.scrollY > 60) {
        this.setState({
          isMin: true
        });
      } else {
        this.setState({
          isMin: false
        });
      }
      if (this.state.isMin) {
        this.setState({
          menuClicked: false
        });
      }
    }
  }

  hideAlert() {
    //  close popup
    this.setState({
      isShowAlert: false
    });
  }

  showAnimateSearch() {
    this.setState({
      ...this.state,
      clicked: !this.state.clicked,
      search: "",
      isSearch: false
    }, () => { !this.state.clicked ? window.location.href = '/scholarships' : "" });
  }

  onSearchSubmit(e) {
    e.preventDefault();
    if (this.state.search == "" || this.state.search.trim() === "") {
      this.setState({
        showMsg: "Cannot Be Empty",
        isShowMsg: true,
        isSearch: false
      });
    }
    else {
      this.setState(
        {
          isSearch: true
        }
        // ,
        // () => {
        //   this.setState({
        //     isSearch: false
        //   });
        // }
      );
    }
  }

  handleChange(event) {
    this.setState({ search: event.target.value });
  }

  handleHamburgerMenuOpen() {
    this.setState({
      ...this.state,
      menuClicked: !this.state.menuClicked
    });

    if (!this.state.menuClicked) {
      this.setState({
        isMin: false
      });
    } else {
      this.setState({
        isMin: true
      });
    }
  }

  renderRedirect() {
    if (this.state.isSearch) {
      return (
        <Redirect to={"/scholarships?q=" + this.state.search} push={true} />
      );
    }
  }

  updateDimensions() {
    if (window && window.screen.width <= 963) {
      this.setState({
        upperBanner: true
      });
    } else {
      this.setState({
        upperBanner: false
      });
    }
  }

  onHeaderAppHandler() {
    if (window) {
      ga("send", {
        hitType: "event",
        eventCategory: "APP",
        eventAction: "Click",
        eventLabel: "top"
      });

      window.location.href =
        "https://play.google.com/store/apps/details?id=com.budy4study.ui&referrer=utm_source%3DWebsiteTop%26utm_medium%3DWebTopHeader%26utm_campaign%3DWebTopAppInstall%26anid%3Dadmob";
    }
  }

  gtmEventHandler(gtm) {
    gblFunc.gaTrack.trackEvent(gtm);
  }

  urlPush(urlPar) {
    this.props.history.push(
      `/myProfile/${urlPar}`
    );
  }


  render() {
    let isApplicationPageStatus = this.props.pageLoc
      ? this.props.pageLoc.includes("/application")
      : false;

    const {
      clicked,
      isLoggedIn,
      userName,
      userProfilePic,
      upperBanner,
      redirectHome
    } = this.state;
    //FIXME: Make this code run only one time
    let headerClass = "";
    let topHeaderBanner = "";
    let parsedLocation = null;
    if (this.props.pageLoc.includes("qna/")) {
      parsedLocation = "qna/";
    } else if (this.props.pageLoc.includes("/otp")) {
      parsedLocation = "page";
    } else {
      parsedLocation = this.getParsedLocation(this.props.pageLoc);
    }

    if (this.state.isMin) {
      headerClass = this.props && (this.props.pageLoc.includes("HEC1/") || this.props.pageLoc.includes("HEC2/") || this.props.pageLoc.includes("HEC4/") || this.props.pageLoc.includes("HEC7/") || this.props.pageLoc.includes("HEC8/") || this.props.pageLoc.includes("HEC9/") || this.props.pageLoc.includes("HECS1/")) ? "" : "sticky ";
    }

    headerClass += headerClasses[parsedLocation];
    topHeaderBanner += headerBannerPosClass[parsedLocation];
    return (
      <Fragment>
        {/* <AlertMessagePopup
          msg={
            "Currently we do not support this Browser. Please switch to some other Browser for better experience."
          }
          isShow={this.state.showAlertMessagePopup}
          okButton={false}
        /> */}

        {this.state.showAlertMessagePopup && (
          <p
            style={{
              background: "#f00",
              padding: "15px 0",
              color: "#fff",
              fontSize: "18px",
              textAlign: "center",
              width: " 100%",
              margin: "0"
            }}
          >
            {" "}
            <i className="fa fa-exclamation-triangle" />
            For better browsing experience and running web-based applications,
            please use updated version of Mozilla Firefox or Chrome or Microsoft
            Edge{" "}
          </p>
        )}

        {/* Interest Button */}
        {this.props.pageLoc === "/media/partners" ? (
          <img
            src={`${imgBaseUrl}intrestBtn.png`}
            className={
              this.state.isFlag == true ? "intrestBtn ani" : "intrestBtn"
            }
            alt="Intrest"
            onClick={e => this.clickToScrollInterestForm(e)}
          />
        ) : (
            ""
          )}
        <section className={`stickyHaderWrapper ${topHeaderBanner}`}>
          <Helmet>
            <title>
              {
                metaTagsJson[
                this.props.pageLoc.length > 1
                  ? this.props.pageLoc.slice(1)
                  : this.props.pageLoc
                ]
              }
            </title>
          </Helmet>
          {this.state.isSearch ? this.renderRedirect() : ""}
          <Loader isLoader={this.props.showLoader} />

          <AlertMessage
            isShow={this.state.isShowAlert}
            msg={this.state.alertMsg}
            close={this.hideAlert}
            status={this.state.statusAlert}
          />
          {typeof window !== "undefined" ? (
            upperBanner &&
              /Android/i.test(navigator.userAgent) &&
              !window.location.pathname.includes("faq") ? (
                <section
                  className={
                    this.props.pageLoc === "/collegeboard" ||
                      this.props.pageLoc === "/ht-hero"
                      ? "appNotificationWrpper in hide"
                      : "appNotificationWrpper in"
                  }
                >
                  <article className="content-wrapper">
                    <dd
                      className="crossPopup"
                      onClick={() => this.setState({ upperBanner: false })}
                      id="closeTopAppBanner"
                    >
                      &times;
                  </dd>
                    Use Buddy4Study on mobile app
                  <a
                      href="javascript:void(0)"
                      className="_getHeaderApp"
                      onClick={this.onHeaderAppHandler}
                    >
                      Use App
                  </a>
                  </article>
                </section>
              ) : null
          ) : null}
          {/* popup for app auto detected */}
          <iframe id="l" width="1" height="1" style={this.iframe} hidden />
          <header className={headerClass}>
            <article className="container">
              <article className="col-sm-3 col-md-3 paddingLeft">
                {this.props.pageLoc === "/collegeboard" ? (
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/white-logo.png"
                    className="logo"
                    alt="buddy4study-logo"
                  />
                ) : (
                    <a href={redirectHome}>
                      <img
                        src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/white-logo.png"
                        className="logo"
                        alt="buddy4study-logo"
                      />
                    </a>
                  )}
              </article>
              {this.props.pageLoc === "/collegeboard" ? (
                <a
                  href="https://signup.collegeboard.org/india-global-alliance/"
                  target="_blank"
                  rel="noopener nofollow"
                >
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/collegeboard-logo.jpg"
                    className="cbLogo"
                    alt="College board"
                    rel="noopener nofollow"
                  />
                </a>
              ) : null}
              <article
                className={
                  this.props.pageLoc === "/collegeboard"
                    ? "col-sm-9 col-md-9 searchContent hide"
                    : "col-sm-9 col-md-9 searchContent"
                }
              >
                <article
                  className={
                    isApplicationPageStatus
                      ? "navigationWrapper application"
                      : "navigationWrapper"
                  }
                >
                  <nav
                    className={`site-nav ${this.state.menuClicked ? "open" : "close"
                      }`}
                  >
                    <article className="overflowNav">
                      {isApplicationPageStatus ? null : (
                        <article className="socialLinkMobo">
                          <span>For scholarship alerts</span>
                          <span className="socialIcons">
                            <a href="https://m.me/buddy4study" target="_blank">
                              <i className="facebook" />
                            </a>
                            <a
                              href="https://wa.me/919582762639?text=Hit%20send"
                              target="_blank"
                            >
                              <i className="whatsapp" />
                            </a>
                          </span>
                        </article>
                      )}
                      <ul className={this.props.isAuthenticated ? "logedIn" : ""}>
                        {isApplicationPageStatus ? null : (
                          <li className="menuStyle paddingtop30">
                            <Link
                              onClick={() =>
                                this.gtmEventHandler(["Header", "scholarships"])
                              }
                              to="/scholarships"
                            >
                              Scholarship
                              <i
                                className="fa fa-caret-down"
                                aria-hidden="true"
                              />
                            </Link>
                            <article className="dropdown-content">
                              <article className="scholarship">
                                <ul>
                                  <li itemRef="AllScholarships">
                                    <a href="javascript:void(0)">
                                      All Scholarships
                                    </a>
                                    <i
                                      className="fa fa-chevron-right"
                                      aria-hidden="true"
                                    />
                                    {/* Category */}
                                    <ul dataref="AllScholarships">
                                      <li>
                                        <Link
                                          to="/scholarships"
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              `scholarships`
                                            ])
                                          }
                                        >
                                          All Scholarships
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              `Online-Application-Form-List`
                                            ])
                                          }
                                          to="/online-application-form"
                                        >
                                          Live Application Form
                                        </Link>
                                      </li>
                                    </ul>
                                  </li>
                                  <li itemRef="Category">
                                    <a href="javascript:void(0)">Category</a>
                                    <i
                                      className="fa fa-chevron-right"
                                      aria-hidden="true"
                                    />
                                    {/* Category */}
                                    <ul dataref="Category">
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              `SC-ST-OBC`
                                            ])
                                          }
                                          to="/scholarships/sc-st-obc"
                                        >
                                          SC/ST/OBC
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Girls"
                                            ])
                                          }
                                          to="/scholarships/girls"
                                        >
                                          Girls
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Minority"
                                            ])
                                          }
                                          to="/scholarships/minority"
                                        >
                                          Minority
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Physically-Disabled"
                                            ])
                                          }
                                          to="/scholarships/physically-disabled"
                                        >
                                          Physically Disabled
                                        </Link>
                                      </li>
                                    </ul>
                                  </li>
                                  <li itemRef="State">
                                    <a href="javascript:void(0)">State</a>
                                    <i
                                      className="fa fa-chevron-right"
                                      aria-hidden="true"
                                    />
                                    {/* State */}
                                    <ul dataref="State">
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "international"
                                            ])
                                          }
                                          to="/scholarship-for/international"
                                        >
                                          International
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "All-India"
                                            ])
                                          }
                                          to="/scholarship-for/All-India"
                                        >
                                          All India
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "West-Bengal"
                                            ])
                                          }
                                          to="/scholarship-for/West-Bengal"
                                        >
                                          West Bengal
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Uttar-Pradesh"
                                            ])
                                          }
                                          to="/scholarship-for/Uttar-Pradesh"
                                        >
                                          Uttar Pradesh
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Bihar"
                                            ])
                                          }
                                          to="/scholarship-for/Bihar"
                                        >
                                          Bihar
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Madhya-Pradesh"
                                            ])
                                          }
                                          to="/scholarship-for/Madhya-Pradesh"
                                        >
                                          Madhya Pradesh
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Maharashtra"
                                            ])
                                          }
                                          to="/scholarships/Maharashtra"
                                        >
                                          Maharashtra
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Karnataka"
                                            ])
                                          }
                                          to="/scholarships/karnataka"
                                        >
                                          Karnataka
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Rajasthan"
                                            ])
                                          }
                                          to="/scholarships/Rajasthan"
                                        >
                                          Rajasthan
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Gujarat"
                                            ])
                                          }
                                          to="/scholarships/Gujarat"
                                        >
                                          Gujarat
                                        </Link>
                                      </li>
                                    </ul>
                                  </li>
                                  <li itemRef="CurrentClass">
                                    <a href="javascript:void(0)">
                                      Current class
                                    </a>
                                    <i
                                      className="fa fa-chevron-right"
                                      aria-hidden="true"
                                    />
                                    {/* Current class */}
                                    <ul dataref="CurrentClass">
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "class-1-to-10"
                                            ])
                                          }
                                          to="/scholarships/class-1-to-10-pre-matric"
                                        >
                                          Class 1 to 10
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Class-11-12 "
                                            ])
                                          }
                                          to="/scholarships/class-11-12"
                                        >
                                          Class 11 &amp; 12
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Polytechnic-Diploma-ITI"
                                            ])
                                          }
                                          to="/scholarships/polytechnic-diploma-iti"
                                        >
                                          Polytechnic/Diploma/ITI
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "graduation"
                                            ])
                                          }
                                          to="/scholarships/graduation"
                                        >
                                          Graduation
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "post-graduation"
                                            ])
                                          }
                                          to="/scholarships/post-graduation"
                                        >
                                          Post Graduation
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "PhD-Post-Doctoral"
                                            ])
                                          }
                                          to="/scholarships/phd-post-doctoral"
                                        >
                                          PhD/Post Doctoral
                                        </Link>
                                      </li>
                                    </ul>
                                  </li>
                                  <li itemRef="Type">
                                    <a href="javascript:void(0)">Type</a>
                                    <i
                                      className="fa fa-chevron-right"
                                      aria-hidden="true"
                                    />
                                    {/* Type */}
                                    <ul dataref="Type">
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Income-Based"
                                            ])
                                          }
                                          to="/scholarships/income-based"
                                        >
                                          Income Based
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Merit-Based"
                                            ])
                                          }
                                          to="/scholarships/merit-based"
                                        >
                                          Merit Based
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Cultural-Talent"
                                            ])
                                          }
                                          to="/scholarships/cultural-talent"
                                        >
                                          Cultural Talent
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Visual-Art"
                                            ])
                                          }
                                          to="/scholarships/visual-art"
                                        >
                                          Visual Art
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Sports-Talent"
                                            ])
                                          }
                                          to="/scholarships/sports-talent"
                                        >
                                          Sports Talent
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Literary-Art"
                                            ])
                                          }
                                          to="/scholarships/literary-art"
                                        >
                                          Literary Art
                                        </Link>
                                      </li>
                                    </ul>
                                  </li>
                                  <li className="ScholarshipExamFair">
                                    <Link
                                      onClick={() =>
                                        this.gtmEventHandler([
                                          "Header",
                                          "India’s Largest Study Abroad Scholarship Exam and Fair"
                                        ])
                                      }
                                      to="/scholarship-conclave?utm_source=ScholarshipMenu&utm_medium=WebMobile"
                                    >
                                      Scholarship Fair
                                    </Link>
                                  </li>
                                  <li itemRef="International">
                                    <a href="javascript:void(0)">
                                      International
                                    </a>
                                    <i
                                      className="fa fa-chevron-right"
                                      aria-hidden="true"
                                    />
                                    {/* International */}
                                    <ul dataref="International">
                                      <li>
                                        <a
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "United-States"
                                            ])
                                          }
                                          href="https://admission.buddy4study.com/courses/United-States"
                                          target="_blank"
                                        >
                                          United States
                                        </a>
                                      </li>
                                      <li>
                                        <a
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Australia"
                                            ])
                                          }
                                          href="https://admission.buddy4study.com/courses/Australia"
                                          target="_blank"
                                        >
                                          Australia
                                        </a>
                                      </li>
                                      <li>
                                        <a
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "New-Zealand"
                                            ])
                                          }
                                          href="https://admission.buddy4study.com/courses/New-Zealand"
                                          target="_blank"
                                        >
                                          New Zealand
                                        </a>
                                      </li>
                                      <li>
                                        <a
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Canada"
                                            ])
                                          }
                                          href="https://admission.buddy4study.com/courses/Canada"
                                          target="_blank"
                                        >
                                          Canada
                                        </a>
                                      </li>
                                      <li>
                                        <a
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Malaysia"
                                            ])
                                          }
                                          href="https://admission.buddy4study.com/courses/Malaysia"
                                          target="_blank"
                                        >
                                          Malaysia
                                        </a>
                                      </li>
                                      <li>
                                        <a
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "United-Kingdom"
                                            ])
                                          }
                                          href="https://admission.buddy4study.com/courses/United-Kingdom"
                                          target="_blank"
                                        >
                                          United Kingdom
                                        </a>
                                      </li>
                                      <li>
                                        <a
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "France"
                                            ])
                                          }
                                          href="https://admission.buddy4study.com/courses/France"
                                          target="_blank"
                                        >
                                          France
                                        </a>
                                      </li>
                                      <li>
                                        <a
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Germany"
                                            ])
                                          }
                                          href="https://admission.buddy4study.com/courses/Germany"
                                          target="_blank"
                                        >
                                          Germany
                                        </a>
                                      </li>
                                      <li>
                                        <a
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Ireland"
                                            ])
                                          }
                                          href="https://admission.buddy4study.com/courses/Ireland"
                                          target="_blank"
                                        >
                                          Ireland
                                        </a>
                                      </li>
                                      <li>
                                        <a
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "View more for all scholarships"
                                            ])
                                          }
                                          href="https://admission.buddy4study.com/courses"
                                          target="_blank"
                                          className="moreBtn"
                                        >
                                          <i
                                            className="fa fa-angle-right"
                                            aria-hidden="true"
                                          />
                                          view more
                                        </a>
                                      </li>
                                    </ul>
                                  </li>
                                  <li itemRef="Government">
                                    <a href="javascript:void(0)">Government</a>
                                    <i
                                      className="fa fa-chevron-right"
                                      aria-hidden="true"
                                    />
                                    {/* Government */}
                                    <ul dataref="Government">
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Post Matric Scholarships Scheme for Minorities 2018-19"
                                            ])
                                          }
                                          to="/scholarship/post-matric-scholarships-scheme-for-minorities-2018-19"
                                        >
                                          Post Matric Scholarships Scheme for
                                          Minorities 2018-19
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Pre-Matric Scholarships Scheme for Minorities 2018-19"
                                            ])
                                          }
                                          to="/scholarship/pre-matric-scholarships-scheme-for-minorities-2018-19"
                                        >
                                          Pre Matric Scholarships Scheme for
                                          Minorities 2018-19
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Merit Cum Means Scholarship for Professional and Technical Courses CS(Minorities) 2018-19"
                                            ])
                                          }
                                          to="/scholarship/merit-cum-means-scholarship-for-professional-and-technical-courses-cs-2018-19"
                                        >
                                          Merit Cum Means Scholarship for
                                          Professional and Technical Courses CS
                                          (Minorities) 2018-19
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Pre-matric Scholarship for Students with Disabilities 2018-19"
                                            ])
                                          }
                                          to="/scholarship/pre-matric-scholarship-for-students-with-disabilities-2018-19"
                                        >
                                          Pre-matric Scholarship for Students
                                          with Disabilities 2018-19
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "View more for all scholarships"
                                            ])
                                          }
                                          to="/scholarships"
                                          className="moreBtn"
                                        >
                                          <i
                                            className="fa fa-angle-right"
                                            aria-hidden="true"
                                          />
                                          view more
                                        </Link>
                                      </li>
                                    </ul>
                                  </li>
                                  <li itemRef="Popular">
                                    <a href="javascript:void(0)">Popular</a>
                                    <i
                                      className="fa fa-chevron-right"
                                      aria-hidden="true"
                                    />
                                    {/* Popular */}
                                    <ul dataref="Popular">
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "NTSE"
                                            ])
                                          }
                                          to="/scholarship/national-talent-search-exam-ntse-2018-19"
                                        >
                                          NTSE
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "National scholarship exam"
                                            ])
                                          }
                                          to="/scholarship/national-scholarship-exam-2018"
                                        >
                                          National scholarship exam
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "KVPY"
                                            ])
                                          }
                                          to="/scholarship/kishore-vaigyanik-protsahan-yojana-kvpy-2018"
                                        >
                                          KVPY
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "L'Oreal Scholarship"
                                            ])
                                          }
                                          to="/scholarship/loreal-india-for-young-women-in-science-scholarship-2018"
                                        >
                                          L'Oreal Scholarship
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Fair and Lovely Foundation Scholarship"
                                            ])
                                          }
                                          to="/scholarship/fair-and-lovely-foundation-scholarship-2018"
                                        >
                                          Fair and Lovely Foundation Scholarship
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "Keep India Smiling Foundational Scholarship Programme"
                                            ])
                                          }
                                          to="colgate-scholarship"
                                        >
                                          Keep India Smiling Foundational
                                          Scholarship Programme
                                        </Link>
                                      </li>
                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "HDFC Bank Educational Crisis Scholarship Support"
                                            ])
                                          }
                                          to="/scholarship/hdfc-bank-educational-crisis-scholarship"
                                        >
                                          HDFC Bank Educational Crisis
                                          Scholarship Support
                                        </Link>
                                      </li>

                                      <li>
                                        <Link
                                          onClick={() =>
                                            this.gtmEventHandler([
                                              "Header",
                                              "View more for all scholarships"
                                            ])
                                          }
                                          to="/scholarships"
                                          className="moreBtn"
                                        >
                                          <i
                                            className="fa fa-angle-right"
                                            aria-hidden="true"
                                          />
                                          view more
                                        </Link>
                                      </li>
                                    </ul>
                                  </li>
                                </ul>
                                <article className="socialLink">
                                  <span>For scholarship alerts</span>
                                  <span className="socialIcons">
                                    <a
                                      href="https://m.me/buddy4study"
                                      target="_blank"
                                    >
                                      <i className="facebook" />
                                    </a>
                                    <a
                                      href="https://wa.me/919582762639?text=Hit%20send"
                                      target="_blank"
                                    >
                                      <i className="whatsapp" />
                                    </a>
                                  </span>
                                </article>
                                <article className="menuContainer" />
                              </article>
                            </article>
                          </li>
                        )}
                        {isApplicationPageStatus ? null : (
                          <li>
                            <Link to="/scholarship-result">
                              Scholarship Results
                            </Link>
                          </li>
                        )}
                        {isApplicationPageStatus ? null : (
                          <li className="article">
                            <a
                              onClick={() =>
                                this.gtmEventHandler(["Header", "Article"])
                              }
                              href="https://www.buddy4study.com/article/"
                              target="_blank"
                            >
                              Articles
                              <i
                                className="fa fa-caret-down"
                                aria-hidden="true"
                              />
                            </a>
                            <article className="dropdown-content">
                              <ul>
                                <li>
                                  <a
                                    onClick={() =>
                                      this.gtmEventHandler([
                                        "Header",
                                        "Hindi Articles"
                                      ])
                                    }
                                    target="_blank"
                                    href="https://hindi.buddy4study.com/"
                                  >
                                    हिंदी लेख
                                  </a>
                                </li>
                                <li>
                                  <a
                                    onClick={() =>
                                      this.gtmEventHandler([
                                        "Header",
                                        "National Scholarship Portal"
                                      ])
                                    }
                                    target="_blank"
                                    href="https://www.buddy4study.com/article/national-scholarship-portal"
                                  >
                                    National Scholarship Portal
                                  </a>
                                </li>
                                <li>
                                  <a
                                    onClick={() =>
                                      this.gtmEventHandler([
                                        "Header",
                                        "Moma Scholarship"
                                      ])
                                    }
                                    target="_blank"
                                    href="https://www.buddy4study.com/article/moma-scholarships"
                                  >
                                    Moma Scholarship
                                  </a>
                                </li>
                                <li>
                                  <a
                                    onClick={() =>
                                      this.gtmEventHandler([
                                        "Header",
                                        "Kishore Vaigyanik Protsahan Yojana"
                                      ])
                                    }
                                    target="_blank"
                                    href="https://www.buddy4study.com/article/kvpy"
                                  >
                                    Kishore Vaigyanik Protsahan Yojana
                                  </a>
                                </li>
                                <li>
                                  <a
                                    onClick={() =>
                                      this.gtmEventHandler([
                                        "Header",
                                        "Begum Hazrat Mahal National Scholarship"
                                      ])
                                    }
                                    target="_blank"
                                    href="https://www.buddy4study.com/article/begum-hazrat-mahal-national-scholarship"
                                  >
                                    Begum Hazrat Mahal National Scholarship
                                  </a>
                                </li>
                                <li>
                                  <a
                                    onClick={() =>
                                      this.gtmEventHandler([
                                        "Header",
                                        "National Talent Search Examination"
                                      ])
                                    }
                                    target="_blank"
                                    href="https://www.buddy4study.com/article/ntse"
                                  >
                                    National Talent Search Examination
                                  </a>
                                </li>
                                <li>
                                  <a
                                    onClick={() =>
                                      props.gtmEventHandler([
                                        "Header",
                                        "Santoor Women's Scholarship for Women in India"
                                      ])
                                    }
                                    target="_blank"
                                    href="https://www.buddy4study.com/article/santoor-womens-scholarship"
                                  >
                                    Santoor Women's Scholarship for Women in
                                    India
                                  </a>
                                </li>
                                <li>
                                  <a
                                    onClick={() =>
                                      this.gtmEventHandler([
                                        "Header",
                                        "Fair and Lovely Scholarship"
                                      ])
                                    }
                                    target="_blank"
                                    href="https://www.buddy4study.com/article/fair-and-lovely-scholarship"
                                  >
                                    Fair and Lovely Scholarship
                                  </a>
                                </li>
                                <li>
                                  <a
                                    onClick={() =>
                                      this.gtmEventHandler([
                                        "Header",
                                        "Swami Vivekananda Scholarship"
                                      ])
                                    }
                                    target="_blank"
                                    href="https://www.buddy4study.com/article/swami-vivekananda-scholarship"
                                  >
                                    Swami Vivekananda Scholarship
                                  </a>
                                </li>
                                <li>
                                  <a
                                    onClick={() =>
                                      this.gtmEventHandler([
                                        "Header",
                                        "Prerana"
                                      ])
                                    }
                                    target="_blank"
                                    href="https://www.buddy4study.com/article/prerana"
                                  >
                                    Prerana scholarship
                                  </a>
                                </li>
                                <li>
                                  <a
                                    onClick={() =>
                                      this.gtmEventHandler([
                                        "Header",
                                        "Pragati Scholarship"
                                      ])
                                    }
                                    target="_blank"
                                    href="https://www.buddy4study.com/article/pragati-scholarship"
                                  >
                                    Pragati Scholarship
                                  </a>
                                </li>
                                <li>
                                  <a
                                    onClick={() =>
                                      this.gtmEventHandler([
                                        "Header",
                                        "Scholarships in India"
                                      ])
                                    }
                                    target="_blank"
                                    href="https://www.buddy4study.com/article/scholarships-in-india"
                                  >
                                    Scholarships in India
                                  </a>
                                </li>
                                <li>
                                  <a
                                    onClick={() =>
                                      this.gtmEventHandler([
                                        "Header",
                                        "Study Abroad Articles"
                                      ])
                                    }
                                    target="_blank"
                                    href="https://admission.buddy4study.com/study-abroad/"
                                  >
                                    Study Abroad Articles
                                  </a>
                                </li>
                              </ul>
                            </article>
                          </li>
                        )}
                        {isApplicationPageStatus ? null : (
                          <li className="moboDevice">
                            <a
                              href="https://www.buddy4study.com/international-study?utm_source=MenuItem&utm_medium=WebMobile"
                              target="_blank"
                            >
                              International Study
                            </a>
                          </li>
                        )}
                        {isApplicationPageStatus ? null : (
                          <li>
                            <Link to="/videos"> Videos</Link>
                          </li>
                        )}
                        {isApplicationPageStatus ? null : (
                          <li>
                            <Link to="/qna" target="_blank">
                              Q&A
                            </Link>
                          </li>
                        )}
                        {this.state.isLoggedIn ? (
                          <ProfileDropDown
                            isLoggedIn={this.state.isLoggedIn}
                            userName={`${userName ? userName : "Buddy"}`}
                            userProfilePic={userProfilePic}
                            logUserOut={this.logUserOut}
                            gtmEventHandler={this.gtmEventHandler}
                            showChangePasswordPopup={
                              this.showChangePasswordPopup
                            }
                            isApplicationPageStatus={isApplicationPageStatus}
                            urlPush={this.urlPush}
                          />
                        ) : (
                            <React.Fragment>
                              <li>
                                <a onClick={this.togglePopup.bind(this)}>
                                  <i className="fa fa-lock" />
                                  LOGIN
                              </a>
                              </li>
                              {isApplicationPageStatus ? null : (
                                <li>
                                  <Link to="/register">
                                    <i className="fa fa-user" />
                                    Register
                                </Link>
                                </li>
                              )}
                            </React.Fragment>
                          )}
                      </ul>
                    </article>
                  </nav>
                </article>
                <article
                  className={
                    isApplicationPageStatus
                      ? `menu-toggle moboAlign ${this.state.menuClicked ? "open" : ""
                      }`
                      : `menu-toggle ${this.state.menuClicked ? "open" : ""}`
                  }
                  onClick={this.handleHamburgerMenuOpen.bind(this)}
                >
                  <article className="hamburger" />
                </article>

                {/* Search Control */}
                {isApplicationPageStatus ? null : (
                  <i
                    className={`${!clicked
                      ? "fa fa-search searchicon"
                      : "fa fa-times closeSearch"
                      }`}
                    aria-hidden="true"
                    onClick={this.showAnimateSearch.bind(this)}
                  />
                )}
                {/* <i className="fa fa-times closeSearch" aria-hidden="true" /> */}
                <section
                  className={`search-wraper-common ${clicked ? "slide-in" : "slide-out"
                    }`}
                >
                  <section className="search-ctrl">
                    <span
                      className={
                        this.state.isShowMsg ? "errorTop ani" : "errorTop"
                      }
                    >
                      {this.state.showMsg}
                    </span>
                    <form
                      onSubmit={this.onSearchSubmit}
                      method="get"
                      autoCapitalize="off"
                    >
                      <input
                        type="text"
                        value={this.state.search}
                        name="search"
                        id="search"
                        placeholder="Search Scholarships"
                        autoComplete={"off"}
                        onChange={this.handleChange}
                        onClick={() =>
                          gblFunc.gaTrack.trackEvent([
                            "Search",
                            "Scholarship Search"
                          ])
                        }
                        onFocus={() =>
                          this.setState({ showMsg: "", isShowMsg: false })
                        }
                      />
                      <button type="submit">
                        <i className="fa fa-search" aria-hidden="true" />
                      </button>

                      {/* <Link
                      to={{
                        pathname: "/scholarships",
                        search: `q=${this.state.search}`
                      }}
                    >
                      <span className="search-but">
                        <i className="fa fa-search" aria-hidden="true" />
                      </span>
                    </Link> */}
                      {/* <button onClick={this.redirectToListPage}>
                      <i className="fa fa-search" aria-hidden="true" />
                    </button> */}
                    </form>
                  </section>
                </section>
              </article>
            </article>
          </header>

          {this.state.showPopup ? (
            <UserLoginRegistrationPopup
              text="Close Me"
              closePopup={this.togglePopup}
              userIsLoggedIn={this.userIsLoggedIn}
              logUserOut={this.logUserOut}
            />
          ) : null}
          {this.state.showChangePasswordPopup ? (
            <ChangePasswordPopup
              updateUserPassword={this.props.updateUserPassword}
              passwordUpdateError={this.props.passwordUpdateError}
              type={this.props.type}
              closePopup={this.closeChangePasswordPopup}
            />
          ) : null}
          {/* <LoadingBar /> */}
        </section>
      </Fragment>
    );
  }
}

const ProfileDropDown = props => {
  let vleUser =
    typeof window !== "undefined" ? localStorage.getItem("vleUser") : false;

  let cscId =
    typeof window !== "undefined" ? localStorage.getItem("cscId") : false;

  if (!props.isLoggedIn) {
    return null;
  }
  const { userProfilePic } = props;
  const defaultProfilePic =
    "https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/profile-img.png";

  return (
    <li className="loginbar">
      <span>
        <img
          property="image"
          src={userProfilePic || defaultProfilePic}
          className="img-circle"
          width="44"
          height="25"
          alt={props.userName}
        />
        {`${props.userName && props.userName.length > 8
          ? props.userName.substr(0, 8) + "..."
          : props.userName
          }`}
      </span>

      <article className="dropdown-content">
        {!props.isApplicationPageStatus ? (
          <ul>
            <li>
              {(cscId == null || cscId == "null" || cscId == "undefined") &&
                vleUser === "true" ? null : cscId && vleUser === "true" ? (
                  <Link
                    onClick={() => props.gtmEventHandler(["Header", `myprofile`])}
                    to="/csc/myprofile"
                  >
                    My Profile
                  </Link>
                ) : (
                    <Link
                      onClick={() => [
                        props.gtmEventHandler(["Header", `myprofile`]),
                        props.urlPush('PersonalInfo')]
                      }
                      to="/myProfile/PersonalInfo"
                    >
                      My Profile
                    </Link>
                  )}
            </li>
            <li>
              <Link
                onClick={(e) => [
                  props.gtmEventHandler(["Header", `matchedScholarships`]),
                  props.urlPush('MatchedScholarships')]
                }
                to="/myProfile/MatchedScholarships"
              >
                Matched Scholarships
                  </Link>
            </li>
            <li>
              {(cscId == null || cscId == "null" || cscId == "undefined") &&
                (vleUser === "true") || (cscId == null || cscId == "null" || cscId == "undefined") ||
                (vleUser === "true") ? null : (
                  <Link
                    onClick={() => [
                      props.gtmEventHandler(["Header", `appliedScholarships`]),
                      props.urlPush('ApplicationStatus')]
                    }
                    to="/myProfile/ApplicationStatus"
                  >
                    Applied Scholarships
                  </Link>)}
            </li>
            <li>
              {(cscId == null || cscId == "null" || cscId == "undefined") &&
                (vleUser === "true") || (cscId == null || cscId == "null" || cscId == "undefined") ||
                (vleUser === "true") ? null : (
                  <Link
                    onClick={() => [
                      props.gtmEventHandler(["Header", `awardedScholarships`]),
                      props.urlPush('AwardeesScholarships')]
                    }
                    to="/myProfile/AwardeesScholarships"
                  >
                    Awarded Scholarships
                  </Link>)}
            </li>
            <li>
              <Link
                onClick={() => [
                  props.gtmEventHandler(["Header", `myFavorites`]),
                  props.urlPush('MyFavorites')]
                }
                to="/myProfile/MyFavorites"
              >
                My Favorites
                  </Link>
            </li>
            <li>
              <Link
                onClick={() => [
                  props.gtmEventHandler(["Header", `questionsAndAnswers`]),
                  props.urlPush('QuestionsAndAnswers')]
                }
                to="/myProfile/QuestionsAndAnswers"
              >
                Questions & Answers
                  </Link>
            </li>
            <li onClick={props.showChangePasswordPopup}>
              <a href="javascript:void(0);">Change Password</a>
            </li>
            <li onClick={props.logUserOut}>
              <a href="javascript:void(0);">Log Out</a>
            </li>
          </ul>
        ) : (
            <ul>
              <li onClick={props.logUserOut}>
                <a href="javascript:void(0);">Log Out</a>
              </li>
            </ul>
          )}
      </article>
    </li >
  );
};

const LoginRegisterDropDown = props =>
  props.isLoggedIn ? null : (
    <li>
      <a onClick={props.togglePopup.bind(this)}>
        <i className="fa fa-user" />
        LOG IN/REGISTER{" "}
      </a>
    </li>
  );

export default Header;
