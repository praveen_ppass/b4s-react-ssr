import React, { Component, Fragment } from "react";
var change_count = 0;
class SurveyForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      change_count: 0
    },
      this.googleSurveyChange = this.googleSurveyChange.bind(this);
  }
  googleSurveyChange() {
    if (change_count === 2 /* NUMBER OF FORM PAGES */) {
      // this.setState({
      //             change_count : change_count
      // },()=>{localStorage.setItem("surveyKey",true)})
      localStorage.setItem("surveyKey", true)
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    }
    else {
      change_count++;
    }
  }
  render() {
    const { change_count } = this.state;
    const { isShow, iFrameOpen, onConfirmationFailure, onConfirmationSuccess, lang } = this.props;
    return (
      <Fragment>
        {isShow && (
          <section className="alert-overlay poupWindow">
            <article className="alertPopup">
              <article className="innerContainer" >
                <img src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/images/surveryimage.jpeg" />
                <article className="btnCtrl">
                  <button onClick={(e) => onConfirmationSuccess("eng")}>English</button>
                  <button onClick={(e) => onConfirmationSuccess("hindi")}>Hindi</button>
                  <button onClick={onConfirmationFailure}>Cancel</button>
                </article>
              </article>
            </article>
          </section>
        )}
        {iFrameOpen && (
          <section className="alert-overlay poupWindow iframe">
            <article className="alertPopup">
              <article className="innerContainer">
                <span className="closeIframe" onClick={onConfirmationFailure}>&#9747;</span>
                {lang === "eng" && (
                  <iframe id="iFrame" onLoad={() => { this.googleSurveyChange() }} src="https://docs.google.com/forms/d/e/1FAIpQLSeItO51szpGhuN6uabaVBzphFb4rCC_FJmYANpOtj2Z5fFNEw/viewform?usp=sf_link&embedded=true" width="640" height="486">Loading…</iframe>
                )}
                {lang === "hindi" && (
                  <iframe id="iFrame" onLoad={() => { this.googleSurveyChange() }} src="https://docs.google.com/forms/d/e/1FAIpQLSdF_04W72k0DHNcUTgwrMMIGoqV2J3jI3oU6LKHmO7LQD8KAQ/viewform?usp=sf_link&embedded=true" width="640" height="486">Loading…</iframe>
                )}
                {
                  change_count == 1 ? <button onClick={onConfirmationFailure}>Ok</button> : ""
                }
              </article>
            </article>
          </section>
        )}
      </Fragment>
    )
  }
}

export default SurveyForms;