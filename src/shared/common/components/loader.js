import React from "react";

const Loader = props => (
  <section className={props.isLoader ? "loader-overlay" : "hidden"}>
    <article id="loading-bar-spinner">
      <article className="spinner-icon" />
    </article>
  </section>
);
export default Loader;
