import React, { Component } from "react";
import { withRouter } from "react-router";
import { guestAuthTokenCall } from "../../../globals/globalApiCalls";
import gblFunctions from "../../../globals/globalFunctions";
import serverError from "../../common/error-404";
import {
  pagesToNotFound,
  IMButtonJS,
  paymentPages
} from "../../../constants/constants";
import { smsApplicationDetailConfig } from "../../application/formconfig";
class HOC extends Component {
  //component to handle all higher order changes to every component, right now swcrolling page to up on every component@Pushpendra
  constructor(props) {
    super(props);
    this.state = {
      isGuestTokenPresent:
        typeof window !== "undefined" && localStorage.getItem("accessToken")
          ? true
          : false,
      isGuestTokenError: false
    };
  }
  componentDidUpdate(prevProps) {
    if (
      smsApplicationDetailConfig.isSMSAdmin() &&
      (!this.props.location.pathname.includes("application") &&
        !this.props.location.pathname.includes("auth"))
    ) {
      localStorage.clear();
      window.location.href = "/";
    }
    if (this.props.location !== prevProps.location) {
      if (
        typeof window !== undefined &&
        this.props.location.pathname &&
        !this.props.location.pathname.includes("/form/") //don't scroll if its application form....
      )
        window.scrollTo(0, 0);
    }
  }
  componentDidMount() {
    if (
      window &&
      window.location &&
      window.location.hostname == "www.buddy4study.com"
    ) {
      console.log = () => {};
    }
    if (
      smsApplicationDetailConfig.isSMSAdmin() &&
      ((!this.props.location.pathname.includes("application") ||
        this.props.location.pathname.includes("/instruction")) &&
        !this.props.location.pathname.includes("auth"))
    ) {
      localStorage.clear();
      window.location.href = "/";
    }
    if (pagesToNotFound.indexOf(this.props.location.pathname) > -1) {
      this.props.history.push("/404");
      return;
    }
    if (!this.state.isGuestTokenPresent) {
      guestAuthTokenCall()
        .then(res => {
          if (typeof window !== undefined)
            localStorage.setItem("accessToken", res.data.access_token);
          this.setState({
            isGuestTokenPresent: true
          });
        })
        .catch(error => {
          this.setState({ isGuestTokenError: true });
        });
    }
    // if (paymentPages.indexOf(this.props.location.pathname) > -1) {
    //   gblFunctions.loadJsScript(IMButtonJS);
    // }
  }
  render() {
    return this.state.isGuestTokenError
      ? serverError()
      : this.state.isGuestTokenPresent
        ? this.props.children
        : "";
  }
}

export default withRouter(HOC);
