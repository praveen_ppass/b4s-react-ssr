import React, { Component } from "react";

import { Route, NavLink, HashRouter, Link } from "react-router-dom";
import { getCookie, setCookie } from "../../constants/constants";
import gblFunc from "../../globals/globalFunctions";
import FooterBanner from "../banners/containers/footerBanner";
//import SurveyForms from "../common/components/survey";
class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initScroll: false,
      name: "",
      email: "",
      isAndorid: false,
      isFooterBanner: false,
      isSchFooterBanner: true,
      showLoanAds: true,
      showModal: false,
      survey: false,
      iFrameOpen: false,
      lang: ""
    };
    this.scrollHandler = this.scrollHandler.bind(this);
    this.validateStickyThumbler = this.validateStickyThumbler.bind(this);
    this.submitSubcription = this.submitSubcription.bind(this);
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.goSubscribe = this.goSubscribe.bind(this);
    this.footerBanner = this.footerBanner.bind(this);
    this.onFooterAppHandler = this.onFooterAppHandler.bind(this);
    this.closeLoanAds = this.closeLoanAds.bind(this);
    this.onChangeSubscriber = this.onChangeSubscriber.bind(this);
    this.onCloseSubscriber = this.onCloseSubscriber.bind(this);
    this.gtmEventHandler = this.gtmEventHandler.bind(this);
    this.onConfirmationSuccess = this.onConfirmationSuccess.bind(this);
    this.onConfirmationFailure = this.onConfirmationFailure.bind(this);
  }

  validateStickyThumbler() {
    if (typeof window !== "undefined") {
      if (window.scrollY == 0) {
        this.setState({
          initScroll: false
        });
      } else {
        this.setState({
          initScroll: true
        });
      }
    }
  }

  onChangeSubscriber() {
    this.gtmEventHandler(["Footer", "subscribe now"]);
    this.setState({
      showModal: true
    });
  }
  onCloseSubscriber() {
    this.setState({
      showModal: false
    });
  }

  componentDidMount() {
    localStorage.getItem("surveyKey") ? this.setState({
      survey: false
    }) : this.setState({
      survey: true
    })
    typeof window !== "undefined"
      ? window.addEventListener("scroll", this.validateStickyThumbler)
      : "";

    this.updateDimensions();

    if (window)
      window.addEventListener("resize", this.updateDimensions.bind(this));

    if (!this.props.dashData) {
      this.props.loadDashData();
    }
  }

  componentWillUnmount() {
    const script = document.createElement("script");

    script.src = "https://platform.twitter.com/widgets.js";
    script.async = true;
    script.charset = "utf-8";
    document.body.appendChild(script);
    if (window)
      window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  submitSubcription(event) {
    event.preventDefault();
  }

  onFormFieldChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  }

  scrollHandler() {
    if (typeof window !== "undefined") {
      window.scrollTo({
        top: 0, // could be negative value
        left: 0,
        behavior: "smooth"
      });
    }
  }

  goSubscribe() {
    gblFunc.gaTrack.trackEvent(["Home-button", "Subscribe now"]);
  }

  footerBanner(cname, cvalue, hours) {
    setCookie(cname, cvalue, hours);

    if (hours === 8) {
      this.setState({
        isFooterBanner: false
      });
    } else {
      this.setState({
        isSchFooterBanner: false
      });
    }
  }
  closeLoanAds() {
    this.setState({
      showLoanAds: false
    });
  }

  updateDimensions() {
    if (typeof window !== "undefined" && window.screen.width <= 963) {
      if (document && /Android/i.test(navigator.userAgent)) {
        if (getCookie("footerName")) {
          this.setState({
            isFooterBanner: false
          });
        } else {
          this.setState({
            isFooterBanner: false
          });
        }
      }
    } else {
      this.setState({
        isFooterBanner: false
      });
    }

    if (!/Android/i.test(navigator.userAgent)) {
      if (document && getCookie("schFooterName")) {
        this.setState({
          isSchFooterBanner: false
        });
      } else {
        this.setState({
          isSchFooterBanner: true
        });
      }
    }
  }

  onFooterAppHandler() {
    if (window) {
      ga("send", {
        hitType: "event",
        eventCategory: "APP",
        eventAction: "Click",
        eventLabel: "pop-up"
      });

      window.location.href =
        "https://play.google.com/store/apps/details?id=com.budy4study.ui&referrer=utm_source%3DWebsiteBottom%26utm_medium%3DWebBottomPopUp%26utm_campaign%3DWebBottomAppInstall%26anid%3Dadmob";
    }
  }

  gtmEventHandler(gtm) {
    gblFunc.gaTrack.trackEvent(gtm);
  }

  onConfirmationSuccess(lang) {
    if (lang === "eng") {
      this.gtmEventHandler(["survey", "English"])
      this.setState({
        lang
      })
    }
    if (lang === "hindi") {
      this.gtmEventHandler(["survey", "Hindi"])
      this.setState({
        lang
      })
    }
    this.setState({
      survey: false,
      iFrameOpen: true
    }, () => {
      localStorage.setItem("surveyKey", true)
    })
  }

  onConfirmationFailure() {
    this.gtmEventHandler(["survey", "Cancel"])
    this.setState({
      survey: false,
      iFrameOpen: false
    }, () => {
      localStorage.setItem("surveyKey", true)
    })
  }

  render() {
    const dashData = this.props.dashData ? this.props.dashData : 0;
    const { isFooterBanner, isSchFooterBanner, survey, iFrameOpen, lang } = this.state;
    return (
      <section>
        {/* <SurveyForms isShow={survey} iFrameOpen={iFrameOpen} onConfirmationFailure={this.onConfirmationFailure} onConfirmationSuccess={this.onConfirmationSuccess} lang={lang} /> */}
        <article
          className={
            this.state.showLoanAds == true
              ? "educationLoanSticky"
              : "educationLoanSticky hide"
          }
        >
          <button
            type="button"
            className="close"
            onClick={() => this.closeLoanAds()}
          >
            <i>&times;</i>
          </button>

          <p>
            <a href="https://www.buddy4study.com/educationloan" target="_blank">
              Need Education Loan
            </a>
          </p>
        </article>
        <footer>
          <section className="upper-footer">
            <section className="container">
              <article className="row">
                <article className="col-md-12">
                  <article className="linkWrapper">
                    <h5>
                      <Link
                        onClick={() =>
                          this.gtmEventHandler(["Footer", "about us"])
                        }
                        to="/about-us"
                      >
                        About Us
                      </Link>
                    </h5>
                    <h5>
                      <Link
                        onClick={() => this.gtmEventHandler(["Footer", "team"])}
                        to="/team"
                      >
                        Team
                      </Link>
                    </h5>
                    <h5>
                      <Link
                        onClick={() =>
                          this.gtmEventHandler(["Footer", "scholarship result"])
                        }
                        to="/scholarship-result"
                      >
                        Scholarship Result
                      </Link>
                    </h5>
                    <h5>
                      <Link
                        onClick={() =>
                          this.gtmEventHandler(["Footer", "media partners"])
                        }
                        to="/media/partners"
                      >
                        Media Partners
                      </Link>
                    </h5>
                    <h5>
                      <Link
                        onClick={() => this.gtmEventHandler(["Footer", "faqs"])}
                        to="/faqs"
                      >
                        Faqs
                      </Link>
                    </h5>
                    {/* <h5>
                      <Link
                        onClick={() =>
                          this.gtmEventHandler(["Footer", "premium membership"])
                        }
                        to="/premium-membership"
                      >
                        PREMIUM PLAN
                      </Link>
                    </h5> */}
                  </article>
                  <article className="subscribeCtrlCounter">
                    <span className="subscribenumber">
                      <i className="fa fa-user" />
                      &nbsp;
                      <span>
                        {dashData.totalUser
                          ? parseInt(dashData.totalUser) +
                          parseInt(dashData.totalLegacyUsers)
                          : " "}
                        +
                      </span>
                    </span>
                    <button
                      className="btn mi-btn"
                      onClick={this.onChangeSubscriber}
                    >
                      Subscribe Now
                    </button>
                  </article>
                </article>
              </article>
            </section>
          </section>
          <section className="container-fluid footer equial-padding">
            <section className="container">
              <section className="row">
                {this.state.initScroll == true ? (
                  <i
                    className={
                      this.state.initScroll == true
                        ? "fa fa-arrow-up bottomarrow-thumbler ani"
                        : "fa fa-arrow-up bottomarrow-thumbler"
                    }
                    onClick={this.scrollHandler}
                  />
                ) : (
                    ""
                  )}
                <article className="col-xs-12 col-sm-4 col-md-4 borderR footer-min-height first">
                  <span className="footer-ul">
                    <h3>State wise Scholarships </h3>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/up-scholarship"
                        target="_blank"
                      >
                        Top Scholarships of Uttar Pradesh
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/maharashtra-scholarship"
                        target="_blank"
                      >
                        Top Scholarships of Maharashtra
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/bihar-scholarship"
                        target="_blank"
                      >
                        Top Scholarships of Bihar
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/west-bengal-scholarship"
                        target="_blank"
                      >
                        Top Scholarships of West Bengal
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/mp-scholarship"
                        target="_blank"
                      >
                        Top Scholarships of Madhya Pradesh
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/tn-scholarship"
                        target="_blank"
                      >
                        Top Scholarships of Tamil Nadu
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/rajasthan-scholarship"
                        target="_blank"
                      >
                        Top Scholarships of Rajasthan
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/top-scholarships-for-students-of-karnataka"
                        target="_blank"
                      >
                        Top Scholarships of Karnataka
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/gujarat-scholarship"
                        target="_blank"
                      >
                        Top Scholarships of Gujarat
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/top-scholarships-for-students-of-andhra-pradesh"
                        target="_blank"
                      >
                        Top Scholarships of Andhra Pradesh
                      </a>
                    </h5>
                  </span>
                </article>

                <article className="col-xs-12 col-sm-4 col-md-4 borderR footer-min-height moboMinHeight">
                  <span className="footer-ul">
                    <h3>Current Class Scholarships</h3>
                    <h5>
                      <Link
                        onClick={() =>
                          this.gtmEventHandler([
                            "Footer",
                            "Top Scholarships for Class 1 to 10"
                          ])
                        }
                        to="/scholarship-for/class-1-to-9"
                      >
                        Top Scholarships for Class 1 to 10
                      </Link>
                    </h5>
                    <h5>
                      <Link
                        onClick={() =>
                          this.gtmEventHandler([
                            "Footer",
                            "Top Scholarships for Class 11, 12"
                          ])
                        }
                        to="/scholarship-for/class-12"
                      >
                        {" "}
                        Top Scholarships for Class 11, 12
                      </Link>
                    </h5>
                    <h5>
                      <a
                        onClick={() =>
                          this.gtmEventHandler([
                            "Footer",
                            "Top Scholarships for Class 12 passed"
                          ])
                        }
                        href="https://www.buddy4study.com/article/scholarships-after-12th-for-college-students"
                        target="_blank"
                      >
                        Top Scholarships for Class 12 passed
                      </a>
                    </h5>

                    <h5>
                      <Link
                        onClick={() =>
                          this.gtmEventHandler([
                            "Footer",
                            "Top Scholarships for Graduation"
                          ])
                        }
                        to="/scholarship-for/graduation"
                      >
                        Top Scholarships for Graduation
                      </Link>
                    </h5>
                    <h5>
                      <Link
                        onClick={() =>
                          this.gtmEventHandler([
                            "Footer",
                            "Top Scholarships for Post-Graduation"
                          ])
                        }
                        to="/scholarship-for/post-graduation"
                      >
                        Top Scholarships for Post-Graduation
                      </Link>
                    </h5>
                    <h5>
                      <Link
                        onClick={() =>
                          this.gtmEventHandler([
                            "Footer",
                            "Top Scholarships for PhD"
                          ])
                        }
                        to="/scholarship-for/phd"
                      >
                        Top Scholarships for PhD
                      </Link>
                    </h5>
                    <h5>
                      <Link
                        onClick={() =>
                          this.gtmEventHandler([
                            "Footer",
                            "Top Scholarships for Research/Fellowship"
                          ])
                        }
                        to="/scholarship-for/phd"
                      >
                        Top Scholarships for Research/Fellowship
                      </Link>
                    </h5>
                    <h5>
                      <Link to="/scholarship-for/polytechnique-diploma">
                        Top Scholarships for Diploma/Polytechnic
                      </Link>
                    </h5>
                    <h5>
                      <Link
                        onClick={() =>
                          this.gtmEventHandler([
                            "Footer",
                            "Top Scholarships for ITI"
                          ])
                        }
                        to="/scholarship-for/iti"
                      >
                        Top Scholarships for ITI
                      </Link>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/top-scholarships-for-certifications"
                        target="_blank"
                      >
                        Top Scholarships for Certifications
                      </a>
                    </h5>
                  </span>
                </article>

                <article className="col-xs-12 col-sm-4 col-md-4 footer-min-height moboMinHeight last">
                  <span className="footer-ul">
                    <h3>Type Based Scholarships</h3>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/top-scholarships-for-indian-girls-and-women-to-study-in-india-and-abroad"
                        target="_blank"
                      >
                        Top Scholarships for Girls/Women
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/merit-scholarships"
                        target="_blank"
                      >
                        Top Scholarships based on Merit
                      </a>
                    </h5>
                    <h5>
                      <Link
                        onClick={() =>
                          this.gtmEventHandler([
                            "Footer",
                            "Top Scholarships based on Means"
                          ])
                        }
                        to="/scholarship-for/means-based-scholarships"
                      >
                        Top Scholarships based on Means
                      </Link>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/top-government-minority-scholarship-for-indian-students"
                        target="_blank"
                      >
                        Top Scholarships for Minorities
                      </a>
                    </h5>
                    <h5>
                      <Link
                        onClick={() =>
                          this.gtmEventHandler([
                            "Footer",
                            "Top Scholarships for Minorities"
                          ])
                        }
                        to="/scholarship-for/talent-based-scholarships"
                      >
                        Top Scholarships based on Talent
                      </Link>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/top-scholarships-for-students-with-disability"
                        target="_blank"
                      >
                        Top Scholarships based on Disability
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/government-scholarships"
                        target="_blank"
                      >
                        Top Government Scholarships
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/top-scholarships-for-medical-students"
                        target="_blank"
                      >
                        Top Scholarships for MBBS Students
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/top-scholarships-for-engineering-students"
                        target="_blank"
                      >
                        Top Scholarships for Engineering Students
                      </a>
                    </h5>
                    <h5>
                      <a
                        href="https://www.buddy4study.com/article/top-scholarships-to-study-abroad"
                        target="_blank"
                      >
                        Top Scholarships for Study Abroad
                      </a>
                    </h5>
                  </span>
                </article>
              </section>
            </section>
          </section>
          <section
            className={`inner-footer ${typeof window !== "undefined" && !getCookie("schFooterName")
                ? "mobileBottomPadding"
                : ""
              }`}
          >
            <section className="container">
              <article className="row">
                <article className="col-md-8">
                  <article className="footer-center">
                    {/* <h5>
 <Link to="/"> Sitemap</Link>
 </h5> */}
                    <ul>
                      <li>
                        <Link
                          onClick={() =>
                            this.gtmEventHandler([
                              "Footer",
                              "Terms & Conditions"
                            ])
                          }
                          to="/terms-and-conditions"
                        >
                          Terms & Conditions
                        </Link>
                      </li>
                      <li>
                        <span>|</span>
                        <Link
                          onClick={() =>
                            this.gtmEventHandler(["Footer", "Privacy Policy"])
                          }
                          to="/privacy-policy"
                        >
                          Privacy Policy
                        </Link>
                      </li>
                      <li>
                        <span>|</span>
                        <Link
                          onClick={() =>
                            this.gtmEventHandler(["Footer", "Disclaimer"])
                          }
                          to="/disclaimer"
                        >
                          Disclaimer
                        </Link>
                      </li>
                      <li>
                        <span>|</span>
                        <Link
                          onClick={() =>
                            this.gtmEventHandler(["Footer", "Careers"])
                          }
                          to="/careers"
                        >
                          Careers
                        </Link>
                      </li>
                      <li>
                        <span>|</span>
                        <Link
                          onClick={() =>
                            this.gtmEventHandler(["Footer", "Contact Us"])
                          }
                          to="/contact-us"
                        >
                          Contact Us
                        </Link>
                      </li>
                    </ul>
                  </article>
                </article>
                <article className="col-md-4">
                  <article className="footer-social">
                    <a
                      href="https://www.facebook.com/buddy4study/"
                      data-placement="top"
                      title="Facebook"
                      target="_blank"
                    >
                      <i className="fa fa-facebook" aria-hidden="true" />
                    </a>
                    <a
                      href="https://twitter.com/Buddy4studyOffc"
                      data-placement="top"
                      title="Twitter"
                      target="_blank"
                    >
                      <i className="fa fa-twitter" aria-hidden="true" />
                    </a>
                    <a
                      href="https://www.buddy4study.com/article/"
                      data-placement="top"
                      title="wordpress"
                      target="_blank"
                    >
                      <i className="fa fa-wordpress" aria-hidden="true" />
                    </a>
                    <a
                      href="https://www.linkedin.com/company/buddy4study-com"
                      data-placement="top"
                      title="Linkedin"
                      target="_blank"
                    >
                      <i className="fa fa-linkedin" aria-hidden="true" />
                    </a>
                    <a
                      href="https://www.youtube.com/user/buddy4study"
                      data-placement="top"
                      title="youtube"
                      target="_blank"
                    >
                      <i className="fa fa-youtube" aria-hidden="true" />
                    </a>
                    <a
                      href="https://www.instagram.com/Buddy4StudyOfficial/"
                      data-placement="top"
                      title="instagram"
                      target="_blank"
                    >
                      <i className="fa fa-instagram" aria-hidden="true" />
                    </a>
                    <a
                      href="https://in.pinterest.com/Buddy4StudyOfficial/"
                      data-placement="top"
                      title="pinterest"
                      target="_blank"
                    >
                      <i className="fa fa-pinterest" aria-hidden="true" />
                    </a>
                  </article>
                </article>
              </article>
            </section>
            <span className="text-center">
              All rights reserved © Smiling Star Advisory Pvt. Ltd.
            </span>
          </section>
          <div className="donation-button">
            <div className="float-button">
              <a href="/covid-19-support-initiative-for-daily-wage-workers-and-migrant-laborers" target="_blank">Donate for Covid-19</a>
            </div>
          </div>
        </footer>
        {this.state.showModal ? (
          <SubscriberModal
            onCloseSubscriber={this.onCloseSubscriber}
            title="Subscribe Now"
            totalSubscriber={
              dashData.totalUser
                ? parseInt(dashData.totalUser) +
                parseInt(dashData.totalLegacyUsers)
                : " "
            }
          />
        ) : null}
        {/* Start footer banner */}
        {typeof window !== "undefined" ? (
          isSchFooterBanner && !window.location.pathname.includes("faq") ? (
            <FooterBanner footerBanner={this.footerBanner} />
          ) : null
        ) : null}

        {/* App bottom Popup */}
        {/* {typeof window !== "undefined" ? (
          isFooterBanner &&
          /Android/i.test(navigator.userAgent) &&
          !window.location.pathname.includes("faq") ? (
            <section className="slideAppPopupWrapper open">
              <dd
                className="crossPopup"
                onClick={() =>
                  this.footerBanner("footerName", "footerBanner", 8)
                }
                id="crossPopupCookies"
              >
                &times;
              </dd>
              <p>Buddy4study.com</p>
              <span>
                <img
                  src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/logo-aap.jpg"
                  alt="buddy4study app"
                />
                <i>
                  Make your Scholarship search faster, simpler and stay updated.
                </i>
              </span>
              <a
                href="javascript:void(0)"
                className="_getFooterApp"
                onClick={this.onFooterAppHandler}
              >
                GET APP
              </a>
            </section>
          ) : null
        ) : null} */}
      </section>
    );
  }
}

export default Footer;
const SubscriberModal = props => {
  return (
    <article className="popup subscribePopupNew">
      <article className="popupsms">
        <article className="subscribe_inner">
          <section className="modal fade modelAuthPopup1">
            <section className="modal-dialog">
              <section className="modal-content modelBg">
                <article className="modal-header">
                  <h3 className="modal-title">{props.title}</h3>
                  <button
                    type="button"
                    className="close btnPos"
                    onClick={props.onCloseSubscriber}
                  >
                    <i>&times;</i>
                  </button>
                </article>

                <article className="modal-body subscribe">
                  <article className="content_inner">
                    <article className="row">
                      <article className="col-md-12">
                        <article className="SubscribeNow">


                          <h1>
                            <i>{props.totalSubscriber}+</i> smart students get
                            regular alerts and apply for new scholarships.
                        </h1>
                          <h2>Are you missing out? Join the league!</h2>
                          <article className="socailgp">
                            <a
                              href="https://wa.me/919582762639?text=Hit%20send"
                              target="_blank"
                            >
                              <button className="btn whatsapp">
                                Subscribe On WhatsApp
                            </button>
                            </a>
                            <a
                              href="https://chatfuel.com/bot/buddy4study"
                              target="_blank"
                            >
                              <button className="btn facebook">
                                Subscribe on Facebook Messenger
                            </button>
                            </a>
                          </article>
                          <article className="inputgroup">
                            <form
                              name="newsLtrForm"
                              method="post"
                              autoCapitalize="off"
                              action="https://mailer.buddy4study.com/subscribe"
                              accept-charset="utf-8"
                            >
                              <input
                                type="text"
                                name="name"
                                id="name"
                                maxLength="20"
                                minLength="3"
                                placeholder="Name"
                                required
                              />

                              <input
                                type="email"
                                maxLength="50"
                                id="email"
                                minLength="1"
                                name="email"
                                placeholder="Email"
                                required
                              />

                              <input
                                value="lc6Vj09u7639foguwkKbxr0w"
                                name="list"
                                type="hidden"
                              />
                              <article style={{ display: "none" }}>
                                <input id="hp" name="hp" type="text" />
                              </article>
                              <input
                                type="reset"
                                value="Reset"
                                style={{ display: "none" }}
                              />
                              <button
                                type="submit"
                                id="submit"
                                name="submit"
                                className="btn"
                              >
                                Send me weekly updates
                            </button>
                            </form>
                          </article>
                          <p>
                            Join the thriving discussions on our social channels
                        </p>
                          <ul>
                            <li>
                              <div
                                className="fb-like"
                                data-href="https://www.facebook.com/buddy4study/"
                                data-layout="button_count"
                                data-action="like"
                                data-size="small"
                                data-show-faces="true"
                                data-share="false"
                              />
                            </li>
                            <li>
                              <a
                                href="http://youtube.com/buddy4study"
                                target="_blank"
                              >
                                <i className="fa fa-youtube" />
                                &nbsp;You Tube
                            </a>
                            </li>
                            <li>
                              <a
                                href="https://twitter.com/Buddy4studyOffc"
                                target="_blank"
                              >
                                <i className="fa fa-twitter" />
                                &nbsp;Twitter
                            </a>
                            </li>
                          </ul>
                        </article>
                      </article>
                    </article>
                  </article>
                </article>
              </section>
            </section>
          </section>

        </article>

      </article>
      <div id="fb-root" />

      <script>
        {(function (d, s, id) {
          var js,
            fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s);
          js.id = id;
          js.src =
            "https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.1";
          fjs.parentNode.insertBefore(js, fjs);
        })(document, "script", "facebook-jssdk")}
      </script>
    </article>
  );
};
