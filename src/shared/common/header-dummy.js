import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
// import LoadingBar from "react-redux-loading-bar";
import UserLoginRegistrationPopup from "../login/containers/userLoginRegistrationContainer";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: false,
      isMin: false,
      showPopup: false,
      menuClicked: false,
      isLoggedIn: false
    };
    this.showAnimateSearch = this.showAnimateSearch.bind(this);
    this.handleStickyLoad = this.handleStickyLoad.bind(this);
    this.togglePopup = this.togglePopup.bind(this);
    this.userIsLoggedIn = this.userIsLoggedIn.bind(this);
    this.logUserOut = this.logUserOut.bind(this);
  }

  componentDidMount() {
    typeof window !== "undefined"
      ? window.addEventListener("scroll", this.handleStickyLoad)
      : "";
    if (this.props.isAuthenticated) {
      this.userIsLoggedIn();
    }
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  userIsLoggedIn() {
    this.setState({
      isLoggedIn: true
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isAuthenticated) {
      this.userIsLoggedIn();
    }
  }

  logUserOut() {
    if (typeof window !== "undefined") {
      window.localStorage.removeItem("USER_DATA");
    }
    this.setState({
      isLoggedIn: false
    });
  }

  /* Sticky Handler  */
  handleStickyLoad() {
    if (typeof window !== "undefined") {
      /* Sticky Header */
      if (window.scrollY > 60) {
        this.setState({
          isMin: true
        });
      } else {
        this.setState({
          isMin: false
        });
      }

      if (this.state.isMin) {
        this.setState({
          menuClicked: false
        });
      }
    }
  }

  showAnimateSearch() {
    this.setState({
      ...this.state,
      clicked: !this.state.clicked
    });
  }
  handleChange(event) {
    this.setState({ search: event.target.value });
  }
  handleHamburgerMenuOpen() {
    this.setState({
      ...this.state,
      menuClicked: !this.state.menuClicked
    });

    if (!this.state.menuClicked) {
      this.setState({
        isMin: false
      });
    } else {
      this.setState({
        isMin: true
      });
    }
  }

  render() {
    const { clicked, isLoggedIn } = this.state;

    let userName = "",
      userProfilePic = "";
    if (isLoggedIn) {
      userName =
        typeof window !== "undefined"
          ? JSON.parse(window.localStorage.getItem("USER_DATA")).FIRST_NAME
          : "";
      userProfilePic =
        typeof window !== "undefined"
          ? JSON.parse(window.localStorage.getItem("USER_DATA")).PIC
          : "";
    }

    return (
      <section>
        <header className={this.state.isMin ? "sticky" : ""}>
          <article className="container">
            <article className="col-sm-3 col-md-3 paddingLeft">
              <a href="/">
                <img
                  src={`${imgBaseUrl}white-logo.png`}
                  className="logo"
                  alt="logo"
                />
              </a>
            </article>
            <article className="col-sm-9 col-md-9 searchContent">
              <nav
                className={`site-nav ${this.state.menuClicked ? "open" : ""}`}
              >
                <article className="overflowNav">
                  <ul>
                    <li>
                      <a>
                        Scholarship
                        <i className="fa fa-caret-down" aria-hidden="true" />
                      </a>
                      <article className="dropdown-content">
                        <ul>
                          <li>
                            <a href="/scholarships">All Scholarships</a>
                          </li>
                          <li>
                            <a href="/scholarship-for/school">School</a>
                          </li>
                          <li>
                            <a href="/scholarship-for/graduation">Graduation</a>
                          </li>
                          <li>
                            <a href="/scholarship-for/post-graduation">
                              Post Graduation
                            </a>
                          </li>
                          <li>
                            <a href="/scholarship-for/phd">Ph.D</a>
                          </li>
                        </ul>
                      </article>
                    </li>
                    <li>
                      <a>Blog & News</a>
                    </li>

                    <ProfileDropDown
                      isLoggedIn={this.state.isLoggedIn}
                      userName={userName}
                      userProfilePic={userProfilePic}
                      logUserOut={this.logUserOut}
                    />
                    {this.state.isLoggedIn ? null : (
                      <li>
                        <a onClick={this.togglePopup.bind(this)}>
                          <i className="fa fa-user" />LOG IN/REGISTER{" "}
                        </a>
                      </li>
                    )}
                  </ul>
                </article>
              </nav>

              <article
                className={`menu-toggle ${
                  this.state.menuClicked ? "open" : ""
                }`}
                onClick={this.handleHamburgerMenuOpen.bind(this)}
              >
                <article className="hamburger" />
              </article>

              {/* Search Control */}
              <i
                className="fa fa-search searchicon"
                aria-hidden="true"
                onClick={this.showAnimateSearch.bind(this)}
              />
              <i className="fa fa-times closeSearch" aria-hidden="true" />
              <section
                className={`search-wraper-common ${
                  clicked ? "slide-in" : "slide-out"
                }`}
              >
                <section className="search-ctrl">
                  <form>
                    <input
                      type="text"
                      value={this.state.search}
                      name="search"
                      id="search"
                      placeholder="Search Scholarships"
                      autoComplete={"off"}
                      onChange={this.handleChange.bind(this)}
                    />
                    <button>
                      <i className="fa fa-search" aria-hidden="true" />
                    </button>
                  </form>
                </section>
              </section>
            </article>
          </article>
        </header>

        {this.state.showPopup ? (
          <UserLoginRegistrationPopup
            text="Close Me"
            closePopup={this.togglePopup}
            userIsLoggedIn={this.userIsLoggedIn}
            logUserOut={this.logUserOut}
          />
        ) : null}
        {/* <LoadingBar /> */}
      </section>
    );
  }
}

const ProfileDropDown = props => {
  if (!props.isLoggedIn) {
    return null;
  }
  const { userProfilePic } = props;
  const defaultProfilePic =
    "https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/profile-img.png";

  return (
    <li className="loginbar">
      <img
        property="image"
        src={userProfilePic || defaultProfilePic}
        alt="buddy4study profile pic"
        className="img-circle"
        width="25"
        height="25"
      />
      {props.userName}
      <b className="caret" />
      <article className="dropdown-content">
        <ul>
          <li>
            <a href="javascript:void(0);">My Profile</a>
          </li>
          <li>
            <a href="#">My Scholarships</a>
          </li>
          <li>
            <a href="javascript:void(0);">My Applications</a>
          </li>
          <li>
            <a href="#">My Subscribers</a>
          </li>
          <li>
            <a href="javascript:void(0);">Change Password</a>
          </li>
          <li onClick={props.logUserOut}>
            <a href="javascript:void(0);">Log Out</a>
          </li>
        </ul>
      </article>
    </li>
  );
};

const LoginRegisterDropDown = props =>
  props.isLoggedIn ? null : (
    <li>
      <a onClick={props.togglePopup.bind(this)}>
        <i className="fa fa-user" />LOG IN/REGISTER{" "}
      </a>
    </li>
  );

export default Header;
