import React from "react";
import { imgBaseUrl } from "../../constants/constants";

const ThankYou = () => (
  <section>
    <section className="errorbggreay hide" />
    <section className="thankspage">
      <img src={`${imgBaseUrl}icon-thanks.jpg`} alt="thankyou-icon" />
      <h2>Confirm Subscription</h2>
      <p>
        Thank you for your interest in subscribing to our Newsletter.<br /> We
          have sent you a confirmation email.<br /> Please confirm your
      subscription by clicking on the link shared in the email.
        </p>
    </section>
  </section>
);
export default ThankYou;
