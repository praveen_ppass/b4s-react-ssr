import React from "react";
import { connect } from "react-redux";
import {
  receivesScholarshipScholarVideo as fetchScholarshipScholarVideoAction,
  receivesScholarshipVideo as fetchScholarshipVideoAction,
  receivesScholarshipRecommendedVideo as fetchScholarshipRecommendedVideoAction
} from "../actions";

import scholarshipVideos from "../components/video";

const mapStateToProps = ({ scholarshipVideo }) => ({
  showLoader: scholarshipVideo.showLoader,
  scholarshipScholarVideoDataList: scholarshipVideo.scholarshipScholarVideoList,
  scholarshipVideoDataList: scholarshipVideo.scholarshipVideoList,
  scholarshipRecommendedVideoDataList:
    scholarshipVideo.scholarshipRecommendedVideoList,
  type: scholarshipVideo.type,
  isError: scholarshipVideo.isError,
  errorMessage: scholarshipVideo.errorMessage
});
const mapDispatchToProps = dispatch => ({
  loadScholarshipScholarVideo: input =>
    dispatch(fetchScholarshipScholarVideoAction(input)),
  loadScholarshipVideo: input => dispatch(fetchScholarshipVideoAction(input)),
  loadScholarshipRecommendedVideo: input =>
    dispatch(fetchScholarshipRecommendedVideoAction(input))
});

export default connect(mapStateToProps, mapDispatchToProps)(scholarshipVideos);
