import {
  FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_REQUESTED,
  FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_SUCCEEDED,
  FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_FAILED,
  FETCH_SCHOLARSHIP_VIDEO_SUCCEEDED,
  FETCH_SCHOLARSHIP_VIDEO_REQUESTED,
  FETCH_SCHOLARSHIP_VIDEO_FAILED,
  FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_SUCCEEDED,
  FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_REQUESTED,
  FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_FAILED
} from "./actions";

const initialState = {
  isError: false,
  showLoader: false,
  errorMessage: "",
  scholarshipScholarVideoList: [],
  scholarshipVideoList: [],
  scholarshipRecommendedVideosList: []
};

const scholarshipVideoReducer = (
  scholarshipvideoState = initialState,
  { type, payload }
) => {
  switch (type) {
    case FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_REQUESTED:
      return {
        payload: null,
        type,
        showLoader: true,
        isError: false,
        errorMessage: ""
      };
    case FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_SUCCEEDED:
      return {
        scholarshipScholarVideoList: payload,
        type,
        showLoader: false,
        isError: false,
        errorMessage: ""
      };

    case FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_FAILED:
      return {
        payload: payload,
        showLoader: false,
        type,
        isError: true,
        scholarshipScholarVideoList: [],
        errorMessage: payload
      };

    case FETCH_SCHOLARSHIP_VIDEO_REQUESTED:
      return {
        payload: null,
        type,
        showLoader: true,
        isError: false,
        errorMessage: ""
      };
    case FETCH_SCHOLARSHIP_VIDEO_SUCCEEDED:
      return {
        scholarshipVideoList: payload,
        type,
        showLoader: false,
        isError: false,
        errorMessage: ""
      };

    case FETCH_SCHOLARSHIP_VIDEO_FAILED:
      return {
        payload: payload,
        showLoader: false,
        type,
        isError: true,
        scholarshipVideoList: [],
        errorMessage: payload
      };

    case FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_REQUESTED:
      return {
        payload: null,
        type,
        showLoader: true,
        isError: false,
        errorMessage: ""
      };
    case FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_SUCCEEDED:
      return {
        scholarshipRecommendedVideoList: payload,
        type,
        showLoader: false,
        isError: false,
        errorMessage: ""
      };

    case FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_FAILED:
      return {
        payload: payload,
        showLoader: false,
        type,
        isError: true,
        scholarshipRecommendedVideoList: [],
        errorMessage: payload
      };

    default:
      return scholarshipvideoState;
  }
};
export default scholarshipVideoReducer;
