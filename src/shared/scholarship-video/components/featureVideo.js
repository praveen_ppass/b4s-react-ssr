import React, { Component } from "react";
import YouTube from "react-youtube";

export default class FeatureVideo extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const opts = {
      playerVars: {
        autoplay: 0,
        rel: 0
      }
    };

    return (
      <section>
        <YouTube
          videoId={this.props.featureVideoChanelId}
          opts={opts}
          target="_blank"
          onReady={this._onReady}
        />
      </section>
    );
  }

  _onReady(event) {
    event.target.pauseVideo();
  }
}
