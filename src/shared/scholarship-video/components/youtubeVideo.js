import React, { Component } from "react";
import YouTube from "react-youtube";

export default class YoutubeVideo extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const opts = {
      playerVars: {
        autoplay: 0,
        rel: 0
      }
    };

    return (
      <YouTube
        videoId={this.props.videoId}
        opts={opts}
        target="_blank"
        onReady={this._onReady}
      />
    );
  }

  _onReady(event) {
    event.target.pauseVideo();
  }
}
