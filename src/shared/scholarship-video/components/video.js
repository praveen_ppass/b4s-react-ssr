import React, { Component } from "react";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import YoutubeVideo from "./youtubeVideo";
import Featurevideo from "../components/featureVideo";
import Slider from "react-slick";
import Loader from "../../common/components/loader";
import gblFunc from "../../../globals/globalFunctions";
import {
  FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_SUCCEEDED,
  FETCH_SCHOLARSHIP_VIDEO_SUCCEEDED,
  FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_SUCCEEDED
} from "../actions";
class scholarshipVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      scholarshipScholarVideosList: [],
      scholarshipVideosList: [],
      scholarshipRecommendedVideosList: [],
      scholarshipScholarFeatureVideoList: [],
      chanelId: "",
      chanelDisc: "",
      featureVideoChanelId: ""
    };
    this.closePopupHandler = this.closePopupHandler.bind(this);
  }

  componentDidMount() {
    this.scholarSpeaksVideos("Scholar Speak Videos", "1");
    this.baatScholarshipKiVideos("Baat Scholarship Ki", "1");
    this.recommendedVideos("Recommended videos", "1");
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    switch (type) {
      case FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_SUCCEEDED:
        if (nextProps.scholarshipScholarVideoDataList.videoLibrary.length > 0) {
          this.setState({
            scholarshipScholarVideosList:
              nextProps.scholarshipScholarVideoDataList.videoLibrary,
            scholarshipScholarFeatureVideoList:
              nextProps.scholarshipScholarVideoDataList.featureVideo
          });
        }
        break;
      case FETCH_SCHOLARSHIP_VIDEO_SUCCEEDED:
        if (nextProps.scholarshipVideoDataList.videoLibrary.length > 0) {
          this.setState({
            scholarshipVideosList:
              nextProps.scholarshipVideoDataList.videoLibrary
          });
        }
        break;
      case FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_SUCCEEDED:
        if (
          nextProps.scholarshipRecommendedVideoDataList.videoLibrary.length > 0
        ) {
          this.setState({
            scholarshipRecommendedVideosList:
              nextProps.scholarshipRecommendedVideoDataList.videoLibrary
          });
        }
        break;
      default:
        break;
    }
  }
  scholarSpeaksVideos(videoCategoryName, videoStatus) {
    const params = { videoCategory: videoCategoryName, status: videoStatus };
    this.props.loadScholarshipScholarVideo(encodeURI(JSON.stringify(params)));
  }
  baatScholarshipKiVideos(videoCategoryName, videoStatus) {
    const params = { videoCategory: videoCategoryName, status: videoStatus };
    this.props.loadScholarshipVideo(encodeURI(JSON.stringify(params)));
  }
  recommendedVideos(videoCategoryName, videoStatus) {
    const params = { videoCategory: videoCategoryName, status: videoStatus };
    this.props.loadScholarshipRecommendedVideo(
      encodeURI(JSON.stringify(params))
    );
  }

  youtubePopupHandler(item) {
    gblFunc.gaTrack.trackEvent([item.videoCategory, item.description]);
    this.setState({
      isOpen: true,
      chanelId: item.videoUrl,
      chanelDisc: item.description
    });
  }

  youtubeFeatureVideoHandler(item) {
    this.setState({
      featureVideoChanelId: item.videoUrl
    });
  }
  closePopupHandler() {
    this.setState({
      isOpen: false
    });
  }

  render() {
    let settings = {
      dots: false,
      className: "center",
      infinite: true,
      autoplay: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      cssEase: "linear",
      speed: 500,
      pauseOnHover: false,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 960,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 736,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    return (
      <section>
        {this.state.isOpen === true ? (
          <section className="playerOverLay">
            <article className="playerWrapper">
              <span className="closeIcon" onClick={this.closePopupHandler} />
              <section className="scholar">
                <YoutubeVideo videoId={this.state.chanelId} />
                <p>{this.state.chanelDisc}</p>
              </section>
            </article>
          </section>
        ) : null}
        <section className="videos">
          <section className="offersWrapper">
            <section className="container-fluid topBannerBg">
              {this.state.scholarshipScholarFeatureVideoList[0] && (
                <Featurevideo
                  featureVideoChanelId={
                    this.state.scholarshipScholarFeatureVideoList[0].videoUrl
                  }
                />
              )}
            </section>
          </section>
          <BreadCrum
            classes={breadCrumObj["videos"]["bgImage"]}
            listOfBreadCrum={breadCrumObj["videos"]["breadCrum"]}
            title={breadCrumObj["videos"]["title"]}
            subTitle={breadCrumObj["videos"]["subTitle"]}
          />

          <Loader isLoader={this.props.showLoader} />

          <section className="container-fluid videogp equial-padding-b">
            <section className="container">
              <section className="row">
                <h3
                  className={
                    this.state.scholarshipRecommendedVideosList.length == 0
                      ? "hide"
                      : ""
                  }
                >
                  Recommended Videos ({
                    this.state.scholarshipRecommendedVideosList.length
                  })
                </h3>
                {this.state.scholarshipRecommendedVideosList.length > 2 ? (
                  <section className="scholar">
                    <Slider {...settings}>
                      {this.state.scholarshipRecommendedVideosList.map(item => {
                        return (
                          <article
                            className="sliderCell"
                            key={item.id}
                            onLoad={this.youtubeFeatureVideoHandler.bind(
                              this,
                              item
                            )}
                          >
                            <span
                              onClick={this.youtubePopupHandler.bind(
                                this,
                                item
                              )}
                            >
                              <img
                                src={item.videoThumbnailImage}
                                alt={item.title}
                              />
                              <i
                                className="fa fa-youtube-play"
                                aria-hidden="true"
                              />
                            </span>
                            <p>{item.description}</p>
                          </article>
                        );
                      })}
                    </Slider>
                  </section>
                ) : (
                    <section className="scholar">
                      {this.state.scholarshipRecommendedVideosList.map(item => {
                        return (
                          <article
                            className="sliderCell normalCell"
                            key={item.id}
                          >
                            <span
                              onClick={this.youtubePopupHandler.bind(this, item)}
                            >
                              <img
                                src={item.videoThumbnailImage}
                                alt={item.title}
                              />
                              <i
                                className="fa fa-youtube-play"
                                aria-hidden="true"
                              />
                            </span>
                            <p>{item.description}</p>
                          </article>
                        );
                      })}
                    </section>
                  )}
              </section>
            </section>
            <section className="container">
              <section className="row">
                <h3
                  className={
                    this.state.scholarshipScholarVideosList.length == 0
                      ? "hide"
                      : ""
                  }
                >
                  Scholar Speak Videos ({
                    this.state.scholarshipScholarVideosList.length
                  })
                </h3>
                {this.state.scholarshipScholarVideosList.length > 2 ? (
                  <section className="scholar">
                    <Slider {...settings}>
                      {this.state.scholarshipScholarVideosList.map(item => {
                        return (
                          <article
                            className="sliderCell"
                            key={item.id}
                            onLoad={this.youtubeFeatureVideoHandler.bind(
                              this,
                              item
                            )}
                          >
                            <span
                              onClick={this.youtubePopupHandler.bind(
                                this,
                                item
                              )}
                            >
                              <img
                                src={item.videoThumbnailImage}
                                alt={item.title}
                              />
                              <i
                                className="fa fa-youtube-play"
                                aria-hidden="true"
                              />
                            </span>
                            <p>{item.description}</p>
                          </article>
                        );
                      })}
                    </Slider>
                  </section>
                ) : (
                    <section className="scholar">
                      {this.state.scholarshipScholarVideosList.map(item => {
                        return this.state.scholarshipScholarVideosList.length <
                          3 ? (
                            <article
                              className="sliderCell normalCell"
                              key={item.id}
                              onLoad={this.youtubeFeatureVideoHandler.bind(
                                this,
                                item
                              )}
                            >
                              <span
                                onClick={this.youtubePopupHandler.bind(this, item)}
                              >
                                <img
                                  src={item.videoThumbnailImage}
                                  alt={item.title}
                                />
                                <i
                                  className="fa fa-youtube-play"
                                  aria-hidden="true"
                                />
                              </span>
                              <p>{item.description}</p>
                            </article>
                          ) : (
                            ""
                          );
                      })}
                    </section>
                  )}
              </section>
            </section>
            <section className="container">
              <section className="row">
                <h3
                  className={
                    this.state.scholarshipVideosList.length == 0 ? "hide" : ""
                  }
                >
                  Baat Scholarship Ki Videos ({
                    this.state.scholarshipVideosList.length
                  })
                </h3>
                {this.state.scholarshipVideosList.length > 2 ? (
                  <section className="scholar">
                    <Slider {...settings}>
                      {this.state.scholarshipVideosList.map(item => {
                        return (
                          <article
                            className="sliderCell"
                            key={item.id}
                            onLoad={this.youtubeFeatureVideoHandler.bind(
                              this,
                              item
                            )}
                          >
                            <span
                              onClick={this.youtubePopupHandler.bind(
                                this,
                                item
                              )}
                            >
                              <img
                                src={item.videoThumbnailImage}
                                alt={item.title}
                              />
                              <i
                                className="fa fa-youtube-play"
                                aria-hidden="true"
                              />
                            </span>
                            <p>{item.description}</p>
                          </article>
                        );
                      })}
                    </Slider>
                  </section>
                ) : (
                    <section className="scholar">
                      {this.state.scholarshipVideosList.map(item => {
                        return (
                          <article
                            className="sliderCell normalCell"
                            key={item.id}
                          >
                            <span
                              onClick={this.youtubePopupHandler.bind(this, item)}
                            >
                              <img
                                src={item.videoThumbnailImage}
                                alt={item.title}
                              />
                              <i
                                className="fa fa-youtube-play"
                                aria-hidden="true"
                              />
                            </span>
                            <p>{item.description}</p>
                          </article>
                        );
                      })}
                    </section>
                  )}
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default scholarshipVideo;
