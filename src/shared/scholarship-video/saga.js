import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_REQUESTED,
  FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_SUCCEEDED,
  FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_FAILED,
  FETCH_SCHOLARSHIP_VIDEO_REQUESTED,
  FETCH_SCHOLARSHIP_VIDEO_SUCCEEDED,
  FETCH_SCHOLARSHIP_VIDEO_FAILED,
  FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_REQUESTED,
  FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_SUCCEEDED,
  FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_FAILED
} from "./actions";

function getScholarshipScholarVideoUrl(videoCategoryName) {
  const url = `${apiUrl.scholarshipVideoApi}${videoCategoryName}`;
  return fetchClient.get(url).then(res => {
    return res.data;
  });
}

function* fetchScholarshipScholarVideo(input) {
  try {
    const scholarship_video = yield call(
      getScholarshipScholarVideoUrl,
      input.payload
    );
    yield put({
      type: FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_SUCCEEDED,
      payload: scholarship_video
    });
  } catch (error) {
    yield put({
      type: FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_FAILED,
      payload: error.errorMessage
    });
  }
}

function getScholarshipVideoUrl(videoCategoryName) {
  const url = `${apiUrl.scholarshipVideoApi}${videoCategoryName}`;
  return fetchClient.get(url).then(res => {
    return res.data;
  });
}

function* fetchScholarshipVideo(input) {
  try {
    const scholarship_video = yield call(getScholarshipVideoUrl, input.payload);
    yield put({
      type: FETCH_SCHOLARSHIP_VIDEO_SUCCEEDED,
      payload: scholarship_video
    });
  } catch (error) {
    yield put({
      type: FETCH_SCHOLARSHIP_VIDEO_FAILED,
      payload: error.errorMessage
    });
  }
}

function getScholarshipRecommendedVideoUrl(videoCategoryName) {
  const url = `${apiUrl.scholarshipVideoApi}${videoCategoryName}`;
  return fetchClient.get(url).then(res => {
    return res.data;
  });
}

function* fetchScholarshipRecommendedVideo(input) {
  try {
    const scholarship_video = yield call(
      getScholarshipRecommendedVideoUrl,
      input.payload
    );
    yield put({
      type: FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_SUCCEEDED,
      payload: scholarship_video
    });
  } catch (error) {
    yield put({
      type: FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_FAILED,
      payload: error.errorMessage
    });
  }
}

export default function* fetchScholarshipVideoSaga() {
  yield takeEvery(
    FETCH_SCHOLARSHIP_SCHOLAR_VIDEO_REQUESTED,
    fetchScholarshipScholarVideo
  );
  yield takeEvery(FETCH_SCHOLARSHIP_VIDEO_REQUESTED, fetchScholarshipVideo);
  yield takeEvery(
    FETCH_SCHOLARSHIP_RECOMMENDED_VIDEO_REQUESTED,
    fetchScholarshipRecommendedVideo
  );
}
