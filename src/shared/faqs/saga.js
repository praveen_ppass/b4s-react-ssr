import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

/************   All actions required in this module starts *********** */
import {
  FETCH_FAQS_SUCCEEDED,
  FETCH_FAQS_REQUESTED,
  FETCH_FAQS_FAILED,
  FETCH_FAQ_DETAILS_REQUESTED,
  FETCH_FAQ_DETAILS_SUCCEEDED,
  FETCH_FAQ_DETAILS_FAILED,
  FETCH_SUBMIT_REQUESTED,
  FETCH_SUBMIT_SUCCEEDED,
  FAQ_SUBMIT_REQUESTED,
  FAQ_SUBMIT_SUCCEEDED,
  FAQ_SUBMIT_FAILED
} from "./actions";

const faqSubmitFormRequest = inputData => {
  return fetchClient.post(apiUrl.contactUs, inputData.data).then(res => {
    return res.data;
  });
};

function* faqSubmitForm(input) {
  try {
    const response = yield call(faqSubmitFormRequest, input.payload);

    yield put({
      type: FAQ_SUBMIT_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    yield put({
      type: FAQ_SUBMIT_FAILED,
      payload: error.errorMessage
    });
  }
}

/************************************************************************************** */
/*** Fetch All Faqs Start***/
/************************************************************************************** */

const fetchUrl = () =>
  fetchClient.get(apiUrl.faqsList).then(res => {
    return res.data;
  });
function* faqs() {
  try {
    const faqs = yield call(fetchUrl);

    yield put({
      type: FETCH_FAQS_SUCCEEDED,
      payload: faqs
    });
  } catch (error) {
    yield put({
      type: FETCH_FAQS_FAILED,
      payload: error
    });
  }
}

/************************************************************************************** */
/*** Fetch All Faqs End***/
/************************************************************************************** */

/************************************************************************************** */
/*** Fetch All Faq Details Start***/
/************************************************************************************** */
const fetchfaqDetailsUrl = input =>
  fetchClient.get(apiUrl.faqDetails + "/" + input).then(res => {
    return res.data;
  });

function* faqDetails(input) {
  try {
    const faqsDt = yield call(fetchfaqDetailsUrl, input.payload.slug);
    yield put({
      type: FETCH_FAQ_DETAILS_SUCCEEDED,
      payload: faqsDt
    });
  } catch (error) {
    yield put({
      type: FETCH_FAQ_DETAILS_FAILED,
      payload: error
    });
  }
}

/************************************************************************************** */
/*** Fetch All FaqDetails End***/
/************************************************************************************** */

export default function* faqsSaga() {
  yield takeEvery(FETCH_FAQS_REQUESTED, faqs);
  yield takeEvery(FETCH_FAQ_DETAILS_REQUESTED, faqDetails);
  yield takeEvery(FAQ_SUBMIT_REQUESTED, faqSubmitForm);
}
