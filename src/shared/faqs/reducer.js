import {
  FETCH_FAQS_SUCCEEDED,
  FETCH_FAQS_REQUESTED,
  FETCH_FAQS_FAILED,
  FETCH_FAQ_DETAILS_REQUESTED,
  FETCH_FAQ_DETAILS_SUCCEEDED,
  FETCH_FAQ_DETAILS_FAILED,
  FAQ_SUBMIT_REQUESTED,
  FAQ_SUBMIT_SUCCEEDED,
  FAQ_SUBMIT_FAILED
} from "./actions";

const initialState = {
  faqList: null,
  isError: false,
  errorMessage: ""
};
const faqsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_FAQS_SUCCEEDED:
      return { showLoader: false, type, faqList: payload };
    case FETCH_FAQS_REQUESTED:
      return { showLoader: true, type, faqList: null };
    case FETCH_FAQS_FAILED:
      return { showLoader: false, type, faqList: null };
    case FETCH_FAQ_DETAILS_SUCCEEDED:
      return { showLoader: false, type, faqDetails: payload };
    case FETCH_FAQ_DETAILS_REQUESTED:
      return { showLoader: true, type, faqDetails: null };
    case FETCH_FAQ_DETAILS_FAILED:
      return { showLoader: false, type, faqDetails: null };

    case FAQ_SUBMIT_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        isSubmit: false,
        isError: false,
        errorMessage: ""
      };

    case FAQ_SUBMIT_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        isSubmit: true,
        isError: false,
        errorMessage: ""
      };

    case FAQ_SUBMIT_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isSubmit: false,
        isError: true,
        errorMessage: payload
      };

    default:
      return state;
  }
};
export default faqsReducer;
