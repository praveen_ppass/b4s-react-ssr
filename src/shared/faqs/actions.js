export const FETCH_FAQS_REQUESTED = "FETCH_FAQS_REQUESTED";
export const FETCH_FAQS_SUCCEEDED = "FETCH_FAQS_SUCCEEDED";
export const FETCH_FAQS_FAILED = "FETCH_FAQS_FAILED";

export const FETCH_FAQ_DETAILS_REQUESTED = "FETCH_FAQ_DETAILS_REQUESTED";
export const FETCH_FAQ_DETAILS_SUCCEEDED = "FETCH_FAQ_DETAILS_SUCCEEDED";
export const FETCH_FAQ_DETAILS_FAILED = "FETCH_FAQ_DETAILS_FAILED";

export const FAQ_SUBMIT_REQUESTED = "FAQ_SUBMIT_REQUESTED";
export const FAQ_SUBMIT_SUCCEEDED = "FAQ_SUBMIT_SUCCEEDED";
export const FAQ_SUBMIT_FAILED = "FAQ_SUBMIT_FAILED";

export const submitFAQ = data => ({
  type: FAQ_SUBMIT_REQUESTED,
  payload: { data: data }
});

export const faqsRequested = data => ({
  type: FETCH_FAQS_REQUESTED,
  payload: { data: data }
});

export const receivesTermsRequested = payload => ({
  type: FETCH_FAQS_SUCCEEDED,
  payload
});

export const fetchFaqDetail = data => ({
  type: FETCH_FAQ_DETAILS_REQUESTED,
  payload: {
    slug: data
  }
});

export const receivesFaqDetail = payload => ({
  type: FETCH_FAQ_DETAILS_SUCCEEDED,
  payload
});
