import React from "react";
import { connect } from "react-redux";
import { faqsRequested as fetchFaqsAction } from "../actions";
import { submitFAQ as submitFAQAction } from "../../contact-us/actions";
import Faqs from "../components/faqsPage";

const mapStateToProps = ({ faqs }) => ({
  faqsList: faqs.faqList,
  showLoader: faqs.showLoader
});

const mapDispatchToProps = dispatch => ({
  loadfaqs: () => dispatch(fetchFaqsAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(Faqs);
