import React from "react";
import { connect } from "react-redux";
import { submitFAQ as submitFAQAction } from "../../faqs/actions";
import FaqWinner from "../components/faq-winner";

const mapStateToProps = ({ faqs }) => ({
  isSubmit: faqs.isSubmit,
  showLoader: faqs.showLoader,
  payload: faqs,
  isError: faqs.isError,
  errorMessage: faqs.errorMessage
});
const mapDispatchToProps = dispatch => ({
  submitFAQ: data => dispatch(submitFAQAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(FaqWinner);
