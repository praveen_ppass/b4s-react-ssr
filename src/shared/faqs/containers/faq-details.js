import React from "react";
import { connect } from "react-redux";
import { fetchFaqDetail as fetchFaqDetailAction } from "../actions";
import FaqDetails from "../components/faq-detailsPage";
import { submitContactUs as submitContactUsAction } from "../../contact-us/actions";

const mapStateToProps = ({ faqs }) => ({
  faqDetails: faqs.faqDetails,
  showLoader: faqs.showLoader
});
const mapDispatchToProps = dispatch => ({
  loadfaqDetails: slug => dispatch(fetchFaqDetailAction(slug)),
  submitContactUs: contactData => dispatch(submitContactUsAction(contactData))
});

export default connect(mapStateToProps, mapDispatchToProps)(FaqDetails);
