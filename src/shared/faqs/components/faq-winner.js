import React, { Component } from "react";
import Loader from "../../common/components/loader";
import { messages } from "../../../constants/constants";
import AlertMessage from "../../common/components/alertMsg";

import { ruleRunner } from "../../../validation/ruleRunner";
import {
  required,
  isEmail,
  minLength,
  isNumeric,
  lengthRange,
  isMobileNumber
} from "../../../validation/rules";

import {
  FAQ_SUBMIT_REQUESTED,
  FAQ_SUBMIT_SUCCEEDED,
  FAQ_SUBMIT_FAILED
} from "../actions";

class FaqsWinner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      mobile: "",
      subject: "",
      comment: "",
      isShowAlert: false,
      statusAlert: false,
      alertMsg: "",
      validations: {
        name: null,
        email: null,
        mobile: null,
        subject: null,
        comment: null
      }
    };
    this.submitFAQ = this.submitFAQ.bind(this);
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
  }

  hideAlert() {
    //  close popup

    this.setState({
      isShowAlert: false
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSubmit && nextProps.payload.type == FAQ_SUBMIT_SUCCEEDED) {
      this.setState({
        isShowAlert: true,
        statusAlert: true,
        alertMsg: messages.generic.success,
        name: "",
        email: "",
        mobile: "",
        subject: "",
        comment: ""
      });
      return false;
    } else if (nextProps.payload.type == FAQ_SUBMIT_FAILED) {
      this.setState({
        isShowAlert: false,
        statusAlert: false,
        alertMsg: messages.generic.failed,
        name: "",
        email: "",
        mobile: "",
        subject: "",
        comment: ""
      });
      return false;
    }
  }

  onFormFieldChange(event) {
    const { id, value } = event.target;
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );
    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];
    this.setState({
      [id]: value,
      validations
    });
  }

  hideAlert() {
    //  close popup
    this.setState({
      showAlertMsg: "",
      isShowAlert: false,
      name: "",
      email: "",
      mobile: "",
      subject: "",
      comment: ""
    });
  }
  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "name":
        validationObject.name = "* Name";
        validationObject.validationFunctions = [required];
        return validationObject;

      case "email":
        validationObject.name = "* Email";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;

      case "mobile":
        validationObject.name = "* Mobile";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;

      case "subject":
        validationObject.name = "* Subject";
        validationObject.validationFunctions = [required, lengthRange(3, 50)];
        return validationObject;

      case "comment":
        validationObject.name = "*Comment";
        validationObject.validationFunctions = [required];
        return validationObject;
    }
  }
  /************ end - validation part - get message and required validation******************* */

  /************ start - validation part - check form submit validation******************* */
  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }
  /************ start - validation part - check form submit validation******************* */
  submitFAQ(event) {
    event.preventDefault();
    const { name, email, mobile, subject, comment } = this.state;
    if (this.checkFormValidations()) {
      this.props.submitFAQ({
        name,
        email,
        mobile,
        subject,
        comment,
        type: this.props.type
      });
    }
  }
  render() {
    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          isShow={this.state.isShowAlert}
          msg={this.state.alertMsg}
          close={this.hideAlert}
          status={this.state.statusAlert}
        />

        <section className="container-fluid faq-winner">
          <section className="box">
            <section className="container equial-padding-t">
              <article className="col-md-12 text-center">
                <h5>Still Need Help? Send us a Note!</h5>
                <p>Submit Your Query</p>
                <form autoCapitalize="off">
                  <article className="ctrl-wrapper">
                    <article className="col-md-12">
                      <article className="row">
                        <article className="col-md-6">
                          <article className="form-group">
                            <label htmlFor="fullname">Full Name</label>
                            <input
                              type="text"
                              name="name"
                              id="name"
                              className="form-control"
                              placeholder="Enter your full name"
                              value={this.state.name}
                              onChange={this.onFormFieldChange}
                              required
                            />

                            {this.state.validations.name ? (
                              <span className="error animated bounce">
                                {this.state.validations.name}
                              </span>
                            ) : null}
                          </article>
                        </article>
                        <article className="col-md-6">
                          <article className="form-group">
                            <label htmlFor="email">Email</label>
                            <input
                              type="email"
                              name="email"
                              id="email"
                              className="form-control"
                              placeholder="Enter your email"
                              value={this.state.email}
                              onChange={this.onFormFieldChange}
                              required
                            />
                            {/* <span className="error animated bounce">* Please enter valid email address.</span>
                                <span className="error animated bounce">* Email is required.</span> */}
                            {this.state.validations.email ? (
                              <span className="error animated bounce">
                                {this.state.validations.email}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      </article>
                      <article className="row">
                        <article className="col-md-6">
                          <article className="form-group">
                            <label htmlFor="number">Contact Number</label>
                            <input
                              type="text"
                              name="mobile"
                              id="mobile"
                              value={this.state.mobile}
                              maxLength={10}
                              className="form-control"
                              placeholder="Enter your contact number"
                              onChange={this.onFormFieldChange}
                              required
                            />
                            {/* <span className="error animated bounce">* Mobile Number is required.</span>
                                            <span className="error animated bounce">* Please enter correct number.</span> */}
                            {this.state.validations.mobile ? (
                              <span className="error animated bounce">
                                {this.state.validations.mobile}
                              </span>
                            ) : null}
                          </article>
                        </article>
                        <article className="col-md-6">
                          <article className="form-group">
                            <label htmlFor="subject">Subject</label>
                            <input
                              type="text"
                              name="subject"
                              id="subject"
                              className="form-control"
                              placeholder="Subject...!"
                              value={this.state.subject}
                              onChange={this.onFormFieldChange}
                            />
                            {/* <span className="error animated bounce">* Subject is required.</span>
                                            <span className="error animated bounce">* Subject is too short.</span>
                                            <span className="error animated bounce">* Subject is too long.</span> */}
                            {this.state.validations.subject ? (
                              <span className="error animated bounce">
                                {this.state.validations.subject}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      </article>
                      <article className="row">
                        <article className="col-md-12">
                          <article className="form-group textAreaCrtlfaq">
                            <label htmlFor="message">Message</label>
                            <textarea
                              name="comment"
                              id="comment"
                              className="form-control"
                              placeholder="Start writing your query...|"
                              value={this.state.comment}
                              onChange={this.onFormFieldChange}
                            />
                            {this.state.validations.comment ? (
                              <span className="error animated bounce">
                                {this.state.validations.comment}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      </article>
                    </article>
                  </article>
                  <article className="relative fullwidth">
                    <button
                      type="submit"
                      id="submit"
                      name="submit"
                      className="btn semibold"
                      onClick={this.submitFAQ}
                    >
                      Submit Query
                    </button>
                  </article>
                  {this.props.isError ? (
                    <span className="error animated bounce">
                      {this.props.errorMessage}
                    </span>
                  ) : null}
                </form>
              </article>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default FaqsWinner;
