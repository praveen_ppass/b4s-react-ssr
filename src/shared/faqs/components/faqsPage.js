import React, { Component } from "react";
import { breadCrumObj } from "../../../constants/breadCrum";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import BreadCrum from "../../components/bread-crum/breadCrum";
import { formInputs } from "../../../constants/formInput";
/* import FaqsWinner from "./faq-winner"; */
import Loader from "../../common/components/loader";
import { ruleRunner } from "../../../validation/ruleRunner";
import FaqContainer from "../../faqs/containers/faq-winner";

import {
  required,
  minLength,
  isEmail,
  isNumeric,
  shouldMatch,
  isMobileNumber
} from "../../../validation/rules";
import gblFunc from "../../../globals/globalFunctions";

class Faqs extends Component {
  constructor(props) {
    super(props);

    /*    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this.inputChangedHandler = this.inputChangedHandler.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this); */
  }

  componentDidMount() {
    if (breadCrumObj["faqs"]["breadCrum"][1]) {
      breadCrumObj["faqs"]["breadCrum"].splice(1, 1);
    }
    this.props.loadfaqs();
  }

  getFAQListing() {
    if (this.props.faqsList != null) {
      return this.props.faqsList.map((item, index) => {
        return (
          <li>
            <Link to={"/faq/" + item.slug}>
              {gblFunc.replaceWithLoreal(item.title, 1)}
            </Link>
          </li>
        );
      });
    }
  }

  render() {
    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <section className="faqs">
          <BreadCrum
            classes={breadCrumObj["faqs"]["bgImage"]}
            listOfBreadCrum={breadCrumObj["faqs"]["breadCrum"]}
            title={breadCrumObj["faqs"]["title"]}
            subTitle={breadCrumObj["faqs"]["subTitle"]}
          />
          <section className="faq-details equial-padding-t">
            <article className="container">
              <article className="row">
                <article className="col-md-12">
                  <h2>Get answers to all your questions about scholarships</h2>
                </article>
                <article className="col-md-12">
                  <ul className="questionList"> {this.getFAQListing()} </ul>
                </article>
              </article>
            </article>

            <FaqContainer type="faq" />
          </section>
        </section>
      </section>
    );
  }
}

export default Faqs;
