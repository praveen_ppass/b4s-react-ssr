import React, { Component } from "react";
import Loader from "../../common/components/loader";
import FaqContainer from "../../faqs/containers/faq-winner";
import BreadCrum from "../../components/bread-crum/breadCrum";
import { breadCrumObj } from "../../../constants/breadCrum";
class FaqsDetails extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let slug = this.props.match.params.slug ? this.props.match.params.slug : "";
    this.props.loadfaqDetails(slug);

    if (slug) {
      breadCrumObj["faqs"]["breadCrum"].push({
        url: "#",
        name: slug
      });
    }
  }

  getQuestion() {
    if (this.props.faqDetails != null) {
      return this.props.faqDetails.question;
    }
  }

  getAnswer() {
    if (this.props.faqDetails != null) {
      return this.props.faqDetails.answer;
    }
  }

  render() {
    const { slug } = this.props.match.params;
    let faqBreadCrum = null;

    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <section className="faqs">
          {/* breacrum trail */}
          <BreadCrum
            classes={breadCrumObj["faqs"]["bgImage"]}
            listOfBreadCrum={breadCrumObj["faqs"]["breadCrum"]}
            subTitle={breadCrumObj["faqs"]["subTitle"]}
          />
          <section className="faq-details">
            <article className="container">
              <article className="row">
                <article className="col-md-12">
                  {this.props.staticContext ? (
                    <h1>{this.props.staticContext.appData}</h1>
                  ) : (
                      <h1
                        dangerouslySetInnerHTML={{ __html: this.getQuestion() }}
                      />
                    )}

                  <p dangerouslySetInnerHTML={{ __html: this.getAnswer() }} />
                </article>
              </article>
            </article>
          </section>
          <FaqContainer type="faqDetail" />
        </section>
      </section>
    );
  }
}

export default FaqsDetails;
