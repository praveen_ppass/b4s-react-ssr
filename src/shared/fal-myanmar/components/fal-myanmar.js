import React, { Component } from "react";
class FALMyanmar extends Component {
  render() {
    return (
      <section className="container-fluid  details-page-wrapper-myanmar">
        <section className="container">
          <section className="row">
            <section className="col-md-12 padding0">
              <ul className="breadcrumb">
                <li>
                  <a href="/">Home</a>
                </li>
                <li>
                  <a href="/scholarships">Scholarship</a>
                </li>
                <li className="marginleft0">
                  <a property="url">
                    Fair & Lovely Scholarship and Mentorship Program 2019
                  </a>
                </li>
              </ul>
            </section>
          </section>
          <section className="row">
            <section className="col-md-12">
              <section className="row">
                <section className="moboCellOrder">
                  <article className="row flex-item">
                    <section className="col-md-9 col-sm-9 heading-static">
                      <h1 className="titleText">
                        Fair & Lovely Scholarship and Mentorship Program 2019
                      </h1>
                    </section>
                    {/* <section className="col-md-3 col-sm-3">
                      <article className="col-md-12 date">
                        Last updated on&nbsp;
                        <br />
                        <span>
                          <i className="fa fa-calendar" /> &nbsp;21-Feb-2019
                        </span>
                      </article>
                    </section> */}
                  </article>
                  <article className="row flex-item">
                    <article className="moboCellOrderIn">
                      <section className="col-md-9 col-sm-9 flex-item padding0">
                        <article className="panel boxPadding posR">
                          <article className="data-row">
                            <h2>
                              What is Fair & Lovely Scholarship and Mentorship Program 2019?
                            </h2>
                            <div>
                              <p>
                                Fair and Lovely Scholarship and Mentorship Program Myanmar
                                invites applications from young women who aim to
                                achieve their dreams through education. Started
                                in 2003, this global initiative of Fair and
                                Lovely Scholarship and Mentorship has supported education
                                of over 2000 girls. The scholarship program
                                supports the higher education of girl students
                                by covering their academic expenses.
                              </p>
                            </div>
                          </article>

                          <article className="data-row">
                            <h2>Who can apply for this scholarship ?</h2>
                            <div>
                              <p>
                                To be eligible for this scholarship programme,
                                an applicant must:&nbsp;
                              </p>

                              <ul>
                                <li>
                                  Must be a 16 years and above female citizen of
                                  Myanmar, studying in the country
                                </li>
                                <li>
                                  Must have at least 50% of marks in every
                                  subject (or overall 300 scores and above) in
                                  Grade-11.
                                </li>
                                <li>
                                  Have a family income not exceeding 5 lakhs
                                  kyats per month.
                                </li>
                                <li>
                                  Be interested in pursuing undergraduate
                                  courses.
                                </li>
                              </ul>
                            </div>
                          </article>
                          <article className="data-row">
                            <h2>What are the important dates ?</h2>
                            <ul>
                              <li>Applications start from October 01, 2019</li>
                              <li>Applications close on February 29, 2020</li>
                            </ul>
                          </article>

                          <article className="data-row">
                            <h2> What are the benefits?</h2>
                            <p>The selected girl will receive 8 lakhs for one year (or) 30 lakhs for 4 years university expense. In addition to that, they will be mentored by experienced leaders in various field.</p>

                          </article>

                          <article className="data-row">
                            <section className="faqs">
                              <h3>Frequently Asked Questions </h3>
                              <section className="accordion">
                                <h2>
                                  What levels of education does this scholarship
                                  programme support?
                                </h2>
                                <p>
                                  The scholarships are available for girl
                                  students who have attained an age of 16 years
                                  as on 28 February, 2020. The programme is
                                  available for those students who are pursuing
                                  undergraduate studies from any recognized
                                  institution in Myanmar.
                                </p>
                              </section>
                              <section className="accordion">
                                <h2>
                                  Can students who are currently in their 2nd
                                  year of graduation programme apply for this
                                  scholarship programme?
                                </h2>
                                <p>
                                  Yes. As long as the students fulfil the
                                  remaining eligibility criteria, they may apply
                                  for the scholarship during any year of their
                                  undergraduate studies.
                                </p>
                              </section>
                            </section>
                          </article>
                          <article className="data-row">
                            <article className="col-md-6 nopadding">
                              <h2>Important Links</h2>
                              <ul className="optional-links-text">
                                <li>
                                  <a
                                    href="/application/FAL10/instruction"
                                    target="_blank"
                                  >
                                    <strong>Apply online link</strong>
                                  </a>
                                </li>
                              </ul>
                            </article>
                            <article className="col-md-6 nopadding">
                              <h2>Contact details</h2>
                              <article>
                                <p>09 9600 54850</p>
                                <p>
                                  Visit Facebook Page &nbsp;
                                  <a
                                    href=" https://www.facebook.com/fairandlovelymyanmar/"
                                    target="_blank"
                                  >
                                    ‘Fair and Lovely Myanmar Facebook’
                                  </a>
                                </p>
                              </article>
                            </article>
                          </article>
                        </article>
                        <article className="panel boxPadding disclaimer">
                          <article className="data-row">
                            <h2>Disclaimer</h2>
                            <p>
                              All the information provided here is for reference
                              purpose only. While we strive to list all
                              scholarships for benefit of students, Buddy4Study
                              does not guarantee the accuracy of the data
                              published here. For official information, please
                              refer to the official website.{" "}
                              <a href="/disclaimer">read more</a>
                            </p>
                          </article>
                        </article>
                      </section>
                      <section className="col-md-3 col-sm-3 flex-item padding0">
                        <section className="panel ">
                          <section className="left-cell-1">
                            <img
                              property="image"
                              src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/F%26L-Logo.png"
                              alt="Fair & Lovely Scholarship and Mentorship Program"
                              className="school-logo"
                            />

                            <article className="deadlineBox">
                              <dd>
                                <span>Deadline:</span> February 29, 2020
                              </dd>
                              <a
                                href="/application/FAL10/instruction"
                                target="_blank"
                              >
                                <button className="but-pink">Apply Now</button>
                              </a>
                              <button className="but-pink">
                                <a href="https://s3.ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/Files/FAL/form/FAL-Myanmar.pdf" target="_blank" className="colorwhite">
                                  Read in Burmese
                                  </a>
                              </button>
                            </article>
                          </section>
                          <section className="left-cell-2">
                            <article className="cellBox">
                              <article>
                                <dd>
                                  <i>Award</i>Up to 30 Lakh Kyats for graduation
                                  programme
                                </dd>
                                <dd>
                                  <i>Eligibility</i>Girl students (Myanmar)
                                  above 16 years age
                                </dd>
                                <dd>
                                  <i>Region</i>
                                  Myanmar
                                </dd>
                              </article>
                            </article>
                          </section>
                        </section>
                      </section>
                    </article>
                  </article>
                </section>
              </section>
            </section>
          </section>
        </section>
      </section >
    );
  }
}

export default FALMyanmar;
