import React from "react";
import { Link } from "react-router-dom";
const nsubscribeUpdateProfile = () => {
  return (
    <article>
      <article className="grp2">
        <p>Update your preferences</p>
        <form
          className="subsribe"
          name="newsLtrForm"
          method="post"
          autoCapitalize="off"
          action="https://mailer.buddy4study.com/subscribe"
          accept-charset="utf-8"
        >
          <input
            type="email"
            maxLength="50"
            id="email"
            minLength="1"
            name="email"
            placeholder="Email"
            required
          />
          <input
            type="text"
            name="fname"
            id="name"
            maxLength="20"
            minLength="3"
            placeholder="First Name"
            required
          />
          <input
            type="text"
            name="fname"
            id="name"
            maxLength="20"
            minLength="3"
            placeholder="Last Name"
            required
          />
          <article className="subgrp">
            <button type="submit" className=" greenBtn">
              Update Profile
            </button>
            <span>or</span>
            <Link to="/"> Unsubscribe</Link>
          </article>
        </form>
      </article>

      <Link to="/"> Return to our website</Link>
    </article>
  );
};
export default nsubscribeUpdateProfile;
