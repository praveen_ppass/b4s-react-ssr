import React, { Component } from "react";
import UnsubscribeReasonFrom from "./unsubscribeReasonFrom";
import UpdateProfile from "./unsubscribeUpdateProfileForm";
import { Link } from "react-router-dom";
import Loader from "../../common/components/loader";

import { imgBaseUrl } from "./../../../constants/constants";

import {
  FETCH_UNSUBSCRIBED_REQUESTED,
  FETCH_UNSUBSCRIBED_SUCCEEDED,
  FETCH_UNSUBSCRIBED_FAILED,
  UPDATE_UNSUBSCRIBED_SUCCEEDED
} from "../actions";
import gblFunc from "../../../globals/globalFunctions";

class ThankYou extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMessageDisplay: false,
      othersReason: "",
      userSubscribedDataList: [],
      radioUnsubscribeReasonGroup: {},
      isTrue: false,
      token: null,
      isAlreadyUnsubscribe: false,
      unSubsList: null,
      isSubscribeAgain: false
    };
    this.userUnsubscribed = this.userUnsubscribed.bind(this);
    this.textAreaHandler = this.textAreaHandler.bind(this);
    this.clickRadioUnsubscribeReasonHandler = this.clickRadioUnsubscribeReasonHandler.bind(
      this
    );
    this.radioUnsubscribeReasonHandler = this.radioUnsubscribeReasonHandler.bind(
      this
    );
    this.onSubscribeAgain = this.onSubscribeAgain.bind(this);
  }
  componentDidMount() {
    const { location } = this.props;
    if (location && location.search && location.search.includes("token=")) {
      const token = location.search.split("=")[1];
      this.setState(
        {
          token
        },
        () => this.userUnsubscribed(this.state.token)
      );
    } else {
      this.props.history.push("/");
    }
  }
  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    switch (type) {
      case FETCH_UNSUBSCRIBED_SUCCEEDED:
        if (nextProps.userSubscribedData.unsubscriptionTypes.length > 0) {
          const { unsubscribed } = nextProps.userSubscribedData;
          if (unsubscribed) {
            this.setState({
              isAlreadyUnsubscribe: true,
              unSubsList: nextProps.userSubscribedData
            });
          } else {
            this.setState({
              unSubsList: nextProps.userSubscribedData,
              userSubscribedDataList:
                nextProps.userSubscribedData.unsubscriptionTypes
            });
          }
        }
        break;
      case UPDATE_UNSUBSCRIBED_SUCCEEDED:
        const { status } = nextProps.userStatus;
        this.setState({
          isMessageDisplay: true,
          isSubscribeAgain: status ? false : true,
          isAlreadyUnsubscribe: false
        });
        break;
      default:
        break;
    }
  }
  userUnsubscribed(token) {
    this.props.loadFetchUserUnsubscribed(token);
  }

  textAreaHandler(e) {
    let othersReason = e.target.value;
    this.setState({
      othersReason
    });
  }

  clickRadioUnsubscribeReasonHandler(e) {
    e.preventDefault();
    let currentothersReason = this.state.othersReason;
    const userId = gblFunc.getStoreUserDetails()["userId"];
    if (this.state.radioUnsubscribeReasonGroup["radio"].id != 914) {
      this.props.postUserUnsubscribed({
        value: {
          otherReason: "",
          unsubscriptionTypeId: this.state.radioUnsubscribeReasonGroup["radio"]
            .id,
          unsubscriptionType: this.state.radioUnsubscribeReasonGroup["radio"]
            .value,
          token: this.state.token
        }
      });
    } else {
      this.props.postUserUnsubscribed({
        value: {
          otherReason: currentothersReason,
          unsubscriptionTypeId: this.state.radioUnsubscribeReasonGroup["radio"]
            .id,
          token: this.state.token
        }
      });
    }
    this.setState({
      isMessageDisplay: true
    });
  }
  radioUnsubscribeReasonHandler(e) {
    const otherVal = e.target.value;
    let radioUnsubscribeReasonGroup = this.state.radioUnsubscribeReasonGroup;
    for (let key in radioUnsubscribeReasonGroup) {
      radioUnsubscribeReasonGroup["radio"] = {
        isTrue: false,
        id: key,
        value: otherVal
      };
    }
    this.state.radioUnsubscribeReasonGroup["radio"] = {
      isTrue: e.target.checked,
      id: e.target.id,
      value: otherVal
    };
    this.setState({
      radioUnsubscribeReasonGroup: radioUnsubscribeReasonGroup,
      otherVal,
      isTrue: this.state.radioUnsubscribeReasonGroup["radio"].isTrue
    });
  }

  onSubscribeAgain() {
    const updateSubs = { ...this.state.unSubsList };
    updateSubs.data.status = false;
    updateSubs.data.token = this.state.token;

    this.props.postUserUnsubscribed({ value: updateSubs.data });
  }

  render() {
    const { userSubscribedDataList } = this.state;
    const userId = gblFunc.getStoreUserDetails()["userId"];

    return (
      <article>
        <Loader isLoader={this.props.showLoader} />
        <section className="unsubscribe">
          <section className="container-fluid equial-padding">
            <section className="container">
              <section className="row">
                <section className="col-md-6 col-sm-6">
                  {!this.state.isMessageDisplay &&
                    this.state.isAlreadyUnsubscribe ? (
                      <section className="control-group">
                        <i className="fa fa-frown-o" />
                        {/* <h2>Unsubscribed!</h2> */}
                        <p>
                          It seems you are no longer subscribed to our content. If
                          you wish to start receiving our newsletters and alerts
                        on scholarships once again, please click to{" "}
                          <a onClick={this.onSubscribeAgain}>Subscribe</a>
                        </p>
                      </section>
                    ) : this.state.isMessageDisplay &&
                      this.state.isSubscribeAgain &&
                      !this.state.isAlreadyUnsubscribe ? (
                        <section className="control-group">
                          {/* <img
                        src={`${imgBaseUrl}unsubscribed.jpg`}
                        alt="Buddy4study-icon"
                      /> */}
                          {/* <h2>Unsubscribed!</h2> */}
                          <i className="fa fa-smile-o" />
                          <p>
                            Yay! Welcome back, Buddy! You are now subscribed to our
                            newsletters and alerts and will receive the latest
                            updates in your inbox.
                      </p>
                        </section>
                      ) : !this.state.isMessageDisplay &&
                        this.state.isAlreadyUnsubscribe ? (
                          <section className="control-group">
                            <i className="fa fa-frown-o" />
                            {/* <h2>Unsubscribed!</h2> */}
                            <p>
                              It seems you are no longer subscribed to our content. If
                              you wish to start receiving our newsletters and alerts
                        on scholarships once again, please click to{" "}
                              <a onClick={this.onSubscribeAgain}>Subscribe</a>
                            </p>
                          </section>
                        ) : (
                          <section className="control-group">
                            {" "}
                            {this.state.isMessageDisplay &&
                              !this.state.isAlreadyUnsubscribe ? (
                                <i className="fa fa-frown-o" />
                              ) : null}
                            {this.state.isMessageDisplay &&
                              !this.state.isAlreadyUnsubscribe ? (
                                <h2>Unsubscribed!</h2>
                              ) : null}
                            {!this.state.isMessageDisplay &&
                              !this.state.isAlreadyUnsubscribe ? (
                                <h3>
                                  Are you sure you wish to leave us?{" "}
                                  <i className="fa fa-frown-o small" />
                                </h3>
                              ) : null}
                            {this.state.isMessageDisplay &&
                              !this.state.isAlreadyUnsubscribe ? (
                                <p>
                                  {" "}
                                  We are sorry to see you go. You shall no longer
                                  receive our newsletter content and deadline alerts for
                                  the latest scholarships. If you wish to start
                                  receiving our scholarship content once again, please
                          click to{" "}
                                  <a onClick={this.onSubscribeAgain}>Subscribe</a>
                                </p>
                              ) : (
                                <p>
                                  As India’s largest scholarship platform, it has always
                                  been our effort to bring you the most relevant
                                  information and alerts on scholarships. If we have
                                  faltered in this endeavor anywhere, do let us know how
                                  we can improve our service.
                        </p>
                              )}
                          </section>
                        )}
                </section>
                <section className="col-md-6 col-sm-6 marginTop">
                  <article className="control-group">
                    <article
                      className={`grp1 ${
                        this.state.isMessageDisplay != null
                          ? "centerText"
                          : "normalText"
                        }`}
                    >
                      {userSubscribedDataList.length > 0 &&
                        !this.state.isMessageDisplay &&
                        !this.state.isAlreadyUnsubscribe ? (
                          <article>
                            <p>
                              You have chosen to unsubscribe from our newsletters
                              and alerts. We request you to please take a moment
                              to let us know your reasons for unsubscribing:
                          </p>
                            <form
                              name="unsubscribedForm"
                              onSubmit={this.clickRadioUnsubscribeReasonHandler}
                            >
                              {userSubscribedDataList.map(item => (
                                <UnsubscribeReasonFrom
                                  key={item.id}
                                  item={item}
                                  radioUnsubscribeReasonHandler={
                                    this.radioUnsubscribeReasonHandler
                                  }
                                  radioUnsubscribeReasonGroup={
                                    this.state.radioUnsubscribeReasonGroup
                                  }
                                  textAreaHandler={this.textAreaHandler}
                                />
                              ))}
                              {this.state.radioUnsubscribeReasonGroup["radio"] &&
                                this.state.radioUnsubscribeReasonGroup["radio"]
                                  .value === "others" ? (
                                  <textarea
                                    row="2"
                                    cols="5"
                                    name="othersReason"
                                    value={this.state.othersReason}
                                    onChange={this.textAreaHandler}
                                  />
                                ) : null}

                              <button
                                type="submit"
                                className={`btn-block greenBtn ${
                                  !this.state.isTrue ? "pointerEvents" : ""
                                  }`}
                              >
                                Submit
                            </button>
                            </form>
                          </article>
                        ) : this.state.isMessageDisplay &&
                          !this.state.isAlreadyUnsubscribe &&
                          !this.state.isSubscribeAgain ? (
                            <h5>YOU ARE NOW UNSUBSCRIBED </h5>
                          ) : !this.state.isMessageDisplay &&
                            this.state.isAlreadyUnsubscribe ? (
                              <h5>YOU ARE UNSUBSCRIBED</h5>
                            ) : this.state.isMessageDisplay &&
                              this.state.isSubscribeAgain ? (
                                <h5>YOU HAVE SUBSCRIBED SUCCESSFULLY</h5>
                              ) : null}
                    </article>
                  </article>
                </section>
              </section>
            </section>
          </section>
        </section>
      </article>
    );
  }
}

export default ThankYou;
