import React from "react";
const unsubscribeReason = props => {
  return (
    <article>
      <label className="control control--radio">
        {props.item.value}
        <input
          type="radio"
          name="radio"
          id={props.item.id}
          value={props.item.value}
          checked={
            props.radioUnsubscribeReasonGroup["radio"] &&
            props.radioUnsubscribeReasonGroup["radio"].value == props.item.value
          }
          onChange={event => props.radioUnsubscribeReasonHandler(event)}
        />
        <article className="control__indicator" />
      </label>
    </article>
  );
};

export default unsubscribeReason;
