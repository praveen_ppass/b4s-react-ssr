import {
  FETCH_UNSUBSCRIBED_REQUESTED,
  FETCH_UNSUBSCRIBED_SUCCEEDED,
  FETCH_UNSUBSCRIBED_FAILED,
  UPDATE_UNSUBSCRIBED_REQUESTED,
  UPDATE_UNSUBSCRIBED_SUCCEEDED,
  UPDATE_UNSUBSCRIBED_FAILED
} from "./actions";

const initialState = {
  showLoader: false,
  isError: false,
  errorMessage: "",
  userSubscribedDataList: []
};

const userSubscribedReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_UNSUBSCRIBED_REQUESTED:
      return {
        payload: null,
        type,
        showLoader: true,
        isError: false,
        errorMessage: "",
        userSubscribedDataList: []
      };
    case FETCH_UNSUBSCRIBED_SUCCEEDED:
      return {
        userSubscribedDataList: payload,
        type,
        showLoader: false,
        isError: false,
        errorMessage: ""
      };

    case FETCH_UNSUBSCRIBED_FAILED:
      return {
        payload: payload,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };
    case UPDATE_UNSUBSCRIBED_REQUESTED:
      return {
        payload: null,
        type,
        showLoader: true,
        isError: false,
        errorMessage: "",
        userSubscribedDataList: []
      };
    case UPDATE_UNSUBSCRIBED_SUCCEEDED:
      return {
        subscribedresponse: payload,
        type,
        showLoader: false,
        isError: false,
        errorMessage: ""
      };

    case UPDATE_UNSUBSCRIBED_FAILED:
      return {
        payload: payload,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };
    default:
      return state;
  }
};
export default userSubscribedReducer;
