import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_UNSUBSCRIBED_SUCCEEDED,
  FETCH_UNSUBSCRIBED_REQUESTED,
  FETCH_UNSUBSCRIBED_FAILED,
  UPDATE_UNSUBSCRIBED_SUCCEEDED,
  UPDATE_UNSUBSCRIBED_REQUESTED,
  UPDATE_UNSUBSCRIBED_FAILED
} from "./actions";

/**
 * GET USER UNSUBSCRIBED API CALL
 *
 */

function fetchUserUnsubscribedUrl(token) {
  const url = `${apiUrl.getUnsubscribed}/UnsubscriptionRequest?token=${token}`;
  return fetchClient.get(url).then(res => {
    return res.data;
  });
}

function* fetchUserUnsubscribed(input) {
  try {
    const userUnsubscribed = yield call(
      fetchUserUnsubscribedUrl,
      input.payload.token
    );

    yield put({
      type: FETCH_UNSUBSCRIBED_SUCCEEDED,
      payload: userUnsubscribed
    });
  } catch (error) {
    yield put({
      type: FETCH_UNSUBSCRIBED_FAILED,
      payload: error.errorMessage
    });
  }
}

function updateUserUnsubscribedUrl({ value }) {
  const url = `${apiUrl.getUnsubscribed}/UnsubscriptionRequest`;
  return fetchClient.post(url, value).then(res => {
    return res.data;
  });
}

function* updateUserUnsubscribed(input) {
  try {
    const userUnsubscribed = yield call(
      updateUserUnsubscribedUrl,
      input.payload
    );

    yield put({
      type: UPDATE_UNSUBSCRIBED_SUCCEEDED,
      payload: userUnsubscribed
    });
  } catch (error) {
    yield put({
      type: UPDATE_UNSUBSCRIBED_FAILED,
      payload: error.errorMessage
    });
  }
}

export default function* fetchUserUnsubscribedSaga() {
  yield takeEvery(FETCH_UNSUBSCRIBED_REQUESTED, fetchUserUnsubscribed);
  yield takeEvery(UPDATE_UNSUBSCRIBED_REQUESTED, updateUserUnsubscribed);
}
