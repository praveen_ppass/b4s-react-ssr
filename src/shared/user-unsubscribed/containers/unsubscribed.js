import React, { Component } from "react";
import Unsubscribed from "../components/unsubscribed";

import {
  fetchUserUnsubscribed as fetchUserUnsubscribedAction,
  updateUserUnsubscribed as postUserUnsubscribedAction
} from "../actions";

import { connect } from "react-redux";

const mapStateToProps = ({ userSubscribed, loginOrRegister }) => ({
  userSubscribedData: userSubscribed.userSubscribedDataList,
  showLoader: userSubscribed.showLoader,
  type: userSubscribed.type,
  isError: userSubscribed.isError,
  errorMessage: userSubscribed.errorMessage,
  userStatus: userSubscribed.subscribedresponse,
  isAuthenticated: loginOrRegister.isAuthenticated
});

const mapDispatchToProps = dispatch => ({
  loadFetchUserUnsubscribed: input =>
    dispatch(fetchUserUnsubscribedAction(input)),
  postUserUnsubscribed: input => dispatch(postUserUnsubscribedAction(input))
});

export default connect(mapStateToProps, mapDispatchToProps)(Unsubscribed);
