import { connect } from "react-redux";
import {
  submitContent as submitContentAction
} from "../actions";
import ContentPage from "../components/contentPage";

const mapStateToProps = ({ contentReducer }) => ({
  contentlist: contentReducer.contentlist,
  type: contentReducer.type
});

const mapDispatchToProps = dispatch => ({
  loadcontent: data => dispatch(submitContentAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(ContentPage);
