import {
  FETCH_CONTENT_SUCCEEDED,
  FETCH_CONTENT_REQUESTED,
  FETCH_CONTENT_FAILED
} from "./actions";

const initialState = {
  contentlist: null
};
const contentReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_CONTENT_REQUESTED:
      return { showLoader: true, type, contentlist: null };
    case FETCH_CONTENT_SUCCEEDED:
      return { showLoader: false, type, contentlist: payload };
    case FETCH_CONTENT_FAILED:
      return { showLoader: false, type, contentlist: null };
    default:
      return state;
  }
};
export default contentReducer;
