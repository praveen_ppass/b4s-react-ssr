export const FETCH_CONTENT_REQUESTED = "FETCH_CONTENT_REQUESTED";
export const FETCH_CONTENT_SUCCEEDED = "FETCH_CONTENT_SUCCEEDED";
export const FETCH_CONTENT_FAILED = "FETCH_CONTENT_FAILED";

export const submitContent = data => ({
  type: FETCH_CONTENT_REQUESTED,
  payload: { data: data }
});
