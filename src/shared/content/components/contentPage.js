import React, { Component } from "react";
import { FETCH_CONTENT_SUCCEEDED, FETCH_CONTENT_FAILED } from "../actions";
class ContentPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      listdetails: {}
    };
  }
  componentDidMount() {
    let slug = location.pathname.split('/content/');
    this.props.loadcontent(slug[1]);
  }
  componentWillReceiveProps(nextProps) {
    const { type, contentlist } = nextProps;
    switch (type) {
      case FETCH_CONTENT_SUCCEEDED:
        contentlist && this.setState({
          listdetails: contentlist
        })
        break;
      case FETCH_CONTENT_FAILED:
        this.setState({
          listdetails: {},
        })
        break;
    }
  }

  render() {
    const { listdetails } = this.state;
    return (
      <section className="container-fluid">
        <section className="container">
          <section className="row">
            <section className="col-md-12">
              <p
                dangerouslySetInnerHTML={{
                  __html: listdetails.title
                }}
              />
            </section>
            <section className="col-md-12 col-sm-12 careerdetails">
              {
                listdetails && <div dangerouslySetInnerHTML={{ __html: listdetails.body }} />
              }
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default ContentPage;
