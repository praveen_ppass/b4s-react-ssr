import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

/************   All actions required in this module starts *********** */
import {
  FETCH_CONTENT_SUCCEEDED,
  FETCH_CONTENT_REQUESTED,
  FETCH_CONTENT_FAILED,
} from "./actions";


/*** Fetch Content Start***/

const fetchContent = (input) =>
  fetchClient.get(`${apiUrl.contentData}/${input.data}`).then(res => {
    return res.data;
  });

function* content(input) {
  try {
    const data = yield call(fetchContent, input.payload);
    yield put({
      type: FETCH_CONTENT_SUCCEEDED,
      payload: data
    });
  } catch (error) {
    yield put({
      type: FETCH_CONTENT_FAILED,
      payload: error
    });
  }
}

export default function* ContentSaga() {
  yield takeEvery(FETCH_CONTENT_REQUESTED, content);
}
