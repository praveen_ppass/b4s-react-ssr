import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_TERMS_CONDITIONS_REQUESTED,
  FETCH_TERMS_CONDITIONS_SUCCEEDED,
  FETCH_TERMS_CONDITIONS_FAILED
} from "./actions";

const fetchUrl = () =>
  fetchClient
    .get(apiUrl.dynamic_page_data + "/terms-and-conditions")
    .then(res => {
      return res.data;
    });
function* fetchTermsConditions(input) {
  try {
    const terms_conditions = yield call(fetchUrl);

    yield put({
      type: FETCH_TERMS_CONDITIONS_SUCCEEDED,
      payload: terms_conditions
    });
  } catch (error) {
    yield put({
      type: FETCH_TERMS_CONDITIONS_FAILED,
      payload: error.errorMessage
    });
  }
}

export default function* fetchTermsConditionsSaga() {
  yield takeEvery(FETCH_TERMS_CONDITIONS_REQUESTED, fetchTermsConditions);
}
