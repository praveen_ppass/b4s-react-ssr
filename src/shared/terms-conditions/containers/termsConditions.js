import React from "react";
import { connect } from "react-redux";
import { fetchTermsConditions as fetchTermsConditionsAction } from "../actions";
import TermsConditions from "../components/termsConditionsPage";

const mapStateToProps = ({ termsConditions }) => ({
  termsConditionList: termsConditions.payload,
  showLoader: termsConditions.showLoader,
  isError: termsConditions.isError,
  errorMessage: termsConditions.errorMessage
});
const mapDispatchToProps = dispatch => ({
  loadTermsConditions: () => dispatch(fetchTermsConditionsAction("pageData"))
});

export default connect(mapStateToProps, mapDispatchToProps)(TermsConditions);
