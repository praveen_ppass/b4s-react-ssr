export const FETCH_TERMS_CONDITIONS_REQUESTED =
  "FETCH_TERMS_CONDITIONS_REQUESTED";
export const FETCH_TERMS_CONDITIONS_SUCCEEDED =
  "FETCH_TERMS_CONDITIONS_SUCCEEDED";
export const FETCH_TERMS_CONDITIONS_FAILED = "FETCH_TERMS_CONDITIONS_FAILED";

export const fetchTermsConditions = data => ({
  type: FETCH_TERMS_CONDITIONS_REQUESTED,
  payload: { data: data }
});

export const receivesTermsConditions = payload => ({
  type: FETCH_TERMS_CONDITIONS_SUCCEEDED,
  payload
});
