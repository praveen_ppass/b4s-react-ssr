import React, { Component } from "react";
import { breadCrumObj } from "../../../constants/breadCrum";
import { Helmet } from "react-helmet";
import BreadCrum from "../../components/bread-crum/breadCrum";
import Loader from "../../common/components/loader";
import ServerError from "../../common/components/serverError";
import { imgBaseUrl } from "../../../constants/constants";

class TermsConditions extends Component {
  componentDidMount() {
    this.props.loadTermsConditions();
  }

  rawMarkUp() {
    if (this.props.termsConditionList != null) {
      let rawMarkup = this.props.termsConditionList.body;
      return { __html: rawMarkup };
    }
  }

  render() {
    if (this.props.isError) {
      return (
        <section>
          <section className="gray">
            <section className="tnc">
              <BreadCrum
                classes={breadCrumObj["terms_conditions"]["bgImage"]}
                listOfBreadCrum={breadCrumObj["terms_conditions"]["breadCrum"]}
                title={breadCrumObj["terms_conditions"]["title"]}
              />
              <ServerError errorMessage={this.props.errorMessage} />
            </section>
          </section>
        </section>
      );
    }

    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <section className="gray">
          <section className="tnc">
            <Helmet> </Helmet>
            <BreadCrum
              classes={breadCrumObj["terms_conditions"]["bgImage"]}
              listOfBreadCrum={breadCrumObj["terms_conditions"]["breadCrum"]}
              title={breadCrumObj["terms_conditions"]["title"]}
            />
            <article className="tnc">
              <section className="conatiner-fluid graybg">
                <article className="container">
                  <article className="row">
                    <article className="col-md-4 col-xs-12 col-sm-4 right">
                      <h1>TERMS AND CONDITIONS</h1>
                      <img src={`${imgBaseUrl}arrowr.png`} alt="arrow" />
                    </article>
                    <article className="col-md-8 col-sm-8 col-xs-12 left">
                      <article
                        className="row"
                        dangerouslySetInnerHTML={this.rawMarkUp()}
                      />
                    </article>
                  </article>
                </article>
              </section>
            </article>
          </section>
        </section>
      </section>
    );
  }
}

export default TermsConditions;
