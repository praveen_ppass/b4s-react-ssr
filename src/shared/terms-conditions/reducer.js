import {
  FETCH_TERMS_CONDITIONS_SUCCEEDED,
  FETCH_TERMS_CONDITIONS_REQUESTED,
  FETCH_TERMS_CONDITIONS_FAILED
} from "./actions";

const initialState = {
  showLoader: false,
  isError: false,
  errorMessage: ""
};
const termsConditionReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_TERMS_CONDITIONS_SUCCEEDED:
      return {
        ...state,
        payload,
        showLoader: false,
        isError: false,
        errorMessage: ""
      };

    case FETCH_TERMS_CONDITIONS_REQUESTED:
      return {
        ...state,
        payload: null,
        showLoader: true,
        isError: false,
        errorMessage: ""
      };

    case FETCH_TERMS_CONDITIONS_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        errorMessage: payload
      };

    default:
      return state;
  }
};
export default termsConditionReducer;
