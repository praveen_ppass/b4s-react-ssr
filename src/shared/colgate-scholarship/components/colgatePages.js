import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";

import {
  defaultImageLogo,
  prodEnv,
  applicationFormsUrl,
  PRODUCTION_URL,
  STAGING_URL
} from "../../../constants/constants";
import Loader from "../../common/components/loader";
import { imgBaseUrl } from "../../../constants/constants";
import { imgBaseUrlDev } from "../../../constants/constants";

var timerId;
class Colgate extends Component {
  constructor() {
    super();
    this.state = {
      showItem: null,
      stickClass: "",
      scrollClass: false
    };

    this.frequestAskedQuesHandler = this.frequestAskedQuesHandler.bind(this);
    this.onScrollHandler = this.onScrollHandler.bind(this);
    this.onApplyHandler = this.onApplyHandler.bind(this);
    this.scroll = this.scroll.bind(this);
  }

  frequestAskedQuesHandler(index) {
    this.setState({
      showItem: index
    });
  }

  onApplyHandler(pathName) {
    if (window) {
      const location = pathName;

      window.open(location, "_blank");
    }
  }
  componentDidMount() {
    //  this.props.fetchPaymentPlan({}, "CB");
    window.addEventListener("scroll", this.scroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.scroll);
    clearTimeout(timerId);
  }

  scroll(e) {
    if (window.scrollY >= 300) {
      this.setState({
        stickClass: "stickeyfix"
      });
    }
    if (window.scrollY <= 300) {
      this.setState({
        stickClass: ""
      });
    }
  }

  onScrollHandler(element, flag) {
    this.setState({ scrollClass: flag });

    //if (!timerId) {
    timerId = setTimeout(() => {
      this.setState({ scrollClass: false });
    }, 20000);
    //}
    element.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }

  render() {
    return (
      <section>
        <section className="gray colgateTempBg">
          <section className="container-fluid bannerBh">
            <section className="container">
              <section className="row">
                <section className="col-md-3">
                  <Link to="/">
                    <img
                      src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/white-logo.png"
                      className="logo"
                      alt="buddy4study-logo"
                    />
                  </Link>
                </section>
                <section className="col-md-9">
                  <a href="" target="_blank" rel="noopener nofollow">
                    <img
                      src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/organisationLogo/2582/Colgate-Logo.png"
                      className="cbLogo"
                      alt="colgate"
                      rel="noopener nofollow"
                    />
                  </a>
                </section>
              </section>
              <section className="row">
                <section className="col-md-12">
                  <article className="centerAlign">
                    <h1>
                      Keep India Smiling Foundational <br />Scholarship
                      Programme
                    </h1>

                    <button
                      onClick={() =>
                        this.onScrollHandler(this.refs.scholarships)
                      }
                    >
                      Apply Now
                    </button>
                  </article>
                </section>
              </section>
            </section>
          </section>
          <section
            className={`hpStepsFollowedContent containerfluid lightGrayBg ${
              this.state.stickClass
              }`}
            ref="sticky"
          >
            <section className="container">
              <section className="row">
                <article className="content">
                  <ul>
                    <li
                      onClick={() => this.onScrollHandler(this.refs.about, 1)}
                    >
                      Home
                    </li>
                    <li
                      onClick={() =>
                        this.onScrollHandler(this.refs.scholarships, 2)
                      }
                    >
                      Apply for Scholarships
                    </li>

                    <li onClick={() => this.onScrollHandler(this.refs.faqs, 3)}>
                      FAQs
                    </li>
                    <li onClick={() => this.onScrollHandler(this.refs.tc, 4)}>
                      Terms & Conditions
                    </li>
                    <li
                      onClick={() => this.onScrollHandler(this.refs.contact, 5)}
                    >
                      Contact Us
                    </li>
                  </ul>
                </article>
              </section>
            </section>
          </section>
          <section
            className={`${
              this.state.scrollClass == 1 ? "scrollClass " : ""
              }container-fluid colgateabout`}
            ref="about"
          >
            <section className="container">
              <h3>About the Programme</h3>
              <section className="row">
                <section className="col-md-12 col-sm-12">
                  <p>
                    <b>Keep India Smiling Foundational Scholarship Programme</b>{" "}
                    aims to provide foundational support to individuals, who are
                    deserving & meritorious but may lack resources to pursue
                    their dreams. Along with the financial support, programme
                    will also focus on mentorship and career guidance to the
                    beneficiaries as and when required.
                  </p>
                </section>
              </section>
            </section>
          </section>

          <section
            className={`${
              this.state.scrollClass == 2 ? "scrollClass " : ""
              }container-fluid colgatescholarship`}
            ref="scholarships"
          >
            <section className="container">
              <h3>Keep India Smiling Foundational Scholarship Programme</h3>
              <section className="row">
                <section className="col-md-4 col-sm-4">
                  <article className="detailsbox">
                    <h4> Scholarship for Class 11 </h4>
                    <article className="amount">
                      <p>ELIGIBILITY </p>
                      <ul>
                        <li>
                          Must have passed Class 10 board examination in 2019
                        </li>
                        <li>
                          Must be enrolled in Class 11 at a recognised school in
                          India
                        </li>
                        <li>
                          Must have scored at least 75% in Class 10 board exams
                        </li>
                        <li>
                          Annual family income should be less than INR 5 Lakhs
                        </li>
                      </ul>

                      <p>Scholarship Amount </p>
                      <ul className="award">
                        <li>INR 20,000 per year for 2 years</li>
                      </ul>
                    </article>
                    <button
                      onClick={() =>
                        this.onApplyHandler(
                          "https://www.buddy4study.com/application/COS1/instruction"
                        )
                      }
                    >
                      {" "}
                      Apply Now{" "}
                    </button>
                  </article>
                </section>
                <section className="col-md-4 col-sm-4">
                  <article className="detailsbox">
                    <h4> Scholarship for Graduation/Diploma</h4>
                    <article className="amount">
                      <p>ELIGIBILITY </p>
                      <ul>
                        <li>
                          Must have passed Class 12 board examination in 2019{" "}
                        </li>
                        <li>
                          Must have scored a minimum 60% in Class 12 board exams{" "}
                        </li>
                        <li>
                          Must be enrolled in a 3-year graduation/diploma
                          programme in a recognised institution of India{" "}
                        </li>
                        <li>
                          Annual family income should be less than INR 5 Lakhs{" "}
                        </li>
                      </ul>

                      <p>Scholarship Amount </p>
                      <ul className="award">
                        <li>INR 30,000 per year for 3 years</li>
                      </ul>
                    </article>
                    <button
                      onClick={() =>
                        this.onApplyHandler(
                          "https://www.buddy4study.com/application/CCS1/instruction"
                        )
                      }
                    >
                      {" "}
                      Apply Now{" "}
                    </button>
                  </article>
                </section>
                <section className="col-md-4 col-sm-4">
                  <article className="detailsbox">
                    <h4> Scholarship for UG/Engineering </h4>
                    <article className="amount">
                      <p>ELIGIBILITY </p>
                      <ul>
                        <li>
                          Must have passed Class 12 board examination in 2019
                        </li>
                        <li>
                          Must have scored a minimum 60% in Class 12 board exams
                        </li>
                        <li>
                          Must be enrolled in any recognised undergraduate
                          engineering programme in India
                        </li>
                        <li>
                          Annual family income should be less than INR 5 Lakhs{" "}
                        </li>
                      </ul>

                      <p>Scholarship Amount </p>
                      <ul className="award">
                        <li>INR 30,000 per year for 4 years</li>
                      </ul>
                    </article>
                    <button
                      onClick={() =>
                        this.onApplyHandler(
                          "https://www.buddy4study.com/application/CES3/instruction"
                        )
                      }
                    >
                      {" "}
                      Apply Now{" "}
                    </button>
                  </article>
                </section>
                <section className="col-md-4 col-sm-4">
                  <article className="detailsbox">
                    <h4> Scholarship for Vocational Courses </h4>
                    <article className="amount">
                      <p>ELIGIBILITY </p>
                      <ul>
                        <li>
                          Must have passed Class 12 board examination in 2019
                        </li>
                        <li>
                          Must have scored a minimum 60% in Class 12 board exams
                        </li>
                        <li>
                          Must be enrolled in any vocational course at any
                          recognised institution in India
                        </li>
                        <li>
                          Annual family income should be less than INR 5 Lakhs{" "}
                        </li>
                      </ul>

                      <p>Scholarship Amount </p>
                      <ul className="award">
                        <li>INR 20,000 for 1 year</li>
                      </ul>
                    </article>
                    <button
                      onClick={() =>
                        this.onApplyHandler(
                          "https://www.buddy4study.com/application/CVS1/instruction"
                        )
                      }
                    >
                      {" "}
                      Apply Now{" "}
                    </button>
                  </article>
                </section>

                <section className="col-md-4 col-sm-4">
                  <article className="detailsbox">
                    <h4> Grant for Individuals Helping Others</h4>
                    <article className="amount">
                      <p>ELIGIBILITY </p>
                      <ul>
                        <li>
                          Must be involved in activities like teaching a group
                          of underprivileged children or providing sports
                          training to them
                        </li>
                        <li>
                          Must be from the economically weaker or
                          moderate-income group of the society
                        </li>
                      </ul>

                      <p>Scholarship Amount </p>
                      <ul className="award">
                        <li>INR 75,000 per year for 2 years</li>
                      </ul>
                    </article>
                    <button
                      onClick={() =>
                        this.onApplyHandler(
                          "https://www.buddy4study.com/application/CHO1/instruction"
                        )
                      }
                    >
                      {" "}
                      Apply Now{" "}
                    </button>
                  </article>
                </section>

                <section className="col-md-4 col-sm-4">
                  <article className="detailsbox">
                    <h4> Scholarship for Sportspersons </h4>
                    <article className="amount">
                      <p>ELIGIBILITY </p>
                      <ul>
                        <li>
                          Must have represented the country/state/district at
                          national/state or district level
                        </li>
                        <li>
                          Must be ranked within 500 in the national ranking/
                          within 100 in the state ranking/ within 10 in the
                          district ranking
                        </li>
                        <li>
                          Annual family income should be less than INR 5 Lakhs
                        </li>
                      </ul>

                      <p>Scholarship Amount </p>
                      <ul className="award">
                        <li>INR 75,000 per year for 3 years</li>
                      </ul>
                    </article>
                    <button
                      onClick={() =>
                        this.onApplyHandler(
                          "https://www.buddy4study.com/application/CSP1/instruction"
                        )
                      }
                    >
                      {" "}
                      Apply Now{" "}
                    </button>
                  </article>
                </section>
              </section>
            </section>
          </section>

          <section
            className={`${
              this.state.scrollClass == 3 ? "scrollClass " : ""
              }colgateFAQsRow container-fluid`}
            ref="faqs"
          >
            <section className="container">
              <section className="row">
                <article className="col-md-12">
                  <h3>Frequently Asked Questions</h3>
                  <article className="content">
                    <FrequestAskedQuestion
                      frequestAskedQuesHandler={this.frequestAskedQuesHandler}
                      showItem={this.state.showItem}
                    />
                  </article>
                </article>
              </section>
            </section>
          </section>
          <section
            className={`${
              this.state.scrollClass == 4 ? "scrollClass " : ""
              }container-fluid colgatetc`}
            ref="tc"
          >
            <section className="container">
              <h3>Terms & Conditions</h3>
              <section className="row">
                <section className="col-md-12 col-sm-12">
                  <p>
                    Please read the below mentioned terms and conditions
                    carefully before submitting the application form : -
                  </p>
                  <ol>
                    <li>
                      ‘Keep India Smiling Foundational Scholarship Programme’
                      (Hereinafter ‘the Scholarship Program’) is being conducted
                      by Colgate-Palmolive (India) Ltd. ("Colgate" / "Company")
                      from June 6,2019 to March 31, 2020 ("Scholarship offer
                      period") and is open for India Nationals Only.{" "}
                    </li>
                    <li>
                      {" "}
                      The Scholarship Program is applicable to
                      individuals/candidates from economically weaker sections/
                      below poverty lines. The documents for same will be as
                      prescribed by statutory norms and is open to those
                      desirous candidates/ individuals who have all the
                      requisite documents at the time of making an application.
                      The specific details of the categories, eligibility
                      criteria and entitlements of the Scholarship Program have
                      been provided herein below.
                    </li>
                    <li>
                      {" "}
                      It is the sole responsibility of a candidate to go through
                      the categories and the corresponding eligibility criteria
                      as mentioned below and apply for the applicable category.
                    </li>
                    <li>
                      The data collected through the registration process will
                      not be used for any commercial purposes other than
                      shortlisting candidates for the Scholarship Program.{" "}
                    </li>
                    <li>
                      Meeting the eligibility criteria and applying for the
                      Scholarship Program does not guarantee selection. The
                      process entails various steps for shortlisting and final
                      selection, which may take few months.
                    </li>
                    <li>
                      Colgate shall not entertain any correspondence with
                      regards to the non-selection of the candidates.
                    </li>
                    <li>
                      Upon successful selection for the Scholarship Program,
                      candidate/ individual will be approached by
                      Colgate/technology implementation partner.{" "}
                    </li>
                    <li>
                      Relevant supporting documents will be required before / on
                      or after allocation of the applicable scholarship amount.
                      The Scholarship Program for specific category will be
                      strictly for the duration mentioned for respective
                      category.{" "}
                    </li>
                    <li>
                      {" "}
                      Colgate holds the right to perform internal due diligence
                      for verifying the documents uploaded by the candidate.
                    </li>
                    <li>
                      Colgate reserves the right, in its sole discretion, to the
                      fullest extent permitted by law: (a) to reject or
                      disqualify any application; or (b) subject to any written
                      directions from a regulatory authority, to modify,
                      suspend, terminate or cancel the scholarship, or (c) to
                      change the program based learning ; as appropriate.{" "}
                    </li>
                    <li>
                      If at any point of time during the period of scholarship
                      or even later it is found that any of the particulars
                      mentioned in the form are/were false and/or misleading or
                      that the candidate has wilfully suppressed any material
                      facts, Colgate will be at liberty to withdraw the
                      scholarship allotted to such candidate.{" "}
                    </li>
                    <li>
                      {" "}
                      The Scholarship Program shall be evaluated at regular
                      intervals by Colgate and terms and conditions thereof may
                      be modified, withdrawn or cancelled at the discretion of
                      the selection panel of Keep India Smiling Foundational
                      Scholarship Programme and CSR committee of Colgate.
                    </li>
                    <li>
                      The candidate hereby acknowledges that Colgate does not
                      commit or intend to employ any of the successful
                      candidates.{" "}
                    </li>
                    <li>
                      {" "}
                      No candidate or Individual has to pay any registration or
                      application fee for applying to the Scholarship Program.
                      No purchase of Colgate products is mandatory for
                      participating in the Scholarship Program.
                    </li>
                    <li>
                      {" "}
                      Colgate reserves the right, to use the photograph /profile
                      of the scholarship applicant/beneficiary for future
                      publication use; and/or disclosures; and/or for
                      communication purposes with Colgate’s stakeholders; and/or
                      for submissions before various authorities and interested
                      parties from time to time for reporting progress thereof;
                      and/or for use in Corporate brochures, websites, social
                      media, archives and other material publicity, if any in
                      relation to its activities without any monetary
                      compensation payable to any applicant and without any
                      restriction to its frequency and use.
                    </li>
                    <li>
                      Colgate would not be liable or held responsible for any
                      lack or lapse in any communication on account of failure
                      or delay by internet, telecom, SMS and/or E-mail service
                      providers. No correspondence in this regard will be
                      entertained.{" "}
                    </li>
                    <li>
                      Employees of the Company, its affiliates and their family
                      members are not eligible to participate in the Scholarship
                      Program.{" "}
                    </li>
                    <li>
                      {" "}
                      The decision of the Company with regard to the Scholarship
                      Program and the declaration of the winners is final and
                      binding.
                    </li>
                    <li>
                      {" "}
                      Disputes; if any, shall be subject to exclusive
                      jurisdiction of the courts at Mumbai only.
                    </li>
                    <li>
                      The candidates are required to upload the following
                      documents:
                      <br /> <br />
                      <ol type="I">
                        <li> Passport sized photograph</li>
                        <li>
                          {" "}
                          Valid ID proof – Either of Aadhaar Card/Driving
                          License/Voter Id Card/Pan Card{" "}
                        </li>
                        <li>
                          {" "}
                          Income Proof – Income certificate/BPL certificate/Food
                          security certificate/Any other certificate of income
                          issued by competent government authority.
                        </li>
                        <li> 10th Class mark sheet</li>
                        <li> Latest mark sheet and education certificate</li>
                        <li>
                          {" "}
                          Fee Receipt/Admission Letter/College ID card/Bonafide
                          certificate (in case admission for professional course
                          already taken)
                        </li>
                        <li>
                          {" "}
                          Entrance examination result (In case seeking to take
                          admission)
                        </li>
                        <li>
                          {" "}
                          Disability certificate, in case of any physical
                          disability
                        </li>
                        <li>
                          {" "}
                          Scanned copy of highest performance achieved (If
                          applying for Sports Scholarship)
                        </li>
                        <li>
                          {" "}
                          Name & Registration no. of NGO/Rotary/Association/ Not
                          for profit (If applying for general financial support
                          category)
                        </li>
                      </ol>
                    </li>
                  </ol>
                </section>
              </section>
            </section>
          </section>
          <section
            className={`${
              this.state.scrollClass == 5 ? "scrollClass " : ""
              }container-fluid colgateSupport`}
            ref="contact"
          >
            <section className="container">
              <section className="row">
                <article className="col-md-12">
                  <h3>Support</h3>

                  <p>In case of queries, please reach out to us:</p>
                  <ul className="supportList">
                    <li>
                      <dd>011-430-92248 (Ext- 125)</dd>
                      <span>(Monday to Friday &ndash; 10:00AM to 6PM)</span>
                    </li>
                    <li>
                      <a href="mailto:keepindiasmiling@buddy4study.com">
                        keepindiasmiling@buddy4study.com
                      </a>
                    </li>
                  </ul>
                </article>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

const FrequestAskedQuestion = props => {
  const frequenstAskedQuestion = [
    {
      title:
        "What are the different scholarships offered under Keep India Smiling Foundational Scholarship Programme?",

      desc:
        "The scholarships offered under Keep India Smiling Foundational Scholarship Programme are as follows:<ul><li>Keep India Smiling Foundational Scholarship Programme for Education </li><li>Keep India Smiling Foundational Scholarship Programme for Sportspersons</li><li>Keep India Smiling Foundational Grant for Individuals Helping Others</li></ul>"
    },
    {
      title:
        "What is Keep India Smiling Foundational Scholarship Programme for Education?",

      desc:
        "Keep India Smiling Foundational Scholarship Programme for Education is a scholarship scheme initiated by Colgate-Palmolive India Ltd with the aim to financially support students of economically-weaker sections of society. Under this umbrella scholarship initiative, students belonging to four different categories will be benefitted. The four categories are as follows:<ul><li>Class 10 passed students for their Higher Secondary Education (10+2)</li><li>Class 12 passed students for their 3-year graduation/diploma programmes</li><li>Class 12 passed students for their 4-year engineering programmes</li><li>Class 12 passed students for their vocational courses</li></ul>"
    },
    {
      title:
        "Who can apply for Keep India Smiling Foundational Scholarship Programme for Sportspersons?",

      desc:
        "Young athletes not older than 28 years are eligible for the sports scholarship. The applicants must have played at national, state or district level."
    },
    {
      title:
        "Who are eligible for Keep India Smiling Foundational Grant for Individuals Helping Others?",

      desc:
        "Those individuals who are involved in activities like teaching a group of underprivileged children or providing sports training to them can apply for this grant. The applicants must also belong to economically-weaker section with annual family income should be less than INR 5 Lakhs."
    },
    {
      title:
        "I am a class 11 student. Am I eligible for Keep India Smiling Foundational Programme for Education?",

      desc:
        "Only those students who has passed class 10 in the year 2019 with a score of 75% are eligible to apply."
    },
    {
      title:
        "I am a 3<sup>rd</sup> year student of Electronics & Communication Engineering degree programme. Can I apply for this scholarship programme?",

      desc:
        "Only class 12 passed students who have completed their Senior Secondary education in the year 2019 and are enrolled for UG Engineering course can apply for this scholarship scheme."
    },
    {
      title: "How can I apply for this scholarship?",

      desc:
        "The application form for this scholarship is hosted on Buddy4Study website only."
    },
    {
      title:
        "What is the family income criteria to be eligible for Keep India Smiling Foundational scholarship programme?",

      desc:
        "The applicant’s annual family income should be less than INR 5 Lakhs to be eligible."
    }
  ];

  return (
    <ul>
      {frequenstAskedQuestion.map((list, index) => (
        <li
          key={index}
          onClick={() => props.frequestAskedQuesHandler(index)}
          className={props.showItem == index ? "active" : ""}
        >
          <h5
            dangerouslySetInnerHTML={{
              __html: list.title
            }}
          />
          <p
            dangerouslySetInnerHTML={{
              __html: list.desc
            }}
          />
        </li>
      ))}
    </ul>
  );
};

export default Colgate;
