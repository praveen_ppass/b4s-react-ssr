import React, { Component } from "react";
import { connect } from "react-redux";
import {
  fetchMediaPartner as fetchMediaPartnerAction,
  fetchMediaZone as fetchMediaZoneAction
} from "../actions";
import mediaPartners from "../components/mediaPartnersPages";

const mapStateToProps = ({ mediaPartner }) => ({
  mediaPartner: mediaPartner.mediaPartner,
  isError: mediaPartner.isError,
  errorMessage: mediaPartner.errorMessage,
  showLoader: mediaPartner.showLoader,
  type: mediaPartner.type,
  zones: mediaPartner.mediaZone
});
const mapDispatchToProps = dispatch => ({
  fetchMedia: mediaData => dispatch(fetchMediaPartnerAction(mediaData)),
  fetchZone: zoneData => dispatch(fetchMediaZoneAction(zoneData))
});

export default connect(mapStateToProps, mapDispatchToProps)(mediaPartners);
