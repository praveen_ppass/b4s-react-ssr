import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_MEDIA_PARTNER_SUCCEEDED,
  FETCH_MEDIA_PARTNER_REQUESTED,
  FETCH_MEDIA_PARTNER_FAILED,
  FETCH_MEDIA_ZONE_SUCCEEDED,
  FETCH_MEDIA_ZONE_REQUESTED,
  FETCH_MEDIA_ZONE_FAILED
} from "./actions";

const fetchUrl = ({ data }) =>
  fetchClient.get(apiUrl.mediaPartner + "/" + data).then(res => {
    return res.data;
  });

function* fetchMediaPartner(input) {
  try {
    const media_partner = yield call(fetchUrl, input.payload);

    yield put({
      type: FETCH_MEDIA_PARTNER_SUCCEEDED,
      payload: media_partner
    });
  } catch (error) {
    yield put({
      type: FETCH_MEDIA_PARTNER_FAILED,
      payload: error.errorMessage
    });
  }
}

const fetchUrlForZone = () =>
  fetchClient.get(apiUrl.mediaZone).then(res => {
    return res.data;
  });

function* fetchMediaZone(input) {
  try {
    const media_zone = yield call(fetchUrlForZone, input.payload);

    yield put({
      type: FETCH_MEDIA_ZONE_SUCCEEDED,
      payload: media_zone
    });
  } catch (error) {
    yield put({
      type: FETCH_MEDIA_ZONE_FAILED,
      payload: error.errorMessage
    });
  }
}

export default function* fetchMediaPartnerSaga() {
  yield takeEvery(FETCH_MEDIA_PARTNER_REQUESTED, fetchMediaPartner);
  yield takeEvery(FETCH_MEDIA_ZONE_REQUESTED, fetchMediaZone);
}
