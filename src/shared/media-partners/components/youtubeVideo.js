import React from "react";
import YouTube from "react-youtube";

class YoutubeVideo extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const opts = {
      height: "auto",
      width: "100%",
      playerVars: {
        autoplay: 0
      }
    };

    return (
      <YouTube
        videoId={this.props.videoId}
        opts={opts}
        target="_blank"
        onReady={this._onReady}
      />
    );
  }

  _onReady(event) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  }
}

export default YoutubeVideo;
