import React, { Component } from "react";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import { Helmet } from "react-helmet";
import ServerError from "../../common/components/serverError";

import {
  defaultImageLogo,
  prodEnv,
  applicationFormsUrl,
  imgBaseUrl,
  imgBaseUrlDev,
  MediaClass
} from "../../../constants/constants";
import OverviewYoutubeVideo from "./youtubeVideo";
import InterestForm from "../../interest-form/containers/interestForm";
import Loader from "../../common/components/loader";
import {
  FETCH_MEDIA_PARTNER_SUCCEEDED,
  FETCH_MEDIA_PARTNER_FAILED,
  FETCH_MEDIA_ZONE_SUCCEEDED,
  FETCH_MEDIA_ZONE_FAILED
} from "../actions";
import AlertMessage from "../../common/components/alertMsg";
class MediaPartner extends Component {
  constructor() {
    super();
    this.state = {
      mediaActive: "",
      showAlertMsg: "Server Error",
      isShowAlert: false,
      mediaContent: "",
      allZones: [],
      activeZone: ""
    };
    this.mediaToggle = this.mediaToggle.bind(this);
    this.zoneSelector = this.zoneSelector.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.clickCounter = 0;
  }

  mediaToggle(activeSection) {
    let copy = activeSection;
    if (copy === this.state.mediaActive) {
      this.clickCounter++;
    }
    if (this.clickCounter === 1) {
      this.setState({ mediaActive: "" });
      this.clickCounter = 0;
    } else {
      this.setState({
        mediaActive: activeSection
      });
    }
  }
  zoneSelector(zone) {
    this.setState({ activeZone: zone.name });
    this.props.fetchMedia(zone.slug);
  }
  hideAlert() {
    this.setState({
      showAlertMsg: "",
      isShowAlert: false
    });
  }

  componentDidMount() {
    this.props.fetchZone();
  }
  componentWillReceiveProps(nextProps) {
    const { mediaPartner, type, zones } = nextProps;

    switch (type) {
      case FETCH_MEDIA_PARTNER_SUCCEEDED:
        this.setState({ mediaContent: mediaPartner });
        break;
      case FETCH_MEDIA_PARTNER_FAILED:
        this.setState({ showAlertMsg: "Server Error", isShowAlert: true });
        break;
      case FETCH_MEDIA_ZONE_SUCCEEDED:
        this.setState({ allZones: zones, activeZone: zones[0]["name"] });
        this.props.fetchMedia(zones[0]["slug"]);
        break;
      case FETCH_MEDIA_ZONE_FAILED:
        this.setState({ showAlertMsg: "Server Error", isShowAlert: true });
        break;
      default:
        break;
    }
  }

  render() {
    const {
      mediaActive,
      activeZone,
      mediaContent,
      allZones,
      isShowAlert,
      showAlertMsg
    } = this.state;
    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          msg={showAlertMsg}
          status={false}
          isShow={isShowAlert}
          close={this.hideAlert}
        />;
        <section className="offersWrapper">
          <section className="container-fluid topBannerBg">
            <img
              src={`${imgBaseUrl}media-top-banner.jpg`}
              alt="Media Partners"
            />
            <article className="bannerContent">
              <article className="innerContent">
                <article className="innerContentBox">
                  <h1>Media Partners</h1>
                  <p>
                    Get curated scholarship content for your print and online
                    publications
                  </p>
                </article>
              </article>
              <InterestForm product={"media"} />
            </article>
          </section>
          <section className="offersContent lightGrayBg">
            <section className="container">
              <section className="row">
                <article className="col-md-7">
                  <article className="content">
                    <h2>Overview</h2>
                    <article className="row">
                      <p>
                        The best way to continuously engage readers is to
                        produce great content. With the growing digital
                        penetration, a large segment of your
                        readership/listenership comes from students in the age
                        groups of 16-24 years. Does your publication publish
                        content that is of interest to them? How do you fulfill
                        their demand for content that can help them better shape
                        their careers? This is the reason why admission and job
                        alerts are one of the most popular sections and
                        publishers have come up with dedicated supplements on
                        these segments.
                      </p>
                      <p>
                        At Buddy4Study, we regularly publish current, relevant
                        and curated content related to scholarships. This
                        content is appropriate for the entire student community
                        of India. We are partners with more than 100 media
                        houses at regional, national & International levels.
                        This list includes leading dailies, online digests,
                        magazines, special publications, TV channels and
                        Community Radio Stations.
                      </p>
                    </article>
                  </article>
                </article>
                <article className="col-md-5">
                  {/* <img
                    src={`${imgBaseUrlDev}overview-img.jpg`}
                    alt="Over view"
                  /> */}
                  <OverviewYoutubeVideo videoId="LkBpJ0nEcyM" />
                </article>
              </section>
            </section>
          </section>
          <section className="offersContent grayBg">
            <section className="container">
              <section className="row">
                <article className="col-md-12">
                  <article className="row">
                    <article className="content">
                      <h3>What do we offer?</h3>
                      <article className="row">
                        <p>
                          As a part of our media alliance, we furnish daily,
                          weekly, fortnightly, monthly ready-to-publish content
                          for our partners. This content is in the form of
                          latest scholarship alerts, articles related to new
                          scholarships/Fellowships and information pieces.
                        </p>
                        <p>
                          <i>Curated content</i>
                          Curated content related to scholarships, Fellowships,
                          Awards, Contest &amp; Olympiads.
                        </p>
                        <p>
                          <i>Timely content</i>
                          All the content is provided to our media partners well
                          in advance for publication.
                        </p>
                        <p>
                          <i>Official Recognition on our portal</i>
                          The media forums will be recognized in our media
                          partners list on our website and social media
                        </p>
                        <p>
                          <i>Authentic content</i>
                          Being India's largest scholarship platform, we know of
                          genuine and fraudulent scholarships information. We
                          make sure only authentic scholarship information is
                          shared with our media partners.
                        </p>
                        <p>
                          <i>Co&ndash;branding on our links</i>
                          The URL will carry the media forums's short form, eg.
                          CD, giving you equal credit for being a partner.
                        </p>
                        <p>
                          <i>Targeted and Exclusive Scholarship Information</i>
                          Whenever available, we keep our partners informed of
                          scholarships that are exclusively meant for the
                          students of a specific region. This benefits
                          newspapers, additionally.
                        </p>
                      </article>
                    </article>
                  </article>
                </article>
              </section>
            </section>
          </section>
          <article className="container-fluid whatDoYouGet bgSize">
            <article className="container">
              <article className="row">
                <h3>What do you get?</h3>
                <article className="media">
                  <article className="col-md-3">
                    <p>Ready-to-publish content in a language of your choice</p>
                  </article>
                  <article className="col-md-3">
                    <p>
                      Curated content for the circle of your
                      publication/broadcast/telecast
                    </p>
                  </article>
                  <article className="col-md-3">
                    <p>
                      Timely scholarships alerts that are of interest to
                      students across India
                    </p>
                  </article>
                  <article className="col-md-3">
                    <p>Verified information about latest scholarships</p>
                  </article>
                </article>
                <article className="media">
                  <article className="col-md-3">
                    <p>
                      Articles about new government/non-government scholarship
                      schemes
                    </p>
                  </article>
                  <article className="col-md-3">
                    <p>
                      Daily/weekly/fortnightly/monthly content as per your
                      publication sections
                    </p>
                  </article>
                  <article className="col-md-3">
                    <p>
                      Ready-to-use content in the format of your choice,
                      depending on the space you can devote
                    </p>
                  </article>
                </article>
              </article>
            </article>
          </article>
          <article className="container-fluid ourClients whiteBg">
            <article className="container">
              <article className="row">
                <h3>Our Media Partners</h3>
                <article className="content mediaPartners">
                  <article className="col-md-2">
                    <ul>
                      {allZones.map(zone => (
                        <li
                          key={zone.id}
                          className={activeZone === zone.name ? "active" : ""}
                          onClick={() => this.zoneSelector(zone)}
                        >
                          {zone.name}
                        </li>
                      ))}
                    </ul>
                  </article>
                  {mediaContent.zoneName && mediaContent.mediaTypes.length ? (
                    <div>
                      <article className="col-md-10">
                        <ul>
                          {mediaContent.zoneName &&
                            mediaContent.mediaTypes.map((mtype, index) => (
                              <li key={index}>
                                <h4
                                  className={`${
                                    MediaClass[mtype.mediaTypeId]
                                    } ${
                                    mediaActive === mtype.mediaTypeId
                                      ? "active"
                                      : ""
                                    }`}
                                  onClick={() =>
                                    this.mediaToggle(mtype.mediaTypeId)
                                  }
                                >
                                  <article className="titleBox">
                                    <span />
                                    <i />
                                    <span className="titleText">
                                      {mtype.mediaType}
                                    </span>
                                  </article>
                                </h4>
                                {mediaActive === mtype.mediaTypeId ? (
                                  <article className="logoWrapper">
                                    {mtype.mediaPartners.map(mpartner => (
                                      <article
                                        className="col-md-2"
                                        key={mpartner.id}
                                      >
                                        <article className="contentInn">
                                          <a
                                            href={mpartner.websiteUrl}
                                            target="_blank"
                                            rel="noopener nofollow"
                                          >
                                            <img
                                              src={mpartner.logo}
                                              alt={mpartner.name ? mpartner.name : "media"}
                                            />
                                          </a>
                                        </article>
                                      </article>
                                    ))}
                                  </article>
                                ) : (
                                    ""
                                  )}
                              </li>
                            ))}
                        </ul>
                      </article>
                    </div>
                  ) : (
                      <div>
                        {!this.props.showLoader && (
                          <h5 className="text-center">No Media Found</h5>
                        )}
                      </div>
                    )}
                </article>
              </article>
            </article>
          </article>
        </section>
      </section>
    );
  }
}

export default MediaPartner;
