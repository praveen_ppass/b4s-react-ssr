export const FETCH_MEDIA_PARTNER_SUCCEEDED = "FETCH_MEDIA_PARTNER_SUCCEEDED";
export const FETCH_MEDIA_PARTNER_REQUESTED = "FETCH_MEDIA_PARTNER_REQUESTED";
export const FETCH_MEDIA_PARTNER_FAILED = "FETCH_MEDIA_PARTNER_FAILED";

export const FETCH_MEDIA_ZONE_SUCCEEDED = "FETCH_MEDIA_ZONE_SUCCEEDED";
export const FETCH_MEDIA_ZONE_REQUESTED = "FETCH_MEDIA_ZONE_REQUESTED";
export const FETCH_MEDIA_ZONE_FAILED = "FETCH_MEDIA_ZONE_FAILED";

export const fetchMediaPartner = data => ({
  type: FETCH_MEDIA_PARTNER_REQUESTED,
  payload: { data: data }
});

export const fetchMediaZone = data => ({
  type: FETCH_MEDIA_ZONE_REQUESTED,
  payload: { data: data }
});

export const receivesMediaPartner = payload => ({
  type: FETCH_MEDIA_PARTNER_SUCCEEDED,
  payload
});
