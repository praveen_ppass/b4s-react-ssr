import {
  FETCH_MEDIA_PARTNER_SUCCEEDED,
  FETCH_MEDIA_PARTNER_REQUESTED,
  FETCH_MEDIA_PARTNER_FAILED,
  FETCH_MEDIA_ZONE_SUCCEEDED,
  FETCH_MEDIA_ZONE_REQUESTED,
  FETCH_MEDIA_ZONE_FAILED
} from "./actions";

const initialState = {
  isError: false,
  showLoader: false,
  errorMessage: "",
  mediaPartner: ""
};

const mediaPartnerReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_MEDIA_PARTNER_REQUESTED:
      return {
        ...state,
        mediaPartner: payload,
        showLoader: true,
        type: type,
        isError: false,
        errorMessage: ""
      };

    case FETCH_MEDIA_PARTNER_SUCCEEDED:
      return {
        ...state,
        mediaPartner: payload,
        type: type,
        showLoader: false,
        isError: false,
        errorMessage: ""
      };

    case FETCH_MEDIA_PARTNER_FAILED:
      return {
        mediaPartner: payload,
        type: type,
        showLoader: false,
        type: type,
        isError: true,
        errorMessage: payload
      };
    case FETCH_MEDIA_ZONE_REQUESTED:
      return {
        ...state,
        mediaZone: payload,
        showLoader: true,
        type: type,
        isError: false,
        errorMessage: ""
      };

    case FETCH_MEDIA_ZONE_SUCCEEDED:
      return {
        ...state,
        mediaZone: payload,
        type: type,
        showLoader: false,
        isError: false,
        errorMessage: ""
      };

    case FETCH_MEDIA_ZONE_FAILED:
      return {
        mediaZone: payload,
        type: type,
        showLoader: false,
        isError: true,
        errorMessage: payload
      };

    default:
      return state;
  }
};
export default mediaPartnerReducer;
