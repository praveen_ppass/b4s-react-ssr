import React from "react";
import { connect } from "react-redux";
import { fetchAboutConditions as fetchAboutAction } from "../actions";
import Careers from "../components/careersPage";

const mapStateToProps = state => ({
  aboutList: state
});
const mapDispatchToProps = dispatch => ({
  loadAboutConditions: () => dispatch(fetchAboutAction("pageData"))
});

export default connect(mapStateToProps, mapDispatchToProps)(Careers);
