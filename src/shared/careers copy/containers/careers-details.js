import React from "react";
import { connect } from "react-redux";
import { fetchCareerDetails as fetchCareerDetailsAction } from "../actions";
import careersDetails from "../components/careers-detailsPage";

const mapStateToProps = ({ careers }) => ({
  careerDetails: careers.careerDetails,
  isError: careers.isError,
  errorMessage: careers.errorMessage
});
const mapDispatchToProps = dispatch => ({
  loadCareerDetails: id => dispatch(fetchCareerDetailsAction(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(careersDetails);
