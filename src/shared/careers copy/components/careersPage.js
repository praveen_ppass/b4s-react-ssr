import React, { Component } from "react";
import { Link } from "react-router-dom";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import { imgBaseUrl } from "../../../constants/constants";

class Careers extends Component {
  render() {
    return (
      <section className="careers">
        {/* <CareersHeaders /> */}
        {/* breacrum trail */}

        <BreadCrum
          classes={breadCrumObj["careers"]["bgImage"]}
          listOfBreadCrum={breadCrumObj["careers"]["breadCrum"]}
          title={breadCrumObj["careers"]["title"]}
          subTitle={breadCrumObj["careers"]["subTitle"]}
        />

        {/* Careers section */}
        <section className="section1">
          <h1>Life at Buddy4Study</h1>
          <article className="container">
            <article className="row format">
              <article className="col-md-7 col-sm-7 col-xs-12">
                <p>
                  Talent forms the bedrock of Buddy4Study – from our core
                  mission to find and encourage hidden talent among India’s
                  youth, to the amazing talent we are blessed to have at our
                  workplace. Our typical ‘buddy’ is curious, driven,
                  compassionate, tenacious, autonomous, friendly, independent,
                  collaborative, communicative, supportive, self-motivated, and
                  can cook a mean chicken biryani. Well, may be the last one is
                  optional. Our employees understand the value of teamwork and
                  work together towards achieving the core vision of Buddy4Study
                  – to make education and scholarship opportunities accessible
                  to all Indian students.
                </p>
              </article>
              <article className="col-md-5 col-sm-5 col-xs-12">
                <img
                  src={`${imgBaseUrl}b4s-group.jpg`}
                  className="img-responsive width100"
                  alt="manasvi singh"
                />
              </article>
            </article>
            <article className="b4sgroup">
              <img
                src={`${imgBaseUrl}b4s-group1.jpg`}
                className="img-responsive"
                alt="buddy4study team"
              />
            </article>
            <article className="b4sgroup">
              <img
                src={`${imgBaseUrl}b4s-group3.jpg`}
                className="img-responsive"
                alt="buddy4study"
              />
            </article>
          </article>
        </section>

        <section className="section2">
          <hr />

          <article className="container">
            <article className="row">
              <article className="col-md-12 col-sm-12 col-xs-12">
                <h4>Why work with us?</h4>
                <h5>CAREER OPPORTUNITIES</h5>
                <p className="topPara">
                  Our work requires different functions and capabilities,
                  allowing you to explore careers in various fields. Discover
                  opportunities in technology, finance, administration, sales,
                  marketing, business development, creative, human resources and
                  more.
                </p>
              </article>
            </article>
          </article>
        </section>
        <article className="container">
          <article className="row how-it-work text-center">
            <article className="col-md-3 col-sm-6 col-xs-12">
              <Link to="/career-opportunities" className="single-work">
                <i>
                  {" "}
                  <img src={`${imgBaseUrl}technology.png`} alt="Technology" />
                </i>
                <h4>Technology</h4>
              </Link>
            </article>

            <article className="col-md-3 col-sm-6 col-xs-12">
              <Link to="/career-opportunities" className="single-work">
                <i>
                  {" "}
                  <img src={`${imgBaseUrl}finace.png`} alt="Finance" />
                </i>
                <h4>Finance</h4>
              </Link>
            </article>

            <article className="col-md-3 col-sm-6 col-xs-12">
              <Link to="/career-opportunities" className="single-work">
                <i>
                  {" "}
                  <img
                    src={`${imgBaseUrl}adminstration.png`}
                    alt="Administration"
                  />
                </i>
                <h4>Administration</h4>
              </Link>
            </article>

            <article className="col-md-3 col-sm-6 col-xs-12">
              <Link to="/career-opportunities" className="single-work">
                <i>
                  {" "}
                  <img src={`${imgBaseUrl}sales.png`} alt="sales" />
                </i>
                <h4>Sales</h4>
              </Link>
            </article>
            <article className="col-md-3 col-sm-6 col-xs-12">
              <Link to="/career-opportunities" className="single-work">
                <i>
                  {" "}
                  <img src={`${imgBaseUrl}marketing.png`} alt="marketing" />
                </i>
                <h4>Marketing</h4>
              </Link>
            </article>
            <article className="col-md-3 col-sm-6 col-xs-12">
              <Link to="/career-opportunities" className="single-work">
                <i>
                  {" "}
                  <img src={`${imgBaseUrl}bd.png`} alt="Business development" />
                </i>
                <h4>Business development</h4>
              </Link>
            </article>
            <article className="col-md-3 col-sm-6 col-xs-12">
              <Link to="/career-opportunities" className="single-work">
                <i>
                  {" "}
                  <img src={`${imgBaseUrl}creative.png`} alt="Creative" />
                </i>
                <h4 className="services">Creative</h4>
              </Link>
            </article>
            <article className="col-md-3 col-sm-6 col-xs-12">
              <Link to="/career-opportunities" className="single-work">
                <i>
                  {" "}
                  <img src={`${imgBaseUrl}hr.png`} alt="Human resource" />
                </i>
                <h4>Human resource</h4>
              </Link>
            </article>
          </article>
        </article>
        <section className="section2">
          <article className="container">
            <article className="row">
              <article className="col-md-12 col-xs-12 col-sm-12">
                <h5>FUN WORKPLACE</h5>
                <p>
                  Our employees are as passionate about life as they are about
                  their work and there’s never a dull moment at the workplace.
                  If ever a workplace could be a second home, you may find it at
                  the youthful and friendly work environment at Buddy4Study.
                </p>
                <img
                  src={`${imgBaseUrl}manoranjan.jpg`}
                  alt="Buddy4study"
                  className="img-responsive"
                />
              </article>

              <article className="col-md-12 col-xs-12 col-sm-12">
                <h5>GOAL ORIENTED</h5>
                <p>
                  No matter where you see yourself 5 or 10 years from now, you
                  can realize your goals with Buddy4Study. We like setting lofty
                  goals for ourselves, and leaving no stone unturned to make
                  them come true. And we look for the same in our employees.
                </p>
                <img
                  src={`${imgBaseUrl}fun.jpg`}
                  alt="Buddy4study"
                  className="img-responsive"
                />
              </article>

              <article className="col-md-12 col-xs-12 col-sm-12">
                <h5>GROW WITH US</h5>
                <p>
                  We’re one of the fastest growing Indian start-ups in
                  education. In the past six years, we’ve grown phenomenally and
                  these are just the first steps in our journey. If you are
                  looking for fast growth and ready for the challenges that come
                  with it, we believe Buddy4Study is the best workplace for you.
                </p>
              </article>
              <article className="col-md-12 col-xs-12 col-sm-12 text-center">
                <Link to="/career-opportunities" className="btn">
                  Explore good opportunities
                </Link>
              </article>
            </article>
          </article>
        </section>
      </section>
    );
  }
}

export default Careers;
