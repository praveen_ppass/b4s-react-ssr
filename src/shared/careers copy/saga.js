import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_OPP_REQUESTED,
  FETCH_OPP_SUCCEEDED,
  FETCH_OPP_FAILED,
  FETCH_CAREER_REQUESTED,
  FETCH_CAREER_SUCCEEDED,
  FETCH_CAREER_FAILED
} from "./actions";

const fetchUrl = () =>
  fetchClient.get(apiUrl.careerOpportunities).then(res => {
    return res.data;
  });

function* fetchOpp(input) {
  try {
    const opp = yield call(fetchUrl);
    yield put({
      type: FETCH_OPP_SUCCEEDED,
      payload: opp
    });
  } catch (error) {
    yield put({
      type: FETCH_OPP_FAILED,
      payload: error.errorMessage
    });
  }
}

const fetchDetailsUrl = input =>
  fetchClient.get(`${apiUrl.careerDetails}/${input}`, {}).then(res => {
    return res.data;
  });

function* fetchCareerDetails(input) {
  try {
    const teamDetails = yield call(fetchDetailsUrl, input.payload["id"]);

    yield put({
      type: FETCH_CAREER_SUCCEEDED,
      payload: teamDetails
    });
  } catch (error) {
    yield put({
      type: FETCH_CAREER_FAILED,
      payload: error.errorMessage
    });
  }
}

export default function* fetchCareerSaga() {
  yield takeEvery(FETCH_OPP_REQUESTED, fetchOpp);
  yield takeEvery(FETCH_CAREER_REQUESTED, fetchCareerDetails);
}
