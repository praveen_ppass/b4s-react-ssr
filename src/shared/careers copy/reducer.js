import {
  FETCH_OPP_SUCCEEDED,
  FETCH_OPP_REQUESTED,
  FETCH_OPP_FAILED,
  FETCH_CAREER_REQUESTED,
  FETCH_CAREER_SUCCEEDED,
  FETCH_CAREER_FAILED
} from "./actions";

const initialState = {
  showLoader: false,
  isError: false,
  oppList: [],
  errorMessage: "",
  carrerDetails: null
};
const oppReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_OPP_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        isError: false,
        errorMessage: ""
      };

    case FETCH_OPP_SUCCEEDED:
      return {
        ...state,
        type,
        oppList: payload,
        showLoader: false,
        isError: false,
        errorMessage: ""
      };

    case FETCH_OPP_FAILED:
      return {
        ...state,
        payload,
        type,
        oppList: [],
        showLoader: false,
        isError: true,
        errorMessage: payload
      };

    case FETCH_CAREER_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        errorMessage: "",
        careerDetails: null,
        type
      };
    case FETCH_CAREER_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        errorMessage: payload,
        type
      };

    case FETCH_CAREER_SUCCEEDED:
      return {
        ...state,
        showLoader: true,
        type,
        isError: false,
        errorMessage: "",
        careerDetails: payload
      };

    default:
      return state;
  }
};
export default oppReducer;
