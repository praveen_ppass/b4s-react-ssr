export const FETCH_OPP_REQUESTED = "FETCH_ABOUT_REQUESTED";
export const FETCH_OPP_SUCCEEDED = "FETCH_OPP_SUCCEEDED";
export const FETCH_OPP_FAILED = "FETCH_OPP_FAILED";
export const FETCH_CAREER_REQUESTED = "FETCH_CAREER_REQUESTED";
export const FETCH_CAREER_SUCCEEDED = "FETCH_CAREER_SUCCEEDED";
export const FETCH_CAREER_FAILED = "FETCH_CAREER_FAILED";

export const fetchOpportunities = data => ({
  type: FETCH_OPP_REQUESTED,
  payload: { data: data }
});

export const receivesOpportunities = payload => ({
  type: FETCH_OPP_SUCCEEDED,
  payload
});

export const fetchCareerDetails = data => ({
  type: FETCH_CAREER_REQUESTED,
  payload: { id: data }
});

export const receivesCareerDetails = payload => ({
  type: FETCH_CAREER_SUCCEEDED,
  payload
});
