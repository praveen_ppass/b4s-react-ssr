import {
  FETCH_VLE_PAYMENT_PLANS_REQUEST,
  FETCH_VLE_PAYMENT_PLANS_SUCCESS,
  FETCH_VLE_PAYMENT_PLANS_FAILURE,
  FETCH_MARCHANT_ID_REQUEST,
  FETCH_MARCHANT_ID_SUCCESS,
  FETCH_MARCHANT_ID_FAIL
} from "../vle/actions";

import {
  FETCH_PAYMENT_STATUS_REQUEST,
  FETCH_PAYMENT_STATUS_SUCCESS,
  FETCH_PAYMENT_STATUS_FAIL
} from "./actions";

const initialState = {
  showLoader: false,
  paymentPlans: [],
  isError: false,
  message: "",
  paymentStatusMode: []
};

const applicationPaymentReducer = (state = initialState, { payload, type }) => {
  switch (type) {
    /**** VLE Payment plans started********** */
    case FETCH_VLE_PAYMENT_PLANS_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_VLE_PAYMENT_PLANS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        paymentPlans: payload
      };

    case FETCH_VLE_PAYMENT_PLANS_FAILURE:
      return { ...state, showLoader: false, type, isError: true };
    /**** VLE Payment plans Ends********** */

    case FETCH_MARCHANT_ID_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_MARCHANT_ID_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        marchantId: payload
      };

    case FETCH_MARCHANT_ID_FAIL:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        message: payload.message ? payload.message : null,
        errorCode: payload.errorCode ? payload.errorCode : null
      };

    case FETCH_PAYMENT_STATUS_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_PAYMENT_STATUS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        paymentStatusMode: payload
      };

    case FETCH_PAYMENT_STATUS_FAIL:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        message: payload.message ? payload.message : null,
        errorCode: payload.errorCode ? payload.errorCode : null
      };
    default:
      return state;
  }
};

export default applicationPaymentReducer;
