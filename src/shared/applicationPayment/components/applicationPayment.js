import React from "react";
import { Redirect } from "react-router-dom";
import Loader from "../../common/components/loader";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import {
  IMButtonJS,
  INSTAMOJO_COLLEGE_BOARD_LIVE,
  INSTAMOJO_COLLEGE_BOARD_TEST,
  messages,
  prodEnv
} from "../../../constants/constants";
import gblFunc from "../../../globals/globalFunctions";
import {
  FETCH_VLE_PAYMENT_PLANS_SUCCESS,
  FETCH_MARCHANT_ID_SUCCESS,
  FETCH_MARCHANT_ID_FAIL
} from "../../vle/actions";
import AlertMessage from "../../common/components/alertMsg";
import UserLoginRegistrationPopup from "../../login/containers/userLoginRegistrationContainer";
import {
  FETCH_PAYMENT_STATUS_SUCCESS,
  FETCH_PAYMENT_STATUS_FAIL
} from "../actions";
import { LOG_USER_OUT_SUCCEEDED } from "../../login/actions";

export default class ApplicationPayment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedPlanId: "",
      hidePaymentLink: false,
      applicationPaymentConfig: {
        url: ``
      },
      showAlert: false,
      msg: "",
      showLoginPopup: false,
      amount: "",
      appSchIds: {
        "10939": {
          bsid: "CBI2"
        },
        "10936": {
          bsid: "CBS3"
        }
      },
      couponCode: null,
      isAppliedCoupon: false,
      isStatus: false,
      paymentPlan: null,
      paymentPlanStructure: {
        transactionFee: null,
        reducedAmount: null,
        totalCost: null
      }
    };

    this.hideAlert = this.hideAlert.bind(this);
    this.onAppPayPopUp = this.onAppPayPopUp.bind(this);
    this.closeLoginPopup = this.closeLoginPopup.bind(this);
    this.onCouponChangeHandler = this.onCouponChangeHandler.bind(this);
    this.instamojoPopUpHandler = this.instamojoPopUpHandler.bind(this);
  }

  componentDidMount() {
    const { match } = this.props;
    const userId = gblFunc.getStoreUserDetails()["userId"];
    gblFunc.loadJsScript(IMButtonJS, true);
    if (!userId) {
      this.setState({
        showLoginPopup: true
      });
    }

    if (
      match &&
      match.params &&
      !["CBI2", "CBS3"].includes(match.params.bsid)
    ) {
      this.props.history.push("/");
    }
    this.props.fetchPaymentPlan({}, "CB");
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case FETCH_VLE_PAYMENT_PLANS_SUCCESS:
        const getCheckPaymentPlanId = {
          CBI2: 6,
          CBS3: 7
        };
        const { vlePaymentPlans } = nextProps;
        const { bsid } = this.props.match.params;

        if (vlePaymentPlans.length > 0) {
          const filteringPaymentPlan = vlePaymentPlans.filter(
            f => f.id === getCheckPaymentPlanId[bsid]
          );

          const { transactionFee, reducedAmount } = filteringPaymentPlan[0];
          const paymentPlanStructure = {
            transactionFee,
            reducedAmount,
            totalCost: reducedAmount + transactionFee
          };
          this.setState({
            paymentPlanStructure
          });
        }
        break;

      case FETCH_MARCHANT_ID_SUCCESS:
        const userData = gblFunc.getStoreUserDetails();
        if (userData) {
          const updateStudentPayment = {
            ...this.state.applicationPaymentConfig
          };
          const { firstName, lastName, email, mobile } = userData;
          const { match } = this.props;
          const { totalCost, reducedAmount } = this.state.paymentPlanStructure;

          let amountValue;

          if (
            match &&
            match.params &&
            match.params.bsid === "CBI2" &&
            nextProps.marchantID.couponCode
          ) {
            amountValue = totalCost - 50;
          } else if (
            match &&
            match.params &&
            match.params.bsid === "CBI2" &&
            !nextProps.marchantID.couponCode
          ) {
            amountValue = totalCost;
          }

          if (
            match &&
            match.params &&
            match.params.bsid === "CBS3" &&
            nextProps.marchantID.couponCode
          ) {
            amountValue = totalCost - 50;
          } else if (
            match &&
            match.params &&
            match.params.bsid === "CBS3" &&
            !nextProps.marchantID.couponCode
          ) {
            amountValue = totalCost;
          }
          //
          // "https://www.instamojo.com/buddy4study/sat-discount-voucher-fee/";
          let url = prodEnv
            ? INSTAMOJO_COLLEGE_BOARD_LIVE
            : INSTAMOJO_COLLEGE_BOARD_TEST;

          if (firstName !== "null" && lastName !== "null") {
            url = url + `?data_name=${firstName} ${lastName}`;
          }
          if (firstName !== "null" && lastName === "null") {
            url = url + `?data_name=${firstName}`;
          }
          if (email !== "null") {
            url = url + `&data_email=${email}`;
          }
          if (mobile !== "null" && mobile !== "0") {
            url = url + `&data_phone=${mobile}`;
          }
          //Field_22676
          //Field_43491
          updateStudentPayment.url = `${url}&data_hidden=data_Field_22676&data_readonly=data_amount&data_amount=${amountValue}&data_Field_22676=${
            nextProps.marchantID
              ? nextProps.marchantID.merchantTransactionNumber
              : ""
            }`;
          this.setState(
            {
              applicationPaymentConfig: updateStudentPayment,
              isAppliedCoupon: nextProps.marchantID.couponCode ? true : false,
              couponCode: nextProps.marchantID.couponCode,
              showAlert: nextProps.marchantID.couponCode ? true : false,
              msg: nextProps.marchantID.couponCode ? "Coupon redeemed" : "",
              isStatus: true
            },
            () => {
              if (!this.state.isAppliedCoupon) {
                this.instamojoPopUpHandler();
              }
            }
          );
        }
        break;
      case FETCH_MARCHANT_ID_FAIL:
        this.setState({
          showAlert: true,
          isStatus: false,
          msg:
            nextProps.errorCode && nextProps.errorCode !== 701
              ? messages.generic.httpErrors[nextProps.errorCode]
              : nextProps.message
        });
        break;
      case FETCH_PAYMENT_STATUS_SUCCESS:
        const userId = gblFunc.getStoreUserDetails()["userId"];
        const { paymentStatusMode } = nextProps;
        const status = paymentStatusMode.filter(
          stMode => stMode.paymentPlanId === this.state.selectedPlanId
        );
        if (userId && !status[0].paid) {
          this.props.getMerchantId({
            userId,
            marchentObj: {
              amount: this.state.amount,
              paymentPlanId: this.state.selectedPlanId,
              currency: "inr",
              initiatorUserId: userId,
              paidForUserId: userId ? userId : null,
              paymentPartner: "CB",
              transactionMode: "D",
              transactionType: "D",
              couponCode: this.state.couponCode ? this.state.couponCode : null
            }
          });
        } else {
          this.setState({
            showAlert: true,
            msg: "Payment is already paid by the user.",
            isStatus: false
          });
        }

        break;
      case FETCH_PAYMENT_STATUS_FAIL:
        // this.setState({
        //   showAlert: true,
        //   msg:
        // });
        break;
    }
  }

  onAppPayPopUp(id, amount) {
    const userData = gblFunc.getStoreUserDetails();
    if (userData && !userData.userId) {
      const { match } = this.props;

      let redirectUrl = `/application/${
        this.props.match.params.bsid
        }/instruction`;
      this.props.history.push(redirectUrl);
    }
    if (userData && userData.userId) {
      this.setState(
        {
          amount:
            this.state.paymentPlanStructure &&
            this.state.paymentPlanStructure.totalCost,
          selectedPlanId: id
        },
        () =>
          this.props.checkPaymentStatus({
            userId: userData.userId,
            paymentPartner: "CB"
          })
      );
    }
  }

  hideAlert() {
    this.setState({ msg: "", showAlert: false });
  }

  closeLoginPopup(e) {
    // e.preventDefault();

    this.setState({
      showLoginPopup: false
    });
  }

  onCouponChangeHandler(event) {
    this.setState({
      couponCode: event.target.value
    });
  }

  instamojoPopUpHandler() {
    const that = this;
    // this.setState({});
    try {
      var promise = gblFunc.loadJsScript(IMButtonJS, true);

      promise.then(
        /**************************************************************************** */
        /* I know this is dirty approach - exploring for standard solution@pushpendra
    /****** ************************************************************************ */

        function () {
          try {
            var a = [
              ...document.getElementsByClassName("im-checkout-btn")
            ].filter(item => {
              return item.parentNode.parentNode.id == that.state.selectedPlanId;
            });

            if (a.length) {
              a[0].setAttribute(
                "href",
                that.state.applicationPaymentConfig.url
              );
            }

            a[0].click();
          } catch (error) {
            //fallback handling......Payment will be processed in every case
          }
          /**************************************************************************** */
          /* I know this is dirty approach - exploring for standard solution@pushpendra
    /****************************************************************************** */
        },
        function (error) {
          gblFunc.logMessage(error);
        }
      );
    } catch (error) {
      gblFunc.logMessage(error);
    }
  }

  render() {
    const { match } = this.props;
    const schId = gblFunc.getStoreApplicationScholarshipId();
    const {
      appSchIds,
      amountOfNinty,
      amountOfFifty,
      paymentPlanStructure
    } = this.state;
    let redirectTo = null;
    // console.log(this.state.paymentPlan);
    if (
      schId &&
      appSchIds[schId] &&
      appSchIds[schId].bsid !== match.params.bsid
    ) {
      redirectTo = (
        <Redirect
          to={{
            pathname: `/application/${match.params.bsid}/instruction`
          }}
        />
      );
    }

    if ((schId && !appSchIds[schId]) || !schId) {
      redirectTo = (
        <Redirect
          to={{
            pathname: `/application/${match.params.bsid}/instruction`
          }}
        />
      );
    }

    return (
      <section>
        {redirectTo}
        {this.state.showLoginPopup ? (
          <UserLoginRegistrationPopup
            closePopup={this.closeLoginPopup}
            hideCloseButton={false}
            hideForgetCloseBtn={false}
          />
        ) : null}
        <section className="contact collegeboardPayment">
          <AlertMessage
            isShow={this.state.showAlert}
            msg={this.state.msg}
            status={this.state.isStatus}
            close={this.hideAlert}
          />
          <Loader isLoader={this.props.showLoader} />
          {/* <BreadCrum
            classes={breadCrumObj["collegeboard_payment"].bgImage}
            listOfBreadCrum={breadCrumObj["collegeboard_payment"].breadCrum}
            subTitle={breadCrumObj["collegeboard_payment"].subTitle}
            title={breadCrumObj["collegeboard_payment"].title}
          /> */}
          <section className="container-fluid collegeBoardiscount">
            <section className="container">
              <span id="dashPayPreFormSubs" />
              <section className="row">
                {match && match.params && match.params.bsid === "CBI2" ? (
                  <section className="col-md-12 col-sm-12">
                    <article className="wrapper">
                      <h4>SAT Discount Voucher </h4>
                      <article className="amount">
                        <h6>
                          Voucher cost:{" "}
                          <span>
                            {" "}
                            &#x20B9;
                            {paymentPlanStructure &&
                              paymentPlanStructure.reducedAmount}{" "}
                          </span>{" "}
                        </h6>
                        <h6>
                          Transaction fee:{" "}
                          <span>
                            {" "}
                            &#x20B9;
                            {paymentPlanStructure &&
                              paymentPlanStructure.transactionFee}{" "}
                          </span>{" "}
                        </h6>
                        {this.state.isAppliedCoupon ? (
                          <h6>
                            Coupon Applied:{" "}
                            <span style={{ color: "red" }}>
                              &ndash; &#x20B9;50{" "}
                            </span>{" "}
                          </h6>
                        ) : null}
                        {this.state.isAppliedCoupon ? (
                          <h6>
                            Total cost:{" "}
                            <span style={{ textDecoration: "line-through" }}>
                              {" "}
                              &#x20B9;
                              {paymentPlanStructure &&
                                paymentPlanStructure.totalCost}{" "}
                            </span>{" "}
                            <span>
                              {" "}
                              &#x20B9;
                              {(paymentPlanStructure &&
                                paymentPlanStructure.totalCost) - 50}{" "}
                            </span>{" "}
                          </h6>
                        ) : (
                            <h6>
                              Total cost:{" "}
                              <span>
                                {" "}
                                &#x20B9;
                              {paymentPlanStructure &&
                                  paymentPlanStructure.totalCost}{" "}
                              </span>{" "}
                            </h6>
                          )}
                      </article>
                      <p>
                        Transaction fee of &#x20B9;
                        {paymentPlanStructure &&
                          paymentPlanStructure.transactionFee}{" "}
                        consists of 2% processing fee, &#x20B9;3 per transaction
                        charge and 18% Goods and Services Tax (GST) on the
                        processing fee
                      </p>
                      <article>
                        <label htmlFor="coupon">
                          Do you have a freecharge coupon code?
                        </label>
                        <input
                          className=""
                          type="text"
                          id="coupon"
                          placeholder="Enter Coupon Code"
                          onChange={this.onCouponChangeHandler}
                          disabled={this.state.isAppliedCoupon ? true : false}
                        />
                        <button
                          className={`redeem ${
                            this.state.couponCode && !this.state.isAppliedCoupon
                              ? ""
                              : "disabledBtn"
                            }`}
                          disabled={
                            this.state.couponCode && !this.state.isAppliedCoupon
                              ? false
                              : true
                          }
                          onClick={() => this.onAppPayPopUp(6, 706)}
                        >
                          {this.state.isAppliedCoupon ? "Applied" : "Apply"}
                        </button>
                      </article>
                      <article
                        id="6"
                        style={{
                          display: !this.state.hidePaymentLink ? "none" : ""
                        }}
                      >
                        <PayButtonLink
                          url={`${this.state.applicationPaymentConfig.url}`}
                          text="Avail 90% discount voucher"
                        />
                      </article>

                      {this.state.isAppliedCoupon ? (
                        <button
                          className="coupon"
                          onClick={() => this.instamojoPopUpHandler()}
                        >
                          PAY NOW
                        </button>
                      ) : (
                          <button
                            disabled={
                              match &&
                                match.params &&
                                match.params.bsid == "CBI2" &&
                                !this.state.isAppliedCoupon &&
                                !this.state.couponCode
                                ? false
                                : true
                            }
                            className={`coupon ${
                              !this.state.isAppliedCoupon &&
                                !this.state.couponCode
                                ? ""
                                : "disabledBtn"
                              }`}
                            onClick={() => this.onAppPayPopUp(6, 706)}
                          >
                            PAY NOW
                        </button>
                        )}
                    </article>
                  </section>
                ) : null}

                {match && match.params && match.params.bsid === "CBS3" ? (
                  <section className="col-md-12 col-sm-12">
                    <article className="wrapper">
                      <h4>SAT Discount Voucher </h4>
                      <article className="amount">
                        <h6>
                          Voucher cost:{" "}
                          <span>
                            {" "}
                            &#x20B9;
                            {paymentPlanStructure &&
                              paymentPlanStructure.reducedAmount}
                          </span>{" "}
                        </h6>
                        <h6>
                          Transaction fee:{" "}
                          <span>
                            {" "}
                            &#x20B9;
                            {paymentPlanStructure &&
                              paymentPlanStructure.transactionFee}{" "}
                          </span>{" "}
                        </h6>

                        {this.state.isAppliedCoupon ? (
                          <h6>
                            Coupon Applied:{" "}
                            <span style={{ color: "red" }}>
                              &ndash; &#x20B9;50{" "}
                            </span>{" "}
                          </h6>
                        ) : null}
                        {this.state.isAppliedCoupon ? (
                          <h6>
                            Total cost:{" "}
                            <span style={{ textDecoration: "line-through" }}>
                              {" "}
                              &#x20B9;
                              {paymentPlanStructure &&
                                paymentPlanStructure.totalCost}{" "}
                            </span>{" "}
                            <span>
                              {" "}
                              &#x20B9;
                              {(paymentPlanStructure &&
                                paymentPlanStructure.totalCost) - 50}{" "}
                            </span>{" "}
                          </h6>
                        ) : (
                            <h6>
                              Total cost:{" "}
                              <span>
                                {" "}
                                &#x20B9;
                              {paymentPlanStructure &&
                                  paymentPlanStructure.totalCost}{" "}
                              </span>{" "}
                            </h6>
                          )}
                      </article>
                      <p>
                        Transaction fee of &#x20B9;
                        {paymentPlanStructure &&
                          paymentPlanStructure.transactionFee}{" "}
                        consists of 2% processing fee, &#x20B9;3 per transaction
                        charge and 18% Goods and Services Tax (GST) on the
                        processing fee
                      </p>
                      <article>
                        <label htmlFor="coupon">
                          Do you have a freecharge coupon code?
                        </label>
                        <input
                          className=""
                          type="text"
                          id="coupon"
                          placeholder="Enter Coupon Code"
                          onChange={this.onCouponChangeHandler}
                          disabled={this.state.isAppliedCoupon ? true : false}
                        />
                        <button
                          className={`redeem ${
                            this.state.couponCode && !this.state.isAppliedCoupon
                              ? ""
                              : "disabledBtn"
                            }`}
                          disabled={
                            this.state.couponCode && !this.state.isAppliedCoupon
                              ? false
                              : true
                          }
                          onClick={() => this.onAppPayPopUp(7, 3515)}
                        >
                          {this.state.isAppliedCoupon ? "Applied" : "Apply"}
                        </button>
                      </article>
                      <article
                        id="7"
                        style={{
                          display: !this.state.hidePaymentLink ? "none" : ""
                        }}
                      >
                        <PayButtonLink
                          url={`${this.state.applicationPaymentConfig.url}`}
                          text="Avail 50% discount voucher"
                        />
                      </article>

                      {this.state.isAppliedCoupon ? (
                        <button
                          className="coupon"
                          onClick={() => this.instamojoPopUpHandler()}
                        >
                          PAY NOW
                        </button>
                      ) : (
                          <button
                            disabled={
                              match &&
                                match.params &&
                                match.params.bsid == "CBS3" &&
                                !this.state.isAppliedCoupon &&
                                !this.state.couponCode
                                ? false
                                : true
                            }
                            className={`coupon ${
                              !this.state.isAppliedCoupon &&
                                !this.state.couponCode
                                ? ""
                                : "disabledBtn"
                              }`}
                            onClick={() => this.onAppPayPopUp(7, 3515)}
                          >
                            PAY NOW
                        </button>
                        )}
                    </article>
                  </section>
                ) : null}
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

const PayButtonLink = props => {
  return (
    <a
      href={props.url}
      rel="im-checkout"
      data-behaviour="remote"
      data-style="flat"
      data-text={props.text}
      className="btn"
    >
      {props.text}
    </a>
  );
};
