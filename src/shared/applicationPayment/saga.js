import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_PAYMENT_STATUS_SUCCESS,
  FETCH_PAYMENT_STATUS_REQUEST,
  FETCH_PAYMENT_STATUS_FAIL
} from "./actions";

const applicationPaymentStatusApi = ({ userId, paymentPartner }) => {
  return fetchClient
    .get(
      apiUrl.paymentStatus +
        `/${userId}/paymentstatus?paymentPartner=${paymentPartner}`
    )
    .then(res => {
      return res.data;
    });
};

function* applicationPaymentStatus(input) {
  try {
    const paymentStatus = yield call(
      applicationPaymentStatusApi,
      input.payload
    );

    yield put({
      type: FETCH_PAYMENT_STATUS_SUCCESS,
      payload: paymentStatus
    });
  } catch (error) {
    yield put({
      type: FETCH_PAYMENT_STATUS_FAIL,
      payload: error.errorMessage
    });
  }
}

export default function* applicationPaymentSaga() {
  yield takeEvery(FETCH_PAYMENT_STATUS_REQUEST, applicationPaymentStatus);
}
