import ApplicationPayment from "../components/applicationPearsonPayment";
import { connect } from "react-redux";

import {
  fetchPaymentPlans as fetchPaymentPlansAction,
  merchantId as getMerchantIdAction
} from "../../vle/actions";

import { paymentStatus as checkPaymentStatusAction } from "../actions";

const mapStateToProps = ({ applicationPayment }) => ({
  vlePaymentPlans: applicationPayment.paymentPlans,
  type: applicationPayment.type,
  marchantID: applicationPayment.marchantId,
  errorCode: applicationPayment.errorCode,
  message: applicationPayment.message,
  showLoader: applicationPayment.showLoader,
  paymentStatusMode: applicationPayment.paymentStatusMode
});

const mapDispatchToProps = dispatch => ({
  fetchPaymentPlan: (inputData, type) =>
    dispatch(fetchPaymentPlansAction(inputData, type)),
  getMerchantId: inputData => dispatch(getMerchantIdAction(inputData)),
  checkPaymentStatus: inputData => dispatch(checkPaymentStatusAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationPayment);
