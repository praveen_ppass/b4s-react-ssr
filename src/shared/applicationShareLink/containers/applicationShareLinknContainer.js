import ApplicationShareLink from "../components/applicationShareLink";
import { connect } from "react-redux";
import { getApplicationInstructions as getApplicationInstructionsAction } from "../../application/actions/applicationInstructionActions";

const mapStateToProps = ({ applicationShareLink }) => ({
  showLoader: applicationShareLink.showLoader,
  type: applicationShareLink.type,
  applicationInstruction: applicationShareLink.applicationInstruction
});

const mapDispatchToProps = dispatch => ({
  getSlutFromInstruction: slug =>
    dispatch(getApplicationInstructionsAction(slug))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  ApplicationShareLink
);
