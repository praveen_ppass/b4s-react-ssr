import {
  FETCH_APPLICATION_INSTRUCTION_REQUEST,
  FETCH_APPLICATION_INSTRUCTION_SUCCESS,
  FETCH_APPLICATION_INSTRUCTION_FAILURE
} from "../application/actions/applicationInstructionActions";

const initialState = {
  applicationInstruction: null
};

const applicationShareLinkReducers = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case FETCH_APPLICATION_INSTRUCTION_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        applicationInstruction: null
      };

    case FETCH_APPLICATION_INSTRUCTION_SUCCESS:
      return {
        ...state,
        applicationInstruction: payload,
        type,
        showLoader: false
      };
    case FETCH_APPLICATION_INSTRUCTION_FAILURE:
      return { ...state, showLoader: false, isError: true, type };
    default:
      return state;
  }
};

export default applicationShareLinkReducers;
