import React from "react";
import { Redirect, Link } from "react-router-dom";
import Loader from "../../common/components/loader";
import { IMButtonJS, messages } from "../../../constants/constants";
import gblFunc from "../../../globals/globalFunctions";
import AlertMessage from "../../common/components/alertMsg";
import {
  FacebookShareButton,
  TwitterShareButton,
  LinkedinShareButton,
  WhatsappShareButton
} from "react-share";

export default class ApplicationPayment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedPlanId: "",
      hidePaymentLink: false,

      showAlert: false,
      msg: "",
      showLoginPopup: false
    };

    this.hideAlert = this.hideAlert.bind(this);

    this.closeLoginPopup = this.closeLoginPopup.bind(this);
    this.onSkipHandler = this.onSkipHandler.bind(this);
  }

  componentDidMount() {
    const slug = this.props.match && this.props.match.params.slug;
    this.props.getSlutFromInstruction(slug);
  }

  componentWillReceiveProps(nextProps) { }

  hideAlert() {
    this.setState({ msg: "", showAlert: false });
  }

  closeLoginPopup(e) {
    e.preventDefault();

    this.setState({
      showLoginPopup: false
    });
  }

  gtmEventHandler(gtm) {
    gblFunc.gaTrack.trackEvent(gtm);
  }

  onSkipHandler() {
    let to = null;
    if (gblFunc.getStoreUserDetails()["vleUser"] == "true") {
      to = `/vle/student-list`;
    } else if (["CBS3", "CBI2"].includes(this.props.match.params.slug)) {
      to = `/payment/${this.props.match.params.slug}/collegeboard`;
    } else if (this.props.applicationInstruction.redirectUrl != null) {
      if (
        this.props.applicationInstruction.redirectUrl.includes("http") ||
        this.props.applicationInstruction.redirectUrl.includes("https")
      ) {
        window.open(this.props.applicationInstruction.redirectUrl, "_blank");
      } else {
        to = this.props.applicationInstruction.redirectUrl;
      }
    } else {
     // to = `/myProfile/ApplicationStatus`;
      to = "/myprofile/application-status";
    }
//    this.props.history.push(`${to}`);
window.open(`https://www.buddy4study.com${to}`,"_self")
  }

  render() {
    const { applicationInstruction } = this.props;
    // const schId = gblFunc.getStoreApplicationScholarshipId();
    // const { appSchIds } = this.state;
    //
    let redirect = null;
    if (
      ["CES7","CES8", "SCE3", "CKS1", "SSE4", "HUL1"].includes(
        this.props.match.params.slug
      )
    ) {
      redirect = (
        <Redirect
          to={`/application/${this.props.match.params.slug}/instruction`}
        />
      );
    }
    return (
      <section>
        {redirect}
        <section className="applicationshareLink ">
          <article className="bgshare">
            <AlertMessage
              isShow={this.state.showAlert}
              msg={this.state.msg}
              status={false}
              close={this.hideAlert}
            />
            <Loader isLoader={this.props.showLoader} />
            <article className="imgAlpha" />
            <section className="container">
              <article className="row">
                <article className="box">
                  <h1>Thank you! </h1>
                  <h3>
                    Your scholarship application has been submitted
                    successfully.
                  </h3>
                  <p>
                    Share this scholarship opportunity with your friends and
                    network!
                  </p>
                  <article className="socialSharedCounter">
                    <a>
                      <div
                        role="button"
                        tabIndex="0"
                        className="SocialMediaShareButton SocialMediaShareButton--facebook"
                        onClick={() => {
                          this.gtmEventHandler([
                            "Social Sharing",
                            "Application Submitted",
                            "Facebook"
                          ]);
                        }}
                      >
                        <FacebookShareButton
                          url={
                            applicationInstruction &&
                              applicationInstruction.slug
                              ? `https://buddy4study.com/scholarship/${
                              applicationInstruction.slug
                              }`
                              : ""
                          }
                          quote="I found this scholarship useful to pursue my education and career dreams!  You can also apply. Please do share in your network to help your friends! #SharingIsCaring #Scholarship @Buddy4Study"
                        >
                          <i className="fa fa-facebook" aria-hidden="true" />
                        </FacebookShareButton>
                      </div>
                    </a>
                    <a>
                      <div
                        role="button"
                        tabIndex="0"
                        className="SocialMediaShareButton SocialMediaShareButton--twitter"
                        onClick={() => {
                          this.gtmEventHandler([
                            "Social Sharing",
                            "Application Submitted",
                            "Twitter"
                          ]);
                        }}
                      >
                        <TwitterShareButton
                          title="I found this scholarship useful to pursue my education and career dreams!  You can also apply. Please do share in your network to help your friends! #SharingIsCaring #Scholarship @Buddy4Study"
                          url={
                            applicationInstruction &&
                              applicationInstruction.slug
                              ? `https://buddy4study.com/scholarship/${
                              applicationInstruction.slug
                              }`
                              : ""
                          }
                          via="buddy4study"
                        >
                          <i className="fa fa-twitter" aria-hidden="true" />
                        </TwitterShareButton>
                      </div>
                    </a>
                    <a>
                      <div
                        role="button"
                        tabIndex="0"
                        className="SocialMediaShareButton SocialMediaShareButton--linkedin"
                        onClick={() => {
                          this.gtmEventHandler([
                            "Social Sharing",
                            "Application Submitted",
                            "Linkedin"
                          ]);
                        }}
                      >
                        <LinkedinShareButton
                          title="I found this scholarship useful to pursue my education and career dreams!  You can also apply. Please do share in your network to help your friends! #SharingIsCaring #Scholarship @Buddy4Study"
                          url={
                            applicationInstruction &&
                              applicationInstruction.slug
                              ? `https://buddy4study.com/scholarship/${
                              applicationInstruction.slug
                              }`
                              : ""
                          }
                          description={`${applicationInstruction &&
                            applicationInstruction.title}`}
                        >
                          <i className="fa fa-linkedin" aria-hidden="true" />
                        </LinkedinShareButton>
                      </div>
                    </a>
                    <a>
                      <article className="whatsapp">
                        <div
                          role="button"
                          tabIndex="0"
                          className="SocialMediaShareButton SocialMediaShareButton--whatsapp"
                          onClick={() => {
                            this.gtmEventHandler([
                              "Social Sharing",
                              "Application Submitted",
                              "Whatsapp"
                            ]);
                          }}
                        >
                          <WhatsappShareButton
                            title="I found this scholarship useful to pursue my education and career dreams!  You can also apply. Please do share in your network to help your friends! #SharingIsCaring #Scholarship @Buddy4Study"
                            url={
                              applicationInstruction &&
                                applicationInstruction.slug
                                ? `https://buddy4study.com/scholarship/${
                                applicationInstruction.slug
                                }`
                                : ""
                            }
                            via="buddy4study"
                          >
                            <i className="fa fa-whatsapp" aria-hidden="true" />
                          </WhatsappShareButton>
                        </div>
                      </article>
                    </a>
                    {/* <a>
                      <article className="envelope">
                        <div
                          role="button"
                          tabIndex="0"
                          className="SocialMediaShareButton SocialMediaShareButton--envelope"
                        >
                          <i className="fa fa-envelope" aria-hidden="true" />
                        </div>
                      </article>
                    </a> */}
                  </article>
                  <article>
                    {/* <span>
                      https://www.buddy4study.com/scholarship/college-board-india-scholars-program-2019-20?utm_source=FeaturedList&utm_medium=WebDetailPage
                    </span> */}
                    <button className="btn" onClick={this.onSkipHandler}>
                      Skip
                    </button>
                  </article>
                </article>
              </article>
            </section>
          </article>
        </section>
      </section>
    );
  }
}
