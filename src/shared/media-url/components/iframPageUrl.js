import React from "react";

const IframSec = props => {
  return (
    <section className="row iframeWrapper">
      <iframe
        rel="nofollow"
        className="embed-responsive-item"
        frameborder="0"
        width="100%"
        height="700"
        src={props.pageUrl}
      />
    </section>
  );
};

export default IframSec;
