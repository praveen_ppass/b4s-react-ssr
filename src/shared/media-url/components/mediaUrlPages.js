import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { defaultImageLogo, imgBaseUrl } from "../../../constants/constants";
import Loader from "../../common/components/loader";
import AlertMessage from "../../common/components/alertMsg";
import {
  FETCH_MEDIA_URL_FAILED,
  FETCH_MEDIA_URL_SUCCEEDED,
  FETCH_SCHOLARSHIP_MEDIA_URL_SUCCEEDED
} from "../actions";
import { messages } from "../../../constants/constants";
import IframSec from "./iframPageUrl";

class MediaUrl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowAlert: false,
      showAlertMsg: "",
      scholarshipTitle: "",
      scholarshipLogo: "",
      scholarshiplogoUrl: "",
      pageUrl: "",
      mediaTitle: null,
      mediaBSID: null,
      openIframe: false,
      redirectToDetail: false
    };
    this.hideAlert = this.hideAlert.bind(this);
  }

  componentDidMount() {
    //get title
    let title = this.props.match.params.title
      ? this.props.match.params.title
      : "";
    let bsid = this.props.match.params.bsid ? this.props.match.params.bsid : "";
    if (
      (title === "id" ||
        title === "web" ||
        this.props.match.url.includes("/id/")) &&
      bsid
    ) {
      this.setState({
        mediaTitle: title,
        mediaBSID: bsid,
        redirectToDetail: true
      });
      this.props.scholarshipDetailsLite(bsid);
      return;
    }

    this.props.shortUrlDetails(title, bsid); //shortUrl call to get details of short url

    this.setState({
      userSource:
        "b4s-meida-url-" +
        this.props.match.params.title +
        "-" +
        this.props.match.params.bsid
    });
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    switch (type) {
      case FETCH_MEDIA_URL_FAILED:
        this.setState({
          isShowAlert: true,
          showAlertMsg: messages.generic.httpErrors[801],
          redirectToDetail: true
        });
        this.props.scholarshipDetailsLite(this.state.mediaBSID);
        //...Redirect to scholarship detail page...
        break;
      case FETCH_MEDIA_URL_SUCCEEDED: //ShortUrl details...
        const mediaUrlData = {
          scholarshiplogoUrl: nextProps.mediaUrl.mainUrl,
          pageUrl: nextProps.mediaUrl.mainUrl,
          scholarshipLogo: nextProps.mediaUrl.logoUrl,
          iframeStatus: nextProps.mediaUrl.iframeStatus,
          isIframeCompatible: !nextProps.mediaUrl.isIframeCompatible
        };
        let openIframeStatus = false;
        if (mediaUrlData.iframeStatus == 1 && mediaUrlData.isIframeCompatible) {
          //Its allowed to open in Iframe...
          if (mediaUrlData.pageUrl.includes("https")) {
            openIframeStatus = true;
          } else {
            if (typeof window !== "undefined") {
              return (document.location =
                "http://app.buddy4study.com/url?url=" +
                encodeURI(mediaUrlData.pageUrl) +
                "&logo=" +
                encodeURI(mediaUrlData.scholarshipLogo));
            }
          }
        } else {
          if (typeof window !== "undefined") {
            return (document.location = mediaUrlData.pageUrl);
          }
        }
        this.setState({
          ...mediaUrlData,
          openIframe: openIframeStatus
        });
        break;
      case FETCH_SCHOLARSHIP_MEDIA_URL_SUCCEEDED: //Scholarship Details Lite...
        this.setState({
          scholarshipTitle: nextProps.mediaUrlSchData.title,
          scholarshipSlug: nextProps.mediaUrlSchData.slug
        });
        break;
    }
  }

  hideAlert() {
    //  close popup
    this.setState({
      showAlertMsg: "",
      isShowAlert: false
    });
  }

  render() {
    if (this.props.isServerError) {
      return <Redirect to="/404" />;
    }
    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          isShow={this.state.isShowAlert}
          msg={this.state.showAlertMsg}
          status="true"
          close={this.hideAlert}
        />
        <section className="gray mediaUrlBg">
          <section className="mediapartners">
            <section className="container-fluid scholarship-network">
              <section className="row headerBg">
                <article className="container iframenew">
                  <section className="col-sm-2 col-md-2 logo">
                    {this.state.scholarshipLogo != null ? (
                      <a href={this.state.scholarshiplogoUrl} target="_blank">
                        <img
                          src={this.state.scholarshipLogo}
                          className="border"
                          alt="media-partner-logo"
                        />
                      </a>
                    ) : (
                        <img
                          src={`${defaultImageLogo}`}
                          className="border"
                          alt="Company logo"
                        />
                      )}
                  </section>

                  <section className="col-sm-2 col-md-2 companyLogo">
                    <Link to="/">
                      <img
                        src={`${imgBaseUrl}white-logo.png`}
                        alt="buddy4study-logo"
                        id="logo-b4s"
                        className="logo-b4s"
                      />
                    </Link>
                  </section>
                </article>
              </section>
              <section className="container bodyContainer">
                <section className="row scrollingMessage">
                  <p>{this.state.scholarshipTitle}</p>
                </section>
                {this.state.openIframe && (
                  <IframSec pageUrl={this.state.pageUrl} />
                )}
                {this.state.redirectToDetail &&
                  this.state.scholarshipSlug && (
                    <Redirect
                      to={"/scholarship/" + this.state.scholarshipSlug}
                    />
                  )}
              </section>

              <ui-view name="main@content" autoscroll="false" />
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default MediaUrl;
