import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl, messages } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_MEDIA_URL_REQUESTED,
  FETCH_MEDIA_URL_SUCCEEDED,
  FETCH_MEDIA_URL_FAILED,
  SUBMIT_MEDIA_URL_REQUESTED,
  SUBMIT_MEDIA_URL_SUCCEEDED,
  SUBMIT_MEDIA_URL_FAILED,
  FETCH_SCHOLARSHIP_SLUG_REQUESTED,
  FETCH_SCHOLARSHIP_SLUG_SUCCEEDED,
  FETCH_SCHOLARSHIP_SLUG_FAILED,
  FETCH_SCHOLARSHIP_MEDIA_URL_REQUESTED,
  FETCH_SCHOLARSHIP_MEDIA_URL_SUCCEEDED,
  FETCH_SCHOLARSHIP_MEDIA_URL_FAILED
} from "./actions";

const fetchUrl = (title, bsid) =>
  fetchClient.get(apiUrl.mediaUrl + title + "/" + bsid).then(res => {
    return res.data;
  });
const checkIframeStatus = iframeUrl =>
  fetchClient.post("/isIframeUrl", iframeUrl).then(res => {
    return res.data;
  });

function* fetchMediaUrl(input) {
  try {
    const urls = yield call(
      fetchUrl,
      input.payload["title"],
      input.payload["bsid"]
    );
    try {
      if (urls.mainUrl && urls.iframeStatus == 1) {
        const iframeStatus = yield call(checkIframeStatus, {
          iframeUrl: urls.mainUrl
        });
        urls["isIframeCompatible"] = iframeStatus.isBlocked;
      }
    } catch (error) {
      urls["isIframeCompatible"] = "404"; //Page does not exist...
      console.log("Server error, iframe status");
    }
    yield put({
      type: FETCH_MEDIA_URL_SUCCEEDED,
      payload: urls
    });
  } catch (error) {
    yield put({
      type: FETCH_MEDIA_URL_FAILED,
      payload: error
    });
  }
}

/* Media Scholarship fetch url */

const fetchScholarshipUrl = bsid =>
  fetchClient.get(apiUrl.mediaUrlScholarship + bsid + "/lite").then(res => {
    return res.data;
  });

function* fetchScholarshipMediaUrl(input) {
  try {
    const urlsscholarshipname = yield call(
      fetchScholarshipUrl,
      input.payload["bsid"]
    );

    yield put({
      type: FETCH_SCHOLARSHIP_MEDIA_URL_SUCCEEDED,
      payload: urlsscholarshipname
    });
  } catch (error) {
    yield put({
      type: FETCH_SCHOLARSHIP_MEDIA_URL_FAILED,
      payload: error
    });
  }
}

const scholarshipSlugApi = inputData => {
  return fetchClient.get(
    apiUrl.scholarshipSlugByBsid + "/" + inputData + "?bsid=true"
  );
};
function* fetchScholarshipSlugUrl(input) {
  try {
    const urls = yield call(scholarshipSlugApi, input.payload["bsid"]);

    yield put({
      type: FETCH_SCHOLARSHIP_SLUG_SUCCEEDED,
      payload: urls
    });
  } catch (error) {
    yield put({
      type: FETCH_SCHOLARSHIP_SLUG_FAILED,
      payload: error
    });
  }
}

const mediaRegisterApi = inputData => {
  return fetchClient.post(apiUrl.registerUser, inputData).then(res => {
    return res.data;
  });
};

function* submitMediaRegister(input) {
  try {
    let obj = {
      firstName: input.payload.inputData.fullname.split(" ")[0],
      lastName: input.payload.inputData.fullname.split(" ")[1],
      email: input.payload.inputData.email,
      mobile: input.payload.inputData.mobile,
      portalId: 1,
      userSource: input.payload.inputData.userSource
    };

    const response = yield call(mediaRegisterApi, obj);

    yield put({
      type: SUBMIT_MEDIA_URL_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    if (error.response && error.response.data.errorCode === 702) {
      yield put({
        type: SUBMIT_MEDIA_URL_FAILED,
        payload: error.response.data.message,
        isServerError: true
      });
    } else {
      yield put({
        type: SUBMIT_MEDIA_URL_FAILED,
        payload: error.errorMessage,
        isServerError: true
      });
    }
  }
}

export default function* fetchMediaUrlSaga() {
  yield takeEvery(FETCH_MEDIA_URL_REQUESTED, fetchMediaUrl);
  yield takeEvery(
    FETCH_SCHOLARSHIP_MEDIA_URL_REQUESTED,
    fetchScholarshipMediaUrl
  );
  yield takeEvery(SUBMIT_MEDIA_URL_REQUESTED, submitMediaRegister);
  yield takeEvery(FETCH_SCHOLARSHIP_SLUG_REQUESTED, fetchScholarshipSlugUrl);
}
