import React, { Component } from "react";
import { connect } from "react-redux";
import {
  fetchMedia as fetchMediaAction,
  fetchScholarshipMedia as fetchScholarshipMediaAction,
  submitMeida as submitMeidaAction,
  fetchScholarshipSlug as fetchScholarshipSlugAction
} from "../actions";

import MediaUrl from "../components/mediaUrlPages";

const mapStateToProps = ({ mediaUrl }) => ({
  mediaUrl: mediaUrl.mediaData,
  mediaUrlSchData: mediaUrl.mediaUrlSchData,
  mediaSubmit: mediaUrl.mediaSubmit,
  showLoader: mediaUrl.showLoader,
  type: mediaUrl.type,
  slug: mediaUrl.slug,
  serverError: mediaUrl.serverError,
  isServerError: mediaUrl.isServerError
});
const mapDispatchToProps = dispatch => ({
  shortUrlDetails: (title, bsid) => dispatch(fetchMediaAction(title, bsid)),
  scholarshipDetailsLite: bsid => dispatch(fetchScholarshipMediaAction(bsid)),
  submitMeida: dataObject => dispatch(submitMeidaAction(dataObject))
  //getScholarshipSlug: bsid => dispatch(fetchScholarshipSlugAction(bsid))
});

export default connect(mapStateToProps, mapDispatchToProps)(MediaUrl);
