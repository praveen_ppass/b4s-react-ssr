export const FETCH_MEDIA_URL_REQUESTED = "FETCH_MEDIA_URL_REQUESTED";
export const FETCH_MEDIA_URL_SUCCEEDED = "FETCH_MEDIA_URL_SUCCEEDED";
export const FETCH_MEDIA_URL_FAILED = "FETCH_MEDIA_URL_FAILED";

export const FETCH_SCHOLARSHIP_MEDIA_URL_REQUESTED =
  "FETCH_SCHOLARSHIP_MEDIA_URL_REQUESTED";
export const FETCH_SCHOLARSHIP_MEDIA_URL_SUCCEEDED =
  "FETCH_SCHOLARSHIP_MEDIA_URL_SUCCEEDED";
export const FETCH_SCHOLARSHIP_MEDIA_URL_FAILED =
  "FETCH_SCHOLARSHIP_MEDIA_URL_FAILED";

export const FETCH_SCHOLARSHIP_SLUG_REQUESTED =
  "FETCH_SCHOLARSHIP_SLUG_REQUESTED";
export const FETCH_SCHOLARSHIP_SLUG_SUCCEEDED =
  "FETCH_SCHOLARSHIP_SLUG_SUCCEEDED";
export const FETCH_SCHOLARSHIP_SLUG_FAILED = "FETCH_SCHOLARSHIP_SLUG_FAILED";
export const SUBMIT_MEDIA_URL_REQUESTED = "SUBMIT_MEDIA_URL_REQUESTED";
export const SUBMIT_MEDIA_URL_SUCCEEDED = "SUBMIT_MEDIA_URL_SUCCEEDED";
export const SUBMIT_MEDIA_URL_FAILED = "SUBMIT_MEDIA_URL_FAILED";

export const submitMeida = data => ({
  type: SUBMIT_MEDIA_URL_REQUESTED,
  payload: { inputData: data }
});
export const fetchScholarshipSlug = bsid => ({
  type: FETCH_SCHOLARSHIP_SLUG_REQUESTED,
  payload: { bsid: bsid }
});
export const receivesScholarshipSlug = payload => ({
  type: FETCH_SCHOLARSHIP_SLUG_SUCCEEDED,
  payload
});
export const fetchMedia = (title, bsid) => ({
  type: FETCH_MEDIA_URL_REQUESTED,
  payload: { title: title, bsid: bsid }
});

export const fetchScholarshipMedia = bsid => ({
  type: FETCH_SCHOLARSHIP_MEDIA_URL_REQUESTED,
  payload: { bsid: bsid }
});

export const receivesMedia = payload => ({
  type: FETCH_MEDIA_URL_SUCCEEDED,
  payload
});
