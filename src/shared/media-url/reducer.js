import {
  FETCH_MEDIA_URL_SUCCEEDED,
  FETCH_MEDIA_URL_REQUESTED,
  FETCH_MEDIA_URL_FAILED,
  SUBMIT_MEDIA_URL_REQUESTED,
  SUBMIT_MEDIA_URL_SUCCEEDED,
  SUBMIT_MEDIA_URL_FAILED,
  FETCH_SCHOLARSHIP_SLUG_REQUESTED,
  FETCH_SCHOLARSHIP_SLUG_SUCCEEDED,
  FETCH_SCHOLARSHIP_SLUG_FAILED,
  FETCH_SCHOLARSHIP_MEDIA_URL_SUCCEEDED,
  FETCH_SCHOLARSHIP_MEDIA_URL_REQUESTED,
  FETCH_SCHOLARSHIP_MEDIA_URL_FAILED
} from "./actions";

const initialState = {
  mediaData: null,
  mediaSubmit: false,
  showLoader: true,
  isServerError: false,
  serverError: ""
};
const mediaUrlReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_MEDIA_URL_SUCCEEDED:
      return {
        mediaData: null,
        mediaSubmit: false,
        showLoader: false,
        type,
        mediaData: payload
      };
    case FETCH_MEDIA_URL_REQUESTED:
      return { ...state, payload, type, mediaSubmit: false };
    case FETCH_MEDIA_URL_FAILED:
      return {
        mediaData: null,
        type,
        mediaSubmit: false,
        showLoader: false,
        payload
      };

    case FETCH_SCHOLARSHIP_MEDIA_URL_SUCCEEDED:
      return {
        mediaUrlSchData: null,
        showLoader: false,
        type,
        mediaUrlSchData: payload
      };
    case FETCH_SCHOLARSHIP_MEDIA_URL_REQUESTED:
      return { ...state, payload, type, showLoader: true };
    case FETCH_SCHOLARSHIP_MEDIA_URL_FAILED:
      return {
        mediaUrlSchData: null,
        type,
        showLoader: false,
        isServerError: true,
        payload
      };

    case SUBMIT_MEDIA_URL_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        mediaSubmit: false,
        isServerError: false,
        serverError: ""
      };

    case SUBMIT_MEDIA_URL_SUCCEEDED:
      return { ...state, type, showLoader: false, mediaSubmit: true };

    case SUBMIT_MEDIA_URL_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        mediaSubmit: false,
        isServerError: true,
        serverError: payload
      };
    case FETCH_SCHOLARSHIP_SLUG_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        slug: false,
        isServerError: false,
        serverError: ""
      };

    case FETCH_SCHOLARSHIP_SLUG_SUCCEEDED:
      return { ...state, type, showLoader: false, slug: payload.data.slug };

    case FETCH_SCHOLARSHIP_SLUG_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        slug: false,
        isServerError: true,
        serverError: payload
      };
    default:
      return state;
  }
};
export default mediaUrlReducer;
