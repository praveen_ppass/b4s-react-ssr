import React, { Component } from "react";
import Cropper from "react-cropper";
if (typeof window !== "undefined") {
  require("cropperjs/dist/cropper.css");
}
import { UPLOAD_USER_PIC_SUCCESS } from "../../dashboard/actions";

class DashboardUI extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedFile: null,
      src: null,
      cropResult: null,
      showPopUp: false
    };

    this.cropImage = this.cropImage.bind(this);
    this.dataURLtoFile = this.dataURLtoFile.bind(this);
    this.closePopUp = this.closePopUp.bind(this);
    this.uploadHandler = this.uploadHandler.bind(this);
    // this.useDefaultImage = this.useDefaultImage.bind(this);
  }

  uploadHandler() {
    const formData = new FormData();
    const { cropResult } = this.state;

    formData.append("docFile", cropResult);

    this.props.uploadPic({ userId: this.props.userId, formData: formData });
    this.setState(
      {
        showPopUp: false,
        selectedFile: null
      },
      () => this.props.clearSelectedFile()
    );
  }

  dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(","),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }

  componentWillReceiveProps(nextProps) {
    const { selectedFile, type } = nextProps;

    if (selectedFile) {
      this.setState({
        showPopUp: true
      });
    } else {
      this.setState({
        showPopUp: false
      });
    }
  }

  cropImage() {
    if (typeof this.cropper.getCroppedCanvas() === "undefined") {
      return;
    }

    const { selectedFile } = this.props;

    let customizeFileName =
      this.props.selectedFile.name.substr(
        0,
        this.props.selectedFile.name.lastIndexOf(".")
      ) +
      this.props.selectedFile.name
        .substr(this.props.selectedFile.name.lastIndexOf("."))
        .toLowerCase();

    var file = this.dataURLtoFile(
      this.cropper
        .getCroppedCanvas({ width: 200, height: 200 })
        .toDataURL("image/jpeg", 0.8),
      customizeFileName
    );

    this.setState(
      {
        cropResult: file
      },
      () => this.uploadHandler()
    );
  }

  closePopUp() {
    this.setState({
      showPopUp: false
    });
  }

  render() {
    const { src } = this.props;

    return this.state.showPopUp ? (
      <section>
        <section className="cropImg">
          <section className="cropImgbg">
            <article className="popup-header">
              <button
                onClick={() => this.closePopUp()}
                type="button"
                className="close"
              >
                ×
              </button>
              <h3 className="popup-title">CROP IMAGE</h3>
            </article>
            <article className="popup-body">
              <article className="row">
                <article className="col-md-12">
                  <article className="cropArea">
                    <Cropper
                      style={{ height: "50vh", width: "100%" }}
                      aspectRatio={1}
                      zoomable={true}
                      autoCropArea={1}
                      preview=".img-preview"
                      guides={false}
                      src={src}
                      ref={cropper => {
                        this.cropper = cropper;
                      }}
                    />
                  </article>
                </article>
              </article>
              <article className="row">
                <article className="col-md-12 text-center">
                  <button
                    className="updatePicBtn"
                    onClick={() => this.cropImage()}
                  >
                    Update Picture
                  </button>
                </article>
              </article>
            </article>
            <article className="popup-footer" />
          </section>
        </section>
      </section>
    ) : // <CropImagePopUP
    //   src={src}
    //   cropper={this.cropper}
    //   cropImage={this.cropImage}
    //   cancelPopUp={this.closePopUp}
    // />
    null;
  }
}

export default DashboardUI;
