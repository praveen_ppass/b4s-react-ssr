import React from "react";
import { Link } from "react-router-dom";
import gblFunc from "../../../globals/globalFunctions";

const DashboardNav = ({ dashNav, scholarFlag }) => {
  const cscUsers = gblFunc.getStoreUserDetails();
  return (
    <ul className="optional-links">
      <li
        className={`myprofile-tab ${dashNav == "My Profile" ? "active" : ""}`}
      >
        {cscUsers.cscId && cscUsers.vleUser === "true" ? (
          <Link to="/csc/myprofile">My Profile</Link>
        ) : (
            <Link to="/myprofile/PersonalInfo">My Profile</Link>
          )}
      </li>
      {cscUsers.cscId && cscUsers.vleUser === "true" ? null : (
        <li
          className={`myscholarship-tab ${
            dashNav == "My Scholarships" ? "active" : ""
            }`}
        >
          <Link
            to={{
              pathname: "/myscholarship",
              state: { switchTab: "MatchedScholarships" }
            }}
          >
            My Scholarships
          </Link>
        </li>
      )}
      {cscUsers && cscUsers.vleUser === "true" ? (
        <li
          className={`mysubscriber-tab ${
            dashNav == "mysubscriber-tab" ? "active" : ""
            }`}
        >
          <Link to="/subscribers">My Subscribers</Link>
        </li>
      ) : (
          ""
        )}
      {/* <li className={`scholar-tab ${dashNav == "scholar-tab" ? "active" : ""}`}>
        <Link to="/scholar">Scholar</Link>
      </li> */}
    </ul>
  );
};

export default DashboardNav;
