import React, { Component } from "react";
import Input from "../user-interface/Input/Input";
import { formInputs } from "../../../constants/formInput";
import { ruleRunner } from "../../../validation/ruleRunner";
import {
  required,
  minLength,
  isEmail,
  isNumeric,
  shouldMatch,
  isMobileNumber
} from "../../../validation/rules";

class NeedHelp extends Component {
  constructor(props) {
    super(props);
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this.inputChangedHandler = this.inputChangedHandler.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.state = {
      formInputs,
      isFormValid: false,
      validations: {
        NAME: null,
        EMAIL: null,
        MOBILE: null
      }
    };
  }

  onSubmitHandler(event) {
    event.preventDefault();
    if (this.checkFormValidations()) {
      let submitContactParams = {};
      for (let key in this.state.formInputs.need_help) {
        submitContactParams[key] = this.state.formInputs.need_help[key].value;
      }
      this.props.submitContact(submitContactParams);
    }
  }

  componentWillReceiveProps(nextProps) {}

  inputChangedHandler(event, inputIdentifier) {
    const { value } = event.target;
    let validations = { ...this.state.validations };
    this.state.formInputs.need_help[inputIdentifier].value = value;

    if (inputIdentifier !== "COMMENT") {
      const { name, validationFunctions } = this.getValidationRulesObject(
        inputIdentifier
      );
      const validationResult = ruleRunner(
        value,
        inputIdentifier,
        name,
        ...validationFunctions
      );

      validations[inputIdentifier] = validationResult[inputIdentifier];
    }

    this.setState({ ...this.state, validations });
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state.formInputs.need_help[key].value,
        key,
        name,
        ...validationFunctions
      );

      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "EMAIL":
        validationObject.name = "*Email address";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;
      case "NAME":
        validationObject.name = "*Name";
        validationObject.validationFunctions = [required, minLength(3)];
        return validationObject;
      case "MOBILE":
        validationObject.name = "*Mobile number";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10, 10)
        ];
        return validationObject;
      default:
        return validationObject;
    }
  }

  render() {
    let formElementsArray = [];

    for (let key in this.state.formInputs.need_help) {
      formElementsArray.push({
        id: key,
        config: this.state.formInputs.need_help[key]
      });
    }

    let form = (
      <form
        name="contactUsForm"
        autoCapitalize="off"
        onSubmit={event => this.onSubmitHandler(event)}
      >
        <article className="ctrl-wrapper">
          {formElementsArray.map(formElement => (
            <Input
              key={formElement.id}
              label={formElement.config.label}
              classes={formElement.config.classes}
              elementType={formElement.config.elementType}
              elementConfig={formElement.config.elementConfig}
              value={formElement.config.value}
              hide={formElement.config.hide}
              validations={this.state.validations[formElement.id]}
              changed={event => this.inputChangedHandler(event, formElement.id)}
            />
          ))}
          <button type="submit">Submit</button>
        </article>
      </form>
    );

    return (
      <section className="overlayNH">
        <section className="nh-winPopup">
          <i className="nh-closeBtn" onClick={this.props.onClose} />
          <h5>Please submit your query</h5>
          {form}
        </section>
      </section>
    );
  }
}

export default NeedHelp;
