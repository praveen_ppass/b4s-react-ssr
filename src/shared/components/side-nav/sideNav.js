import React from "react";
import { Link, Redirect } from "react-router-dom";
//import { sideTabList } from "../../../constants/sideNav";
//import gblFunc from "../../../globals/globalFunctions";

const SideNav = props => {
	if (typeof window !== "undefined" && window.localStorage.getItem("isAuth") !== "1") {
		return (<Redirect to="/" />)
	}
	return (
		<article className="page-nav listNav page-nav-scholarships">
			{/* Start My Profile Navigation */}
			<SideNavLists
				//page={props.currentPage}
				//tab={props.tab}
				//sideTabClick={event => props.sideTabFormHandler(event)}
				//sideTabName={props.sideNavTabName}
				tabToggleHandler={props.tabToggleHandler}
				isTabToggle={props.isTabToggle}
				isDocumentIssue={props.isDocumentIssue}
				urlPush={props.urlPush}
			/>
		</article>
	);
};

const SideNavLists = ({ page, tab, sideTabClick, sideTabName, tabToggleHandler, isTabToggle, isDocumentIssue, urlPush }) => {
	/* let sideList = null;
	const cscUsers = gblFunc.getStoreUserDetails();
	let cscNavTab = [
		{ name: "Downloads", id: "CscDownloads", classes: "download" },
		{ name: "Tutorials", id: "CscDemo", classes: "demo" }
	]; */

	/* if (cscUsers.cscId && cscUsers.vleUser === "true" && tab === "profile") {
		sideList = [
			{ name: "My Profile", id: "MyProfile", classes: "myprofile" }
		];
	} else {
		sideList =
			sideTabList[page] && sideTabList[page][tab]
				? sideTabList[page][tab]
				: null;
	} */

	/* if (cscUsers.cscId && cscUsers.vleUser === "true" && tab === "mySubscriber") {
		sideList = sideList.concat(cscNavTab);
	} */

	/* if (cscUsers.cscId && cscUsers.vleUser === "true") {
		// sideList.splice(1, sideList.length - 1); //Have only one tab personal info
		sideList.forEach((list, index) =>
			list.id === "PersonalInfo"
				? (sideList[index].id = "CscPersonalInfo")
				: list.id
		);
	} */

	return (
		<ul className={`listNav listNavdashboardApp ${isTabToggle ? "mobo" : ""}`}>
			<li className={`My-Profile ${(typeof window !== 'undefined' && location.pathname.includes('PersonalInfo') || typeof window !== 'undefined' && location.pathname.includes('UpdateEmail') || typeof window !== 'undefined' && location.pathname.includes('UpdateMobile')) ? "active" : ""}`}>
				<Link
					onClick={() => [
						props.gtmEventHandler(["SideNav", 'My Profile']),
						urlPush("PersonalInfo")]
					}
					to="/myProfile/PersonalInfo"
				>My Profile</Link>
			</li>
			<li className={`matched-scholarships ${typeof window !== 'undefined' && location.pathname.includes('MatchedScholarships') ? "active" : ""}`}>
				<Link
					onClick={() => [
						props.gtmEventHandler(["SideNav", 'Matched Scholarships']),
						urlPush("MatchedScholarships")]
					}
					to="/myProfile/MatchedScholarships"
				>Matched Scholarships</Link>
			</li>
			<li className={`applied-scholarships ${(typeof window !== 'undefined' && location.pathname.includes('ApplicationStatus') || typeof window !== 'undefined' && location.pathname.includes('DocumentIssues')) ? "active" : ""}`}>
				<Link
					onClick={() => [
						props.gtmEventHandler(["SideNav", 'Applied Scholarships']),
						urlPush("ApplicationStatus")]
					}
					to="/myProfile/ApplicationStatus"
				><span>Applied Scholarships {!!isDocumentIssue == true && (<i className="blink"></i>)}</span></Link>
			</li>
			<li className={`awarded-scholarships ${typeof window !== 'undefined' && location.pathname.includes('AwardeesScholarships') ? "active" : ""}`}>
				<Link
					onClick={() => [
						props.gtmEventHandler(["SideNav", 'Awarded Scholarships']),
						urlPush("AwardeesScholarships")]
					}
					to="/myProfile/AwardeesScholarships"
				>Awarded Scholarships</Link>
			</li>
			<li className={`my-fav ${typeof window !== 'undefined' && location.pathname.includes('MyFavorites') ? "active" : ""}`}>
				<Link
					onClick={() => [
						props.gtmEventHandler(["SideNav", 'My Favorites']),
						urlPush("MyFavorites")]
					}
					to="/myProfile/MyFavorites"
				>My Favorites</Link>
			</li>
			<li className={`questions-answers ${typeof window !== 'undefined' && location.pathname.includes('QuestionsAndAnswers') ? "active" : ""}`}>
				<Link
					onClick={() => [
						props.gtmEventHandler(["SideNav", 'Questions & Answers']),
						urlPush("QuestionsAndAnswers")]
					}
					to="/myProfile/QuestionsAndAnswers"
				>Questions & Answers</Link>
			</li>

			<span className="signSymbol" onClick={tabToggleHandler}>
				<i className="line-one"></i>
				{!isTabToggle && <i className="line-two"></i>}
			</span>
		</ul>
	);
};

export default SideNav;
