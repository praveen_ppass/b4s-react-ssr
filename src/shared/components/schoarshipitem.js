import React from "react";
import moment from "moment";
import { Link } from "react-router-dom";
import gblFunc from "../../globals/globalFunctions";
import { imgBaseUrl, defaultImageLogo } from "../../constants/constants";

const ScholarshipItem = props => {
  return props.scholarshipList.map((item, index) => {
    let daysToGO;
    //let viewCounter;
    // if (props.activeScholarship) {

    daysToGO =
      parseInt(
        moment(item.deadlineDate, "YYYY-MM-DD").diff(
          moment().startOf("day"),
          "days"
        )
      ) + 1;
    // }

    let daysLabel;
    if (daysToGO === 1) {
      daysLabel = "Last";
    } else if (daysToGO === 2) {
      daysLabel = "1";
    } else {
      daysLabel = daysToGO;
    }

    /* if (
      item.scholarshipMultilinguals &&
      item.scholarshipMultilinguals[0] &&
      item.scholarshipMultilinguals[0].viewCounter
    ) {
      viewCounter = item.scholarshipMultilinguals[0].viewCounter;
    } */

    return (
      <article className="box" key={index}>
        <article className="data-row flex-container">
          <article className="col-md-2 col-sm-12 flex-item">
            <article className="tablecell">
              <article className="logotext">
                <Link to={`/scholarship/${item.slug}`}>
                  <img
                    src={item.logoFid ? item.logoFid : defaultImageLogo}
                    onError={e => {
                      e.target.src = defaultImageLogo;
                    }}
                    className="img-responsive"
                    alt={item.scholarshipName}
                  />
                </Link>
                {/* <p>
                  {parseInt(viewCounter)
                    ? `${
                    parseInt(viewCounter) > 1
                      ? parseInt(viewCounter)
                      : "view"
                    } views`
                    : 0 + "view"}
                </p> */}
              </article>
            </article>
          </article>
          <article className="col-md-7 col-sm-9 flex-item">
            <article className="tablecell">
              <article className="contentdisplay">
                <h2>
                  <Link to={`/scholarship/${item.slug}`} className="ellipsis">
                    <span
                      dangerouslySetInnerHTML={{
                        __html: item.scholarshipName
                          ? gblFunc.replaceWithLoreal(item.scholarshipName)
                          : ""
                      }}
                    />
                  </Link>
                  <article className="namelistTooltip">
                    <span
                      dangerouslySetInnerHTML={{
                        __html: item.scholarshipName
                          ? gblFunc.replaceWithLoreal(item.scholarshipName)
                          : ""
                      }}
                    />
                    <i className="arrow" />
                  </article>
                </h2>
                <p>
                  <img
                    src={`${imgBaseUrl}scholarship-icon.png`}
                    alt="scholarship-icon"
                  />
                  {item.scholarshipMultilinguals &&
                    item.scholarshipMultilinguals.length > 0 &&
                    item.scholarshipMultilinguals[0].applicableFor}
                </p>
                <p>
                  <img src={`${imgBaseUrl}awards-icon.png`} alt="awards-icon" />
                  {item.scholarshipMultilinguals &&
                    item.scholarshipMultilinguals.length > 0 &&
                    item.scholarshipMultilinguals[0].purposeAward}
                </p>
              </article>
            </article>
          </article>
          {daysToGO >= 1 ? (
            item.deadlineDate ? (
              <article className="col-md-3 col-sm-3 flex-item">
                <article className="tablecell">
                  {daysToGO < 17 ? (
                    <article
                      className={daysToGO < 9 ? "daysgo pink" : "daysgo yellow"}
                    >
                      <span>{daysLabel}</span>
                      <p>{`day${daysToGO > 2 ? "s" : ""} to go`}</p>
                    </article>
                  ) : (
                      <article className="calender">
                        <p className="text-center">Last Date to apply</p>
                        <dd>{moment(item.deadlineDate).format("D")}</dd>
                        <p className="text-center date">
                          {moment(item.deadlineDate).format("MMM, YY")}
                        </p>
                      </article>
                    )}
                </article>
              </article>
            ) : (
                ""
              )
          ) : item.deadlineDate ? (
            <article className="col-md-3 col-sm-3 flex-item">
              <article className="tablecell">
                <article className="daysgo gray">
                  <span>Closed</span>
                </article>
              </article>
            </article>
          ) : (
                <article className="col-md-3 col-sm-3 flex-item">
                  <article className="tablecell">
                    <article className="daysgo pink always">
                      <p>Always Open</p>
                    </article>
                  </article>
                </article>
              )}
        </article>
      </article>
    );
  });
};

// const ScholarshipTimeLine = props => {
//   props.activeScholarship ? (
//     item.DEADLINE ? (
//       <article className="col-md-3 pull-right">
//         <article className="left" />
//         {daysToGO < 16 ? (
//           <article className={daysToGO < 8 ? "daysgo" : "always"}>
//             <span>{daysToGO}</span>
//             <p>days to go</p>
//           </article>
//         ) : (
//           <article className="calender">
//             <p className="text-center">Last Date to apply</p>
//             <dd>{daysToGO}</dd>
//             <p className="text-center date">{item.DEADLINE}</p>
//           </article>
//         )}
//       </article>
//     ) : (
//       ""
//     )
//   ) : (
//     <article className="col-md-3 pull-right">
//       <article className="left" />
//       <article className="closed">
//         <span>Closed</span>
//       </article>
//     </article>
//   );
// };

export default ScholarshipItem;
