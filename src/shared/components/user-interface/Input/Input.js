import React, { Fragment } from "react";

const input = props => {
  let inputElement = null;
  let lablelShow = null;
  let spanShow = null;
  switch (props.elementType) {
    case "input":
      inputElement = (
        <input
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      );
      break;
    case "textarea":
      inputElement = (
        <textarea
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      );
      break;
    case "select":
      inputElement = (
        <select
          value={props.value}
          className={props.elementConfig.className}
          onChange={props.changed}
        >
          {props.elementConfig.optionTitle ? (
            <option value="">{props.elementConfig.optionTitle}</option>
          ) : null}
          {props.elementConfig.options.map(rule => (
            <option key={rule.ID} value={rule.ID}>
              {rule.RULEVALUE}
            </option>
          ))}
        </select>
      );
      break;
    default:
      inputElement = (
        <input
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      );
  }
  if (props.hide && props.hide.label) {
    lablelShow = <label className={props.label}>{props.label}</label>;
  } else {
    lablelShow = null;
  }

  return (
    <article className={props.classes.join(" ")}>
      <article className="form-group">
        {lablelShow}
        {inputElement}
        {props.validations ? (
          <span className="error animated bounce error1">
            {props.validations}
          </span>
        ) : null}
      </article>
    </article>
  );
};

export default input;
