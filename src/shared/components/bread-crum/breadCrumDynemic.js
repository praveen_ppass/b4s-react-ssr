import React from "react";
import { Link } from "react-router-dom";

export const BreadCrumDynemic = ({ list, classes }) => {
  return (
    <section className="container-fluid brandPageRowBg">
      <section class="container">
        <section class="row">
          <section className="col-md-12">
            <ul className="breadcrumb">
              {list.length
                ? list.map(b => (
                    <li key={b.pageName}>
                      <Link to={b.pageUrl || "#"}>{b.pageName}</Link>
                    </li>
                  ))
                : ""}
            </ul>
          </section>
        </section>
      </section>
    </section>
  );
};
