import React from "react";
import { Link } from "react-router-dom";

const breacrumList = props => {
  return (
    <ul className="col-md-12">
      <li key="Home">
        <Link to="/">Home</Link>
      </li>
      {props.breadCrumArray.map(list => {
        return (
          <li key={list.name}>
            <Link to={list.url}> {list.name}</Link>
          </li>
        );
      })}
    </ul>
  );
};

export default breacrumList;
