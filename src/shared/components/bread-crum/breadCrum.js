import React from "react";
import BreadCrumList from "./bread-crum-list/breadCrumList";

const breadCrum = props => {
  let hideBanner = null;
  function createMarkup() {
    return { __html: props.title };
  }

  function createSubTitleMarkup() {
    return { __html: props.subTitle };
  }

  if (props.hideBanner) {
    hideBanner = null;
  } else {
    hideBanner = (
      <section className="row marginEquals">
        <article className="col-md-12">
          <article className="middleComponent">
            <article className={props.classes.join(" ")}>
              {props.title ? (
                <h1 dangerouslySetInnerHTML={createMarkup()} />
              ) : (
                  ""
                )}
              <p
                className="careerscontentWrapper"
                dangerouslySetInnerHTML={createSubTitleMarkup()}
              />
            </article>
          </article>
        </article>
      </section>
    );
  }

  return (
    <div>
      {hideBanner}
      {/* breacrum trail */}
      {props.listOfBreadCrum.length > 0 ? (
        <section className="bggreay">
          <article className="container">
            <article className="row">
              <BreadCrumList breadCrumArray={props.listOfBreadCrum} />
            </article>
          </article>
        </section>
      ) : null}
    </div>
  );
};

export default breadCrum;
