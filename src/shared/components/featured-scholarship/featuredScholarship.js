import React from "react";
import { Link } from "react-router-dom";
import gblFunc from "../../../globals/globalFunctions";
import moment from "moment";
const FeaturedScholarships = props => {
  const { featured, classes, headerTitle, fromListPage } = props;

  if (fromListPage) {
    let premiumScholarships = featured.filter(
      scholarship => scholarship.status == 1 && scholarship.slug != ""
    );

    return (
      <article className="box listFeaturedScholarships">
        <h4>{headerTitle}</h4>
        {premiumScholarships.map(scholarship => (
          <p key={scholarship.id}>
            <Link
              to={`/scholarship/${
                scholarship.slug
              }?utm_source=FeaturedList&utm_medium=WebDetailPage`}
            >
              <img
                property="image"
                src={scholarship.logoFid}
                alt="buddy4study-features"
              />
            </Link>
            <span>
              <Link
                to={`/scholarship/${
                  scholarship.slug
                }?utm_source=FeaturedList&utm_medium=WebDetailPage`}
                dangerouslySetInnerHTML={{
                  __html: scholarship.scholarshipName
                    ? gblFunc.replaceWithLoreal(scholarship.scholarshipName)
                    : ""
                }}
              />
              <i>
                {moment(scholarship.deadlineDate, "YYYY-MM-DD").format(
                  "DD-MMM-YYYY"
                )}
              </i>
            </span>
          </p>
        ))}
      </article>
    );
  } else {
    return (
      <article className={classes.join(" ")}>
        <article className="cellBox-mobo">
          <article>
            <dd>{headerTitle}</dd>
            {featured && featured.length > 0
              ? featured
                  .filter(scholarship => {
                    return scholarship.status == 1 && scholarship.slug != "";
                  })
                  .map(scholarship => (
                    <ScholarshipInfo
                      key={scholarship.id}
                      scholarship={scholarship}
                      fromListPage={fromListPage}
                    />
                  ))
              : null}
          </article>
        </article>
      </article>
    );
  }
};

const ScholarshipInfo = props => {
  return (
    <p>
      <Link
        to={`/scholarship/${
          props.scholarship.slug
        }?utm_source=FeaturedList&utm_medium=WebDetailPage`}
      >
        <img
          property="image"
          src={props.scholarship.logoFid}
          alt="buddy4study-features"
        />
      </Link>

      <span>
        <Link
          to={`/scholarship/${
            props.scholarship.slug
          }?utm_source=FeaturedList&utm_medium=WebDetailPage`}
          dangerouslySetInnerHTML={{
            __html: props.scholarship.scholarshipName
              ? gblFunc.replaceWithLoreal(props.scholarship.scholarshipName)
              : ""
          }}
        />
        <i>
          {moment(props.scholarship.deadlineDate, "YYYY-MM-DD").format(
            "DD-MMM-YYYY"
          )}
        </i>
      </span>
    </p>
  );
};

export default FeaturedScholarships;
