import React from "react";
const ValidationError = ({ fieldValidation }) => {
  return fieldValidation ? (
    <span className="error animated bounce">{fieldValidation}</span>
  ) : null;
};
export default ValidationError;
