import React, { Component } from "react";
import querystring from "query-string";
import { Link } from "react-router-dom";
import Loader from "../../common/components/loader";
import { ruleRunner } from "../../../validation/ruleRunner";
import {
  required,
  isEmail,
  minLength,
  isNumeric,
  isMobileNumber
} from "../../../validation/rules";
import AlertMessage from "../../common/components/alertMsg";
import {
  SUBMIT_EDUCATIONLOAN_SUCCEEDED,
  SUBMIT_EDUCATIONLOAN_FAILED
} from "../../education-loan/actions";

import { messages, imgBaseUrl, utmSources } from "../../../constants/constants";
class EducationLoanPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      admissionSecured: "",
      comment: "",
      email: "",
      familyIncome: "",
      fullName: "",
      interestedIn: "",
      mobile: "",
      orgId: 0,
      status: "",
      utmSource: "",
      willJoin: "",
      isShowAlert: false,
      statusAlert: false,
      alertMsg: "",
      validations: {
        fullName: null,
        email: null,
        mobile: null,
        willJoin: null,
        admissionSecured: null,
        familyIncome: null,
        interestedIn: null
      }
    };

    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.submitEducation = this.submitEducation.bind(this);
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
  }

  hideAlert() {
    //  close popup

    this.setState({
      isShowAlert: false
    });
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "fullName":
        validationObject.name = "* Name";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "email":
        validationObject.name = "* Email";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;
      case "mobile":
        validationObject.name = "* Mobile";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;
      case "willJoin":
        validationObject.name = "* Will join";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "admissionSecured":
        validationObject.name = "* Admission secured";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "familyIncome":
        validationObject.name = "* Family income";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "interestedIn":
        validationObject.name = "* Will study";
        validationObject.validationFunctions = [required];
        return validationObject;
    }
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );

      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  submitEducation(event) {
    event.preventDefault();
    const {
      fullName,
      email,
      mobile,
      willJoin,
      admissionSecured,
      familyIncome,
      interestedIn,
      utmSource
    } = this.state;

    if (this.checkFormValidations()) {
      let apiParams = {
        fullName,
        email,
        mobile,
        willJoin,
        admissionSecured,
        familyIncome,
        interestedIn
      };
      //Don't add key if not present in utm source
      if (utmSource) {
        apiParams.utmSource = utmSource;
      }

      this.props.submitEducationLoad(apiParams);
    }
  }

  onFormFieldChange(event) {
    const { id, value } = event.target;
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );
    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];
    this.setState({
      [id]: value,
      validations
    });
  }

  componentDidMount() {
    if (this.props.location.search) {
      const parsedQueryString = querystring.parse(this.props.location.search);
      if (parsedQueryString.utm_source) {
        this.setState({
          utmSource: parsedQueryString.utm_source
        });
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps.educationLoanData;
    switch (type) {
      case SUBMIT_EDUCATIONLOAN_SUCCEEDED:
        const parsedQueryString = querystring.parse(this.props.location.search);

        if (
          parsedQueryString.utm_source &&
          parsedQueryString.utm_source.toLowerCase() === utmSources.GET_MY_UNI
        ) {
          var s = document.createElement("img");
          s.src = "https://www.getmyuni.com/pixel?client_id=27";
          (s.height = "1"), (s.width = "1"), (s.style = "display: none");

          try {
            document.body.appendChild(s);
          } catch (e) { }
        } else if (
          parsedQueryString.utm_campaign &&
          parsedQueryString.utm_campaign.toLowerCase() === utmSources.SVG &&
          this.state.familyIncome === "more than 2.5L"
        ) {
          var s = document.createElement("script");
          s.type = "text/javascript";
          s.src =
            "https://www.s2d6.com/js/globalpixel.js?x=sp&a=2234&h=71148&o=" +
            this.state.mobile +
            "&g=&s=0.00&q=1";
          try {
            document.body.appendChild(s);
          } catch (e) { }
        }
        this.setState({
          fullName: "",
          email: "",
          mobile: "",
          willJoin: "",
          admissionSecured: "",
          familyIncome: "",
          interestedIn: "",
          statusAlert: true,
          alertMsg: messages.educationLoan.success,
          isShowAlert: true
        });
        break;

      case SUBMIT_EDUCATIONLOAN_FAILED:
        this.setState({
          fullName: "",
          email: "",
          mobile: "",
          willJoin: "",
          admissionSecured: "",
          familyIncome: "",
          interestedIn: "",
          statusAlert: false,
          alertMsg: nextProps.educationLoanData.educationError,
          isShowAlert: true
        });
        break;
    }
  }

  render() {
    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          isShow={this.state.isShowAlert}
          msg={this.state.alertMsg}
          close={this.hideAlert}
          status={this.state.statusAlert}
        />
        <section>
          <section className="container-fluid eduTopBg">
            <section className="container containerWidth positionRelative containerHeight">
              <Link to="/">
                <img
                  src={`${imgBaseUrl}logo-b4s.png`}
                  alt="buddy4study-logo"
                  className="eduLogo"
                />
              </Link>
              <img
                src={`${imgBaseUrl}graphicObj.png`}
                alt="buddy4study-logo"
                className="graphicsObj"
              />
              <section className="eduFormWrapper">
                <form name="loanEduForm">
                  <p>
                    If you require further information, please leave your
                    details and we will get in touch with you
                  </p>
                  <article className="row positionRelative">
                    <input
                      type="text"
                      name="fullName"
                      placeholder="Name"
                      id="fullName"
                      onChange={this.onFormFieldChange}
                      value={this.state.fullName}
                    />
                    {this.state.validations.fullName ? (
                      <i className="error">{this.state.validations.fullName}</i>
                    ) : null}
                  </article>
                  <article className="row positionRelative">
                    <input
                      type="email"
                      name="email"
                      placeholder="Email ID"
                      id="email"
                      onChange={this.onFormFieldChange}
                      value={this.state.email}
                    />
                    {this.state.validations.email ? (
                      <i className="error">{this.state.validations.email}</i>
                    ) : null}
                  </article>
                  <article className="row positionRelative">
                    <input
                      type="text"
                      name="mobile"
                      placeholder="Contact Number"
                      id="mobile"
                      maxLength="10"
                      onChange={this.onFormFieldChange}
                      value={this.state.mobile}
                    />
                    {this.state.validations.mobile ? (
                      <i className="error">{this.state.validations.mobile}</i>
                    ) : null}
                  </article>

                  <article className="row positionRelative">
                    <article>
                      <label>Will join</label>
                      <dd className="select-container">
                        <span className="select-arrow" />
                        <select
                          name="willJoin"
                          id="willJoin"
                          onChange={this.onFormFieldChange}
                          value={this.state.willJoin}
                        >
                          <option value="" defaultValue="selected">
                            --Select--
                          </option>
                          <option value="Undergraduate"> Undergraduate </option>
                          <option value="Postgraduate"> Postgraduate </option>
                        </select>
                        {this.state.validations.willJoin ? (
                          <i className="error">
                            {this.state.validations.willJoin}
                          </i>
                        ) : null}
                      </dd>
                    </article>
                    <article>
                      <label>Admission secured</label>
                      <dd className="select-container">
                        <span className="select-arrow" />
                        <select
                          name="admissionSecured"
                          id="admissionSecured"
                          onChange={this.onFormFieldChange}
                          value={this.state.admissionSecured}
                        >
                          <option value="" defaultValue="selected">
                            --Select--
                          </option>
                          <option value="Yes"> Yes </option>
                          <option value="No"> No </option>
                        </select>
                        {this.state.validations.admissionSecured ? (
                          <i className="error">
                            {this.state.validations.admissionSecured}
                          </i>
                        ) : null}
                      </dd>
                    </article>
                  </article>
                  <article className="row">
                    <article>
                      <label>Family income</label>
                      <dd className="select-container">
                        <span className="select-arrow" />
                        <select
                          name="familyIncome"
                          id="familyIncome"
                          onChange={this.onFormFieldChange}
                          value={this.state.familyIncome}
                        >
                          <option value="" defaultValue="selected">
                            --Select--
                          </option>
                          <option value="more than 2.5L">more than 2.5L</option>
                          <option value="Less than 2.5L">
                            {" "}
                            Less than 2.5L{" "}
                          </option>
                        </select>
                        {this.state.validations.familyIncome ? (
                          <i className="error">
                            {this.state.validations.familyIncome}
                          </i>
                        ) : null}
                      </dd>
                    </article>
                    <article>
                      <label>Will study </label>
                      <dd className="select-container">
                        <span className="select-arrow" />
                        <select
                          name="interestedIn"
                          id="interestedIn"
                          onChange={this.onFormFieldChange}
                          value={this.state.interestedIn}
                        >
                          <option value="" defaultValue="selected">
                            --Select--
                          </option>
                          <option value="India"> In India </option>
                          <option value="Abroad"> Abroad </option>
                        </select>
                        {this.state.validations.interestedIn ? (
                          <i className="error">
                            {this.state.validations.interestedIn}
                          </i>
                        ) : null}
                      </dd>
                    </article>
                  </article>
                  <button type="submit" onClick={this.submitEducation}>
                    Submit
                  </button>
                </form>
              </section>
            </section>
          </section>
          <section className="container-fluid paddingLR">
            <section className="container containerWidth paddingLR">
              <section className="row">
                <h1 className="titleText">WHY BUDDY4STUDY</h1>
                <section className="row eduyoutube">
                  <section className="col-md-7 paddingL">
                    <iframe
                      width="100%"
                      height="400"
                      src="https://www.youtube.com/embed/ApIhI2gC-n4"
                      frameBorder="0"
                      allow="autoplay; encrypted-media"
                      allowFullScreen=""
                    />
                  </section>
                  <section className="col-md-5 paddingL paddingR">
                    <article className="edustep">
                      <ul>
                        <li> 100% Financing loans up to 1.5 Crores </li>
                        <li>
                          {" "}
                          Attractive interest rates as low as 8 per cent{" "}
                        </li>
                        <li> Collateral Free loans* all Inclusive </li>
                        <li>
                          US Co-Signer Facility for Studying Abroad in the US
                        </li>
                        <li>
                          Zero Pre-Payment Fees* pay back your loan early
                          without worries
                        </li>
                        <li>
                          Fast-track processing get your loan before admission
                          or visa formalities
                        </li>
                      </ul>
                    </article>
                    <p>Terms and conditions Apply*</p>
                  </section>
                </section>
              </section>
            </section>
          </section>
          <section className="container-fluid eduTopBorder eduTopBorder paddingLR">
            <section className="container containerWidth paddingLR">
              <section className="row">
                <h2 className="titleText ">
                  BUDDY4STUDY – REDEFINING STUDENT FRIENDLY
                </h2>
                <article className="redefining-student-friendly-box">
                  <ul>
                    <li>
                      <dd>Student-First Approach</dd> – We understand the
                      student perspective and put your interest first
                    </li>
                    <li>
                      <dd>Customized Product</dd> – Loan terms as per your
                      unique requirements
                    </li>
                    <li>
                      <dd>Best Deal</dd> – We negotiate on your behalf to get
                      the best possible terms
                    </li>
                    <li>
                      <dd>Process Support</dd> – No need to navigate the
                      cumbersome process alone as we do it for you
                    </li>
                    <li>
                      <dd>Scholarship Opportunities</dd> – Find eligible
                      scholarships instantly and get application support
                    </li>
                  </ul>
                </article>
              </section>
            </section>
          </section>
          <section className="container-fluid eduTopBorder eduBottomBorder">
            <section className="container containerWidth paddingLR">
              <section className="row">
                <h2 className="titleText ">OUR PARTNERS</h2>
                <p className="textpara ">
                  Buddy4Study has partnered with many esteemed names in the
                  world of education finance to help students realize their
                  dream education.
                </p>
                <img
                  src={`${imgBaseUrl}partner-logos.png`}
                  alt="partner-logo"
                  className="partnerLogo img-responsive"
                />
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default EducationLoanPage;
