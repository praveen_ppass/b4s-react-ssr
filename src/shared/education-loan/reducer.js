import {
  SUBMIT_EDUCATIONLOAN_REQUESTED,
  SUBMIT_EDUCATIONLOAN_SUCCEEDED,
  SUBMIT_EDUCATIONLOAN_FAILED
} from "./actions";

const initialState = {
  showLoader: true,
  educationError: ""
};
const educationLoanReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SUBMIT_EDUCATIONLOAN_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        educationError: "",
        isError: false
      };
    case SUBMIT_EDUCATIONLOAN_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        educationError: "",
        isError: false
      };
    case SUBMIT_EDUCATIONLOAN_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        educationError: payload,
        isError: true
      };
    default:
      return state;
  }
};
export default educationLoanReducer;
