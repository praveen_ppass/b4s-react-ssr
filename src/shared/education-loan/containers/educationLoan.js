import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchMedia as fetchMediaAction } from "../actions";
import { submitEducation as submitEducationAction } from "../actions";
import EducationLoanPage from "../../education-loan/components/educationLoanPages";

const mapStateToProps = ({ educationLoan }) => ({
  educationLoanData: educationLoan
});
const mapDispatchToProps = dispatch => ({
  submitEducationLoad: data => dispatch(submitEducationAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(EducationLoanPage);
