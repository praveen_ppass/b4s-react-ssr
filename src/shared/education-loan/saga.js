import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  SUBMIT_EDUCATIONLOAN_REQUESTED,
  SUBMIT_EDUCATIONLOAN_SUCCEEDED,
  SUBMIT_EDUCATIONLOAN_FAILED
} from "./actions";

const educationRegisterApi = inputData => {
  return fetchClient.post(apiUrl.educationLoan, inputData).then(res => {
    return res.data;
  });
};

function* submitEducationRegister(input) {
  try {
    const response = yield call(educationRegisterApi, input.payload.inputData);

    yield put({
      type: SUBMIT_EDUCATIONLOAN_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    if (error.response.status === 409) {
      error.errorMessage = "Email id already exists.";
    }

    yield put({
      type: SUBMIT_EDUCATIONLOAN_FAILED,
      payload: error.errorMessage
    });
  }
}

export default function* educationLoanSaga() {
  yield takeEvery(SUBMIT_EDUCATIONLOAN_REQUESTED, submitEducationRegister);
}
