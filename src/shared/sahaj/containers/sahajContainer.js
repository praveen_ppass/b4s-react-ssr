import React from "react";
import { connect } from "react-redux";

import Sahaj from "../components/sahaj";

const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Sahaj);
