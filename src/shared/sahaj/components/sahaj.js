import React, { Component } from "react";
import { imgBaseUrl } from "../../../constants/constants";

class SAHAJ extends Component {
  constructor(props) {
    super(props);

    this.showPopup = this.showPopup.bind(this);
    this.hidePopup = this.hidePopup.bind(this);

    this.state = {
      onFirstLoad: true,
      isShowPopup: false
    };
  }

  componentDidMount() { }

  showPopup() {
    this.setState({
      isShowPopup: true
    });
  }
  hidePopup() {
    this.setState({
      isShowPopup: false
    });
  }

  render() {
    return (
      <section>
        <section className="container-fuild">
          <section className="container">
            <section className="row">
              <section className="col-md-12 cell-padding">
                <article className="text-center">
                  <a href="/">
                    <img
                      height="60"
                      src={`${imgBaseUrl}white-logo.png`}
                      alt="Buddy4study – Gateway to scholarship world"
                      id="logo-b4s"
                      className="logo"
                    />
                  </a>
                  <h2>INDIA'S LARGEST SCHOLARSHIP PLATFORM</h2>
                  <p>Making Education Affordable</p>
                </article>
                <article className="listtyoP">
                  <p>Start adding students and find them scholarships</p>
                  <ul>
                    <li>
                      We hand pick every scholarship listed on our portal for
                      you
                    </li>
                    <li>
                      You will only see scholarships you stand a chance to win
                    </li>
                    <li>
                      We will help you fill out the scholarship application
                      step-by-step
                    </li>
                    <li>
                      We track your applications so that you can apply for more
                      scholarships
                    </li>
                    <li>
                      You will never miss out on a matching scholarship again
                    </li>
                  </ul>
                  <article className="buttonWrapper">
                    <a href="#">Login with sahaj portal</a>

                    <button className="cscHelpVideo">Help</button>
                  </article>
                </article>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default SAHAJ;
