import { call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";
import {
  FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_SUCCESS,
  FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_FAILURE,
  FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_REQUEST
} from "./actions";

const scholarshipSlugApi = inputData => {
  return fetchClient
    .get(apiUrl.scholarshipSlugByBsid + "/" + inputData + "?bsid=true")
    .then(res => res.data);
};

function* fetchScholarshipSlugUrl(input) {
  try {
    const scholarshipDetails = yield call(
      scholarshipSlugApi,
      input.payload.bsid
    );

    yield put({
      type: FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_SUCCESS,
      payload: scholarshipDetails
    });
  } catch (error) {
    let payload;
    if (error.response.status === 404) {
      payload = { is404Error: true, errorMessage: error.errorMessage };
      yield put({
        type: FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_FAILURE,
        payload
      });
    } else {
      payload = { is404Error: false, errorMessage: error.errorMessage };
      yield put({
        type: FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_FAILURE,
        payload
      });
    }
  }
}

export default function* bookSlotSaga() {
  yield takeEvery(
    FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_REQUEST,
    fetchScholarshipSlugUrl
  );
}
