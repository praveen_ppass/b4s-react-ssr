import { connect } from "react-redux";

import RegistrationForm from "../components/registrationForm";
import { registerUser as registerUserAction } from "../../login/actions";

const mapStateToProps = ({ loginOrRegister }) => ({
  type: loginOrRegister.type,
  serverErrorRegister: loginOrRegister.serverErrorRegister,
  serverErrorLogin: loginOrRegister.serverErrorLogin
});

const mapDispatchToProps = dispatch => ({
  registerUser: inputData => dispatch(registerUserAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationForm);
