import { connect } from "react-redux";

import BookSlotInstructions from "../components/BookSlotInstructions";

import { fetchUserRules as fetchUserRulesAction } from "../../../constants/commonActions";
import { fetchBookSlotInstructions as fetchBookSlotInstructionsAction } from "../actions";
import { applicationStatus as applicationStatusAction } from "../../dashboard/actions";
import { logUserOut as logUserOutAction } from "../../login/actions";

import {
  fetchBookSlotDates as fetchBookSlotDatesAction,
  fetchBookSlot as fetchBookSlotInfoAction,
  updateBookingSlot as updateBookSlotAction,
  fetchPreferLang as fetchPreferLangAction,
} from "../../dashboard/actions";

const mapStateToProps = ({
  bookSlot,
  loginOrRegister,
  dashboard
}) => ({
  isAuthenticated: loginOrRegister.isAuthenticated,
  showLoader: bookSlot.showLoader,
  scholarshipDetails: bookSlot.scholarshipDetails,
  userLoginData: bookSlot.userLoginData,
  applicationStatus: bookSlot.applicationStatus,
  userRulesData: bookSlot.userRulesData,
  error: bookSlot.error,
  type: bookSlot.type,
  bookslotType: dashboard.type,
  bookSlotData: dashboard.bookSlotData,
  bookSlotType: dashboard.type,
  bookSlot: dashboard.bookSlot,
  preferLang: dashboard.preferredLang
});

const mapDispatchToProps = dispatch => ({
  fetchBookSlotInstructions: inputData =>
    dispatch(fetchBookSlotInstructionsAction(inputData)),
  fetchUserRules: inputData => dispatch(fetchUserRulesAction(inputData)),
  fetchApplicationStatus: inputData =>
    dispatch(applicationStatusAction(inputData)),
  logUserOut: inputData => dispatch(logUserOutAction(inputData)),
  fetchBookSlotDates: inputData => dispatch(fetchBookSlotDatesAction(inputData)),

  fetchBookSlotInfo: inputData => dispatch(fetchBookSlotInfoAction(inputData)),
  updatingBookSlot: inputData => dispatch(updateBookSlotAction(inputData)),
  fetchPreferLanguage: inputData => dispatch(fetchPreferLangAction(inputData))
});

// const mySpecialContainerCreator = connect(mapStateToProps, mapDispatchToProps);

// export const applicationInstructionContainer = mySpecialContainerCreator(
//   applicationInstructions
// );
// export const applicationFormContainer = mySpecialContainerCreator(
//   applicationForm
// );

export default connect(mapStateToProps, mapDispatchToProps)(
  BookSlotInstructions
);
