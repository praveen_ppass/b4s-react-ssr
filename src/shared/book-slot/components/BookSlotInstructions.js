import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import moment from "moment";
import AlertMessagePopup from "../../common/components/alertMsg";
import LoginRegisterSection from "./loginRegisterSection";
import BookSlotInfo from "./BookSlotInfo";
import Loader from "../../common/components/loader";
import {
  LOGIN_USER_SUCCEEDED,
  REGISTER_USER_SUCCEEDED,
  FORGOT_USER_PASSWORD_SUCCESS
} from "../../login/actions";
import { FETCH_USER_RULES_SUCCESS } from "../../../constants/commonActions";
import gblFunc from "../../../globals/globalFunctions";
import {
  FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_FAILURE,
  FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_SUCCESS
} from "../actions";

//book slot
import BookSlot from "../../dashboard/components/extras/book_slot";
import {
  FETCH_BOOK_SLOT_SUCCESS,
  UPDATE_BOOK_SLOT_SUCCESS,
  UPDATE_BOOK_SLOT_FAIL,
  FETCH_APPLICATION_STATUS_SUCCESS,
  FETCH_BOOK_SLOT_DATES_SUCCESS,
  FETCH_BOOK_SLOT_DATES_FAIL
} from "../../dashboard/actions";
import { ruleRunner } from "../../../validation/ruleRunner";
import {
  maxFileSize,
  hasValidImageExtenstion,
  required,
  isEmail,
  minLength,
  isNumeric,
  isMobileNumber
} from "../../../validation/rules";

import ServerError from "../../common/components/serverError";
import { messages } from "../../../constants/constants";

class BookSlotInstructions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: true,
      showBookSlotSection: false,
      redirectToMyScholarships: false,
      showFailBookSlotMessage: false,
      showAlertMessagePopup: false,
      alertMessage: "",
      statusAlert: true,
      bookSlotDate: "",
      isBookSlot: true,
      //book slot
      showBookSlot: false,
      ON_FIRST_LOAD: true,
      isFormValid: false,
      userId: null,
      scholarshipId: null,
      validations: {
        comments: null,
        scheduleDate: null,
        scheduleTime: null,
        mobile: null,
        prefferedLanguages: null
      },
      book_slot: {
        id: null,
        comments: "",
        scheduleDate: "",
        scheduleTime: "",
        mobile: "",
        prefferedLanguages: [],
        scholarshipId: null,
        userId: null
      },
      preferredLanguage: []
    };
    this.triggerRedirection = this.triggerRedirection.bind(this);
    this.showBookSlotSection = this.showBookSlotSection.bind(this);
    this.showFailBookSlotMessage = this.showFailBookSlotMessage.bind(this);
    this.logUserOut = this.logUserOut.bind(this);
    this.hideBookSlotSection = this.hideBookSlotSection.bind(this);
    this.showAlertMessagePopup = this.showAlertMessagePopup.bind(this);
    this.hideAlertMessagePopup = this.hideAlertMessagePopup.bind(this);

    /* book slot */
    this.bookSlotPopUpHandler = this.bookSlotPopUpHandler.bind(this);
    this.getBookSlotInfo = this.getBookSlotInfo.bind(this);
    this.onUploadBookSlotHandler = this.onUploadBookSlotHandler.bind(this);
    this.currentBookSlotFormHandler = this.currentBookSlotFormHandler.bind(this);
    this.closeBookSlot = this.closeBookSlot.bind(this);
  }

  showAlertMessagePopup(message) {
    this.setState({
      showAlertMessagePopup: true,
      alertMessage: message
    });
  }

  hideAlertMessagePopup() {
    this.setState({
      showAlertMessagePopup: false,
      alertMessage: ""
    });
  }

  triggerRedirection() {
    this.setState({
      redirectToMyScholarships: true
    });
  }

  showBookSlotSection() {
    this.setState({
      showBookSlotSection: true
    });
  }

  hideBookSlotSection() {
    this.setState({
      showBookSlotSection: false,
      showFailBookSlotMessage: false
    });
  }

  logUserOut() {
    this.props.logUserOut();
  }

  showFailBookSlotMessage() {
    this.setState({
      showFailBookSlotMessage: true
    });
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps.bookSlotType, '[nextProps.bookSlotData]')
    const { type, bookSlotType } = nextProps;
    const userList = gblFunc.getStoreUserDetails();

    if (this.state.isBookSlot && nextProps.scholarshipDetails) {
      this.props.fetchBookSlotDates(nextProps.scholarshipDetails.id);
      this.setState({ isBookSlot: false });
    }
    switch (type) {
      case FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_FAILURE:
        if (nextProps.error) {
          if (nextProps.error.is404Error) {
            this.setState({
              redirectTo404: true
            });
          }
        }
        break;

      case FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_SUCCESS:
        if (gblFunc.isUserAuthenticated()) {
          const { userId } = gblFunc.getStoreUserDetails();
          this.props.fetchApplicationStatus({ userId });
        }

        break;

      case FETCH_APPLICATION_STATUS_SUCCESS:
        let { applicationStatus } = nextProps;

        const { bsid } = this.props.match.params;

        const applStatus = applicationStatus.filter(
          applicantStatus => applicantStatus.bsid === bsid
        );

        if (applStatus.length === 0) {
          this.showFailBookSlotMessage();
        }

        if (
          applStatus.length &&
          (applStatus[0].step !== 4 && applStatus[0].step !== 5)
        ) {
          this.showFailBookSlotMessage();
        } else {
        }
        break;
      case "VERIFY_TOKEN":
        if (
          nextProps.userLoginData &&
          nextProps.userLoginData.data &&
          nextProps.userLoginData.data.data &&
          nextProps.userLoginData.data.data.mobileVerified == "0"
        ) {
          this.props.history.push({
            pathname: "/otp",
            state: {
              userId: nextProps.userLoginData.data.data.userId,
              mobile: nextProps.userLoginData.data.data.mobile,
              countryCode: nextProps.userLoginData.data.data.countryCode,
              temp_Token: nextProps.userLoginData.data.data.token,
              location:
                this.props.location && this.props.location.pathname
                  ? this.props.location.pathname
                  : null
            }
          });
        }
        break;
      case LOGIN_USER_SUCCEEDED:
        let { userId } = nextProps.userLoginData;

        // Store auth token
        // Fix for auth token
        gblFunc.storeAuthToken(nextProps.userLoginData.access_token);

        this.props.fetchUserRules({
          userid: userId
        });

        break;

      case FETCH_USER_RULES_SUCCESS:
        //SAVE USER RELEVANT DATA IN LOCALSTORAGE HERE.
        //USER IS AUTHENTICATED IN THIS CASE ONLY ( USER RULES ARE FETCHED AND TOKEN IS FETCHED)
        const {
          firstName,
          lastName,
          pic,
          profilePercentage
        } = nextProps.userRulesData;

        // Store login details here
        gblFunc.storeUserDetails({
          firstName,
          lastName,
          pic,
          profilePercentage
        });

        gblFunc.storeAuthDetails(this.props.userLoginData);

        if (!this.props.applicationStatus) {
          const { userId } = gblFunc.getStoreUserDetails();
          this.props.fetchApplicationStatus({ userId });
        }

        this.showBookSlotSection();
        this.showAlertMessagePopup(messages.login.success);

        break;
      case REGISTER_USER_SUCCEEDED:
        this.showAlertMessagePopup(messages.register.success);
        break;

      case FORGOT_USER_PASSWORD_SUCCESS:
        this.showAlertMessagePopup(messages.forgotPssword.success);
        break;
    }
    switch (bookSlotType) {
      /* case case FETCH_APPLICATION_STATUS_SUCCESS::
        this.setState({ bookSlotDate: nextProps.bookSlotData });
        break; */
      /* case FETCH_BOOK_SLOT_DATES_FAIL:
        this.setState({
          showAlertMessagePopup: true,
          statusAlert: false,
          alertMessage:
            "Book slot instruction is not available for this scholarship. Kindly contact administrator."
        });
        break; */
      // book slot popup
      case FETCH_BOOK_SLOT_SUCCESS:
        this.mapBookSlotApiToState(nextProps);
        this.props.fetchPreferLanguage("PREFERRED LANG");
        return;
      case UPDATE_BOOK_SLOT_SUCCESS:
        this.setState(
          {
            showBookSlot: false,
            isShowAlert: true,
            statusAlert: true,
            showButton: true,
            alertMsg: messages.bookSlot.success
          },
          () =>
            this.props.fetchApplicationStatus({
              userId: gblFunc.getStoreUserDetails()["userId"]
            })
        );
        return;
      case UPDATE_BOOK_SLOT_FAIL:
        // isShowAlert: true,
        // showModal: false,
        // showButton: true,
        this.setState({
          statusAlert: false,
          showButton: true,
          showBookSlot: false,
          isShowAlert: true,

          alertMsg: nextProps.updateError
        });
      case FETCH_BOOK_SLOT_DATES_SUCCESS:
        if (nextProps.bookSlotData) {
          const isExpired = moment.duration(
            moment(nextProps.bookSlotData.dateFrom, "YYYY-MM-DD").diff(
              moment().startOf("day")
            )
          ).asDays();
          if (isExpired > 0) {
            this.setState({
              isShowAlert: true,
              statusAlert: false,
              alertMsg: `Slot booking for interviews will start from ${moment(nextProps.bookSlotData.dateFrom).format('LL')}.`,
              bookSlotDate: ""
            });
          } else {
            this.setState({ bookSlotDate: nextProps.bookSlotData });
          }
        }
        break;
      case FETCH_BOOK_SLOT_DATES_FAIL:
        this.setState({
          isShowAlert: true,
          statusAlert: false,
          alertMsg: "Interview slots for this scholarship are not yet opened. You will be informed over email about the dates once it is available.",
          bookSlotDate: ""
        });
        break;
    }
  }

  componentDidMount() {
    const { bsid } = this.props.match.params;
    this.props.fetchBookSlotInstructions({ bsid });
  }

  // Book Slot
  mapBookSlotApiToState(bookProps) {
    let updatePreferLang = [...this.state.preferredLanguage];
    const updateBooksSlot = { ...this.state.book_slot };
    if (
      bookProps &&
      bookProps.bookSlot &&
      Object.keys(bookProps.bookSlot).length > 0 &&
      this.state.ON_FIRST_LOAD
    ) {
      const { bookSlot } = bookProps;
      for (let key in bookProps.bookSlot) {
        updateBooksSlot[key] = bookProps.bookSlot[key];
        if (key == "prefferedLanguages") {
          updatePreferLang =
            bookProps.bookSlot[key] === null ? [] : bookProps.bookSlot[key];
        }
      }
    }
    this.setState({
      book_slot: updateBooksSlot,
      preferredLanguage: updatePreferLang
    });
  }

  bookSlotPopUpHandler(name, userId, scholarshipId, bsid) {
    const flushValidation = {
      docFile: null,
      //comments: null,
      scheduleDate: null,
      scheduleTime: null,
      mobile: null,
      prefferedLanguages: null
    };
    if (
      ["Book Slot", "Slot Booked"].indexOf(name) === -1 &&
      typeof window !== "undefined"
    ) {
      const redirectURL = `/application/${bsid}/instruction`;
      this.props.history.push(redirectURL);
    }

    this.getBookSlotInfo(userId, scholarshipId);
    if (typeof window !== "undefined") {
      if (window.innerWidth <= 853) {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
      }
    }
    this.setState({
      showBookSlot: true,
      userId,
      bsid,
      scholarshipId,
      validations: flushValidation
    });
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "mobile":
        validationObject.name = "* Mobile";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;
      case "scheduleDate":
        validationObject.name = "* Schedule date";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "scheduleTime":
        validationObject.name = "* Schedule time";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "prefferedLanguages":
        validationObject.name = "* Preferred languages";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "comments":
        validationObject.name = "* Comments";
        validationObject.validationFunctions = [required];
        return validationObject;
    }
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      if (key != "docFile") {
        let { name, validationFunctions } = this.getValidationRulesObject(key);
        let validationResult = ruleRunner(
          key === "prefferedLanguages" && this.state.book_slot[key].length === 0
            ? ""
            : this.state.book_slot[key],
          key,
          name,
          ...validationFunctions
        );
        validations[key] = validationResult[key];
        if (validationResult[key] !== null) {
          isFormValid = false;
        }
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  getBookSlotInfo(userId, scholarshipId) {
    this.props.fetchBookSlotInfo({ userId, scholarshipId });
    this.props.fetchBookSlotDates(scholarshipId);
  }

  currentBookSlotFormHandler(e, key = "") {
    const { id, value } =
      key == "dob"
        ? {
          id: "scheduleDate",
          value:
            moment(e).format("DD-MM-YYYY") == "Invalid date"
              ? ""
              : moment(e).format("DD-MM-YYYY")
        }
        : e.target;

    const { book_slot, preferredLanguage } = { ...this.state };
    const validations = { ...this.state.validations };

    switch (id) {
      case "prefferedLanguages":
        if (
          preferredLanguage.indexOf(value) > -1 &&
          book_slot.prefferedLanguages.indexOf(value) > -1
        ) {
          preferredLanguage.splice(preferredLanguage.indexOf(value), 1);
          // book_slot.prefferedLanguages.splice(
          //   book_slot.prefferedLanguages.indexOf(value),
          //   1
          // );
        } else {
          preferredLanguage.push(value);
          // book_slot.prefferedLanguages.push(value);
        }
        break;
      case id:
        book_slot[id] = value;
        break;
    }

    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      const validationResult = ruleRunner(
        id === "prefferedLanguages" && preferredLanguage.length === 0
          ? ""
          : value,
        id,
        name,
        ...validationFunctions
      );

      validations[id] = validationResult[id];
    }

    this.setState({
      preferredLanguage,
      validations
    });
  }

  onUploadBookSlotHandler(event, scholarshipId) {
    event.preventDefault();
    const userList = gblFunc.getStoreUserDetails();
    const { book_slot, preferredLanguage } = { ...this.state };
    book_slot.userId = this.state.userId;
    book_slot.scholarshipId = this.state.scholarshipId;
    book_slot.prefferedLanguages = preferredLanguage;
    if (this.checkFormValidations()) {
      this.props.updatingBookSlot({
        userId: userList.userId,
        scholarshipId: this.state.scholarshipId,
        book_slot
      });
    }
  }

  closeBookSlot() {
    this.setState({
      showBookSlot: false,
      validations: {
        docFile: null,
        //comments: null,
        scheduleDate: null,
        scheduleTime: null,
        mobile: null,
        prefferedLanguages: null
      },
      book_slot: {
        id: null,
        comments: "",
        scheduleDate: "",
        scheduleTime: "",
        mobile: "",
        prefferedLanguages: [],
        scholarshipId: null,
        userId: null
      },
      bsid: null,
      preferredLanguage: []
    });

  }

  render() {
    const { bsid } = this.props.match.params;
    const isUserAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;

    let firstName, lastName, fullName;
    if (isUserAuthenticated) {
      const userDetails = gblFunc.getStoreUserDetails();

      firstName = userDetails.firstName;
      lastName = userDetails.lastName;

      fullName = `${firstName} ${lastName}`;
    }

    if (isUserAuthenticated && !this.state.showBookSlotSection) {
      this.showBookSlotSection();
    } else if (!isUserAuthenticated && this.state.showBookSlotSection) {
      this.hideBookSlotSection();
    }

    let scholarshipSlug = "",
      scholarshipName = "",
      scholarshipLogo = "";
    if (this.props.scholarshipDetails) {
      scholarshipSlug = this.props.scholarshipDetails.slug;
      scholarshipName = this.props.scholarshipDetails.scholarshipName;
      scholarshipLogo = this.props.scholarshipDetails.logoFid;
      scholarshipName = gblFunc.replaceWithLoreal(scholarshipName, true);
    }

    if (this.props.error) {
      if (this.props.error.is404Error) {
        return <Redirect to="/404" />;
      } else {
        return (
          <section className="margin__top__10">
            <section className="container">
              <ServerError errorMessage={this.props.error.errorMessage} />
            </section>
          </section>
        );
      }
    }
    return (
      <section>
        <AlertMessagePopup
          msg={this.state.alertMessage}
          isShow={this.state.showAlertMessagePopup}
          status={this.state.statusAlert}
          close={this.hideAlertMessagePopup}
        />
        {this.state.showBookSlot && this.state.bookSlotDate ? (
          <BookSlot
            bookSlotData={this.state.book_slot}
            bsid={this.state.bsid}
            preferLang={this.props.preferLang}
            currentBookSlotFormHandler={this.currentBookSlotFormHandler}
            updateBookSlotHandler={this.onUploadBookSlotHandler}
            closeBookSlot={this.closeBookSlot}
            validations={this.state.validations}
            bookSlotDates={this.state.bookSlotDate}
          />
        ) : null}
        <section className="container">
          <Loader isLoader={this.props.showLoader} />
          <article className="application alignTB">
            <article className="row">
              <article className="col-md-12">
                <a target="_blank" href={`/scholarship/${scholarshipSlug}`}>
                  <img
                    alt={scholarshipName}
                    className="scholarshipLogo"
                    src={scholarshipLogo}
                  />
                </a>
                <h3 className="text-center titleMargin">
                  <a target="_blank" href={`/scholarship/${scholarshipSlug}`}>
                    {scholarshipName}
                  </a>
                </h3>
                {this.state.showFailBookSlotMessage ? null : (
                  <BookSlotInfo
                    data={this.props.scholarshipDetails}
                    bookSlotDates={this.state.bookSlotDate}
                    bsid={bsid}
                  />
                )}

                {/* Start login  / Registration form */}

                <article
                  className={`${
                    this.state.showFailBookSlotMessage
                      ? "col-md-12"
                      : "col-md-5"
                    }`}
                >
                  {this.state.showBookSlotSection ? (
                    <article className="bglogin topPadding pull-left">
                      <article className="text-center">
                        <strong className="fontsize23">
                          {`Dear ${fullName}`},
                        </strong>
                        <p className="margintop20">
                          {"You have logged in successfully. "}
                          <br />
                          {`${
                            this.state.showFailBookSlotMessage
                              ? "Sorry you are not shortlisted for this application"
                              : "Now you can book slot or "
                            }`}
                          <br />
                          <a onClick={this.logUserOut} className="logout">Log Out</a>
                        </p>
                        {this.state.showFailBookSlotMessage ? null : (
                          <a className="btn btn-primary margintop20 bookSlotnew"
                            onClick={() =>
                              this.bookSlotPopUpHandler(
                                "Book Slot",
                                gblFunc.getStoreUserDetails()["userId"],
                                this.props.bookSlotData.scholarshipId,
                                this.props.scholarshipDetails.bsid
                              )
                            }
                          >
                            BOOK SLOT
                          </a>
                        )}
                      </article>
                    </article>
                  ) : (
                      <LoginRegisterSection />
                    )}
                </article>
              </article>
            </article>
          </article>
        </section>
      </section>
    );
  }
}

export default BookSlotInstructions;
