import React, { Component } from "react";

import { Link } from "react-router-dom";
import { ruleRunner } from "../../../validation/ruleRunner";
import { required, isEmail, minLength } from "../../../validation/rules";
import { LOGIN_USER_FAILED, LOGIN_USER_SUCCEEDED } from "../../login/actions";
import gblFunc from "../../../globals/globalFunctions";
import { FETCH_USER_RULES_SUCCESS } from "../../../constants/commonActions";

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.loginUser = this.loginUser.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);

    this.state = {
      username: "",
      password: "",
      isFormValid: false,
      validations: {
        username: null,
        password: null
      }
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const { id, value } = e.target;

    this.setState({
      [id]: value
    });
  }

  loginUser(event) {
    event.preventDefault();
    if (this.checkFormValidations()) {
      this.props.loginUser({
        username: this.state.username,
        password: this.state.password
      });
    }
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "username":
        validationObject.name = "*Email address";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;

      case "password":
        validationObject.name = "*Password ";
        validationObject.validationFunctions = [required, minLength(6)];
        return validationObject;
    }
  }

  handleChange(event) {
    const { id, value } = event.target;

    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );

    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    this.setState({
      [id]: value,
      validations
    });
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );

      validations[key] = validationResult[key];

      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }

    this.setState({
      validations,
      isFormValid
    });

    return isFormValid;
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case LOGIN_USER_FAILED:
        console.log("I am in login user failed ");
        let validations = { ...this.state.validations };
        validations.username = nextProps.serverErrorLogin;
        this.setState({
          validations
        });
        break;
    }
  }

  render() {
    return (
      <article className="bglogin pull-left paddingBoth">
        <article className="applicationform register">
          <article>
            <h2>Please Log in/Register to start application</h2>

            <form name="regForm" onSubmit={this.loginUser}>
              <section className="ctrl-wrapper">
                <article className="form-group">
                  <input
                    type="username"
                    className="form-control"
                    minLength="3"
                    name="username"
                    placeholder="Enter email address"
                    id="username"
                    required
                    value={this.state.username}
                    onChange={this.handleChange}
                    autoComplete="off"
                    tabIndex="1"
                  />
                  {this.state.validations.username ? (
                    <span className="error animated bounce">
                      {this.state.validations.username}
                    </span>
                  ) : null}
                </article>
              </section>
              <section className="ctrl-wrapper">
                <article className="form-group">
                  <input
                    type="password"
                    className="form-control"
                    maxLength="20"
                    minLength="6"
                    name="password"
                    placeholder="Password"
                    onChange={this.handleChange}
                    value={this.state.password}
                    id="password"
                    autoComplete="off"
                    required
                    tabIndex="2"
                  />
                  {this.state.validations.password ? (
                    <span className="error animated bounce">
                      {this.state.validations.password}
                    </span>
                  ) : null}
                </article>
              </section>

              <section className="ctrl-wrapper">
                <article className="form-group">
                  <button
                    type="submit"
                    className="btn-block greenBtn margintop"
                    tabIndex="3"
                  >
                    Sign In!
                  </button>
                </article>
              </section>
            </form>

            <article className="textPara">
              <dd>
                By clicking on any of the buttons, you agree to Buddy4Study's
                &nbsp;
                <Link to="/terms-and-conditions">terms &amp; conditions</Link>
              </dd>
              <article className="textPara">
                <a
                  onClick={this.props.showForgotPasswordForm}
                  className="forgotpwd-link"
                >
                  Forgot password
                </a>
              </article>
            </article>
          </article>
        </article>
      </article>
    );
  }
}

export default LoginForm;
