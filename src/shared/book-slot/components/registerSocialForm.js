import React, { Component } from "react";

import FacebookLoginButton from "./facebookLoginButton";
import GoogleLoginButton from "./googleLoginButton";

class RegistrationSocialForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fbError: false,
      googleError: false
    };
    this.onFacebookLoginSuccess = this.onFacebookLoginSuccess.bind(this);
    this.onFacebookLoginFailure = this.onFacebookLoginFailure.bind(this);
    this.onGoogleLoginSuccess = this.onGoogleLoginSuccess.bind(this);
    this.onGoogleLoginFailure = this.onGoogleLoginFailure.bind(this);
  }

  /***********Social Login Call back Start******** */
  onFacebookLoginSuccess(data) {
    let user = data._profile;
    let objectData = {
      username: user.email,
      grant_type: "password",
      socialLogin: true,
      userData: {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        userSource: /Android/i.test(navigator.userAgent)
          ? "FB-MOBILE-WEBSITE"
          : "FB-WEBSITE",
        portalId: 1,
        socialLogin: true
      }
    };
    this.props.socialLogin(objectData);
  }

  onFacebookLoginFailure(err) {
    this.setState(
      {
        fbError: true
      },
      () => this.setState({ fbError: false })
    );
  }

  onGoogleLoginSuccess(data) {
    let user = data._profile;
    let objectData = {
      username: user.email,
      grant_type: "password",
      socialLogin: true,
      userData: {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        userSource: /Android/i.test(navigator.userAgent)
          ? "GOOGLE-MOBILE-WEBSITE"
          : "GOOGLE-WEBSITE",
        portalId: 1,
        socialLogin: true
      }
    };
    this.props.socialLogin(objectData);
  }

  onGoogleLoginFailure(err) {
    this.setState(
      {
        googleError: true
      },
      () => this.setState({ googleError: false })
    );
  }

  render() {
    return (
      <article className="bglogin pull-left paddingBoth">
        <h2>Please Log in/Register to start application</h2>
        <article className="applicationform">
          {/* Login button */}
          <section className="btn-wrapper">
            <article className="social-box ">
              <section className="ctrl-wrapper">
                <article className="form-group widthAuto">
                  <article className="row mg-btm">
                    {this.state.fbError ? null : (
                      <FacebookLoginButton
                        provider="facebook"
                        appId="266013666868531" //buddy4study official website buddy4study@gmail.com
                        onLoginSuccess={this.onFacebookLoginSuccess}
                        onLoginFailure={this.onFacebookLoginFailure}
                      />
                    )}
                    {this.state.googleError ? null : (
                      <GoogleLoginButton
                        provider="google"
                        appId="913333428875-ca46kak1a1nvlq9ul30e6nned0t2sr98.apps.googleusercontent.com" //info@buddy4study.com
                        onLoginSuccess={this.onGoogleLoginSuccess}
                        onLoginFailure={this.onGoogleLoginFailure}
                      />
                    )}
                    <span
                      onClick={this.props.showRegisterForm}
                      className="btn btn-block b4s"
                    >
                      <i className="fa fa-envelope" aria-hidden="true" />{" "}
                      Register with your email
                    </span>
                  </article>
                </article>
              </section>
            </article>
            <dd>
              By clicking on any of the buttons, you agree to Buddy4Study's
              &nbsp;
              <a href="/terms-and-conditions" target="_blank">
                terms &amp; conditions
              </a>
            </dd>
            <article className="textPara">
              <p>
                <i>Already registered?</i> &nbsp;
                <a
                  onClick={this.props.showLoginSection}
                  className="forgotpwd-link"
                >
                  Sign in here
                </a>
              </p>
            </article>
          </section>
        </article>
      </article>
    );
  }
}

export default RegistrationSocialForm;
