import React from "react";
import gblFunc from "../../../globals/globalFunctions";
import moment from "moment";

const BookSlotInfo = ({ data, bookSlotDates, bsid }) =>
  bookSlotDates && data ? (
    <article className="col-md-7">
      <article>
        {/* Eligibility */}
        <article>
          <article className="eligibility unorderList">
            <article>
              <p>{`Dear Applicant of ${gblFunc.replaceWithLoreal(
                data.scholarshipName,
                true
              )}`}</p>
              <p>
                Congratulations! You’ve been shortlisted for telephonic round
                interview.
              </p>
              <p>
                Please login and book a time slot for your telephonic round. You
                will receive a call on your number. Be available on your slot
                time given below:
              </p>
              <ul>
                <li>10:00am-01:30pm</li>
                <li>02:30pm-04:30pm</li>
                <li>05:00pm-07:00pm</li>
              </ul>
              <p className="fwbold ng-binding">
                {`Last date of interview is ${moment(
                  bookSlotDates.interviewEndDate
                ).format("DD-MM-YYYY")}  ${
                  bsid === "HUS2"
                    ? "No interviews will be conducted on 21-03-2019"
                    : "No interviews will be conducted on Saturday's & Sunday's."
                  }`}
              </p>
            </article>
          </article>
        </article>
      </article>
    </article>
  ) : (
      <article className="col-md-7" />
    );

export default BookSlotInfo;
