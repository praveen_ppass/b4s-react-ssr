import React, { Component } from "react";

import RegistrationForm from "../containers/registrationFormContainer";
import { RegisterSocialFormContainer as RegisterSocialForm } from "../containers/socialLoginContainer";

class RegisterSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //TODO: Add form fields
      currentForm: "REGISTER_SOCIAL_BUTTONS"
    };

    this.showRegisterForm = this.showRegisterForm.bind(this);
  }

  showRegisterForm() {
    this.setState({
      currentForm: "REGISTER_FORM"
    });
  }

  renderCurrentForm() {
    const { currentForm } = this.state;

    switch (currentForm) {
      case "REGISTER_SOCIAL_BUTTONS":
        return (
          <RegisterSocialForm
            showRegisterForm={this.showRegisterForm}
            showLoginSection={this.props.showLoginSection}
          />
        );

      case "REGISTER_FORM":
        return (
          <RegistrationForm
            showLoginSection={this.props.showLoginSection}
            showRegisterSection={this.props.showRegisterSection}
          />
        );
    }
  }

  render() {
    return (
      <article className="bglogin pull-left paddingBoth">
        {this.renderCurrentForm()}
      </article>
    );
  }
}

export default RegisterSection;
