import React, { Component } from "react";

import { ruleRunner } from "../../../validation/ruleRunner";
import {
  required,
  minLength,
  isEmail,
  isNumeric,
  shouldMatch,
  isMobileNumber
} from "../../../validation/rules";
import { REGISTER_USER_SUCCEEDED } from "../../login/actions";

class RegistrationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      mobile: "",
      email: "",
      password: "",
      conf_password: "",
      isFormValid: false,
      validations: {
        firstName: null,
        //lastName: null,
        mobile: null,
        email: null,
        password: null,
        conf_password: null
      }
    };
    this.onFieldChange = this.onFieldChange.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.onRegisterClick = this.onRegisterClick.bind(this);
    this.mapLocalStateToApiCall = this.mapLocalStateToApiCall.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
  }

  onFieldChange(event) {
    const { id, value } = event.target;

    this.setState({
      [id]: value
    });
  }

  componentWillReceiveProps(nextProps) {
    const { serverErrorRegister, type } = nextProps;
    if (serverErrorRegister) {
      let validations = { ...this.state.validations };
      validations.email = serverErrorRegister;
      this.setState({
        validations
      });
    }

    switch (type) {
      case REGISTER_USER_SUCCEEDED:
        this.props.showLoginSection();
    }
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );

      validations[key] = validationResult[key];

      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }

    this.setState({
      validations,
      isFormValid
    });

    return isFormValid;
  }

  onRegisterClick(event) {
    event.preventDefault();
    /* call for register user */
    if (this.checkFormValidations()) {
      let mappedUserApiObject = this.mapLocalStateToApiCall(this.state);

      this.props.registerUser(mappedUserApiObject);
    }
  }

  mapLocalStateToApiCall({ email, password, firstName, lastName, mobile }) {
    /*  Sample API call
"email": "abhinav.misra@buddy4study.com",
"userSource": /Android/i.test(navigator.userAgent)? "EMAIL-MOBILE-WEBSITE": "EMAIL-WEBSITE",
"password": "repassword",
"firstName": "Abhinav",
"lastName": "Mishra",
"mobile": "9953189925"  */

    return {
      email,
      userSource: /Android/i.test(navigator.userAgent)
        ? "EMAIL-MOBILE-WEBSITE"
        : "EMAIL-WEBSITE",
      password,
      firstName,
      lastName,
      mobile
    };
  }

  /*

ORDER OF VALIDATION FUNCTIONS MATTERS.

*/
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "email":
        validationObject.name = "*Email address";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;
      case "firstName":
        validationObject.name = "*First name";
        validationObject.validationFunctions = [required, minLength(1)];
        return validationObject;
      case "lastName":
        validationObject.name = "*Last name";
        validationObject.validationFunctions = [required, minLength(1)];
        return validationObject;
      case "mobile":
        validationObject.name = "*Mobile number";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;
      case "password":
        validationObject.name = "*Password ";
        validationObject.validationFunctions = [required, minLength(6)];
        return validationObject;
      case "conf_password":
        validationObject.name = "*Passwords ";
        validationObject.validationFunctions = [
          shouldMatch(this.state.password)
        ];
        return validationObject;
    }
  }

  onFieldChange(event) {
    let { value, id } = event.target;
    let shouldUpdate = true;

    let { name, validationFunctions } = this.getValidationRulesObject(id);
    let validationResult = ruleRunner(value, id, name, ...validationFunctions);

    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    let newState = { validations };

    newState[id] = value;

    this.setState(newState);
  }

  render() {
    return (
      <article className="applicationform register">
        <article>
          <h2>Please Log in/Register to start application</h2>
          <p>Let's set up your account in just a couple of steps.</p>
          <form name="regForm" autoCapitalize="off">
            {/* <section className="ctrl-wrapper">
              <article className="form-group">
                <i className="fa fa-map-marker iconpos ft hide" />
                <select
                  name="role"
                  id="role"
                  className="form-control"
                  required=""
                  tabIndex="1"
                >
                  <option value="">--Select your role--</option>
                  <option value="" />
                </select>
                {this.state.validations.role ? (
                  <span className="error animated bounce">* Role is required.</span>
                ) : null}
              </article>
            </section> */}
            {/* <section className="ctrl-wrapper">
              <article className="form-group">
                <i className="fa fa-user-secret iconpos lt hide" />
                <select
                  name="class"
                  id="class"
                  className="form-control"
                  required=""
                  tabIndex="2"
                >
                  <option value="">--Select your class--</option>
                  <option value="" />
                </select>
                {this.state.validations.class ? (
                  <span className="error animated bounce">
                    * Class is required.
                  </span>
                ) : null}
              </article>
            </section> */}
            <section className="ctrl-wrapper">
              <article className="form-group">
                <input
                  type="text"
                  className="form-control"
                  maxLength="80"
                  minLength="1"
                  name="first_name"
                  placeholder="First Name"
                  id="firstName"
                  value={this.state.firstName}
                  onChange={this.onFieldChange}
                  autoComplete="off"
                  required=""
                  tabIndex="3"
                />
                {this.state.validations.firstName ? (
                  <span className="error animated bounce">
                    {this.state.validations.firstName}
                  </span>
                ) : null}
              </article>
            </section>
            <section className="ctrl-wrapper">
              <article className="form-group">
                <input
                  type="text"
                  className="form-control"
                  maxLength="80"
                  minLength="1"
                  name="last_name"
                  placeholder="Last Name"
                  ng-model="lastName"
                  onChange={this.onFieldChange}
                  value={this.state.lastName}
                  id="lastName"
                  autoComplete="off"
                  required=""
                  tabIndex="4"
                />
                {this.state.validations.lastName ? (
                  <span className="error animated bounce">
                    {this.state.validations.lastName}
                  </span>
                ) : null}
              </article>
            </section>
            <section className="ctrl-wrapper">
              <article className="form-group">
                <input
                  maxLength="10"
                  pattern="/(^1$)|(^[7-9]\d{0,9}$)/"
                  type="tel"
                  className="form-control"
                  name="mobile"
                  id="mobile"
                  onChange={this.onFieldChange}
                  value={this.state.mobile}
                  autoComplete="off "
                  placeholder="Enter mobile number"
                  tabIndex="6"
                />

                {this.state.validations.mobile ? (
                  <span className="error animated bounce">
                    {this.state.validations.mobile}
                  </span>
                ) : null}
              </article>
            </section>
            <section className="ctrl-wrapper">
              <article className="form-group">
                <input
                  type="email"
                  name="email"
                  className="form-control"
                  placeholder="Email"
                  id="email"
                  onChange={this.onFieldChange}
                  value={this.state.email}
                  autoComplete="off"
                  required
                  tabIndex="5"
                />
                {this.state.validations.email ? (
                  <span className="error animated bounce">
                    {this.state.validations.email}
                  </span>
                ) : null}
                {/* <span className="error animated bounce">
                  * Please enter valid email address.
                </span>
                <span className="error animated bounce">* Email is required.</span> */}
              </article>
            </section>
            <section className="ctrl-wrapper">
              <article className="form-group">
                <input
                  type="password"
                  className="form-control"
                  name="password"
                  maxLength="20 "
                  minLength="6"
                  placeholder="Password"
                  id="password"
                  onChange={this.onFieldChange}
                  value={this.state.password}
                  autoComplete="off"
                  required
                  tabIndex="7"
                />
                {this.state.validations.password ? (
                  <span className="error animated bounce">
                    {this.state.validations.password}
                  </span>
                ) : null}
              </article>
            </section>
            <section className="ctrl-wrapper">
              <article className="form-group">
                <input
                  type="password"
                  className="form-control"
                  name="conf_password"
                  placeholder="Confirm Password"
                  id="conf_password"
                  autoComplete="off"
                  onChange={this.onFieldChange}
                  value={this.state.conf_password}
                  required
                  password-verify="regPassword"
                  tabIndex="8"
                />
                {this.state.validations.conf_password ? (
                  <span className="error animated bounce">
                    {this.state.validations.conf_password}
                  </span>
                ) : null}
              </article>
            </section>
            <section className="ctrl-wrapper">
              <article className="form-group">
                <button
                  type="submit"
                  className="btn-block greenBtn margintop"
                  tabIndex="10"
                  onClick={this.onRegisterClick}
                >
                  Register Now!
                </button>
              </article>
            </section>
          </form>
          <article className="textPara">
            <p>
              <i>Already registered?</i> &nbsp;
              <a
                onClick={this.props.showLoginSection}
                className="forgotpwd-link"
              >
                Sign in here
              </a>
            </p>
            <i>Register with social link</i> &nbsp;
            <a onClick={this.props.showLoginSection} className="forgotpwd-link">
              Facebook &amp; Google
            </a>
          </article>
        </article>
      </article>
    );
  }
}

export default RegistrationForm;
