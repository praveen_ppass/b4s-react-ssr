import React from "react";
import SocialLogin from "react-social-login";

const GoogleLoginButton = ({ children, triggerLogin, ...props }) => (
  <span onClick={triggerLogin} className="btn btn-block googleplus" {...props}>
    <i className="fa fa-google-plus" aria-hidden="true" />Login with Google
    {children}
  </span>
);

export default SocialLogin(GoogleLoginButton);
