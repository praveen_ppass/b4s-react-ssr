import React, { Component } from "react";

import { LoginSocialFormContainer as LoginSocialForm } from "../containers/socialLoginContainer";
import LoginForm from "../containers/loginFormContainer";
import ForgotPasswordForm from "../containers/forgotPasswordContainer";

class LoginSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentForm: "LOGIN_SOCIAL_FORM"
    };
    this.renderCurrentForm = this.renderCurrentForm.bind(this);
    this.showForgotPasswordForm = this.showForgotPasswordForm.bind(this);
    this.showLoginForm = this.showLoginForm.bind(this);
  }

  showLoginForm() {
    this.setState({
      currentForm: "LOGIN_FORM"
    });
  }

  showForgotPasswordForm() {
    this.setState({
      currentForm: "FORGOT_PASSWORD_FORM"
    });
  }

  renderCurrentForm(currentForm) {
    switch (currentForm) {
      case "LOGIN_SOCIAL_FORM":
        return (
          <LoginSocialForm
            showLoginForm={this.showLoginForm}
            showRegisterSection={this.props.showRegisterSection}
            showForgotPasswordForm={this.showForgotPasswordForm}
          />
        );

      case "LOGIN_FORM":
        return (
          <LoginForm
            showForgotPasswordForm={this.showForgotPasswordForm}
            triggerRedirection={this.props.triggerRedirection}
          />
        );

      case "FORGOT_PASSWORD_FORM":
        return <ForgotPasswordForm showLoginForm={this.showLoginForm} />;
    }
  }

  render() {
    return this.renderCurrentForm(this.state.currentForm);
  }
}

export default LoginSection;
