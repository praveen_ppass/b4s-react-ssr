import React, { Component } from "react";

import RegisterSection from "./registerSection";
import LoginSection from "./loginSection";
import ForgotPasswordForm from "./forgotPasswordForm";
class LoginRegisterSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSection: "Login"
    };
    this.renderCurrentForm = this.renderCurrentForm.bind(this);
    this.showRegisterSection = this.showRegisterSection.bind(this);
    this.showLoginSection = this.showLoginSection.bind(this);
    this.onTabChange = this.onTabChange.bind(this);
  }

  onTabChange(event) {
    event.stopPropagation();
    const { id } = event.target;
    const parentId = event.target.parentNode.id;

    const isLoginOrRegister = value =>
      value === "Login" || value === "Register";

    if (isLoginOrRegister(id) || isLoginOrRegister(parentId)) {
      this.setState({
        currentSection: id || parentId
      });
    }
  }

  showRegisterSection() {
    this.setState({
      currentSection: "Register"
    });
  }

  showLoginSection() {
    this.setState({
      currentSection: "Login"
    });
  }

  renderCurrentForm() {
    const { currentSection } = this.state;

    switch (currentSection) {
      case "Login":
        return (
          <LoginSection
            showRegisterSection={this.showRegisterSection}
            showLoginSection={this.showLoginSection}
          />
        );

      case "Register":
        return (
          <RegisterSection
            showLoginSection={this.showLoginSection}
            showRegisterSection={this.showRegisterSection}
          />
        );

      case "ForgotPassword":
        return <ForgotPasswordForm />;
    }
  }

  render() {
    return (
      <article>
        <ul onClick={this.onTabChange} className="optional-links">
          <li
            id="Login"
            className={this.state.currentSection === "Login" ? "active" : ""}
          >
            <span>Login</span>
          </li>
          {/* <li
            id="Register"
            className={this.state.currentSection === "Register" ? "active" : ""}
          >
            <span>Registration</span>
          </li> */}
        </ul>
        {this.renderCurrentForm()}
      </article>
    );
  }
}

export default LoginRegisterSection;
