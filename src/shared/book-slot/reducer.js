import {
  LOGIN_USER_REQUESTED,
  LOGIN_USER_SUCCEEDED,
  LOGIN_USER_FAILED,
  FORGOT_USER_PASSWORD_REQUEST,
  FORGOT_USER_PASSWORD_FAILURE,
  FORGOT_USER_PASSWORD_SUCCESS,
  SOCIAL_LOGIN_USER_REQUESTED,
  SOCIAL_LOGIN_USER_SUCCEEDED,
  SOCIAL_LOGIN_USER_FAILED,
  LOG_USER_OUT,
  LOG_USER_OUT_FAILED,
  LOG_USER_OUT_SUCCEEDED,
  REGISTER_USER_REQUEST,
  REGISTER_USER_FAILED,
  REGISTER_USER_SUCCEEDED
} from "../login/actions";
import {
  FETCH_USER_RULES_REQUEST,
  FETCH_USER_RULES_SUCCESS,
  FETCH_USER_RULES_FAILURE
} from "../../constants/commonActions";
import {
  FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_REQUEST,
  FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_SUCCESS,
  FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_FAILURE
} from "./actions";
import {
  FETCH_APPLICATION_STATUS_REQUEST,
  FETCH_APPLICATION_STATUS_SUCCESS,
  FETCH_APPLICATION_STATUS_FAIL
} from "../dashboard/actions";

const initialState = {
  showLoader: false,
  isError: false,
  errorMessage: "",
  userLoginData: null,
  userRulesData: null,
  scholarshipDetails: null,
  serverErrorForgotPassword: "",
  applicationStatus: null,
  error: null
};

const bookSlotReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    /* Login related actions */
    case LOGIN_USER_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case REGISTER_USER_REQUEST:
      return {
        ...state,
        showLoader: true,
        isError: false
      };

    case REGISTER_USER_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: false
      };

    case REGISTER_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type
      };

    case LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload
      };
    case "VERIFY_TOKEN":
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload
      };

    case LOGIN_USER_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        serverErrorLogin: payload,
        isError: true
      };

    case FETCH_USER_RULES_REQUEST:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case FETCH_USER_RULES_SUCCESS:
      return {
        ...state,
        showLoader: false,
        userRulesData: payload,
        isError: false,
        type
      };

    case FETCH_USER_RULES_FAILURE:
      return {
        ...state,
        showLoader: false,
        isError: true,
        type
      };

    /* Forgot password related actions*/
    case FORGOT_USER_PASSWORD_REQUEST:
      return {
        ...state,
        type,
        showLoader: true,
        serverErrorForgotPassword: ""
      };

    case FORGOT_USER_PASSWORD_SUCCESS:
      return { ...state, type, showLoader: false };

    case FORGOT_USER_PASSWORD_FAILURE:
      return {
        ...state,
        type,
        showLoader: false,
        isError: true,
        serverErrorForgotPassword: payload
      };

    /* Fetch book slot details actions */
    case FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_REQUEST:
      return { ...state, showLoader: true, type, isError: false, error: null };

    case FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_SUCCESS:
      return { ...state, showLoader: false, type, scholarshipDetails: payload };

    case FETCH_BOOK_SLOT_SCHOLARSHIP_DETAILS_FAILURE:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        error: payload
      };

    /*  Social login related actions */
    case SOCIAL_LOGIN_USER_REQUESTED:
      return { ...state, type, showLoader: true };

    case SOCIAL_LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type,
        userLoginData: payload,
        isAuthenticated: true
      };

    case SOCIAL_LOGIN_USER_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        type,
        serverErrorLogin: payload
      };

    case FETCH_APPLICATION_STATUS_REQUEST:
      return { ...state, showLoader: true, isError: false, type };

    case FETCH_APPLICATION_STATUS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        applicationStatus: payload,
        isError: false,
        type
      };

    case FETCH_APPLICATION_STATUS_FAIL:
      return { ...state, showLoader: false, isError: true, type };

    case LOG_USER_OUT:
      return { ...state, showLoader: true, isError: false, type };

    case LOG_USER_OUT_FAILED:
      return { ...state, showLoader: false, isError: true, type };

    case LOG_USER_OUT_SUCCEEDED:
      return { ...state, showLoader: false, isError: false, type };

    default:
      return state;
  }
};
export default bookSlotReducer;
