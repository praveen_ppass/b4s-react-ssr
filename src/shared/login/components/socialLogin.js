import React from "react";
import SocialLogin from "react-social-login";

const Button = ({ children, triggerLogin, ...props }) => (
  <article className="social">
    <a onClick={triggerLogin} {...props}>
      {children}
    </a>
  </article>
);

export default SocialLogin(Button);
