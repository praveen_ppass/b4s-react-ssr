import React, { Component } from "react";
import gblFunc from "../../../globals/globalFunctions";
import { ruleRunner } from "../../../validation/ruleRunner";
import { required, isEmail, minLength, isEmailMobile } from "../../../validation/rules";

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);

    this.loginUser = this.loginUser.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);

    this.state = {
      username: "",
      password: "",
      validations: {
        username: null,
        password: null
      },
      isFormValid: false
    };
  }

  loginUser(event) {
    event.preventDefault();
    if (this.checkFormValidations()) {
      gblFunc.gaTrack.trackEvent(["Login Success", "LoginwithEmail"])
      this.props.loginUser({
        username: this.state.username,
        password: this.state.password
      });
    }
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "username":
        validationObject.name = "*Email address/ Mobile";
        validationObject.validationFunctions = [required, isEmailMobile];
        return validationObject;

      case "password":
        validationObject.name = "*Password ";
        validationObject.validationFunctions = [required, minLength(6)];
        return validationObject;
    }
  }

  handleChange(event) {
    const { id, value } = event.target;
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );

    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    this.setState({
      [id]: value,
      validations
    });
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );

      validations[key] = validationResult[key];

      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }

    this.setState({
      validations,
      isFormValid
    });

    return isFormValid;
  }

  componentWillReceiveProps(nextProps) {
    let { serverError } = nextProps;
    if (serverError) {
      let validations = { ...this.state.validations };
      validations.username = serverError;
      this.setState({
        validations
      });
    }
  }

  render() {
    const { isVLElogin, isShowOtp } = this.props;
    return (
      <div
        // id="tab-2"
        className={`${isVLElogin === true ? "paddingtop65" : ""}`}
      >
        <form name="loginForm" className="formelemt" autoCapitalize="off">
          <section className="row">
            <section className="ctrl-wrapper">
              <article className="form-group">
                <input
                  type="text"
                  name="emailMobile"
                  className="form-control"
                  placeholder="Enter Your Email Address/Mobile"
                  id="username"
                  required
                  value={this.state.username}
                  onChange={this.handleChange}
                />
                {this.state.validations.username ? (
                  <dd className="animated bounce error1">
                    {this.state.validations.username}
                  </dd>
                ) : null}
              </article>
            </section>

            <section className="ctrl-wrapper">
              <article className="form-group">
                <input
                  type="password"
                  name="password"
                  className="form-control"
                  placeholder="Password"
                  id="password"
                  required
                  value={this.state.password}
                  onChange={this.handleChange}
                />
                {this.state.validations.password ? (
                  <dd className="animated bounce error1">
                    {this.state.validations.password}
                  </dd>
                ) : null}
              </article>
            </section>
          </section>
          <section className="row">
            <button
              type="submit"
              onClick={this.loginUser}
              className="btn-block greenBtn"
            >
              Login
            </button>
            {!isVLElogin && (
              <span
                onClick={this.props.showForgotPasswordForm}
                className="handPointer"
              >
                Forgot Password
              </span>
            )}
          </section>
        </form>
      </div>
    );
  }
}

export default LoginForm;
