import React from "react";

const CompanyInfoSection = props => (
  <article className="col-md-5">
    <article className="listtyoP">
      <p>Looking for scholarships?</p>
      <ul>
        <li>
          Get access to thousands of national and international scholarships
        </li>
        <li>Find compatible scholarships that you stand a chance to win</li>
        <li>
          Hassle-free application process and complete application support at
          every step
        </li>
        <li> Keep a track of all your applications</li>
        <li>Get notified of new scholarships matching your profile</li>
      </ul>
    </article>
  </article>
);

export default CompanyInfoSection;
