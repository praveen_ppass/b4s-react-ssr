import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { ruleRunner } from "../../../validation/ruleRunner";
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import {
  required,
  minLength,
  isEmail,
  isNumeric,
  shouldMatch,
  isMobileNumber,
  isChar
} from "../../../validation/rules";
import AlertMessagePopup from "../../../shared/common/components/alertMsg";
import Loader from "../../common/components/loader";
import gblFunc from "../../../globals/globalFunctions";
import SocialButton from "./socialLogin"
import {
  REGISTER_USER_SUCCEEDED,
  REGISTER_USER_FAILED,
  LOGIN_USER_SUCCEEDED,
  GET_EXISTING_MOBILE_SUCCESS,
  GET_EXISTING_MOBILE_FAILURE,
} from "../actions";
import {
  FETCH_RULES_SUCCEEDED,
  UPDATE_USER_RULES_SUCCESS,
  FETCH_USER_RULES_SUCCESS,
} from "../../../constants/commonActions";


class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.onFieldChange = this.onFieldChange.bind(this);
    this.onRegisterClick = this.onRegisterClick.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.onFacebookLoginSuccess = this.onFacebookLoginSuccess.bind(this);
    this.onFacebookLoginFailure = this.onFacebookLoginFailure.bind(this);
    this.onGoogleLoginSuccess = this.onGoogleLoginSuccess.bind(this);
    this.onGoogleLoginFailure = this.onGoogleLoginFailure.bind(this);
    this.mobileStatus = this.mobileStatus.bind(this);
    this.closePopup = this.closePopup.bind(this);

    this.state = {
      firstName: "",
      lastName: "",
      countryCode: "+91",
      mobile: "",
      email: "",
      password: "",
      conf_password: "",
      isTrue: true,
      isFormValid: false,
      isAlertShow: false,
      alertStatus: "",
      alertMsg: "",
      isOtpTrue: false,
      redirectUrlState: "",
      isRedirectUserInfo: false,
      currentPath: false,
      validations: {
        firstName: null,
        //lastName: null,
        mobile: null,
        email: null,
        password: null,
        conf_password: null
      }
    };
  }

  componentDidMount() {
    if (!gblFunc.isUserAuthenticated()) {
      gblFunc.removeUserDetailsForRule()
    }
    this.setState({
      redirectUrlState: getNestedObjKey(this.props, ["location", "state", "location"])
        ? getNestedObjKey(this.props, ["location", "state", "location"])
        : this.props.location
    })
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.type == nextProps.type) {
      this.setState({
        mobile: "",
        email: "",
        firstName: "",
        lastName: "",
        password: "",
        conf_password: ""
      })
    }
    const { type, userData, serverErrorRegister, userRulesData, userLoginData } = nextProps;
    switch (type) {
      case REGISTER_USER_SUCCEEDED:
        const { countryCode, email, mobile } = userData
        let user_id = userData.id
        if (getNestedObjKey(userData, ["loginDetails", "access_token"])) {
          gblFunc.storeRegAuthDetails(userData.loginDetails)
          this.liteTimer = setTimeout(() => {
            this.props.fetchUserRules({
              userid: getNestedObjKey(userData, ["loginDetails", "userId"])
            });
          }, 200)
        } else if (countryCode && email && user_id && mobile) {
          this.props.history.push({
            pathname: "/otp", mobile: mobile, countryCode: countryCode, userId: user_id,
            state: {
              location: getNestedObjKey(this.props, ["location", "state", "location"]),
              mobile: mobile,
              countryCode: countryCode,
              userId: user_id
            }
          })
        }
        break
      case REGISTER_USER_FAILED:
        let errCode = getNestedObjKey(serverErrorRegister, ["response", "data", "errorCode"])
        if (errCode == 1108) {
          this.setState({
            isAlertShow: true,
            alertStatus: false,
            alertMsg: "Your mobile number is already registered with us. If you remember your registered email id, please use login to sign in. In case you have forgotten your password, please use 'forgot password' to retrieve your account details."
          })
        } else if (serverErrorRegister == "User with given email already exists." || errCode == 702) {
          if (this.props.type != nextProps.type) {
            this.setState({
              isAlertShow: true,
              alertStatus: false,
              alertMsg: "Your email ID is already registered with us. If you don't remember your password, please use 'forgot password' to retrieve your account details."
            })
          }
        } else if (errCode == 500) {
          this.setState({
            isAlertShow: true,
            alertStatus: false,
            alertMsg: "Server error",
          })
        }
        else {
          if (this.props.type != nextProps.type) {
            this.setState({
              isAlertShow: true,
              alertStatus: false,
              alertMsg: serverErrorRegister,
              email: "",
              password: "",
              conf_password: ""
            })
          }
        }
        break
      case "VERIFY_TOKEN":
        if (getNestedObjKey(userLoginData, ["data", "errorCode"]) == 1001) {
          if (nextProps.type != this.props.type) {
            this.props.history.push({
              pathname: "/otp", userId: getNestedObjKey(userLoginData, ["data", "data", "userId"]),
              state: {
                location: getNestedObjKey(this.props, ["location", "state", "location"]),
                userId: getNestedObjKey(userLoginData, ["data", "data", "userId"]),
                temp_Token: getNestedObjKey(nextProps, ["userLoginData", "data", "data", "token"]),
                countryCode: getNestedObjKey(nextProps, ["userLoginData", "data", "data", "countryCode"])
              }
            })
          }
        }
        if (
          nextProps.userLoginData &&
          nextProps.userLoginData.data &&
          nextProps.userLoginData.data.data &&
          nextProps.userLoginData.data.data.mobileVerified == "0"
        ) {
          if (nextProps.type != this.props.type) {
            this.props.history.push({
              pathname: "/otp",
              state: {
                location: getNestedObjKey(this.props, ["location", "state", "location"]),
                mobile: getNestedObjKey(nextProps, ["userLoginData", "data", "data", "mobile"]),
                countryCode: getNestedObjKey(nextProps, ["userLoginData", "data", "data", "countryCode"]),
                userId: getNestedObjKey(nextProps, ["userLoginData", "data", "data", "userId"]),
                temp_Token: getNestedObjKey(nextProps, ["userLoginData", "data", "data", "token"]),
              }
            })
          }
        }
        break
      case LOGIN_USER_SUCCEEDED:
        let { userId } = nextProps.userLoginData;
        const userid =
          typeof window !== "undefined"
            ? window.localStorage.getItem("userId")
            : "";
        // Store auth token
        // Fix for auth token
        gblFunc.storeRegAuthDetails(nextProps.userLoginData);
        this.timer = setTimeout(() => {
          this.props.fetchUserRules({
            userid: userId
          });
        }, 0);
        break;
      case FETCH_USER_RULES_SUCCESS:
        //SAVE USER RELEVANT DATA IN LOCALSTORAGE HERE.
        //USER IS AUTHENTICATED IN THIS CASE ONLY ( USER RULES ARE FETCHED AND TOKEN IS FETCHED)
        const { userRules, id } = userRulesData
        let country_code = userRulesData.countryCode
        gblFunc.storeUserDetails(userRulesData)
        // gblFunc.storeRegAuthDetails(this.props.userLoginData);
        let isUserRuleActive = 0
        let userInfo = {}
        let requiredRules = 3
        if (country_code == +977 || country_code == +95) {
          requiredRules = 2
        }
        userInfo["countryCode"] = country_code
        userInfo["userId"] = id
        if (userRules && userRules.length) {
          userRules.map(x => {
            if (x.ruleTypeId == 1 && x.ruleId) {
              isUserRuleActive++
              userInfo["class"] = x.ruleId
            }
            if (x.ruleTypeId == 5 && x.ruleId) {
              userInfo["gender"] = x.ruleId
              isUserRuleActive++
            }
            if (x.ruleTypeId == 21 && x.ruleId) {
              userInfo["state"] = x.ruleId
              isUserRuleActive++
            }
            return x
          })
        } else {
          this.setState({ isRedirectUserInfo: true, countryCode: country_code }, () => {
            localStorage.setItem("isAuth", 0);
            this.props.history.push({
              pathname: "/user-info", userRulesData: nextProps.userRulesData,
              state: { countryCode: country_code },
              location: getNestedObjKey(this.props, ["location", "state", "location"])
            })
          })
          return
        }
        if (isUserRuleActive == requiredRules && this.state.isTrue) {
          this.setState({ isTrue: false })
          localStorage.setItem("isAuth", 1)
          localStorage.setItem("userId", id)
          if (this.state.redirectUrlState.pathname !== "/register") {

            this.setState({ currentPath: true })
            if (nextProps.userRulesData && nextProps.userRulesData.vleUser) {
              this.setState({ userRulesData: nextProps.userRulesData })
              if (
                nextProps.userRulesData.cscId &&
                nextProps.userRulesData.cscId != null
              ) {
                return this.props.history.push("/subscribers");
              }
              return this.props.history.push("/vle/add-student");
            } else {
              return this.props.history.push(this.state.redirectUrlState);
            }
          } else {
            this.props.history.push("/");
          }
        }
        else if (isUserRuleActive < requiredRules) {
          this.setState({ isRedirectUserInfo: true, userInfo: userInfo })
          localStorage.setItem("isAuth", 0);
        }
        break;
      case GET_EXISTING_MOBILE_FAILURE:
        if (nextProps.type != this.props.type) {
          this.setState({
            isAlertShow: true,
            alertStatus: false,
            mobile: "",
            alertMsg: "Your mobile number is already registered with us. If you remember your registered email id, please use login to sign in. In case you have forgotten your password, please use 'forgot password' to retrieve your account details."
          })
        }
        break
    }
  }
  componentWillUnmount() {
    clearTimeout(this.timer)
    clearTimeout(this.liteTimer)
  }
  checkFormValidations() {
    let validations = { ...this.state.validations };
    const shouldBeTrimmed = id =>
      id === "firstName" || id === "lastName" || id === "email";
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        shouldBeTrimmed(key) ? this.state[key].trim() : this.state[key],
        key,
        name,
        ...validationFunctions
      );

      validations[key] = validationResult[key];

      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }

    this.setState({
      validations,
      isFormValid
    });

    return isFormValid;
  }

  onRegisterClick(event) {
    event.preventDefault();
    /* call for register user */
    if (this.checkFormValidations()) {
      let mappedUserApiObject = this.mapLocalStateToApiCall(this.state);

      this.props.registerUser(mappedUserApiObject);
    }
  }

  mapLocalStateToApiCall({
    email,
    password,
    firstName,
    lastName,
    mobile,
    countryCode
  }) {
    /*  Sample API call
"email": "abhinav.misra@buddy4study.com",
"userSource": /Android/i.test(navigator.userAgent)? "EMAIL-MOBILE-WEBSITE": "EMAIL-WEBSITE",
"password": "repassword",
"firstName": "Abhinav",
"lastName": "Mishra",
"mobile": "9953189925"  */

    return {
      email,
      userSource: /Android/i.test(navigator.userAgent)
        ? "EMAIL-MOBILE-WEBSITE"
        : "EMAIL-WEBSITE",
      password,
      firstName,
      lastName,
      mobile,
      countryCode,
      requireOtp: true
    };
  }

  /*

ORDER OF VALIDATION FUNCTIONS MATTERS.

*/

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "email":
        validationObject.name = "*Email address";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;
      case "firstName":
        validationObject.name = "*First name";
        validationObject.validationFunctions = [required, minLength(1), isChar];
        return validationObject;
      case "lastName":
        validationObject.name = "*Last name";
        validationObject.validationFunctions = [minLength(1)];
        return validationObject;
      case "mobile":
        validationObject.name = "*Mobile number";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;
      case "password":
        validationObject.name = "*Password ";
        validationObject.validationFunctions = [required, minLength(6)];
        return validationObject;
      case "conf_password":
        validationObject.name = "*Passwords ";
        validationObject.validationFunctions = [
          shouldMatch(this.state.password)
        ];
        return validationObject;
    }
  }

  shouldFieldUpdate(id, value) {
    switch (id) {
      case "mobile":
        return value.length <= 10;
      default:
        return true;
    }
  }

  onFieldChange(event) {
    const shouldBeTrimmed = id =>
      id === "firstName" || id === "lastName" || id === "email";

    let { value, id } = event.target;
    let shouldUpdate = true;

    let { name, validationFunctions } = this.getValidationRulesObject(id);
    let validationResult = ruleRunner(
      shouldBeTrimmed(id) ? value.trim() : value,
      id,
      name,
      ...validationFunctions
    );

    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    let newState = { validations };

    if (this.shouldFieldUpdate(id, value)) {
      newState[id] = value;
    }
    this.setState(newState);
  }
  closePopup() {
    this.setState({
      isAlertShow: false,
      alertStatus: "",
      alertMsg: "",
    })
  }

  /***********Social Login Call back Start******** */
  onFacebookLoginSuccess(data) {
    let user = data._profile;
    let objectData = {
      username: user.email,
      grant_type: "password",
      socialLogin: true,
      userData: {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        userSource: /Android/i.test(navigator.userAgent)
          ? "FB-MOBILE-WEBSITE"
          : "FB-WEBSITE",
        portalId: 1,
        socialLogin: true
      }
    };
    this.props.socialLogin(objectData);
  }
  onFacebookLoginFailure(err) {
    this.setState(
      {
        fbError: true
      },
      () => this.setState({ fbError: false })
    );
  }
  onGoogleLoginSuccess(data) {
    let user = data._profile;
    let objectData = {
      username: user.email,
      grant_type: "password",
      socialLogin: true,
      userData: {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        userSource: /Android/i.test(navigator.userAgent)
          ? "GOOGLE-MOBILE-WEBSITE"
          : "GOOGLE-WEBSITE",
        portalId: 1,
        socialLogin: true
      }
    };
    this.props.socialLogin(objectData);
  }

  onGoogleLoginFailure(err) {
    this.setState(
      {
        googleError: true
      },
      () => this.setState({ googleError: false })
    );
  }
  /*********************************************** */

  mobileStatus(e) {
    let num = e.target.value
    if (num.toString().length == 10) {
      this.props.existingMobileStatus({ mobile: num })
    }
  }
  render() {
    const { userId, email, firstName } = gblFunc.getStoreUserDetails()
    if (this.state.isRedirectUserInfo) {
      if (typeof window !== "undefined") {
        window.localStorage.setItem("userInfo", JSON.stringify(this.state.userInfo))
      }
      return (
        <Redirect to={{
          pathname: '/user-info',
          state: {
            userRulesData: this.state.userRulesData,
            location: getNestedObjKey(this.props, ["location", "state", "location"]),
            countryCode: this.state.countryCode
          }
        }} />
      )
    }
    else if (userId && email && firstName && !this.state.currentPath) {
      this.props.history.push("/")
    }
    const { isAlertShow, alertStatus, alertMsg } = this.state
    return (
      <div>
        <Loader isLoader={this.props.showLoader} />
        <AlertMessagePopup
          isShow={isAlertShow}
          status={alertStatus}
          msg={alertMsg}
          close={this.closePopup}
        />
        <section className="container registerform">
          <h3>Get details from</h3>

          <article className="moboContentScroll">
            <article className="socialLoginWrapper">
              {this.state.fbError ? null : (
                <SocialButton
                  className="btn facebook"
                  key="1"
                  provider="facebook"
                  appId="266013666868531" //buddy4study official website buddy4study@gmail.com
                  onLoginSuccess={this.onFacebookLoginSuccess}
                  onLoginFailure={this.onFacebookLoginFailure}
                >
                  Facebook
            </SocialButton>
              )}
              {this.state.googleError ? null : (
                <SocialButton
                  className="btn googleplus"
                  provider="google"
                  key="2"
                  appId="913333428875-ca46kak1a1nvlq9ul30e6nned0t2sr98.apps.googleusercontent.com" //info@buddy4study.com
                  onLoginSuccess={this.onGoogleLoginSuccess}
                  onLoginFailure={this.onGoogleLoginFailure}
                >
                  Google+
            </SocialButton>
              )}
            </article>
          </article>
          <form className="formelemt" name="regForm" autoCapitalize="off">
            <section className="row">
              <section className="ctrl-wrapper">
                <article className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    name="FIRST_NAME"
                    placeholder="First Name"
                    id="firstName"
                    required
                    value={this.state.firstName}
                    onChange={this.onFieldChange}
                  />
                  {this.state.validations.firstName ? (
                    <dd className="animated bounce error1">
                      {this.state.validations.firstName}
                    </dd>
                  ) : null}
                </article>
              </section>
              <section className="ctrl-wrapper">
                <article className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    name="LAST_NAME"
                    placeholder="Last Name"
                    id="lastName"
                    required=""
                    value={this.state.lastName}
                    onChange={this.onFieldChange}
                  />
                  {this.state.validations.lastName ? (
                    <dd className="animated bounce error1">
                      {this.state.validations.lastName}
                    </dd>
                  ) : null}
                </article>
              </section>
            </section>

            <section className="row">
              <section className="ctrl-wrapper">
                <article className="form-group">
                  <select
                    className="form-control"
                    value={this.state.countryCode}
                    onChange={e => this.setState({ countryCode: e.target.value })}
                  >
                    <option value="+91">India(+91)</option>
                    <option value="+977">Nepal(+977)</option>
                    <option value="+95">Myanmar(+95)</option>
                  </select>
                </article>
              </section>
              <section className="ctrl-wrapper">
                <article className="form-group">
                  <input
                    type="tel"
                    className="form-control"
                    name="MOBILE_NUMBER"
                    id="mobile"
                    autoComplete="off "
                    placeholder="Enter mobile number"
                    value={this.state.mobile}
                    onChange={this.onFieldChange}
                    onBlur={this.mobileStatus}
                  />
                  {this.state.validations.mobile ? (
                    <dd className="animated bounce error1">
                      {this.state.validations.mobile}
                    </dd>
                  ) : null}
                </article>
              </section>
            </section>
            <section className="row">
              <section className="ctrl-wrapper">
                <article className="form-group">
                  <input
                    type="email"
                    name="EMAIL"
                    className="form-control"
                    placeholder="Email"
                    id="email"
                    required
                    value={this.state.email}
                    onChange={this.onFieldChange}
                  />
                  {this.state.validations.email ? (
                    <dd className="animated bounce error1">
                      {this.state.validations.email}
                    </dd>
                  ) : null}
                </article>
              </section>
              <section className="ctrl-wrapper ">
                <article className="form-group">
                  <input
                    type="password"
                    className="form-control"
                    name="PASSWORD"
                    placeholder="Password"
                    id="password"
                    required
                    value={this.state.password}
                    onChange={this.onFieldChange}
                  />
                  {this.state.validations.password ? (
                    <dd className="animated bounce error1">
                      {this.state.validations.password}
                    </dd>
                  ) : null}
                </article>
              </section>
            </section>
            <section className="row">
              <section className="ctrl-wrapper ">
                <article className="form-group">
                  <input
                    type="password"
                    className="form-control"
                    name="CONF_PASSWORD"
                    placeholder="Confirm password"
                    id="conf_password"
                    required
                    pwd-verify="regpwd"
                    value={this.state.conf_password}
                    onChange={this.onFieldChange}
                  />
                  {this.state.validations.conf_password ? (
                    <dd className="animated bounce error1">
                      {this.state.validations.conf_password}
                    </dd>
                  ) : null}
                </article>
              </section>
            </section>
            <section className="row btnWrapper">
              <p className="tc">
                By clicking on any of the buttons, you agree to Buddy4Study's
              <a href="/terms-and-conditions">Terms & Conditions</a>
              </p>
              <button
                type="submit"
                className="btn-block greenBtn"
                onClick={this.onRegisterClick}
              >
                {"Register"}
              </button>
            </section>
          </form>
        </section>
      </div>
    );
  }
}

export default RegisterForm;
