import React, { Component } from "react";
import Loader from "../../common/components/loader";
import { ruleRunner } from "../../../validation/ruleRunner";
import { required, isEmailMobile, isEmail, isMobileNumber, isNumeric, minLength, maxLength } from "../../../validation/rules";
import { messages } from "../../../constants/constants";
import OtpInput from "react-otp-input";
import gblFunc from "../../../globals/globalFunctions";

class ForgotPassWordPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      mobile: null,
      isFormValid: false,
      otpEntered: "",
      isCallFire: false,
      disableOTP: false,
      isActiveVarifyBtn: false,
      validations: {
        userName: null,
        // email: null,
        // mobile: null,
      }
    };
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.submitForgotPassword = this.submitForgotPassword.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.onBlurChange = this.onBlurChange.bind(this);
  }

  submitForgotPassword(event) {
    event.preventDefault();
    const { email, mobile } = this.state;

    if (this.checkFormValidations()) {
      if (this.state.email) {
        //console.log('email,mobile', this.state.mobile)
        const params = { email, portalId: 1 };
        this.props.forgotUserPassword({ params });
      }
      if (this.state.mobile) {
        //console.log('mobile', this.state.mobile)
        this.props.submitLoginOtp(this.state.mobile)
      }
    }
  }

  submitOtp(e) {
    e.preventDefault()
    this.props.otpPasswordChange(this.state.otpEntered)
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  handleFieldChange(otp) {
    // console.log('otp', otp);
    // console.log(this.state.otpEntered)
    if (otp.length == 6) {
      this.setState({ otpEntered: otp, isActiveVarifyBtn: true });
    } else {
      this.setState({ otpEntered: otp, isActiveVarifyBtn: false });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.serverError) {
      let validations = { ...this.state.validations };
      const httpErrorCode =
        nextProps.serverError && nextProps.serverError.status
          ? nextProps.serverError.status
          : "";

      validations.email = messages.generic.httpErrors[httpErrorCode];
      this.setState({
        validations
      });
    }
  }

  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "userName":
        validationObject.name = "* Email/Mobile ";
        validationObject.validationFunctions = [required, isEmailMobile];
        return validationObject;



      // case "mobile":
      //   validationObject.name = "* Mobile ";
      //   validationObject.validationFunctions = [required, isMobileNumber];
      //   return validationObject;
    }
  }
  /************ end - validation part - get message and required validation******************* */

  /************ start - validation part - check form submit validation******************* */
  onBlurChange(event) {
    const { id, value } = event.target;
    // const { name, validationFunctions } = this.getValidationRulesObject(id, value);
    // const validationResult = ruleRunner(
    //   value,
    //   id,
    //   name,
    //   ...validationFunctions
    // );
    // let validations = { ...this.state.validations };
    // validations[id] = validationResult[id];

    if (value.includes('@')) {
      //console.log('value in email', value)
      this.setState({
        email: value
      });
    } else {
      //console.log('value in mobile', value);
      this.setState({
        mobile: value
      })
    }
    // this.setState({
    //   [id]: value,
    //   validations
    // });
  }

  onFormFieldChange(event) {
    const { id, value } = event.target;
    //console.log(value, 'forgotPassword Value')
    const { name, validationFunctions } = this.getValidationRulesObject(id, value);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );
    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    // if (value.includes('@')) {
    //   this.setState({
    //     email: value
    //   });
    // } else {
    //   this.setState({
    //     mobile: value
    //   })
    // }
    this.setState({
      [id]: value,
      validations
    });
  }

  render() {
    const { isShowOtp } = this.props
    const { isCallFire, disableOTP, otpTimedOut, mobile } = this.props;
    return (
      <article className="popup">
        <Loader isLoader={this.props.showLoader} />
        <article className="popupsms">
          <article className="popup_inner loginpopup">
            <article className="LoginRegPopup">
              <section className="modal fade modelAuthPopup1">
                <section className="modal-dialog">
                  <section className="modal-content modelBg  emailverification">
                    <article className="modal-header">
                      <h3 className="modal-title">Forgot Password</h3>
                      {!this.props.hideForgetCloseBtn && (
                        <button
                          type="button"
                          className="close btnPos"
                          onClick={this.props.closePopup}
                        >
                          <i>&times;</i>
                        </button>
                      )}
                    </article>

                    <article className="modal-body forgot top0">
                      {!isShowOtp &&
                        <article className="row">
                          <form
                            name="forgotForm"
                            className="formelemt"
                            autoCapitalize="off"
                            onSubmit={this.submitForgotPassword}
                          >
                            <section className="row">
                              <section className="ctrl-wrapper">
                                <article className="form-group">
                                  <input
                                    type="text"
                                    name="emailMobile"
                                    className="form-control"
                                    placeholder="Enter your Email/mobile"
                                    id="userName"
                                    value={this.state.userName}
                                    onChange={this.onFormFieldChange}
                                    onBlur={this.onBlurChange}
                                  />
                                  {this.state.validations.userName ? (
                                    <span className="error1 animated bounce">
                                      {this.state.validations.userName}
                                    </span>
                                  ) : null}
                                </article>
                              </section>
                            </section>

                            <section className="row">
                              <button
                                type="submit"
                                className="btn-block greenBtn recover"
                              >
                                Recover password
                              </button>
                            </section>

                            <article className="row tc">
                              <span
                                onClick={this.props.showLoginRegisterForm}
                                className="handPointer"
                              >
                                Back
                              </span>
                            </article>
                          </form>
                        </article>}

                      {isShowOtp && (
                        <form onSubmit={(e) => this.submitOtp(e)} className="formelemt">
                          <article className="loginotp">
                            <article className="container-fluid otpbg">
                              <article className="otpWrapper otpmobile">
                                <article className="formWrapper forogtotp">
                                  <article className="cellWrapper">
                                    <article className="width100">
                                      <p>{this.props.mobile}</p>
                                      <article className="form-group otp">
                                        <OtpInput
                                          numInputs={6}
                                          separator={<span>-</span>}
                                          onChange={otp => this.handleFieldChange(otp)}
                                        />
                                      </article>
                                    </article>
                                  </article>
                                  {!this.props.disableOTP && !this.props.isCallFire ? (
                                    <React.Fragment>
                                      {this.props.otpTimedOut && (
                                        <button
                                          className="resendbut linkButton "
                                          style={{
                                            color: "blue",
                                            textDecorationLine: "underline"
                                          }}
                                          onClick={() => {
                                            this.props.requestOTPCount();
                                          }}
                                          type="button"
                                        >
                                          Request OTP on call
                          </button>
                                      )}
                                    </React.Fragment>
                                  ) : (
                                      <React.Fragment>
                                        {isCallFire ?
                                          <p>
                                            We are trying to call you with OTP
                     </p>
                                          : <p>
                                            Maximum attempts reached. Please try again after some
                                            time.
                      </p>
                                        }
                                      </React.Fragment>
                                    )}

                                  <button
                                    disabled={!this.state.isActiveVarifyBtn}
                                    type="submit"
                                    onClick={() => gblFunc.gaTrack.trackEvent(["Verify OTP", "VerifywithOTP"])}
                                    className="btn-block greenBtn otpbtn">
                                    Verify OTP
                            </button>

                                </article>
                              </article>
                            </article>
                          </article>
                        </form>)}
                    </article>
                  </section>
                </section>
              </section>
            </article>
          </article>
        </article>
      </article>
    );
  }
}

export default ForgotPassWordPopup;
