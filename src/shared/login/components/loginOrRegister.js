import React, { Component } from "react";
import { Link } from "react-router-dom";

import SocialButton from "./socialLogin";
import RegisterForm from "./registerForm";
import LoginForm from "./loginForm";
import CompanyInfoSection from "./static/companyInfoSection";
import { LOGIN_USER_SUCCEEDED, REGISTER_USER_SUCCEEDED } from "../actions";

import {
  loginRegisterRequiredRules,
  requiredRuleKeys
} from "../../../constants/constants";

import gblFunc from "../../../globals/globalFunctions";
import ForgotPasswordPopup from "./forgotPasswordPopup";
import Loader from "../../common/components/loader";

class UserLoginRegistration extends Component {
  constructor(props) {
    super(props);
    this.changeTab = this.changeTab.bind(this);
    this.renderCurrentForm = this.renderCurrentForm.bind(this);
    this.onFacebookLoginSuccess = this.onFacebookLoginSuccess.bind(this);
    this.onFacebookLoginFailure = this.onFacebookLoginFailure.bind(this);
    this.onGoogleLoginSuccess = this.onGoogleLoginSuccess.bind(this);
    this.onGoogleLoginFailure = this.onGoogleLoginFailure.bind(this);
    this.redirectReg = this.redirectReg.bind(this);
    this.state = {
      currentTab: "LOGIN",
      fbError: false,
      googleError: false,
      isVLElogin: false
    };
  }

  /***********Social Login Call back Start******** */
  onFacebookLoginSuccess(data) {
    let user = data._profile;
    let objectData = {
      username: user.email,
      grant_type: "password",
      socialLogin: true,
      userData: {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        userSource: /Android/i.test(navigator.userAgent)
          ? "FB-MOBILE-WEBSITE"
          : "FB-WEBSITE",
        portalId: 1,
        socialLogin: true
      }
    };
    this.props.socialLogin(objectData);
  }
  componentDidMount() {
    this.setState({
      currentTab: this.props.currentTab,
      isVLElogin: this.props.isVLElogin
    });
  }
  onFacebookLoginFailure(err) {
    this.setState(
      {
        fbError: true
      },
      () => this.setState({ fbError: false })
    );
  }
  onGoogleLoginSuccess(data) {
    let user = data._profile;
    let objectData = {
      username: user.email,
      grant_type: "password",
      socialLogin: true,
      userData: {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        userSource: /Android/i.test(navigator.userAgent)
          ? "GOOGLE-MOBILE-WEBSITE"
          : "GOOGLE-WEBSITE",
        portalId: 1,
        socialLogin: true
      }
    };
    this.props.socialLogin(objectData);
  }

  onGoogleLoginFailure(err) {
    this.setState(
      {
        googleError: true
      },
      () => this.setState({ googleError: false })
    );
  }
  /*********************************************** */

  closeEmailPopup() {
    this.setState({
      currentTab: "LOGIN"
    });
  }

  /*
  Handler function which triggers form re render on tab change.
  */

  changeTab(event) {
    if (event.target.id === "LOGIN" || event.target.id === "REGISTER") {
      this.setState({ currentTab: event.target.id });
    }
  }
  redirectReg() {
    // this.props.closePopup()
    this.props.allProps.history.push({
      pathname: "/register",
      state: {
        location: this.props.location
      }
    })
  }
  /*
  Render login / registration form on tab change
  */

  renderCurrentForm() {
    const { currentTab } = this.state;
    switch (currentTab) {
      case "REGISTER":
        return (
          <LoginForm
            isVLElogin={this.state.isVLElogin}
            loginUser={this.props.loginUser}
            showForgotPasswordForm={this.props.showForgotPasswordForm}
            serverError={this.props.serverErrorLogin}
            registerUser={this.props.registerUser}
            //serverError={this.props.serverErrorRegister || ""}
            submitLoginOtp={this.props.submitLoginOtp}
            isShowOtp={this.props.isShowOtp}
            sendOtpForLogin={this.props.sendOtpForLogin}
            requestOTPCount={this.props.requestOTPCount}
            isCallFire={this.props.isCallFire}
            disableOTP={this.props.disableOTP}
            otpTimedOut={this.props.otpTimedOut}
            mobile={this.props.mobile}
          />
          // <RegisterForm
          //   registerUser={this.props.registerUser}
          //   serverError={this.props.serverErrorRegister || ""}
          //   submitLoginOtp={this.props.submitLoginOtp}
          //   isShowOtp={this.props.isShowOtp}
          //   sendOtpForLogin={this.props.sendOtpForLogin}
          //   requestOTPCount={this.props.requestOTPCount}
          //   isCallFire={this.props.isCallFire}
          //   disableOTP={this.props.disableOTP}
          //   otpTimedOut={this.props.otpTimedOut}
          //   mobile={this.props.mobile}
          // />
        );

      case "LOGIN":
        return (
          <LoginForm
            isVLElogin={this.state.isVLElogin}
            loginUser={this.props.loginUser}
            showForgotPasswordForm={this.props.showForgotPasswordForm}
            serverError={this.props.serverErrorLogin}
            registerUser={this.props.registerUser}
            //serverError={this.props.serverErrorRegister || ""}
            submitLoginOtp={this.props.submitLoginOtp}
            isShowOtp={this.props.isShowOtp}
            sendOtpForLogin={this.props.sendOtpForLogin}
            requestOTPCount={this.props.requestOTPCount}
            isCallFire={this.props.isCallFire}
            disableOTP={this.props.disableOTP}
            otpTimedOut={this.props.otpTimedOut}
            mobile={this.props.mobile}
          />
        );
      default:
        return null;
    }
  }

  getTitle() {
    const currentTab = this.state.currentTab;

    switch (currentTab) {
      case "LOGIN":
        return "Please Login";
      case "REGISTER":
        return "Please Login";
    }
  }

  render() {
    const title = this.getTitle();
    const { isVLElogin } = this.state;
    return (
      <article className="popup">
        <Loader isLoader={this.props.showLoader} />
        <article className="popupsms">
          <article className="popup_inner loginpopup">
            <article className="LoginRegPopup">
              <section className="modal fade modelAuthPopup1">
                <section className="modal-dialog">
                  <section
                    className={`modal-content modelBg ${
                      this.state.showForgotPasswordPopup
                        ? "login emailverification"
                        : "login"
                      } `}
                  >
                    <article>
                      <article className="modal-header">
                        <h3 className="modal-title">{title}</h3>
                        {!isVLElogin && (
                          <div>
                            {this.props.hideCloseButton ? null : (
                              <button
                                type="button"
                                className="close"
                                onClick={this.props.closePopup}
                              >
                                <i>&times;</i>
                              </button>
                            )}
                          </div>
                        )}
                      </article>
                      <article className="modal-body">
                        <article className="row">
                          {/* <CompanyInfoSection /> */}
                          <LoginRegisterFormContent
                            isVLElogin={isVLElogin}
                            renderCurrentForm={this.renderCurrentForm}
                            changeTab={this.changeTab}
                            currentTab={this.state.currentTab}
                            fbError={this.state.fbError}
                            googleError={this.state.googleError}
                            onFacebookLoginSuccess={this.onFacebookLoginSuccess}
                            onFacebookLoginFailure={this.onFacebookLoginFailure}
                            onGoogleLoginSuccess={this.onGoogleLoginSuccess}
                            onGoogleLoginFailure={this.onGoogleLoginFailure}
                          />
                          <p>Don't have an account? <a onClick={(e) => {
                            this.props.closePopup();
                            this.redirectReg();
                          }}>Register</a></p>
                          <p className="footerText">
                            If you are having problem in login, please contact{" "}
                            <a href="mailto:info@buddy4study.com">
                              info@buddy4study.com
                            </a>
                          </p>
                        </article>
                      </article>
                    </article>
                  </section>
                </section>
              </section>
            </article>
          </article>
        </article>
      </article>

    );
  }
}

const CloseModalButton = props => {
  if (props.needMoreDetails) {
    return null;
  } else {
    return (
      <button type="button" className="close" onClick={props.closePopup}>
        <i>&times;</i>
      </button>
    );
  }
};

const LoginRegisterFormContent = props => (
  <article className="col-md-12 mg-btm">
    <article className="moboContentScroll">
      {!props.isVLElogin && (
        <article className="socialLoginWrapper">
          {props.fbError ? null : (
            <SocialButton
              className="btn facebook"
              key="1"
              provider="facebook"
              appId="266013666868531" //buddy4study official website buddy4study@gmail.com
              onLoginSuccess={props.onFacebookLoginSuccess}
              onLoginFailure={props.onFacebookLoginFailure}
            >
              Facebook
            </SocialButton>
          )}
          {props.googleError ? null : (
            <SocialButton
              className="btn googleplus"
              provider="google"
              key="2"
              appId="913333428875-ca46kak1a1nvlq9ul30e6nned0t2sr98.apps.googleusercontent.com" //info@buddy4study.com
              onLoginSuccess={props.onGoogleLoginSuccess}
              onLoginFailure={props.onGoogleLoginFailure}
            >
              Google+
            </SocialButton>
          )}
        </article>
      )}
      {!props.isVLElogin && <h2 className="no-span">Login with Email/Mobile</h2>}
      <article
        className={
          props.isVLElogin
            ? "outer-tab-wrapper vleMarginTop"
            : "outer-tab-wrapper"
        }
      >
        {/* <LoginRegisterTab
          changeTab={props.changeTab}
          currentTab={props.currentTab}
          isVLElogin={props.isVLElogin}
        /> */}
        <div>{props.renderCurrentForm()}</div>
      </article>
      <p className="footerText1">
        If you are having problem in login, please contact{" "}
        <a href="mailto:info@buddy4study.com">info@buddy4study.com</a>
      </p>
    </article>
  </article>
);

const LoginRegisterTab = props => {
  const { currentTab, changeTab } = props;
  let registerClass = "",
    loginClass = "";
  if (currentTab === "REGISTER") {
    registerClass = "current";
  } else {
    loginClass = "current";
  }

  return (
    <ul onClick={changeTab} className="tabs">
      <li
        id="LOGIN"
        className={
          props.isVLElogin ? `loginClass vleTheme ${loginClass}` : loginClass
        }
      >Email</li>
      {!props.isVLElogin && (
        <li id="REGISTER" className={registerClass}>
          Mobile
        </li>
      )}
      {/* <li id="LOGIN" className={loginClass}>
        Login
        </li> */}


    </ul>
  );
};

export default UserLoginRegistration;
