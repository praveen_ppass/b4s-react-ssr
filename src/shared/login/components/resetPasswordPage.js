import React, { Component } from "react";
import querystring from "query-string";
import Loader from "../../common/components/loader";
import AlertMessagePopup from "../../../shared/common/components/alertMsg";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import { Helmet } from "react-helmet";
import {
  RESET_USER_PASSWORD_SUCCESS,
  RESET_USER_PASSWORD_TOKEN_CHECK_SUCCESS,
  RESET_USER_PASSWORD_TOKEN_CHECK_FAILURE
} from "../actions";
import { ruleRunner } from "../../../validation/ruleRunner";
import { required, minLength, shouldMatch } from "../../../validation/rules";
import { messages } from "../../../constants/constants";
import { Redirect } from "react-router-dom";

class ResetPasswordPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: "",
      confirmNewPassword: "",
      token: "",
      showAlertPopup: false,
      isFormValid: false,
      validations: {
        newPassword: null,
        confirmNewPassword: null
      },
      expired: false,
      status: false
    };
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.submitResetPassword = this.submitResetPassword.bind(this);
    this.closeAlertPopup = this.closeAlertPopup.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
  }
  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "newPassword":
        validationObject.name = "* New password ";
        validationObject.validationFunctions = [required, minLength(6)];
        return validationObject;
      case "confirmNewPassword":
        validationObject.name = "* Confirm new password ";
        validationObject.validationFunctions = [required, minLength(6)];
        validationObject.validationFunctions = [
          shouldMatch(this.state.newPassword)
        ];
        return validationObject;
    }
  }
  /************ end - validation part - get message and required validation******************* */

  submitResetPassword(event) {
    event.preventDefault();
    if (this.checkFormValidations()) {
      const { newPassword, token } = this.state;
      const params = { newPassword, token, portalId: 1 };
      this.props.resetUserPassword({ params });
    }
  }

  closeAlertPopup() {
    this.setState({
      showAlertPopup: false,
      newPassword: "",
      confirmNewPassword: ""
    });

    this.props.history.push({
      pathname: "/",
      state: {
        from: "resetPassword",
        isTrue: this.state.expired ? true : false
      }
    });
  }

  componentDidMount() {
    const { query } = querystring.parse(this.props.location.search);
    const { token } = querystring.parse(this.props.location.search);
    this.setState(
      {
        token: query || token
      },
      () => this.props.resetTokenCheck({ token: query || token, portalId: 1 })
    );
  }

  onFormFieldChange(event) {
    const { id, value } = event.target;
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );
    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];
    this.setState({
      [id]: value,
      validations
    });
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case RESET_USER_PASSWORD_TOKEN_CHECK_FAILURE:
        this.setState({
          showAlertPopup: true,
          expired: true,
          messages: messages.resetPasswordTokenExpire.success,
          status: false
        });
        break;
      case RESET_USER_PASSWORD_SUCCESS:
        this.setState({
          showAlertPopup: true,
          messages: messages.resetPassword.success,
          status: true
        });
        break;
    }
  }

  render() {
    return (
      <section className="forgot">
        <Loader isLoader={this.props.showLoader} />
        {this.state.showAlertPopup ? (
          <AlertMessagePopup
            msg={this.state.messages}
            isShow={this.state.showAlertPopup}
            status={this.state.status}
            close={this.closeAlertPopup}
          />
        ) : null}
        <Helmet> </Helmet>
        <BreadCrum
          classes={breadCrumObj["forgotpassword"]["bgImage"]}
          listOfBreadCrum={breadCrumObj["forgotpassword"]["breadCrum"]}
          title={breadCrumObj["forgotpassword"]["title"]}
          subTitle={breadCrumObj["forgotpassword"]["subTitle"]}
        />

        <section className="container pos-relative equial-padding">
          <section className="row content-row">
            <article className="social-box">
              <article id="changePassword" className="row fwd">
                <form
                  name="forgotPassForm"
                  autoCapitalize="off"
                  onSubmit={this.submitResetPassword}
                >
                  <section className="ctrl-wrapper widget-border">
                    <h2>Forgot Password</h2>
                    <article className="form-group">
                      <input
                        type="password"
                        placeholder="New Password"
                        id="newPassword"
                        onChange={this.onFormFieldChange}
                        className="form-control"
                        value={this.state.newPassword}
                      />
                      {this.state.validations.newPassword ? (
                        <span className="error animated bounce">
                          {this.state.validations.newPassword}
                        </span>
                      ) : null}
                    </article>

                    <article className="form-group">
                      <input
                        type="password"
                        className="form-control"
                        id="confirmNewPassword"
                        placeholder="Re-type Password"
                        onChange={this.onFormFieldChange}
                        value={this.state.confirmNewPassword}
                      />
                      {this.state.validations.confirmNewPassword ? (
                        <span className="error animated bounce">
                          {this.state.validations.confirmNewPassword}
                        </span>
                      ) : null}
                    </article>
                  </section>
                  <article className="btn-ctrl">
                    <button type="submit" className="btn btn-block greenBtn">
                      Submit
                    </button>
                  </article>
                </form>
              </article>
            </article>
          </section>
        </section>
      </section>
    );
  }
}
export default ResetPasswordPage;
