import React, { Component } from "react";
import querystring from "query-string";

import UserLoginRegistrationPopup from "../../../shared/login/containers/userLoginRegistrationContainer";
import Loader from "../../common/components/loader";
import gblFunc from "../../../globals/globalFunctions";
import { Redirect } from "react-router-dom";
import { imgBaseUrl } from "../../../constants/constants";

class VerifyUserRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showLoginPopup: false
    };

    this.showLoginPopup = this.showLoginPopup.bind(this);
    this.closePopup = this.closePopup.bind(this);
  }

  showLoginPopup() {
    this.setState({
      showLoginPopup: true
    });
  }

  componentDidMount() {
    //TODO: Get route params and fetch url based on it
    //TODO: Get token and userid from route
    // const { userId, token } = this.props.match.params;
    // const params = { userId, token, portalId: 1 };
    const { userid, token } = querystring.parse(this.props.location.search);
    const params = { userId: userid, token, portalId: 1 };

    this.props.verifyUserRegistration({ params });
  }

  componentWillReceiveProps(nextProps) { }

  showLoginPopup() {
    this.setState({
      showLoginPopup: true
    });
  }

  closePopup() {
    this.setState({
      showLoginPopup: false
    });
  }

  render() {
    return gblFunc.isUserAuthenticated() &&
      this.props.match.path == "/verifyuser" ? (
        <Redirect
          to={{
            pathname: "/"
          }}
        />
      ) : (
        <section>
          <Loader isLoader={this.props.showLoader} />
          <section className="errorbggreay hide" />
          {this.state.showLoginPopup ? (
            <UserLoginRegistrationPopup closePopup={this.closePopup} />
          ) : null}
          <VerificationContent
            showLoader={this.props.showLoader}
            showLoginPopup={this.showLoginPopup}
          />
        </section>
      );
  }
}

const VerificationContent = props =>
  props.showLoader ? (
    <section className="thankspage">
      <img src={`${imgBaseUrl}icon-thanks.jpg`} alt="icon-thanks" />
      <h2>Congratulations!</h2>
      <p>
        Verifying your email address<br />
      </p>
    </section>
  ) : (
      <section className="thankspage">
        <img src={`${imgBaseUrl}icon-thanks.jpg`} alt="icon-thanks" />
        <h2>Congratulations!</h2>
        <p>
          Your email has been verified. <br />
          Please click on the below button to login.<br />
          <span className="btn" onClick={props.showLoginPopup}>
            Login
        </span>
        </p>
      </section>
    );

export default VerifyUserRegistration;
