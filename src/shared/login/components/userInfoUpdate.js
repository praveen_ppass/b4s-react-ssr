import React, { Component } from "react";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import {
  GET_ALL_RULES_SUCCESS,
  GET_ALL_RULES_FAILURE
} from "../../application/actions/applicationFormAction";
import {
  GET_USERINFO_BY_TOKEN_SUCCESS,
  GET_USERINFO_BY_TOKEN_FAILURE,
  POST_USERINFO_BY_TOKEN_SUCCESS,
  POST_USERINFO_BY_TOKEN_FAILURE,
  POST_USERINFO_BY_USERID_SUCCESS,
  POST_USERINFO_BY_USERID_FAILURE
} from "../actions";
import AlertMessagePopup from "../../../shared/common/components/alertMsg";
import Loader from "../../common/components/loader";
import gblFunc from "../../../globals/globalFunctions";

class UserInfoUpdate extends Component {
  constructor() {
    super();
    this.state = {
      showAlertPopup: false,
      status: "",
      messages: "",
      allrules: "",
      userInfo: "",
      userToken: null,
      isRedirect: false,
      userRulesData: "",
      redirectUrlState: "",
      isRedirectByUserId: false,
      userId: "",
      altName: "Buddy",
      initialValues: {
        gender: "",
        state: "",
        class: "",
        countryCode: ""
      }
    };
    this.closeAlertPopup = this.closeAlertPopup.bind(this);
    this.userIfoValidationSchema = this.userIfoValidationSchema.bind(this);
  }

  componentDidMount() {
    this.props.fetchAllRules();
    const { token } = this.props.match.params;
    const userRulesData = getNestedObjKey(this.props, ["location", "state", "userRulesData"])
    const contryCode = { ...this.state.initialValues, countryCode: getNestedObjKey(this.props, ["location", "state", "countryCode"]) }
    this.setState({
      userRulesData: userRulesData,
      redirectUrlState: getNestedObjKey(this.props, ["location", "state", "location"])
      ? this.props.location.state
      : this.props.location,
      initialValues: contryCode
    })
    if (token) {
      this.setState({ userToken: token });
      this.props.fetchUserInfo({ userToken: token });
    }
    if (typeof window !== "undefined" && !token) {
    this.updateUserTimer = setTimeout(() => {
    let userInfo = JSON.parse(window.localStorage.getItem('userInfo'))
    let userData = {
      ...this.state.initialValues,
      gender: getNestedObjKey(userInfo, ["gender"]),
      class: getNestedObjKey(userInfo, ["class"]),
      state: getNestedObjKey(userInfo, ["state"]),
     // countryCode: getNestedObjKey(userInfo,["countryCode"]),
    };
    this.setState({
      initialValues: userData,
      userId: getNestedObjKey(userInfo, ["userId"]),
    });
    if (getNestedObjKey(userInfo, ["gender"]) && getNestedObjKey(userInfo, ["class"]) && getNestedObjKey(userInfo, ["state"])) {
      this.props.history.push(getNestedObjKey(this.props, ["location", "state", "location"])
      ? getNestedObjKey(this.props, ["location", "state", "location"])
      : "/")
      localStorage.setItem("isAuth", 1)
    }
  }, 400)
  }
  }

  componentWillUnmount() {
    clearTimeout(this.updateUserTimer)
  }

  userIfoValidationSchema(userInfo) {
     return (
     Yup.object().shape({
     gender: Yup.string().required("Required."),
     class: Yup.string().required("Required."),
     state: Yup.string().test("match", "Required", function (state) {
      if (userInfo.countryCode == "+977" || userInfo.countryCode == "+95") {
        return true;
     } else if (state) {
      return true;
     }
    }
    )
  })
)
  }

  componentWillReceiveProps(nextProps) {
    const { type, rulesData, userInfo, infoType } = nextProps;
    switch (type) {
      case GET_ALL_RULES_SUCCESS:
        this.setState({
          allrules: rulesData
        });
        break;
      case GET_ALL_RULES_FAILURE:
        this.setState({
          showAlertPopup: true,
          status: false,
          messages: "Server error"
        });
        break;
    }
    switch (infoType) {
      case GET_USERINFO_BY_TOKEN_SUCCESS:
        const genderData = userInfo.userRules.find(
          x => x.ruleTypeValue == "gender"
        );
        const classData = userInfo.userRules.find(
          x => x.ruleTypeValue == "class"
        );
        const stateData = userInfo.userRules.find(
          x => x.ruleTypeValue == "state"
        );
        let data = {
          ...this.state.initialValues,
          gender: getNestedObjKey(genderData, ["ruleId"]),
          class: getNestedObjKey(classData, ["ruleId"]),
          state: getNestedObjKey(stateData, ["ruleId"])
        };
        this.setState({
          userInfo: userInfo,
          initialValues: data
        });
        break;
      case GET_USERINFO_BY_TOKEN_FAILURE:
         let errMsg = getNestedObjKey(userInfo, ["data", "message"])
         let errCode = getNestedObjKey(userInfo, ["data", "errorCode"])
          if (errCode == 801) {
            this.setState({
              showAlertPopup: true,
              status: false,
              messages: errMsg
            });
          } else {       
        this.setState({
          showAlertPopup: true,
          status: false,
          messages: "Server error"
        });
      }
        break;
      case POST_USERINFO_BY_TOKEN_SUCCESS:
        this.setState({
          showAlertPopup: true,
          status: true,
          isRedirect: true,
          messages: "Your details has been saved successfully"
        });
        break;
      case POST_USERINFO_BY_TOKEN_FAILURE:
          let errPostMsg = getNestedObjKey(userInfo, ["data", "message"])
          let errPostCode = getNestedObjKey(userInfo, ["data", "errorCode"])
           if (errPostCode == 801) {
             this.setState({
               showAlertPopup: true,
               status: false,
               messages: errPostMsg
             });
           } else {       
         this.setState({
           showAlertPopup: true,
           status: false,
           messages: "Server error"
         });
        }
        break;
      case POST_USERINFO_BY_USERID_SUCCESS:
          this.setState({
            showAlertPopup: true,
            status: true,
            isRedirectByUserId: true,
            messages: "Your details has been saved successfully"
          });
        break;
      case POST_USERINFO_BY_USERID_FAILURE:
           errPostMsg = getNestedObjKey(userInfo, ["data", "message"])
           errPostCode = getNestedObjKey(userInfo, ["data", "errorCode"])
           if (errPostCode == 801) {
             this.setState({
               showAlertPopup: true,
               status: false,
               messages: errPostMsg
             });
           } else {       
         this.setState({
           showAlertPopup: true,
           status: false,
           messages: "Server error"
         });
        }
        break
    }
  }
  closeAlertPopup() {
    const { userRulesData, redirectUrlState } = this.state
    this.setState(
      {
        showAlertPopup: false,
        status: "",
        messages: ""
      },
      () => {
        // if (this.state.isRedirect) this.props.history.push("/");
    if (getNestedObjKey(this.state,["redirectUrlState","location","pathname"])) {
          localStorage.setItem("isAuth", 1);
          if (userRulesData && userRulesData.vleUser) {
            if (
              userRulesData.cscId &&
              userRulesData.cscId != null
            ) {
              return this.props.history.push("/subscribers");
            }else{
              return this.props.history.push("/vle/add-student");
            }
          } else {
             this.props.history.push(getNestedObjKey(this.state,["redirectUrlState","location","pathname"]));
          }
      }else if(localStorage.getItem('currentPath')){
     localStorage.setItem("isAuth", 1);
     this.props.history.push(localStorage.getItem('currentPath'));
     localStorage.removeItem("currentPath"); 
    } else { 
          if(this.props.history.location && this.props.history.location.state && this.props.history.location.state.location && this.props.history.location.state.location.includes("CES8")){
          localStorage.setItem("isAuth", 1);

          this.props.history.push('/application/CES8/instruction')
          
          }else{
          localStorage.setItem("isAuth", 1);

            this.props.history.push("/")
  
          }
        
      }
      }
    );
  }

  render() {
    if (typeof window !== "undefined") {
      if (localStorage.getItem("isAuth") == 1) {
        if(this.props.history.location && this.props.history.location.state && this.props.history.location.state.location && this.props.history.location.state.location.includes("CES8")){
        this.props.history.push('/application/CES8/instruction')
        
        }else{
          this.props.history.push("/")

        }
      } 
      else if (!getNestedObjKey(this.props, ["match", "params", "token"]) && localStorage.getItem("userId") == null) {
       this.props.history.push("/")
      }
     history.pushState(null, null, location.href);
      window.onpopstate = function (event) {
        history.go(1);
      };
    }
    const { allrules, userInfo, altName } = this.state;
    return (
      <section className="container-fluid bguser">
        <Loader isLoader={this.props.showLoader} />
        {this.state.showAlertPopup ? (
          <AlertMessagePopup
            msg={this.state.messages}
            isShow={this.state.showAlertPopup}
            status={this.state.status}
            close={this.closeAlertPopup}
          />
        ) : null}
        <section className="container">
          <section className="row">
            <article className="col-md-12">
              <div className="userinfo">
                <div className="item">
                  <Formik
                    initialValues={this.state.initialValues}
                    onSubmit={values => {
                      let rules = []
                          rules.push(
                            { ruleId: parseInt(values.gender), ruleTypeId: 5, ruleTypeValue: "gender" },
                            { ruleId: parseInt(values.state), ruleTypeId: 21, ruleTypeValue: "state" },
                            { ruleId: parseInt(values.class), ruleTypeId: 1, ruleTypeValue: "class" }
                         )
                     let country_code = this.state.initialValues.countryCode
                     if (country_code == "+977" || country_code == "+95") {
                      rules = rules.filter(x => x.ruleTypeId != 21)
                     }

                      let finalData = {
                        userToken: this.state.userToken,
                        data: rules,
                        userId: this.state.userId
                      };
                      if (this.state.userId || localStorage.getItem("userId")) {
                      this.props.sendUserInfoById(finalData)
                      localStorage.removeItem("userInfo")
                      } else {
                        this.props.sendUserInfo(finalData);
                     }
                    }}
                    enableReinitialize
                    validationSchema={() => this.userIfoValidationSchema(this.state.initialValues)}
                  >
                    {({ errors, touched, values, setFieldValue }) => {
                      return (
                        <Form name="user-update">
                          <h4>
                            <p> Dear {userInfo.firstName || gblFunc.getStoreUserDetails().firstName || altName},</p>
                            Let us find better matching scholarship for you. Help us with your profile details{" "}
                          </h4>
                          <section className="formelemt">
                            <section className="ctrl-wrapper">
                              <article className="form-group">
                                <lable>Gender</lable>
                                <Field
                                  className="form-control"
                                  name="gender"
                                  component="select"
                                >
                                  <option value="">--Select gender--</option>
                                  {allrules.gender && allrules.gender.length
                                    ? allrules.gender.map(g => {
                                      if (g.id != 229) {
                                        return (
                                        <option key={g.id} value={g.id}>
                                          {g.rulevalue}
                                        </option>
                                        )
                                      }
                                    })
                                    : ""}
                                </Field>
                                {errors.gender && touched.gender && (
                                  <span className="error">{errors.gender}</span>
                                )}
                              </article>
                            </section>

                            {values.countryCode != "+977" && values.countryCode != "+95" ? <section className="ctrl-wrapper">
                              <article className="form-group">
                                <lable>State</lable>
                                <Field
                                  className="form-control"
                                  name="state"
                                  component="select"
                                >
                                  <option value="">--Select state--</option>
                                  {allrules.state && allrules.state.length
                                    ? allrules.state.map(s => (
                                        <option key={s.id} value={s.id}>
                                          {s.rulevalue}
                                        </option>
                                      ))
                                    : ""}
                                </Field>
                                {errors.state && touched.state && (
                                  <span className="error">{errors.state}</span>
                                )}
                              </article>
                            </section> : ""}

                            <section className="ctrl-wrapper">
                              <article className="form-group">
                                <lable>Present class</lable>
                                <Field
                                  className="form-control"
                                  name="class"
                                  component="select"
                                >
                                  <option value="">--Select class--</option>
                                  {allrules.class && allrules.class.length
                                    ? allrules.class.map(c => (
                                        <option key={c.id} value={c.id}>
                                          {c.rulevalue}
                                        </option>
                                      ))
                                    : ""}
                                </Field>
                                {errors.class && touched.class && (
                                  <span className="error">{errors.class}</span>
                                )}
                              </article>
                            </section>

                            <article className="ctrl-wrapper width">
                              <article className="form-group">
                                <button type="submit" className="greenBtn btn">
                                  Submit
                                </button>
                              </article>
                            </article>
                          </section>
                        </Form>
                      );
                    }}
                  </Formik>
                </div>
              </div>
            </article>
          </section>
        </section>
      </section>
    );
  }
}

export default UserInfoUpdate;
