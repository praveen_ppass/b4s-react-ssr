import React, { Component } from "react";
import OtpInput from "react-otp-input";
import { ruleRunner } from "../../../validation/ruleRunner";
import {
  required,
  minLength,
  isEmail,
  isNumeric,
  shouldMatch,
  isMobileNumber
} from "../../../validation/rules";
import gblFunc from "../../../globals/globalFunctions";
class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.onFieldChange = this.onFieldChange.bind(this);
    this.onRegisterClick = this.onRegisterClick.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.submitOtp = this.submitOtp.bind(this);
    this.submitMobile = this.submitMobile.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.state = {
      firstName: "",
      lastName: "",
      countryCode: "+91",
      mobile: "",
      email: "",
      password: "",
      otpEntered: "",
      isCallFire: false,
      disableOTP: false,
      isActiveVarifyBtn: false,
      conf_password: "",
      isFormValid: false,
      validations: {
        // firstName: null,
        //lastName: null,
        mobile: null,
        // email: null,
        // password: null,
        // conf_password: null
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    const { serverError } = nextProps;
    if (serverError) {
      let validations = { ...this.state.validations };
      validations.email = serverError;
      this.setState({
        validations
      });
    }
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    const shouldBeTrimmed = id =>
      id === "firstName" || id === "lastName" || id === "email";
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        shouldBeTrimmed(key) ? this.state[key].trim() : this.state[key],
        key,
        name,
        ...validationFunctions
      );

      validations[key] = validationResult[key];

      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }

    this.setState({
      validations,
      isFormValid
    });

    return isFormValid;
  }

  onRegisterClick(event) {
    event.preventDefault();
    /* call for register user */
    // if (this.checkFormValidations()) {
    //   let mappedUserApiObject = this.mapLocalStateToApiCall(this.state);

    //   this.props.registerUser(mappedUserApiObject);
    // }
  }

  submitMobile(e) {
    e.preventDefault()
    if (this.checkFormValidations())
      this.props.submitLoginOtp(this.state.mobile)
  }

  submitOtp(e) {
    e.preventDefault()
    this.props.sendOtpForLogin(this.state.otpEntered)
  }

  handleFieldChange(otp) {
    if (otp.length == 6) {
      this.setState({ otpEntered: otp, isActiveVarifyBtn: true });
    } else {
      this.setState({ otpEntered: otp, isActiveVarifyBtn: false });
    }
  }

  mapLocalStateToApiCall({
    email,
    password,
    firstName,
    lastName,
    mobile,
    countryCode
  }) {
    /*  Sample API call
"email": "abhinav.misra@buddy4study.com",
"userSource": /Android/i.test(navigator.userAgent)? "EMAIL-MOBILE-WEBSITE": "EMAIL-WEBSITE",
"password": "repassword",
"firstName": "Abhinav",
"lastName": "Mishra",
"mobile": "9953189925"  */

    return {
      email,
      userSource: /Android/i.test(navigator.userAgent)
        ? "EMAIL-MOBILE-WEBSITE"
        : "EMAIL-WEBSITE",
      password,
      firstName,
      lastName,
      mobile,
      countryCode
    };
  }

  /*

ORDER OF VALIDATION FUNCTIONS MATTERS.

*/

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "mobile":
        validationObject.name = "*Mobile number";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;
    }
  }

  shouldFieldUpdate(id, value) {
    switch (id) {
      case "mobile":
        return value.length <= 10;
      default:
        return true;
    }
  }

  onFieldChange(event) {
    const shouldBeTrimmed = id =>
      id === "firstName" || id === "lastName" || id === "email";

    let { value, id } = event.target;
    let shouldUpdate = true;

    let { name, validationFunctions } = this.getValidationRulesObject(id);
    let validationResult = ruleRunner(
      shouldBeTrimmed(id) ? value.trim() : value,
      id,
      name,
      ...validationFunctions
    );

    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    let newState = { validations };

    if (this.shouldFieldUpdate(id, value)) {
      newState[id] = value;
    }
    this.setState(newState);
  }
  render() {
    const { isShowOtp } = this.props
    const { isCallFire, disableOTP, otpTimedOut, mobile } = this.props
    return (
      <div id="tab-1">
        <article className="formelemt">
          <section className="row">
            <section className="ctrl-wrapper">
              <article className="col-md-12">
                {/* mobile screen */}
                {!isShowOtp ? <form className="formelemt" onSubmit={(e) => this.submitMobile(e)} name="regForm" autoCapitalize="off" >
                  <article className="form-group">
                    <span className="exten">+91</span>
                    <input
                      type="tel"
                      className="form-control padding40"
                      name="mobile"
                      id="mobile"
                      autoComplete="off "
                      placeholder="Enter mobile number"
                      value={this.state.mobile}
                      onChange={this.onFieldChange}
                    />
                    {this.state.validations.mobile ? (
                      <dd className="animated bounce error1">
                        {this.state.validations.mobile}
                      </dd>
                    ) : null}
                  </article>
                  <article className="form-group">
                    <input
                      type="password"
                      name="password"
                      className="form-control"
                      placeholder="Password"
                      id="password"
                      required
                      value={this.state.password}
                      onChange={this.handleChange}
                    />
                    {this.state.validations.password ? (
                      <dd className="animated bounce error1">
                        {this.state.validations.password}
                      </dd>
                    ) : null}
                  </article>

                  <section className="row equial-padding">
                    {/* <button
                      type="submit"
                      onClick={() => gblFunc.gaTrack.trackEvent(["Login Success", "LoginwithOTP"])}
                      className="btn-block greenBtn" >
                      Get OTP
                  </button> */}
                    <button
                      type="submit"
                      onClick={this.loginUser}
                      className="btn-block greenBtn"
                    >
                      Login
                  </button>
                  </section>
                </form> :
                  <form onSubmit={(e) => this.submitOtp(e)}>
                    <article className="loginotp">
                      <article className="container-fluid otpbg">
                        <article className="otpWrapper otpmobile">
                          <article className="formWrapper">
                            <article className="cellWrapper">
                              <article className="width100">
                                <p>{mobile}</p>
                                <article className="form-group otp">
                                  <OtpInput
                                    numInputs={6}
                                    separator={<span>-</span>}
                                    onChange={otp => this.handleFieldChange(otp)}
                                  />
                                </article>
                              </article>
                            </article>
                            {!disableOTP && !isCallFire ? (
                              <React.Fragment>
                                {otpTimedOut && (
                                  <button
                                    className="resendbut linkButton "
                                    style={{
                                      color: "blue",
                                      textDecorationLine: "underline"
                                    }}
                                    onClick={() => {
                                      this.props.requestOTPCount();
                                    }}
                                    type="button"
                                  >
                                    Request OTP on call
                          </button>
                                )}
                              </React.Fragment>
                            ) : (
                                <React.Fragment>
                                  {isCallFire ?
                                    <p>
                                      We are trying to call you with OTP
                     </p>
                                    : <p>
                                      Maximum attempts reached. Please try again after some
                                      time.
                      </p>
                                  }
                                </React.Fragment>
                              )}

                            <button
                              disabled={!this.state.isActiveVarifyBtn}
                              type="submit"
                              onClick={() => gblFunc.gaTrack.trackEvent(["Verify OTP", "VerifywithOTP"])}
                              className="btn-block greenBtn">
                              Verify OTP
                            </button>

                          </article>
                        </article>
                      </article>
                    </article>
                  </form>}
                {/* end otp screen */}
              </article>
            </section>
          </section>


        </article>
      </div >
    );
  }
}

export default RegisterForm;
