import React, { Component } from "react";
import { Redirect, withRouter } from "react-router-dom";
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import Loader from "../../common/components/loader";
import CompanyInfoSection from "./static/companyInfoSection";
import SocialLogin from "./socialLogin";
import RegisterForm from "./registerForm";
import LoginForm from "./loginForm";
import LoginRegisterForm from "./loginOrRegister";
import LoginRegisterIntermediate from "./loginRegisterIntermediate";
import AlertMessagePopup from "../../../shared/common/components/alertMsg";
import { messages } from "../../../constants/constants";
var jwtDecode = require('base-64');

import {
  LOGIN_USER_SUCCEEDED,
  REGISTER_USER_SUCCEEDED,
  FORGOT_USER_PASSWORD_SUCCESS,
  FORGOT_USER_PASSWORD_FAILURE,
  LOGIN_USER_FAILED,
  POST_EXISTING_MOBILE_SUCCESS,
  POST_EXISTING_MOBILE_FAILURE,
  POST_OTP_FOR_PASSWORD_CHANGE_SUCCESS,
  POST_OTP_FOR_PASSWORD_CHANGE_FAILURE
} from "../actions";
import {
  POST_VERIFY_OTP_SUCCEEDED,
  POST_VERIFY_OTP_FAILED,
  REQUEST_OTP_COUNTING_SUCCEEDED,
  REQUEST_OTP_COUNTING_FAILED
} from "../../scholarship-conclave/actions";
import {
  FETCH_RULES_SUCCEEDED,
  UPDATE_USER_RULES_SUCCESS,
  FETCH_USER_RULES_SUCCESS,
} from "../../../constants/commonActions";

import {
  loginRegisterRequiredRules,
  requiredRuleKeys
} from "../../../constants/constants";

import gblFunc from "../../../globals/globalFunctions";
import ForgotPasswordPopup from "./forgotPasswordPopup";
var otpTimerId;
const otpTimeOut = 15000; //ms
class UserLoginRegistrationPopup extends Component {
  constructor(props) {
    super(props);
    this.changeTab = this.changeTab.bind(this);
    this.renderCurrentForm = this.renderCurrentForm.bind(this);
    this.mapRequiredRules = this.mapRequiredRules.bind(this);
    this.intermediateLogout = this.intermediateLogout.bind(this);
    this.showForgotPasswordForm = this.showForgotPasswordForm.bind(this);
    this.showLoginRegisterForm = this.showLoginRegisterForm.bind(this);
    this.submitLoginOtp = this.submitLoginOtp.bind(this);
    this.sendOtpForLogin = this.sendOtpForLogin.bind(this);
    this.requestOTPCount = this.requestOTPCount.bind(this);
    this.closePopup = this.closePopup.bind(this);
    this.otpPasswordChange = this.otpPasswordChange.bind(this);

    this.state = {
      currentForm: "LOGINREGISTER",
      showEmailPopup: false,
      showForgotPasswordPopup: false,
      userRulesMappingRequired: false,
      isRedirecting: false,
      isShowAlert: false,
      alertStatus: "",
      alertMsg: "",
      redirectTo: "",
      isShowOtp: false,
      isCallFire: false,
      disableOTP: false,
      otpTimedOut: false,
      user_id: "",
      mobile: "",
      isTrue: true,
      userInfo: "",
      redirectUrlState:
        this.props.location &&
          this.props.location.state &&
          this.props.location.state.location
          ? this.props.location.state.location
          : this.props.location,
      isRedirectUserInfo: false
    };
  }

  showLoginRegisterForm() {
    this.setState({
      currentForm: "LOGINREGISTER"
    });
  }

  componentDidMount() {
    if (this.props.isRedirecting) {
      this.setState({
        isRedirecting: true,
        redirectTo: this.props.redirectTo
      });
    }
  }

  /*
  Intermediate form logs out user and resets state,
  to show the required Login / Register form.
  */

  intermediateLogout() {
    this.props.logUserOut();
    this.setState({
      currentForm: "LOGINREGISTER"
    });
  }

  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.tokenRefreshed) {
  //     alert("yes under token refreshed");
  //     window.location.reload();
  //   }
  // }

  /*
  Handler function which triggers form re render on tab change.
  */

  changeTab(event) {
    if (event.target.id === "LOGIN" || event.target.id === "REGISTER") {
      this.setState({ currentForm: event.target.id });
    }
  }

  showForgotPasswordForm() {
    this.setState({
      currentForm: "FORGOTPWD"
    });
  }

  /*
  Render login / registration form on tab change
  */

  renderCurrentForm() {
    const { currentForm } = this.state;

    switch (currentForm) {
      case "LOGINREGISTER":
        return (
          <LoginRegisterForm
            registerUser={this.props.registerUser}
            socialLogin={this.props.socialLogin}
            loginUser={this.props.loginUser}
            showLoader={this.props.showLoader}
            showForgotPasswordForm={this.showForgotPasswordForm}
            closePopup={this.props.closePopup}
            currentTab={this.props.currentTab || "LOGIN"}
            hideCloseButton={this.props.hideCloseButton}
            serverErrorRegister={this.props.serverErrorRegister || ""}
            serverErrorLogin={this.props.serverErrorLogin}
            isVLElogin={this.props.isVLElogin}
            location={this.state.redirectUrlState}
            allProps={this.props}
            submitLoginOtp={this.submitLoginOtp}
            isShowOtp={this.state.isShowOtp}
            sendOtpForLogin={this.sendOtpForLogin}
            requestOTPCount={this.requestOTPCount}
            isCallFire={this.state.isCallFire}
            disableOTP={this.state.disableOTP}
            otpTimedOut={this.state.otpTimedOut}
            mobile={this.state.mobile}
          />
        );

      case "FORGOTPWD":
        return (
          <ForgotPasswordPopup
            forgotUserPassword={this.props.forgotUserPassword}
            submitLoginOtp={this.submitLoginOtp}
            showLoginRegisterForm={this.showLoginRegisterForm}
            closePopup={this.props.closePopup}
            showLoader={this.props.showLoader}
            hideForgetCloseBtn={this.props.hideForgetCloseBtn}
            submitLoginOtp={this.submitLoginOtp}
            isShowOtp={this.state.isShowOtp}
            sendOtpForLogin={this.sendOtpForLogin}
            otpPasswordChange={this.otpPasswordChange}
            requestOTPCount={this.requestOTPCount}
            isCallFire={this.state.isCallFire}
            disableOTP={this.state.disableOTP}
            otpTimedOut={this.state.otpTimedOut}
            mobile={this.state.mobile}
            serverError={
              this.props.serverErrorForgotPassword
                ? this.props.serverErrorForgotPassword
                : ""
            }
          />
        );

      case "LOGINREGINTERMEDIATE":
        const { rulesList, userLoginData, userRulesData } = this.props;
        const mappedRulesList = this.mapRequiredRules(
          rulesList,
          userRulesData && userRulesData.userRules
        );
        return (
          <AlertMessagePopup
            isShow="true"
            status="true"
            msg={messages.login.success}
            close={this.props.closePopup}
          />
        );

      // mappedRulesList = this.mapRequiredRules(
      //   rulesList,
      //   userRulesData.userRules
      // );

      // if (mappedRulesList.isCheck) {
      //   return (
      //     <LoginRegisterIntermediate
      //       rulesList={mappedRulesList.mappedRules}
      //       userRules={this.props.userLoginData}
      //       showLoader={this.props.showLoader}
      //       logUserOut={this.intermediateLogout}
      //       addOrUpdateUserRules={this.props.addOrUpdateUserRules}
      //     />
      //   );
      // } else {
      //   return null;
      // }

      case "REGISTER_SUCCESS":
        return (
          <AlertMessagePopup
            isShow="true"
            status="true"
            msg={messages.register.success}
            close={this.props.closePopup}
          />
        );

      case "FORGOT_PASSWORD_SUCCESS":
        return (
          <AlertMessagePopup
            isShow="true"
            status="true"
            msg={messages.forgotPssword.success}
            close={this.props.closePopup}
          />
        );

      case "FORGOT_PASSWORD_FAILURE":
        return (
          <AlertMessagePopup
            isShow="true"
            status="false"
            msg={messages.forgotPssword.error}
            close={this.props.closePopup}
          />
        );

      case "SUCCESSFULL_LOGIN":
        if (this.state.isRedirecting) {
          return <Redirect to={this.state.redirectTo} />;
        }

        return (
          <AlertMessagePopup
            isShow="true"
            status="true"
            msg={messages.login.success}
            close={this.props.closePopup}
          />
        );
      case "OTP-Page":
        return (
          <AlertMessagePopup
            isShow="true"
            status="true"
            msg={"Please confirm your mobile number"}
            close={() => {
              this.props.closePopup();
              if (
                this.state.isRedirecting &&
                this.props.userLoginData &&
                this.props.userLoginData.data &&
                this.props.userLoginData.data.data
              ) {
                return this.props.history.push({
                  pathname: "/otp",
                  state: {
                    userId: this.props.userLoginData.data.data.userId,
                    mobile: this.props.userLoginData.data.data.mobile,
                    countryCode: this.props.userLoginData.data.data.countryCode,
                    temp_Token: this.props.userLoginData.data.data.token,
                    location:
                      this.props.location && this.props.location.pathname
                        ? this.props.location.pathname
                        : null
                  }
                });
              }
            }}
          />
        );

      default:
        return;
    }
  }

  /*
  Check if user rules are present in login user call data recieved
  */

  areUserRulesPresent(userRules) {
    return (
      userRules.filter(usrRule =>
        loginRegisterRequiredRules.includes(usrRule.ruleTypeId)
      ).length === 4
    );

    //  return loginRegisterRequiredRules.filter(userReqRules => userReqRules.ruleTypeId === userRules. );
    // return loginRegisterRequiredRules.every(
    //   ruleKey => ruleKey in userRules && userRules[ruleKey][0].ID !== ""
    // );
  }

  /*
  Map required rules to pass on as props to intermediate form.
  */
  mapRequiredRules(passedRulesList, userRules) {
    /*
    Rule mapper maps rules to an array like
    [{KEY:"gender",ruleList:[{ID:1,value:"Male"},{ID:2,value:"Female"}]},
    {KEY:"religion",ruleList:[{ID:22,value:"Hindu"},{ID:23,value:"Muslim"}]}
    ]
    */
    //Map selected values with user rules to be used in it's component
    let SELECTED_VALUES = [];
    let CHECKINTERMEDIATE = [];
    let selected_obj = {};
    //FIXME: Can be optimised to use object instead of iterating over array.
    //FIXME: Fix this to use loginRegisterRequiredRules instead of userRules
    if (userRules && userRules.length > 0) {
      SELECTED_VALUES = userRules.map(
        usrRule =>
          loginRegisterRequiredRules.includes(usrRule.ruleTypeId)
            ? { ID: usrRule.ruleId, RULETYPEID: usrRule.ruleTypeId }
            : ""
      );
      for (let key in SELECTED_VALUES) {
        selected_obj[SELECTED_VALUES[key]["RULETYPEID"]] = SELECTED_VALUES[key];
      }
    }
    let check_rule_intmd = [];

    let mappedRulesList =
      loginRegisterRequiredRules &&
      loginRegisterRequiredRules.length &&
      loginRegisterRequiredRules.map((ruleKey, index) => {
        let KEY = requiredRuleKeys[ruleKey];
        let SELECTED_VALUE =
          selected_obj[ruleKey] && selected_obj[ruleKey].ID
            ? selected_obj[ruleKey].ID
            : SELECTED_VALUES || "";

        let rulesList;

        if (selected_obj[ruleKey] && selected_obj[ruleKey].ID) {
          CHECKINTERMEDIATE.push(selected_obj[ruleKey].ID);
          check_rule_intmd = CHECKINTERMEDIATE;
        }
        // Test From Here...............................
        // Gender rulekey is 5 ,remove for all in that case.
        if (ruleKey === 5) {
          rulesList = passedRulesList[requiredRuleKeys[ruleKey]]
            .filter(rule => rule.id !== 229)
            .map(rule => ({
              ID: rule.id,
              value: rule.rulevalue
            }));
        } else {
          rulesList =
            passedRulesList[requiredRuleKeys[ruleKey]] &&
            passedRulesList[requiredRuleKeys[ruleKey]].length &&
            passedRulesList[requiredRuleKeys[ruleKey]].map(rule => ({
              ID: rule.id,
              value: rule.rulevalue
            }));
        }
        return {
          KEY,
          SELECTED_VALUE,
          rulesList
        };
      });

    return {
      isCheck: check_rule_intmd.length < 4 ? true : false,
      mappedRules: mappedRulesList
    };
  }

  /*
 Handles all the cases of successfull login and registration
  */

  componentWillReceiveProps(nextProps) {
    const { type, userRulesData, userLoginData } = nextProps;
    //console.log('nextProps', nextProps);
    //console.log('type', nextProps.type);
    switch (type) {

      case "VERIFY_TOKEN":

        localStorage.setItem("currentPath", location.pathname);
        if (getNestedObjKey(userLoginData, ["data", "errorCode"]) == 1001) {
          this.props.closePopup()
          this.props.history.push({
            pathname: "/otp",
            state: {
              userId: getNestedObjKey(userLoginData, ["data", "data", "userId"]),
              temp_Token: getNestedObjKey(nextProps, ["userLoginData", "data", "data", "token"]),
              countryCode: getNestedObjKey(nextProps, ["userLoginData", "data", "data", "countryCode"])
            }
          })
        }
        if (
          nextProps.userLoginData &&
          nextProps.userLoginData.data &&
          nextProps.userLoginData.data.data &&
          nextProps.userLoginData.data.data.mobileVerified == "0"
        ) {
          return this.setState({
            currentForm: "OTP-Page",
            isRedirecting: true,
            redirectTo: "/otp"
          });
        }
        break;
      case LOGIN_USER_SUCCEEDED:
        let { userId } = nextProps.userLoginData;
        const userid =
          typeof window !== "undefined"
            ? window.localStorage.getItem("userId")
            : "";
        // Store auth token
        // Fix for auth token
        gblFunc.storeAuthToken(nextProps.userLoginData.access_token);
        this.timer = setTimeout(() => {
          this.props.fetchUserRules({
            userid: userId
          });
        }, 0);
        break;
      case REGISTER_USER_SUCCEEDED:
        this.setState({
          currentForm: "REGISTER_SUCCESS"
          //  currentForm: "LOGIN"
        });

        break;

      case FETCH_RULES_SUCCEEDED:
        this.setState({
          currentForm: "LOGINREGINTERMEDIATE"
        });
        break;

      case UPDATE_USER_RULES_SUCCESS:
        this.props.fetchUserRules({
          userid: gblFunc.getStoreUserDetails()["userId"]
        });
        this.props.closePopup();
        break;

      case FETCH_USER_RULES_SUCCESS:
        //SAVE USER RELEVANT DATA IN LOCALSTORAGE HERE.
        //USER IS AUTHENTICATED IN THIS CASE ONLY ( USER RULES ARE FETCHED AND TOKEN IS FETCHED)
        let userRules = getNestedObjKey(userRulesData, ["userRules"])
        let id = getNestedObjKey(userRulesData, ["id"])
        let countryCode = getNestedObjKey(userRulesData, ["countryCode"])
        gblFunc.storeUserDetails(userRulesData)
        gblFunc.storeRegAuthDetails(this.props.userLoginData);
        let isUserRuleActive = 0
        let userInfo = {}
        let requiredRules = 3
        if (countryCode == +977 || countryCode == +95) {
          requiredRules = 2
        }
        userInfo["countryCode"] = countryCode
        userInfo["userId"] = id
        if (userRules && userRules.length) {
          userRules.map(x => {
            if (x.ruleTypeId == 1 && x.ruleId) {
              isUserRuleActive++
              userInfo["class"] = x.ruleId
            }
            if (x.ruleTypeId == 5 && x.ruleId) {
              userInfo["gender"] = x.ruleId
              isUserRuleActive++
            }
            if (x.ruleTypeId == 21 && x.ruleId) {
              userInfo["state"] = x.ruleId
              isUserRuleActive++
            }
            return x
          })
        } else {
          this.setState({ isRedirectUserInfo: true }, () => {
            localStorage.setItem("isAuth", 0);
            this.props.history.push({
              pathname: "/user-info", userRulesData: nextProps.userRulesData, location: this.state.redirectUrlState,
              state: { countryCode: countryCode }
            })
            return
          })
        }
        if (isUserRuleActive == requiredRules) {
          localStorage.setItem("isAuth", 1);
          localStorage.setItem("userId", id)
          if (localStorage.getItem("currentPath") && localStorage.getItem("bsid")) {
            localStorage.removeItem("currentPath")
            localStorage.removeItem("bsid")
            if (nextProps.userRulesData && nextProps.userRulesData.vleUser) {
              this.setState({ userRulesData: nextProps.userRulesData })
              if (
                nextProps.userRulesData.cscId &&
                nextProps.userRulesData.cscId != null
              ) {
                return this.props.history.push("/subscribers");
              }
              return this.props.history.push("/vle/add-student");
            } else {
              return this.props.history.push(this.state.redirectUrlState);
            }
          }
          if (this.state.isTrue) {
            const {
              firstName,
              lastName,
              email,
              mobile,
              pic,
              profilePercentage,
              vleUser,
              membershipExpiry
            } = nextProps.userRulesData;

            // Store login details here
            gblFunc.storeUserDetails({
              firstName,
              lastName,
              email,
              mobile,
              pic,
              profilePercentage,
              vleUser,
              membershipExpiry
            });
            gblFunc.storeRegAuthDetails(this.props.userLoginData);
            if (window && window.location.href.includes("/page/")) {
              this.setState({
                currentForm: "SUCCESSFULL_LOGIN"
              });
            } else {
              if (!this.areUserRulesPresent(nextProps.userRulesData.userRules)) {
                this.props.fetchRules();
              } else {
                if (vleUser) {
                  this.setState({
                    currentForm: "SUCCESSFULL_LOGIN",
                    isRedirecting: true,
                    redirectTo: "/vle/add-student"
                  });
                } else {
                  this.setState({
                    currentForm: "SUCCESSFULL_LOGIN"
                  });
                }
              }
            }
            this.setState({ isTrue: false });
          }
        }
        else {
          this.setState({ isRedirectUserInfo: true, userInfo: userInfo, countryCode: countryCode })
          localStorage.setItem("isAuth", 0);
        }
        break;

      case FORGOT_USER_PASSWORD_SUCCESS:
        this.setState({
          currentForm: "FORGOT_PASSWORD_SUCCESS"
        });
        break;

      case FORGOT_USER_PASSWORD_FAILURE:
        this.setState({
          currentForm: "FORGOT_PASSWORD_FAILURE"
        });
        break;

      // case FORGOT_USER_PASSWORD_FAILURE:
      //   const errorKey = getNestedObjKey(nextProps.userLogData, [
      //     "response",
      //     "data",
      //     "errorCode"
      //   ]);
      //   console.log('errorKey', errorKey)
      //   if (errorKey == 1004) {
      //     this.setState({
      //       isShowAlert: true,
      //       alertStatus: false,
      //       alertMsg: "This email/mobile is not registered with us. Please click on register to sign up or use your email id/mobile to login."
      //     })
      //   }
      //   else if (errorKey == 1003) {
      //     this.setState({
      //       isShowAlert: true,
      //       alertStatus: false,
      //       alertMsg: "This mobile number is registered with multiple users. Please contact info@buddy4study.com to get your login credentials details."
      //     })
      //   }
      //   else {
      //     this.setState({
      //       isShowAlert: true,
      //       alertStatus: false,
      //       alertMsg: "Server error"
      //     })
      //   }
      //   break;
      case UPDATE_USER_RULES_SUCCESS:
        this.setState({
          currentForm: "SUCCESSFULL_LOGIN"
        });
        break;
      case POST_EXISTING_MOBILE_SUCCESS:
        const user_id = jwtDecode.decode(getNestedObjKey(nextProps, ["userLogData", "user"]))
        const mob = getNestedObjKey(nextProps, ["userLogData", "mobile"])
        this.setState({
          isShowOtp: true,
          user_id,
          mobile: mob
        })
        otpTimerId = setTimeout(() => {
          this.setState({ otpTimedOut: true });
        }, otpTimeOut);
        if (nextProps.type != this.props.type) {
          let isCall = true
          if (mob && isCall) {
            isCall = false
            this.otpTimerCall = setTimeout(() => {
              this.requestOTPCount();
            }, 60000);
          }
          this.otpMsgTimerStart = setTimeout(() => {
            this.setState({ isCallFire: true });
          }, 50000)
        }
        break;
      case POST_EXISTING_MOBILE_FAILURE:
        const errorToken = getNestedObjKey(nextProps.userLogData, [
          "response",
          "data",
          "errorCode"
        ]);
        //console.log('errorKey email failed', errorToken)
        if (errorToken == 1004) {
          this.setState({
            isShowAlert: true,
            alertStatus: false,
            alertMsg: "This email/mobile is not registered with us. Please click on register to sign up or use your email id/mobile to login."
          })
        }
        else if (errorToken == 1003) {
          this.setState({
            isShowAlert: true,
            alertStatus: false,
            alertMsg: "This mobile number is registered with multiple users. Please contact info@buddy4study.com to get your login credentials details."
          })
        }
        else {
          this.setState({
            isShowAlert: true,
            alertStatus: false,
            alertMsg: "Server error"
          })
        }
        break;
      case POST_OTP_FOR_PASSWORD_CHANGE_SUCCESS:
        const changePasswordToken = nextProps.otpPasswordChange ? nextProps.otpPasswordChange.token : null;
        //console.log(changePasswordToken, 'password token')
        this.setState({
          changePasswordToken: changePasswordToken
        }, () => {
          if (changePasswordToken) {
            this.props.closePopup();
            this.props.history.push({
              pathname: `/resetPassword`,
              search: `?query=${changePasswordToken}`
              // state: {
              //   token: changePasswordToken
              // }
            })
          }

        })
        break;
      case POST_OTP_FOR_PASSWORD_CHANGE_FAILURE:
        if (nextProps.otpPasswordChange != this.props.otpPasswordChange) {
          // let errorCode = getNestedObjKey(nextProps.otpPasswordChange.errorCode, ["errors", "response",
          //   "data", "errorCode"])
          let errorCode = nextProps.otpPasswordChange && nextProps.otpPasswordChange.errorCode;
          if (errorCode == 701) {
            this.setState({
              alertStatus: false,
              isShowAlert: true,
              alertMsg: "Invalid OTP"
            }, () => {
              clearTimeout(otpTimerId);
              clearTimeout(this.otpTimerCall);
              clearTimeout(this.otpMsgTimerStart);
              clearTimeout(this.otpMsgTimerEnd);
            })
            errorCode = ""
          } else {
            this.setState({
              alertStatus: false,
              isShowAlert: true,
              alertMsg:
                nextProps.errorMessage.errorCode == 404
                  ? messages.otp.verifyError
                  : messages.generic.error
            }, () => {
              clearTimeout(otpTimerId);
              clearTimeout(this.otpTimerCall);
              clearTimeout(this.otpMsgTimerStart);
              clearTimeout(this.otpMsgTimerEnd);
            });
          }
        }
        break;
    }


    switch (nextProps.varifyType) {
      case POST_VERIFY_OTP_SUCCEEDED:
        break
      case POST_VERIFY_OTP_FAILED:
        if (nextProps.varifyType != this.props.varifyType) {
          let errorCode = getNestedObjKey(nextProps.errorMessage, ["errors", "response",
            "data", "errorCode"])
          if (errorCode == 701) {
            this.setState({
              alertStatus: false,
              isShowAlert: true,
              alertMsg: "Invalid OTP"
            }, () => {
              clearTimeout(otpTimerId);
              clearTimeout(this.otpTimerCall);
              clearTimeout(this.otpMsgTimerStart);
              clearTimeout(this.otpMsgTimerEnd);
            })
            errorCode = ""
          } else {
            this.setState({
              alertStatus: false,
              isShowAlert: true,
              alertMsg:
                nextProps.errorMessage.errorCode == 404
                  ? messages.otp.verifyError
                  : messages.generic.error
            }, () => {
              clearTimeout(otpTimerId);
              clearTimeout(this.otpTimerCall);
              clearTimeout(this.otpMsgTimerStart);
              clearTimeout(this.otpMsgTimerEnd);
            });
          }
        }
        break
      case REQUEST_OTP_COUNTING_SUCCEEDED:
        this.setState({
          otpCounter: nextProps.otpCounter && nextProps.otpCounter.retryCount
        }, () => {
          clearTimeout(otpTimerId);
          clearTimeout(this.otpTimerCall);
          clearTimeout(this.otpMsgTimerStart);
          // clearTimeout(this.otpMsgTimerEnd);
        });
        break;
      case REQUEST_OTP_COUNTING_FAILED:
        const errorKey = getNestedObjKey(nextProps.otpCounter, [
          "response",
          "data",
          "errorCode"
        ]);
        clearTimeout(otpTimerId);
        clearTimeout(this.otpTimerCall);
        clearTimeout(this.otpMsgTimerStart);
        // clearTimeout(this.otpMsgTimerEnd);
        if (errorKey == 1105) {
          this.setState({
            alertStatus: false,
            isShowAlert: true,
            redirectingToLogin: true,
            alertMsg: "Maximum attempt reached"
          });
        } else {
          this.setState({
            alertStatus: false,
            isShowAlert: true,
            alertMsg: "Server error"
          });
          break;
        }
    }
  }
  componentWillUnmount() {
    clearInterval(this.timer);
    clearTimeout(otpTimerId);
    clearTimeout(this.otpTimerCall);
    clearTimeout(this.otpMsgTimerStart);
    clearTimeout(this.otpMsgTimerEnd);
  }

  submitLoginOtp(data) {
    this.props.sendMobileForOtp(data)
  }

  sendOtpForLogin(otp) {
    this.props.verifyOTP({
      mobile: this.state.mobile,
      otp: otp,
      userId: this.state.user_id
    })
  }

  otpPasswordChange(otp) {
    this.props.sendOtpForPasswordChange({
      mobile: this.state.mobile,
      otp: otp,
      userId: this.state.user_id
    })
  }

  requestOTPCount() {
    if (this.state.mobile) {
      clearTimeout(this.otpTimerCall);
      this.props.fetchOTPCounting({
        mobile: this.state.mobile,
        userId: this.state.user_id
      });
      if (this.state.otpCounter > 2) {
        this.setState({ disableOTP: true });
        return;
      }
    }
    this.otpMsgTimerEnd = setTimeout(() => {
      this.setState({ isCallFire: false });
    }, 15000)
  }

  closePopup() {
    this.setState({
      isShowAlert: false,
      alertStatus: "",
      alertMsg: ""
    });
  }
  /*
  Helper function to get title of modal on the basis of current state
  */

  render() {
    if (this.state.isRedirectUserInfo) {
      if (typeof window !== "undefined") {
        window.localStorage.setItem("userInfo", JSON.stringify(this.state.userInfo))
      }
      return (
        <Redirect to={{
          pathname: '/user-info',
          state: {
            userRulesData: this.state.userRulesData,
            location: this.state.redirectUrlState,
            countryCode: this.state.countryCode
          }
        }} />
      )
    }
    return (
      <React.Fragment>
        <AlertMessagePopup
          isShow={this.state.isShowAlert}
          status={this.state.alertStatus}
          msg={this.state.alertMsg}
          close={this.closePopup}
        />
        <Loader isLoader={this.props.showLoader || this.props.showVerLoader} />
        {this.renderCurrentForm(this.state.currentForm)}
      </React.Fragment>
    )
  }
}

export default withRouter(UserLoginRegistrationPopup);
