import React, { Component } from "react";

import CompanyInfoSection from "./static/companyInfoSection";

import { ruleRunner } from "../../../validation/ruleRunner";
import { required } from "../../../validation/rules";
import { requiredRulesMap } from "../../../constants/constants";
import Loader from "../../common/components/loader";
class LoginRegisterIntermediateForm extends Component {
  constructor(props) {
    super(props);
    this.onSelectChange = this.onSelectChange.bind(this);
    this.onRulesSubmit = this.onRulesSubmit.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);

    this.state = {
      formRules: {
        class: "",
        gender: "",
        religion: "",
        quota: ""
      },
      validations: {
        class: "",
        gender: "",
        religion: "",
        quota: ""
      },
      isFormValid: false
    };
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "class":
        validationObject.name = "*Education";
        validationObject.validationFunctions = [required];
        return validationObject;

      case "gender":
        validationObject.name = "*Gender ";
        validationObject.validationFunctions = [required];
        return validationObject;

      case "religion":
        validationObject.name = "*Religion ";
        validationObject.validationFunctions = [required];
        return validationObject;

      case "quota":
        validationObject.name = "*Quota ";
        validationObject.validationFunctions = [required];
        return validationObject;
    }
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state.formRules[key],
        key,
        name,
        ...validationFunctions
      );

      validations[key] = validationResult[key];

      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }

    this.setState({
      validations,
      isFormValid
    });

    return isFormValid;
  }

  componentWillMount() {
    //TODO: Set state of already set fields in case of Login
    //TODO: Set all fields in case of register
    let formRules = {};
    this.props.rulesList.forEach(
      ruleDetail => (formRules[ruleDetail.KEY] = ruleDetail.SELECTED_VALUE)
    );
    this.setState({
      formRules
    });
  }

  onSelectChange(event) {
    const { id, value } = event.target;
    let formRules = { ...this.state.formRules };

    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );

    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    formRules[id] = value;
    this.setState({ formRules, validations });
  }

  onRulesSubmit(event) {
    event.preventDefault();
    //TODO: Makes update rules call
    if (this.checkFormValidations()) {
      let formRules = { ...this.state.formRules };
      const userid =
        typeof window !== "undefined"
          ? window.localStorage.getItem("userId")
          : "";
      //Map rules
      const params = Object.keys(formRules).map(key => ({
        ruleTypeId: requiredRulesMap.get(key),
        ruleId: formRules[key]
      }));
      this.props.addOrUpdateUserRules({ userid, params });

      // this.props.addUserRules({ userid, params });
      // this.props.userIsLoggedIn();
      // this.props.closePopup();
    }
  }

  render() {
    return (
      <article className="popup">
        <Loader isLoader={this.props.showLoader} />
        <article className="popupsms">
          <article className="popup_inner loginpopup">
            <article className="LoginRegPopup">
              <section className="modal fade modelAuthPopup1">
                <section className="modal-dialog">
                  <section
                    className={`modal-content modelBg ${
                      this.state.showForgotPasswordPopup
                        ? "login emailverification"
                        : "login"
                    } `}
                  >
                    <article>
                      <article className="modal-header">
                        <h3 className="modal-title">
                          {"Please provide more details"}
                        </h3>
                      </article>
                      <article className="modal-body">
                        <article className="row">
                          {/* <CompanyInfoSection /> */}

                          <article className="col-md-7 mg-btm register">
                            <form className="formelemt" autoCapitalize="off">
                              <section className="row">
                                {this.props.rulesList.map(ruleDetail => (
                                  <SelectDropDown
                                    KEY={ruleDetail.KEY}
                                    selectedValue={
                                      this.state.formRules[ruleDetail.KEY]
                                    }
                                    ruleOptions={ruleDetail.rulesList}
                                    onSelectChange={this.onSelectChange}
                                    validationError={
                                      this.state.validations[ruleDetail.KEY]
                                    }
                                  />
                                ))}
                              </section>
                              <section className="row emailbtn">
                                <span
                                  onClick={this.props.logUserOut}
                                  className="pull-left"
                                >
                                  {"Logout"}
                                </span>
                                <button
                                  type="submit"
                                  className="greenBtn"
                                  disabled={this.state.disabled}
                                  onClick={this.onRulesSubmit}
                                >
                                  Submit
                                </button>
                              </section>
                            </form>
                          </article>
                        </article>
                      </article>
                    </article>
                  </section>
                </section>
              </section>
            </article>
          </article>
        </article>
      </article>
    );
  }
}

const SelectDropDown = props => {
  const {
    KEY,
    ruleOptions,
    selectedValue,
    onSelectChange,
    validationError
  } = props;
  return (
    <section className="ctrl-wrapper">
      <article className="form-group">
        <select
          name={KEY}
          id={KEY}
          value={selectedValue}
          className="form-control"
          onChange={onSelectChange}
        >
          <option value="">{`--Select ${
            KEY == "class" ? `current ${KEY}` : `${KEY}`
          }--   `}</option>
          {ruleOptions &&
            ruleOptions.length &&
            ruleOptions.map(rule => (
              <option key={rule.ID} value={rule.ID}>
                {rule.value}
              </option>
            ))}
        </select>
        {validationError ? (
          <span className="error animated bounce error1">
            {validationError}
          </span>
        ) : null}
      </article>
    </section>
  );
};

export default LoginRegisterIntermediateForm;
