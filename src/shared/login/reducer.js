import {
  REGISTER_USER_REQUEST,
  REGISTER_USER_SUCCEEDED,
  REGISTER_USER_FAILED,
  LOGIN_USER_REQUESTED,
  LOGIN_USER_SUCCEEDED,
  LOGIN_USER_FAILED,
  ADD_USER_RULES_REQUESTED,
  ADD_USER_RULES_SUCCEEDED,
  ADD_USER_RULES_FAILED,
  LOG_USER_OUT,
  UPDATE_USER_PASSWORD_FAILURE,
  UPDATE_USER_PASSWORD_REQUEST,
  UPDATE_USER_PASSWORD_SUCCESS,
  VERIFY_USER_REGISTRATION_REQUEST,
  VERIFY_USER_REGISTRATION_SUCCESS,
  VERIFY_USER_REGISTRATION_FAILURE,
  FORGOT_USER_PASSWORD_REQUEST,
  FORGOT_USER_PASSWORD_SUCCESS,
  FORGOT_USER_PASSWORD_FAILURE,
  RESET_USER_PASSWORD_REQUEST,
  RESET_USER_PASSWORD_SUCCESS,
  RESET_USER_PASSWORD_FAILURE,
  SOCIAL_LOGIN_USER_REQUESTED,
  SOCIAL_LOGIN_USER_SUCCEEDED,
  SOCIAL_LOGIN_USER_FAILED,
  LOG_USER_OUT_FAILED,
  LOG_USER_OUT_SUCCEEDED,
  RESET_USER_PASSWORD_TOKEN_CHECK_REQUEST,
  RESET_USER_PASSWORD_TOKEN_CHECK_SUCCESS,
  RESET_USER_PASSWORD_TOKEN_CHECK_FAILURE,
  GET_USERINFO_BY_TOKEN_REQUEST,
  GET_USERINFO_BY_TOKEN_SUCCESS,
  GET_USERINFO_BY_TOKEN_FAILURE,
  POST_USERINFO_BY_TOKEN_REQUEST,
  POST_USERINFO_BY_TOKEN_SUCCESS,
  POST_USERINFO_BY_TOKEN_FAILURE,
  POST_USERINFO_BY_USERID_REQUEST,
  POST_USERINFO_BY_USERID_SUCCESS,
  POST_USERINFO_BY_USERID_FAILURE,
  GET_EXISTING_MOBILE_REQUEST,
  GET_EXISTING_MOBILE_SUCCESS,
  GET_EXISTING_MOBILE_FAILURE,
  POST_EXISTING_MOBILE_REQUEST,
  POST_EXISTING_MOBILE_SUCCESS,
  POST_EXISTING_MOBILE_FAILURE,
  POST_OTP_FOR_LOGIN_REQUEST,
  POST_OTP_FOR_LOGIN_SUCCESS,
  POST_OTP_FOR_LOGIN_FAILURE,
  POST_OTP_FOR_PASSWORD_CHANGE_REQUEST,
  POST_OTP_FOR_PASSWORD_CHANGE_SUCCESS,
  POST_OTP_FOR_PASSWORD_CHANGE_FAILURE,
} from "./actions";
import {
  FETCH_RULES_SUCCEEDED,
  FETCH_USER_RULES_REQUEST,
  FETCH_USER_RULES_SUCCESS,
  FETCH_USER_RULES_FAILURE,
  UPDATE_USER_RULES_SUCCESS,
  UPDATE_USER_RULES_REQUEST,
  UPDATE_USER_RULES_FAILURE
} from "../../constants/commonActions";
import gblFunc from "../../globals/globalFunctions";
import {
  UPLOAD_USER_PIC_SUCCESS,
  FETCH_UPDATE_USER_SUCCEEDED
} from "../dashboard/actions";

const isAuthenticated = gblFunc.isUserAuthenticated() || false;

const initialState = {
  showLoader: false,
  isError: false,
  userLoginData: null,
  serverErrorRegister: "",
  serverErrorLogin: "",
  isAuthenticated,
  serverErrorForgotPassword: "",
  tokenRefreshed: false,
  passwordUpdateError: "",
  updatePasswordSuccessfull: false
};

const loginRegistrationReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_UPDATE_USER_SUCCEEDED:
      return Object.assign({}, state, {
        updateUserInfo: payload,
        showLoader: false,
        type
      });

    case REGISTER_USER_REQUEST:
      return { ...state, showLoader: true, type, serverErrorRegister: "" };

    case REGISTER_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userData: payload
      };

    case REGISTER_USER_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        serverErrorRegister: payload,
        type
      };

    case FETCH_RULES_SUCCEEDED:
      return { ...state, showLoader: false, type };

    //Login user cases

    case LOGIN_USER_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        userRulesPresent: false,
        serverErrorLogin: ""
      };

    case LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload,
        isAuthenticated: true,
        userRulesPresent: false
      };

    case LOGIN_USER_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        type,
        serverErrorLogin: payload
      };
    case "VERIFY_TOKEN":
      return {
        ...state,
        showLoader: false,
        isError: false,
        type: "VERIFY_TOKEN",
        userLoginData: payload,
        //       isAuthenticated: true,
        userRulesPresent: false
      };
    case LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload
      };
    case UPLOAD_USER_PIC_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        isError: false,
        uploadPicData: payload
      };

    // User rules cases

    case ADD_USER_RULES_REQUESTED:
      return { ...state, showLoader: true, type, isError: false };

    case ADD_USER_RULES_SUCCEEDED:
      return { ...state, showLoader: false, type, isError: false };

    case ADD_USER_RULES_FAILED:
      return { ...state, showLoader: false, type, isError: true };

    case FETCH_USER_RULES_REQUEST:
      return { ...state, showLoader: true, type, isError: false };

    case FETCH_USER_RULES_SUCCESS:
      return {
        ...state,
        showLoader: false,
        userRulesData: payload,
        type,
        isError: false
      };

    case FETCH_USER_RULES_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    case LOG_USER_OUT:
      gblFunc.removeUserDetails();
      gblFunc.removeCSCUserDetails();
      return {
        ...state,
        isAuthenticated: false,
        type,
        showLoader: true,
        isError: false
      };

    case LOG_USER_OUT_SUCCEEDED:
      return { ...state, type, showLoader: false };

    case LOG_USER_OUT_FAILED:
      return { ...state, type, showLoader: false, isError: true };

    case UPDATE_USER_PASSWORD_REQUEST:
      return {
        ...state,
        showLoader: true,
        type
      };

    case UPDATE_USER_PASSWORD_SUCCESS:
      return {
        ...state,
        updatedPasswordData: payload,
        showLoader: false,
        type
      };

    case UPDATE_USER_PASSWORD_FAILURE:
      return {
        ...state,
        type,
        passwordUpdateError: payload,
        isError: true,
        showLoader: false
      };

    //USER REGISTRATION VERIFICATION
    case VERIFY_USER_REGISTRATION_REQUEST:
      return { ...state, type, showLoader: true };

    case VERIFY_USER_REGISTRATION_SUCCESS:
      return { ...state, type, showLoader: false };

    case VERIFY_USER_REGISTRATION_FAILURE:
      return { ...state, type, showLoader: false, isError: true };

    //FORGOT USER PASSWORD
    case FORGOT_USER_PASSWORD_REQUEST:
      return {
        ...state,
        type,
        showLoader: true,
        serverErrorForgotPassword: ""
      };

    case FORGOT_USER_PASSWORD_SUCCESS:
      return { ...state, type, showLoader: false };

    case FORGOT_USER_PASSWORD_FAILURE:
      return {
        ...state,
        type,
        showLoader: false,
        isError: true,
        serverErrorForgotPassword: payload
      };

    //RESET USER PASSWORD: Forgot password case
    case RESET_USER_PASSWORD_REQUEST:
      return { ...state, type, showLoader: true };

    case RESET_USER_PASSWORD_SUCCESS:
      return { ...state, type, showLoader: false };

    case RESET_USER_PASSWORD_FAILURE:
      return { ...state, type, showLoader: false, isError: true };

    case SOCIAL_LOGIN_USER_REQUESTED:
      return { ...state, type, showLoader: true };

    case SOCIAL_LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type,
        userLoginData: payload,
        isAuthenticated: true
      };

    case SOCIAL_LOGIN_USER_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        type,
        serverErrorLogin: payload
      };

    //UPDATE USER RULES
    case UPDATE_USER_RULES_REQUEST:
      return { ...state, type, showLoader: true, isError: false };

    case UPDATE_USER_RULES_SUCCESS:
      return { ...state, type, showLoader: false };

    case UPDATE_USER_RULES_FAILURE:
      return { ...state, type, showLoader: false };

    case RESET_USER_PASSWORD_TOKEN_CHECK_REQUEST:
      return { ...state, type, showLoader: true, isError: false };

    case RESET_USER_PASSWORD_TOKEN_CHECK_SUCCESS:
      return { ...state, type, tokenCheck: payload, showLoader: false };

    case RESET_USER_PASSWORD_TOKEN_CHECK_FAILURE:
      return { ...state, type, showLoader: false };

    // Get user info by token
    case GET_USERINFO_BY_TOKEN_REQUEST:
      return { ...state, type, showLoader: true };

    case GET_USERINFO_BY_TOKEN_SUCCESS:
      return { ...state, type, userInfo: payload, showLoader: false };

    case GET_USERINFO_BY_TOKEN_FAILURE:
      return { ...state, type, userInfo: payload, showLoader: false };

    // Post user info by token
    case POST_USERINFO_BY_TOKEN_REQUEST:
      return { ...state, type, showLoader: true };

    case POST_USERINFO_BY_TOKEN_SUCCESS:
      return { ...state, type, userInfo: payload, showLoader: false };

    case POST_USERINFO_BY_TOKEN_FAILURE:
      return { ...state, type, userInfo: payload, showLoader: false };

    // Post user info by UserID
    case POST_USERINFO_BY_USERID_REQUEST:
      return { ...state, type, showLoader: true };

    case POST_USERINFO_BY_USERID_SUCCESS:
      return { ...state, type, userInfo: payload, showLoader: false };

    case POST_USERINFO_BY_USERID_FAILURE:
      return { ...state, type, userInfo: payload, showLoader: false };

    // Get existing mobile number
    case GET_EXISTING_MOBILE_REQUEST:
      return { ...state, type, mobileStatus: "", showLoader: true };

    case GET_EXISTING_MOBILE_SUCCESS:
      return { ...state, type, mobileStatus: payload, showLoader: false };

    case GET_EXISTING_MOBILE_FAILURE:
      return { ...state, type, mobileStatus: payload, showLoader: false };
    // OTP for login
    case POST_EXISTING_MOBILE_REQUEST:
      return { ...state, type, userLogData: "", showLoader: true };
    case POST_EXISTING_MOBILE_SUCCESS:
      return { ...state, type, userLogData: payload, showLoader: false };
    case POST_EXISTING_MOBILE_FAILURE:
      return { ...state, type, userLogData: payload, showLoader: false };

    case POST_OTP_FOR_PASSWORD_CHANGE_REQUEST:
      return { ...state, type, otpPasswordChange: "", showLoader: true };
    case POST_OTP_FOR_PASSWORD_CHANGE_SUCCESS:
      return { ...state, type, otpPasswordChange: payload, showLoader: false };
    case POST_OTP_FOR_PASSWORD_CHANGE_FAILURE:
      return { ...state, type, otpPasswordChange: payload, showLoader: false };
    default:
      return state;
  }
};

export default loginRegistrationReducer;
