import { connect } from "react-redux";

import {
  registerUser as registerUserAction,
  loginUser as loginUserAction,
  addUserRules as addUserRulesAction,
  verifyUserRegistration as verifyUserRegistrationAction,
  forgotUserPassword as forgotUserPasswordAction,
  socialLoginUser as socialLoginUserAction,
  logUserOut as logUserOutAction,
  getExistingMobile as getExistingMobileAction
} from "../actions";

import {
  fetchRules as fetchRulesAction,
  fetchUserRules as fetchUserRulesAction,
  addOrUpdateUserRules as addOrUpdateUserRulesAction
} from "../../../constants/commonActions";
import Register from "../components/new-registration-form";

const mapStateToProps = ({
  loginOrRegister: {
    userData,
    type,
    userLoginData,
    serverErrorLogin,
    serverErrorRegister,
    serverErrorForgotPassword,
    uploadPicData,
    tokenRefreshed,
    showLoader
  },
  common: { rulesList, userRulesData }
}) => ({
  rulesList,
  type,
  userData,
  userLoginData,
  serverErrorLogin,
  serverErrorRegister,
  serverErrorForgotPassword,
  tokenRefreshed,
  userRulesData,
  uploadPicData,
  showLoader
});

const mapDispatchToProps = dispatch => ({
  socialLogin: inputData => dispatch(socialLoginUserAction(inputData)),
  registerUser: inputData => dispatch(registerUserAction(inputData)),
  loginUser: inputData => dispatch(loginUserAction(inputData)),
  fetchRules: inputData => dispatch(fetchRulesAction()),
  fetchUserRules: inputData => dispatch(fetchUserRulesAction(inputData)),
  addOrUpdateUserRules: inputData =>
    dispatch(addOrUpdateUserRulesAction(inputData)),
  logUserOut: inputData => dispatch(logUserOutAction(inputData)),
  verifyUserRegistration: inputData =>
    dispatch(verifyUserRegistrationAction(inputData)),
  forgotUserPassword: inputData => dispatch(forgotUserPasswordAction(inputData)),
  existingMobileStatus: inputData => dispatch(getExistingMobileAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  Register
);
