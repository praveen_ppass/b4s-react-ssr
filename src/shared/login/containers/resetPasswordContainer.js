import { connect } from "react-redux";

import {
  resetUserPassword as resetUserPasswordAction,
  resetTokenCheck as resetTokenCheckAction
} from "../actions";

import ResetPasswordPage from "../components/resetPasswordPage";

const mapStateToProps = ({
  loginOrRegister: { userPasswordResetData, type, showLoader }
}) => ({
  type,
  userPasswordResetData,
  showLoader
});

const mapDispatchToProps = dispatch => ({
  resetUserPassword: inputData => dispatch(resetUserPasswordAction(inputData)),
  resetTokenCheck: inputData => dispatch(resetTokenCheckAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(ResetPasswordPage);
