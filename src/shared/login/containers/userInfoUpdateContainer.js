import { connect } from "react-redux";
import UserInfoUpdate from "../components/userInfoUpdate";
import { getAllRules as getAllRulesAction } from "../../application/actions/applicationFormAction";
import {
  getUserInfoByToken as getUserInfoByTokenAction,
  sendUserInfoByToken as sendUserInfoByTokenAction,
  sendUserInfoByUserId as sendUserInfoByUserIdAction
} from "../actions";
const mapStateToProps = ({ applicationForm, loginOrRegister }) => ({
  rulesData: applicationForm.rulesData,
  type: applicationForm.type,
  showLoader: applicationForm.showLoader,
  userInfo: loginOrRegister.userInfo,
  infoType: loginOrRegister.type
});

const mapDispatchToProps = dispatch => ({
  fetchAllRules: data => dispatch(getAllRulesAction(data)),
  fetchUserInfo: data => dispatch(getUserInfoByTokenAction(data)),
  sendUserInfo: data => dispatch(sendUserInfoByTokenAction(data)),
  sendUserInfoById: data => dispatch(sendUserInfoByUserIdAction(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserInfoUpdate);
