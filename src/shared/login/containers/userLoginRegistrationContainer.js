import { connect } from "react-redux";

import {
  registerUser as registerUserAction,
  loginUser as loginUserAction,
  addUserRules as addUserRulesAction,
  verifyUserRegistration as verifyUserRegistrationAction,
  forgotUserPassword as forgotUserPasswordAction,
  socialLoginUser as socialLoginUserAction,
  logUserOut as logUserOutAction,
  sendMobileForOtp as sendMobileForOtpAction,
  sendOtpForLogin as sendOtpForLoginAction,
  sendOtpForPasswordChange as sendOtpForPasswordChangeAction,
} from "../actions";

import {
  verifyUserOTPAction,
  getOTPCountingAction
} from "../../scholarship-conclave/actions"

import {
  fetchRules as fetchRulesAction,
  fetchUserRules as fetchUserRulesAction,
  addOrUpdateUserRules as addOrUpdateUserRulesAction
} from "../../../constants/commonActions";
import UserLoginRegistrationPopup from "../components/userLoginRegistration";

const mapStateToProps = ({
  loginOrRegister: {
    userData,
    type,
    userLoginData,
    serverErrorLogin,
    serverErrorRegister,
    serverErrorForgotPassword,
    uploadPicData,
    tokenRefreshed,
    showLoader,
    userLogData,
    otpPasswordChange,
    otpPasswordError
  },
  common: { rulesList, userRulesData },
  scholarshipConclave
}) => ({
  rulesList,
  type,
  userData,
  userLoginData,
  serverErrorLogin,
  serverErrorRegister,
  serverErrorForgotPassword,
  tokenRefreshed,
  userRulesData,
  uploadPicData,
  showLoader,
  otpPasswordChange,
  otpPasswordError,
  userLogData,
  varifyType: scholarshipConclave.type,
  verifyOTPResp: scholarshipConclave.verifyOTPResp,
  otpCounter: scholarshipConclave.otpCounter,
  errorMessage: scholarshipConclave.errorMessage,
  showVerLoader: scholarshipConclave.showLoader
});

const mapDispatchToProps = dispatch => ({
  socialLogin: inputData => dispatch(socialLoginUserAction(inputData)),
  registerUser: inputData => dispatch(registerUserAction(inputData)),
  loginUser: inputData => dispatch(loginUserAction(inputData)),
  fetchRules: inputData => dispatch(fetchRulesAction()),
  fetchUserRules: inputData => dispatch(fetchUserRulesAction(inputData)),
  addOrUpdateUserRules: inputData =>
    dispatch(addOrUpdateUserRulesAction(inputData)),
  logUserOut: inputData => dispatch(logUserOutAction(inputData)),
  verifyUserRegistration: inputData =>
    dispatch(verifyUserRegistrationAction(inputData)),
  forgotUserPassword: inputData => dispatch(forgotUserPasswordAction(inputData)),
  sendMobileForOtp: inputData => dispatch(sendMobileForOtpAction(inputData)),
  sendOtpForLogin: inputData => dispatch(sendOtpForLoginAction(inputData)),
  sendOtpForPasswordChange: inputData => dispatch(sendOtpForPasswordChangeAction(inputData)),
  verifyOTP: data => dispatch(verifyUserOTPAction(data)),
  fetchOTPCounting: inputData => dispatch(getOTPCountingAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  UserLoginRegistrationPopup
);
