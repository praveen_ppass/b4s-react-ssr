import { connect } from "react-redux";

import { verifyUserRegistration as verifyUserRegistrationAction } from "../actions";

import VerifyUserRegistration from "../../login/components/verifyUserRegistration";

const mapStateToProps = ({ loginOrRegister: { showLoader } }) => ({
  showLoader
});

const mapDispatchToProps = dispatch => ({
  verifyUserRegistration: inputData =>
    dispatch(verifyUserRegistrationAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  VerifyUserRegistration
);
