import { call, put, takeEvery } from "redux-saga/effects";
import axios from "axios";

import {
  loginRegisterRequiredRules,
  loginRegisterRuleTypes,
  messages
} from "../../constants/constants";
import gblFunc from "../../globals/globalFunctions";

import {
  REGISTER_USER_REQUEST,
  REGISTER_USER_SUCCEEDED,
  REGISTER_USER_FAILED,
  LOGIN_USER_REQUESTED,
  LOGIN_USER_SUCCEEDED,
  LOGIN_USER_FAILED,
  ADD_USER_RULES_FAILED,
  ADD_USER_RULES_REQUESTED,
  ADD_USER_RULES_SUCCEEDED,
  UPDATE_USER_PASSWORD_SUCCESS,
  UPDATE_USER_PASSWORD_FAILURE,
  UPDATE_USER_PASSWORD_REQUEST,
  VERIFY_USER_REGISTRATION_SUCCESS,
  VERIFY_USER_REGISTRATION_FAILURE,
  VERIFY_USER_REGISTRATION_REQUEST,
  //Forgot user password actions
  FORGOT_USER_PASSWORD_REQUEST,
  FORGOT_USER_PASSWORD_SUCCESS,
  FORGOT_USER_PASSWORD_FAILURE,
  RESET_USER_PASSWORD_SUCCESS,
  RESET_USER_PASSWORD_FAILURE,
  RESET_USER_PASSWORD_REQUEST,
  SOCIAL_LOGIN_USER_REQUESTED,
  LOG_USER_OUT_SUCCEEDED,
  LOG_USER_OUT_FAILED,
  LOG_USER_OUT,
  RESET_USER_PASSWORD_TOKEN_CHECK_SUCCESS,
  RESET_USER_PASSWORD_TOKEN_CHECK_REQUEST,
  RESET_USER_PASSWORD_TOKEN_CHECK_FAILURE,
  SEND_OTP_REQUEST,
  SEND_OTP_SUCCESS,
  SEND_OTP_FAILED,
  VERIFY_OTP_SUCCESS,
  VERIFY_OTP_REQUEST,
  VERIFY_OTP_FAILED,
  GET_USERINFO_BY_TOKEN_REQUEST,
  GET_USERINFO_BY_TOKEN_SUCCESS,
  GET_USERINFO_BY_TOKEN_FAILURE,
  POST_USERINFO_BY_TOKEN_REQUEST,
  POST_USERINFO_BY_TOKEN_SUCCESS,
  POST_USERINFO_BY_TOKEN_FAILURE,
  POST_USERINFO_BY_USERID_REQUEST,
  POST_USERINFO_BY_USERID_SUCCESS,
  POST_USERINFO_BY_USERID_FAILURE,
  GET_EXISTING_MOBILE_REQUEST,
  GET_EXISTING_MOBILE_SUCCESS,
  GET_EXISTING_MOBILE_FAILURE,
  POST_EXISTING_MOBILE_REQUEST,
  POST_EXISTING_MOBILE_SUCCESS,
  POST_EXISTING_MOBILE_FAILURE,
  POST_OTP_FOR_PASSWORD_CHANGE_REQUEST,
  POST_OTP_FOR_PASSWORD_CHANGE_SUCCESS,
  POST_OTP_FOR_PASSWORD_CHANGE_FAILURE,
} from "./actions";

import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";
import { FETCH_USER_RULES_REQUEST } from "../../constants/commonActions";

/* Register user saga */
const registerUserApi = input =>
  fetchClient.post(apiUrl.registerUser, input).then(res => {
    return res.data;
  });

function* registerUser({ payload }) {
  try {
    const response = yield call(registerUserApi, payload);

    gblFunc.gaTrack.trackEvent(["Registration", "Email"]);
    yield put({
      type: REGISTER_USER_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    //FIXME: Don't hardcode here.
    if (error.response && error.response.data.errorCode === 702) {
      //same user registration check
      yield put({
        type: REGISTER_USER_FAILED,
        payload: error.response.data.message,
        isServerError: true
      });
    } else {
      yield put({
        type: REGISTER_USER_FAILED,
        payload: error,
        isServerError: false
      });
    }
  }
}

/* New login saga */

//Fetch rules in case user rules are missing
//TODO: Add handling for rules failure
const fetchLoginRulesApi = input =>
  fetchClient
    .get(apiUrl.rulesList, {
      params: { filter: JSON.stringify(input) }
    })
    .then(res => {
      return res.data;
    });

//User rules are fetched in case of successful login
//TODO: Add handling for user rules failure
const userRulesApi = ({ inputData }) => {
  const url = `${apiUrl.userRules}/${inputData.userid}/lite`;
  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

const newLoginApi = input =>
  axios({
    url: apiUrl.loginUserNew,
    method: "post",
    headers: {
      "content-type":
        "multipart/form-data; boundary=---011000010111000001101001"
    },
    data: input,
    //FIXME: Add in config file
    auth: {
      username: "b4s",
      password: "B$S!#"
    }
  }).then(res => {
    return res.data;
  });

function* loginUserNew({ payload }) {
  try {
    const { username, password } = payload;

    var form = new FormData();
    form.append("username", username);
    form.append("password", password);
    form.append("grant_type", "password");
    form.append("portalId", 1);

    const response = yield call(newLoginApi, form);

    if (
      response.userId != null &&
      response.userId != "" &&
      response.isVerified
    ) {
      gblFunc.gaTrack.trackEvent(["Login", "Email"]);

      const userRulesData = yield call(userRulesApi, {
        userid: response.userId
      });

      const { userRules } = userRulesData;

      //Check if user rules are missing or present
      const userRulesPresent =
        userRules.filter(usrRule =>
          loginRegisterRequiredRules.includes(usrRule.ruleTypeId)
        ).length === 4;

      /* Store user data here. (Auth token, firstname lastname pic etc)
      NOTE: THIS WILL BE DONE IN BOTH CASES( SINCE INTERMEDIATE FORM HAS LOGOUT BUTTON )
      */
      const {
        firstName,
        lastName,
        pic,
        profilePercentage,
        membershipExpiry
      } = userRulesData;
      //FIXME: Store this when action is recieved in reducer
      gblFunc.storeAuthDetails(response);
      gblFunc.storeUserDetails({
        firstName,
        lastName,
        pic,
        profilePercentage,
        vleUser,
        membershipExpiry
      });

      if (userRulesPresent) {
        response.showIntermediateForm = false;
      } else {
        response.showIntermediateForm = true;
        const { RULETYPES } = loginRegisterRuleTypes;
        const loginRules = yield call(fetchLoginRulesApi, RULETYPES);
      }

      yield put({
        type: LOGIN_USER_SUCCEEDED,
        payload: response
      });
    }
    // else {
    //   response.showIntermediateForm = true;
    //   yield put({
    //     type: "VERIFY_TOKEN",
    //     payload: response
    //   });
    // }
  } catch (error) {
    if (error.response.status === 400 || error.response.status === 401) {
      yield put({
        type: LOGIN_USER_FAILED,
        payload: "Invalid email address or password"
      });
    }
  }
}

/* Login user saga */

const loginUserApi = input =>
  axios({
    url: apiUrl.loginUser,
    method: "post",
    headers: {
      "content-type":
        "multipart/form-data; boundary=---011000010111000001101001"
    },
    data: input,
    //FIXME: Add in config file
    auth: {
      username: "b4s",
      password: "B$S!#"
    }
  }).then(res => {
    return res.data;
  });

function* socialLogin({ payload }) {
  try {
    const { username, grant_type, userData, socialLogin, csc } = payload;
    var formData = new FormData();
    formData.append("username", username);
    formData.append("grant_type", grant_type);
    formData.append("userData", JSON.stringify(userData));
    formData.append("socialLogin", socialLogin);

    const response = yield call(loginUserApi, formData);
    if (!csc) {
      if (response.userId != null && response.userId != "") {
        if (
          userData.userSource == /Android/i.test(navigator.userAgent)
            ? "FB-MOBILE-WEBSITE"
            : "FB-WEBSITE" && !response.newUser
        ) {
          gblFunc.gaTrack.trackEvent(["Login", "Facebook"]);
        } else {
          gblFunc.gaTrack.trackEvent(["Registration", "Facebook"]);
        }

        if (
          userData.userSource == /Android/i.test(navigator.userAgent)
            ? "GOOGLE-MOBILE-WEBSITE"
            : "GOOGLE-WEBSITE" && !response.newUser
        ) {
          gblFunc.gaTrack.trackEvent(["Login", "Google"]);
        } else {
          gblFunc.gaTrack.trackEvent(["Registration", "Google"]);
        }
      }
    }

    yield put({
      type: LOGIN_USER_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    if (
      error.response &&
      error.response.errorCode &&
      (error.response.errorCode == 1001 || error.response.errorCode == 1002)
    ) {
      yield put({
        type: "VERIFY_TOKEN",
        payload: error.response
      });
    } else if (
      error.response &&
      error.response.data &&
      error.response.data.errorCode &&
      (error.response.data.errorCode == 1001 ||
        error.response.data.errorCode == 1002)
    ) {
      yield put({
        type: "VERIFY_TOKEN",
        payload: error.response
      });
    } else {
      yield put({
        type: LOGIN_USER_FAILED
        //payload: error.response.data.BODY.DATA
      });
    }
  }
}

function* loginUser({ payload }) {
  try {
    const { username, password } = payload;

    var form = new FormData();
    form.append("username", username);
    form.append("password", password);
    form.append("grant_type", "password");
    form.append("portalId", 1);

    const response = yield call(loginUserApi, form);
    if (
      response.userId != null &&
      response.userId != "" &&
      response.isVerified
    ) {
      gblFunc.gaTrack.trackEvent(["Login", "Email"]);
    }

    // if (response.isVerified) {//commented as of now, as per requirement
    yield put({
      type: LOGIN_USER_SUCCEEDED,
      payload: response
    });
    // } else {
    //   yield put({
    //     type: LOGIN_USER_FAILED,
    //     payload: "Please verify your email address before you login"
    //   });
    // }
  } catch (error) {
    if (
      error.response &&
      error.response.errorCode &&
      (error.response.errorCode == 1001 || error.response.errorCode == 1002)
    ) {
      yield put({
        type: "VERIFY_TOKEN",
        payload: error.response
      });
    } else if (
      error.response &&
      error.response.data &&
      error.response.data.errorCode &&
      (error.response.data.errorCode == 1001 ||
        error.response.data.errorCode == 1002)
    ) {
      yield put({
        type: "VERIFY_TOKEN",
        payload: error.response
      });
    }
    if (error.response.status === 400 || error.response.status === 401) {
      yield put({
        type: LOGIN_USER_FAILED,
        payload: "Invalid email/mobile or password"
      });
    }
  }
}

const verifyOtp = input =>
  axios({
    url: apiUrl.loginUser,
    method: "post",
    headers: {
      "content-type":
        "multipart/form-data; boundary=---011000010111000001101001"
    },
    data: input,
    auth: {
      username: "b4s",
      password: "B$S!#"
    }
  }).then(res => {
    return res.data;
  });

const sendOtp = input =>
  axios({
    url: apiUrl.loginUser,
    method: "post",
    headers: {
      "content-type":
        "multipart/form-data; boundary=---011000010111000001101001"
    },
    data: input,
    auth: {
      username: "b4s",
      password: "B$S!#"
    }
  }).then(res => {
    return res.data;
  });

function* verifyOtpRequest({ payload }) {
  try {
    const response = yield call(verifyOtp, payload);
    // if (
    //   response.userId != null &&
    //   response.userId != "" &&
    //   response.isVerified
    // ) {
    //   gblFunc.gaTrack.trackEvent(["Login", "Email"]);
    // }
    yield put({
      type: VERIFY_OTP_SUCCESS,
      payload: response
    });
  } catch (error) {
    if (error.response.status === 400 || error.response.status === 401) {
      yield put({
        type: VERIFY_OTP_FAILED,
        payload: "Verify OTP request failed"
      });
    }
  }
}

function* sendOtpRequest({ payload }) {
  try {
    const response = yield call(sendOtp, payload);
    // if (
    //   response.userId != null &&
    //   response.userId != "" &&
    //   response.isVerified
    // ) {
    //   gblFunc.gaTrack.trackEvent(["Login", "Email"]);
    // }
    yield put({
      type: SEND_OTP_SUCCESS,
      payload: response
    });
  } catch (error) {
    if (error.response.status === 400 || error.response.status === 401) {
      yield put({
        type: SEND_OTP_FAILED,
        payload: "Send OTP request failed"
      });
    }
  }
}

const addUserRulesApi = ({ USERID, rules }) =>
  fetchClient.post(`${apiUrl.addUserRules}/${USERID}`, { rules }).then(res => {
    return res.data;
  });

function* addUserRules(input) {
  try {
    const { payload } = input;
    const response = yield call(addUserRulesApi, payload);

    yield put({
      type: ADD_USER_RULES_SUCCEEDED,
      payload: response.BODY.DATA
    });
  } catch (error) {
    yield put({
      type: ADD_USER_RULES_FAILED,
      payload: error
    });
  }
}

/* Change user password  saga */

const updateUserPasswordApi = ({ inputData }) =>
  fetchClient
    .post(`${apiUrl.changeUserPassword}/${inputData.userid}/password/change`, {
      ...inputData.params,
      portalId: 1
    })
    .then(res => res.data);

function* updatePassword(input) {
  try {
    const changePasswordData = yield call(updateUserPasswordApi, input.payload);

    yield put({
      type: UPDATE_USER_PASSWORD_SUCCESS,
      payload: changePasswordData
    });
  } catch (error) {
    if (error.response.data.errorCode === 803) {
      yield put({
        type: UPDATE_USER_PASSWORD_FAILURE,
        payload: error.response.data.message
      });
    } else {
      yield put({
        type: UPDATE_USER_PASSWORD_FAILURE,
        payload: error
      });
    }
  }
}

/*  User registration verification */

const verifyUserRegistrationApi = ({ inputData }) =>
  fetchClient
    .post(apiUrl.verifyUserRegistration, inputData.params)
    .then(res => res.data);

function* verifyUserRegistration(input) {
  try {
    const userRegistrationVerification = yield call(
      verifyUserRegistrationApi,
      input.payload
    );

    yield put({
      type: VERIFY_USER_REGISTRATION_SUCCESS,
      payload: userRegistrationVerification
    });
  } catch (error) {
    yield put({
      type: VERIFY_USER_REGISTRATION_FAILURE,
      payload: error.response
    });
  }
}

/* Forgot user password */

const forgotUserPasswordApi = ({ inputData }) =>
  fetchClient
    .post(apiUrl.forgotUserPassword, inputData.params)
    .then(res => res.data);

function* forgotUserPassword(input) {
  try {
    const forgotUserPasswordData = yield call(
      forgotUserPasswordApi,
      input.payload
    );

    yield put({
      type: FORGOT_USER_PASSWORD_SUCCESS,
      payload: forgotUserPasswordData
    });
  } catch (error) {
    if (error.response.status === 404) {
      yield put({
        type: FORGOT_USER_PASSWORD_FAILURE,
        payload: messages.forgotPssword.error
      });
    } else {
      yield put({
        type: FORGOT_USER_PASSWORD_FAILURE,
        payload: error.response
      });
    }
  }
}

/*  Reset user password -> Forgot password case */
const resetUserPasswordApi = ({ inputData }) =>
  fetchClient
    .post(apiUrl.resetUserPassword, inputData.params)
    .then(res => res.data);

function* resetUserPassword(input) {
  try {
    const resetUserPasswordData = yield call(
      resetUserPasswordApi,
      input.payload
    );

    yield put({
      type: RESET_USER_PASSWORD_SUCCESS,
      payload: resetUserPasswordData
    });
  } catch (error) {
    yield put({
      type: RESET_USER_PASSWORD_FAILURE,
      payload: error.response
    });
  }
}

/* Guest auth token call*/
const guestTokenCall = () => {
  let input = new FormData();
  input.append("grant_type", "client_credentials");
  input.append("portalId", 1);

  return axios({
    url: apiUrl.tokenGeneration,
    method: "post",
    headers: {
      "content-type":
        "multipart/form-data; boundary=---011000010111000001101001"
    },
    data: input,
    auth: {
      username: "b4s",
      password: "B$S!#"
    }
  }).then(res => res.data);
};

function* getGuestToken() {
  try {
    const guestAuthtoken = yield call(guestTokenCall);

    //Store guest auth token call here
    gblFunc.storeAuthToken(guestAuthtoken.access_token);

    yield put({
      type: LOG_USER_OUT_SUCCEEDED,
      payload: guestAuthtoken
    });
  } catch (error) {
    yield put({
      type: LOG_USER_OUT_FAILED,
      payload: error.errorMessage
    });
  }
}

const resetTokenCheckApi = inputData =>
  fetchClient
    .post(apiUrl.resetPasswordTokenCheck, inputData)
    .then(res => res.data);

function* resetTokenCheck(input) {
  try {
    const resetToken = yield call(resetTokenCheckApi, input.payload);

    yield put({
      type: RESET_USER_PASSWORD_TOKEN_CHECK_SUCCESS,
      payload: resetToken
    });
  } catch (error) {
    yield put({
      type: RESET_USER_PASSWORD_TOKEN_CHECK_FAILURE,
      payload: error.response
    });
  }
}

// get user info by token
const getUserInfoApi = inputData => {
  return fetchClient
    .get(`${apiUrl.userInfoByToken}/token/${inputData.userToken}`)
    .then(res => res.data);
};
function* getUserInfo(input) {
  try {
    const response = yield call(getUserInfoApi, input.payload);
    yield put({
      type: GET_USERINFO_BY_TOKEN_SUCCESS,
      payload: response
    });
  } catch (error) {
    yield put({
      type: GET_USERINFO_BY_TOKEN_FAILURE,
      payload: error.response
    });
  }
}

// post user info by token
const sendUserInfoApi = inputData =>
  fetchClient
    .post(
      `${apiUrl.userInfoByToken}/token/${inputData.userToken}/rule`,
      inputData.data
    )
    .then(res => res.data);

function* sendUserInfo(input) {
  try {
    const response = yield call(sendUserInfoApi, input.payload);
    yield put({
      type: POST_USERINFO_BY_TOKEN_SUCCESS,
      payload: response
    });
  } catch (error) {
    yield put({
      type: POST_USERINFO_BY_TOKEN_FAILURE,
      payload: error.response
    });
  }
}

// post user info by userId
const sendUserRuleInfoApi = inputData => {
  const userId = localStorage.getItem("userId") || inputData.userId
  return fetchClient
    .post(
      `${apiUrl.userInfoByToken}/${userId}/rule`,
      inputData.data
    )
    .then(res => res.data);
}
function* sendUserRuleInfo(input) {
  try {
    const response = yield call(sendUserRuleInfoApi, input.payload);
    yield put({
      type: POST_USERINFO_BY_USERID_SUCCESS,
      payload: response
    });
  } catch (error) {
    yield put({
      type: POST_USERINFO_BY_USERID_FAILURE,
      payload: error.response
    });
  }
}

// get Existing mobile status
const getExistingMobileStatusApi = inputData => {
  return fetchClient
    .get(`${apiUrl.userInfoByToken}/mobile/exist?mobile=${inputData.mobile}&userId=${inputData.userId ? inputData.userId : ''}`)
    .then(res => res.data);
};
function* getExistingMobileStatus(input) {
  try {
    const response = yield call(getExistingMobileStatusApi, input.payload);
    yield put({
      type: GET_EXISTING_MOBILE_SUCCESS,
      payload: response
    });
  } catch (error) {
    yield put({
      type: GET_EXISTING_MOBILE_FAILURE,
      payload: error.response
    });
  }
}

const sendMobileForOtpApi = inputData => {
  return fetchClient
    .post(
      `${apiUrl.otpLogin}/otp`,
      { mobile: parseInt(inputData) }
    )
    .then(res => res.data);
}
function* sendMobileForOtp(input) {
  try {
    const response = yield call(sendMobileForOtpApi, input.payload);
    yield put({
      type: POST_EXISTING_MOBILE_SUCCESS,
      payload: response
    });
  } catch (error) {
    yield put({
      type: POST_EXISTING_MOBILE_FAILURE,
      payload: error
    });
  }
}

const sendOtpForPasswordChangeApi = inputData => {
  return fetchClient
    .post(
      `${apiUrl.otpPasswordChange}`,
      {
        mobile: parseInt(inputData.mobile),
        userId: inputData.userId,
        otp: inputData.otp
      }
    )
    .then(res => res.data);
}
function* sendOtpForPasswordChange(input) {
  try {
    const response = yield call(sendOtpForPasswordChangeApi, input.payload);
    yield put({
      type: POST_OTP_FOR_PASSWORD_CHANGE_SUCCESS,
      payload: response
    });
  } catch (error) {
    yield put({
      type: POST_OTP_FOR_PASSWORD_CHANGE_FAILURE,
      payload: error.response.data
    });
  }
}

export default function* loginRegisterSaga() {
  yield takeEvery(RESET_USER_PASSWORD_REQUEST, resetUserPassword);
  yield takeEvery(LOG_USER_OUT, getGuestToken);
  yield takeEvery(FORGOT_USER_PASSWORD_REQUEST, forgotUserPassword);
  yield takeEvery(VERIFY_USER_REGISTRATION_REQUEST, verifyUserRegistration);
  yield takeEvery(ADD_USER_RULES_REQUESTED, addUserRules);
  yield takeEvery(REGISTER_USER_REQUEST, registerUser);
  yield takeEvery(LOGIN_USER_REQUESTED, loginUser);
  yield takeEvery(UPDATE_USER_PASSWORD_REQUEST, updatePassword);
  yield takeEvery(SOCIAL_LOGIN_USER_REQUESTED, socialLogin);
  yield takeEvery(RESET_USER_PASSWORD_TOKEN_CHECK_REQUEST, resetTokenCheck);
  yield takeEvery(GET_USERINFO_BY_TOKEN_REQUEST, getUserInfo);
  yield takeEvery(POST_USERINFO_BY_TOKEN_REQUEST, sendUserInfo);
  yield takeEvery(POST_USERINFO_BY_USERID_REQUEST, sendUserRuleInfo);
  yield takeEvery(GET_EXISTING_MOBILE_REQUEST, getExistingMobileStatus);
  yield takeEvery(POST_EXISTING_MOBILE_REQUEST, sendMobileForOtp);
  yield takeEvery(POST_OTP_FOR_PASSWORD_CHANGE_REQUEST, sendOtpForPasswordChange);
}
