import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

/************   All actions required in this module starts *********** */
import {
  FETCH_QUESTION_ANSWER_REQUESTED_BRAND,
  FETCH_QUESTION_ANSWER_SUCCEEDED_BRAND,
  FETCH_QUESTION_ANSWER_FAILED_BRAND,
} from "./action";

const fetchQuestAnsAPI = ({ orgId }) =>
  fetchClient.get(apiUrl.getQuestAnsw1 + "/qna-question/organisation/" + orgId+"?size=2").then(res => {
    return res.data;
  });

function* fetchQuestAns(input) {
  try {
    const questAnsw = yield call(fetchQuestAnsAPI, input.payload);
    yield put({
      type: FETCH_QUESTION_ANSWER_SUCCEEDED_BRAND,
      payload: questAnsw
    });
  } catch (error) {
    yield put({
      type: FETCH_QUESTION_ANSWER_FAILED_BRAND,
      payload: error
    });
  }
}


export default function* brandPageSaga1() {
  yield takeEvery(FETCH_QUESTION_ANSWER_REQUESTED_BRAND, fetchQuestAns);
}
