import React from "react";
import { connect } from "react-redux";
import Colgate from "../components/colgatePages";
import { applyNowHitCounter as applyNowHitCounterAction } from "./../../../shared/brandPage/action";
import {
  
  fetchAutoSuggestList as fetchAutoSuggestListAction,
  saveQuestion as saveQuestionAction,
  fetchCatgList as fetchCatgListAction,
  fetchQuesAnsTypes as questAnswTypeAction,

} from "../../question-answer/actions";
import {fetchQuesAnsBrand} from  "../action";

const mapStateToProps = ({ scholarship,quesAnswerData,brandPageReducer1 }) => ({
  applyHit: scholarship.applyHitCounter,
  autoSuggestQuests: quesAnswerData.autoSuggestQuestList,
  categoryTags: quesAnswerData.categoryTags,
  type: quesAnswerData.type,
  errorMessage: quesAnswerData.errorMessage,
  quesAnswer1:brandPageReducer1.quesAnswer1
  
});

const mapDispatchToProps = dispatch => ({
  applyNowHit: inputData => dispatch(applyNowHitCounterAction(inputData)),
  loadAutoSuggestList: inputData =>
    dispatch(fetchAutoSuggestListAction(inputData)),
    postQuestion: inputData => dispatch(saveQuestionAction(inputData)),
    loadCatgList: inputData => dispatch(fetchCatgListAction(inputData)),
    fetchQuesAnsType: inputData => dispatch(questAnswTypeAction(inputData)),
    fetchQuesAnsBrand: inputData => dispatch(fetchQuesAnsBrand(inputData)),




});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Colgate);
