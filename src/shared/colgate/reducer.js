import {
  FETCH_QUESTION_ANSWER_REQUESTED_BRAND,
  FETCH_QUESTION_ANSWER_SUCCEEDED_BRAND,
  FETCH_QUESTION_ANSWER_FAILED_BRAND,
 
} from "./action";


const initialState = {
  showLoader: true,
  isError: false,
  errorMessage: "",
  quesAnswer: null,
  homeQuesAnswer: null,
  autoSuggestQuestList: [],
  savedLikeFollow: null,
  likeFollowDts: null,
  allLikeFollowDts: null,
  postedQuestionDts: null,
  answers: []
};

const brandPageReducer1 = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_QUESTION_ANSWER_REQUESTED_BRAND:

      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case FETCH_QUESTION_ANSWER_SUCCEEDED_BRAND:
      return {
        ...state,
        quesAnswer1: payload,
        showLoader: false,
        isError: false,
        type
      };
    case FETCH_QUESTION_ANSWER_FAILED_BRAND:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage1: payload
      };

    default:
      return state;
  }
};

export default brandPageReducer1;
