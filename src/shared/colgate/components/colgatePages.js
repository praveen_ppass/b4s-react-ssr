import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { Helmet } from "react-helmet";
import Responsive from "./studentVideosSlider";
import gblFunc from "../../../globals/globalFunctions";
import Loader from "../../common/components/loader";
import QuestionPopup from "../../question-answer/components/questionPopup";
import { messages } from "../../../constants/constants";
import {
  FETCH_CATEGORY_LIST_SUCCEEDED,
  SAVE_QUESTION_SUCCEEDED,
  SAVE_QUESTION_FAILED
} from "../../question-answer/actions";
import UserLoginRegistrationPopup from "../.././login/containers/userLoginRegistrationContainer";
import {
  isMobile
} from "react-device-detect";
import {
  WhatsappShareButton, WhatsappIcon
} from "react-share";
import { required } from "../../../validation/rules";
import { ruleRunner } from "../../../validation/ruleRunner";
import AlertMessage from "../../common/components/alertMsg";
var timerId;
class Colgate extends Component {
  constructor() {
    super();
    this.state = {
      showItem: null,
      activeTab: "",
      isFlag: true,
      stickClass: "",
      scrollClass: false,
      baseId: "",
      openLoginPagePopup: false,
      pathName: null,
      scholarshipId: null,
      isFollowStatus: false,
      showLoginPopup: false,
      totalFollowers: "",
      showAlertMessagePopup: false,
      scholarshipData: null,
      showQuestionPopUp: false,
      quesStatusErr: false,
      postQuestionVal: "",
      selectedOptions: [],
      options: [],
      validations: {
        // additionalDetail: null,
        questionText: null,
        qnaCategories: null
      },
      msg: "",


      questData: {
        selectedOptions: null,
        // additionalDts: null,
        questionText: null
      }
    };
    this.frequestAskedQuesHandler = this.frequestAskedQuesHandler.bind(this);
    this.onApplyHandler = this.onApplyHandler.bind(this);
    this.closeAd = this.closeAd.bind(this);
    this.onScrollHandler = this.onScrollHandler.bind(this);
    this.scroll = this.scroll.bind(this);
    this.changeTab = this.changeTab.bind(this);
    this.renderScholers = this.renderScholers.bind(this);
    this.closePopup = this.closePopup.bind(this);
    this.closeLoginPopup = this.closeLoginPopup.bind(this);
    this.brandFollowHandler = this.brandFollowHandler.bind(this);
    this.onCanclePostQuestionPopUp = this.onCanclePostQuestionPopUp.bind(this);
    this.onQuestionPopUpHandler = this.onQuestionPopUpHandler.bind(this);
    this.showQuesPopUpHandler = this.showQuesPopUpHandler.bind(this);
    this.onSubmitQuestionHandler = this.onSubmitQuestionHandler.bind(this);
    this.onAdditionDtHandler = this.onAdditionDtHandler.bind(this);
    this.onSearchPopQuesHandler = this.onSearchPopQuesHandler.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.onPostChangeHandler = this.onPostChangeHandler.bind(this);



  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
  }

  onPostChangeHandler({ target }) {
    const questData = { ...this.state };
    questData.questionText = target.value;
    const isAuth = gblFunc.isUserAuthenticated() || this.props.isAuthenticated;

    if (isAuth) {
      if (target.value) {
        this.setState(
          {
            postQuestionVal: target.value,
            questData
          },
          () => this.showQuesPopUpHandler()
        );
      } else {
        this.showQuesPopUpHandler();
      }
    } else if (!isAuth) {
      this.setState({
        showLoginPopup: true
      });
    } else {
      this.setState({
        postQuestionVal: target.value,
        questData
      });
    }
  }


  checkFormValidations(questData) {
    let validations = { ...this.state.validations };

    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);

      let value;
      if (
        // key == "qnaCategories" &&
        questData[key] &&
        questData[key].length == 0
      ) {
        value = "";
      } else if (
        // key == "qnaCategories" &&
        questData[key] &&
        questData[key].length > 0
      ) {
        value = true;
      } else {
        value = questData[key];
      }

      let validationResult = ruleRunner(
        value,
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations
    });
    return isFormValid;
  }


  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "qnaCategories":
        validationObject.name = "* Category";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "questionText":
        validationObject.name = "* Question";
        validationObject.validationFunctions = [required];
        return validationObject;
      // case "additionalDetail":
      //   validationObject.name = "* Additional Detail";
      //   validationObject.validationFunctions = [required];
      //   return validationObject;
    }
  }

  onSearchPopQuesHandler(value) {
    const questData = { ...this.state.questData };

    questData["questionText"] = value;

    this.setState({ questData });
  }

  onAdditionDtHandler({ target }) {
    const updatedQuesData = { ...this.state.questData };
    updatedQuesData[target.name] = target.value;
    this.setState({
      questData: updatedQuesData
    });
  }
  onSubmitQuestionHandler(e) {
    e.preventDefault();
    const { selectedOptions, questData } = this.state;
    var selectedOptionsTag = [];
    var data1 = {};
    if (!!this.props.aboutUsData && !!this.props.aboutUsData.qnaCategoryId) {
      data1 = {
        id: this.props.aboutUsData.qnaCategoryId
      }

    }
    else {
      data1 = {
        id: 99,
        text: "Miscellaneous Questions",
        slug: "na",
        status: 1,
        weight: 1,
      };

    }

    // selectedOptionsTag[0]= this.props.categoryTags.data[56];
    selectedOptionsTag[0] = data1;
    const data = {
      qnaCategories: selectedOptionsTag,
      questionText: questData && questData.questionText,
      // additionalDetail: questData && questData.additionalDts,
      orgId: this.state.orgId
    };
    const isAuth = gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    if (this.checkFormValidations(data)) {
      if (isAuth) {
        data.authorUserId = gblFunc.getStoreUserDetails()["userId"];
        this.props.postQuestion(data);
      }
      else {
        this.setState({
          showLoginPopup: true
        })

      }

    }
    // if(!isAuth){
    //   this.setState({
    //     showLoginPopup: true
    //   })
    // }
  }
  onCanclePostQuestionPopUp() {
    this.setState({
      showQuestionPopUp: false,
      selectedOptions: [],
      postQuestionVal: "",
      msg: "",
      validations: {
        questionText: null,
        qnaCategories: null,
        // additionalDetail: null
      },
      questData: {
        selectedOptions: null,
        // additionalDts: null,
        questionText: null
      }
    });
  }

  showQuesPopUpHandler() {
    const { showQuestionPopUp } = this.state;

    if (!showQuestionPopUp) {
      this.setState(
        {
          showQuestionPopUp: true
        },
        () => this.props.loadCatgList({ page: 0, length: 100 })
      );
    }
  }

  onQuestionPopUpHandler(e) {
    e.preventDefault();

    const isAuth = gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    if (isAuth) {
      this.showQuesPopUpHandler();
    } else {
      this.setState({
        showLoginPopup: true
      });
    }
  }
  changeTab(tab) {
    if (tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  toggleTab(e) {
    if (e.target.nodeName === "LI") {
      const { id } = e.target;
      this.setState({
        activeTab: id
      });
    }
  }

  frequestAskedQuesHandler(index) {
    this.setState({
      showItem: index
    });
  }
  /* Tomorrow Task */
  onApplyHandler(scholership) {
    if (window) {
      const userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      const isAuth = parseInt(localStorage.getItem("isAuth"));
      gblFunc.gaTrack.trackEvent(["Scholarship", "Apply Now"]);
      if (userId && scholership.scholarshipId && isAuth) {
        this.props.applyNowHit({ userId, scholarshipId: scholership.scholarshipId });
        if (scholership.applyLink && (scholership.applyLink.includes("http") || scholership.applyLink.includes("https"))) {
          return window.location.replace(scholership.applyLink);
        } else if (scholership.applyLink) {
          return this.props.history.push(scholership.applyLink);
        }
      } else {
        this.setState({
          showLoginPopup: true,
          scholarshipData: { ...scholership }
        });
      }
    }
  }

  componentDidMount() {
    if (typeof window !== undefined) {
      window.addEventListener("scroll", this.scroll);
    }
    if (typeof window !== "undefined" && window.navigator.userAgent) {
      if (
        window.navigator.userAgent.indexOf("MSIE") > 0 ||
        window.navigator.userAgent.match(/Trident.*rv\:11\./)
      ) {
        this.setState({ showAlertMessagePopup: true });
      }
    }
  }

  componentWillUnmount() {
    if (typeof window !== undefined) {
      window.removeEventListener("scroll", this.scroll);
    }
    clearTimeout(timerId);
  }

  componentWillReceiveProps(nextProps) {
    const userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
    if (nextProps.loginSuccess && nextProps.loginSuccess.userLoginData && nextProps.loginSuccess.userLoginData.userId && this.state.scholarshipData) {
      const scholarshipData = this.state.scholarshipData;
      this.setState({ scholarshipData: null }, () => this.onApplyHandler(scholarshipData));
    }
    const orgId =
      nextProps && nextProps.aboutUsData && nextProps.aboutUsData.orgId;
    const groupIds = { userId, orgId };
    this.setState({
      orgId: nextProps && nextProps.aboutUsData && nextProps.aboutUsData.orgId,
      isFollowStatus:
        nextProps && nextProps.aboutUsData && nextProps.followerStatus.follow,
      actionType: nextProps.actionType,
      totalFollowers:
        nextProps && nextProps.followData && nextProps.followData.totalFollowed
    });
    let activeTab = "";
    if (this.state.activeTab === "" && nextProps) {
      activeTab =
        nextProps.aboutUsData &&
          nextProps.aboutUsData.videos &&
          nextProps.aboutUsData.videos.length > 0
          ? 3
          : 2;
      this.setState({ activeTab });
    }
    if (orgId && nextProps.actionType === "GET_BRAND_PAGE_FOLLOW_SUCCEEDED") {
      this.props.getFollowerStatus(groupIds);
    }
    // if (orgId) {
    //   if (!this.props.quesAnswer1) {
    //     this.props.fetchQuesAnsBrand({ orgId: orgId })
    //   }
    // }

    const { type, actionType } = nextProps;
    switch (actionType) {
      case "GET_BRAND_PAGE_ABOUT_SUCCEEDED":
        if (orgId) {
          if (!this.props.quesAnswer1) {
            this.props.fetchQuesAnsBrand({ orgId: orgId })
          }
        }
        break;
      default:
        break;
    }
    switch (type) {
      // case "FETCH_HOME_QUES_ANS_SUCCEEDED":
      //   this.setState(
      //     {
      //       quesAnsResponse: nextProps.homeQuesAnswer
      //     },
      //     () => {
      //       this.props.loadScholarships({
      //         page: 0,
      //         length: 10,
      //         rules: [{ rule: [698] }],
      //         mode: "OPEN"
      //       });

      // this.props.loadLikeAndFollow({
      //   userId: gblFunc.getStoreUserDetails()["userId"],
      //   categoryId
      // });
      //     }
      //   );
      //   break;
      // case "FETCH_SCHOLARSHIPS__SUCCEEDED":
      //   this.setState({
      //     internationalSch: nextProps.scholarshipList
      //   });
      //   break;
      case FETCH_CATEGORY_LIST_SUCCEEDED:
        const { categoryTags } = nextProps;

        if (categoryTags.data.length > 0) {
          this.setState({
            options: categoryTags.data
          });
        }

        break;


      case SAVE_QUESTION_SUCCEEDED:
        this.setState({
          msg: "Question posted successfully",
          isShow: true,
          showLoader: true,
          status: true,
          showQuestionPopUp: false,
          postQuestionVal: "",
          selectedOptions: [],
          validations: {
            questionText: null,
            qnaCategories: null,
            // additionalDetail: null
          }
        });
        // if(nextProps.data1.slug){
        //   this.props.history.push({
        //     pathname: `/qna/${nextProps.data1.slug}`
        //   });
        // }

        break;
      case SAVE_QUESTION_FAILED:
        const { data } = nextProps.errorMessage;
        const { generic } = messages;
        this.setState({
          msg:
            generic.httpErrors[data.errorCode] && data.errorCode !== 702
              ? generic.httpErrors[data.errorCode]
              : "Question is already posted",
          quesStatusErr: true
          // isShow: true,
          // showLoader: true,
          // status: false
        });
        break;
      default:
        break;
    }
  }

  scroll(e) {
    if (window.scrollY >= 300) {
      this.setState({
        stickClass: "stickeyfix"
      });
    }
    if (window.scrollY <= 300) {
      this.setState({
        stickClass: ""
      });
    }
  }

  closeAd() {
    this.setState({
      isFlag: false
    });
  }
  onScrollHandler(element, flag) {
    this.setState({ scrollClass: flag });

    //if (!timerId) {
    timerId = setTimeout(() => {
      this.setState({ scrollClass: false });
    }, 20000);
    //}
    element.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }

  renderScholers(scholer, index) {
    return (
      <article className="col-md-3" key={index}>
        <article className="box border">
          <article className="box-in">
            <img
              src={
                scholer.pic
                  ? scholer.pic
                  : "https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/profile-img.png"
              }
            />
            <h4>{scholer.name}</h4>
            <p>{scholer.scholarshipName}</p>
            <p>{scholer.presentClass}</p>
          </article>
        </article>
      </article>
    );
  }

  closePopup() {
    this.setState(
      {
        showLoginPopup: false
      });
  }
  closeLoginPopup() {
    this.setState({
      showLoginPopup: false
    });
  }

  brandFollowHandler(userId, orgId) {
    const user = {
      data: {
        userId,
        orgId,
        follow: !this.state.isFollowStatus,
        flgDeleted: false
      }
    };
    const isAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    if (isAuthenticated) {
      this.props.postBrandFollower(user);
    } else {
      this.setState({
        showLoginPopup: true
      });
    }
  }


  render() {
    const { aboutUsData, quesAnswer1 } = this.props;
    const userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
    const orgId = aboutUsData.orgId;
    const {
      isFollowStatus,
      totalFollowers,
      postQuestionVal,
      selectedOptions,
      options,
      questData,
      validations,
      quesStatusErr,
      msg
    } = this.state;
    const propsStates = {
      postQuestionVal,
      selectedOptions,
      options,
      questData,
      validations,
      quesStatusErr,
      msg
    };
    const isAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    const schema = {
      type: "application/ld+json",
      innerHTML: JSON.stringify({
        "@context": "https://schema.org",
        "@type": "https://schema.org/Article",
        name: aboutUsData ? aboutUsData.programName : "",
        datePublished: aboutUsData ? aboutUsData.datePublished : "",
        image: aboutUsData ? aboutUsData.logoUrl : "",
        author: {
          "@type": "Person",
          name: "Buddy4Study"
        },
        headline: aboutUsData ? aboutUsData.headline : "",
        publisher: {
          "@type": "Organization",
          name: "Buddy4Study",
          logo: {
            "@type": "ImageObject",
            name: "buddy4studyLogo",
            url:
              "https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/logo.png"
          }
        },
        mainEntityOfPage: aboutUsData
          ? `https://www.buddy4study.com/page/${aboutUsData.slug}`
          : "https://www.buddy4study.com"
      })
    };
    if (!!quesAnswer1) {
      var quesAnswer1List = quesAnswer1.data.map((item) => {
        return item
      })
    }
    return (
      <section>
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        {this.state.showAlertMessagePopup && (
          <p
            style={{
              background: "#f00",
              padding: "15px 0",
              color: "#fff",
              fontSize: "18px",
              textAlign: "center",
              width: " 100%",
              margin: "0"
            }}
          >
            {" "}
            <i className="fa fa-exclamation-triangle" />
            For better browsing experience and running web-based applications,
            please use updated version of Mozilla Firefox or Chrome or Microsoft
            Edge{" "}
          </p>
        )}

        {this.state.showQuestionPopUp ? (
          <QuestionPopup
            {...this.props}
            {...propsStates}
            onSelectHandler={this.onSelectHandler}
            onCanclePostQuestionPopUp={this.onCanclePostQuestionPopUp}
            onSubmitQuestionHandler={this.onSubmitQuestionHandler}
            onAdditionDtHandler={this.onAdditionDtHandler}
            onSearchPopQuesHandler={this.onSearchPopQuesHandler}
          />
        ) : null}
        {this.state.showLoginPopup ? (
          <UserLoginRegistrationPopup
            closePopup={this.closeLoginPopup}
            hideCloseButton={this.state.hideCloseButton}
            hideForgetCloseBtn={this.state.hideForgetCloseBtn}
            bsid={this.state.baseId}
            slug={this.props.aboutUsData.slug}
            pathName={this.props.history.location.pathname}
          />
        ) : null}
        <Helmet script={[schema]} />
        <section className=" MarginRow templateBg">
          <section className="container-fluid flexible">
            <article className="imgBanner">
              <img src={this.props.aboutUsData.bannerImageUrl} />
              {/* <article className="imgOverLay" /> */}
              <article className="container scholarshipName">
                <section className="row">
                  <section className="col-md-3">
                    <Link to="/">
                      <img
                        src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/white-logo.png"
                        className="logo"
                        alt="buddy4study-logo"
                      />
                    </Link>
                  </section>
                  <section className="col-md-9">
                    <img
                      src={this.props.aboutUsData.logoUrl}
                      className="cbLogo"
                      alt="logo name"
                    />
                  </section>
                </section>
                <section className="row">
                  <section className="col-md-12">
                    <article className="centerAlign">
                      <h1>
                        {this.props.aboutUsData &&
                          this.props.aboutUsData.programName}
                      </h1>
                      <p>
                        {this.props.aboutUsData &&
                          this.props.aboutUsData.headline}
                      </p>
                    </article>
                  </section>
                  <section className="col-md-12">
                    <section className="row">
                      <article className="btngrouppage">
                        <button
                          onClick={() => {
                            if (
                              this.props.scholarshipData &&
                              this.props.scholarshipData.length > 0
                            ) {
                              this.onScrollHandler(this.refs.scholarships);
                            }
                            // if (!isMobile) {
                            //   typeof window !== undefined &&
                            //     window.open("https://admission.buddy4study.com/study-abroad/why-study-in-uk", '_blank', 'location=yes,height=50,width=50,scrollbars=yes,status=yes');
                            // }
                          }}
                        >
                          {this.props.scholarshipData &&
                            this.props.scholarshipData.length > 0
                            ? "Apply Now"
                            : "Application is Closed"}
                        </button>

                        <article className="groupfollowers">
                          <button
                            className={`followBtn ${
                              isAuthenticated && isFollowStatus ? "active" : ""
                              }`}
                            onClick={() => this.brandFollowHandler(userId, orgId)}
                          >
                            <i className="icon" />
                            {isAuthenticated && isFollowStatus
                              ? "Followed"
                              : "Follow"}
                          </button>
                          {totalFollowers > 0 && (
                            <p className="followersLabel">
                              <i>{totalFollowers}</i>
                              <i>Followers</i>
                            </p>
                          )}
                        </article>

                        <article className="whatsapp">
                          <WhatsappShareButton onClick={gblFunc.gaTrack.trackEvent([`https://www.buddy4study.com${this.props.history.location.pathname}`, "Whatsapp Share"])} url={`https://www.buddy4study.com${this.props.history.location.pathname}`} ><i className="fa fa-whatsapp icon" aria-hidden="true" /> <span>Share</span></WhatsappShareButton>
                        </article>
                      </article>
                    </section>

                  </section>
                </section>
              </article>
            </article>
          </section>
          <section
            className={`templateStepsContent containerfluid lightGrayBg ${this.state.stickClass}`}
            ref="sticky"
          >
            <section className="container">
              <section className="row">
                <article className="content">
                  <ul>
                    {this.props.aboutUsData &&
                      this.props.aboutUsData.aboutProgram && (
                        <li
                          onClick={() =>
                            this.onScrollHandler(this.refs.aboutSAT, 1)
                          }
                        >
                          Home
                        </li>
                      )}
                    {this.props.aboutUsData &&
                      this.props.aboutUsData.aboutProgram && (
                        <li
                          onClick={() =>
                            this.onScrollHandler(this.refs.about, 2)
                          }
                        >
                          About Program
                        </li>
                      )}
                    {this.props.scholarshipData &&
                      this.props.scholarshipData.length > 0 && (
                        <li
                          onClick={() =>
                            this.onScrollHandler(this.refs.scholarships, 3)
                          }
                        >
                          Scholarships
                        </li>
                      )}
                    {this.props.scholarsData &&
                      this.props.scholarsData.length > 0 && (
                        <li
                          onClick={() =>
                            this.onScrollHandler(this.refs.scholars, 4)
                          }
                        >
                          {" "}
                          Scholars
                        </li>
                      )}

                    {this.props.faqData && this.props.faqData.length > 0 && (
                      <li
                        onClick={() => this.onScrollHandler(this.refs.faqs, 5)}
                      >
                        FAQs
                      </li>
                    )}

                    <li
                      onClick={() => this.onScrollHandler(this.refs.qna, 7)}
                    >
                      Q&A
                      </li>

                    {this.props.aboutUsData && this.props.aboutUsData.tnc && (
                      <li
                        onClick={() => this.onScrollHandler(this.refs.faqs, 6)}
                      >
                        Terms & Conditions
                      </li>
                    )}
                    <li
                      onClick={() => this.onScrollHandler(this.refs.contact, 7)}
                    >
                      Contact Us
                    </li>
                    <li
                      onClick={event => window.location.href = '/scholarships'}
                    >
                      Back
                    </li>
                  </ul>
                </article>
              </section>
            </section>
          </section>
          {this.props.aboutUsData && this.props.aboutUsData.aboutProgram && (
            <section
              className={`${
                this.state.scrollClass == 2 || this.state.scrollClass == 5
                  ? "scrollClass "
                  : ""
                }container-fluid templateabout`}
              ref="about"
            >
              <section className="container">
                <h3>About the Program</h3>
                <section className="row">
                  <article className="display-flex-audio">
                    {aboutUsData &&
                      aboutUsData.audios &&
                      aboutUsData.audios.length > 0 &&
                      aboutUsData.audios.map((item, index) => {
                        return (
                          <article className="col-md-4 col-xs-12 col-sm-6" key={index}>
                            <article className="audio_img">

                              <p>Play in {item.languageName}</p>
                              <div className="playpauseBtn" id={`play_${index}`} onClick={(e) => {
                                const id = e.target.id.split("_");
                                var sounds = document.getElementsByTagName('audio');
                                const playPaused = document.getElementById(id[1]).paused;
                                for (var i = 0; i < sounds.length; i++) sounds[i].pause();
                                !playPaused ? document.getElementById(id[1]).pause() : document.getElementById(id[1]).play();
                                if (playPaused) {
                                  gblFunc.gaTrack.trackEvent([aboutUsData.programName, `Audio play Start`, `Audio in ${item.languageName}`]);
                                }
                                document.getElementById(id[1]).addEventListener("timeupdate", function () {
                                  if (Math.round(document.getElementById(id[1]).currentTime) == 15 || Math.round(document.getElementById(id[1]).currentTime) == 30 || Math.round(document.getElementById(id[1]).currentTime) == 45) {
                                    setTimeout(() => {
                                      gblFunc.gaTrack.trackEvent([aboutUsData.programName, `Audio play time ${Math.round(document.getElementById(id[1]).currentTime)}`, `Audio in ${item.languageName}`]);
                                    }, 1000);
                                  }
                                });
                              }}>
                              </div>
                              <audio
                                className="playback"
                                controls
                                play="false"
                                preload="metadata"
                                controlsList="download"
                                src={item.url}
                                type="audio/mp4"
                                id={index}
                              />

                            </article>
                          </article>
                        );
                      })}
                  </article>
                </section>
                <section className="row">
                  <section className="col-sm-12 col-md-12">
                    <article
                      dangerouslySetInnerHTML={{
                        __html:
                          this.props.aboutUsData &&
                          this.props.aboutUsData.aboutProgram &&
                          this.props.aboutUsData.aboutProgram
                      }}
                    />
                  </section>
                </section>
              </section>
            </section>
          )}
          {((this.props.aboutUsData &&
            this.props.aboutUsData.videos &&
            this.props.aboutUsData.videos.length > 0) ||
            (this.props.blogsData &&
              this.props.blogsData.data &&
              this.props.blogsData.data.length > 0)) && (
              <section
                className={`${
                  this.state.scrollClass == 1 ? "scrollClass " : ""
                  }container-fluid collegeBoardEvent`}
                ref="aboutSAT"
              >
                {
                  <section className="container">
                    <section className="row">
                      <article className="col-md-12">
                        <article className="tabWrapper">
                          <ul className="tablist">
                            {this.props.aboutUsData &&
                              this.props.aboutUsData.videos &&
                              this.props.aboutUsData.videos.length > 0 && (
                                <li
                                  className={
                                    this.state.activeTab == 2 ? "active" : ""
                                  }
                                  onClick={() => this.changeTab(2)}
                                >
                                  Videos
                                </li>
                              )}
                            {this.props.blogsData &&
                              this.props.blogsData.data &&
                              this.props.blogsData.data.length > 0 && (
                                <li
                                  className={
                                    this.state.activeTab == 3 ? "active" : ""
                                  }
                                  onClick={() => this.changeTab(3)}
                                >
                                  Article
                                </li>
                              )}
                          </ul>
                          <article className="tabContent">
                            {this.state.activeTab == 2 &&
                              this.props.aboutUsData &&
                              this.props.aboutUsData.videos &&
                              this.props.aboutUsData.videos.length > 0 && (
                                <article className="AboutSATExam">
                                  <article className="speech">
                                    <Responsive
                                      list={this.props.aboutUsData.videos}
                                    />
                                  </article>
                                </article>
                              )}
                            {this.state.activeTab == 3 ? (
                              <article className="OfficialSATPractice">
                                <article className="row">
                                  <article className="col-md-12">
                                    <article className="scrollbar scrollbarColor">
                                      {this.props.blogsData &&
                                        this.props.blogsData.data &&
                                        this.props.blogsData.data.length > 0 &&
                                        this.props.blogsData.data.map(blog => (
                                          <p>
                                            <a
                                              href={`https://www.buddy4study.com/article/${blog.slug}`}
                                              target="_blank"
                                              rel="noopener nofollow"
                                            >
                                              <span>{blog.title}</span>
                                            </a>
                                          </p>
                                        ))}
                                    </article>
                                  </article>
                                </article>
                              </article>
                            ) : (
                                ""
                              )}
                          </article>
                        </article>
                      </article>
                    </section>
                  </section>
                }
              </section>
            )}

          {/* <section
            className={`${
              this.state.scrollClass == 3 ? "scrollClass " : ""
            }scholarsRow  container-fluid`}
            ref="scholarships"
          >
            <section className="container">
              <section className="row">
                <h3>Scholarships</h3>
                <section className="borderbox">
                  <article className="sholarship-list sholarship-list-scroll">
                    {this.props.scholarshipData &&
                      this.props.scholarshipData.length > 0 &&
                      this.props.scholarshipData.map(d =>
                        this.renderScholarships(d)
                      )}
                  </article>
                </section>
              </section>
            </section>
          </section> */}
          {this.props.scholarshipData && this.props.scholarshipData.length > 0 && (
            <section
              className={`${
                this.state.scrollClass == 3 ? "scrollClass " : ""
                }container-fluid templatescholarship`}
              ref="scholarships"
            >
              <section className="container">
                <h3>
                  {this.props.scholarshipData &&
                    this.props.scholarshipData.length == 1
                    ? "Scholarship"
                    : "Scholarships"}
                </h3>
                <section className="row">
                  {this.props.scholarshipData &&
                    this.props.scholarshipData.length == 1 &&
                    (this.props.scholarshipData &&
                      this.props.scholarshipData.length > 0 &&
                      this.props.scholarshipData.map((scholership, index) => {
                        return (
                          <section className="col-md-12 col-sm-12">
                            <ScholarshipListTable
                              key={index}
                              title={scholership.title}
                              deadline={scholership.deadline}
                              eligibility={scholership.eligibility}
                              purposeAward={scholership.purposeAward}
                              applyLink={
                                scholership.applyLink &&
                                  scholership.applyLink != ""
                                  ? true
                                  : false
                              }
                              onApplyHandler={() => this.onApplyHandler(scholership)}
                            />
                          </section>
                        );
                      }))}
                  {this.props.scholarshipData &&
                    this.props.scholarshipData.length == 2 &&
                    (this.props.scholarshipData &&
                      this.props.scholarshipData.length > 0 &&
                      this.props.scholarshipData.map((scholership, index) => {
                        return (
                          <section className="col-md-6 col-sm-12">
                            <ScholarshipListTable
                              key={index}
                              title={scholership.title}
                              deadline={scholership.deadline}
                              eligibility={scholership.eligibility}
                              purposeAward={scholership.purposeAward}
                              applyLink={
                                scholership.applyLink &&
                                  scholership.applyLink != ""
                                  ? true
                                  : false
                              }
                              onApplyHandler={() => this.onApplyHandler(scholership)}
                            />
                          </section>
                        );
                      }))}
                  {this.props.scholarshipData &&
                    this.props.scholarshipData.length >= 3 &&
                    (this.props.scholarshipData &&
                      this.props.scholarshipData.length > 0 &&
                      this.props.scholarshipData.map((scholership, index) => {
                        return (
                          <section className="col-md-4 col-sm-12">
                            <ScholarshipListTable
                              key={index}
                              title={scholership.title}
                              deadline={scholership.deadline}
                              eligibility={scholership.eligibility}
                              purposeAward={scholership.purposeAward}
                              applyLink={
                                scholership.applyLink &&
                                  scholership.applyLink != ""
                                  ? true
                                  : false
                              }
                              onApplyHandler={() => this.onApplyHandler(scholership)}
                            />
                          </section>
                        );
                      }))}
                </section>
              </section>
            </section>
          )}
          {this.props.scholarsData && this.props.scholarsData.length > 0 && (
            <section
              className={`${
                this.state.scrollClass == 4 ? "scrollClass " : ""
                }templateScholar container-fluid`}
              ref="scholars"
            >
              <section className="container">
                <section className="row">
                  <h3>Scholars</h3>
                  <section className="borderbox">
                    <section className="awardeesSlider awardeesSlider-scroll">
                      {this.props.scholarsData &&
                        this.props.scholarsData.length > 0 &&
                        this.props.scholarsData.map((d, index) =>
                          this.renderScholers(d, index)
                        )}
                    </section>
                  </section>
                </section>
              </section>
            </section>
          )}

          {this.props.faqData && this.props.faqData.length > 0 && (
            <section
              className={`${
                this.state.scrollClass == 5 ? "scrollClass " : ""
                }templateFAQs container-fluid`}
              ref="faqs"
            >
              <section className="container">
                <section className="row">
                  <article className="col-md-12">
                    <h3>Frequently Asked Questions</h3>
                    <article className="content">
                      <FrequestAskedQuestion
                        frequestAskedQuesHandler={this.frequestAskedQuesHandler}
                        showItem={this.state.showItem}
                        faq={this.props}
                      />
                    </article>
                  </article>
                </section>
              </section>
            </section>
          )}

          <section
            className="templateqna container-fluid"
            ref="qna"
          >
            <section className="container">
              <section className="row">
                <article className="col-md-12">
                  <h3>Ask your question and our scholarship support will get back to you?</h3>
                  <article className="content">
                    <article className="cateWrapper answer">
                      <article className="imgWrapper">
                        {!!quesAnswer1List ?
                          quesAnswer1List.map(catg => (
                            <article key={catg.id} className="imgWrapper">

                              <div className="left">

                                <h5>
                                  {" "}
                                  <Link
                                    to={{
                                      pathname: `/qna/${catg.slug}`
                                    }}
                                  >
                                    {" "}
                                    {catg.questionText}
                                  </Link>
                                </h5>
                                <p>Asked by <span> {catg.authorName}</span> on <span>{catg && catg.createdAt && catg.createdAt.substr(0, catg.createdAt.indexOf(' '))}</span></p>
                              </div>
                              <article className="ansWrapper">
                                <article className="comments"> <span><i>{!!catg.answers ? catg.answers.data.length : 0}</i>Answer</span></article>
                                <ul>
                                  {!!catg.answers.data && catg.answers.data.length > -1 ?

                                    catg.answers.data.map(item1 => (
                                      <li>
                                        <div className="left noborder">
                                          {!!item1.authorPic ?
                                            <img src={item1.authorPic} />
                                            :
                                            <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/profile-img.png" />


                                          }
                                          <h6><span>{item1.authorName}</span> on <span>{item1 && item1.createdAt && item1.createdAt.substr(0, item1.createdAt.indexOf(' '))}</span></h6>
                                          <p
                                            dangerouslySetInnerHTML={{
                                              __html: item1.answerText
                                            }} />
                                          <Link
                                            to={{
                                              pathname: `/qna/${catg.slug}`
                                            }}
                                          >
                                            {" "}
                                            Continue reading
                                </Link>
                                          {/* <a target="_blank" className="viewanswer" href="">Continue reading</a> */}
                                        </div>
                                      </li>

                                    ))
                                    :
                                    <p></p>
                                  }

                                </ul>


                              </article>
                            </article>


                          ))
                          : ""

                        }


                        {/* {console.log(quesAnswer1List)}
                        {console.log(!!quesAnswer1List)}
                        {!!quesAnswer1List ?
                        (
                          quesAnswer1List.map((item,index)=>(
                          
                              console.log("truesna bfnsaf",item)
                              <div>
                              <h5>{item}</h5>

                              </div>



                            
                           <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/profile-img.png" />
                           <h5>What is the selection criteria of FAL Career Foundation Scholarship?</h5> 
                           <p>Asked by <span> User_Name</span> on <span>Dec 11,2019</span></p>

                          ))
                          

                        ):(
                          <div></div>

                        )} */}


                        {/* <article className="ansWrapper">
                          <article className="comments"> <span><i>(10)</i>Answer</span></article>
                          <ul>
                            <li>
                              <div className="left noborder">
                                <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/profile-img.png" />
                                <h6><span> Monika Sharma</span> on <span>2days ago</span></h6>
                                <p>What is the selection criteria of FAL Career Foundation Scholarship?
                                  What is the selection criteria of FAL Career Foundation Scholarship?What
                                  is the selection criteria of FAL Career Foundation
                                  on criteria of FAL Career Foundation Scholarship?What
                                  is the selection criteria of FAL Career Foundation
                                  on criteria of FAL Career Foundation Scholarship?What
                                  is the selection criteria of FAL Career Foundation
                                </p>
                                <a target="_blank" className="viewanswer" href="">Continue reading</a>
                              </div>
                            </li>
                            <li>
                              <div className="left noborder">
                                <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/profile-img.png" />
                                <h6><span> Monika Sharma</span> on <span>2days ago</span></h6>
                                <p>What is the selection criteria of FAL Career Foundation Scholarship?
                                  What is the selection criteria of FAL Career Foundation Scholarship?What
                                  is the selection criteria of FAL Career Foundation
                                </p>
                                <a target="_blank" className="viewanswer" href="">Continue reading</a>
                              </div>
                            </li>
                            <li>
                              <div className="left noborder">
                                <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/profile-img.png" />
                                <h6><span> Monika Sharma</span> on <span>2days ago</span></h6>
                                <p>What is the selection criteria of FAL Career Foundation Scholarship?
                                  What is the selection criteria of FAL Career Foundation Scholarship?What
                                  is the selection criteria of FAL Career Foundation
                                </p>
                                <a target="_blank" className="viewanswer" href="">Continue reading</a>
                              </div>
                            </li>
                          </ul>
                        </article> */}
                      </article>
                    </article>
                  </article>
                </article>
                <article className="col-md-12 text-center">
                  {/* <a className="btn qnabtn" href="/qna">View All Question & Answer</a> */}
                  <Link
                    className="btn qnabtn"
                    to={{
                      pathname: aboutUsData.qnaCategorySlug ? `/qna/categories/${aboutUsData.qnaCategorySlug}` : '/qna'
                    }}
                  >
                    {" "}
                                            View All Question & Answer
                                </Link>
                  &nbsp;&nbsp;
                  <button type="submit" className="btn qnabtnyellow"
                    //   onClick={() => {
                    //     const isAuth =
                    //       gblFunc.isUserAuthenticated() ||
                    //       this.props.isAuthenticated;
                    //     if (isAuth) {
                    //       this.setState({ showQuestionPopUp: true }
                    //         // this.props.loadCatgList({ page: 0, length: 100 })
                    //       );
                    //     } else {
                    //       this.setState({
                    //         showLoginPopup: true
                    //       });
                    //     }
                    //   }
                    // }
                    onClick={() => {
                      this.setState({
                        showQuestionPopUp: true
                      })
                    }}
                  >
                    <span>Ask Your Question</span>
                  </button>
                </article>
                <article className="col-md-12 text-center">
                  <section className="search-wraper">
                    <section className="search-ctrl">
                      {/* <input type="text" autocomplete="off" placeholder="Ask your question" value="" /> */}

                    </section>
                  </section>
                </article>
              </section>
            </section>
          </section>


          {this.props.aboutUsData && this.props.aboutUsData.tnc && (
            <section
              className={`${
                this.state.scrollClass == 6 ? "scrollClass " : ""
                }templatetc container-fluid`}
              ref="contact"
            >
              <section className="container">
                <h3>Terms &amp; Conditions</h3>
                <section className="row">
                  <section className="col-md-12 col-sm-12">
                    <p
                      dangerouslySetInnerHTML={{
                        __html: this.props.aboutUsData.tnc
                      }}
                    />
                  </section>
                </section>
              </section>
            </section>
          )}
          <section
            className={`${
              this.state.scrollClass == 7 ? "scrollClass " : ""
              }templateContact container-fluid`}
            ref="contact"
          >
            <section className="container">
              <section className="row">
                <article className="col-md-12">
                  <h3>Contact Us</h3>

                  <p>In case of queries, please reach out to us:</p>
                  <ul className="supportList">
                    {this.props.aboutUsData && this.props.aboutUsData.phone && (
                      <li>
                        <dd>{this.props.aboutUsData.phone} </dd>
                        <span>(Monday to Friday &ndash; 10:00AM to 6PM)</span>
                      </li>
                    )}
                    <li>
                      <a
                        href={`mailto:${this.props.aboutUsData &&
                          this.props.aboutUsData.email}`}
                      >
                        {this.props.aboutUsData && this.props.aboutUsData.email}
                      </a>
                    </li>
                  </ul>
                </article>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

const FrequestAskedQuestion = props => {
  const { faq } = props;
  const frequenstAskedQuestion =
    faq.faqData && faq.faqData.length > 0 ? faq.faqData : [];

  return (
    <ul>
      {frequenstAskedQuestion &&
        frequenstAskedQuestion.length > 0 &&
        frequenstAskedQuestion.map((list, index) => (
          <li
            key={index}
            onClick={() => props.frequestAskedQuesHandler(index)}
            className={props.showItem == index ? "active" : ""}
          >
            <h5>{list.question}</h5>
            <p
              dangerouslySetInnerHTML={{
                __html: list.answer
              }}
            />
          </li>
        ))}
    </ul>
  );
};

const ScholarshipListTable = props => {
  const getDeadlineFormat = deadline => {
    const d = new Date(deadline);
    return d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
  };
  return (
    <article className="detailsbox">
      <h4>{props.title}</h4>
      <article className="deadlineBox">
        <dd>
          <span>Deadline: </span>
          {props.deadline ? getDeadlineFormat(props.deadline) : ""}
        </dd>
      </article>
      <article className="amount amount-scroll">
        <h5>Eligibility</h5>

        <artilce
          dangerouslySetInnerHTML={{
            __html: props.eligibility
          }}
        />

        <h5>Scholarship Amount </h5>
        <ul className="award">
          <li>{props.purposeAward}</li>
        </ul>
      </article>
      {props.applyLink && (
        <button onClick={props.onApplyHandler}>Apply Now </button>
      )}
    </article>
  );
};

export default Colgate;
