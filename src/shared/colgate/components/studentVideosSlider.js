import React, { Component } from "react";
import Slider from "react-slick";
import { imgBaseUrl } from "../../../constants/constants";
import YoutubeVideo from "./youtubeVideo";

export default class Responsive extends Component {
  constructor(props) {
    super(props);
    this.renderVedio = this.renderVedio.bind(this);
  }
  renderVedio(d, index) {
    return (
      <article className="col-md-4" key={index}>
        <article className="cellWrapper">
          <article className="contentBody">
            <YoutubeVideo videoId={d.url} />
          </article>
          <article className="contentFooter">
            <p>{d.description}</p>
          </article>
        </article>
      </article>
    );
  }
  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 853,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            initialSlide: 2,
            dots: true
          }
        },
        {
          breakpoint: 603,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 1,
            dots: true
          }
        }
      ]
    };
    if (this.props.list && this.props.list.length > 2) {
      return (
        <Slider {...settings}>
          {this.props.list.map((d, index) => this.renderVedio(d, index))}
        </Slider>
      );
    } else {
      return (
        <article className="row">
          {this.props.list.map((d, index) => this.renderVedio(d, index))}
        </article>
      );
    }
  }
}
