import React from "react";
import {
  required,
  isEmail,
  minLength,
  isNumeric,
  isMobileNumber,
  lengthRange
} from "../../../validation/rules";
import { ruleRunner } from "../../../validation/ruleRunner";
import AlertMessage from "../../common/components/alertMsg";
import Loader from "../../common/components/loader";
import { SUBMIT_INTEREST_FORM_SUCCEEDED } from "../actions";
import { Offer } from "../../../constants/constants";
export default class InterestForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      companyName: "",
      designation: "",
      email: "",
      mobile: "",
      message: "",
      product: Offer[this.props.product],
      isShowAlert: false,
      msg: "Submit Successfully",
      validations: {
        email: null,
        mobile: null,
        name: null,
        designation: null,
        companyName: null
      }
    };
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.closeAlert = this.closeAlert.bind(this);
  }

  onFormFieldChange(event) {
    const { id, value } = event.target;
    let validations = { ...this.state.validations };
    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      const validationResult = ruleRunner(
        id,
        value,
        name,
        ...validationFunctions
      );
      validations[id] = validationResult[id];
    }
    this.setState({
      [id]: value,
      validations
    });
  }

  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "name":
        validationObject.name = "* Name";
        validationObject.validationFunctions = [required, lengthRange(3, 60)];
        return validationObject;
      case "designation":
        validationObject.name = "* Designation";
        validationObject.validationFunctions = [required, lengthRange(3, 60)];
        return validationObject;
      case "companyName":
        validationObject.name = "* Company name";
        validationObject.validationFunctions = [required, lengthRange(3, 60)];
        return validationObject;
      case "email":
        validationObject.name = "* Email ";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;
      case "mobile":
        validationObject.name = "* Mobile number ";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;
      case "subject":
        validationObject.name = "* Subject ";
        validationObject.validationFunctions = [required, lengthRange(6, 60)];
        return validationObject;
    }
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      if (validations.hasOwnProperty(key)) {
        let { name, validationFunctions } = this.getValidationRulesObject(key);
        let validationResult = ruleRunner(
          this.state[key],
          key,
          name,
          ...validationFunctions
        );
        validations[key] = validationResult[key];
        if (validationResult[key] !== null) {
          isFormValid = false;
        }
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }
  onSubmitHandler(event) {
    event.preventDefault();
    const {
      message,
      email,
      mobile,
      name,
      designation,
      companyName,
      product
    } = this.state;
    if (this.checkFormValidations()) {
      this.props.submitInterestForm({
        message,
        email,
        mobile,
        name,
        product,
        designation,
        companyName
      });
    }
  }
  closeAlert() {
    this.setState({ isShowAlert: false });
  }
  componentWillReceiveProps(nextProps) {
    const {
      message,
      email,
      mobile,
      name,
      designation,
      companyName
    } = this.state;
    const { type } = nextProps;
    switch (type) {
      case SUBMIT_INTEREST_FORM_SUCCEEDED:
        this.setState({
          isShowAlert: true,
          message: "",
          email: "",
          mobile: "",
          name: "",
          designation: "",
          companyName: ""
        });
        break;

      default:
        break;
    }
  }

  render() {
    const {
      name,
      companyName,
      designation,
      email,
      mobile,
      message,
      msg,
      isShowAlert
    } = this.state;
    return (
      <article className="formWrapper">
        <AlertMessage
          isShow={isShowAlert}
          msg={msg}
          status={true}
          close={this.closeAlert}
        />
        <Loader isLoader={this.props.showLoader} />
        <h4>If you are interested, please fill the fields</h4>
        <article className="formControl">
          <form autoCapitalize="off">
            <article className="form-group">
              <input
                type="text"
                className="form-control"
                id="name"
                name="name"
                placeholder="Enter full name"
                value={name}
                onChange={this.onFormFieldChange}
                required
              />
              {this.state.validations.name ? (
                <span className="error animated bounce">
                  {this.state.validations.name}
                </span>
              ) : null}
            </article>
            <article className="form-group">
              <input
                type="text"
                className="form-control"
                id="companyName"
                name="companyName"
                placeholder="Enter company name"
                value={companyName}
                onChange={this.onFormFieldChange}
                required
              />
              {this.state.validations.companyName ? (
                <span className="error animated bounce">
                  {this.state.validations.companyName}
                </span>
              ) : null}
            </article>
            <article className="form-group">
              <input
                type="text"
                className="form-control"
                id="designation"
                name="designation"
                placeholder="Enter designation"
                value={designation}
                onChange={this.onFormFieldChange}
                required
              />
              {this.state.validations.designation ? (
                <span className="error animated bounce">
                  {this.state.validations.designation}
                </span>
              ) : null}
            </article>
            <article className="form-group">
              <input
                type="text"
                className="form-control"
                id="email"
                name="email"
                placeholder="Enter email ID"
                value={email}
                onChange={this.onFormFieldChange}
                required
              />
              {this.state.validations.email ? (
                <span className="error animated bounce">
                  {this.state.validations.email}
                </span>
              ) : null}
            </article>
            <article className="form-group">
              <input
                type="text"
                className="form-control"
                id="mobile"
                name="mobile"
                placeholder="Enter mobile"
                value={mobile}
                onChange={this.onFormFieldChange}
                maxLength="10"
                required
              />
              {this.state.validations.mobile ? (
                <span className="error animated bounce">
                  {this.state.validations.mobile}
                </span>
              ) : null}
            </article>
            <article className="form-group">
              <textarea
                placeholder="Comment"
                id="message"
                name="message"
                className="form-control"
                value={message}
                onChange={this.onFormFieldChange}
              />
            </article>
            <button type="submit" onClick={this.onSubmitHandler}>
              Submit
            </button>
          </form>
        </article>
      </article>
    );
  }
}
