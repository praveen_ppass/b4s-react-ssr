import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  SUBMIT_INTEREST_FORM_REQUESTED,
  SUBMIT_INTEREST_FORM_SUCCEEDED,
  SUBMIT_INTEREST_FORM_FAILED
} from "./actions";

const interestFormApi = ({ inputData }) => {
  return fetchClient.post(apiUrl.interestForm, inputData).then(res => {
    return res.data;
  });
};

function* submitInterestForm(input) {
  try {
    const interest_form = yield call(interestFormApi, input.payload);
    yield put({
      type: SUBMIT_INTEREST_FORM_SUCCEEDED,
      payload: interest_form
    });
  } catch (error) {
    yield put({
      type: SUBMIT_INTEREST_FORM_FAILED,
      payload: error.errorMessage
    });
  }
}

export default function* interestFormSaga() {
  yield takeEvery(SUBMIT_INTEREST_FORM_REQUESTED, submitInterestForm);
}
