import React, { Component } from "react";
import InterestForm from "../components/interestForm";

import { submitInterestForm as submitInterestFormAction } from "../actions";
import { connect } from "react-redux";

const mapStateToProps = ({ interestForm }) => ({
  showLoader: interestForm.showLoader,
  type: interestForm.type,
  isError: interestForm.isError,
  errorMessage: interestForm.errorMessage
});

const mapDispatchToProps = dispatch => ({
  submitInterestForm: interestFormData =>
    dispatch(submitInterestFormAction(interestFormData))
});

export default connect(mapStateToProps, mapDispatchToProps)(InterestForm);
