import {
  SUBMIT_INTEREST_FORM_REQUESTED,
  SUBMIT_INTEREST_FORM_SUCCEEDED,
  SUBMIT_INTEREST_FORM_FAILED
} from "./actions";

const initialState = {
  showLoader: false,
  isError: false,
  errorMessage: "",
  successMsg: ""
};

const interestFormReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SUBMIT_INTEREST_FORM_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type,
        errorMessage: ""
      };

    case SUBMIT_INTEREST_FORM_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type: "SUBMIT_INTEREST_FORM_SUCCEEDED",
        successMsg: payload
      };

    case SUBMIT_INTEREST_FORM_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        type: "SUBMIT_INTEREST_FORM_FAILED",
        errorMessage: payload
      };
    default:
      return state;
  }
};
export default interestFormReducer;
