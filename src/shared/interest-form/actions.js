export const SUBMIT_INTEREST_FORM_REQUESTED = "SUBMIT_INTEREST_FORM_REQUESTED";
export const SUBMIT_INTEREST_FORM_SUCCEEDED = "SUBMIT_INTEREST_FORM_SUCCEEDED";
export const SUBMIT_INTEREST_FORM_FAILED = "SUBMIT_INTEREST_FORM_FAILED";

export const submitInterestForm = data => ({
  type: SUBMIT_INTEREST_FORM_REQUESTED,
  payload: { inputData: data }
});
