export const FETCH_TEAM_REQUESTED = "FETCH_TEAM_REQUESTED";
export const FETCH_TEAM_SUCCEEDED = "FETCH_TEAM_SUCCEEDED";
export const FETCH_TEAM_FAILED = "FETCH_TEAM_FAILED";

export const fetchTeam = data => ({
  type: FETCH_TEAM_REQUESTED,
  payload: { data: data }
});

export const receivesTeam = payload => ({
  type: FETCH_TEAM_SUCCEEDED,
  payload
});
