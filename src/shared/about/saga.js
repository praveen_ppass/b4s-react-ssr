import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_TEAM_REQUESTED,
  FETCH_TEAM_SUCCEEDED,
  FETCH_TEAM_FAILED
} from "./actions";

const fetchUrl = () =>
  fetchClient.get(apiUrl.team).then(res => {
    return res.data;
  });

function* fetchTeam(input) {
  try {
    const team = yield call(fetchUrl);
    yield put({
      type: FETCH_TEAM_SUCCEEDED,
      payload: team
    });
  } catch (error) {
    yield put({
      type: FETCH_TEAM_FAILED,
      payload: error.errorMessage
    });
  }
}

export default function* fetchAboutSaga() {
  yield takeEvery(FETCH_TEAM_REQUESTED, fetchTeam);
}
