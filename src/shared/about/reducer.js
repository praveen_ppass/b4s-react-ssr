import {
  FETCH_TEAM_SUCCEEDED,
  FETCH_TEAM_REQUESTED,
  FETCH_TEAM_FAILED
} from "./actions";

const initialState = {
  showLoader: true,
  team: [],
  isError: false,
  errorMessage: ""
};

const teamReducer = (previousState = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_TEAM_REQUESTED:
      return Object.assign({}, previousState, {
        type: type,
        showLoader: true,
        isError: false,
        errorMessage: ""
      });

    case FETCH_TEAM_SUCCEEDED:
      return Object.assign({}, previousState, {
        team: payload,
        type: type,
        showLoader: false,
        isError: false,
        errorMessage: ""
      });

    case FETCH_TEAM_FAILED:
      return { showLoader: false, isError: true, errorMessage: payload };

    default:
      return previousState;
  }
};
export default teamReducer;
