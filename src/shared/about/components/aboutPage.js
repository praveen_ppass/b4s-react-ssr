import React, { Component } from "react";
import ServerError from "../../common/components/serverError";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import { Helmet } from "react-helmet";
import TeamSlider from "./slider.js";
import OurPartnerSlider from "./slider-our-partners";
import Loader from "../../common/components/loader";
import { imgBaseUrl } from "../../../constants/constants";

class AboutUs extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let Data = this.props.loadTeam();
  }

  render() {
    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <section className="about">
          {/* breacrum trail */}

          <BreadCrum
            classes={breadCrumObj["about"]["bgImage"]}
            listOfBreadCrum={breadCrumObj["about"]["breadCrum"]}
            title={breadCrumObj["about"]["title"]}
            subTitle={breadCrumObj["about"]["subTitle"]}
          />

          {/* section About Buddy4Study */}
          <article className="container sectionAbout">
            <article className="row">
              <article className="col-md-6 col-sm-12 col-xs-12">
                <h2>About Buddy4Study</h2>
                <p>
                  A large segment of India’s potential workforce is
                  unemployable. With 76.04% literacy rate and an increasing
                  number of dropouts failing to enrol or complete any form of
                  higher education, the situation is worrying. Financial
                  constraints, lack of know-how about education funding schemes
                  are some of the some of the key contributors to this effect.
                </p>
                <p>
                  Buddy4Study, since 2011, is endeavouring to bridge the gap
                  between scholarship providers and scholarship seekers. As
                  India’s largest scholarship listing portal, we help more than
                  1 million students by connecting the right scholarships with
                  the right students. Backed by its robust scholarship search
                  engine, it is the only platform in the country that allows
                  both seekers and scholarship providers to access curated
                  scholarship information across the globe.
                </p>
              </article>
              <article className="col-md-6 col-sm-12 col-xs-12">
                <img
                  src={`${imgBaseUrl}about-learning.jpg`}
                  className="img-responsive"
                  alt="scholarship portal"
                />
              </article>
            </article>
            <article className="row aboutsec equial-padding">
              <article className="col-md-6 col-sm-6 col-xs-12">
                <article className="col-md-12">
                  <article className="col-md-6 smimg col-xs-12">
                    <img
                      src={`${imgBaseUrl}we.jpg`}
                      className="img-responsive"
                      alt="online scholarship"
                    />
                  </article>
                  <article className="col-md-6 smcontent col-xs-12">
                    <h3>We are</h3>
                    <p>
                      India’s largest scholarship network with the vision to
                      make quality education accessible for all. A brainchild of
                      IIT, IIM, and BITS Pilani alumni, Buddy4Study aggregates
                      global scholarship information.
                    </p>
                  </article>
                </article>
              </article>
              <article className="col-md-6 col-sm-6 col-xs-12">
                <article className="col-md-12">
                  <article className="col-md-6 smimg col-xs-12">
                    <img
                      src={`${imgBaseUrl}our-mission.jpg`}
                      className="img-responsive"
                      alt="scholarship programs"
                    />
                  </article>
                  <article className="col-md-6 smcontent col-xs-12">
                    <h3>We do</h3>
                    <p>
                      We provide easy access to scholarships with end-to-end
                      application support to seekers and end-to-end management
                      and monitoring of scholarship programs to providers
                    </p>
                  </article>
                </article>
              </article>
            </article>
          </article>
          <section className="container-fluid about-b4s-bg common-align equial-padding">
            <section className="container">
              <h4> Our Genesis</h4>
              <section className="row margin-top">
                <article className="col-sm-6 col-xs-12 col-md-6 hwdi cell-padding">
                  <article className="col-margin-about">
                    <article className="details">
                      <h5 className="vi">Vision</h5>
                      <p>To ensure affordable quality education for all</p>
                    </article>
                  </article>
                  <article className="col-margin-about">
                    <article className="details">
                      <h5 className="mi">Mission</h5>
                      <p>
                        To simplify scholarship accessibility for students and
                        disbursement process for providers
                      </p>
                    </article>
                  </article>
                  <article className="col-margin-about">
                    <article className="details">
                      <h5 className="va">Values</h5>
                      <p>Honesty | Integrity | Transparency</p>
                    </article>
                  </article>
                </article>
                <article className="col-sm-6 col-xs-12 col-md-6 hwdi cell-padding hidden-xs hidden-sm">
                  <article className="pos-relative float-right imgContainer">
                    <img
                      src={`${imgBaseUrl}call-us.png`}
                      className="img-responsive"
                      alt="online scholarship application"
                    />
                  </article>
                </article>
              </section>
            </section>
          </section>
          <section className="container-fluid equial-padding companyHist">
            <section className="container">
              <h4>Company History</h4>
              <section className="historyPoll">
                <ul>
                  <li>
                    <i className="hisYear">2019</i>
                  </li>

                  <li>
                    <article className="captionLeft">
                      <h5>
                        Recognised for 'Creating Social Impact through Access to
                        Education'- World CSR Congress: CSR Leadership Awards
                        2019
                      </h5>
                    </article>
                    <i className="stripLeft" />
                    <i className="hisMonth">March</i>
                  </li>

                  <li>
                    <article className="captionLeft">
                      <h5>
                        Best CSR Impact Award 2019- UBS Forums Corporate Social
                        Responsibility Awards
                      </h5>
                    </article>
                    <i className="stripLeft" />
                    <i className="hisMonth">February</i>
                  </li>
                  <li>
                    <i className="hisYear">2017</i>
                  </li>

                  <li>
                    <article className="captionLeft">
                      <h5>Innovators Race Top 50 2017 finalists</h5>
                    </article>
                    <i className="striptRight" />
                    <i className="hisMonth">June</i>
                  </li>

                  <li>
                    <article className="captionLeft">
                      <h5>Winner of Best CSR Project Award by IndiaCSR</h5>
                    </article>
                    <i className="striptRight" />
                    <i className="hisMonth">March</i>
                  </li>

                  <li>
                    <article className="captionLeft">
                      <h5>
                        Awards of Appreciation by IndiaCSR presented in National
                        Summit on Best Practices in CSR
                      </h5>
                    </article>
                    <i className="striptRight" />
                    <i className="hisMonth">March</i>
                  </li>

                  <li>
                    <i className="hisYear">2016</i>
                  </li>
                  <li>
                    <article className="captionRight">
                      <h5>
                        Incubated at IIM Calcutta through a rigorous selection
                        out of 900 applicants
                      </h5>
                    </article>
                    <i className="stripLeft" />
                    <i className="hisMonth">March</i>
                  </li>

                  <li>
                    <i className="hisYear">2015</i>
                  </li>
                  <li>
                    <article className="captionLeft">
                      <h5>
                        Chosen among top 50 Most Innovative IT Product Companies
                        of 2015 as per NASSCOM Emerge 50 Awards
                      </h5>
                    </article>
                    <i className="striptRight" />
                    <i className="hisMonth">October</i>
                  </li>

                  <li>
                    <article className="captionLeft">
                      <h5>
                        Recognized by Indian Institute of Management, Rohtak at
                        2nd Internet Start-up Summit – October
                      </h5>
                    </article>
                    <i className="striptRight" />
                    <i className="hisMonth">October</i>
                  </li>

                  <li>
                    <article className="captionLeft">
                      <h5>
                        Top 10 Social Enterprise of 2015 among India’s 100
                        leading Social Entrepreneurs as per Action for India
                        Forum
                      </h5>
                    </article>
                    <i className="striptRight" />
                    <i className="hisMonth">February</i>
                  </li>

                  <li>
                    <i className="hisYear">2014</i>
                  </li>

                  <li>
                    <article className="captionRight">
                      <h5>
                        Winner of South Asia & Asia Pacific Manthan Award in
                        education category among 69 teams from 39 countries
                      </h5>
                    </article>
                    <i className="stripLeft" />
                    <i className="hisMonth">December</i>
                  </li>
                </ul>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default AboutUs;
