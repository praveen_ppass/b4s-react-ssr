import React, { Component } from "react";

import Slider from "react-slick";
class TeamSlider extends Component {
  constructor() {
    super();
  }

  getTeamSlider() {
    if (this.props.sliderData != null) {
      //checking ...

      // start if
      let teamList = this.props.sliderData;
      return teamList
        .sort((a, b) => {
          return a.WEIGHT - b.WEIGHT;
        })
        .map((item, index) => {
          return (
            <div className="box1">
              <img src={item.pic} alt="team-member" />
              <span className="titleTxt">
                {item.name}
                <i>{item.designations[0] && item.designations[0].name}</i>
              </span>
              <span className="border" />
              <p
                dangerouslySetInnerHTML={{
                  __html: item.aboutMe
                }}
              />
              <span className="border" />
            </div>
          );
        });
    }
  }

  componentDidMount() {}

  render() {
    this.getTeamSlider();
    const settings = {
      className: "center",
      centerMode: true,
      infinite: true,
      autoplay: false,
      centerPadding: "60px",
      slidesToShow: 3,
      speed: 500,
      speed: 2000,
      autoplaySpeed: 4000,

      responsive: [
        {
          breakpoint: 1024,
          settings: {
            className: "center",
            centerMode: true,
            infinite: true,
            autoplay: false,
            centerPadding: "60px",
            slidesToShow: 3,
            speed: 500,
            speed: 2000,
            autoplaySpeed: 4000
          }
        },
        /* {
          breakpoint: 1023,
          settings: {
            className: "center",
            centerMode: true,
            infinite: true,
            autoplay: false,
            centerPadding: "0px",
            slidesToShow: 1,
            speed: 500,
            speed: 2000,
            autoplaySpeed: 4000
          }
        }, */
        {
          breakpoint: 480,
          settings: {
            className: "center",
            centerMode: true,
            infinite: true,
            autoplay: false,
            centerPadding: "0px",
            slidesToShow: 1,
            speed: 500,
            speed: 2000,
            autoplaySpeed: 4000
          }
        }
      ]
    };
    return (
      <section className="container-fluid team common-align equial-padding">
        <section className="container ">
          <article className="row">
            <article className="col-md-12">
              <h4>Team</h4>
              <Slider {...settings}>{this.getTeamSlider()}</Slider>
            </article>
          </article>
        </section>
      </section>
    );
  }
}
export default TeamSlider;
