import React, { Component } from "react";
import { imgBaseUrl, imgBaseUrlDev } from "../../../constants/constants";

import Slider from "react-slick";
class OurPartnerSlider extends Component {
  render() {
    const settings = {
      className: "center",
      centerMode: true,
      infinite: true,
      autoplay: true,
      centerPadding: "0px",
      slidesToShow: 3,
      speed: 2000,
      autoplaySpeed: 4000,

      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            initialSlide: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            centerMode: true,
            infinite: true,
            autoplay: true,
            centerPadding: "0px",
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    return (
      <section className="container-fluid partner common-align equial-padding">
        <section className="container">
          <article className="row">
            <article className="col-md-12">
              <h4>Our Partners and Associations</h4>
              <Slider {...settings}>
                <a href="javascript:void(0);" className="thumbnail">
                  <img
                    src={`${imgBaseUrl}colgatelogoabout.png`}
                    alt="Colgate"
                    title="Colgate"
                  />
                </a>
                <a href="javascript:void(0);" className="thumbnail">
                  <img
                    src={`${imgBaseUrl}DLFFoundationabout.png`}
                    alt="DLF Foundation"
                    title="DLF Foundation"
                  />
                </a>
                <a href="javascript:void(0);" className="thumbnail">
                  <img
                    src={`${imgBaseUrl}hplogoabout.png`}
                    alt="HP"
                    title="HP"
                  />
                </a>
                <a href="javascript:void(0);" className="thumbnail">
                  <img
                    src={`${imgBaseUrl}loreallogoabout.png`}
                    alt="Loreal"
                    title="L'Oreal"
                  />
                </a>
                <a href="javascript:void(0);" className="thumbnail">
                  <img
                    src={`${imgBaseUrl}lwvlogoabout.png`}
                    alt="Learning with Vodafone Idea"
                    title="Learning with Vodafone Idea"
                  />
                </a>

                <a href="javascript:void(0);" className="thumbnail">
                  <img src={`${imgBaseUrl}3.png`} alt="fair-and-lovely-logo" />
                </a>
                <a href="javascript:void(0);" className="thumbnail">
                  <img src={`${imgBaseUrl}4.png`} alt="marubeni-logo" />
                </a>
                <a href="javascript:void(0);" className="thumbnail">
                  <img src={`${imgBaseUrl}5.png`} alt="tata-trust-logo" />
                </a>
              </Slider>
            </article>
          </article>
        </section>
      </section>
    );
  }
}
export default OurPartnerSlider;
