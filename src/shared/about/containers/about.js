import React from "react";
import { connect } from "react-redux";
import { fetchTeam as fetchAboutAction } from "../actions";
import About from "../components/aboutPage";

const mapStateToProps = ({ about }) => ({
  showLoader: about.showLoader,
  team: about.team,
  isError: about.isError,
  errorMessage: about.errorMessage
});
const mapDispatchToProps = dispatch => ({
  loadTeam: () => dispatch(fetchAboutAction("pageData"))
});

export default connect(mapStateToProps, mapDispatchToProps)(About);
