export const FETCH_DONATION_REQUESTED = "FETCH_DONATION_REQUESTED";
export const FETCH_DONATION_SUCCEEDED = "FETCH_DONATION_SUCCEEDED";
export const FETCH_DONATION_FAILED = "FETCH_DONATION_FAILED";

export const donationList = data => ({
  type: FETCH_DONATION_REQUESTED,
  payload: { data: data }
});
