import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

/************   All actions required in this module starts *********** */
import {
  FETCH_DONATION_SUCCEEDED,
  FETCH_DONATION_REQUESTED,
  FETCH_DONATION_FAILED
} from "./actions";


/*** Fetch Content Start***/

const fetchDonate = (input) =>
  fetchClient.get(`${apiUrl.donationUrl}?page=0&size=10`).then(res => {
    return res.data;
  });

function* donate(input) {
  try {
    const data = yield call(fetchDonate, input.payload);
    yield put({
      type: FETCH_DONATION_SUCCEEDED,
      payload: data
    });
  } catch (error) {
    yield put({
      type: FETCH_DONATION_FAILED,
      payload: error
    });
  }
}

export default function* DonateSaga() {
  yield takeEvery(FETCH_DONATION_REQUESTED, donate);
}
