import React, { Component } from "react";
import Slider from "react-slick";


class Beneficiaries extends Component {

	render() {
		// const { donationList } = this.props;
		var settings = {
			dots: true,
			infinite: true,
			speed: 500,
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: [
				{
					breakpoint: 1199,
					settings: {
						dots: true,
						infinite: true,
						speed: 500,
						slidesToShow: 3,
						slidesToScroll: 3
					}
				},
				{
					breakpoint: 853,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 3,
						dots: true
					}
				},
				{
					breakpoint: 603,
					settings: {
						dots: true,
						infinite: true,
						speed: 500,
						slidesToShow: 1,
						slidesToScroll: 3,
						dots: true
					}
				}
			]
		};

		return (
			<section>

				<Slider {...settings}>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/zeyaa.mp4" type="video/mp4" />
										</video>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/aditi.mp4" type="video/mp4" />
										</video>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/raju.mp4" type="video/mp4" />
										</video>


									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/farman.mp4" type="video/mp4" />
										</video>


									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/lado.mp4" type="video/mp4" />
										</video>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/manpreet.mp4" type="video/mp4" />
										</video>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/ponting.mp4" type="video/mp4" />
										</video>


									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/pooja.mp4" type="video/mp4" />
										</video>


									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/rajan.mp4" type="video/mp4" />
										</video>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/ravi.mp4" type="video/mp4" />
										</video>


									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/rayapati.mp4" type="video/mp4" />
										</video>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/sanjay.mp4" type="video/mp4" />
										</video>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/shivani.mp4" type="video/mp4" />
										</video>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="box-beni-video">
							<div className="courses-thumb">
								<div className="courses-top">
									<div className="courses-image">
										<video width="100%" height="240" controls>
											<source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/vinod.mp4" type="video/mp4" />
										</video>

									</div>
								</div>
							</div>
						</div>
					</div>

				</Slider>
				<br /><br /><br />
			</section>


		)
	}
}



export default Beneficiaries;
