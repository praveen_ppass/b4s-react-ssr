import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Testimonials from "./testimonials-donation";
import { donationList as donationListAction, FETCH_DONATION_SUCCEEDED, FETCH_DONATION_REQUESTED } from "../actions";
import { FacebookIcon, LinkedinIcon, TwitterIcon, WhatsappIcon, FacebookShareButton, LinkedinShareButton, TwitterShareButton, WhatsappShareButton } from "react-share";
class donation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgpopup: false,
      coupanCode: null,
      donationList: [],
      beneficiaries: [{
        name: "Mustafa Khan",
        location: "Shamli, UP",
        img: "https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/mustafa-family.jpeg",
        discription: `The coronavirus crisis has crippled us and resulted in the loss of my only source of income. Before this lockdown was announced, I worked at a small kirana store on a daily wage-basis. My monthly income of Rs. 3000 barely met the daily needs of my three children and wife, but at least, we were able to live a life of dismal choices. Today, my job of stacking the groceries neatly in the store is of no use to the owner. For how many more days will we survive in this state? How will I feed my family? Will we even survive this crisis?`
      }, {
        name: "Balraj Singh",
        location: "Hisar, Haryana",
        img: "https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/balraj.jpg",
        discription: `With no work available for me as a daily wage worker, my family of five including 3 children have no place to go. Due to depleting savings, I'm not even left with enough money to buy basic dal and atta. The other day I got so desperate that I decided to ask for help from someone within the city, but the police restrictions due to the fear of coronavirus are so strict that I wasn't able to go anywhere. My children are eating bare minimum food and feel sick already. If help doesn't reach us on time, I see no hope for the future. `
      }, {
        name: "Md Hossain Mullick",
        location: "Howrah",
        img: "https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/hossain.jpeg",
        discription: "6 of us family members have been locked down in our one room house since last month due to the panic of coronavirus. With no daily wage jobs available, we somehow survived for the first 21 days by borrowing money from our relatives. But after the lockdown extension, even they aren't able to help anymore. Due to the fast diminishing food supplies, I and my wife let the children have their 3 meals while we survive on just one. In a few days, I’m afraid that even they won’t get enough."
      }, {
        name: "Mahesh Metri",
        location: "Belgaum, Karnataka",
        img: "https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/mahesh-metri.jpeg",
        discription: "A father's worst nightmare is seeing his children hungry and hopeless. Unfortunately, I am living my worst at the moment. Due to the spread of this virus, I’ve lost my job as a daily wage worker and see no hope of resuming work any sooner. The meager income of 16,000 a year that I earn is dedicated to bringing food on the table every night. With that income gone, my family of 6 is devastated. The local government is not giving enough groceries for us to survive. I’m seeking help for the sake of my children."
      }],
      itemPopup: null,
      successPay: false
    };
    this.openPopup = this.openPopup.bind(this);
    this.closePopup = this.closePopup.bind(this);
    this.popupPayment = this.popupPayment.bind(this);
  }
  popupPayment() {
    let newurl = window.location.protocol + "//" + window.location.host + window.location.pathname;
    window.history.pushState({ path: newurl }, '', newurl);
    this.setState({
      successPay: false
    });
  }
  componentWillReceiveProps(nextProps) {
    const { donationlist, type } = nextProps;
    switch (type) {
      case FETCH_DONATION_REQUESTED:
        this.setState({
          donationList: []
        });
        break;
      case FETCH_DONATION_SUCCEEDED:
        this.setState({
          donationList: donationlist && donationlist.donations ? donationlist.donations : []
        })
        break;
    }
  }

  componentDidMount() {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams && urlParams.get('status') === "success") {
      this.setState({
        successPay: true
      });
    }

    if (urlParams && urlParams.get('coupon')) {
      this.setState({
        coupanCode: urlParams.get('coupon')
      })
    }
    this.props.donationList();
    const script = document.createElement("script");
    script.setAttribute("src", "https://js.instamojo.com/v1/button.js");
    script.setAttribute("type", "text/javascript");
    document.getElementById("donateButton").appendChild(script);
    setTimeout(() => {
      this.windowJump();
    }, 1000);
  }

  windowJump() {
    const { hash } = this.props.location;
    if (hash && hash.includes("#transparency")) {
      this.refs.transparency.scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "nearest"
      });
    }
  }
  openPopup(item) {
    this.setState({
      imgpopup: true,
      itemPopup: item
    });
  }
  closePopup() {
    this.setState({
      imgpopup: false,
      itemPopup: null
    });
  }
  render() {
    const { beneficiaries, itemPopup, imgpopup, successPay, donationList, coupanCode } = this.state;
    return (
      <section className="donation">
        <article className="banner">
          <div className="but-payment">
            {/* <a data-text="Contribute Now" data-css-style="color:#ffffff; background:#5DAF5B;" href="https://www.instamojo.com/b4sindiafoundation/covid-19-support-initiative-for-daily-wage-w/" rel="im-checkout">
            </a> */}
            {coupanCode ?
              <a data-text="Contribute Now" data-css-style="color:#ffffff; background:#5DAF5B;" href={`https://www.instamojo.com/b4sindiafoundation/covid-19-support-initiative-for-daily-wage-w/?data_Field_68182=${coupanCode}&data_readonly=data_Field_68182`} rel="im-checkout">
              </a>
              : <a data-text="Contribute Now" data-css-style="color:#ffffff; background:#5DAF5B;" href="https://www.instamojo.com/b4sindiafoundation/covid-19-support-initiative-for-daily-wage-w/" rel="im-checkout">
              </a>
            }
          </div>
          <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/covid-19-banner-updated.jpg" />
          <p className="text-center posabs">
            <div className="shareicon-web">
              <FacebookShareButton url={'https://bit.ly/2KA1dvO'}
                quote="I contributed to Buddy4study Foundation's support for daily wage workers and migrant laborers. If you are also worried about the survival of the families of daily wage earners and want to make a maximum impact by ensuring direct fund transfer to verified families, please contribute."
              >
                <FacebookIcon size={35} round={false} />
              </FacebookShareButton>
            </div>
            <div className="shareicon-web">
              <LinkedinShareButton url={'https://bit.ly/3aBezlS'}
                title="I contributed to Buddy4study Foundation's support for daily wage workers and migrant laborers. If you are also worried about the survival of the families of daily wage earners and want to make a maximum impact by ensuring direct fund transfer to verified families, please contribute." >
                <LinkedinIcon size={35} round={false} />
              </LinkedinShareButton>
            </div>
            <div className="shareicon-web">
              <TwitterShareButton url={'www.buddy4study.com/covid-19-support-initiative-for-daily-wage-workers-and-migrant-laborers'}
                title="I contributed to Buddy4study Foundation's support for daily wage workers and migrant laborers. If you are also worried about the survival of the families of daily wage earners and want to make a maximum impact by ensuring direct fund transfer to verified families, please contribute.">
                <TwitterIcon size={35} round={false} />
              </TwitterShareButton>
            </div>
            <div className="shareicon-web">
              <WhatsappShareButton url={'www.buddy4study.com/covid-19-support-initiative-for-daily-wage-workers-and-migrant-laborers'}
                title="I contributed to Buddy4study Foundation's support for daily wage workers and migrant laborers. If you are also worried about the survival of the families of daily wage earners and want to make a maximum impact by ensuring direct fund transfer to verified families, please contribute."
              >
                <WhatsappIcon size={35} round={false} />
              </WhatsappShareButton>
            </div>

          </p>
        </article >
        <section>
          <div className="container">
            <div className="row">
              <div className="latest-update">
                <Testimonials donationList={donationList} />

              </div>
            </div>
          </div>
        </section >
        <section id="feature">
          <div className="container">
            <div className="row">

              <div className="col-md-4 col-sm-4">
                <div className="feature-thumb">
                  <h3>What type of families will be supported?</h3>
                  <p>Daily wage workers & migrant laborers who have lost their jobs & battling to survive</p>
                </div>
              </div>

              <div className="col-md-4 col-sm-4">
                <div className="feature-thumb">

                  <h3>How much money will go to individual beneficiary?</h3>
                  <p>These families need support for 2 to 3 months so that they can survive lockdown & job-loss</p>
                </div>
              </div>

              <div className="col-md-4 col-sm-4">
                <div className="feature-thumb bordernone-feature">

                  <h3>How the beneficiary will use money?</h3>
                  <p>The beneficiary can use the money for their survival including grocery, medicines & other essential items</p>
                </div>
              </div>

            </div>
          </div>
        </section>

        {/* About Program */}
        <section id="program">
          <div className="container">
            <div className="row">

              <div className="col-md-12 col-sm-12">
                <h2 className="text-center">Why you should donate?</h2>
                <p className="text-left donate">
                  With more than 80 percent of India’s workforce employed in the informal sector, and one-third working as casual laborers, it is crucial that we together support them to survive. The biggest impact of COVID-19 is on the daily wage workers and migrant laborers, who never had sufficient savings to survive the lockdown. Millions of such workers if not supported today, will not be in a position to survive staying at home and social distancing. This might result in social strife with millions coming on roads as seen during the first 2 days of lockdown when migrant workers crowded bus stands in hope of ferrying a right back home.
</p>

                <p className="text-left donate">
                  Workers at construction sites, shops, restaurants, delivery staff and local transport systems, the daily wager community has seen their only source of income come to a screeching halt, leaving them with no means to earn themselves a meal during these days of lockdown.


</p>
                <p className="text-left donate">
                  Buddy4Study India Foundation has helped children of over 85000 families with financial aid to complete education. Now due to the COVID19 pandemic they are facing the even bigger problem of survival and hunger. Buddy4Study team has started this campaign to help these families survive and entire money raised will be transferred to the neediest families.
</p>
                <p className="text-left donate">
                  Transferring money to their accounts will not only help them buy food but other essentials such as medicine, groceries and other essentials. It is not onetime support and then leave them helpless but a support over a period of 2 to 3 months to survive this pandemic and stay at home.
</p>

                <p className="text-left donate">
                  Your donation will help them in surviving this tough time and stay at home for the benefit of society.
                </p>
              </div>
            </div>
          </div>
        </section>
        <section id="transparency" ref="transparency" name="transparency" className="padding60">
          <div className="container">
            <div className="row">
              <h2 className="text-center">How do we ensure transparency?</h2>

              <div className="col-md-6 col-sm-12">
                <div className="about-info">
                  <figure>
                    <span><i className="fa fa-arrow-right"></i></span>
                    <figcaption>

                      <h3>Buddy4Study connects donors with beneficiaries directly without any intermediary NGO, leveraging our large registered EWS (Economically weaker section) student base </h3>
                    </figcaption>
                  </figure>

                  <figure>
                    <span><i className="fa fa-arrow-right"></i></span>
                    <figcaption>

                      <h3>Buddy4Study team will do a three-step verification for all beneficiary:
                      <ul>
                          <li>Application and document verification</li>
                          <li>Telephonic verification of participants</li>
                          <li>Video interviews to verify their home and family members</li>
                        </ul>


                      </h3>
                    </figcaption>
                  </figure>

                  <figure>
                    <span><i className="fa fa-arrow-right"></i></span>
                    <figcaption>

                      <h3>Details of all beneficiaries will be uploaded on the website for donor verifications</h3>
                    </figcaption>
                  </figure>
                  <figure>
                    <span><i className="fa fa-arrow-right"></i></span>
                    <figcaption>

                      <h3>Beneficiary payments acknowledgments will be uploaded at Buddy4Study</h3>
                    </figcaption>
                  </figure>
                </div>
              </div>
              <div className="col-md-6">
                <img className="img-responsive" src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/donation-img.jpg" />
              </div>
            </div>
          </div>
        </section>

        {/* donor */}
        <section id="courses">
          <div className="container">
            <div className="row">

              <div className="col-md-12 col-sm-12">
                <div className="section-title">
                  <h2>Our Beneficiaries</h2>
                </div>

                <div className="owl-carousel owl-theme owl-courses">

                  {
                    beneficiaries && beneficiaries.map((item, id) => (
                      <div className="col-md-3 col-sm-3" key={id}>
                        <div className="item">
                          <div className="courses-thumb">
                            <div className="courses-top">
                              <div className="courses-image">
                                <img src={item.img} className="img-responsive img-width" alt="" />
                              </div>
                            </div>
                            <div className="courses-detail">
                              <h3>{item.name}</h3>
                              <p>Location: {item.location}</p>
                            </div>

                            <div className="courses-info">
                              <div className="button-ben" onClick={() => this.openPopup(item)}>Read More</div>
                            </div>

                          </div>
                        </div>
                      </div>
                    ))
                  }
                  <div className="courses-info">
                    <Link to="beneficiaries"> <div className="button">View all 20 beneficiaries</div></Link>
                  </div>
                  {imgpopup ? (
                    <div className="donation-popup">
                      <div className="modal-dialog">
                        <div className="modal-content">
                          <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" onClick={this.closePopup}>&times;</button>
                            <h4 className="modal-title">{itemPopup.name}</h4>
                          </div>
                          <div className="modal-body">
                            <p>{itemPopup.discription}</p>

                            <p></p>
                          </div>
                        </div>

                      </div>
                    </div>
                  ) : null}

                  {successPay ?
                    <div className="donation-popup">
                      <div className="modal-dialog">
                        <div className="modal-content">
                          <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" onClick={this.popupPayment}>&times;</button>
                            <h4 className="modal-title">Payment Success</h4>
                          </div>
                          <div className="modal-body text-center">
                            <p>Thanks for contributing to Buddy4Study Covid-19 Livelihood Support Initiative. We will share beneficiary names shortly with you. Please share this with your network to help us support more families.</p>

                            <p className="text-center">
                              <b>Share with</b><br />
                              <div className="shareicon">
                                <FacebookShareButton url={'https://bit.ly/2KA1dvO'}
                                  quote="I contributed to Buddy4study Foundation's support for daily wage workers and migrant laborers. If you are also worried about the survival of the families of daily wage earners and want to make a maximum impact by ensuring direct fund transfer to verified families, please contribute."
                                >
                                  <FacebookIcon size={50} round={true} />
                                </FacebookShareButton>
                              </div>
                              <div className="shareicon">
                                <LinkedinShareButton url={'https://bit.ly/3aBezlS'}
                                  title="I contributed to Buddy4study Foundation's support for daily wage workers and migrant laborers. If you are also worried about the survival of the families of daily wage earners and want to make a maximum impact by ensuring direct fund transfer to verified families, please contribute." >
                                  <LinkedinIcon size={50} round={true} />
                                </LinkedinShareButton>
                              </div>
                              <div className="shareicon">
                                <TwitterShareButton url={'www.buddy4study.com/covid-19-support-initiative-for-daily-wage-workers-and-migrant-laborers'}
                                  title="I contributed to Buddy4study Foundation's support for daily wage workers and migrant laborers. If you are also worried about the survival of the families of daily wage earners and want to make a maximum impact by ensuring direct fund transfer to verified families, please contribute.">
                                  <TwitterIcon size={50} round={true} />
                                </TwitterShareButton>
                              </div>
                              <div className="shareicon">
                                <WhatsappShareButton url={'www.buddy4study.com/covid-19-support-initiative-for-daily-wage-workers-and-migrant-laborers'}
                                  title="I contributed to Buddy4study Foundation's support for daily wage workers and migrant laborers. If you are also worried about the survival of the families of daily wage earners and want to make a maximum impact by ensuring direct fund transfer to verified families, please contribute.">
                                  <WhatsappIcon size={50} round={true} />
                                </WhatsappShareButton>
                              </div>

                            </p>

                            <button type="button" className="button" onClick={this.popupPayment}>Share</button>
                          </div>
                        </div>

                      </div>
                    </div>
                    : null}





                </div>

              </div>
            </div>
          </div>
        </section>
        {/* why donate */}
        <section id="program">
          <div className="container">
            <div className="row">

              <div className="col-md-12 col-sm-12">
                <h2 className="text-center"> Why donate through Buddy4Study India Foundation?</h2>
                <div className="col-md-6 col-sm-12">
                  <div className="buddybox">
                    Buddy4Study has been working with Economically weaker section families for over 8 years and trusted by various corporates including Loreal, Fair & Lovely, HUL, Tata Trust, HP, Colgate among many others.
</div>
                </div>
                <div className="col-md-6 col-sm-12 ">
                  <div className="buddybox">
                    Buddy4Study has developed a technology-driven transparent platform to connect donors and beneficiaries. We have received various accolades including "Best Social Enterprise Award", "Top 10 social enterprise by Action for India", "Nasscom Top 50 Innovative companies" among many others
</div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="buddybox">
                    Buddy4Study India Foundation is not for profit section 8 company
</div>
                </div>

                <div className="col-md-6 col-sm-12">
                  <div className="buddybox">
                    Tax exemption through 80G certificate for the donated amount


</div>
                </div>
              </div>
            </div>
          </div>
        </section>


        {/* About Program */}
        <section id="about" className="padding60">
          <div className="container">
            <div className="row">

              <div className="col-md-12 col-sm-12">
                <h2 className="text-center">About Buddy4Study</h2>
                <p className="text-center">
                  We are India’s largest financial support platform with information of more than 3 million families across India. We have been working with India’s leading corporates such as HUL, Wipro, Tata Trusts, DLF Foundation, HP, Rolls-Royce, STFC as their technology partners for implementation of their scholarship programme. Our technology-driven platform has helped more than enabled more than 85,000 students with scholarship disbursement of over INR 90 crores.
</p>
              </div>
            </div>
          </div>
        </section>
        {/* footer */}
        <footer id="footer">
          <div className="container">
            <div className="row">

              <div className="col-md-12 col-sm-12">
                <div className="footer-info">


                  <div className="copyright-text">
                    <p>Copyright &copy; 2020 Buddy4Study All rights reserved</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>
        {/* why transpernacy */}
        <span id="donateButton" className="float-button">
          {/* <a data-text="Contribute Now" data-css-style="color:#ffffff; background:#5DAF5B;" href="https://www.instamojo.com/b4sindiafoundation/covid-19-support-initiative-for-daily-wage-w/" rel="im-checkout">Contribute Now</a> */}
          {coupanCode ?
            <a data-text="Contribute Now" data-css-style="color:#ffffff; background:#5DAF5B;" href={`https://www.instamojo.com/b4sindiafoundation/covid-19-support-initiative-for-daily-wage-w/?data_Field_68182=${coupanCode}&data_readonly=data_Field_68182`} rel="im-checkout">
            </a>
            : <a data-text="Contribute Now" data-css-style="color:#ffffff; background:#5DAF5B;" href="https://www.instamojo.com/b4sindiafoundation/covid-19-support-initiative-for-daily-wage-w/" rel="im-checkout">
            </a>
          }

        </span>
      </section >
    );
  }
}

const mapStateToProps = ({ dontionReducer }) => ({
  type: dontionReducer.type,
  donationlist: dontionReducer.donationlist
});

const mapDispatchToProps = dispatch => ({
  donationList: contactData => dispatch(donationListAction(contactData))
});

export default connect(mapStateToProps, mapDispatchToProps)(donation);
