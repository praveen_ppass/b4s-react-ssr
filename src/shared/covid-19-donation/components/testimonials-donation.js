import React, { Component } from "react";
import Slider from "react-slick";
import moment from "moment";

class Testimonials extends Component {

	render() {
const {donationList} = this.props;
		var settings = {
			dots: true,
			infinite: donationList && donationList.length > 5,
			speed: 500,
			slidesToShow: 5,
			slidesToScroll: 5,
			responsive: [
				{
					breakpoint: 1199,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4,
						infinite: true,
						dots: true
					}
				},
				{
					breakpoint: 853,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4,
						dots: true
					}
				},
				{
					breakpoint: 603,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
						dots: true
					}
				}
			]
		};

		return (
			<section>
{donationList && donationList.length > 0 ? 
				<Slider {...settings}>
{donationList.map((item,index)=>(
					<div className="col-md-2 col-sm-4 col-xs-6" key={index}>
						<div className="staff">
							<div className="d-flex mb-4">
								<div className="info ml-4">
									<h3><a>{item.name}</a></h3>
									{/* <span className="position">Donated {moment(item.donationTime).fromNow()}</span> */}
									<div className="text">
										<p>Donated <br /><span>₹{item.amount}</span></p>
									</div>
								</div>
							</div>
						</div>
					</div>

))}
				</Slider> : ""
				}
			</section>
		);
	}
}

export default Testimonials;
