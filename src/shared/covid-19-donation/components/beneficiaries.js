import React, { Component } from "react";
import { Link } from "react-router-dom";
import Beneficiaries from "./slider-beneficiaries";
class beneficiaries extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showItem: null,
      customAccor: "ACCORDION1"
    };
    this.ChangeTab = this.ChangeTab.bind(this);
    this.frequestAskedQuesHandler = this.frequestAskedQuesHandler.bind(this);
  }
  ChangeTab(event) {
    const id = event.target.id;
    this.setState({
      customAccor: id
    });
  }

  frequestAskedQuesHandler(index) {
    this.setState({
      showItem: index
    });
  }

  render() {
    return (
      <section className="donation">
        <section id="courses" className="baneficiaries">
          <div className="container">
            <div className="row">
              <div className="col-md-12 col-sm-12">
                <div className="section-title">
                  <div className="paddinglr">
                    <h2 className="pull-left">Our Beneficiaries Stories</h2>
                    <Link className="pull-right" to="covid-19-support-initiative-for-daily-wage-workers-and-migrant-laborers">Back</Link>
                  </div>
                </div>
                <div className="row paddinglr">
                  <div className="col-md-12 col-sm-12 col-xs-12">
                    <div className="box-beni-video ">
                      <div className="courses-thumb">
                        <div className="courses-top">
                          <div className="courses-image">
                            <video width="100%" height="240" controls>
                              <source src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/Raju story_updated.mp4" type="video/mp4" />
                            </video>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <Beneficiaries /> */}
                </div>


                {/* accordion */}
                {/* <article className="templateFAQs">
                  <article className="content">
                    <FrequestAskedQuestion
                      frequestAskedQuesHandler={this.frequestAskedQuesHandler}
                      showItem={this.state.showItem}
                    />
                  </article>
                </article> */}

                <div className="section-title accordion">
                  <div>

                    <h2
                      id="ACCORDION1"
                      onClick={this.ChangeTab}
                      className={
                        this.state.customAccor === "ACCORDION1" ? "active" : ""
                      }
                    >&nbsp;&nbsp;
                <i className={this.state.customAccor === "ACCORDION1" ? "fa fa-minus" : "fa fa-plus"}></i> &nbsp;Batch 2 - Disbursal of May
              </h2>



                  </div>
                </div>
                {this.state.customAccor === "ACCORDION1" ? <BatchoneAccordion /> : ""}

                <div className="section-title accordion margintop20">
                  <div>
                    <div>
                      <h2
                        id="ACCORDION2"
                        onClick={this.ChangeTab}
                        className={
                          this.state.customAccor === "ACCORDION2" ? "active" : ""
                        }
                      >&nbsp;&nbsp;
                <i className={this.state.customAccor === "ACCORDION2" ? "fa fa-minus" : "fa fa-plus"}></i> &nbsp;Batch 1 - Disbursal of April
              </h2>

                    </div>
                  </div>
                </div>
                {this.state.customAccor === "ACCORDION2" ? <BatchTwoAccordion /> : ""}



              </div>
            </div>
          </div>
        </section>

      </section >
    );
  }
}
export default beneficiaries;



// const FrequestAskedQuestion = props => {
//   const frequenstAskedQuestion = [
//     {
//       title:
//         "<div className='section- title accordion'><div><h2>&nbsp;&nbsp;Batch 2 - Disbursal of May</h2> </div></div>",

//       desc:
//         "aaaaaaaaaaaaa"
//     },
//     {
//       title:
//         "What is Keep India Smiling Foundational Scholarship Programme for Education?",

//       desc:
//         "aaaaaaaaaaaaa"
//     }
//   ];

// return (
//   <ul>
//     {frequenstAskedQuestion.map((list, index) => (
//       <li
//         key={index}
//         onClick={() => props.frequestAskedQuesHandler(index)}
//         className={props.showItem == index ? "active" : ""}
//         dangerouslySetInnerHTML={{
//           __html: list.title
//         }}
//       >
//         <h5
//           dangerouslySetInnerHTML={{
//             __html: list.title
//           }}
//         />
//         <p
//           dangerouslySetInnerHTML={{
//             __html: list.desc
//           }}
//         />
//       </li>
//     ))}
//   </ul>
// );
// };


const BatchoneAccordion = props => {
  return (
    <div className="container-box" >
      <div className="section-title">
        <div className="paddinglr">
          <h2 className="pull-left font-weightnone" >Our Beneficiaries </h2>
          <h3 className="pull-right font-weightnone">Total 15 Beneficiaries</h3>
        </div>
      </div>
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/poornima.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Purnima Tyagi </h3>
              <p>
                <ul>
                  <li>Location: Roorkee, Haridwar</li>
                  <li>Occupation: Daily Wage Worker</li>
                </ul>
              </p>
            </div>

            <div>
              Survival on 2 measly meals was not what Purnima had ever imagined for her family. As soon as the COVID-19 lockdown got announced, Purnima’s father lost his job as an e-rickshaw driver. However now, even as the lockdown has been eased in their small town of Ramnagar, her father hasn’t found any means to earn money. The ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ by Buddy4Study gave this family the encouragement to keep moving forward till the next three months.
     </div>

          </div>
        </div>
      </div>

      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/toshika.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Toshika Nainiwal </h3>
              <p>
                <ul>
                  <li>Location: Ambala, Haryana</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Life was hard for Toshika and her 4-member family even before the world started grappling with coronavirus. Her father, a labourer in the local leather factory, used to earn less than 2000 a month. With even this source of income gone, the fight for survival has been never ending.  The ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ prevented this family from living a day-to-day existence.

         </div>

          </div>
        </div>
      </div>

      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/ram-kishan.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Ram Kishan</h3>
              <p>
                <ul>
                  <li>Location: Mathura, Uttar Pradesh</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Ram’s family hit rock bottom on the very first day of lockdown. This family of 5 survived on Rs. 5000 a month, an earning that came from his father’s job as a manual worker in a truck loading factory in Mathura. Buddy4Study’s help by the way of their ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ has proved to be of much help during these adverse times.
 </div>

          </div>
        </div>
      </div>

      {/* <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/shivani.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Rayapati Rohith Sai </h3>
              <p>
                <ul>
                  <li>Location: West Godavari, Andhra Pradesh</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Rohith belongs to Velivennu Village in Andhra Pradesh, an area which comes in the red zone of COVID-19 affected areas. Before the lockdown was declared, his father used to work as a daily wage worker in the nearby fields. With a decline in farmer’s income, labourers like him went out of work. Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ has helped this family endure these grim circumstances.
 </div>

          </div>
        </div>
      </div> */}

      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/shruti.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Shruti Sharma</h3>
              <p>
                <ul>
                  <li>Location: Ambala, Haryana</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Shruti’s father used to work as an auto driver in Ambala to earn Rs. 6000 a month and feed his family of 4. With the transport system shut down, their family was struggling to survive before Shruti decided to apply for Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’. Today, with 3 months of financial help, this family has bought some time to figure a way out for the future.
 </div>

          </div>
        </div>
      </div>


      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/kapil.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Kapil Kumar Goyal</h3>
              <p>
                <ul>
                  <li>Location: Jhunjhunu, Rajasthan</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Being the only earning member in his family of 7, Kapil has many responsibilities to shoulder. Prior to the lockdown, he worked a daily wage worker in an electric shop (in Udaipurwati city, Rajasthan) to earn Rs. 9000 a month. With no regular job and many mouths to feed, Kapil felt stranded. Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ came to his family’s aid in this precarious situation.
 </div>

          </div>
        </div>
      </div>

      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/divya.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Divya Kumari</h3>
              <p>
                <ul>
                  <li>Location: Ghaziabad, Uttar Pradesh</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Divya’s father is a labourer in Bhangel Village, Noida who used to earn less than Rs. 7000 a month to feed his family of 5. Ever since the lockdown took place, he hasn’t been able to earn a single penny. Buddy4Study extended financial help through its ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’, giving this family a ray of hope.
 </div>

          </div>
        </div>
      </div>

      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/sunny.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Sunny Kumar</h3>
              <p>
                <ul>
                  <li>Location: Allahabad, Uttar Pradesh</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Sunny is a B.A. 1st year student, living with his family of 4 in the city of Allahabad. His father worked as a bus seat cover tailor in a small garage to earn a monthly income of Rs. 7000. With his job gone due to this lockdown, they were facing a deep crisis. With all their savings spent on food, Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ was their only chance of surviving this phase.
 </div>

          </div>
        </div>
      </div>

      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/piyush.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Piyush Tripathi </h3>
              <p>
                <ul>
                  <li>Location: Allahabad, Uttar Pradesh</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              As compared to what Piyush’s family went through during this lockdown phase, his previous earning of Rs. 3000 a month seems like a blessing. Owing to coronavirus, he has been unable to work in the local cybercafé, which was the only source of survival for his family of 4. Applying for the ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ by Buddy4Study has been a life-saver for them and will continue to be so for the coming 3 months.
 </div>

          </div>
        </div>
      </div>

      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/rahul.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Rahul Deb Mallick</h3>
              <p>
                <ul>
                  <li>Location: Bankura, West Bengal</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Rahul, his wife and two children live a humble life in Bankura District, West Bengal. Before lockdown, he used to work as a daily wage worker and earn around Rs. 3000 monthly. Losing his job brought such misery to his family that he will not be able to forget for years to come. Registering for Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ at the right time saved his loved ones.
 </div>

          </div>
        </div>
      </div>

      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/lubna.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Lubna Ahmad</h3>
              <p>
                <ul>
                  <li>Location: Bijnor, Uttar Pradesh</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              It was unbearable for Lubna to see her family struggling for food ever since her father lost his daily wage work. Being hungry is the worst feeling anyone could ever experience, and makes the bravest of people lose hope. Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ has given them the courage to endure this disaster at least for the next 3 months, till they find a way to earn again.
 </div>

          </div>
        </div>
      </div>

      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/deeraj.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Dheeraj Kumar</h3>
              <p>
                <ul>
                  <li>Location: Jalore, Rajasthan</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Dheeraj used to work as a shoe maker and was proud of his humble job. His monthly income of Rs. 3000 was all he had to feed his 4 children and wife. When people stopped coming out of their homes in Sanchore, Rajasthan, due to the lockdown, his earning stopped, and so did his family’s source of survival. Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ has provided them that source till things come back to normal.
 </div>

          </div>
        </div>
      </div>
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/Gautam.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Gautam Kumar</h3>
              <p>
                <ul>
                  <li>Location: Patna, Bihar</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Gautam’s father, a MGNREGA labourer in Fatuha, Patna, used to earn Rs. 5000 a month to sustain his 4-member family. He stopped getting work post lockdown, leaving the family penniless and starved. Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ gave them a second chance at life.
 </div>

          </div>
        </div>
      </div>
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/sandeep.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Sandeep Roy</h3>
              <p>
                <ul>
                  <li>Location: Ambala, Haryana</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Sandeep’s father, a daily wager, lost his courage and health after losing his job because of the coronavirus pandemic. Hunger broke this 4-member family down both physical and emotionally. By supporting them for the coming 3 months, Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ has given them the hope to strive for a source of revenue again.
 </div>

          </div>
        </div>
      </div>

      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/ritika.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Ritika Saini</h3>
              <p>
                <ul>
                  <li>Location: Ambala, Haryana</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              It has been two months since Ritika’s father lost his job as a labourer in a neighbouring agricultural field. Previously, her family of 4 used to survive on a monthly income of Rs. 8000. Helpless and desperate for help, Ritika applied for Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ and quickly got selected. She and her family now has 3 months of support to strive forward.
 </div>

          </div>
        </div>
      </div>
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/aman.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Aman Faruqi</h3>
              <p>
                <ul>
                  <li>Location: Hapur, Uttar Pradesh</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Aman is a fruit seller, living with his wife and 3 children in Hapur, UP. This lockdown period proved so ruthless for his family that they barely had 2 meals a day to survive. Medically unfit, Aman is scared to venture out and make a living in these times of coronavirus. Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ has supported this family financially and helped them stay inside them homes to protect themselves.
 </div>

          </div>
        </div>
      </div>
    </div>
  );
};

const BatchTwoAccordion = props => {
  return (
    <div className="container-box">
      <div className="section-title">
        <div className="paddinglr">
          <h2 className="pull-left font-weightnone" >Our Beneficiaries </h2>
          <h3 className="pull-right font-weightnone">Total 19 Beneficiaries</h3>
        </div>
      </div>
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/ponting.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Ponting Janagal</h3>
              <p>
                <ul>
                  <li>Location: Fatehabad, Haryana</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Ponting is a class 11 student who is unable to help his father in this difficult time. His father, a daily wage worker, and the only bread earner in the family, has lost his job due to the lockdown and is unsure of any future employment opportunities. The 3-month 'COVID 19 Support Programme for Daily Wage Workers and Migrant Labourers' by Buddy4Study has become their lifeline in this moment of crisis.
     </div>

          </div>
        </div>
      </div>
      {/* end box */}

      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/pooja.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Pooja Rani</h3>
              <p>
                <ul>
                  <li>Location: Udham Singh Nagar, Uttarakhand</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Rani's father, a daily wage worker at a local poultry farm, lost his job due to the COVID-19 lockdown. This job used to bring in Rs. 3000, which was just enough money for the three members in her family to survive. Buddy4Study's initiative 'COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers' gave them hope during this scary situation. Her family no longer needs to borrow resources to survive in their small village of Bajpur, Uttarakhand.                        </div>

          </div>
        </div>
      </div>
      {/* end box */}

      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/shivani.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>	Shivani Pundir</h3>
              <p>
                <ul>
                  <li>Location: Ambala, Haryana</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Shivani is a B. Com 2nd year student from Ambala. Her family of four has faced the toughest time of their lives ever since this lockdown was announced. Her father, a mechanic at a local garage who earned Rs. 8000 a month, lost his job due to the coronavirus situation. Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ is helping them cope with their circumstances by providing them assistance for the coming 3 months.
 </div>

          </div>
        </div>
      </div>
      {/* end box */}

      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/manpreet.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Manpreet Singh</h3>
              <p>
                <ul>
                  <li>Location: Ambala , Haryana</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Manpreet is a BCA 2nd year student living in a joint family of 28 members in Ambala. She has seen her family surviving on bare minimum and borrowing money from neighbours, ever since her father, a daily wage worker, lost his job due to the COVID-19 lockdown. A heart patient, her father wasn’t even getting proper medicines before the Buddy4Study team extended its help through the ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’. With the financial assistance provided, Shivani hopes for better times to come.
      </div>

          </div>
        </div>
      </div>
      {/* end box */}

      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/sanjay.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Sanjay Kumar</h3>
              <p>
                <ul>
                  <li>Location: Dibrugarh, Assam</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>Sanjay is the only bread earner in his family of five. Due to the disruption caused by coronavirus in his city of Dibrugarh in Assam, he lost his daily wage job of a bamboo crafts worker. Ever since, feeding his three children has been a challenge. Due to lack of proper documents, he wasn’t even able to get help from the local government. With the help of Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’, he has enough funds to pass through the next three months and even save for further unpredictable days.
  </div>

          </div>
        </div>
      </div>
      {/* end box */}

      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/rajan.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>	Rajankumar Shantibhai</h3>
              <p>
                <ul>
                  <li>Location: Surat, Gujrat</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Rajankumar’s family was desperate to make ends meet ever since his father, the only wage earner in the family, lost his job. A factory labourer, Rajan’s father earned a monthly salary of Rs. 10,000 before coronavirus brought everything to a stop. Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ saved this family from incurring a heavy debt due to consistent borrowing from relatives.
    </div>

          </div>
        </div>
      </div>
      {/* end box */}

      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/rayapati.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Rayapati Ganesh Shyam</h3>
              <p>
                <ul>
                  <li>Location: West Godavari, Andhra Pradesh</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Due to the coronavirus situation, the families of daily wage workers like Rayapati’s are in deep misery. Rayapati’s family of five has been surviving on 1 kg dal and 5 kgs of rice provided by the government. However, they live in a rented house and were threatened by the possibility of imminent homelessness. Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ provided them financial assistance to live a dignified life during these challenging months.
     </div>

          </div>
        </div>
      </div>
      {/* end box */}

      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/abhiram.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Jaanagonda Abhiraam</h3>
              <p>
                <ul>
                  <li>Location: Kadapa, Andhra Pradesh</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              It was a dark day for Jaanagonda when his father, a daily wager, lost his job due to the COVID-19 lockdown. They were more scared of hunger than the disease itself, since the monthly income of Rs. 5,800 that his father earned, was the only source of livelihood for the 4 members on their family. The ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ by Buddy4Study guaranteed them financial help for the coming 3 months, giving them enough time to find newer ways to find a living.
      </div>

          </div>
        </div>
      </div>
      {/* end box */}

      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/farman.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Farman Ahmad</h3>
              <p>
                <ul>
                  <li>Location: Lucknow, Uttar Pradesh</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Before the trouble of coronavirus started, Farman was earning Rs. 5000 a month as a mechanic. Today, he is not only out of work, but has also lost the means to feed his children. On top of it, getting the government’s supply of wheat and rice has been a harrowing experience because of the strict movement curb around his area in Lucknow. Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ has helped him withstand this painful period and provide for his family.
       </div>

          </div>
        </div>
      </div>
      {/* end box */}


      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/aditi.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Aditi Singla</h3>
              <p>
                <ul>
                  <li>Location: Ambala, Haryana</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Aditi’s family has been in utter despair ever since the markets stopped functioning due to coronavirus. Her father, the only earning member in a family of 4, used to work as a helper in a shop in Ambala. Today, he has no job and the family has no means of their own to live or think of a life further. The ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ by Buddy4Study has given them the courage to overcome the difficulties in the approaching months.
       </div>

          </div>
        </div>
      </div>
      {/* end box */}
      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/zeya.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Zeya Null</h3>
              <p>
                <ul>
                  <li>Location: Kanpur Nagar, Uttar Pradesh</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Zeya’s father is a daily wager who earned a meagre Rs. 4000 a month before the lockdown was announced. Coronavirus hit her family hard since their only source of income was lost. Even though they are receiving rice and wheat from the local government, it’s not enough for them to survive in the coming months. Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ has provided them with the monetary assistance needed to deal with the challenging times that lie ahead.
   </div>

          </div>
        </div>
      </div>
      {/* end box */}
      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/vinod.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Vinod Kumar</h3>
              <p>
                <ul>
                  <li>Location: Alwar, Rajasthan</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Vinod’s monthly income of Rs. 5000 was the only source of feeding his 3 children. After losing his job as a labourer due to the coronavirus disruption, there were nights when he even had to send them to bed on an empty stomach. His wife doesn’t keep well too and he was unable to bring any medical help for her since the last one month. Buddy4Study’s ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ has become his support system in this grim situation.
       </div>

          </div>
        </div>
      </div>
      {/* end box */}
      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/lado.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Lado Null</h3>
              <p>
                <ul>
                  <li>Location: East Delhi, Delhi</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Lado works as a maid in East Delhi and lives all by herself, without the security of her family, in a rented room. Post the lockdown, she stopped receiving her monthly earning of Rs. 3000 and was reduced to a pitiable state. The ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ by Buddy4Study has taken care of her expenses for the coming months. This has relieved her greatly since she was left with no back up resources of her own.
       </div>

          </div>
        </div>
      </div>
      {/* end box */}
      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/ravikant.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Ravikant Balai</h3>
              <p>
                <ul>
                  <li>Location: Alwar, Rajasthan</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              The only security that Ravikant’s family has in the present is their house. Besides that, there was no hope left until they received help from Buddy4Study. Ravi’s father, a daily wager in Alwar, Rajasthan, used to earn around Rs. 5,000 a month which was barely enough for the 4 members to survive even before he lost his job due to the coronavirus lockdown. The ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ has saved them from potential bankruptcy in the coming months.

     </div>

          </div>
        </div>
      </div>
      {/* end box */}
      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/files/beneficiaries/raju.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Raju Kumar</h3>
              <p>
                <ul>
                  <li>Location: Samastipur, Bihar</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              Raju migrated from Samastipur Village in Bihar to Ghaziabad a few years back in search for a job. Before this lockdown took place, he used to work at a dhaba in the city and send his hard-earned money back home. After losing his job, he has been unable to send any funds to feed his family of six. The ‘COVID 19 Support Programme for Daily Wage Workers and Migrant Laborers’ has come as a saviour for him and his family during these testing times.

     </div>

          </div>
        </div>
      </div>
      {/* end box */}

      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/mustafa-family.jpeg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Mustafa Khan</h3>
              <p>
                <ul>
                  <li>Location: Shamli, UP</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              The coronavirus crisis has crippled us and resulted in the loss of my only source of income. Before this lockdown was announced, I worked at a small kirana store on a daily wage-basis. My monthly income of Rs. 3000 barely met the daily needs of my three children and wife, but at least, we were able to live a life of dismal choices. Today, my job of stacking the groceries neatly in the store is of no use to the owner. For how many more days will we survive in this state? How will I feed my family? Will we even survive this crisis?

     </div>

          </div>
        </div>
      </div>
      {/* end box */}

      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/balraj.jpg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Balraj Singh</h3>
              <p>
                <ul>
                  <li>Location: Hisar, Haryana</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              With no work available for me as a daily wage worker, my family of five including 3 children have no place to go. Due to depleting savings, I'm not even left with enough money to buy basic dal and atta. The other day I got so desperate that I decided to ask for help from someone within the city, but the police restrictions due to the fear of coronavirus are so strict that I wasn't able to go anywhere. My children are eating bare minimum food and feel sick already. If help doesn't reach us on time, I see no hope for the future.

     </div>

          </div>
        </div>
      </div>
      {/* end box */}

      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/hossain.jpeg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Md Hossain Mullick</h3>
              <p>
                <ul>
                  <li>Location: Howrah</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              6 of us family members have been locked down in our one room house since last month due to the panic of coronavirus. With no daily wage jobs available, we somehow survived for the first 21 days by borrowing money from our relatives. But after the lockdown extension, even they aren't able to help anymore. Due to the fast diminishing food supplies, I and my wife let the children have their 3 meals while we survive on just one. In a few days, I’m afraid that even they won’t get enough.

     </div>

          </div>
        </div>
      </div>
      {/* end box */}

      {/* box1 */}
      <div className="col-md-4 col-sm-4 col-xs-12">
        <div className="box-beni">
          <div className="courses-thumb">
            <div className="courses-top">
              <div className="courses-image">
                <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/mahesh-metri.jpeg" className="img-responsive img-width ben-box" alt="" />
              </div>
            </div>
            <div className="courses-detail">
              <h3>Mahesh Metri</h3>
              <p>
                <ul>
                  <li>Location: Belgaum, Karnataka</li>
                  <li>Occupation: Daily Wage Worker</li>

                </ul>
              </p>
            </div>

            <div>
              A father's worst nightmare is seeing his children hungry and hopeless. Unfortunately, I am living my worst at the moment. Due to the spread of this virus, I’ve lost my job as a daily wage worker and see no hope of resuming work any sooner. The meager income of 16,000 a year that I earn is dedicated to bringing food on the table every night. With that income gone, my family of 6 is devastated. The local government is not giving enough groceries for us to survive. I’m seeking help for the sake of my children.
     </div>

          </div>
        </div>
      </div>
      {/* end box */}

      {/* box1 */}

    </div>

  );
};
