import {
  FETCH_DONATION_SUCCEEDED,
  FETCH_DONATION_REQUESTED,
  FETCH_DONATION_FAILED
} from "./actions";

const initialState = {
  donationlist: null,
type:null
};
const dontionReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_DONATION_REQUESTED:
      return { showLoader: true, type, donationlist: null };
    case FETCH_DONATION_SUCCEEDED:
      return { showLoader: false, type, donationlist: payload };
    case FETCH_DONATION_FAILED:
      return { showLoader: false, type, donationlist: null };
    default:
      return state;
  }
};
export default dontionReducer;
