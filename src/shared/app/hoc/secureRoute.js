import React, { Component } from "react";
import { Route } from "react-router-dom";

import utils from "../../../globals/globalFunctions";
import { guestAuthTokenCall } from "../../../globals/globalApiCalls";

const SecureRoute = (_, passedProps) => {
  return class extends Component {
    constructor(props) {
      super(props);
      this.state = {
        isAuthTokenPresent: false
      };
    }

    componentDidMount() {
      ////TODO: Authentication token for user

      guestAuthTokenCall()
        .then(response => {
          typeof window !== "undefined"
            ? localStorage.setItem("B4S_AUTHTOKEN", response.data.access_token)
            : "";
          this.setState({
            isAuthTokenPresent: true
          });
        })
        .catch(error => {});
    }

    render() {
      return <Route {...this.props} {...passedProps} {...this.state} />;
    }
  };
};

// class SecureRoute extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       isAuthTokenPresent: false
//     };
//   }

//   componentWillMount() {
//     // if (utils.isAuthTokenPresent()) {
//     //   if (utils.isUserAuthenticated()) {
//     //     //TODO: Authentication token for user
//     //   }
//     // } else {
//     // }
//   }

//   componentDidMount() {
//     // if (utils.isAuthTokenPresent()) {
//     //   if (utils.isUserAuthenticated()) {
//     //     //TODO: Authentication token for user
//     //   }
//     // } else {
//     // }

//     guestAuthTokenCall()
//       .then(response => {
//         localStorage.setItem("B4S_AUTHTOKEN", response.data.access_token);
//         this.setState({
//           isAuthTokenPresent: true
//         });
//       })
//       .catch(error => );
//   }

//   render() {
//     const { InnerComponent } = this.props;
//     return <InnerComponent {...this.props} {...this.state} />;
//   }
// }

// // const SecureRoute = InnerComponent =>
// //   class extends Component {
// //     constructor(props) {
// //       super(props);
// //     }

// //     componentWillMount() {}

// //     render() {}
// //   };

export default SecureRoute;
