import loadable from "loadable-components";

export const HomePage = loadable(() => import("../home/containers/home"));
export const scholarshiplistPage = loadable(() =>
	import("../scholarship/containers/scholarshipListContainer")
);
export const RegisterPage = loadable(() => import("../login/containers/new-registration-form-container"));
export const scholarshipDetailPage = loadable(() =>
	import("../scholarship/containers/scholarshipDetailsContainer")
);
export const termsConditionsPage = loadable(() =>
	import("../terms-conditions/containers/termsConditions")
);

export const contactUsPage = loadable(() =>
	import("../contact-us/containers/contactUs")
);

export const disclaimerPage = loadable(() =>
	import("../disclaimer/components/disclaimerPage")
);

export const cityPage = loadable(() => import("../city/containers/city"));

export const privacyPolicyPage = loadable(() =>
	import("../privacy-policy/containers/privacyPolicy")
);
export const scholarshipConclavePage = loadable(() =>
	import("../scholarship-conclave/components/scholarship-conclave-landing")
);
export const scholarshipConclaveProfilePage = loadable(() =>
	import("../scholarship-conclave/components/scholarship-conclave")
);
export const premiummembershipPage = loadable(() =>
	import("../premium-membership/containers/premiumMembership")
);
export const mediaPartnersPage = loadable(() =>
	import("../media-partners/containers/mediaPartners")
);
export const mediaUrlPage = loadable(() =>
	import("../media-url/containers/mediaUrl")
);
export const teamPage = loadable(() => import("../team/containers/team"));
export const aboutPage = loadable(() => import("../about/containers/about"));
export const careersPage = loadable(() =>
	import("../careers/containers/careers")
);
export const careeropportunitiesPage = loadable(() =>
	import("../careers/containers/career-opportunities")
);
export const careerdetailsPage = loadable(() =>
	import("../careers/containers/careers-details")
);
export const faqsPage = loadable(() => import("../faqs/containers/faqs"));
export const ErrorPage = loadable(() => import("../common/error-404"));
export const ConfirmSubscriptionPage = loadable(() =>
	import("../common/confirm-subscription")
);
export const paymentcancelledPage = loadable(() =>
	import("../common/payment-cancelled")
);

export const paymentsuccessfulPage = loadable(() =>
	import("../common/payment-successful")
);
export const subscriptionsuccessfulPage = loadable(() =>
	import("../common/subscription-successful")
);
export const unsubscribedPage = loadable(() =>
	import("./../user-unsubscribed/containers/unsubscribed")
);
export const faqDetailsPage = loadable(() =>
	import("../faqs/containers/faq-details")
);
export const myProfile = loadable(() =>
	import("../dashboard/containers/myprofileContainer")
);
export const updateMobEmail = loadable(() =>
	import("../dashboard/containers/updateMobileEmailContainer")
);
export const ScholarshipResultListPage = loadable(() =>
	import("../scholarship/containers/ScholarshipResultListContainer")
);
export const ScholarshipResultDetailsPage = loadable(() =>
	import("../scholarship/containers/ScholarshipResultDetailsContainer")
);
export const applicationInstructionContainer = loadable(() =>
	import("../application/containers/applicationInstructionContainer")
);
export const onlineApplicationFormlistPage = loadable(() =>
	import(
		"../online-application-form-list/containers/onlineApplicationFormlistContainer"
	)
);
export const applicationFormContainer = loadable(() =>
	import("../application/containers/applicationFormContainer")
);
export const loginOrRegister = loadable(() =>
	import("../login/containers/userLoginRegistrationContainer")
);
export const scholar = loadable(() =>
	import("../dashboard/containers/scholarContainer")
);

export const mySubscriber = loadable(() =>
	import("../dashboard/containers/mysubscribersContainer")
);

export const myScholarship = loadable(() =>
	import("../dashboard/containers/myscholarshipsContainer")
);

export const myQna = loadable(() =>
	import("../dashboard/containers/questionAnswerContainer")
);

export const forgotPassword = loadable(() =>
	import("../common/components/forgotPassword")
);

export const verifyUserRegistration = loadable(() =>
	import("../login/containers/verifyUserRegistrationContainer")
);

export const collegeBoard = loadable(() =>
	import("../college-board/containers/collegeBoard")
);

export const colgate = loadable(() =>
	import("../colgate-scholarship/components/colgatePages")
);
export const resetPasswordPage = loadable(() =>
	import("../login/containers/resetPasswordContainer")
);

export const educationLoanPage = loadable(() =>
	import("../education-loan/containers/educationLoan")
);

export const kindLanding = loadable(() =>
	import("../kind-landing/components/kindlanding")
);
export const contentPage = loadable(() =>
	import("../content/containers/contentPage")
);

export const scholarProfile = loadable(() =>
	import("../scholar/container/dis-first-scholar-container")
);

export const ikiGai = loadable(() => import("../ikigai/components/ikigai"));
export const donation = loadable(() => import("../covid-19-donation/components/donation"));

export const FAL9 = loadable(() => import("../fnlNepal/components/nepal"));

export const FALMyanmar = loadable(() =>
	import("../fal-myanmar/components/fal-myanmar")
);

export const swiggyResult = loadable(() =>
	import("../swiggy-result/components/swiggy-result")
);

export const studyAbroadPages = loadable(() =>
	import("../international-study/containers/")
);

export const BookSlotRegistration = loadable(() =>
	import("../book-slot/containers/bookSlotInstructionContainer")
);

export const cscPage = loadable(() => import("../csc/containers/cscContainer"));

export const sahajPage = loadable(() =>
	import("../sahaj/containers/sahajContainer")
);

// vle routes
export const vleStudentList = loadable(() =>
	import("../vle/containers/vleSubscriberContainer")
);

export const vleScholarship = loadable(() =>
	import("../vle/containers/vleScholarshipContainer")
);

export const vlePayment = loadable(() =>
	import("../vle/containers/vlePaymentContainer")
);
export const internationalStudy = loadable(() =>
	import("../international-study/containers/index")
);

export const questionAnswer = loadable(() =>
	import("../question-answer/containers/questionAnswerContainer")
);

export const importRoutes = loadable(() =>
	import("../question-answer/containers/importRoutesContainer")
);
export const scholarshipVideo = loadable(() =>
	import("../scholarship-video/containers/scholarship-video")
);

export const brandPage = loadable(() =>
	import("../brandPage/container/brandPageContainer")
);

// Loadable.Map({
//   loader: {
//     [a,b]: () => import('./Bar')
//   }
// });
export const collegeBoardPayment = loadable(() =>
	import("../applicationPayment/containers/applcationPaymentContainer")
);

export const pearsonPayment = loadable(() =>
	import("../applicationPayment/containers/applicationPearsonPaymentContainer")
);

export const scholarResults = loadable(() =>
	import("../scholarResults/containers/scholarResultsContainer")
);

export const applicationShareLink = loadable(() =>
	import("../applicationShareLink/containers/applicationShareLinknContainer")
);

export const otp = loadable(() =>
	import("../scholarship-conclave/containers/otp-container")
);

export const UserInfoUpdate = loadable(() =>
	import("../login/containers/userInfoUpdateContainer")
);

export const paymentAcknowledgement = loadable(() =>
	import("../scholar/container/paymentAcknowledgemrntContainer")
);

export const Summary = loadable(() =>
	import("../summary/components/summary")
);

export const covid = loadable(() =>
	import("../covid-19-donation/components/donation")
);
export const beneficiaries = loadable(() =>
	import("../covid-19-donation/components/beneficiaries")
);