import React from "react";
import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import Helmet from "react-helmet";
import SecureRoute from "./hoc/secureRoute";
import utils from "../../globals/globalFunctions";
import { guestAuthTokenCall } from "../../globals/globalApiCalls";
import * as Routes from "./routes";
import Header from "../../shared/common/containers/headerContainer";
import Footer from "../../shared/common/containers/footerContainer";
import gblFunc from "../../globals/globalFunctions";
import { smsApplicationDetailConfig } from "../application/formconfig";
import "../b4s-theme/app.scss";
import {
  prodEnv,
  applicationFormsUrl,
  apiUrl,
  PRODUCTION_URL
} from "../../constants/constants";

const uid = "UID" || "uid";
const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        gblFunc.isUserAuthenticated() ||
          rest.location.search.includes("token") ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{
                pathname: "/",
                state: {
                  from: props.location,
                  isRedirecting: true,
                  openLoginPopup: true
                }
              }}
            />
          )
      }
    />
  );
};

const IS_CHILD_HUL = function () {
  let isHul = false;
  const locationPath = typeof window !== "undefined" ? location.pathname : "";
  if (locationPath.includes("/instruction")) {
    const getBsid = locationPath.split("/")[2];
    isHul = ["SKKS1", "SSE4", "HUL1", "SCE3", "CKS1", "CES7","CES8", "FAL9","HCL2"].includes(
      getBsid
    );
  }
  return isHul;
};

const App = withRouter(({ location, history }) => {
  if (location.pathname.indexOf("application/") > -1) {
    let urlArray = location.pathname.split("/");
    if (urlArray.length > 3 && urlArray[2] !== urlArray[2].toUpperCase()) {
      urlArray[2] = urlArray[2].toUpperCase();
      const urlBsid = urlArray.join("/");
      return history.push(`${urlBsid}`);
    }
  }
  if (
    location.pathname.indexOf("scholarship/") > -1 ||
    location.pathname.indexOf("scholarship-conclave") > -1
  ) {
    fetch(apiUrl.seoTags, {
      method: "post",
      headers: {
        Authorization:
          "Bearer " +
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJyZWFkIl0sImV4cCI6MTYyMTg0MzM1NSwiYXV0aG9yaXRpZXMiOlsiVVNFUiJdLCJqdGkiOiI4YWE1NjNhZS1iMTFlLTQ4MmItYjZlNS01MjRiMWRhYThkMGMiLCJjbGllbnRfaWQiOiJiNHMifQ.OTodqbDjesRWwGnQZURsS53n_gs2AYsNhbAKWqXb1xc",
        Accept: "*/*",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ url: location.pathname.replace("/", "") })
    })
      .then(response => response.json())
      .then(data => {
        if (data && data.redirectUrl && data.redirectUrl.length > 0) {
          if (
            data.redirectUrl.includes("https://") ||
            data.redirectUrl.includes("http://")
          ) {
            window.location = data.redirectUrl;
          } else {
            return history.push(`/${data.redirectUrl}`);
          }
        } else {
          return;
        }
      });
  }
  return (
    <div>
      {/* <Helmet
      htmlAttributes={{ lang: "en", amp: undefined }} // amp takes no value
      titleTemplate="%s"
      titleAttributes={{ itemprop: "name", lang: "en" }}
      meta={[
        { name: "description", content: "Buddy4Study..." },
        {
          name: "viewport",
          content:
            "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
        }
      ]}
    /> */}
      {/************************CONDITIONAL OPEN HEADER**************************/}
      {location.pathname.substring(1, 10) == "media-url" ||
        (location.pathname.includes("application") &&
          smsApplicationDetailConfig.isSMSAdmin()) ||
        location.pathname.substring(1, 20) === "international-study" ||
        location.pathname.substring(1, 14) == "educationloan" ||
        location.pathname.substring(1, 10) == "csc/login" ||
        location.pathname.substring(1, 12) === "sahaj/login" ||
        location.pathname.substring(1, 10) == "book-slot" ||
        location.pathname.substring(1, 13) == "applications" ||
        location.pathname.substring(1, 15) === "kind-community" ||
        location.pathname.substring(1, 8) === "summary" ||
        location.pathname.substring(1, 72) === "covid-19-support-initiative-for-daily-wage-workers-and-migrant-laborers" ||
        location.pathname.substring(1, 14) === "beneficiaries" ||
        location.pathname.substring(1, 21) === "international-study" ||
        location.pathname.substring(1, 10) == "book-slot" ||
        location.pathname.substring(1, 13) === "collegeboard" ||
        location.pathname.substring(1, 20) === "colgate-scholarship" ||
        location.pathname.substring(1, 21) == "scholarship-conclave" ||
        location.pathname.substring(1, 52) ==
        "fair-and-lovely-career-foundation-nepal-scholarship" ||
        location.pathname.substring(1, 54) ==
        "fair-and-lovely-career-foundation-myanmar-scholarship" ||
        location.pathname.includes(`payment/`) ||
        location.pathname.includes(`page/`) ||
        location.pathname.substring(1, 10) == "user-info" ||
        IS_CHILD_HUL() ||
        location.pathname.includes("application-submitted") ? (
          " "
        ) : (
          <Header pageLoc={location.pathname} />
        )}
      <section
        className={
          location.pathname.substring(1, 10) == "csc/login" ||
            location.pathname.substring(1, 12) == "sahaj/login"
            ? "mainWrapper cscbg"
            : "mainWrapper"
        }
      >
        <Switch>
          {gblFunc.gaTrack.trackPageView(location.pathname)}
          <Route
            key="test"
            exact
            path="/auth/:token"
            component={Routes.HomePage}
          />
          <Route exact path="/" component={Routes.HomePage} />
          <Route
            path="/scholarship/:slug"
            key={new Date()}
            component={Routes.scholarshipDetailPage}
          />

          <Route
            path={`/${uid}/:usid`}
            key={new Date()}
            component={Routes.scholarshiplistPage}
          />
          <Route
            path="/scholarships/:filters"
            key={new Date()}
            component={Routes.scholarshiplistPage}
          />
          <Route
            path="/scholarships"
            key={new Date()}
            component={Routes.scholarshiplistPage}
          />
          <Route
            path="/scholarship-for/:class"
            key={new Date()}
            component={Routes.scholarshiplistPage}
          />
          {/* <Route path="/summary" component={Routes.Summary} /> */}
          <PrivateRoute
            path="/application/:bsid/form/summary"
            component={Routes.Summary}
          />
          <Route path="/user/login" component={Routes.HomePage} />
          <Route path="/vle/login" component={Routes.HomePage} />
          <Route path="/user/register" component={Routes.HomePage} />
          <Route path="/user/logout" component={Routes.HomePage} />
          // path="/organisations/login/:orgid"
          {typeof window !== "undefined" ? (
            <Route
              path="/organisations/login/:orgid"
              exact
              render={props => {
                const searchString = props.location.search || "";
                const url = `${applicationFormsUrl.prod}${props.location.pathname}${searchString}`;
                window.location = url;
              }}
            />
          ) : (
              ""
            )}
          <Route
            path="/terms-and-conditions"
            component={Routes.termsConditionsPage}
          />
          <Route path="/contact-us" component={Routes.contactUsPage} />
          <Route path="/privacy-policy" component={Routes.privacyPolicyPage} />
          <Route
            exact
            path="/scholarship-conclave"
            component={Routes.scholarshipConclaveProfilePage}
          />
          <Route exact path="/otp" component={Routes.otp} />
          <Route
            exact
            path="/scholarship-conclave/profile"
            component={Routes.scholarshipConclaveProfilePage}
          />
          <Route path="/team" component={Routes.teamPage} />
          <Route path="/disclaimer" component={Routes.disclaimerPage} />
          <Route path="/mict-result" component={Routes.cityPage} />
          <Route path="/media/partners" component={Routes.mediaPartnersPage} />
          <Route
            path="/media-url/:title/:bsid"
            component={Routes.mediaUrlPage}
          />
          <Route path="/id/:bsid" exact component={Routes.mediaUrlPage} />
          <Route
            key={new Date()}
            path="/premium-membership"
            component={Routes.premiummembershipPage}
          />
          <Route path="/about-us" component={Routes.aboutPage} />
          <Route path="/careers" component={Routes.careersPage} />
          <Route
            path="/career-opportunities"
            component={Routes.careeropportunitiesPage}
          />
          <Route
            path="/career-details/:id"
            component={Routes.careerdetailsPage}
          />
          <Route exact path="/faqs" component={Routes.faqsPage} />
          <Route
            path="/confirm-newsletter-subscription"
            component={Routes.cscPage}
          />
          <Route
            path="/payment-cancelled"
            component={Routes.paymentcancelledPage}
          />
          <Route
            path="/payment-successful"
            component={Routes.paymentsuccessfulPage}
          />
          <Route path="/thank-your-subscription" component={Routes.cscPage} />
          <Route path="/verifyuser" component={Routes.verifyUserRegistration} />
          <Route path="/resetPassword" component={Routes.resetPasswordPage} />
          <Route path="/unsubscribed" component={Routes.unsubscribedPage} />
          <Route exact path="/faq/:slug" component={Routes.faqDetailsPage} />
          {/* <Route exact path="/update/:title" component={Routes.updateMobEmail} /> */}

          {/* My profile dashboard routes */}
          <Route path="/myProfile/:dashboard" component={Routes.myProfile} />
          {typeof window !== "undefined" && location.pathname == "/myProfile" && localStorage.getItem("isAuth") !== "1" ? (<Redirect to="/" />) : (<Route path="/myprofile" component={Routes.myProfile} />)}

          {typeof window !== "undefined" ? (
            <Route
              path="/applications/:slug/instruction"
              exact
              //component={Routes.applicationInstructionContainer}
              component={() =>
                (window.location =
                  (prodEnv
                    ? applicationFormsUrl.prod
                    : applicationFormsUrl.qa) +
                  location.pathname +
                  "/" +
                  gblFunc.getRefreshToken() +
                  "/b4s")
              }
            />
          ) : (
              ""
            )}
          {/* {typeof window !== "undefined" ? (
          <Route
            path="/book-slot/:slug/:pageName"
            exact
            render={props => {
              const url = `${
                prodEnv ? applicationFormsUrl.prod : applicationFormsUrl.qa
              }${location.pathname}`;
              window.location = url;
            }}
          />
        ) : (
          ""
        )} */}
          {typeof window !== "undefined" ? (
            <Route
              path="/PCEM/SCE2/instruction"
              exact
              query={{ mode: "offline" }}
              render={props => {
                const url = `${applicationFormsUrl.prod}/PCEM/SCE2/instruction?mode=offline`;
                window.location = url;
              }}
            />
          ) : (
              ""
            )}
          {typeof window !== "undefined" ? (
            <Route
              path="/RSSM/CES6/instruction"
              exact
              query={{ mode: "offline" }}
              render={props => {
                const url = `${applicationFormsUrl.prod}/RSSM/CES6/instruction?mode=offline`;
                window.location = url;
              }}
            />
          ) : (
              ""
            )}
          {typeof window !== "undefined" ? (
            <Route
              path="/PCE/SSE4/instruction"
              render={props => {
                const url = `${PRODUCTION_URL}application/SSE4/instruction`;
                window.location = url;
              }}
            />
          ) : (
              ""
            )}
          {typeof window !== "undefined" ? (
            <Route
              path="/RSSM1/HUL1/instruction"
              render={props => {
                const url = `${PRODUCTION_URL}application/HUL1/instruction`;
                window.location = url;
              }}
            />
          ) : (
              ""
            )}
          {/* {typeof window !== "undefined" ? (
          <Route
            path="/application/:slug/:step/:no"
            exact
            component={() =>
              (window.location =
                (prodEnv ? applicationFormsUrl.prod : applicationFormsUrl.qa) +
                location.pathname +
                "/" +
                gblFunc.getRefreshToken() +
                "/b4s")
            }
          />
        ) : (
          ""
        )} */}
          {typeof window !== "undefined" ? (
            <Route
              path="/scholaryou"
              exact
              component={() =>
                (window.location =
                  (prodEnv
                    ? applicationFormsUrl.prod
                    : applicationFormsUrl.qa) + location.pathname)
              }
            />
          ) : (
              ""
            )}
          {typeof window !== "undefined" ? (
            <Route
              path="/blogcontest"
              exact
              component={() =>
                (window.location =
                  (prodEnv
                    ? applicationFormsUrl.prod
                    : applicationFormsUrl.qa) + location.pathname)
              }
            />
          ) : (
              ""
            )}
          {typeof window !== "undefined" ? (
            <Route
              path="/scholaryou/share"
              exact
              component={() =>
                (window.location =
                  (prodEnv
                    ? applicationFormsUrl.prod
                    : applicationFormsUrl.qa) + location.pathname)
              }
            />
          ) : (
              ""
            )}
          {typeof window !== "undefined" ? (
            <Route
              path="/product/:slug"
              exact
              component={() =>
                (window.location =
                  (prodEnv
                    ? applicationFormsUrl.prod
                    : applicationFormsUrl.qa) + location.pathname)
              }
            />
          ) : (
              ""
            )}
          {typeof window !== "undefined" ? (
            <Route
              path="/university-ucc"
              exact
              component={() =>
                (window.location =
                  (prodEnv
                    ? applicationFormsUrl.prod
                    : applicationFormsUrl.qa) + location.pathname)
              }
            />
          ) : (
              ""
            )}
          {typeof window !== "undefined" ? (
            <Route
              path="/blogcontest/share"
              exact
              component={() =>
                (window.location =
                  (prodEnv
                    ? applicationFormsUrl.prod
                    : applicationFormsUrl.qa) + location.pathname)
              }
            />
          ) : (
              ""
            )}
          {typeof window !== "undefined" ? (
            <Route
              path="/scholaryou/share"
              exact
              component={() =>
                (window.location =
                  (prodEnv
                    ? applicationFormsUrl.prod
                    : applicationFormsUrl.qa) + location.pathname)
              }
            />
          ) : (
              ""
            )}
          <Route
            exact
            path="/online-application-form"
            component={Routes.onlineApplicationFormlistPage}
          />
          <Route
            path="/book-slot/:bsid/registration"
            component={Routes.BookSlotRegistration}
          />
          <Route
            exact
            path="/scholarship-result"
            component={Routes.ScholarshipResultListPage}
          />
          <Route exact path="/collegeboard" component={Routes.collegeBoard} />
          <Route exact path="/colgate-scholarship" component={Routes.colgate} />
          <Route
            exact
            path="/payment/:bsid/collegeboard"
            component={Routes.collegeBoardPayment}
          />
          <Route
            exact
            path="/payment/:bsid/pearson"
            component={Routes.pearsonPayment}
          />
          <Route
            exact
            key={new Date()}
            path="/scholarship-result/:slug"
            component={Routes.ScholarshipResultDetailsPage}
          />
          <Route
            path="/application/:slug/instruction"
            exact
            component={Routes.applicationInstructionContainer}
          />
          <Route
            path="/user-info/:token?"
            component={Routes.UserInfoUpdate}
          />
          <Route
            path="/payment-acknowledgement"
            component={Routes.paymentAcknowledgement}
          />
          <Route path="/register"
            exact
            component={Routes.RegisterPage}
          />
          <PrivateRoute
            path="/application/:bsid/form"
            component={Routes.applicationFormContainer}
          />
          <PrivateRoute path="/scholar" component={Routes.scholar} />
          <PrivateRoute path="/subscribers" component={Routes.mySubscriber} />
          <PrivateRoute path="/myQnA" component={Routes.myQna} />
          <Route path="/forgot-password" component={Routes.forgotPassword} />
          <Route path="/educationloan" component={Routes.educationLoanPage} />
          <Route
            path="/international-study"
            component={Routes.studyAbroadPages}
          />
          <Route exact path="/csc/myprofile" component={Routes.myProfile} />
          <Route exact path="/csc/login" component={Routes.cscPage} />
          <Route exact path="/csc/payment-success" component={Routes.cscPage} />
          <Route exact path="/csc/payment-cancel" component={Routes.cscPage} />
          <Route path="/payment-complete" component={Routes.cscPage} />
          <Route path="/sahaj/login" component={Routes.sahajPage} />
          <PrivateRoute
            exact path="/vle/student-list"
            component={Routes.vleStudentList}
          />
          <PrivateRoute
            path="/vle/matched-scholarship"
            component={Routes.vleScholarship}
          />
          <PrivateRoute
            path="/vle/add-student"
            component={Routes.vleStudentList}
          />
          <PrivateRoute
            path="/vle/edit-student"
            component={Routes.vleStudentList}
          />
          <PrivateRoute
            path="/scholar-profile/:provName/:schId/:schName/:disNumber/:disbursalId"
            component={Routes.scholarProfile}
          />
          <Route exact path="/vle/payment-success" component={Routes.vlePayment} />
          <Route exact path="/vle/payment-cancel" component={Routes.vlePayment} />
          <Route exact path="/vle/download" component={Routes.vleStudentList} />
          <Route exact path="/vle/demo" component={Routes.vleStudentList} />
          <Route exact path="/vle/upgrade" component={Routes.vleStudentList} />
          <Route exact path="/vle/plans" component={Routes.vleStudentList} />
          <Route exact path="/vle/document" component={Routes.vleStudentList} />
          <Route
            path="/international-study"
            component={Routes.internationalStudy}
          />
          <Route
            path="/beneficiaries"
            component={Routes.beneficiaries}
          />

          <Route exact path="/qna" component={Routes.questionAnswer} />
          <Route exact path="/covid-19-support-initiative-for-daily-wage-workers-and-migrant-laborers" component={Routes.covid} />
          <Route
            exact
            key={new Date()}
            path="/qna/:routes"
            component={Routes.importRoutes}
          />
          <Route
            exact
            key={new Date()}
            path="/qna/:pathName/:routes"
            component={Routes.importRoutes}
          />
          <Route path="/page/:brandPage" component={Routes.brandPage} />
          <Route path="/kind-community" component={Routes.kindLanding} />
          <Route path="/content/:slug" component={Routes.contentPage} />
          <Route path="/ikigai-scholarship" component={Routes.ikiGai} />

          <Route
            path="/fair-and-lovely-career-foundation-nepal-scholarship"
            component={Routes.FAL9}
          />
          <Route
            path="/fair-and-lovely-career-foundation-myanmar-scholarship"
            component={Routes.FALMyanmar}
          />
          <Route
            path="/swiggy-result"
            component={Routes.swiggyResult}
          />
          <Route path="/videos" component={Routes.scholarshipVideo} />
          <Route
            path="/:slug/application-submitted"
            component={Routes.applicationShareLink}
          />
          <Redirect from="/offerings/corporates" to="/" />
          <Route path="/404" component={Routes.cscPage} />
          <Route path="/results" component={Routes.scholarResults} />
          <Redirect from="*" to="/404" />
        </Switch>
      </section>
      {location.pathname.substring(1, 14) === "educationloan" ||
        location.pathname.substring(1, 20) === "international-study" ||
        location.pathname.substring(1, 10) === "csc/login" ||
        location.pathname.substring(1, 12) === "sahaj/login" ||
        location.pathname.substring(1, 10) == "book-slot" ||
        location.pathname.substring(1, 13) == "applications" ||
        location.pathname.substring(1, 12) == "application" ||
        location.pathname.substring(1, 10) === "book-slot" ||
        location.pathname.substring(1, 13) === "collegeboard" ||
        location.pathname.substring(1, 20) === "colgate-scholarship" ||
        location.pathname.substring(1, 15) === "kind-community" ||
        location.pathname.substring(1, 8) === "summary" ||
        location.pathname.substring(1, 72) === "covid-19-support-initiative-for-daily-wage-workers-and-migrant-laborers" ||
        location.pathname.substring(1, 14) === "beneficiaries" ||
        location.pathname.substring(1, 21) === "international-study" ||
        location.pathname.substring(1, 21) == "scholarship-conclave" ||
        location.pathname.includes(`payment/`) ||
        location.pathname.includes(`page/`) ||
        location.pathname.includes("application-submitted") ||
        location.pathname.substring(1, 52) ==
        "fair-and-lovely-career-foundation-nepal-scholarship" ||
        location.pathname.substring(1, 54) ==
        "fair-and-lovely-career-foundation-myanmar-scholarship" ||
        location.pathname.includes(`payment/`) ||
        location.pathname.substring(1, 10) == "user-info" ||
        location.pathname.includes("application-submitted") ? (
          ""
        ) : (
          <Footer key={new Date()} />
        )}
    </div>
  );
});
export default App;
