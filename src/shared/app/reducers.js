import { combineReducers } from "redux";

import homeReducer from "../home/reducer";
import scholarshipReducer from "../scholarship/reducer";

const rootReducer = combineReducers({
  homeReducer: homeReducer,
  scholarship: scholarshipReducer
});
export default rootReducer;
