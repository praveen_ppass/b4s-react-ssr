import { all } from "redux-saga/effects";

import fetchScholarshipsSaga from "../scholarship/saga";
import fetchOnlineApplicationFormListSaga from "../online-application-form-list/saga";
import fetchTermsConditionsSaga from "../terms-conditions/saga";
import contactUsSaga from "../contact-us/saga";
import fetchPrivacyConditionSaga from "../privacy-policy/saga";
import fetchTeamSaga from "../team/saga";
import fetchCareerSaga from "../careers/saga";
import faqsSaga from "../faqs/saga";
import homeSaga from "../home/sagas";
import commonSaga from "../../constants/commonSaga";
import userProfileSaga from "../dashboard/saga";
import loginRegisterSaga from "../login/sagas";
import fetchMediaPartnerSaga from "../media-partners/saga";
import fetchMediaUrlSaga from "../media-url/saga";
import educationLoanSaga from "../education-loan/saga";
import fetchCSCSaga from "../csc/saga";
import bookSlotSaga from "../book-slot/saga";
import applicationReferenceSaga from "../application/sagas/applicationReferenceSaga";
import applicationQuestionSaga from "../application/sagas/applicationQuestionSaga";
import vleSaga from "../vle/saga";
import applicationInstructionsSaga from "../application/sagas/applicationInstructionsSaga";
import applicationEducationSaga from "../application/sagas/applicationEducationSaga";
import applicationFamilyEarningsSaga from "../application/sagas/applicationFamilyEarningsSaga";
import applicationFormSaga from "../application/sagas/applicationFormSaga";
import applicationSummerySaga from "../application/sagas/applicationSummerySaga";
import applicationAwardWonSags from "../application/sagas/applicationAwardWonSags";
import applicationDocumentSaga from "../application/sagas/applicationDocumentSaga";
import applicationPersonalInfoSaga from "../application/sagas/applicationPersonalInfoSaga";
import internationStudySaga from "../international-study/saga";
import applicationEntranceExamSaga from "../application/sagas/applicationEntranceExamSaga";
import interestFormSaga from "../interest-form/saga";
import footerBanner from "../banners/saga";
import fetchQuestAnswSaga from "../question-answer/saga";
import fetchScholarshipVideoSaga from "../scholarship-video/saga";
import fetchUserUnsubscribedSaga from "../user-unsubscribed/saga";
import brandPageSaga from "../brandPage/saga";
import brandPageSaga1 from '../colgate/saga';
import scholarConclaveSaga from "..//scholarship-conclave/saga";
import applicationPaymentSaga from "../applicationPayment/saga";
import applicationBankDtsSaga from "../application/sagas/applicationBankDetailsSaga";
import resultLookUpSaga from "../scholarResults/saga";
import hulLoginFormSaga from "../application/sagas/hulLoginFormSaga";
import scholarSaga from "../scholar/scholarSaga";
import ContentSaga from "../content/saga";
import DonateSaga from "../covid-19-donation/saga";
export default function* rootSaga() {
  yield all([
    fetchScholarshipsSaga(),
    fetchOnlineApplicationFormListSaga(),
    fetchTermsConditionsSaga(),
    contactUsSaga(),
    fetchPrivacyConditionSaga(),
    fetchTeamSaga(),
    faqsSaga(),
    homeSaga(),
    commonSaga(),
    loginRegisterSaga(),
    fetchCareerSaga(),
    userProfileSaga(),
    fetchMediaPartnerSaga(),
    fetchMediaUrlSaga(),
    educationLoanSaga(),
    fetchCSCSaga(),
    bookSlotSaga(),
    vleSaga(),
    applicationInstructionsSaga(),
    applicationReferenceSaga(),
    applicationQuestionSaga(),
    applicationEducationSaga(),
    applicationFamilyEarningsSaga(),
    applicationFormSaga(),
    applicationSummerySaga(),
    applicationAwardWonSags(),
    applicationDocumentSaga(),
    applicationPersonalInfoSaga(),
    internationStudySaga(),
    interestFormSaga(),
    applicationEntranceExamSaga(),
    footerBanner(),
    fetchUserUnsubscribedSaga(),
    fetchQuestAnswSaga(),
    fetchScholarshipVideoSaga(),
    brandPageSaga(),
    brandPageSaga1(),
    scholarConclaveSaga(),
    applicationPaymentSaga(),
    applicationBankDtsSaga(),
    resultLookUpSaga(),
    hulLoginFormSaga(),
    scholarSaga(),
    ContentSaga(),
   DonateSaga()
  ]);
}
