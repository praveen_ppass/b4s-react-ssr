import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  dependantRequests,
  getYearOrMonth
} from "../../../../constants/constants";
import {
  FETCH_STUDENT_DETAILS_SUCCESS,
  UPDATE_STUDENT_SUCCESS,
  EDIT_STUDENT_DETAILS_SUCCESS,
  UPDATE_STUDENT_FAIL
} from "../../actions";
import { ruleRunner } from "../../../../validation/ruleRunner";
import {
  required,
  isEmail,
  minLength,
  isNumeric,
  isMobileNumber,
  lengthRange,
  isGreaterThan,
  isLessThan
} from "../../../../validation/rules";
import Loader from "../../../common/components/loader";
import DatePicker from "react-datepicker";
import moment from "moment";
if (typeof window !== "undefined") {
  require("react-datepicker/dist/react-datepicker.css");
}
class Follow extends Component {
  constructor() {
    super();
    this.state = {
      isAni: true
    };
    this.socialIconHandler = this.socialIconHandler.bind(this);
  }
  socialIconHandler() {
    this.setState({
      isAni: !this.state.isAni
    });
  }

  componentWillReceiveProps(nextProps) { }

  componentDidMount() { }

  render() {
    return (
      <section className="qnaWrapper">
        <section className="container-fluid">
          <section className="container">
            <section className="row">
              <section className="col-md-9">
                <article className="col-xs-12 col-sm-12 col-md-12">
                  <article className="cateWrapper">
                    <h5>
                      {" "}
                      Which are the best colleges for a BSc (Hons) in physics
                      that also provide some scholarships for above 95%?{" "}
                    </h5>
                    <p>
                      {" "}
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Aenean euismod bibendum{" "}
                    </p>

                    <div className="left">
                      <h6>
                        {" "}
                        <span className="like">Liked by you</span> 5 Hours ago{" "}
                      </h6>
                    </div>
                    <i className="share-icon" onClick={this.socialIconHandler}>
                      <ul
                        className={
                          this.state.isAni
                            ? "social-icons hide"
                            : "social-icons show"
                        }
                      >
                        <li>
                          <a className="social-icon" href="">
                            <i className="fa fa-facebook" />
                          </a>
                        </li>
                        <li>
                          <a className="social-icon" href="">
                            <i className="fa fa-twitter" />
                          </a>
                        </li>
                        <li>
                          <a className="social-icon" href="">
                            <i className="fa fa-linkedin" />
                          </a>
                        </li>
                        <li>
                          <a className="social-icon" href="">
                            <i className="fa fa-instagram" />
                          </a>
                        </li>
                      </ul>
                    </i>
                  </article>
                </article>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default Follow;
