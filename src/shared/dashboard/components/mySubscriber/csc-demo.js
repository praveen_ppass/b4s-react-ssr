import React, { Component } from "react";

class CscDemo extends Component {
  // constructor(props) {
  //   super(props);
  // }
  render() {
    return (
      <article className="cscdemo">
        <section className="container">
          <article className="row">
            <article className="col-md-9">
              <h4 className="titletext marginTop">Tutorials</h4>
              <article className="col-md-12">
                <ul className="pdfDocDownloadList">
                  <li>
                    <a
                      href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/VLETutorials/Begum%20Hazrat%20Mahal_MAEF%20scholarship%20SOP.pdf"
                      target="_blank"
                      className="pdf"
                    >
                      Begum Hazrat Mahal_MAEF scholarship SOP
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/VLETutorials/SOP+for+Nagaland+Post+Matric+Scholarship+for+ST+Students.pdf"
                      target="_blank"
                      className="pdf"
                    >
                      SOP for Nagaland Post Matric Scholarship for ST Students
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/VLETutorials/SOP+for+Nagaland+State+Merit+based+Scholarship.pdf"
                      target="_blank"
                      className="pdf"
                    >
                      SOP for Nagaland State Merit based Scholarship
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/VLETutorials/SOP+for+Post+Matric+Madhya+Pradesh.pdf"
                      target="_blank"
                      className="pdf"
                    >
                      SOP for Post-Matric Scholarship of Madhya Pradesh
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/VLETutorials/SOP_NSP+(Autosaved).pdf"
                      target="_blank"
                      className="pdf"
                    >
                      SOP of MOMA Scholarships
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/VLETutorials/SOP_Verification_Beghum+Hazrat+Mahal.pdf"
                      target="_blank"
                      className="pdf"
                    >
                      SOP For The Verification of Beghum Hazrat Mahal
                      Scholarship
                    </a>
                  </li>

                  <li>
                    <a
                      href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/VLETutorials/Letter+to+School-Colleges-CSC.doc.docx"
                      target="_blank"
                      className="doc"
                    >
                      Letter to School Colleges CSC
                    </a>
                  </li>
                </ul>
              </article>
            </article>
          </article>
        </section>
      </article>
    );
  }
}

export default CscDemo;
