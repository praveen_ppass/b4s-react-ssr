import React, { Component } from "react";
import Loader from "../../../common/components/loader";
import gblFunc from "../../../../globals/globalFunctions";
import {
  FETCH_CSC_PAYMENT_DETAILS_SUCCESS,
  FETCH_CSC_REDIRECT_SUCCESS
} from "../../actions";

import Pagination from "rc-pagination";

class StudentList extends Component {
  constructor(props) {
    super(props);

    this.onViewStudentHandler = this.onViewStudentHandler.bind(this);
    this.state = {
      hideStudentList: true,
      paypop: false,
      payload: null,
      url: null,
      method: "POST",
      disableButton: false,
      ITEMS_PER_PAGE: 10,
      currentPage: 1
    };
    this.openpaypop = this.openpaypop.bind(this);
    this.closepaypop = this.closepaypop.bind(this);
    this.checkType = this.checkType.bind(this);
    this.onBackHandler = this.onBackHandler.bind(this);
    this.getStudentList = this.getStudentList.bind(this);
    this.onPageChangeHandler = this.onPageChangeHandler.bind(this);
  }

  openpaypop(userid = "", studentList) {
    const vleUserList = gblFunc.getStoreUserDetails();
    if (vleUserList.vleUser === "true") {
      this.setState(
        {
          disableButton: true
        },
        () =>
          this.props.fetchCscPaymentDt({
            amount: 118,
            cscId: +vleUserList.cscId,
            initiatorUserId: +vleUserList.userId,
            paidForUserId: +userid
          })
      );
    } else {
      this.setState({
        paypop: true,
        studentList
      });
    }
  }

  closepaypop() {
    this.setState({
      paypop: false
    });
  }

  componentDidMount() {
    const { ITEMS_PER_PAGE, currentPage } = this.state;
    let page = currentPage - 1;
    this.getStudentList({ page, length: ITEMS_PER_PAGE });
  }

  getStudentList({ page, length }) {
    this.props.fetchStudentList({
      userId: gblFunc.getStoreUserDetails()["userId"],
      page,
      length
    });
  }

  checkType(list) {}

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case FETCH_CSC_PAYMENT_DETAILS_SUCCESS:
        const { merchantId, payAddress, encText } = nextProps.cscPayment;

        const msg = `${merchantId}|${encText}`;
        const url = payAddress;
        const payload = new FormData();

        payload.append("message", msg);
        this.setState({
          url,
          payload: msg
        });

        setTimeout(() => {
          document.forms["cscForm"].submit();
        }, 1000);
        //this.props.redirectCSC({ url, formData: payload });
        break;
      case FETCH_CSC_REDIRECT_SUCCESS:
        window.location.href = this.props.cscPayment.payAddress;
      default:
        break;
    }
  }

  onViewStudentHandler(studentId) {
    this.setState(
      {
        hideStudentList: false
      },
      () =>
        this.props.fetchStudentDetails({
          userId: gblFunc.getStoreUserDetails()["userId"],
          studentId
        })
    );
  }
  onBackHandler(tabName, funName) {
    const { ITEMS_PER_PAGE, currentPage } = this.state;
    let page = currentPage - 1;
    this.setState(
      {
        hideStudentList: true
      },
      () => {
        this.props.fetchStudentList({
          userId: gblFunc.getStoreUserDetails()["userId"],
          page,
          length: ITEMS_PER_PAGE
        });
      }
    );
  }

  onPageChangeHandler(currentPage) {
    const { ITEMS_PER_PAGE } = this.state;
    let page = currentPage - 1;

    const length = ITEMS_PER_PAGE;

    this.getStudentList({
      page,
      length
    });
    this.setState({
      currentPage
    });
  }

  render() {
    const {
      studentList,
      ITEMS_PER_PAGE,
      currentPage,
      url,
      payload
    } = this.state;
    const vleUserList = gblFunc.getStoreUserDetails();
    const { studentData } = this.props;
    let stdList = null;
    let viewStudent = null;

    const totalStudents =
      this.props.studentData && this.props.studentData.total
        ? this.props.studentData.total
        : 0;

    if (studentData && studentData.data && studentData.data.length > 0) {
      stdList = studentData.data.map(list => (
        <tr>
          <td>
            <span title={list.email}>
              <a onClick={() => this.onViewStudentHandler(list.id)}>
                {list.firstName} {list.lastName}
              </a>
              <br />
              {list.email}
            </span>
          </td>
          <td>{list.academicClass.value}</td>
          <td>
            {vleUserList && vleUserList.vleUser === "true" ? (
              <span
                style={{
                  cursor: "pointer",
                  pointerEvents: list.membershipExpiry ? "visible" : "none"
                }}
                className="label label-primary recommend"
                onClick={() =>
                  this.props.studentMatchingSch(
                    list.id,
                    "MatchedScholarships",
                    true
                  )
                }
              >
                {`${list.matchedScholarship.full +
                  list.matchedScholarship.partial}`}
              </span>
            ) : (
              <span
                style={{
                  cursor: "pointer"
                }}
                className="label label-primary recommend"
                onClick={() =>
                  this.props.studentMatchingSch(
                    list.id,
                    "MatchedScholarships",
                    false
                  )
                }
              >
                {`${list.matchedScholarship.full +
                  list.matchedScholarship.partial}`}
              </span>
            )}
          </td>
          <td>
            {list.membershipExpiry ? (
              <span className="label label-primary">
                {list.membershipExpiry}
              </span>
            ) : (
              <p>NA</p>
            )}
          </td>
          <td>
            {/* <button className="btn btn-success hide">PAID</button> */}
            {list.membershipExpiry ? (
              <span className="label label-primary">PAID</span>
            ) : (
              <button
                className="payNowBtn"
                disabled={this.props.disableButton}
                onClick={() => this.props.openpaypop(list.id, list)}
              >
                Pay Now
              </button>
            )}
            {/* <article className="popupxx">
              <article className="popupsms">
                {this.state.paypop ? (
                  <PayNowPopUP
                    closepaypop={this.closepaypop}
                    studentList={this.state.studentList}
                  />
                ) : null}
              </article>
            </article> */}
          </td>
        </tr>
      ));
    }

    if (studentData && studentData["personalInfo"]) {
      viewStudent = (
        <section className="row">
          <section className="col-xs-12 col-sm-12 col-md-6 tblMob boxBg">
            <article className="box-primary">
              <article className="box-body box-profile">
                <h3 className="profile-username text-center">
                  {studentData["personalInfo"]["firstName"]}{" "}
                  {studentData["personalInfo"]["lastName"]}
                </h3>
                <p className="text-muted text-center">
                  {studentData["personalInfo"]["email"]}
                </p>
              </article>
            </article>
            <article className="box-primary">
              <article className="box-header with-border">
                <h3 className="box-title">About Me</h3>
              </article>
              <article className="box-body">
                {" "}
                <strong>
                  <i className="fa fa-book margin-r-5" /> Education
                </strong>
                <p className="text-muted">
                  {
                    studentData["educationalInfo"]["userAcademicInfo"][
                      "academicClass"
                    ].value
                  }
                </p>
                <hr />
                <strong>
                  <i className="fa fa-map-marker margin-r-5" /> Location
                </strong>
                <p className="text-muted">
                  {studentData["personalInfo"]["userAddress"]["state"].value}
                </p>
                <hr />
                <strong>
                  <i className="fa fa-pencil margin-r-5" /> Area of Interest
                </strong>
                {studentData["personalInfo"]["userRules"].map(list => {
                  if (list.ruleTypeValue == "special") {
                    return `${list.ruleValue}, `;
                  }
                })}
                <hr />
                <p>
                  <button
                    onClick={() =>
                      this.props.editDetailHandler(
                        studentData["personalInfo"]["id"],
                        this.props.userId
                      )
                    }
                    className="btn btn-danger"
                  >
                    Edit Details
                  </button>
                </p>
              </article>
            </article>
          </section>
          <section className="col-xs-12 col-sm-12 col-md-6 tblMob">
            <section className="table-responsive studentList border">
              <table className="table table-striped table-bordered table-hover">
                <tbody>
                  <tr>
                    <td>NAME</td>
                    <td>
                      {studentData["personalInfo"]["firstName"]}{" "}
                      {studentData["personalInfo"]["lastName"]}
                    </td>
                  </tr>
                  <tr>
                    <td>EMAIL</td>
                    <td>{studentData["personalInfo"]["email"]}</td>
                  </tr>
                  <tr>
                    <td>MOBILE</td>
                    <td>{studentData["personalInfo"]["mobile"]}</td>
                  </tr>
                  <tr>
                    <td>PASSING YEAR</td>
                    <td>
                      {
                        studentData["educationalInfo"]["userAcademicInfo"][
                          "passingYear"
                        ]
                      }
                    </td>
                  </tr>
                  <tr>
                    <td>PERCENTAGE</td>
                    <td>{studentData["personalInfo"]["profilePercentage"]}</td>
                  </tr>
                  <tr>
                    <td>SPECIAL</td>
                    {studentData["personalInfo"]["userRules"].map(list => {
                      if (list.ruleTypeValue == "special") {
                        return `${list.ruleValue}, `;
                      }
                    })}
                  </tr>
                  <tr>
                    <td>GENDER</td>
                    {studentData["personalInfo"]["userRules"].map(list => {
                      if (list.ruleTypeValue == "gender") {
                        return <td>{list.ruleValue}</td>;
                      }
                    })}
                  </tr>

                  <tr>
                    <td>QUOTA</td>
                    {studentData["personalInfo"]["userRules"].map(list => {
                      if (list.ruleTypeValue == "quota") {
                        return <td>{list.ruleValue}</td>;
                      }
                    })}
                  </tr>
                  <tr>
                    <td>Institute/School/College Name</td>
                    <td>
                      {
                        studentData["educationalInfo"]["userInstituteInfo"][
                          "instituteName"
                        ]
                      }
                    </td>
                  </tr>
                  <tr>
                    <td>STATE</td>
                    <td>
                      {
                        studentData["personalInfo"]["userAddress"]["state"]
                          .value
                      }
                    </td>
                  </tr>
                  <tr>
                    <td>SUBSCRIBER RELATION</td>
                    {studentData["personalInfo"]["userRules"].map(list => {
                      if (list.ruleTypeValue == "subscriber_relation") {
                        return <td>{list.ruleValue}</td>;
                      }
                    })}
                  </tr>
                  <tr>
                    <td>CLASS</td>
                    <td>
                      {
                        studentData["educationalInfo"]["userAcademicInfo"][
                          "academicClass"
                        ].value
                      }
                    </td>
                  </tr>
                  <tr>
                    <td>FAMILY INCOME</td>
                    <td>{studentData["personalInfo"]["familyIncome"]}</td>
                  </tr>
                  <tr>
                    <td>RELIGION</td>
                    {studentData["personalInfo"]["userRules"].map(list => {
                      if (list.ruleTypeValue == "religion") {
                        return <td>{list.ruleValue}</td>;
                      }
                    })}
                  </tr>
                </tbody>
              </table>
            </section>
          </section>
        </section>
      );
    }

    return (
      <section>
        <Loader isLoader={this.props.showLoader} />

        {this.state.hideStudentList ? (
          <section>
            {url && payload ? (
              <form
                style={{ display: "none" }}
                name="cscForm"
                id="cscForm"
                method="POST"
                action={url}
              >
                <input
                  type="hidden"
                  id="message"
                  name="message"
                  value={payload}
                />
              </form>
            ) : null}
            <h4 className="titletext marginTop">Student List </h4>
            <section className="row col msch">
              <section className="col-xs-12 col-sm-12 col-md-12 tblMob">
                {studentData &&
                studentData.data &&
                studentData.data.length > 0 ? (
                  <section className="table-responsive studentList">
                    <table className="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>Name/Email</th>
                          <th>Class</th>
                          <th>Recommended scholarships</th>
                          <th>Subscription valid till</th>
                          <th>Paid</th>
                        </tr>
                      </thead>
                      <tbody>{stdList}</tbody>
                    </table>
                  </section>
                ) : (
                  <p>NO STUDENT FOUND.</p>
                )}
              </section>
            </section>
            <article className="col-md-9 col-sm-9 col-xs-12 marginRight flex-container-list">
              <article className="pagination">
                {totalStudents > 10 ? (
                  <Pagination
                    defaultPageSize={ITEMS_PER_PAGE}
                    pageSize={ITEMS_PER_PAGE}
                    defaultCurrent={1}
                    current={currentPage}
                    onChange={this.onPageChangeHandler}
                    total={totalStudents}
                    showTitle={false}
                  />
                ) : null}
              </article>
            </article>
          </section>
        ) : (
          <section>
            <section>
              <h4 className="titletext marginTop">
                VIEW STUDENT DATA
                <span onClick={this.onBackHandler}>
                  <i className="fa fa-long-arrow-left" aria-hidden="true" />{" "}
                  Back
                </span>
              </h4>
            </section>
            {viewStudent}
          </section>
        )}

        {/* View Student Data */}
      </section>
    );
  }
}

export default StudentList;
