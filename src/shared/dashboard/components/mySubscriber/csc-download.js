import React, { Component } from "react";

class CscDownloads extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <article className="cscdownload">
        <h4 className="titletext marginTop">Download Creative </h4>
        <p>
          Use this ready-to-print poster to spread information about the program
          in your office, during seminars or other requirements.
        </p>
        <article className="col-md-12">
          <ul className="pdfDocDownloadList">
            <li>
              <a
                href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/VLETutorials/About+Buddy4Study.pdf"
                target="_blank"
                className="pdf"
              >
                About Buddy4Study
              </a>
            </li>
            <li>
              <a
                href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/VLETutorials/CSC+Leaflet.pdf "
                target="_blank"
                className="pdf"
              >
                CSC Leaflet
              </a>
            </li>
            <li>
              <a
                href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/VLETutorials/CSC+Whatsap+Banner.jpg"
                target="_blank"
                className="pdf"
              >
                CSC Whatsapp Banner
              </a>
            </li>

            <li>
              <a
                href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/VLETutorials/Poster.pdf"
                target="_blank"
                className="pdf"
              >
                CSC Poster
              </a>
            </li>
          </ul>
        </article>
        {/*  <a
          href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/vle-creatives.zip"
          className="btn"
        >
          Download Creative
        </a> */}
      </article>
    );
  }
}

export default CscDownloads;
