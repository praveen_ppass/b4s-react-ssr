import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { breadCrumObj } from "../../../../constants/breadCrum";
import { Helmet } from "react-helmet";
import BreadCrum from "../../../components/bread-crum/breadCrum";
import { dashboardFormContent } from "../importForm";
import SideNav from "../../../components/side-nav/sideNav";
import { formInputs } from "../../../../constants/formInput";
import {
  dependantRequests,
  imageFileExtension
} from "../../../../constants/constants";
import DashboardUI from "../../../components/dashboard/dashboardUI";
import DashboardNav from "../../../components/dashboard/dashboard-nav";
import ProfileImg from "../extras/profile_img";
import gblFunc from "../../../../globals/globalFunctions";
import {
  UPLOAD_USER_PIC_SUCCESS,
  FETCH_STUDENT_LIST_SUCCESS,
  FETCH_CSC_PAYMENT_DETAILS_SUCCESS,
  FETCH_CSC_REDIRECT_SUCCESS
} from "../../actions";
import { ruleRunner } from "../../../../validation/ruleRunner";
import {
  maxFileSize,
  hasValidImageExtenstion
} from "../../../../validation/rules";
import DashPayNowPopUP from "../extras/paynow";
import { FETCH_USER_RULES_SUCCESS } from "../../../../constants/commonActions";

class MySubscriber extends Component {
  constructor(props) {
    super(props);

    this.currentFormChangeHandler = this.currentFormChangeHandler.bind(this);
    this.mapUserApiToState = this.mapUserApiToState.bind(this);
    this.editDetailHandler = this.editDetailHandler.bind(this);
    this.studentMatchingSch = this.studentMatchingSch.bind(this);
    this.imagefileChangedHandler = this.imagefileChangedHandler.bind(this);
    this.clearSelectedFile = this.clearSelectedFile.bind(this);
    this.goToNextPage = this.goToNextPage.bind(this);
    this.updateUser = this.updateUser.bind(this);
    this.fileValidationHandler = this.fileValidationHandler.bind(this);
    this.setUserConfigHandler = this.setUserConfigHandler.bind(this);
    this.redirectToMembership = this.redirectToMembership.bind(this);
    this.onBackHandler = this.onBackHandler.bind(this);
    this.fullNameHandler = this.fullNameHandler.bind(this);
    this.state = {
      form: { tabName: "StudentList" },
      tab: "mySubscriber",
      isEdit: false,
      selectedFile: null,
      src: null,
      studentId: null,
      formInputs,
      userId: null,
      payload: null,
      url: null,
      method: "POST",
      disableButton: false,
      paypop: false,
      studentList: null,
      validations: {
        docFile: null
      },
      updateUserConfig: {
        fullName: null,
        fromRecommended: false,
        pic: null,
        userId: null,
        percentage: null
      },
      pool: false
    };

    this.openpaypop = this.openpaypop.bind(this);
    this.closepaypop = this.closepaypop.bind(this);
  }

  tabFormHandler(event, tabName) {
    event.preventDefault();

    let updateForm = { ...this.state, ...this.state.form, isEdit: false };

    updateForm.form["tabName"] = event.target.id;
    if (updateForm.form["tabName"] === "StudentList") {
      const { userId } = gblFunc.getStoreUserDetails();
      this.setState({
        ...updateForm,
        updateUserConfig: {
          ...this.state.updateUserConfig,
          userId,
          fromRecommended: false
        }
      });
    } else {
      this.setState(updateForm);
    }
  }
  fullNameHandler() {
    const {
      firstName,
      lastName,
      pic,
      userId,
      percentage
    } = gblFunc.getStoreUserDetails();

    this.setState({
      updateUserConfig: {
        fullName: `${firstName} ${lastName}`,
        pic,
        userId,
        percentage
      }
    });
  }

  componentWillMount() {
    const { userId } = gblFunc.getStoreUserDetails();

    if (
      this.props.location.state &&
      this.props.location.state.cscUser &&
      this.props.location.state.cscUser.userId
    ) {
      this.studentMatchingSch(
        this.props.location.state.cscUser.userId,
        "MatchedScholarships",
        true
      );
    } else {
      this.props.fetchUserRules({
        userid: userId
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    switch (type) {
      case UPLOAD_USER_PIC_SUCCESS:
        typeof window !== "undefined"
          ? localStorage.setItem("pic", nextProps.uploadPicData.location)
          : "";
        this.setState({
          updateUserConfig: {
            ...this.state.updateUserConfig,
            pic: nextProps.uploadPicData.location
          }
        });
        break;
      case FETCH_CSC_PAYMENT_DETAILS_SUCCESS:
        const { merchantId, payAddress, encText } = nextProps.cscPayment;

        const msg = `${merchantId}|${encText}`;
        const url = payAddress;
        const payload = new FormData();

        payload.append("message", msg);
        this.setState({
          url,
          payload: msg
        });

        setTimeout(() => {
          document.forms["cscForm"].submit();
        }, 1000);
        //this.props.redirectCSC({ url, formData: payload });
        break;
      case FETCH_CSC_REDIRECT_SUCCESS:
        window.location.href = this.props.cscPayment.payAddress;
        break;
      case FETCH_USER_RULES_SUCCESS:
        if (typeof window !== "undefined") {
          gblFunc.storeUserDetails(nextProps.userRulesData);
        }
        typeof window !== "undefined"
          ? localStorage.setItem(
            "percentage",
            nextProps.userRulesData &&
              nextProps.userRulesData.profilePercentage
              ? nextProps.userRulesData.profilePercentage
              : 0
          )
          : "";
        this.setState(
          {
            updateUserConfig: {
              ...this.state.updateUserConfig,
              percentage:
                nextProps.userRulesData &&
                  nextProps.userRulesData.profilePercentage
                  ? nextProps.userRulesData.profilePercentage
                  : 0
            }
          },
          () => this.fullNameHandler()
        );
        break;
      default:
        break;
    }
  }

  goToNextPage(tabName) {
    this.setState({
      form: { tabName }
    });
  }

  onSubmitHandler(event) { }

  currentFormChangeHandler(event, KEY, formName) {
    if (dependantRequests.includes(KEY)) {
      //TODO: Call dependant key request , pick from props in container
      this.props.makeDependantRequest(KEY, event.target.value);
    }
    // this.state.formInputs[this.state.form.tabName][KEY].value =
    //   event.target.value;

    let formIsValid = true;
    // for (let KEY in this.state.formInputs[this.state.form.tabName]) {
    //   formIsValid =
    //     this.state.formInputs[this.state.form.tabName][KEY].valid &&
    //     formIsValid;
    // }
    let currentForm = Object.assign({}, this.state.currentForm);
    currentForm[KEY] = event.target.value;
    this.setState({
      //input_form: this.state.formInputs[this.state.form.tabName],
      currentForm,
      formIsValid: formIsValid
    });
  }

  mapUserApiToState(USER, USER_RULE) {
    let setCurrentForm = { ...this.state.currentForm };

    for (let user_key in USER) {
      setCurrentForm[user_key] = USER[user_key];
    }
    for (let rule_key in USER_RULE) {
      setCurrentForm[rule_key.toUpperCase()] = USER_RULE[rule_key][0].ID;
    }
    this.setState({
      currentForm: setCurrentForm
    });
  }

  editDetailHandler(studentId, userId, isEdit) {
    this.setState(
      { form: { tabName: "AddStudent" }, studentId, isEdit: true },
      () =>
        this.props.fetchStudentDetails({
          userId,
          studentId: studentId
        })
    );
  }

  onBackHandler(tabName) {
    this.setState({
      form: {
        tabName
      }
    });
  }

  studentMatchingSch(userId, tabName, isVle) {
    const userConfig = { ...this.state.updateUserConfig };
    const userList = gblFunc.getStoreUserDetails();
    userConfig.userId = userId;
    userConfig.fromRecommended = true;
    this.setState({
      form: { tabName },
      updateUserConfig: userConfig,
      pool: isVle ? true : false
    });
  }

  imagefileChangedHandler(e) {
    let checkValidate = null;
    e.preventDefault();
    const { id } = e.target;

    let files;
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }
    const reader = new FileReader();
    reader.onload = () => {
      this.setState({ src: reader.result });
    };
    reader.readAsDataURL(files[0]);

    // Check Size In MB.
    const sizeInMb = Math.round(files[0].size / (1024 * 1024));
    const fileExtension = files[0].name.slice(files[0].name.lastIndexOf("."));

    if (fileExtension) {
      checkValidate = this.fileValidationHandler(
        fileExtension,
        id,
        "File",
        hasValidImageExtenstion(imageFileExtension),
        files
      );
      if (checkValidate) return;
    }

    if (sizeInMb > 2) {
      checkValidate = this.fileValidationHandler(
        sizeInMb,
        id,
        "Uploaded file",
        maxFileSize(2),
        files
      );

      if (checkValidate) return;
    }
  }

  fileValidationHandler(fileConfig, id, name, validationConfig, files) {
    let validationResult = null;
    validationResult = ruleRunner(fileConfig, id, name, validationConfig);

    const validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    this.setState({
      selectedFile: validations[id] ? null : files[0],
      validations
    });
    return validations[id] ? true : false;
  }

  clearSelectedFile() {
    this.setState({
      selectedFile: null
    });
  }

  updateUser({ firstName, lastName, percentage }) {
    this.setState({
      updateUserConfig: {
        ...this.state.updateUserConfig,
        fullName: `${firstName ? firstName : "Buddy"} ${
          lastName ? lastName : ""
          }`
      }
    });
  }

  componentWillMount() {
    this.setUserConfigHandler();
  }

  setUserConfigHandler() {
    const {
      firstName,
      lastName,
      pic,
      userId,
      percentage
    } = gblFunc.getStoreUserDetails();
    this.setState({
      updateUserConfig: {
        fullName: `${
          firstName != ("null" || "undefined" || "") ? firstName : "Buddy"
          } ${lastName != ("null" || "undefined" || "") ? lastName : ""}`,
        pic: pic,
        userId,
        percentage
      }
    });
  }

  openpaypop(userid = "", studentList) {
    const vleUserList = gblFunc.getStoreUserDetails();
    if (vleUserList.vleUser === "true") {
      this.setState(
        {
          disableButton: true
        },
        () =>
          this.props.fetchCscPaymentDt({
            amount: 118,
            cscId: +vleUserList.cscId,
            initiatorUserId: +vleUserList.userId,
            paidForUserId: +userid
          })
      );
    } else {
      this.setState({
        paypop: true,
        studentList
      });
    }
  }

  closepaypop() {
    this.setState({
      paypop: false
    });
  }

  redirectToMembership() {
    this.props.history.push("/premium-membership");
  }

  render() {
    const district = this.props.district ? this.props.district : [];
    const userUpdated = this.state.updateUserConfig;
    const userDetails = gblFunc.getStoreUserDetails();
    const userList = gblFunc.getStoreUserDetails();
    const isUserAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    if (!isUserAuthenticated) {
      return <Redirect to="/" />;
    }

    return (
      <section>
        <DashboardUI
          src={this.state.src}
          selectedFile={this.state.selectedFile}
          uploadPic={this.props.uploadPic}
          clearSelectedFile={this.clearSelectedFile}
          type={this.props.type}
          userId={userUpdated.userId}
        />
        {this.state.paypop ? (
          <DashPayNowPopUP
            closepaypop={this.closepaypop}
            studentList={this.state.studentList}
          />
        ) : null}

        <section className="dashboard">
          <Helmet> </Helmet>
          {/* <BreadCrum
            classes={breadCrumObj["mySubscribers"]["bgImage"]}
            listOfBreadCrum={breadCrumObj["mySubscribers"]["breadCrum"]}
            title={breadCrumObj["mySubscribers"]["title"]}
          /> */}

          <section className="conatiner-fluid">
            <article className="container dashboard-container">
              <ProfileImg
                imagefileChangedHandler={this.imagefileChangedHandler}
                userUpdated={userUpdated}
                userDetails={userDetails}
                validations={this.state.validations}
                redirectToMembership={this.redirectToMembership}
              />
              <article className="row">
                <article className="dashboard-nav-bg">
                  <article className="dashboard-nav-in ">
                    {/* SIDE NAV TABS*/}
                    <article className="col-md-12">
                      <DashboardNav dashNav="mysubscriber-tab" />
                    </article>

                    <SideNav
                      currentPage="dashboard"
                      tab={this.state.tab}
                      sideTabFormHandler={event => this.tabFormHandler(event)}
                      sideNavTabName={this.state.form.tabName}
                    />
                    <article className="col-md-9">
                      <article className="tab-border">
                        <FormContent
                          {...this.props}
                          tabName={this.state.form.tabName}
                          editDetailHandler={this.editDetailHandler}
                          isEdit={this.state.isEdit}
                          district={district}
                          studentMatchingSch={this.studentMatchingSch}
                          userId={userUpdated.userId}
                          pool={this.state.pool}
                          fromRecommended={userUpdated.fromRecommended}
                          goToNextPage={this.goToNextPage}
                          updateUser={this.updateUser}
                          disableButton={this.state.disableButton}
                          closepaypop={this.closepaypop}
                          openpaypop={this.openpaypop}
                          studentId={this.state.studentId}
                          onBackHandler={this.onBackHandler}
                          studentMatchList={this.props.studentMatchList}
                        />
                      </article>
                    </article>
                  </article>
                </article>
              </article>
            </article>
          </section>
        </section>
      </section>
    );
  }
}

const FormContent = props => {
  let navTopTab = props.tab;

  let FormOnTab = dashboardFormContent[props.tabName];

  return <FormOnTab {...props} />;
};

export default MySubscriber;
