import React, { Component } from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import Loader from "../../../common/components/loader";
import { imgBaseUrl } from "../../../../constants/constants";
import gblFunc from "../../../../globals/globalFunctions";

class MyFavorites extends Component {
	constructor(props) {
		super(props);
		this.onDeleteFavSch = this.onDeleteFavSch.bind(this);

		this.state = {
			ON_FIRST_LOAD: true,
			active: true
		};
	}
	componentDidMount() {
		// this.props.loadPrivacyPolicy();
		this.props.fetchMyFavScholarships({ userId: this.props.userId });
	}

	componentWillReceiveProps(nextProps) {
		const { favScholarships, deleteFavSch, type } = nextProps;

		try {
			if (
				type == "DELETE_FAV_SUCCESS" &&
				deleteFavSch
				//deleteFavSch.flgDeleted
			) {
				this.props.fetchMyFavScholarships({ userId: this.props.userId });
			}
		} catch (e) {
			this.setState({
				exception: e
			});
		}

		this.setState({
			ON_FIRST_LOAD: false
		});
	}

	onDeleteFavSch(schId) {
		this.props.deleteFavScholarships({
			userId: this.props.userId,
			scholarshipId: schId
		});
	}
	// rawMarkUp() {
	//     let rawMarkup = this.props.privacyPolicyList.privacyConditions
	//     return { __html: rawMarkup };
	// }

	render() {
		const { favScholarships } = this.props;

		return (
			<section>
				<Loader isLoader={this.props.showLoader} />
				<section className="row col">
					<section className="col-md-12 col-sm-12 schlarshipmatching flex-container-list">
						<section className="floatBox">
							<h4 className="titletext">MY FAVORITES</h4>
							<section>
								{favScholarships && favScholarships.length > 0 ? (
									<Myfavorite
										favs={favScholarships}
										activeScholarship={this.state.active}
										deleteFavSch={this.onDeleteFavSch}
									/>
								) : (
										"You have not added any scholarship to your favorites list."
									)}
							</section>
						</section>
					</section>
				</section>
			</section>
		);
	}
}

const Myfavorite = ({ favs, activeScholarship, deleteFavSch }) => {
	return favs.map(list => {
		let daysToGO;
		// let viewCounter;
		if (activeScholarship) {
			daysToGO =
				parseInt(
					moment(list.deadlineDate, "YYYY-MM-DD").diff(
						moment().startOf("day"),
						"days"
					)
				) + 1;
		}

		let daysLabel;
		if (daysToGO === 1) {
			daysLabel = "Last";
		} else if (daysToGO === 2) {
			daysLabel = "1";
		} else {
			daysLabel = daysToGO;
		}

		// if (
		//   list.scholarshipMultilinguals &&
		//   list.scholarshipMultilinguals[0] &&
		//   list.scholarshipMultilinguals[0].viewCounter
		// ) {
		//   viewCounter = list.scholarshipMultilinguals[0].viewCounter;
		// }
		return (
			<article className="box posR">
				<article className="data-row flex-container">
					<section className="col-md-2 col-sm-12 flex-item">
						<article className="tablecell">
							<article className="logotext">
								<Link to={`scholarship/${list.slug}`} className="ellipsis">
									<img
										alt={gblFunc.replaceWithLoreal(list.scholarshipName)}
										className="img-responsive"
										src={list.logoFid ? list.logoFid : ""}
									/>
								</Link>
								{/* <p>
                  {parseInt(viewCounter)
                    ? `${
                        parseInt(viewCounter) > 1
                          ? parseInt(viewCounter)
                          : "view"
                      } views`
                    : 0 + "view"}
                </p> */}
							</article>
						</article>
					</section>
					<section className="col-md-7 col-sm-9 flex-item">
						<article className="tablecell">
							<article className="contentdisplay">
								<h2>
									<Link to={`scholarship/${list.slug}`} className="ellipsis">
										<span
											dangerouslySetInnerHTML={{
												__html: list.scholarshipName
													? gblFunc.replaceWithLoreal(list.scholarshipName)
													: ""
											}}
										/>
									</Link>
									<article className="namelistTooltip">
										<span
											dangerouslySetInnerHTML={{
												__html: list.scholarshipName
													? gblFunc.replaceWithLoreal(list.scholarshipName)
													: ""
											}}
										/>
										<i className="arrow" />
									</article>
								</h2>
								<p>
									<img
										src={`${imgBaseUrl}scholarship-icon.png`}
										alt="buddy4study"
									/>
									{list.scholarshipMultilinguals.length > 0
										? list.scholarshipMultilinguals[0].applicableFor
										: ""}
								</p>
								<p>
									<img src={`${imgBaseUrl}awards-icon.png`} alt="buddy4study" />
									{list.scholarshipMultilinguals.length > 0
										? list.scholarshipMultilinguals[0].purposeAward
										: ""}
								</p>
							</article>
						</article>
					</section>

					<article className="col-md-3 col-sm-3 flex-item">
						<article className="tablecell">
							{daysToGO < 17 ? (
								moment().isSameOrBefore(list.deadlineDate, "day") ? (
									<article>
										<article
											className={daysToGO < 9 ? "daysgo pink" : "daysgo yellow"}
										>
											<span>{daysLabel}</span>
											<p>{`day${daysToGO > 2 ? "s" : ""} to go`}</p>
										</article>
										<span
											className="removeBtn"
											onClick={() => deleteFavSch(list.id)}
										>
											Remove X
                    </span>
									</article>
								) : (
										<CloseScholarship deleteFavSch={deleteFavSch} id={list.id} />
									)
							) : moment().isSameOrBefore(list.deadlineDate, "day") ? (
								<article>
									<article className="calender">
										<p className="text-center">Last Date to apply</p>
										<dd>{moment(list.deadlineDate).format("D")}</dd>
										<p className="text-center date">
											{moment(list.deadlineDate).format("MMM, YY")}
										</p>
									</article>

									<span
										className="removeBtn posBtn"
										onClick={() => deleteFavSch(list.id)}
									>
										Remove X
                  </span>
								</article>
							) : (
										<CloseScholarship deleteFavSch={deleteFavSch} id={list.id} />
									)}
						</article>
					</article>
				</article>
			</article>
		);
	});
};

export const CloseScholarship = ({ id, deleteFavSch }) => {
	return (
		<article>
			<article className="daysgo gray">
				<span>Close</span>
			</article>
			<span className="removeBtn" onClick={() => deleteFavSch(id)}>
				Remove X
      </span>
		</article>
	);
};

export default MyFavorites;
