import React, { Component, Fragment } from "react";
import { Link, Redirect } from "react-router-dom";
import querystring from "query-string";
import moment from "moment";
import gblFunc from "../../../../globals/globalFunctions";
import DocumentIssue from "../../components/myScholarship/documentIssues";
import AlertMessage from "../../../common/components/alertMsg";
import BookSlot from "../extras/book_slot";
import { messages } from "../../../../constants/constants";
import {
  FETCH_BOOK_SLOT_SUCCESS,
  UPDATE_BOOK_SLOT_SUCCESS,
  UPDATE_BOOK_SLOT_FAIL,
  FETCH_APPLICATION_STATUS_SUCCESS,
  FETCH_BOOK_SLOT_DATES_SUCCESS,
  FETCH_BOOK_SLOT_DATES_FAIL
} from "../../actions";
import { ruleRunner } from "../../../../validation/ruleRunner";
import {
  maxFileSize,
  hasValidImageExtenstion,
  required,
  isEmail,
  minLength,
  isNumeric,
  isMobileNumber
} from "../../../../validation/rules";
import Loader from "../../../common/components/loader";
class ApplicationStatus extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDocumentIssue: false,
      scholarshipName: '',
      showBookSlot: false,
      ON_FIRST_LOAD: true,
      isFormValid: false,
      isShowAlert: false,
      userId: null,
      scholarshipId: null,
      statusAlert: true,
      statusMsgAlert: false,
      alertMsg: "",
      validations: {
        comments: null,
        scheduleDate: null,
        scheduleTime: null,
        mobile: null,
        prefferedLanguages: null
      },
      book_slot: {
        id: null,
        comments: "",
        scheduleDate: "",
        scheduleTime: "",
        mobile: "",
        prefferedLanguages: [],
        scholarshipId: null,
        userId: null
      },
      preferredLanguage: [],
      isMsgPopupOpen: false,
      msgClosed: `The scholarship is closed for processing. You cannot upload your documents now. Please get in touch with <a href='mailto:info@buddy4study.com'>info@buddy4study.com</a> if you have any questions.`
    };
    this.bookSlotPopUpHandler = this.bookSlotPopUpHandler.bind(this);
    this.getBookSlotInfo = this.getBookSlotInfo.bind(this);
    this.closeBookSlot = this.closeBookSlot.bind(this);
    this.msgPopupHandler = this.msgPopupHandler.bind(this);
    this.hideMsgPopup = this.hideMsgPopup.bind(this);
    this.hidePopup = this.hidePopup.bind(this);
    this.urlPush = this.urlPush.bind(this);
    this.setScholarshipVal = this.setScholarshipVal.bind(this);
    this.currentBookSlotFormHandler = this.currentBookSlotFormHandler.bind(this);
    this.onUploadBookSlotHandler = this.onUploadBookSlotHandler.bind(this);
    this.closeBookSlot = this.closeBookSlot.bind(this);
  }

  componentDidMount() {
    this.props.fetchApplicationStatusDoc({ userId: this.props.userId });
  }

  closeBookSlot() {
    this.setState({
      showBookSlot: false,
      validations: {
        docFile: null,
        //comments: null,
        scheduleDate: null,
        scheduleTime: null,
        mobile: null,
        prefferedLanguages: null
      },
      book_slot: {
        id: null,
        comments: "",
        scheduleDate: "",
        scheduleTime: "",
        mobile: "",
        prefferedLanguages: [],
        scholarshipId: null,
        userId: null
      },
      bsid: null,
      preferredLanguage: []
    });
  }

  mapBookSlotApiToState(bookProps) {
    let updatePreferLang = [...this.state.preferredLanguage];
    const updateBooksSlot = { ...this.state.book_slot };
    if (
      bookProps &&
      bookProps.bookSlot &&
      Object.keys(bookProps.bookSlot).length > 0 &&
      this.state.ON_FIRST_LOAD
    ) {
      const { bookSlot } = bookProps;
      for (let key in bookProps.bookSlot) {
        updateBooksSlot[key] = bookProps.bookSlot[key];
        if (key == "prefferedLanguages") {
          updatePreferLang =
            bookProps.bookSlot[key] === null ? [] : bookProps.bookSlot[key];
        }
      }
    }
    this.setState({
      book_slot: updateBooksSlot,
      preferredLanguage: updatePreferLang
    });
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    const userList = gblFunc.getStoreUserDetails();
    switch (type) {
      case FETCH_APPLICATION_STATUS_SUCCESS:
        if (this.props.location.search) {
          const parsedQuery = querystring.parse(this.props.location.search);
          if (parsedQuery.tabName && parsedQuery.bsid) {
            if (parsedQuery.tabName === "app_status") {
              const { applStatusDoc } = nextProps;
              const appStatus = applStatusDoc.filter(
                appStatus => appStatus.bsid === parsedQuery.bsid
              );
              if (appStatus.length > 0 && appStatus[0].step === 4) {
                this.bookSlotPopUpHandler(
                  "Book Slot",
                  userList.userId,
                  appStatus[0].scholarshipId,
                  appStatus[0].bsid
                );
              }
            }
          }
        }
        break;
      case FETCH_BOOK_SLOT_SUCCESS:
        this.mapBookSlotApiToState(nextProps);
        this.props.fetchPreferLanguage("PREFERRED LANG");
        return;
      case UPDATE_BOOK_SLOT_SUCCESS:
        this.setState(
          {
            showBookSlot: false,
            isShowAlert: true,
            statusAlert: true,
            showButton: true,
            alertMsg: messages.bookSlot.success
          },
          () =>
            this.props.fetchApplicationStatus({
              userId: gblFunc.getStoreUserDetails()["userId"]
            })
        );
        return;
      case UPDATE_BOOK_SLOT_FAIL:
        // isShowAlert: true,
        // showModal: false,
        // showButton: true,
        this.setState({
          statusAlert: false,
          showButton: true,
          showBookSlot: false,
          isShowAlert: true,

          alertMsg: nextProps.updateError
        });
      case FETCH_BOOK_SLOT_DATES_SUCCESS:
        if (nextProps.bookSlotData) {
          const isExpired = moment.duration(
            moment(nextProps.bookSlotData.dateFrom, "YYYY-MM-DD").diff(
              moment().startOf("day")
            )
          )
            .asDays();
          if (isExpired >= 0) {
            this.setState({
              isShowAlert: true,
              statusAlert: false,
              alertMsg: `Slot booking for interviews will start from ${moment(nextProps.bookSlotData.dateFrom).format('LL')}.`,
              bookSlotDates: ""
            });
          } else {
            this.setState({ bookSlotDates: nextProps.bookSlotData });
          }
        }
        break;
      case FETCH_BOOK_SLOT_DATES_FAIL:
        this.setState({
          isShowAlert: true,
          statusAlert: false,
          alertMsg: "Interview slots for this scholarship are not yet opened. You will be informed over email about the dates once it is available.",
          bookSlotDates: ""
        });
        break;
    }
  }


  bookSlotPopUpHandler(name, userId, scholarshipId, bsid) {
    const flushValidation = {
      docFile: null,
      //comments: null,
      scheduleDate: null,
      scheduleTime: null,
      mobile: null,
      prefferedLanguages: null
    };
    if (
      ["Book Slot", "Slot Booked"].indexOf(name) === -1 &&
      typeof window !== "undefined"
    ) {
      const redirectURL = `/application/${bsid}/instruction`;
      this.props.history.push(redirectURL);
    }

    this.getBookSlotInfo(userId, scholarshipId);
    if (typeof window !== "undefined") {
      if (window.innerWidth <= 853) {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
      }
    }
    this.setState({
      showBookSlot: true,
      userId,
      bsid,
      scholarshipId,
      validations: flushValidation
    });
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "mobile":
        validationObject.name = "* Mobile";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;
      case "scheduleDate":
        validationObject.name = "* Schedule date";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "scheduleTime":
        validationObject.name = "* Schedule time";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "prefferedLanguages":
        validationObject.name = "* Preferred languages";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "comments":
        validationObject.name = "* Comments";
        validationObject.validationFunctions = [required];
        return validationObject;
    }
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      if (key != "docFile") {
        let { name, validationFunctions } = this.getValidationRulesObject(key);
        let validationResult = ruleRunner(
          key === "prefferedLanguages" && this.state.book_slot[key].length === 0
            ? ""
            : this.state.book_slot[key],
          key,
          name,
          ...validationFunctions
        );
        validations[key] = validationResult[key];
        if (validationResult[key] !== null) {
          isFormValid = false;
        }
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  getBookSlotInfo(userId, scholarshipId) {
    this.props.fetchBookSlotInfo({ userId, scholarshipId });
    this.props.fetchBookSlotDates(scholarshipId);
  }

  currentBookSlotFormHandler(e, key = "") {
    const { id, value } =
      key == "dob"
        ? {
          id: "scheduleDate",
          value:
            moment(e).format("DD-MM-YYYY") == "Invalid date"
              ? ""
              : moment(e).format("DD-MM-YYYY")
        }
        : e.target;

    const { book_slot, preferredLanguage } = { ...this.state };
    const validations = { ...this.state.validations };

    switch (id) {
      case "prefferedLanguages":
        if (
          preferredLanguage.indexOf(value) > -1 &&
          book_slot.prefferedLanguages.indexOf(value) > -1
        ) {
          preferredLanguage.splice(preferredLanguage.indexOf(value), 1);
          // book_slot.prefferedLanguages.splice(
          //   book_slot.prefferedLanguages.indexOf(value),
          //   1
          // );
        } else {
          preferredLanguage.push(value);
          // book_slot.prefferedLanguages.push(value);
        }
        break;
      case id:
        book_slot[id] = value;
        break;
    }

    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      const validationResult = ruleRunner(
        id === "prefferedLanguages" && preferredLanguage.length === 0
          ? ""
          : value,
        id,
        name,
        ...validationFunctions
      );

      validations[id] = validationResult[id];
    }

    this.setState({
      preferredLanguage,
      validations
    });
  }

  onUploadBookSlotHandler(event, scholarshipId) {
    event.preventDefault();
    const userList = gblFunc.getStoreUserDetails();
    const { book_slot, preferredLanguage } = { ...this.state };
    book_slot.userId = this.state.userId;
    book_slot.scholarshipId = this.state.scholarshipId;
    book_slot.prefferedLanguages = preferredLanguage;
    if (this.checkFormValidations()) {
      this.props.updatingBookSlot({
        userId: userList.userId,
        scholarshipId: this.state.scholarshipId,
        book_slot
      });
    }
  }

  closeBookSlot() {
    this.setState({
      showBookSlot: false,
      book_slot: {
        id: null,
        comments: "",
        scheduleDate: "",
        scheduleTime: "",
        mobile: "",
        prefferedLanguages: [],
        scholarshipId: null,
        userId: null
      }
    });
  }

  msgPopupHandler() {
    this.setState({
      isMsgPopupOpen: true
    })
  }

  hideMsgPopup() {
    this.setState({
      isMsgPopupOpen: false
    })
  }

  hidePopup() {
    this.setState({
      isShowAlert: false
    })
  }
  urlPush(urlPar) {
    this.props.history.push(
      `/myProfile/${urlPar}`
    );
  }

  setScholarshipVal(scholarshipId, scholarshipName) {
    typeof window !== 'undefined' && localStorage.setItem('scholarshipId', scholarshipId);
    typeof window !== 'undefined' && localStorage.setItem('scholarshipName', scholarshipName);
  }

  render() {
    const { isDocumentIssue, showBookSlot, bookSlotDates, isMsgPopupOpen, msgClosed, statusMsgAlert, statusAlert, alertMsg, isShowAlert, bsid, book_slot, validations } = this.state;
    const { applStatusDoc } = this.props;
    return (
      <Fragment>
        {this.state.showBookSlot && this.state.bookSlotDates ? (
          <BookSlot
            bookSlotData={this.state.book_slot}
            bsid={this.state.bsid}
            preferLang={this.props.preferLang}
            currentBookSlotFormHandler={this.currentBookSlotFormHandler}
            updateBookSlotHandler={this.onUploadBookSlotHandler}
            closeBookSlot={this.closeBookSlot}
            validations={this.state.validations}
            bookSlotDates={this.state.bookSlotDates}
          />
        ) : null}

        {isMsgPopupOpen && (<AlertMessage
          isShow={isMsgPopupOpen}
          msg={msgClosed}
          status={statusMsgAlert}
          close={this.hideMsgPopup}
        />)}
        <AlertMessage
          isShow={isShowAlert}
          msg={alertMsg}
          status={statusAlert}
          close={this.hidePopup}
        />

        <Loader isLoader={this.props.showLoader} />
        {isDocumentIssue ?
          <DocumentIssue /> : (
            <section className="row col">
              <section className="col-md-12 col-sm-12">
                <h4 className="titletext">APPLICATION STATUS</h4>
                <article className="col-md-12 activities">
                  {applStatusDoc && applStatusDoc.scholarshipApplyResponseList.length > 0 ? (
                    <article className="page-nav">
                      <ul>
                        <ApplicationStatusItem
                          applicationStatusList={applStatusDoc && applStatusDoc.scholarshipApplyResponseList}
                          bookSlotPopUpHandler={this.bookSlotPopUpHandler}
                          applicationStatusListOpt={this.applicationStatusListOpt}
                          msgPopupHandler={this.msgPopupHandler}
                          urlPush={this.urlPush}
                          setScholarshipVal={this.setScholarshipVal}
                        />
                      </ul>
                    </article>
                  ) : (
                      "You have not applied for any scholarship yet."
                    )}
                </article>
              </section>
            </section>
          )}
      </Fragment>
    );
  }
}

const ApplicationStatusItem = ({ applicationStatusList, bookSlotPopUpHandler, msgPopupHandler, urlPush, setScholarshipVal }) => {
  return applicationStatusList.map(list => {
    let dayToGo = "";
    if (list.submittedAt) {
      dayToGo = moment.unix(list.submittedAt / 1000, "YYYYMMDD").fromNow(); //submittedAt date format change like startDate    
    } else if (list.startDate) {
      dayToGo = moment.unix(list.startDate / 1000, "YYYYMMDD").fromNow();
    }
    return (
      <Status
        dayToGo={dayToGo}
        key={list.scholarshipId}
        list={list}
        onBookSlotPopUp={bookSlotPopUpHandler}
        msgPopupHandler={msgPopupHandler}
        urlPush={urlPush}
        setScholarshipVal={setScholarshipVal}
      />
    );
  });
};

const Status = ({ list, onBookSlotPopUp, dayToGo, msgPopupHandler, urlPush, setScholarshipVal }) => {
  const getStatus = {
    1: {
      statusName: "Not Submitted",
      colorCode: "color6"
    },
    2: {
      statusName: "Submitted",
      colorCode: "color2"
    },
    3: {
      statusName: "Submitted",
      colorCode: "color2"
    },
    4: {
      statusName: "Book Slot",
      colorCode: "color4"
    },
    5: {
      statusName: "Slot Booked",
      colorCode: "color5"
    },
    6: {
      statusName: "Completed",
      colorCode: "color5"
    },
    7: {
      statusName: "Completed",
      colorCode: "color5"
    },
    8: {
      statusName: "Awardee",
      colorCode: "color1"
    },
    9: {
      statusName: "Submitted",
      colorCode: "color2"
    },
    10: {
      statusName: "Submitted",
      colorCode: "color2"
    }
  };

  return (
    <li className={list.scholarshipStatus === 0 ? 'closedStatus' : getStatus[list.step] && getStatus[list.step].colorCode}>
      <p>{dayToGo}</p>
      <p
        dangerouslySetInnerHTML={{
          __html: gblFunc.replaceWithLoreal(list.scholarshipName)
        }}
      />
      <article className="activities-wrapper">
        <ul>
          <li>
            {list.scholarshipStatus === 0 ? (
              <dd onClick={msgPopupHandler}>Closed</dd>
            ) : (
                <dd onClick={() =>
                  onBookSlotPopUp(
                    getStatus[list.step].statusName,
                    list.userId,
                    list.scholarshipId,
                    list.bsid
                  )
                }
                  className={getStatus[list.step].statusName == "Completed" && "pointerEvent"}>{getStatus[list.step].statusName}</dd>
              )}

            {/* Document status active && scholarship status not closed */}
            {list.documentStatus !== null && list.documentStatus === 2 && list.scholarshipStatus === 1 && (<Link className="documentIssueBtn" onClick={() => [setScholarshipVal(list.scholarshipId, list.scholarshipName), this.props.history.push(<Redirect to="/myProfile/DocumentIssues" />)]} to="/myProfile/DocumentIssues"><i>Document Issue</i></Link>)}

            {/* Document status active && scholarship status closed */}
            {list.documentStatus !== null && list.documentStatus === 2 && list.scholarshipStatus === 0 && (<button className="defaultDocumentBtn" onClick={msgPopupHandler}><i>Document Issue</i></button>)}


          </li>
        </ul>
      </article>
    </li >
  );
};
export default ApplicationStatus;
