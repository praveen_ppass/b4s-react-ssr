import React, { Component } from "react";
import querystring from "query-string";
import moment from "moment";
import { Redirect } from "react-router-dom";
import { breadCrumObj } from "../../../../constants/breadCrum";
import { Helmet } from "react-helmet";
import BreadCrum from "../../../components/bread-crum/breadCrum";
import { dashboardFormContent } from "../importForm";
import SideNav from "../../../components/side-nav/sideNav";
import { formInputs } from "../../../../constants/formInput";
import {
  dependantRequests,
  imageFileExtension,
  prodEnv,
  applicationFormsUrl,
  upgradedApplicationBSID
} from "../../../../constants/constants";
import DashboardUI from "../../../components/dashboard/dashboardUI";
import DashboardNav from "../../../components/dashboard/dashboard-nav";
import ProfileImg from "../extras/profile_img";
import gblFunc from "../../../../globals/globalFunctions";
import { messages } from "../../../../constants/constants";
import {
  UPLOAD_USER_PIC_SUCCESS,
  FETCH_BOOK_SLOT_SUCCESS,
  UPDATE_BOOK_SLOT_SUCCESS,
  UPDATE_BOOK_SLOT_FAIL,
  FETCH_APPLICATION_STATUS_SUCCESS,
  FETCH_BOOK_SLOT_DATES_SUCCESS,
  FETCH_BOOK_SLOT_DATES_FAIL
} from "../../actions";
import { ruleRunner } from "../../../../validation/ruleRunner";
import {
  maxFileSize,
  hasValidImageExtenstion,
  required,
  isEmail,
  minLength,
  isNumeric,
  isMobileNumber
} from "../../../../validation/rules";
import BookSlot from "../extras/book_slot";
import AlertMessage from "../../../common/components/alertMsg";
import { FETCH_USER_RULES_SUCCESS } from "../../../../constants/commonActions";

class MyScholarship extends Component {
  constructor(props) {
    super(props);

    this.currentFormChangeHandler = this.currentFormChangeHandler.bind(this);
    this.imagefileChangedHandler = this.imagefileChangedHandler.bind(this);
    this.clearSelectedFile = this.clearSelectedFile.bind(this);
    this.updateUser = this.updateUser.bind(this);
    this.swtichTab = this.swtichTab.bind(this);
    this.fileValidationHandler = this.fileValidationHandler.bind(this);

    this.getBookSlotInfo = this.getBookSlotInfo.bind(this);
    this.bookSlotPopUpHandler = this.bookSlotPopUpHandler.bind(this);
    this.currentBookSlotFormHandler = this.currentBookSlotFormHandler.bind(
      this
    );
    this.onUploadBookSlotHandler = this.onUploadBookSlotHandler.bind(this);
    this.closeBookSlot = this.closeBookSlot.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.redirectToMembership = this.redirectToMembership.bind(this);
    this.fullNameHandler = this.fullNameHandler.bind(this);
    this.state = {
      form: { tabName: "MatchedScholarships" },
      tab: "profile",
      selectedFile: null,
      applicationStatus: null,
      src: null,
      formInputs,
      userId: null,
      isShowAlert: false,
      statusAlert: true,
      showBookSlot: false,
      ON_FIRST_LOAD: true,
      alertMsg: "",
      validations: {
        docFile: null,
        //comments: null,
        scheduleDate: null,
        scheduleTime: null,
        mobile: null,
        prefferedLanguages: null
      },
      book_slot: {
        id: null,
        comments: "",
        scheduleDate: "",
        scheduleTime: "",
        mobile: "",
        prefferedLanguages: [],
        scholarshipId: null,
        userId: null
      },
      preferredLanguage: [],
      updateUserConfig: {
        fullName: null,
        pic: null,
        userId: null,
        percentage: null
      },
      bsid: null
    };
  }

  bookSlotPopUpHandler(name, userId, scholarshipId, bsid) {
    const flushValidation = {
      docFile: null,
      //comments: null,
      scheduleDate: null,
      scheduleTime: null,
      mobile: null,
      prefferedLanguages: null
    };
    if (
      ["Book Slot", "Slot Booked"].indexOf(name) === -1 &&
      typeof window !== "undefined"
    ) {
      const redirectURL = `/application/${bsid}/instruction`;
      this.props.history.push(redirectURL);
    }

    this.getBookSlotInfo(userId, scholarshipId);
    if (typeof window !== "undefined") {
      if (window.innerWidth <= 853) {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
      }
    }
    this.setState({
      showBookSlot: true,
      userId,
      bsid,
      scholarshipId,
      validations: flushValidation
    });
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "mobile":
        validationObject.name = "* Mobile";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;
      case "scheduleDate":
        validationObject.name = "* Schedule date";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "scheduleTime":
        validationObject.name = "* Schedule time";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "prefferedLanguages":
        validationObject.name = "* Preferred languages";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "comments":
        validationObject.name = "* Comments";
        validationObject.validationFunctions = [required];
        return validationObject;
    }
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      if (key != "docFile") {
        let { name, validationFunctions } = this.getValidationRulesObject(key);
        let validationResult = ruleRunner(
          key === "prefferedLanguages" && this.state.book_slot[key].length === 0
            ? ""
            : this.state.book_slot[key],
          key,
          name,
          ...validationFunctions
        );
        validations[key] = validationResult[key];
        if (validationResult[key] !== null) {
          isFormValid = false;
        }
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  getBookSlotInfo(userId, scholarshipId) {
    this.props.fetchBookSlotInfo({ userId, scholarshipId });
    this.props.fetchBookSlotDates(scholarshipId);
  }

  currentBookSlotFormHandler(e, key = "") {
    const { id, value } =
      key == "dob"
        ? {
          id: "scheduleDate",
          value:
            moment(e).format("DD-MM-YYYY") == "Invalid date"
              ? ""
              : moment(e).format("DD-MM-YYYY")
        }
        : e.target;

    const { book_slot, preferredLanguage } = { ...this.state };
    const validations = { ...this.state.validations };

    switch (id) {
      case "prefferedLanguages":
        if (
          preferredLanguage.indexOf(value) > -1 &&
          book_slot.prefferedLanguages.indexOf(value) > -1
        ) {
          preferredLanguage.splice(preferredLanguage.indexOf(value), 1);
          // book_slot.prefferedLanguages.splice(
          //   book_slot.prefferedLanguages.indexOf(value),
          //   1
          // );
        } else {
          preferredLanguage.push(value);
          // book_slot.prefferedLanguages.push(value);
        }
        break;
      case id:
        book_slot[id] = value;
        break;
    }

    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      const validationResult = ruleRunner(
        id === "prefferedLanguages" && preferredLanguage.length === 0
          ? ""
          : value,
        id,
        name,
        ...validationFunctions
      );

      validations[id] = validationResult[id];
    }

    this.setState({
      preferredLanguage,
      validations
    });
  }

  onUploadBookSlotHandler(event, scholarshipId) {
    event.preventDefault();
    const userList = gblFunc.getStoreUserDetails();
    const { book_slot, preferredLanguage } = { ...this.state };
    book_slot.userId = this.state.userId;
    book_slot.scholarshipId = this.state.scholarshipId;
    book_slot.prefferedLanguages = preferredLanguage;
    if (this.checkFormValidations()) {
      this.props.updatingBookSlot({
        userId: userList.userId,
        scholarshipId: this.state.scholarshipId,
        book_slot
      });
    }
  }

  closeBookSlot() {
    this.setState({
      showBookSlot: false,
      book_slot: {
        id: null,
        comments: "",
        scheduleDate: "",
        scheduleTime: "",
        mobile: "",
        prefferedLanguages: [],
        scholarshipId: null,
        userId: null
      }
    });
  }

  validateTabFromApplicationForm() {
    if (
      gblFunc.getParameterByName("tab") != null &&
      gblFunc.getParameterByName("tab") != undefined
    ) {
      if (gblFunc.getParameterByName("tab") == "application-status") {
        this.setState({
          form: { tabName: "ApplicationStatus" }
        });
      }
    }
  }

  componentDidMount() {
    this.swtichTab();
    const { userId } = gblFunc.getStoreUserDetails();

    this.props.fetchUserRules({
      userid: userId
    });
    this.validateTabFromApplicationForm();
  }

  swtichTab() {
    const { action, location } = this.props.history;
    if (this.props.location.search) {
      const parsedQuery = querystring.parse(this.props.location.search);
      if (parsedQuery.tabName && parsedQuery.bsid) {
        if (parsedQuery.tabName === "app_status") {
          this.setState({
            form: { tabName: "ApplicationStatus" }
          });
        }
      }
    }

    if (action == "PUSH" && location.state && location.state.switchTab) {
      this.setState({
        form: { tabName: location.state.switchTab }
      });
    }
    if (
      action == "REPLACE" &&
      location.state &&
      location.state.switchTab === "Redirect-To-ApplicationStatus"
    ) {
      this.setState({
        form: {
          tabName: "ApplicationStatus"
        }
      });
    }
  }

  mapBookSlotApiToState(bookProps) {
    let updatePreferLang = [...this.state.preferredLanguage];
    const updateBooksSlot = { ...this.state.book_slot };
    if (
      bookProps &&
      bookProps.bookSlot &&
      Object.keys(bookProps.bookSlot).length > 0 &&
      this.state.ON_FIRST_LOAD
    ) {
      const { bookSlot } = bookProps;
      for (let key in bookProps.bookSlot) {
        updateBooksSlot[key] = bookProps.bookSlot[key];
        if (key == "prefferedLanguages") {
          updatePreferLang =
            bookProps.bookSlot[key] === null ? [] : bookProps.bookSlot[key];
        }
      }
    }
    this.setState({
      book_slot: updateBooksSlot,
      preferredLanguage: updatePreferLang
    });
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    const userList = gblFunc.getStoreUserDetails();
    switch (type) {
      case FETCH_APPLICATION_STATUS_SUCCESS:
        if (this.props.location.search) {
          const parsedQuery = querystring.parse(this.props.location.search);
          if (parsedQuery.tabName && parsedQuery.bsid) {
            if (parsedQuery.tabName === "app_status") {
              const { applStatus } = nextProps;
              const appStatus = applStatus.filter(
                appStatus => appStatus.bsid === parsedQuery.bsid
              );
              if (appStatus.length > 0 && appStatus[0].step === 4) {
                this.bookSlotPopUpHandler(
                  "Book Slot",
                  userList.userId,
                  appStatus[0].scholarshipId,
                  appStatus[0].bsid
                );
              }
            }
          }
        }
        break;
      case UPLOAD_USER_PIC_SUCCESS:
        typeof window !== "undefined"
          ? localStorage.setItem("pic", nextProps.uploadPicData.location)
          : "";
        this.setState({
          updateUserConfig: {
            ...this.state.updateUserConfig,
            pic: nextProps.uploadPicData.location
          }
        });
      case FETCH_BOOK_SLOT_SUCCESS:
        this.mapBookSlotApiToState(nextProps);
        this.props.fetchPreferLanguage("PREFERRED LANG");
        return;
      case UPDATE_BOOK_SLOT_SUCCESS:
        this.setState(
          {
            showBookSlot: false,
            isShowAlert: true,
            statusAlert: true,
            showButton: true,
            alertMsg: messages.bookSlot.success
          },
          () =>
            this.props.fetchApplicationStatus({
              userId: gblFunc.getStoreUserDetails()["userId"]
            })
        );
        return;
      case UPDATE_BOOK_SLOT_FAIL:
        // isShowAlert: true,
        // showModal: false,
        // showButton: true,
        this.setState({
          statusAlert: false,
          showButton: true,
          showBookSlot: false,
          isShowAlert: true,

          alertMsg: nextProps.updateError
        });
      case FETCH_USER_RULES_SUCCESS:
        if (typeof window !== "undefined") {
          gblFunc.storeUserDetails(nextProps.userRulesData);
        }
        typeof window !== "undefined"
          ? localStorage.setItem(
            "percentage",
            nextProps.userRulesData &&
              nextProps.userRulesData.profilePercentage
              ? nextProps.userRulesData.profilePercentage
              : 0
          )
          : "";
        this.setState(
          {
            updateUserConfig: {
              ...this.state.updateUserConfig,
              percentage:
                nextProps.userRulesData &&
                  nextProps.userRulesData.profilePercentage
                  ? nextProps.userRulesData.profilePercentage
                  : 0
            }
          },
          () => this.fullNameHandler()
        );
        break;
      case FETCH_BOOK_SLOT_DATES_SUCCESS:
        if (nextProps.bookSlotData) {
          const isExpired = moment.duration(
            moment(nextProps.bookSlotData.dateFrom, "YYYY-MM-DD").diff(
              moment().startOf("day")
            )
          )
            .asDays();
          if (isExpired >= 0) {
            this.setState({
              isShowAlert: true,
              statusAlert: false,
              alertMsg: `Slot booking for interviews will start from ${moment(nextProps.bookSlotData.dateFrom).format('LL')}.`,
              bookSlotDates: ""
            });
          } else {
            this.setState({ bookSlotDates: nextProps.bookSlotData });
          }
        }
        break;
      case FETCH_BOOK_SLOT_DATES_FAIL:
        this.setState({
          isShowAlert: true,
          statusAlert: false,
          alertMsg: "Interview slots for this scholarship are not yet opened. You will be informed over email about the dates once it is available.",
          bookSlotDates: ""
        });
        break;
    }
  }

  componentDidUpdate(nextProps, nextState) {
    const { location } = this.props.history;
    if (nextProps.location.key != location.key) {
      this.swtichTab();
    }
  }

  currentFormChangeHandler(event, KEY, formName) {
    if (dependantRequests.includes(KEY)) {
      //TODO: Call dependant key request , pick from props in container
      this.props.makeDependantRequest(KEY, event.target.value);
    }
    // this.state.formInputs[this.state.form.tabName][KEY].value =
    //   event.target.value;
  }

  tabFormHandler(event, tabName) {
    event.preventDefault();
    let updateForm = { ...this.state, ...this.state.form };

    updateForm.form["tabName"] = event.target.id;
    this.setState(updateForm);
  }

  imagefileChangedHandler(e) {
    let checkValidate = null;
    e.preventDefault();
    const { id } = e.target;

    let files;
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }
    const reader = new FileReader();
    reader.onload = () => {
      this.setState({ src: reader.result });
    };
    reader.readAsDataURL(files[0]);

    // Check Size In MB.
    const sizeInMb = Math.round(files[0].size / (1024 * 1024));
    const fileExtension = files[0].name.slice(files[0].name.lastIndexOf("."));

    if (fileExtension) {
      checkValidate = this.fileValidationHandler(
        fileExtension,
        id,
        "File",
        hasValidImageExtenstion(imageFileExtension),
        files
      );
      if (checkValidate) return;
    }

    if (sizeInMb > 2) {
      checkValidate = this.fileValidationHandler(
        sizeInMb,
        id,
        "Uploaded file",
        maxFileSize(2),
        files
      );

      if (checkValidate) return;
    }
  }

  fileValidationHandler(fileConfig, id, name, validationConfig, files) {
    let validationResult = null;
    validationResult = ruleRunner(fileConfig, id, name, validationConfig);

    const validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    this.setState({
      selectedFile: validations[id] ? null : files[0],
      validations
    });
    return validations[id] ? true : false;
  }

  clearSelectedFile() {
    this.setState({
      selectedFile: null
    });
  }

  updateUser({ firstName, lastName, percentage }) {
    this.setState({
      updateUserConfig: {
        ...this.state.updateUserConfig,
        fullName: `${firstName ? firstName : "Buddy"} ${
          lastName ? lastName : ""
          }`
      }
    });
  }

  hideAlert() {
    this.setState({
      isShowAlert: false,
      book_slot: {
        id: null,
        comments: "",
        scheduleDate: "",
        scheduleTime: "",
        mobile: "",
        prefferedLanguages: [],
        scholarshipId: null,
        userId: null
      }
    });
  }
  fullNameHandler() {
    const {
      firstName,
      lastName,
      pic,
      userId,
      percentage
    } = gblFunc.getStoreUserDetails();

    this.setState({
      updateUserConfig: {
        fullName: `${firstName} ${lastName}`,
        pic,
        userId,
        percentage
      }
    });
  }

  componentWillMount() {
    const {
      firstName,
      lastName,
      pic,
      userId,
      percentage
    } = gblFunc.getStoreUserDetails();
    this.setState({
      updateUserConfig: {
        fullName: `${
          firstName != ("null" || "undefined" || "") ? firstName : "Buddy"
          } ${lastName != ("null" || "undefined" || "") ? lastName : ""}`,
        pic: pic,
        userId,
        percentage
      }
    });
  }

  redirectToMembership() {
    this.props.history.push("/premium-membership");
  }

  render() {
    const district = this.props.district ? this.props.district : [];
    const userUpdated = this.state.updateUserConfig;
    const userDetails = gblFunc.getStoreUserDetails();
    const isUserAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;

    if (!isUserAuthenticated) {
      return <Redirect to="/" />;
    }
    return (
      <section>
        {/* {this.state.showBookSlot && this.state.bookSlotDates ? (
          <BookSlot
            bookSlotData={this.state.book_slot}
            bsid={this.state.bsid}
            preferLang={this.props.preferLang}
            currentBookSlotFormHandler={this.currentBookSlotFormHandler}
            updateBookSlotHandler={this.onUploadBookSlotHandler}
            closeBookSlot={this.closeBookSlot}
            validations={this.state.validations}
            bookSlotDates={this.state.bookSlotDates}
          />
        ) : null} */}
        <AlertMessage
          isShow={this.state.isShowAlert}
          msg={this.state.alertMsg}
          status={this.state.statusAlert}
          close={this.hideAlert}
        />
        <DashboardUI
          src={this.state.src}
          selectedFile={this.state.selectedFile}
          uploadPic={this.props.uploadPic}
          clearSelectedFile={this.clearSelectedFile}
          type={this.props.type}
          userId={userUpdated.userId}
        />
        <section className="dashboard">
          <Helmet> </Helmet>

          {/* <BreadCrum
            classes={breadCrumObj["myScholarships"]["bgImage"]}
            listOfBreadCrum={breadCrumObj["myScholarships"]["breadCrum"]}
            title={breadCrumObj["myScholarships"]["title"]}
          /> */}

          <section className="conatiner-fluid">
            <article className="container dashboard-container">
              <ProfileImg
                imagefileChangedHandler={this.imagefileChangedHandler}
                userUpdated={userUpdated}
                userDetails={userDetails}
                validations={this.state.validations}
                redirectToMembership={this.redirectToMembership}
              />
              <article className="row">
                <article className="dashboard-nav-bg">
                  <article className="dashboard-nav-in ">
                    <article className="col-md-12">
                      <DashboardNav dashNav="My Scholarships" />
                    </article>

                    {/* SIDE NAV TABS*/}
                    <SideNav
                      currentPage="dashboard"
                      tab={this.state.tab}
                      sideTabFormHandler={event => this.tabFormHandler(event)}
                      sideNavTabName={this.state.form.tabName}
                    />
                    <article className="col-md-9">
                      <article className="tab-border">
                        <FormContent
                          {...this.props}
                          tabName={this.state.form.tabName}
                          editDetailHandler={this.editDetailHandler}
                          district={district}
                          userId={userUpdated.userId}
                          updateUser={this.updateUser}
                          bookSlotPopUpHandler={this.bookSlotPopUpHandler}
                        />
                      </article>
                    </article>
                  </article>
                </article>
              </article>
            </article>
          </section>
        </section>
      </section>
    );
  }
}

const FormContent = props => {
  let navTopTab = props.tab;

  let FormOnTab = dashboardFormContent[props.tabName];

  return <FormOnTab {...props} />;
};

export default MyScholarship;
