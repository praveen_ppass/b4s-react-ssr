import React, { Component, Fragment } from 'react';
import moment from "moment";
import gblFunc from "../../../../globals/globalFunctions";
import {
	FETCH_AWARDEE_DOC_REQUEST,
	FETCH_AWARDEE_DOC_SUCCESS,
	FETCH_AWARDEE_DOC_FAIL,
	FETCH_DOCUMENT_ISSUE_UPLOAD_SUCCESS,

	//Disbursal Acknowledgement
	POST_DISBURSAL_ACK_REQUESTED,
	POST_DISBURSAL_ACK_SUCCEEDED,
	POST_DISBURSAL_ACK_FAILED
} from "../../actions";

import Loader from "../../../common/components/loader";
import AlertMessage from "../../../common/components/alertMsg";
import Cropper from "react-cropper";
if (typeof window !== "undefined") {
	require("cropperjs/dist/cropper.css");
}
class scholarshipApplyAwardeeList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			awardeeDocData: null,
			isMore: false,
			fileName: "",
			id: "",
			isAlertShow: false,
			alertStatus: "",
			alertMsg: "",
			disbursalAcknowledgementData: null,
			isShowCropPup: false,
			cropSrc: "",
			cropResult: null,
			stringifiedData: null,
			scholarshipIdDoc: null,
			disbursementCycleId:null
		};
		this.moreHandler = this.moreHandler.bind(this);
		this.documentUploadIssue = this.documentUploadIssue.bind(this);
		this.closePopup = this.closePopup.bind(this);
		this.disbursalAcknowledgementCycle = this.disbursalAcknowledgementCycle.bind(this);
		this.cropImage = this.cropImage.bind(this);
		this.dataURLtoFile = this.dataURLtoFile.bind(this);
		
	}

	cropImage(docObj) {
		if (typeof this.cropper.getCroppedCanvas() === "undefined") {
			return;
		}

		const selectedFile = this.state.docFile;

		let customizeFileName =
			selectedFile.name.substr(0, selectedFile.name.lastIndexOf(".")) +
			selectedFile.name
				.substr(selectedFile.name.lastIndexOf("."))
				.toLowerCase();

		var file = this.dataURLtoFile(
			this.cropper
				.getCroppedCanvas({ width: 200, height: 200 })
				.toDataURL("image/jpeg", 0.8),
			customizeFileName
		);

		let reader = new FileReader(); // read upload file
		reader.readAsDataURL(file);
		this.setState(
			{
				cropResult: file,
				isShowCropPup: false
			}
			, () =>
				this.props.fetchDocumentUploadIssue({
					docFile: file,
					scholarshipId: this.state.scholarshipIdDoc,
					userId: this.props.userId,
					userDocRequest: this.state.stringifiedData,
					disbursementCycleId: this.state.disbursementCycleId,
					
				})
			//   () =>
			//    this.uploadDocumentWithOutCrop(docObj, Array(file))
		);

		this.setState({ fileName: file.name, id: this.state.stringifiedData.documentType });
	}

	dataURLtoFile(dataurl, filename) {
		var arr = dataurl.split(","),
			mime = arr[0].match(/:(.*?);/)[1],
			bstr = atob(arr[1]),
			n = bstr.length,
			u8arr = new Uint8Array(n);
		while (n--) {
			u8arr[n] = bstr.charCodeAt(n);
		}
		return new File([u8arr], filename, { type: mime });
	}


	componentDidMount() {
		this.props.fetchAwardeeDocList({ userId: this.props.userId });
	}

	componentWillReceiveProps(nextProps) {
		const { type } = nextProps;
		switch (type) {
			case FETCH_AWARDEE_DOC_SUCCESS:
				this.setState({
					awardeeDocData: nextProps.awardeeDocData
				})
				break;
			case FETCH_DOCUMENT_ISSUE_UPLOAD_SUCCESS:
				this.setState(
					{
						isAlertShow: true,
						alertStatus: true,
						alertMsg: "Document has been successfully uploaded."
					});
				break;
			case POST_DISBURSAL_ACK_SUCCEEDED:
				// this.componentDidMount()
				if (!!this.props.userId) {
					this.props.fetchAwardeeDocList({ userId: this.props.userId });
				}

				break;

		}

	}
	moreHandler() {
		this.setState({
			isMore: true
		})
	}

	disbursalAcknowledgementCycle(disbursementCycleId) {
		this.props.disbursalAcknowledgementCycle({
			userId: this.props.userId,
			disbursementCycleId: disbursementCycleId
		})
	}

	documentUploadIssue(e, id, scholarshipIdDoc, disbursementCycleId, documentTypeCategory, documentTypeCategoryOption, documentDescription) {
		const nullIfFalsy = value => value || null;
		const stringifiedData = JSON.stringify({
			documentType: id,
			documentTypeCategory: nullIfFalsy(documentTypeCategory), // pass type Year or Semester
			documentTypeCategoryOption: nullIfFalsy(documentTypeCategoryOption),
			documentDescription: documentDescription // pass year Value or Semester Value
		});
		const file = e.target.files[0];
		// this.props.fetchDocumentUploadIssue({
		// 	docFile: file,
		// 	scholarshipId: scholarshipIdDoc,
		// 	userId: this.props.userId,
		// 	disbursementCycleId: disbursementCycleId,
		// 	userDocRequest: stringifiedData
		// });
		
		if (id == 1) {
			let reader = new FileReader(); // read upload file
			reader.onload = e => {
				this.setState({ cropSrc: e.target.result });
			};
			reader.readAsDataURL(event.target.files[0]);
			this.setState({
				stringifiedData: stringifiedData,
				isShowCropPup: true,
				docFile: file,
				scholarshipIdDoc,
			disbursementCycleId: disbursementCycleId,

			})
		} else {
			this.props.fetchDocumentUploadIssue({
			docFile: file,
			scholarshipId: scholarshipIdDoc,
			userId: this.props.userId,
			disbursementCycleId: disbursementCycleId,
			userDocRequest: stringifiedData
		});
			this.setState({ fileName: file.name, id: id });

		}
		// this.setState({ fileName: file.name, id: id });
	}


	closePopup() {
		this.setState({
			isAlertShow: false,
			alertStatus: "",
			alertMsg: "",
			isShowCropPup:false
		})
	}

	render() {
		const { awardeeDocData, isMore, isAlertShow, alertStatus, alertMsg, disbursalAcknowledgementData } = this.state;
		return (
			<section>
				<AlertMessage
					isShow={isAlertShow}
					status={alertStatus}
					msg={alertMsg}
					close={this.closePopup}
				/>
				<Loader isLoader={this.props.showLoader} />
				<section className="row col">
					<section className="col-md-12 col-sm-12">
						<h4 className="titletext">AWARDED SCHOLARSHIPS</h4>
						<article className="col-md-12 activities">
							{awardeeDocData && awardeeDocData.scholarshipApplyAwardeeList !== null ? (
								<article className="page-nav awardee">
									<ul>
										<ScholarshipApplyAwardeeListItem
											scholarshipApplyAwardeeList={awardeeDocData && awardeeDocData.scholarshipApplyAwardeeList}
											moreHandler={this.moreHandler}
											isMore={isMore}
											documentUploadIssue={this.documentUploadIssue}
											disbursalAcknowledgementCycle={this.disbursalAcknowledgementCycle}
											cropImage={()=>this.cropImage()}
											cropSrc={this.state.cropSrc}
											isShowCropPup={this.state.isShowCropPup}
											closePopup={this.closePopup}
											cropper={this.cropper}
											that={this}
											
										/>
									</ul>
								</article>
							) : (
									"You have not been awarded any scholarship yet. But don’t be disappointed, keep applying in scholarships to increase your chance of getting awarded."
								)}
						</article>
					</section>
				</section>
			</section>
		);
	}
}

const ScholarshipApplyAwardeeListItem = ({
	scholarshipApplyAwardeeList, moreHandler, isMore, documentUploadIssue, disbursalAcknowledgementCycle,isShowCropPup,cropSrc,closePopup,cropImage,cropper,that
}) => {
	return scholarshipApplyAwardeeList.map((list, index) => {
		let dayToGo = "";
		if (list.scholarshipApplyResponse.submittedAt) {
			dayToGo = moment.unix(list.scholarshipApplyResponse.submittedAt / 1000, "YYYYMMDD").fromNow();
		} else if (list.scholarshipApplyResponse.startDate) {
			dayToGo = moment.unix(list.scholarshipApplyResponse.startDate / 1000, "YYYYMMDD").fromNow();
		}
		return (
			<Status
				dayToGo={dayToGo}
				key={index}
				list={list}
				moreHandler={moreHandler}
				isMore={isMore}
				documentUploadIssue={documentUploadIssue}
				disbursalAcknowledgementCycle={disbursalAcknowledgementCycle}
				isShowCropPup={isShowCropPup}
				cropImage={cropImage}
				cropSrc={cropSrc}
				closePopup={closePopup}
				cropper={cropper}
				that={that}

			/>
		);
	});
};

const Status = ({ list, dayToGo, documentUploadIssue, disbursalAcknowledgementCycle, moreHandler, isMore,isShowCropPup,cropSrc,closePopup,cropImage,cropper,that }) => {
	const getStatus = {
		1: {
			statusName: "Pending",
			colorCode: "color6"
		},
		2: {
			statusName: "Submitted",
			colorCode: "color2"
		},
		3: {
			statusName: "Submitted",
			colorCode: "color2"
		},
		4: {
			statusName: "Book Slot",
			colorCode: "color4"
		},
		5: {
			statusName: "Slot Booked",
			colorCode: "color5"
		},
		6: {
			statusName: "Completed",
			colorCode: "color5"
		},
		7: {
			statusName: "Completed",
			colorCode: "color5"
		},
		8: {
			statusName: "Awardee",
			colorCode: "color1"
		},
		9: {
			statusName: "Submitted",
			colorCode: "color2"
		},
		10: {
			statusName: "Submitted",
			colorCode: "color2"
		}
	};

	return (
		<li className={getStatus[list.scholarshipApplyResponse.step] && getStatus[list.scholarshipApplyResponse.step].colorCode}>
			<p>{dayToGo}</p>
			<p
				dangerouslySetInnerHTML={{
					__html: gblFunc.replaceWithLoreal(list.scholarshipApplyResponse && list.scholarshipApplyResponse.scholarshipName)
				}}
			/>
			<article className="activities-wrapper">
				<ul>
					<li>
						<dd className="pointerEvent">{getStatus[list.scholarshipApplyResponse.step].statusName}</dd>
					</li>
				</ul>
				{list.userDisbursementCycleDetails && list.userDisbursementCycleDetails.length > 0 ? (
					<article className="disbursal-wrapper">
						{list.userDisbursementCycleDetails.map(item => (
							<article className="disbursal-box">
								<h6>{item.disbursalTitle}</h6>
								<article className="disbursal-items">
									<article className="disbursal-amount">
										<span>Amount</span>
										{item.amount == null ? "N/A" : item.amount}
									</article>
									<article className="disbursal-status">
										<span>Disbursal Status</span>
										{(item.disbursementStatusId == 1 && "Not Initiated") || (item.disbursementStatusId == 2 && "Initiated") || (item.disbursementStatusId == 3 && "Documents Uploaded") || (item.disbursementStatusId == 4 && "Documents Verified") || (item.disbursementStatusId == 5 && "Documents Incomplete") || (item.disbursementStatusId == 6 && "Disbursal Rejected") || (item.disbursementStatusId == 7 && "Disbursal Done") || (item.disbursementStatusId == 8 && "Disbursal Failed") || (item.disbursementStatusId == 9 && "Payment Acknowledged")}
									</article>
									<article className="disbursal-transaction-id">
										{item.disbursementStatusId !== 7 ? (
											<Fragment>
												<span>Transaction ID</span>
												{item.transactionReference !== null ? item.transactionReference : "N/A"}</Fragment>) : (
												""
											)}
									</article>
									<article className="btnCtrl">
										{item.disbursementStatusId == 7 ? (
											<Fragment>
												{
													(<button className="acknowledge" onClick={(e) => disbursalAcknowledgementCycle(item.disbursementCycleId)}>Acknowledge</button>)}</Fragment>)
											: item.acknowledgementStatus ? (<button className="acknowledged">Acknowledged</button>) : ""}
									</article>
								</article>
								{item.userDisbursementDocumentResponseList && item.userDisbursementDocumentResponseList.length > 0 && (
									<Fragment>
										<article className="document-list-items">
											{item.userDisbursementDocumentResponseList.map(innerItem => (
												<article className="document-list-item" key={innerItem.id}>
													<article className="content-wrapper">
														<article className="name">
															<span>{innerItem.docNameLabel}</span>
														</article>
														<article className="status">
															<span>{(innerItem.documentStatus ? innerItem.documentStatus : "N/A")}</span>
														</article>
													</article>
													<article className="btnCtrl">
														{innerItem.documentTypeVerificationId != 1 ?
															<label className="upload">
																<input
																	type="file"
																	onChange={e =>
																		documentUploadIssue(e, innerItem.documentType, list.scholarshipApplyResponse.scholarshipId, item.disbursementCycleId, null, null, innerItem.documentDescription)
																	}
																/>
																<span>Upload</span>
															</label> : ""}
														<a href={innerItem.location} className="view" target="_blank">View</a>

													</article>
													{isShowCropPup &&
													innerItem.documentType == 1 ? (
														<section className="cropImg">
															<section className="cropImgbg">
																<article className="popup-header">
																	<button
																		onClick={closePopup}
																		type="button"
																		className="close"
																	>
																		×
                                  </button>
																	<h3 className="popup-title">CROP IMAGE</h3>
																</article>
																<article className="popup-body">
																	<article className="row">
																		<article className="col-md-12">
																			<article className="cropArea">
																				<Cropper
																					style={{
																						height: "50vh",
																						width: "100%"
																					}}
																					aspectRatio={1}
																					zoomable={true}
																					autoCropArea={1}
																					preview=".img-preview"
																					guides={false}
																					src={cropSrc}
																					minCropBoxWidth={50}
																					minCropBoxHeight={50}
																					/* cropBoxMovable= {false}
																					cropBoxResizable = {false} */
																					ref={cropper => {
																						that.cropper = cropper;
																					}}
																				/>
																			</article>
																		</article>
																	</article>
																	<article className="row">
																		<article className="col-md-12 text-center">
																			<button
																				className="updatePicBtn"
																				onClick={event => cropImage(innerItem)}
																			>
																				Update Picture
                                      </button>
																		</article>
																	</article>
																</article>
																<article className="popup-footer" />
															</section>
														</section>
													) : (
														""
													)}
												</article>
											))}
										</article>
										{/* {item.userDisbursementDocumentResponseList && item.userDisbursementDocumentResponseList.length > 3 &&
											<button className="arrow bounce" onClick={moreHandler}>
												&#8623;
											</button> 
										}*/}
									</Fragment>
								)}
							</article>
						))}
					</article>
				) : (<span className="noDataFound">No data found</span>)}
			</article>
		</li>
	);
};


export default scholarshipApplyAwardeeList;
