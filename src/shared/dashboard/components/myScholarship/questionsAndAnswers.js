import React, { Component } from 'react';
import { fromNow } from "../../../../constants/constants";
import moment from "moment";

class QuestionsAndAnswers extends Component {
	constructor(props) {
		super(props);
		this.state = {
			dashQna: []
		}
	}

	componentDidMount() {
		const userId = localStorage.getItem('userId');
		this.props.fetchQnaDash({ userId: userId, page: 0, length: 20 })
		this.props.fetchQnaNotification({ userId: userId, page: 0, length: 10 })   // change path url of api in saga
	}

	componentWillReceiveProps(nextProps) {
		const { dashQna, qnaNotification } = nextProps;
		console.log(dashQna, '[dashQna]')
		if (!!dashQna && !!dashQna.data) {
			this.setState({
				dashQna: dashQna.data
			})
		}
		if (!!qnaNotification && !!qnaNotification.data) {
			this.setState({
				qnaNotificationData: qnaNotification.data
			})
		}
	}
	render() {
		const { dashQna, qnaNotificationData } = this.state;
		return (
			<section className="questions-answers-wrapper">
				<article className="table-one">
					<h3>Questions & Answers</h3>
					{dashQna.length > 0 ? (
						<ul>
							{dashQna.map(item => {
								return (
									<li>
										<span onClick={() => this.props.history.push(`/qna/${item.slug}`)}>{item.questionText}</span>
										<span><i>{!!item.answers.total ? `${item.answers.total} answers` : 'No answer yet'}</i>
											<i>{!!item.createdAt && `Asked ${item.createdAt.substr(0, item.createdAt.indexOf(' '))}`}</i></span>
									</li>
								)
							})}
						</ul>
					) : (
							<p className="defaultMessage">You have not asked any questions yet. Ask your question <a href="/qna">here</a>.</p>
						)}
				</article>
				<article className="table-two">
					<h3>All Notifications</h3>
					<ul>
						{!!qnaNotificationData && qnaNotificationData.map((item, index) => (
							<li>
								<span
									onClick={() => this.props.history.push(`/qna/${item.slug}`)}
								>{item.name}<i>{item.action === 'Commented' ? 'commented on your answer' : 'answered to your question'}</i></span>
								<span>{!!item.createdAt && fromNow(item.createdAt)}</span>
							</li>
						)
						)}
					</ul>
					{!!qnaNotificationData && qnaNotificationData.length < 1 && (
						<p className="defaultMessage">There is no new notification for you. Keep exploring new scholarships <a href="/scholarships">here</a>.</p>
					)}
				</article>
			</section>
		)
	}
}

export default QuestionsAndAnswers;
