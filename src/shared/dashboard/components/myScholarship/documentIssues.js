import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import Loader from "../../../common/components/loader";
import gblFunc from "../../../../globals/globalFunctions";
import { FETCH_DOCUMENT_ISSUE_SUCCESS, FETCH_DOCUMENT_ISSUE_UPLOAD_SUCCESS } from "../../actions";
import AlertMessage from "../../../common/components/alertMsg";
import Cropper from "react-cropper";
if (typeof window !== "undefined") {
	require("cropperjs/dist/cropper.css");
}
class DocumentIssues extends Component {
	constructor(props) {
		super(props);
		this.state = {
			documentIssueData: null,
			fileName: "",
			id: "",
			isAlertShow: false,
			alertStatus: "",
			alertMsg: "",
			isShowCropPup: false,
			cropSrc: "",
			cropResult: null,
			stringifiedData: null,
			scholarshipIdDoc: null

		};
		this.documentUploadIssue = this.documentUploadIssue.bind(this);
		this.closePopup = this.closePopup.bind(this);
		this.cropImage = this.cropImage.bind(this);
		this.dataURLtoFile = this.dataURLtoFile.bind(this);


	}

	cropImage(docObj) {
		if (typeof this.cropper.getCroppedCanvas() === "undefined") {
			return;
		}

		const selectedFile = this.state.docFile;

		let customizeFileName =
			selectedFile.name.substr(0, selectedFile.name.lastIndexOf(".")) +
			selectedFile.name
				.substr(selectedFile.name.lastIndexOf("."))
				.toLowerCase();

		var file = this.dataURLtoFile(
			this.cropper
				.getCroppedCanvas({ width: 200, height: 200 })
				.toDataURL("image/jpeg", 0.8),
			customizeFileName
		);

		let reader = new FileReader(); // read upload file
		reader.readAsDataURL(file);
		this.setState(
			{
				cropResult: file,
				isShowCropPup: false
			}
			, () =>
				this.props.fetchDocumentUploadIssue({
					docFile: file,
					scholarshipId: this.state.scholarshipIdDoc,
					userId: this.props.userId,
					userDocRequest: this.state.stringifiedData
				})
			//   () =>
			//    this.uploadDocumentWithOutCrop(docObj, Array(file))
		);

		this.setState({ fileName: file.name, id: this.state.stringifiedData.documentType });
	}

	dataURLtoFile(dataurl, filename) {
		var arr = dataurl.split(","),
			mime = arr[0].match(/:(.*?);/)[1],
			bstr = atob(arr[1]),
			n = bstr.length,
			u8arr = new Uint8Array(n);
		while (n--) {
			u8arr[n] = bstr.charCodeAt(n);
		}
		return new File([u8arr], filename, { type: mime });
	}

	componentDidMount() {
		this.props.fetchDocumentViewIssue({ userId: this.props.userId, scholarshipId: typeof window !== 'undefined' && localStorage.getItem('scholarshipId') })
	}

	componentWillReceiveProps(nextProps) {
		const { type } = nextProps;
		const userList = gblFunc.getStoreUserDetails();
		switch (type) {
			case FETCH_DOCUMENT_ISSUE_SUCCESS:
				this.setState({
					documentIssueData: nextProps.documentIssue
				});
				break;
			case FETCH_DOCUMENT_ISSUE_UPLOAD_SUCCESS:
				this.setState(
					{
						isAlertShow: true,
						alertStatus: true,
						alertMsg: "Document has been successfully uploaded."
					});
				break;
		}
	}

	pushTobackState(urlPar) {
		const href = `/myProfile/${urlPar}`;
		const as = href;
		Router.push(href, as, { shallow: true });
	}

	documentUploadIssue(e, id, scholarshipIdDoc, documentTypeCategory, documentTypeCategoryOption, documentDescription) {
		const nullIfFalsy = value => value || null;
		const stringifiedData = JSON.stringify({
			documentType: id,
			documentTypeCategory: nullIfFalsy(documentTypeCategory), // pass type Year or Semester
			documentTypeCategoryOption: nullIfFalsy(documentTypeCategoryOption),
			documentDescription: documentDescription // pass year Value or Semester Value
		});
		const file = e.target.files[0];


		if (id == 1) {
			let reader = new FileReader(); // read upload file
			reader.onload = e => {
				this.setState({ cropSrc: e.target.result });
			};
			reader.readAsDataURL(event.target.files[0]);
			this.setState({
				stringifiedData: stringifiedData,
				isShowCropPup: true,
				docFile: file,
				scholarshipIdDoc,
			})
		} else {
			this.props.fetchDocumentUploadIssue({
				docFile: file,
				scholarshipId: scholarshipIdDoc,
				userId: this.props.userId,
				userDocRequest: stringifiedData
			});
			this.setState({ fileName: file.name, id: id });

		}
	}

	closePopup() {
		this.setState({
			isAlertShow: false,
			alertStatus: "",
			alertMsg: "",
			isShowCropPup: false

		})
	}

	render() {
		const { documentIssueData, isAlertShow, alertStatus, alertMsg } = this.state;
		return (
			<Fragment>
				<AlertMessage
					isShow={isAlertShow}
					status={alertStatus}
					msg={alertMsg}
					close={this.closePopup}
				/>
				<Loader isLoader={this.props.showLoader} />
				<h4 className="titletext marginTop">Document Issue <Link
					onClick={() => [
						props.gtmEventHandler(["Header", `ApplicationStatus`]),
						this.pushTobackState('ApplicationStatus')]
					}
					to="/myProfile/ApplicationStatus"
					className="backBtn"
				>Back</Link></h4>
				<section className="row document-issue">
					<section className="col-xs-12 col-sm-12 col-md-12">
						<h3 dangerouslySetInnerHTML={{
							__html: gblFunc.replaceWithLoreal(typeof window !== 'undefined' && localStorage.getItem('scholarshipName'))
						}} />

						{/* Document Issue List */}
						{documentIssueData && documentIssueData.applcationDocuments.length > 0 ? (
							<section className="document-issue-wrapper">
								{documentIssueData && documentIssueData.applcationDocuments.map(item => {
									return (<Fragment>
										{item && item.userDocument && item.userDocument.map(innerItem => (
											innerItem.documentTypeCategory !== null && innerItem.documentTypeCategory.id > 0 ? (
												<section className={innerItem.documentTypeCategory ? "issue-wrapper-documentTypeCategory" : "issue-wrapper-single"} >
													<h6>{item.documentType.description}</h6>
													<article className="document-type-wrapper">
														<article className="content-wrapper">
															<article className="name">
																<span>{item.docNameLabel}</span>
															</article>
															<article className="status">
																<span>{(innerItem.verificationStatus == 2 && "In Correct") || (innerItem.verificationStatus == 3 && "Missing") || (innerItem.verificationStatus == 4 && "Blurred")}</span>
															</article>
														</article>
														<article className="btnCtrl">
															<a href={innerItem.location} className="view" target="_blank">View</a>
															<label className="upload">
																<input
																	type="file"
																	onChange={e =>
																		this.documentUploadIssue(e, item.documentType.id, typeof window !== 'undefined' && localStorage.getItem('scholarshipId'), innerItem.documentTypeCategory !== null ? innerItem.documentTypeCategory.id : 0, innerItem.documentTypeCategory !== null ? innerItem.documentTypeCategory.id : 0, innerItem.documentDescription)
																	}
																/>
																<span>Upload</span>
															</label>
														</article>
													</article>
												</section>
											) : innerItem.documentTypeCategory == null || innerItem.documentTypeCategory.id == 0 ? <article className="issue-wrapper-single">
												<article className="content-wrapper">
													<article className="name">
														<span>{item.docNameLabel}</span>
													</article>
													<article className="status">
														<span>{(item.userDocument && item.userDocument.length > 0 && item.userDocument[0].verificationStatus == 2 && "In Correct") || (item.userDocument && item.userDocument.length > 0 && item.userDocument[0].verificationStatus == 3 && "Missing") || (item.userDocument && item.userDocument.length > 0 && item.userDocument[0].verificationStatus == 4 && "Blurred")}</span>
													</article>
												</article>
												<article className="btnCtrl">
													<a href={item.userDocument && item.userDocument.length > 0 && item.userDocument[0].location} className="view" target="_blank">View</a>
													<label className="upload">
														<input
															type="file"
															onChange={e =>
																this.documentUploadIssue(e, item.documentType.id, typeof window !== 'undefined' && localStorage.getItem('scholarshipId'),
																	item.userDocument && item.userDocument.length > 0 && item.userDocument[0].documentTypeCategory !== null ? item.userDocument[0].documentTypeCategory.id : null, item.userDocument && item.userDocument.length > 0 && item.userDocument[0].documentTypeCategory !== null ? item.userDocument[0].documentTypeCategory.id : null, item.userDocument && item.userDocument.length > 0 && item.userDocument[0].documentDescription)
															}
														/>
														<span>Upload</span>
													</label>
												</article>
												{this.state.isShowCropPup &&
													item.documentType.id == 1 ? (
														<section className="cropImg">
															<section className="cropImgbg">
																<article className="popup-header">
																	<button
																		onClick={this.closePopup}
																		type="button"
																		className="close"
																	>
																		×
                                  </button>
																	<h3 className="popup-title">CROP IMAGE</h3>
																</article>
																<article className="popup-body">
																	<article className="row">
																		<article className="col-md-12">
																			<article className="cropArea">
																				<Cropper
																					style={{
																						height: "50vh",
																						width: "100%"
																					}}
																					aspectRatio={1}
																					zoomable={true}
																					autoCropArea={1}
																					preview=".img-preview"
																					guides={false}
																					src={this.state.cropSrc}
																					minCropBoxWidth={50}
																					minCropBoxHeight={50}
																					/* cropBoxMovable= {false}
																					cropBoxResizable = {false} */
																					ref={cropper => {
																						this.cropper = cropper;
																					}}
																				/>
																			</article>
																		</article>
																	</article>
																	<article className="row">
																		<article className="col-md-12 text-center">
																			<button
																				className="updatePicBtn"
																				onClick={event => this.cropImage(item)}
																			>
																				Update Picture
                                      </button>
																		</article>
																	</article>
																</article>
																<article className="popup-footer" />
															</section>
														</section>
													) : (
														""
													)}
											</article>

													: ""

										))}

									</Fragment>)
								})}
							</section>
						) : (<span className="noDataFound marginTop">There is no pending document found for this scholarship.</span>)}

					</section>
				</section>
			</Fragment>
		);
	}
}

export default DocumentIssues;
