import PersonalInfo from "../components/profile-info/personalinfo";
import Education from "../components/profile-info/education";
import FamilyEarnings from "../components/profile-info/familyEarnings";
import Interest from "../components/profile-info/interest";
import Documents from "../components/profile-info/documents";
import EntranceExam from "../components/profile-info/entranceExam";
import ScholarshipHistory from "../components/profile-info/scholarshipHistory";
import References from "../components/profile-info/references";
import BankDetails from "../components/profile-info/bankDetails";
import MatchedScholarships from "../components/extras/matchedScholarship";
import MyFavorites from "../components/myScholarship/myFavorites";
import ApplicationStatus from "../components/myScholarship/applicationStatus";
import DocumentIssues from "../components/myScholarship/documentIssues";
import QuestionsAndAnswers from '../components/myScholarship/questionsAndAnswers';
import AwardeesScholarships from '../components/myScholarship/awardeesScholarship';
import StudentList from "../components/mySubscriber/studentList";
import AddStudent from "../components/mySubscriber/addStudent";
import CscDemo from "../components/mySubscriber/csc-demo";
import CscDownloads from "../components/mySubscriber/csc-download";
import CscPersonalInfo from "../components/profile-info/cscPersonalInfo";
import Question from "../components/myQuestion/question";
import Answer from "../components/myQuestion/answer";
import Follow from "../components/myQuestion/follow";
import Like from "../components/myQuestion/liked";
import MyQuestionAnswer from "./myQuestionAnswer/myquestionanswer";
import UpdateEmail from "./../components/profile-info/updateMobEmail";
import UpdateMobile from "./../components/profile-info/updateMobEmail";


export const dashboardFormContent = {
	PersonalInfo,
	CscPersonalInfo,
	Education,
	FamilyEarnings,
	Interest,
	Documents,
	EntranceExam,
	ScholarshipHistory,
	References,
	BankDetails,
	MatchedScholarships,
	QuestionsAndAnswers,
	AwardeesScholarships,
	MyFavorites,
	ApplicationStatus,
	StudentList,
	AddStudent,
	CscDemo,
	CscDownloads,
	Question,
	Answer,
	Follow,
	Like,
	MyQuestionAnswer,
	DocumentIssues,
	UpdateEmail,
	UpdateMobile
};
