import React, { Component } from "react";

class DashPayNowPopUP extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const script = document.createElement("script");
    script.setAttribute("src", "https://js.instamojo.com/v1/button.js");
    script.setAttribute("type", "text/javascript");
    document.getElementById("dashPayPreFormSubs").appendChild(script);
  }

  render() {
    return (
      <article className="popup">
        <article className="popupsms">
          <article className="popup_inner premium-popup">
            <article className="LoginRegPopup">
              <section className="modal fade modelAuthPopup1">
                <section className="modal-dialog">
                  <section className="modal-content modelBg">
                    <article>
                      <span id="dashPayPreFormSubs" />
                      <button
                        type="button"
                        className="close"
                        onClick={this.props.closepaypop}
                      >
                        <i>&times;</i>
                      </button>
                    </article>

                    {/* Login / Register Section*/}
                    <section className="row premium-row-details">
                      <article className="col-md-12 text-center">
                        <h1 className="premium">PREMIUM MEMBERSHIP</h1>
                        <p className="subtitle">
                          Access best-fit scholarship | Timely alerts |
                          Application support{" "}
                        </p>
                        <article className="col-md-6">
                          <article>
                            <i>199</i>
                            <p>
                              6 Months <span>Membership</span>
                            </p>
                          </article>
                          <article className="btn-ctrl popbutton">
                            <span id="preAnnMem">
                              <div className="im-checkout btn-28">
                                {/* <PayButtonLink
                                  url={
                                    "https://www.instamojo.com/buddy4study/annual-membership-4c345/?data_name=rajkishore%20Kishorr&data_email=raj.rore@gmail.com&data_phone=9971584533&data_Field_74346=2&data_Field_74344=rajkishore%20Kishorr&data_Field_37677=9971584533&data_hidden=data_Field_74346&data_hidden=data_Field_74344&data_hidden=data_Field_37677"
                                  }
                                  text={"Starter Pack @ Rs 199 only"}
                                /> */}
                              </div>
                            </span>
                          </article>
                        </article>
                        <article className="col-md-6">
                          <article>
                            <i>365</i>
                            <p>
                              1 Year <span>Membership</span>
                            </p>
                          </article>
                          <article className="btn-ctrl popbutton">
                            <span id="preAnnMem">
                              <div className="im-checkout btn-28">
                                {/* <PayButtonLink
                                  url={
                                    "https://www.instamojo.com/buddy4study/premium-membership-431a7/?data_name=rajkishore%20Kishorr&data_email=raj.rore@gmail.com&data_phone=9971584533&data_Field_44258=2&data_hidden=data_Field_44258"
                                  }
                                  text={"Annual Pack @ Re 1 per day only"}
                                /> */}
                              </div>
                            </span>
                          </article>
                        </article>
                      </article>
                    </section>
                  </section>
                </section>
              </section>
            </article>
          </article>
        </article>
      </article>
    );
  }
}

const PayButtonLink = props => {
  return (
    <a
      href={props.url}
      rel="im-checkout"
      data-behaviour="remote"
      data-style="flat"
      data-text={props.text}
      className="btn"
    />
  );
};

export default DashPayNowPopUP;
