import React from "react";
import DatePicker from "react-datepicker";
import moment from "moment";
import { bookSlotInfo } from "../../../../constants/constants";
if (typeof window !== "undefined") {
  require("react-datepicker/dist/react-datepicker.css");
}

const BookSlot = ({
  bookSlotData,
  currentBookSlotFormHandler,
  preferLang,
  updateBookSlotHandler,
  closeBookSlot,
  validations,
  bsid,
  bookSlotDates
}) => {
  let data = [];
  let lang;
  if (bookSlotDates) {
    lang = bookSlotDates.prefferedLanguage.split(',');
    lang.length > 0 && lang.map(item => {
      preferLang && preferLang.map(pre => {
        if (item == pre.id) {
          data.push(pre);
        }
      })
    })
  };
  const isExpired = moment.duration(
    moment(bookSlotDates.dateTo, "YYYY-MM-DD").diff(
      moment().startOf("day")
    )
  )
    .asDays();
  return (
    <section>
      <section className="bookSlot" />
      <section className="bookSlotbg">
        <article className="popup-header">
          <button
            type="button"
            className="close"
            onClick={() => closeBookSlot()}
          >
            <i>&times;</i>
          </button>
          <h3 className="popup-title">
            <i>Fill the following form to book your</i>
            <i>telephonic interview round slot.</i>
          </h3>
        </article>
        <article className="popup-body">
          <article className="row">
            <article className="col-md-12">
              <article className="contentArea">
                <form
                  name="bookSlot"
                  autoCapitalize="off"
                  onSubmit={event =>
                    updateBookSlotHandler(event, bookSlotData.scholarshipId)
                  }
                >
                  <section className="row margintop20">
                    <section className=" col-md-4">
                      <article className="ctrl-wrapper">
                        <article className="form-group posdate">
                          <DatePicker
                            showYearDropdown
                            scrollableYearDropdown
                            name="dob"
                            id="dob"
                            placeholderText="Click to select a date"
                            className="form-control"
                            autoCapitalize="off"
                            selected={
                              bookSlotData.scheduleDate
                                ? moment(
                                  bookSlotData.scheduleDate,
                                  "DD-MM-YYYY"
                                )
                                : ""
                            }
                            minDate={moment(bookSlotDates.dateFrom)}
                            maxDate={moment(bookSlotDates.dateTo)}
                            onChange={event =>
                              currentBookSlotFormHandler(event, "dob")
                            }
                            dateFormat="DD-MM-YYYY"
                            excludeDates={bookSlotDates.excludes}
                          />
                          <i
                            className="fa fa-calendar icon"
                            aria-hidden="true"
                          />
                          {validations.scheduleDate ? (
                            <span className="error animated bounce">
                              {validations.scheduleDate}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    </section>
                    <section className="col-md-4">
                      <article className="ctrl-wrapper">
                        <article className="form-group">
                          <input
                            type="input"
                            name="MobileNumber"
                            maxLength="10"
                            placeholder="Mobile number"
                            id="mobile"
                            value={bookSlotData.mobile}
                            onChange={event =>
                              currentBookSlotFormHandler(event)
                            }
                            className="form-control"
                          />
                          <i className="fa fa-phone icon" aria-hidden="true" />
                          <i className="fa fa-phone icon" aria-hidden="true" />
                          {validations.mobile ? (
                            <span className="error animated bounce">
                              {validations.mobile}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    </section>
                    <section className="col-md-4">
                      <article className="ctrl-wrapper">
                        <article className="form-group">
                          <select
                            name="scheduleTime"
                            className="form-control"
                            id="scheduleTime"
                            value={bookSlotData.scheduleTime}
                            onChange={event =>
                              currentBookSlotFormHandler(event)
                            }
                          >
                            <option value="">--Select Interview Time--</option>
                            <option value="10:00:00">
                              10:00 AM - 01:30 PM
                            </option>
                            <option value="02:30:00">
                              02:30 PM - 04:30 PM
                            </option>
                            <option value="05:00:00">
                              05:00 PM - 07:00 PM
                            </option>
                          </select>
                          <i
                            className="fa fa-clock-o icon"
                            aria-hidden="true"
                          />
                          {validations.scheduleTime ? (
                            <span className="error animated bounce">
                              {validations.scheduleTime}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    </section>
                    {/* <section className="col-md-6">
                        <article className="ctrl-wrapper">
                          <article className="form-group">
                            <select
                              name="location1"
                              className="form-control"
                              id="preMOC"
                              required=""
                            >
                              <option value="" selected="selected">
                                --Select Preferred Location 1--
                              </option>
                              <option value="New Delhi">New Delhi</option>
                              <option value="Kolkata">Kolkata</option>
                              <option value="Hyderabad">Hyderabad</option>
                              <option value="Goa">Goa</option>
                            </select>
                            <i
                              className="fa fa-map-marker icon"
                              aria-hidden="true"
                            />
                            <span className="error animated bounce">
                              Location is required.
                            </span>
                          </article>
                        </article>
                      </section> */}
                  </section>
                  {/* <section className="row">
                      <section className="col-md-6">
                        <article className="ctrl-wrapper">
                          <article className="form-group">
                            <select
                              name="location2"
                              className="form-control"
                              id="preMOC"
                              required=""
                            >
                              <option value="" selected="selected">
                                --Select Preferred Location 2--
                              </option>
                              <option value="New Delhi">New Delhi</option>
                              <option value="Kolkata">Kolkata</option>
                              <option value="Hyderabad">Hyderabad</option>
                              <option value="Goa">Goa</option>
                            </select>
                            <i
                              className="fa fa-map-marker icon"
                              aria-hidden="true"
                            />
                            <span className="error animated bounce">
                              * Location is required.
                            </span>
                          </article>
                        </article>
                      </section>
                      <section className="col-md-6">
                        <article className="ctrl-wrapper">
                          <article className="form-group">
                            <select
                              name="location3"
                              className="form-control"
                              required=""
                            >
                              <option value="" selected="selected">
                                --Select Preferred Location 3--
                              </option>
                              <option value="New Delhi">New Delhi</option>
                              <option value="Kolkata">Kolkata</option>
                              <option value="Hyderabad">Hyderabad</option>
                              <option value="Goa">Goa</option>
                            </select>
                            <i
                              className="fa fa-map-marker icon"
                              aria-hidden="true"
                            />
                            <span className="error animated bounce">
                              * Location is required.
                            </span>
                          </article>
                        </article>
                      </section>
                    </section> */}
                  <section className="row">
                    <section className="ctrl-wrapper col-md-12">
                      <article className="form-group preferred-language">
                        <article className="col-md-12 ">
                          <p>Preferred Language</p>
                        </article>

                        <article className="col-md-12">
                          {data && data.length > 0
                            ? data.map(list => {
                              return (
                                <label key={list.id}>
                                  <input
                                    type="checkbox"
                                    id="prefferedLanguages"
                                    value={list.language}
                                    checked={
                                      bookSlotData.prefferedLanguages &&
                                      bookSlotData.prefferedLanguages.indexOf(
                                        list.language
                                      ) > -1
                                    }
                                    onChange={event =>
                                      currentBookSlotFormHandler(event)
                                    }
                                  />
                                  {list.language}
                                  <dd className="chkbox" />
                                </label>
                              );

                            })
                            : null}
                          {validations.prefferedLanguages ? (
                            <span className="error animated bounce">
                              {validations.prefferedLanguages}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    </section>
                  </section>

                  <section className="col-md-12">
                    <article className="row">
                      <article className="ctrl-wrapper">
                        <article className="form-group textAreaCrtl bookSlot">
                          <textarea
                            type="input"
                            name="comments"
                            placeholder="Comment"
                            id="comments"
                            value={bookSlotData.comments}
                            className="form-control"
                            onChange={event =>
                              currentBookSlotFormHandler(event)
                            }
                          />
                          <i
                            className="fa fa-comment icon"
                            aria-hidden="true"
                          />
                          {validations.comments ? (
                            <span className="error animated bounce">
                              {validations.comments}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    </article>
                  </section>
                  {
                    isExpired >= 0 ? <button type="submit" className="bookSlotBtn margintop">
                      Submit
               </button> : <p className="colorRed">Book slot deadline has been expired.</p>
                  }
                </form>
              </article>
            </article>
          </article>
        </article>
        <article className="popup-footer" />
      </section>
    </section>
  );
};

export default BookSlot;
