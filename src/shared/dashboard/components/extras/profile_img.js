import React, { Component } from "react";
import Loader from "../../../common/components/loader";
class ProfileImg extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      imagefileChangedHandler,
      showLoader,
      userUpdated,
      validations,
      userDetails,
      redirectToMembership,
      profileTitleMsg
    } = this.props;

    const defaultProfilePic =
      "https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/profile-img.png";
    return (
      <section>
        <Loader isLoader={showLoader} />
        <article className="row">
          <article className="col-md-12">
            <section className="profile-content-pos">
              <section className="profile-content">
                <p className="profileTitleMsg">{profileTitleMsg}</p>
                <form name="myForm" autoCapitalize="off">
                  <article className="imgWrapper">
                    <article className="fileuploadBg">
                      <label htmlFor="docFile">
                        Upload Profile Picture
                          <input
                          type="file"
                          name="file"
                          id="docFile"
                          onChange={event => imagefileChangedHandler(event)}
                          accept="image/*"
                          required=""
                        />
                      </label>
                    </article>
                    {validations.docFile ? (
                      <i className="fileSizeError">{validations.docFile}</i>
                    ) : null}
                    {/* {validations.docFile ? (
                      <span className="error animated bounce">
                        {validations.docFile}
                      </span>
                    ) : null} */}
                    <img
                      alt={userUpdated.fullName}
                      src={`${
                        userUpdated.pic && userUpdated.pic != "null"
                          ? userUpdated.pic
                          : defaultProfilePic
                        }`}
                    />
                  </article>
                </form>
                <article className="attention-bar-wrapper">
                  <h1>{`${userUpdated.fullName}`}</h1>
                  <article className="attention-bar">
                    <section className="progress-container">
                      <section
                        className={`progress-line ${userUpdated.percentage == 100 && "complete"}`}
                        style={{ width: `${userUpdated.percentage}%` }}
                      />
                    </section>
                    {/* {userUpdated.percentage > 100 && ( */}
                    <span>
                      <dd>
                        {userUpdated.percentage}
                        <i>%</i>
                      </dd>{" "}
                      Profile Completed
                        </span>
                    {/*  )} */}
                  </article>
                </article>
              </section>
            </section>
          </article>
        </article>
      </section>
    );
  }
}

// const PremiumStatus = ({ userDetails, redirectToMembership }) => {
//   let status = null;

//   if (
//     userDetails.membershipExpiry !== "null" &&
//     userDetails.membershipExpiry !== "" &&
//     userDetails.membershipExpiry !== "undefined"
//   ) {
//     status = (
//       <div className="premem-but">
//         <i className="fa fa-check" />&nbsp; Premium Member
//       </div>
//     );
//   } else {
//     status = (
//       <div
//         className="Bpremem-but"
//         style={{ cursor: "pointer" }}
//         onClick={() => redirectToMembership()}
//       >
//         <i className="fa fa-star" />&nbsp; Become Premium Member
//       </div>
//     );
//   }
//   return <article>{status}</article>;
// };
export default ProfileImg;
