import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import Slider from "react-slick";
import { breadCrumObj } from "../../../../constants/breadCrum";
import BreadCrum from "../../../components/bread-crum/breadCrum";
import { Helmet } from "react-helmet";
import SideNav from "../../../components/side-nav/sideNav";
import { imageFileExtension } from "../../../../constants/constants";
import DashboardUI from "../../../components/dashboard/dashboardUI";
import DashboardNav from "../../../components/dashboard/dashboard-nav";
import ProfileImg from "../extras/profile_img";
import gblFunc from "../../../../globals/globalFunctions";
import moment from "moment";
import {
  UPLOAD_USER_PIC_SUCCESS,
  FETCH_SCHOLAR_SCHOLARSHIP_WON_SUCCESS,
  FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_SUCCESS
} from "../../actions";
import { ruleRunner } from "../../../../validation/ruleRunner";
import { dashboardFormContent } from "../importForm";
import {
  maxFileSize,
  hasValidImageExtenstion,
  IsMessage
} from "../../../../validation/rules";
import Loader from "../../../../shared/common/components/loader";

class Scholar extends Component {
  constructor(props) {
    super(props);
    this.imagefileChangedHandler = this.imagefileChangedHandler.bind(this);
    this.clearSelectedFile = this.clearSelectedFile.bind(this);
    this.updateUser = this.updateUser.bind(this);
    this.fileValidationHandler = this.fileValidationHandler.bind(this);
    this.setUserConfigHandler = this.setUserConfigHandler.bind(this);
    this.redirectToMembership = this.redirectToMembership.bind(this);
    this.onFormChangeHandler = this.onFormChangeHandler.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.pdfDownloadHandler = this.pdfDownloadHandler.bind(this);
    this.msgHandler = this.msgHandler.bind(this);
    this.state = {
      selectedFile: null,
      src: null,
      tab: "scholar",
      userId: null,
      scholarshipId: null,
      adminId: "",
      id: "",
      message: "",
      validations: {
        docFile: null,
        message: null
      },
      updateUserConfig: {
        fullName: null,
        fromRecommended: false,
        pic: null,
        userId: null,
        percentage: null
      },
      isFormValid: true,
      msgStatus: true,
      activeTabIndex: 0,
      scholarScholarshipWonlistData: []
    };
  }

  /** start - validation part - get message and required validation **/
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "message":
        validationObject.name = "* Message";
        validationObject.validationFunctions = [required, IsMessage];
        return validationObject;
    }
  }
  /** end - validation part - get message and required validation **/

  /** start - validation part - check form submit validation **/
  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }
  /** end - validation part - check form submit validation **/

  componentWillMount() {
    this.setUserConfigHandler();
    this.props.scholarScholarshipWonData({
      userId: gblFunc.getStoreUserDetails()["userId"]
    });
  }

  /************ start - validation part - rules******************* */
  onFormChangeHandler(event) {
    const { id, value } = event.target;
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );
    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];
    this.setState({
      [id]: value,
      validations
    });
  }

  /************ end - validation part - rules******************* */

  componentWillReceiveProps(nextProps) {
    const {
      type,
      scholarScholarshipWonlistData,
      scholarScholarshipCertificateListPdfData
    } = nextProps;
    switch (type) {
      case UPLOAD_USER_PIC_SUCCESS:
        typeof window !== "undefined"
          ? localStorage.setItem("pic", nextProps.uploadPicData.location)
          : "";
        this.setState({
          updateUserConfig: {
            ...this.state.updateUserConfig,
            pic: nextProps.uploadPicData.location
          }
        });
        break;

      case FETCH_SCHOLAR_SCHOLARSHIP_WON_SUCCESS:
        if (
          nextProps.scholarScholarshipWonlistData &&
          nextProps.scholarScholarshipWonlistData.length > 0
        ) {
          this.setState({
            scholarshipId: scholarScholarshipWonlistData[0].scholarshipId,
            scholarScholarshipWonlistData: scholarScholarshipWonlistData
          });
        }
        break;

      case FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_SUCCESS:
        window.open(scholarScholarshipCertificateListPdfData.certificatePath);
        break;
      default:
        break;
    }
  }

  imagefileChangedHandler(e) {
    let checkValidate = null;
    e.preventDefault();
    const { id } = e.target;

    let files;
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }
    const reader = new FileReader();
    reader.onload = () => {
      this.setState({ src: reader.result });
    };
    reader.readAsDataURL(files[0]);

    // Check Size In MB.
    const sizeInMb = Math.round(files[0].size / (1024 * 1024));
    const fileExtension = files[0].name.slice(files[0].name.lastIndexOf("."));

    if (fileExtension) {
      checkValidate = this.fileValidationHandler(
        fileExtension,
        id,
        "File",
        hasValidImageExtenstion(imageFileExtension),
        files
      );
      if (checkValidate) return;
    }

    if (sizeInMb > 2) {
      checkValidate = this.fileValidationHandler(
        sizeInMb,
        id,
        "Uploaded file",
        maxFileSize(2),
        files
      );

      if (checkValidate) return;
    }
  }

  fileValidationHandler(fileConfig, id, name, validationConfig, files) {
    let validationResult = null;
    validationResult = ruleRunner(fileConfig, id, name, validationConfig);

    const validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    this.setState({
      selectedFile: validations[id] ? null : files[0],
      validations
    });
    return validations[id] ? true : false;
  }

  clearSelectedFile() {
    this.setState({
      selectedFile: null
    });
  }

  updateUser({ firstName, lastName, percentage }) {
    this.setState({
      updateUserConfig: {
        ...this.state.updateUserConfig,
        fullName: `${firstName ? firstName : "Buddy"} ${
          lastName ? lastName : ""
          }`
      }
    });
  }

  setUserConfigHandler() {
    const {
      firstName,
      lastName,
      pic,
      userId,
      percentage
    } = gblFunc.getStoreUserDetails();
    this.setState({
      updateUserConfig: {
        fullName: `${
          firstName != ("null" || "undefined" || "") ? firstName : "Buddy"
          } ${lastName != ("null" || "undefined" || "") ? lastName : ""}`,
        pic: pic,
        userId,
        percentage
      }
    });
  }

  redirectToMembership() {
    this.props.history.push("/premium-membership");
  }

  /* side Tabs */
  tabFormHandler(event) {
    event.preventDefault();

    let updateForm = { ...this.state, ...this.state.form };

    updateForm.form["tabName"] = event.target.id;
    this.setState(updateForm);
  }

  //msg Handler/
  msgHandler() {
    this.setState({
      msgStatus: false
    });
  }

  onCloseHandler() {
    this.setState({
      isShow: false
    });
  }
  goToNextPage(tabName) {
    this.setState({
      form: { tabName }
    });
  }

  mapToPostApiParams() {
    const { adminId, id, message } = this.state;

    let params = {
      adminId,
      id,
      message
    };
    return params;
  }

  pdfDownloadHandler(scholarshipId) {
    this.props.scholarScholarshipCertificatePdfData({
      userId: gblFunc.getStoreUserDetails()["userId"],
      scholarshipId: scholarshipId
    });
  }

  render() {
    const { scholarScholarshipWonlistData } = this.state;
    const userName = gblFunc.getStoreUserDetails()["firstName"];
    const userUpdated = this.state.updateUserConfig;
    const userDetails = gblFunc.getStoreUserDetails();
    const isUserAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    if (!isUserAuthenticated) {
      return <Redirect to="/" />;
    }

    if (this.props.isError) {
      return <ServerError errorMessage={this.props.errorMessage} />;
    }

    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <DashboardUI
          src={this.state.src}
          selectedFile={this.state.selectedFile}
          uploadPic={this.props.uploadPic}
          clearSelectedFile={this.clearSelectedFile}
          type={this.props.type}
          userId={userUpdated.userId}
        />

        <section className="dashboard">
          <Helmet> </Helmet>

          <section className="conatiner-fluid">
            <article className="container dashboard-container">
              <ProfileImg
                imagefileChangedHandler={this.imagefileChangedHandler}
                userUpdated={userUpdated}
                userDetails={userDetails}
                validations={this.state.validations}
                redirectToMembership={this.redirectToMembership}
              />
              <article className="row">
                <article className="dashboard-nav-bg">
                  <article className="dashboard-nav-in ">
                    {/* SIDE NAV TABS*/}
                    <article className="col-md-12">
                      <DashboardNav
                        dashNav="scholar-tab"
                        scholarFlag={scholarScholarshipWonlistData.length}
                      />
                    </article>
                    <section className="col-md-12">
                      <section className="scholarTabContentWrapper">
                        {" "}
                        <article className="tabContent">
                          <article className="contentRow">
                            {/* Scholarship won */}
                            {this.state.activeTabIndex == 0 && (
                              <TabScholarshipWon
                                msgStatus={this.state.msgStatus}
                                msgHandler={this.msgHandler}
                                scholarScholarshipWonlistData={
                                  scholarScholarshipWonlistData
                                }
                                firstName={
                                  gblFunc.getStoreUserDetails()["firstName"]
                                }
                                pdfDownloadHandler={this.pdfDownloadHandler}
                              />
                            )}
                          </article>
                        </article>
                      </section>
                    </section>
                    {/* SIDE NAV TABS*/}
                  </article>
                </article>
              </article>
            </article>
          </section>
        </section>
      </section>
    );
  }
}

const TabScholarshipWon = props => {
  var date = moment(new Date());
  const publishDate = moment(
    props.scholarScholarshipWonlistData &&
    props.scholarScholarshipWonlistData.length > 0 &&
    props.scholarScholarshipWonlistData[0].publishDate
  );
  const expireyDays = date.diff(publishDate, "days");
  return (
    <section className="tabScholarshipWon">
      {props.scholarScholarshipWonlistData.length > 0 ? (
        <article>
          {expireyDays <= 10 && (
            <article
              className={`msgNotification ${props.msgStatus ? "in" : "out"}`}
            >
              <p>
                Hi <i>{props.firstName}</i>, Congratulations! You are among our
                proud scholars now. As a next step towards scholarship
                disbursement process, please update your profile, bank details,
                institute details and other documents. For any support, visit
                our help section OR email us at{" "}
                <a href="mailto:info@buddy4study.com">info@buddy4study.com</a>.
              </p>
              <span className="closeMsg" onClick={props.msgHandler} />
            </article>
          )}
          <ul className="scholarshipList">
            {props.scholarScholarshipWonlistData.map(list => {
              return (
                <li key={list.bsid}>
                  <article className="listShadow">
                    <span>
                      <Link to={`scholarship/${list.slug}`}>{list.title}</Link>
                    </span>
                    <i
                      onClick={() =>
                        props.pdfDownloadHandler(list.scholarshipId)
                      }
                    >
                      Download Certificate
                    </i>
                  </article>
                  <span className="supportLink">
                    Support:
                    {list.contactEmail !== null ? (
                      <i>
                        [Email:{" "}
                        <a href={`mailto:${list.contactEmail}`}>
                          {list.contactEmail}
                        </a>]
                      </i>
                    ) : (
                        <i>
                          [Email: <i>NA</i>]
                      </i>
                      )}
                    {list.contactNumber !== null ? (
                      <i>
                        [Contact:{" "}
                        <a href={`tel:${list.contactNumber}`}>
                          {list.contactNumber}
                        </a>]
                      </i>
                    ) : (
                        <i>
                          [Contact: <i>NA</i>]
                      </i>
                      )}
                  </span>
                </li>
              );
            })}
          </ul>
        </article>
      ) : (
          <p>
            You are not a scholar yet. Kindly{" "}
            <Link to="/scholarships">click here</Link> to apply for scholarship.
        </p>
        )}
    </section>
  );
};
export default Scholar;
