import React, { Component, Fragment } from "react";
import { Redirect, Link } from "react-router-dom";
import DatePicker from "react-datepicker";
import moment from "moment";
if (typeof window !== "undefined") {
	require("react-datepicker/dist/react-datepicker.css");
}
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import ServerError from "../../../common/components/serverError";
import { dependantRequests } from "../../../../constants/constants";
import {
	FETCH_USERLIST_SUCCEEDED,
	FETCH_FILTER_RULES_SUCCEEDED,
	FETCH_RULES_SUCCEEDED,
	FETCH_USER_RULES_SUCCESS,
	UPDATE_USER_RULES_SUCCESS
} from "../../../../constants/commonActions";
import {
	FETCH_UPDATE_USER_SUCCEEDED,
	FETCH_EDUCATION_INFO_SUCCESS,
	UPDATE_EDUCATION_INFO_SUCCESS
} from "../../actions";
import { ruleRunner } from "../../../../validation/ruleRunner";
import gblFunc from "../../../../globals/globalFunctions";
import Loader from "../../../common/components/loader";
import {
	required,
	isEmail,
	minLength,
	isNumeric,
	lengthRange,
	isMobileNumber,
	isPINCODE,
	isAadhar
} from "../../../../validation/rules";
import { isOneCheckBoxSelected } from "../../../../validation/errorMessages";
import UpdateMobileEmail from "./../../components/profile-info/updateMobEmail";

const hideSubject = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 25, 577, 752];
const elementryClass = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 25, 752];
const othersClass = [577];

class PersonalInfo extends Component {
	constructor(props) {
		super(props);

		this.currentFormChangeHandler = this.currentFormChangeHandler.bind(this);
		this.currentFormRulesHandler = this.currentFormRulesHandler.bind(this);
		this.getRuleId = this.getRuleId.bind(this);
		this.getDistrictList = this.getDistrictList.bind(this);
		this.checkFormValidations = this.checkFormValidations.bind(this);
		this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
		this.checkStateHandler = this.checkStateHandler.bind(this);
		this.onCheckBoxToggle = this.onCheckBoxToggle.bind(this);
		//this.onClickHandler = this.onClickHandler.bind(this);
		this.createScholarshipRulesAndfetchUserRules = this.createScholarshipRulesAndfetchUserRules.bind(
			this
		);
		this.onEducationSubmitHandler = this.onEducationSubmitHandler.bind(this);

		this.oldEmailHandle = this.oldEmailHandle.bind(this);
		this.oldMobileHandle = this.oldMobileHandle.bind(this);

		this.state = {
			//scholarshipCheckboxes: {},
			//checkedCheckboxes: [],
			isChangeMobileEmail: false,
			isEduValid: true,
			eduInfo: {
				userAcademicInfo: {
					academicClass: { id: null, value: null },
					board: null,
					courseDuration: "",
					currentDegreeYear: null,
					degree: null,
					// fee: null,
					id: null,
					otherBoard: "",
					passingMonth: null,
					passingYear: null,
					percentage: null,
					presentClass: 1,
					stream: null,
					marksObtained: "",
					totalMarks: null,
					grade: "",
					markingType: "",
					verifiedPresentClass: null,
					currentAcademicSem:null,
					currentAcademicYear:null
				},
				userInstituteInfo: {
					// academicDetailId: null,
					address: "",
					city: "",
					// country: "",
					// description: "",
					district: null,
					id: null,
					instituteEmail: "",
					instituteName: "",
					//institutePhone: "",
					pincode: null,
					// principalName: "",
					state: null
				}
			},
			PERSONAL_INFO: {
				firstName: "",
				lastName: "",
				email: "",
				mobile: "",
				dob: "",
				aadharCard: "",
				familyIncome: "",
				portalId: 0,

				userAddress: {
					addressLine: "",
					city: "",
					country: "",
					district: { id: null, value: null },
					id: 0,
					pincode: "",
					state: { id: null, value: null },
					type: 1 //permanent address
				},
				userRules: []
			},

			rules: {},
			dropDownRules: {
				religion: {
					ruleId: null,
					ruleTypeId: 4
				},
				class: {
					ruleId: null,
					ruleTypeId: 1
				},
				quota: {
					ruleId: null,
					ruleTypeId: 6
				},
				gender: {
					ruleId: null,
					ruleTypeId: 5
				},
				disabled: {
					ruleId: 903,
					ruleTypeId: 48
				},
				foreign: {
					ruleId: 820,
					ruleTypeId: 46
				}
			},
			userid: null,
			ON_FIRST_LOAD: true,
			validations: {
				firstName: null,
				lastName: null,
				email: null,
				mobile: null,
				familyIncome: null,
				quota: null,
				district: null,
				religion: null,
				gender: null,
				state: null,
				gender: null,
				academicClass: null,
				stream: null
			},
			interestValidation: null,
			isFormValid: false,
			isFilter: true
		};
	}

	componentWillReceiveProps(nextProps) {
		switch (nextProps.type) {
			case FETCH_UPDATE_USER_SUCCEEDED:
				const { firstName, lastName } = nextProps.updateUserInfo;
				localStorage.setItem("firstName", firstName);
				localStorage.setItem("lastName", lastName);
				this.props.updateUser({ firstName, lastName });
				 this.onEducationSubmitHandler();
				break;
			case FETCH_USERLIST_SUCCEEDED:
				const { userList } = nextProps;
				if (userList && userList.userAddress && userList.userAddress["state"]) {
					if (
						userList.userAddress["state"] &&
						!this.props.district.length &&
						this.state.ON_FIRST_LOAD
					) {
						this.getDistrictList("state", userList.userAddress["state"]["id"]);
					}
					this.setState({ ON_FIRST_LOAD: false });
				}
				this.props.fetchEducationInfoDetails({ userId: this.props.userId });
				break;
			case FETCH_EDUCATION_INFO_SUCCESS:
				this.mapUserApiToState(nextProps);
				if (this.props.rulesList) {
					const { special } = this.props.rulesList;
					this.createScholarshipRulesAndfetchUserRules(special);
				}
				if (
					nextProps.educationData &&
					nextProps.educationData.length > 0 &&
					nextProps.educationData[0].userAcademicInfo &&
					nextProps.educationData[0].userAcademicInfo.academicClass
				) {
					return this.setState({ verifiedPresentClass: nextProps.educationData[0].userAcademicInfo.academicClass.id })
				}
				break;
			// case FETCH_RULES_SUCCEEDED:
			//   const { special } = nextProps.rulesList;
			//   this.createScholarshipRulesAndfetchUserRules(special);

			//   break;
			case FETCH_USER_RULES_SUCCESS:
				if (nextProps.userRulesData) {
					let { userRules } = nextProps.userRulesData;

					let filteredUserRules = nextProps.userRulesData.userRules.filter(
						userRule => userRule.ruleTypeId === 7
					);

					userRules = filteredUserRules.reduce((acc, rule, i) => {
						acc[rule.ruleId] = true;
						return acc;
					}, {});

					let checkedCheckboxes = filteredUserRules.map(rule =>
						parseInt(rule.ruleId, 10)
					);

					let scholarshipCheckboxes = {
						...this.state.scholarshipCheckboxes,
						...userRules
					};

					this.setState({ scholarshipCheckboxes, checkedCheckboxes });
				}

				break;
			case UPDATE_EDUCATION_INFO_SUCCESS:
				this.props.goToNextPage("Education");
				gblFunc.scrollPage(this.refs.personalProfile, "personalProfile");
				break;
		}
	}

	mapUserApiToState(nextProps) {
		const updatePersonal = { ...this.state.PERSONAL_INFO };
		const updateRules = { ...this.state.dropDownRules };
		const { eduInfo, isEduValid, isFilter } = this.state;
		const { userList, educationData, userRulesData } = nextProps;
		// const updateUserRule = { ...this.state.USER_RULE };
		if (userList) {
			for (let i in updatePersonal) {
				if (i !== "userRule") {
					updatePersonal[i] =
						i == "userAddress" && !userList[i]
							? updatePersonal[i]
							: userList[i];

					this.setState({ userid: this.props.userId });
				}
			}

			if (userList.userRules && userList.userRules.length > 0) {
				let resRules = userList.userRules;
				resRules.map(list => {
					for (let key in updateRules) {
						if (updateRules[key].ruleTypeId == list.ruleTypeId) {
							updateRules[key].ruleId = list.ruleId;
						}
					}
				});
			}
		}

		let { serverError } = nextProps;
		if (serverError) {
			let validations = { ...this.state.validations };
			validations.city = serverError;
			this.setState({
				validations
			});
		}

		if (educationData && educationData.length > 0 && isEduValid) {
			const educationInfo = educationData.filter(
				eduInfo => eduInfo.userAcademicInfo.presentClass == 1
			);

			const { userAcademicInfo, userInstituteInfo } =
				educationInfo[0] || eduInfo;

			if (userAcademicInfo && userInstituteInfo) {
				this.mapApiToCurrentState(
					userAcademicInfo,
					"userAcademicInfo",
					nextProps.type
				);
				this.mapApiToCurrentState(
					userInstituteInfo,
					"userInstituteInfo",
					nextProps.type
				);
			}
			if (
				userAcademicInfo &&
				userAcademicInfo["academicClass"] &&
				nextProps.type == "FETCH_EDUCATION_INFO_SUCCESS" &&
				isFilter
			) {
				this.getFilterRules(userAcademicInfo["academicClass"]["id"]);
			}
		}

		if (educationData.length === 0) {
			const rules = { ...this.state.dropDownRules };
			if (rules.class && rules.class.ruleId) {
				this.state.eduInfo.userAcademicInfo.academicClass.id =
					rules.class.ruleId;
				this.getFilterRules(
					this.state.eduInfo.userAcademicInfo.academicClass.id
				);
			}
		}

		this.setState({
			...this.state,
			PERSONAL_INFO: updatePersonal,
			dropDownRules: updateRules,
			isFilter: false
			// USER_RULE: updateUserRule
		});
	}

	mapApiToCurrentState(userEduData, KEY, type) {
		const userUpdatedState = { ...this.state.eduInfo };
		for (let key in userUpdatedState[KEY]) {
			userUpdatedState[KEY][key] = userEduData[key];
		}

		this.setState({
			...this.state,
			eduInfo: userUpdatedState,
			isEduValid: false
		});
	}

	componentDidMount() {
		this.props.loadRules();
		if (this.props.userId) {
			this.props.loadUserList(this.props.userId);
		}
	}

	getFilterRules(ruleId) {
		this.props.fetchFilterRuleById(ruleId);
	}

	onSubmitHandler(event) {
		event.preventDefault();
		const userRules = [{ ruleId: 120, ruleTypeId: 7 },
		{ ruleId: 121, ruleTypeId: 7 }];
		let userRulesApiParams = null;
		const personal_info = {
			...this.state.PERSONAL_INFO,
			...this.state.PERSONAL_INFO.userAddress
		};
		const address = { ...this.state.PERSONAL_INFO.userAddress };
		const updaterules = { ...this.state.dropDownRules };

		for (let val in updaterules) {
			userRules.push(updaterules[val]);
		}

		/* if (this.state.checkedCheckboxes.length > 0) {
			userRulesApiParams = this.state.checkedCheckboxes.map(rule => ({
				ruleId: rule,
				ruleTypeId: 7
			})); */

		/* this.props.addOrUpdateUserRules({
			userid: this.props.userId,
			params: userRulesApiParams
		}); */
		// } else {
		// 	const interestValidation = isOneCheckBoxSelected("Scholarship interest");
		// 	this.setState({
		// 		interestValidation
		// 	});
		// 	// return;
		// }

		personal_info.userRules = userRules;

		if (
			this.checkFormValidations()
		) {
			this.props.loadSaveUser(personal_info, this.props.userId); //this.props.userId
		}
		this.setState({ isFormValid: false });
	}

	onEducationSubmitHandler() {
		const updateEducation = [{ ...this.state.eduInfo }];
		if (this.checkFormValidations()) {
			this.props.updateEducationInfo(updateEducation);
		}
		this.setState({ isFormValid: false });
	}

	/* onClickHandler() {
		if (this.state.checkedCheckboxes.length > 0) {
			const userRulesApiParams = this.state.checkedCheckboxes.map(rule => ({
				ruleId: rule,
				ruleTypeId: 7
			}));

			this.props.addOrUpdateUserRules({
				userid: this.props.userId,
				params: userRulesApiParams
			});
		} else {
			const interestValidation = isOneCheckBoxSelected("Scholarship interest");
			this.setState({
				interestValidation
			});
		}
	} */

	createScholarshipRulesAndfetchUserRules(scholarshipRules) {
		const scholarshipTypesObject = scholarshipRules.reduce(
			(acc, curScholarship, i) => {
				acc[curScholarship.id] = false;
				return acc;
			},
			{}
		);

		this.setState({ scholarshipCheckboxes: scholarshipTypesObject }, () =>
			this.props.fetchUserRules({ userid: this.props.userId })
		);
	}

	currentFormChangeHandler(event, KEY, SUBKEY = "") {
		let index =
			event.nativeEvent && event.nativeEvent.target
				? event.nativeEvent.target.selectedIndex
				: null;
		const updatePersonForm = { ...this.state.PERSONAL_INFO };

		const { id, value } =
			KEY == "dob"
				? { id: "dob", value: moment(event).format("YYYY-MM-DD") }
				: event.target;
		let validations = { ...this.state.validations };

		if (validations.hasOwnProperty(id)) {
			const { name, validationFunctions } = this.getValidationRulesObject(id);
			const validationResult = ruleRunner(
				value,
				id,
				name,
				...validationFunctions
			);

			validations[id] = validationResult[id];
		}

		if (!SUBKEY) {
			updatePersonForm[KEY] = value;
		} else {
			let typeOfProp = {
				addressLine: value,
				city: value,
				country: value,
				district: {
					id: +value,
					value:
						event.nativeEvent.target && event.nativeEvent.target[index]
							? event.nativeEvent.target[index].text
							: null
				},
				id: +value,
				pincode: value,
				state: {
					id: +value,
					value:
						event.nativeEvent.target && event.nativeEvent.target[index]
							? event.nativeEvent.target[index].text
							: null
				},
				familyIncome: value
			};
			updatePersonForm[SUBKEY]
				? (updatePersonForm[SUBKEY][KEY] = typeOfProp[KEY])
				: null;
		}

		if (id == "academicClass") {
			this.getFilterRules(value);
		}

		// let keyToUpperCase = KEY.toUpperCase();
		if (dependantRequests.includes(KEY)) {
			//TODO: Call dependant key request , pick from props in container
			this.getDistrictList(KEY, event.target.value);
			this.setState({ ON_FIRST_LOAD: false });
		}

		this.setState({ PERSONAL_INFO: updatePersonForm, validations });
	}

	eduCurrentFormChangeHandler(event, PARENT_KEY) {
		const { id, value } = event.target;
		const updateEdu = { ...this.state.eduInfo };
		let validations = { ...this.state.validations };
		let index =
			event.nativeEvent && event.nativeEvent.target
				? event.nativeEvent.target.selectedIndex
				: null;

		if (validations.hasOwnProperty(id)) {
			const { name, validationFunctions } = this.getValidationRulesObject(id);
			const validationResult = ruleRunner(
				value,
				id,
				name,
				...validationFunctions
			);

			validations[id] = validationResult[id];
		}

		updateEdu[PARENT_KEY][id] =
			id == "academicClass"
				? {
					id: +value,
					value:
						event.nativeEvent.target && event.nativeEvent.target[index]
							? event.nativeEvent.target[index].text
							: null
				}
				: value;

		if (id == "academicClass") {
			this.getFilterRules(value);
		}

		this.setState({ eduInfo: updateEdu, validations, isFormValid: false });
	}

	currentFormRulesHandler(event, KEY) {
		let index = event.nativeEvent.target.selectedIndex;
		const { id, value } = event.target;
		let validations = { ...this.state.validations };

		if (validations.hasOwnProperty(id)) {
			const { name, validationFunctions } = this.getValidationRulesObject(id);
			const validationResult = ruleRunner(
				value,
				id,
				name,
				...validationFunctions
			);

			validations[id] = validationResult[id];
		}

		const userRule = {};
		const updateUserRules = { ...this.state.dropDownRules };

		updateUserRules[KEY].ruleId = +value;

		this.setState({
			dropDownRules: updateUserRules,
			validations
		});
	}

	getDistrictList(key, value) {
		this.props.loadDistrictList({
			KEY: key,
			DATA: value
		});
	}

	onCheckBoxToggle(event) {
		const { id, value } = event.target;
		//toggle checkbox state
		const checkBoxState = !this.state.scholarshipCheckboxes[parseInt(id, 10)];

		const scholarshipCheckboxes = {
			...this.state.scholarshipCheckboxes,
			[parseInt(id, 10)]: checkBoxState
		};

		let checkedCheckboxes = [...this.state.checkedCheckboxes];
		checkedCheckboxes = checkedCheckboxes.includes(parseInt(id, 10))
			? checkedCheckboxes.filter(chkBox => chkBox != parseInt(id, 10))
			: checkedCheckboxes.concat(parseInt(id, 10));

		this.setState({ scholarshipCheckboxes, checkedCheckboxes });
	}

	getRuleId(ruleTypeId) {
		const updatedrules = this.state.PERSONAL_INFO.userRules;
		let ruleid = [];
		if (updatedrules.length > 0) {
			ruleid = updatedrules.filter(list => list.ruleTypeId == ruleTypeId);
		}
		return ruleid.length > 0 ? ruleid[0].ruleId : "";
	}

	checkStateHandler(key) {
		const currentState = this.state.PERSONAL_INFO;
		const eduCurrentState = this.state.eduInfo;
		let rules = { ...this.state.dropDownRules };

		let address = {
			state: "userAddress",
			district: "userAddress",
			addressLine: "userAddress",
			pincode: "userAddress",
			city: "userAddress",
			country: "userAddress"
		};

		let educationSection = {
			stream: "userAcademicInfo",
			academicClass: "userAcademicInfo"
		};

		if (address[key] && currentState[address[key]]) {
			if (key === "state" || key === "district") {
				return currentState[address[key]][key]
					? currentState[address[key]][key].id
					: null;
			} else {
				return currentState[address[key]][key];
			}
		} else if (
			educationSection[key] &&
			eduCurrentState[educationSection[key]][key] &&
			key == "academicClass"
		) {
			return eduCurrentState[educationSection[key]][key]
				? eduCurrentState[educationSection[key]][key].id
				: null;
		} else if (educationSection[key] && key == "stream") {
			return eduCurrentState[educationSection[key]][key]
				? eduCurrentState[educationSection[key]][key]
				: null;
		} else if (rules[key]) {
			return rules[key].ruleId;
		} else {
			return key == "mobile" && currentState[key]
				? currentState[key].toString()
				: currentState[key];
		}
	}

	/************ start - validation part - get message and required validation******************* */
	getValidationRulesObject(fieldID) {
		let validationObject = {};
		switch (fieldID) {
			case "firstName":
				validationObject.name = "* First name";
				validationObject.validationFunctions = [required, lengthRange(3, 40)];
				return validationObject;
			case "lastName":
				validationObject.name = "* Last name";
				validationObject.validationFunctions = [lengthRange(3, 40)];
				return validationObject;
			case "pincode":
				validationObject.name = "* Pin code";
				validationObject.validationFunctions = [required, isNumeric, isPINCODE];
				return validationObject;
			case "email":
				validationObject.name = "* Email ";
				validationObject.validationFunctions = [required, isEmail];
				return validationObject;
			case "mobile":
				validationObject.name = "* Mobile number ";
				validationObject.validationFunctions = [
					required,
					isNumeric,
					isMobileNumber,
					minLength(10)
				];
				return validationObject;
			case "aadharCard":
				validationObject.name = "* Aadhaar number  ";
				validationObject.validationFunctions = [
					required,
					isAadhar,
					minLength(12)
				];
				return validationObject;
			case "addressLine":
				validationObject.name = "* Address ";
				validationObject.validationFunctions = [required];
				return validationObject;
			case "district":
				validationObject.name = "* District ";
				validationObject.validationFunctions = [required];
				return validationObject;
			case "city":
				validationObject.name = "* City ";
				validationObject.validationFunctions = [required];
				return validationObject;
			case "state":
				validationObject.name = "* State ";
				validationObject.validationFunctions = [required];
				return validationObject;
			case "gender":
				validationObject.name = "* Gender ";
				validationObject.validationFunctions = [required];
				return validationObject;
			case "religion":
				validationObject.name = "* Religion ";
				validationObject.validationFunctions = [required];
				return validationObject;
			case "quota":
				validationObject.name = "* Category ";
				validationObject.validationFunctions = [required];
				return validationObject;
			case "familyIncome":
				validationObject.name = "* Annual family income ";
				validationObject.validationFunctions = [required, isNumeric];
				return validationObject;
			case "academicClass":
				validationObject.name = "* Present class/Degree";
				validationObject.validationFunctions = [required];
				return validationObject;
			case "stream":
				validationObject.name = "* Stream";
				validationObject.validationFunctions = [required];
				return validationObject;
		}
	}
	/************ end - validation part - get message and required validation******************* */

	/************ start - validation part - check form submit validation******************* */
	checkFormValidations() {
		let validations = { ...this.state.validations };

		const { userAcademicInfo } = this.state.eduInfo;
		let isFormValid = true;

		if (hideSubject.indexOf(+userAcademicInfo.academicClass.id) > -1) {
			delete validations["stream"];
		}

		for (let key in validations) {
			let { name, validationFunctions } = this.getValidationRulesObject(key);

			let validationResult = ruleRunner(
				this.checkStateHandler(key),
				key,
				name,
				...validationFunctions
			);
			validations[key] = validationResult[key];
			if (validationResult[key] !== null) {
				isFormValid = false;
			}
		}
		this.setState({
			validations,
			isFormValid
		});
		return isFormValid;
	}
	/************ start - validation part - check form submit validation******************* */

	oldEmailHandle(oldEmail) {
		typeof window !== 'undefined' && localStorage.setItem('oldEmail', oldEmail)
	}
	oldMobileHandle(oldMobile) {
		typeof window !== 'undefined' && localStorage.setItem('oldMobile', oldMobile)
	}

	render() {
		let personalFormContent = null;
		const filterRules = this.props.filterRules ? this.props.filterRules : [];
		const { addressLine, city, country, district, pincode, state, type } = this
			.state.PERSONAL_INFO.userAddress
			? this.state.PERSONAL_INFO.userAddress
			: {
				addressLine: "",
				city: "",
				country: "",
				district: { id: null, value: null },
				pincode: "",
				state: { id: null, value: null },
				type: 1
			};

		const {
			religion,
			gender,
			disabled,
			quota,
			foreign
		} = this.state.dropDownRules;

		const { isChangeMobileEmail } = this.state;
		const { rulesList, userList } = this.props;
		console.log(userList && userList.dob, '[userList]')
		const { userAcademicInfo } = this.state.eduInfo;
		// if (this.props.isError) {
		//   return <ServerError errorMessage={this.props.errorMessage} />;
		// }
		const isReligion = userList && userList.userRules && userList.userRules.length && userList.userRules.find(x => x.ruleTypeId == 4 && x.ruleValue ? x.ruleValue : "")
		const isGender = userList && userList.userRules && userList.userRules.length && userList.userRules.find(x => x.ruleTypeId == 5 && x.ruleValue ? x.ruleValue : "")
		if (rulesList && userList) {
			personalFormContent = (
				<Fragment>
					<article className="ctrl-wrapper widget-border">
						<h2 className="boxTitle" ref="personalProfile">
							Personal Information
          				</h2>
						<article className="ctrl-wrapper">
							<article className="form-group">
								<label htmlFor="firstName">First Name*</label>
								<input
									type="text"
									maxLength="80"
									minLength="1"
									className="form-control"
									id="firstName"
									value={
										this.state.PERSONAL_INFO.firstName
											? this.state.PERSONAL_INFO.firstName
											: ""
									}
									disabled={userList.firstName ? true : false}
									onChange={event =>
										this.currentFormChangeHandler(event, "firstName")
									}
									name="firstName"
									placeholder="Enter your first name"
								/>
								{this.state.validations.firstName ? (
									<span className="error animated bounce">
										{this.state.validations.firstName}
									</span>
								) : null}
							</article>
							<article className="form-group">
								<label htmlFor="lastName">Last Name</label>
								<input
									type="text"
									maxLength="80"
									minLength="1"
									className="form-control"
									name="lastName"
									id="lastName"
									value={
										this.state.PERSONAL_INFO.lastName
											? this.state.PERSONAL_INFO.lastName
											: ""
									}
									disabled={userList.lastName ? true : false}
									onChange={event =>
										this.currentFormChangeHandler(event, "lastName")
									}
									placeholder="Enter your last name"
								/>
								{this.state.validations.lastName ? (
									<span className="error animated bounce">
										{this.state.validations.lastName}
									</span>
								) : null}
							</article>
							<article className="form-group">
								<label htmlFor="email">Email*</label>
								<input
									type="email"
									name="email"
									className="form-control"
									id="email"
									value={
										this.state.PERSONAL_INFO.email
											? this.state.PERSONAL_INFO.email
											: ""
									}
									onChange={event =>
										this.currentFormChangeHandler(event, "email")
									}
									placeholder="Enter your email"
									disabled={userList.email ? true : false}
								/>
								{this.state.validations.email ? (
									<span className="error animated bounce">
										{this.state.validations.email}
									</span>
								) : null}
								<Link
									className="updateBtn"
									onClick={() => [
										oldEmailHandle(this.state.PERSONAL_INFO.email),
										this.props.history.push(<Redirect to="/myProfile/UpdateEmail" />)
									]}
									to="/myProfile/UpdateEmail"
								>Change Email ID</Link>

								{this.state.validations.email ? (
									<span className="error animated bounce">
										{this.state.validations.email}
									</span>
								) : null}
							</article>
							<article className="form-group">
								<label htmlFor="mnumber">Mobile Number*</label>
								<input
									maxLength="10"
									minLength="10"
									type="text"
									name="mobile"
									placeholder="Enter your mobile"
									className="form-control"
									value={
										this.state.PERSONAL_INFO.mobile
											? this.state.PERSONAL_INFO.mobile
											: ""
									}
									disabled={userList.mobile ? true : false}
									onChange={event =>
										this.currentFormChangeHandler(event, "mobile")
									}
									id="mobile"
								/>
								{this.state.validations.mobile ? (
									<span className="error animated bounce">
										{this.state.validations.mobile}
									</span>
								) : null}

								<Link
									className="updateBtn"
									onClick={() => [
										oldMobileHandle(this.state.PERSONAL_INFO.mobile),
										this.props.history.push(<Redirect to="/myProfile/UpdateMobile" />)
									]}
									to="/myProfile/UpdateMobile"
								>Change Mobile No.
								</Link>
								{this.state.validations.mobile ? (
									<span className="error animated bounce">
										{this.state.validations.mobile}
									</span>
								) : null}
							</article>

							<article className="form-group">
								<label htmlFor="dob">Date of Birth</label>
								<article className="input-group date">
									<DatePicker
										showYearDropdown
										scrollableYearDropdown
										yearDropdownItemNumber={40}
										minDate={moment().subtract(100, "years")}
										maxDate={moment().add(1, "years")}
										name="dob"
										id="dob"
										className="form-control"
										autoCapitalize="off"
										selected={
											this.state.PERSONAL_INFO.dob
												? moment(
													moment(this.state.PERSONAL_INFO.dob).format(
														"DD-MM-YYYY"
													),
													"DD-MM-YYYY"
												)
												: null
										}
										onChange={event =>
											this.currentFormChangeHandler(event, "dob")
										}
										//disabled={userList.dob ? true : false}
										dateFormat="DD-MM-YYYY"
									/>
									<label htmlFor="dob" className="input-group-addon">
										<i className="fa fa-calendar" aria-hidden="true" />
									</label>
								</article>
							</article>
							<article className="form-group">
								<label htmlFor="gender">Gender*</label>
								<select
									name="gender"
									className="form-control"
									id="gender"
									onChange={event =>
										this.currentFormRulesHandler(event, "gender")
									}
									value={gender.ruleId ? gender.ruleId : ""}
									disabled={isGender && isGender.ruleId ? true : false}
								>
									<option value="">--Gender--</option>
									{this.props.rulesList["gender"]
										.filter(gList => gList["id"] !== 229)
										.map(list => {
											return (
												<option key={list.id} value={list.id}>
													{list.rulevalue}
												</option>
											);
										})}
								</select>
								{this.state.validations.gender ? (
									<span className="error animated bounce">
										{this.state.validations.gender}
									</span>
								) : null}
							</article>

							<article className="form-group">
								<label htmlFor="state">State*</label>
								<select
									className="form-control"
									id="state"
									value={state ? (state.id ? state.id : "") : ""}
									onChange={event =>
										this.currentFormChangeHandler(event, "state", "userAddress")
									}
								>
									<option value="">--Select State--</option>
									{this.props.rulesList["state"]
										.filter(fList => fList["id"] !== 447)
										.map(list => {
											return (
												<option
													key={list.id}
													name={list.rulevalue}
													value={list.id}
												>
													{list.rulevalue}
												</option>
											);
										})}
								</select>
								{this.state.validations.state ? (
									<span className="error animated bounce">
										{this.state.validations.state}
									</span>
								) : null}
							</article>
							<article className="form-group">
								<label htmlFor="district">District*</label>
								<select
									name="district"
									className="form-control"
									id="district"
									value={district ? district.id : ""}
									onChange={event =>
										this.currentFormChangeHandler(
											event,
											"district",
											"userAddress"
										)
									}
								>
									<option value="">--District--</option>
									{this.props.district.map(list => {
										return (
											<option key={list.id} value={list.id}>
												{list.districtName}
											</option>
										);
									})}
								</select>
								{this.state.validations.district ? (
									<span className="error animated bounce">
										{this.state.validations.district}
									</span>
								) : null}
							</article>

							<article className="form-group">
								<label htmlFor="religion">Religion*</label>
								<select
									name="religion"
									value={religion.ruleId ? religion.ruleId : ""}
									onChange={event =>
										this.currentFormRulesHandler(event, "religion")
									}
									disabled={isReligion && isReligion.ruleId ? true : false}
									className="form-control"
									id="religion"
								>
									<option value="">--Religion--</option>
									{this.props.rulesList["religion"].map(list => {
										return (
											<option key={list.id} value={list.id}>
												{list.rulevalue}
											</option>
										);
									})}
								</select>
								{this.state.validations.religion ? (
									<span className="error animated bounce">
										{this.state.validations.religion}
									</span>
								) : null}
							</article>
							<article className="form-group">
								<label htmlFor="category">Category*</label>
								<select
									name="category"
									value={quota.ruleId ? quota.ruleId : ""}
									onChange={event => this.currentFormRulesHandler(event, "quota")}
									className="form-control"
									id="quota"
								>
									<option value="">--Category--</option>
									{this.props.rulesList["quota"].map(list => {
										return (
											<option key={list.id} value={list.id}>
												{list.rulevalue}
											</option>
										);
									})}
								</select>
								{this.state.validations.quota ? (
									<span className="error animated bounce">
										{this.state.validations.quota}
									</span>
								) : null}
							</article>
						</article>
					</article>
					<article className="ctrl-wrapper widget-border">
						<h3 className="boxTitle" ref="personalProfile">
							Education Information
          </h3>

						{/* Education Info. */}
						<article className="ctrl-wrapper">
							<article className="form-group">
								<label htmlFor="acadSub" className="control-label">
									Present Class/Degree*
              					</label>
								<select
									name="academicClass"
									className="form-control"
									id="academicClass"
									value={
										userAcademicInfo.academicClass.id
											? userAcademicInfo.academicClass.id
											: ""
									}
									
									onChange={event =>
										this.eduCurrentFormChangeHandler(event, "userAcademicInfo")
									}
								>
									<option value="">--Select Class/Degree--</option>
									{this.props.rulesList["class"].map(list => {
										return (
											<option key={list.id} value={list.id}
											//  disabled={this.state.verifiedPresentClass ? !(this.state.verifiedPresentClass == list.id ||
											// 	 (moment(moment().format('YYYY-MM-DD')).diff(moment(this.props.educationData[0].userAcademicInfo.lastUpdatedAt, 'YYYY-MM-DD'), 'months') >= 4 &&
											// 	  gblFunc.checkPresentClass(this.state.verifiedPresentClass, list.id))) : false
											// }
											disabled={this.state.verifiedPresentClass ?
												!(this.state.verifiedPresentClass == list.id ||
													gblFunc.checkPresentClass(this.state.verifiedPresentClass, list.id)) : false
											  }
											>
												{list.rulevalue}
											</option>
										);
									})}
								</select>

								{this.state.validations.academicClass ? (
									<span className="error animated bounce">
										{this.state.validations.academicClass}
									</span>
								) : null}
							</article>
							<Subject
								academicClass={userAcademicInfo.academicClass}
								filterRules={filterRules}
								subjectValid={this.state.validations.stream}
								stream={userAcademicInfo.stream}
								currentFormChangeHandler={event =>
									this.eduCurrentFormChangeHandler(event, "userAcademicInfo")
								}
							/>
						</article>
					</article>
					<article className="ctrl-wrapper widget-border">
						<h4 className="boxTitle" ref="personalProfile">
							Other Information
          				</h4>
						{/* Other Info. */}
						<article className="ctrl-wrapper">
							<article className="form-group">
								<label htmlFor="family_income">Annual Family Income*</label>
								<article className="input-group inputWrapper">
									<span className="input-group-addon preContent">INR</span>
									<input
										type="text"
										id="familyIncome"
										name="annual_family_income"
										className="form-control prepost-input"
										value={
											this.state.PERSONAL_INFO.familyIncome
												? this.state.PERSONAL_INFO.familyIncome
												: ""
										}
										onChange={event =>
											this.currentFormChangeHandler(event, "familyIncome")
										}
										aria-label="Amount (to the nearest dollar)"
									/>
									<span className="input-group-addon postContent">Annual</span>
								</article>
								{this.state.validations.familyIncome ? (
									<span className="error animated bounce">
										{this.state.validations.familyIncome}
									</span>
								) : null}
							</article>
							<article className="form-group">
								<label>
									Physically Challenged
              </label>
								{/* <select
                  name="physically-challenged"
                  className="form-control"
                  id="physically-challenged"
                  value={disabled.ruleId}
                  onChange={event =>
                    this.currentFormRulesHandler(event, "disabled")
                  }
                >
                  <option value="903">NO</option>
                  <option value="700">YES</option>
                </select> */}

								<article className="radioCtlBtn">
									<label htmlFor="903">
										<input
											type="radio"
											id="903"
											name="physically-challenged"
											value="903"
											checked={(!!this.state.dropDownRules && !!this.state.dropDownRules.disabled && this.state.dropDownRules.disabled.ruleId == '903') ? true : false}
											onChange={event =>
												this.currentFormRulesHandler(event, "disabled")
											} />
										<dd className="radiobox">NO</dd>
									</label>
									<label htmlFor="700">
										<input
											type="radio"
											id="700"
											name="physically-challenged"
											value="700"
											checked={(!!this.state.dropDownRules && !!this.state.dropDownRules.disabled && this.state.dropDownRules.disabled.ruleId == '700') ? true : false}
											onChange={event =>
												this.currentFormRulesHandler(event, "disabled")
											} />
										<dd className="radiobox">YES</dd>
									</label>
								</article>
							</article>
							<article className="form-group">
								<label htmlFor="foreignSch">
									Are you looking for a scholarship to study abroad?
              					</label>
								<article className="radioCtlBtn">
									<label htmlFor="820">
										<input type="radio"
											id="820"
											name="foreignSch"
											value="820"
											checked={(!!this.state.dropDownRules && !!this.state.dropDownRules.foreign && this.state.dropDownRules.foreign.ruleId == '820') ? true : false}
											onChange={event =>
												this.currentFormRulesHandler(event, "foreign")
											} />
										<dd className="radiobox">NO</dd>
									</label>
									<label htmlFor="698">
										<input type="radio"
											id="698"
											name="foreignSch"
											value="698"
											checked={(!!this.state.dropDownRules && !!this.state.dropDownRules.foreign && this.state.dropDownRules.foreign.ruleId == '698') ? true : false}
											onChange={event =>
												this.currentFormRulesHandler(event, "foreign")
											} />
										<dd className="radiobox">YES</dd>
									</label>
								</article>
							</article>
						</article>
					</article>
				</Fragment>
			);
		}
		return (
			<Fragment>
				{isChangeMobileEmail ? <UpdateMobileEmail /> : (
					<section className="dashboardCtrl">
						<Loader isLoader={this.props.showLoader} />
						<form
							name="profileForm"
							autoCapitalize="off"
							onSubmit={event => this.onSubmitHandler(event)}
						>
							{personalFormContent}
							<article className="errorContainer">
								{this.props.isUpdateError ? (
									<span className="error animated bounce">
										{this.props.updateError}
									</span>
								) : null}
							</article>
							<article className="col-md-12 width100">
								<article className="btn-ctrl">
									<button type="submit">Save</button>
								</article>
							</article>
						</form>
					</section>
				)}
			</Fragment>
		);
	}
}

const DynamicInterestForm = props => (
	<article className="form-group interest">
		{props.interestValidation ? (
			<span className="error animated bounce">{props.interestValidation} </span>
		) : null}

		{props.scholarshipTypes.map(scholarshipType => (
			<CheckboxWrapper
				key={scholarshipType.id}
				data={scholarshipType}
				isChecked={props.scholarshipCheckboxes[scholarshipType.id]}
				onCheckBoxToggle={props.onCheckBoxToggle}
			/>
		))}
	</article>
);

const CheckboxWrapper = props => (
	<label htmlFor={props.data.id}>
		{props.data.rulevalue}
		<input
			type="checkbox"
			id={props.data.id}
			name={props.data.id}
			checked={props.isChecked}
			value={props.data.id ? props.data.id : ""}
			onChange={props.onCheckBoxToggle}
		/>
		<dd className="chkbox" />
	</label>
);

const Subject = props => {
	const subjectShow =
		hideSubject.indexOf(+props.academicClass.id) > -1 ? null : (
			<article className="form-group">
				<label className="control-label">Course/Subject/Stream*</label>
				<select
					name="presentclassSubject"
					className="form-control"
					id="stream"
					value={props.stream ? props.stream : ""}
					onChange={event => props.currentFormChangeHandler(event)}
				>
					<option value="">--Select Subject--</option>
					{props.filterRules.map(list => {
						return (
							<option key={list.id} value={list.id}>
								{list.rulevalue}
							</option>
						);
					})}
				</select>

				{props.subjectValid ? (
					<span className="error animated bounce">{props.subjectValid}</span>
				) : null}
				{/* <span className="error">* Subject/Stream is required.</span> */}
			</article>
		);

	return subjectShow;
};

export default PersonalInfo;
