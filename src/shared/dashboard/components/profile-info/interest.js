import React, { Component } from "react";

import { isOneCheckBoxSelected } from "../../../../validation/errorMessages";

import {
  FETCH_RULES_SUCCEEDED,
  FETCH_USER_RULES_SUCCESS,
  UPDATE_USER_RULES_SUCCESS
} from "../../../../constants/commonActions";
import { FETCH_ENTRANCE_EXAMINATIONS_DETAILS_SUCCESS } from "../../actions";
import Loader from "../../../common/components/loader";

class Interest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scholarshipCheckboxes: {},
      checkedCheckboxes: [],
      validation: null,
      isFormValid: true
    };

    this.createScholarshipRulesAndfetchUserRules = this.createScholarshipRulesAndfetchUserRules.bind(
      this
    );
    this.onCheckBoxToggle = this.onCheckBoxToggle.bind(this);
    this.onClickHandler = this.onClickHandler.bind(this);
  }

  onClickHandler(e) {
    e.preventDefault();
    if (this.state.checkedCheckboxes.length > 0) {
      const userRulesApiParams = this.state.checkedCheckboxes.map(rule => ({
        ruleId: rule,
        ruleTypeId: 7
      }));

      this.props.addOrUpdateUserRules({
        userid: this.props.userId,
        params: userRulesApiParams
      });
    } else {
      const validation = isOneCheckBoxSelected("Scholarship interest");
      this.setState({
        validation
      });
    }
  }

  onCheckBoxToggle(event) {
    const { id, value } = event.target;
    //toggle checkbox state
    const checkBoxState = !this.state.scholarshipCheckboxes[parseInt(id, 10)];

    const scholarshipCheckboxes = {
      ...this.state.scholarshipCheckboxes,
      [parseInt(id, 10)]: checkBoxState
    };

    let checkedCheckboxes = [...this.state.checkedCheckboxes];
    checkedCheckboxes = checkedCheckboxes.includes(parseInt(id, 10))
      ? checkedCheckboxes.filter(chkBox => chkBox != parseInt(id, 10))
      : checkedCheckboxes.concat(parseInt(id, 10));

    this.setState({ scholarshipCheckboxes, checkedCheckboxes });
  }

  createScholarshipRulesAndfetchUserRules(scholarshipRules) {
    const scholarshipTypesObject = scholarshipRules.reduce(
      (acc, curScholarship, i) => {
        acc[curScholarship.id] = false;
        return acc;
      },
      {}
    );

    this.setState({ scholarshipCheckboxes: scholarshipTypesObject }, () =>
      this.props.fetchUserRules({ userid: this.props.userId })
    );
  }

  componentDidMount() {
    if (this.props.rulesList) {
      const { special } = this.props.rulesList;
      this.createScholarshipRulesAndfetchUserRules(special);
    } else {
      this.props.loadRules();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case FETCH_USER_RULES_SUCCESS:
        let { userRules } = nextProps.userRulesData;

        let filteredUserRules = nextProps.userRulesData.userRules.filter(
          userRule => userRule.ruleTypeId === 7
        );

        userRules = filteredUserRules.reduce((acc, rule, i) => {
          acc[rule.ruleId] = true;
          return acc;
        }, {});

        let checkedCheckboxes = filteredUserRules.map(rule =>
          parseInt(rule.ruleId, 10)
        );

        let scholarshipCheckboxes = {
          ...this.state.scholarshipCheckboxes,
          ...userRules
        };

        this.setState({ scholarshipCheckboxes, checkedCheckboxes });

        break;
      case FETCH_RULES_SUCCEEDED:
        const { special } = nextProps.rulesList;
        this.createScholarshipRulesAndfetchUserRules(special);

        break;

      case UPDATE_USER_RULES_SUCCESS:
        //Uncomment in case Save button is added, apart from Save / Next
        /* let updatedUserRulesData = nextProps.updatedUserRulesData;

        let updatedCheckBoxes = updatedUserRulesData.map(rule =>
          parseInt(rule.ruleId, 10)
        );

        let updatedUserRules = updatedUserRulesData.reduce((acc, rule, i) => {
          acc[rule.ruleId] = true;
          return acc;
        }, {});

        let updatedScholarshipCheckboxes = {
          ...this.state.scholarshipCheckboxes,
          ...updatedUserRules
        };

        this.setState({
          scholarshipCheckboxes: updatedScholarshipCheckboxes,
          checkedCheckboxes: updatedCheckBoxes
        });
      */

        this.props.goToNextPage("Documents");

        break;
    }
  }

  render() {
    const { special } = this.props.rulesList || [];

    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <form onSubmit={this.onClickHandler} autoCapitalize="off">
          <article className="ctrl-wrapper widget-border">
            <h4>Select your Interest for the Scholarship*</h4>
            <article className="col-md-12">
              <article className="ctrl-wrapper ">
                <DynamicInterestForm
                  scholarshipTypes={special}
                  scholarshipCheckboxes={this.state.scholarshipCheckboxes}
                  onCheckBoxToggle={this.onCheckBoxToggle}
                  validation={this.state.validation}
                />
              </article>
            </article>
          </article>
          <article className="col-md-12 width100">
            <article className="row">
              <article className="btn-ctrl">
                <button
                  type="submit"
                  className="btn pull-right"
                  onClick={this.onClickHandler}
                >
                  Save/Next
                </button>
              </article>
            </article>
          </article>
        </form>
      </section>
    );
  }
}

const DynamicInterestForm = props => (
  <article className="form-group interest">
    {props.validation ? (
      <span className="error animated bounce">{props.validation}</span>
    ) : null}

    {props.scholarshipTypes.map(scholarshipType => (
      <CheckboxWrapper
        data={scholarshipType}
        isChecked={props.scholarshipCheckboxes[scholarshipType.id]}
        onCheckBoxToggle={props.onCheckBoxToggle}
      />
    ))}
  </article>
);

const CheckboxWrapper = props => (
  <label htmlFor={props.data.id}>
    {props.data.rulevalue}
    <input
      type="checkbox"
      id={props.data.id}
      name={props.data.id}
      checked={props.isChecked}
      value={props.data.id}
      onChange={props.onCheckBoxToggle}
    />
    <dd className="chkbox" />
  </label>
);

export default Interest;
