import React, { Component } from "react";

import Loader from "../../../common/components/loader";
import { ruleRunner } from "../../../../validation/ruleRunner";
import {
  required,
  isEmail,
  minLength,
  isNumeric,
  lengthRange,
  isPanCard
} from "../../../../validation/rules";
import {
  UPDATE_FAMILY_EARNING_DETAILS_SUCCESS,
  DELETE_FAMILY_EARNING_DETAILS_SUCCESS,
  FETCH_OCCUPATION_DATA_SUCCESS
} from "../../actions";
import { FETCH_RULES_SUCCEEDED } from "../../../../constants/commonActions";

import { confirmationMessages } from "../../../../constants/constants";
import ConfirmMessagePopup from "../../../common/components/confirmMessagePopup";

class FamilyEarnings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      relation: null,
      qualification: null,
      occupation: null,
      absoluteIncome: null,
      goToNextPage: false,
      panNumber: "",
      showConfirmationPopup: false,
      deleteSuccessCallBack: null,
      validations: {
        relation: null,
        name: null,
        qualification: null,
        occupation: null,
        absoluteIncome: null,
        panNumber: null
      },
      isFormShown: false,
      isFormValid: false
    };

    this.showForm = this.showForm.bind(this);
    this.closeForm = this.closeForm.bind(this);
    this.formChangeHandler = this.formChangeHandler.bind(this);
    this.addOrUpdateUserDetails = this.addOrUpdateUserDetails.bind(this);
    this.editClickHandler = this.editClickHandler.bind(this);
    this.deleteClickHandler = this.deleteClickHandler.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.saveAndGoToNextPage = this.saveAndGoToNextPage.bind(this);

    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.hideConfirmationPopup = this.hideConfirmationPopup.bind(this);
  }

  componentDidMount() {
    if (this.props.rulesList) {
      this.props.fetchOccupationData();
    } else {
      this.props.loadRules();
    }
    // this.props.loadPrivacyPolicy();
  }

  showConfirmationPopup() {
    this.setState({
      showConfirmationPopup: true
    });
  }

  hideConfirmationPopup() {
    this.setState({
      showConfirmationPopup: false,
      deleteSuccessCallBack: null
    });
  }

  // rawMarkUp() {
  //     let rawMarkup = this.props.privacyPolicyList.privacyConditions
  //     return { __html: rawMarkup };
  // }

  saveAndGoToNextPage(e) {
    e.preventDefault();
    this.setState({ goToNextPage: true }, () => this.addOrUpdateUserDetails(e));
  }

  mapToUpdateApiParams() {
    const {
      name,
      relation,
      qualification,
      occupation,
      absoluteIncome,
      panNumber,
      id
    } = this.state;

    let params = {
      name,
      relation,
      qualification,
      occupation,
      absoluteIncome,
      panNumber
    };

    if (id) {
      params.id = id;
    }

    return { userid: this.props.userId, params };
  }

  editClickHandler(id) {
    const userFamilyData = this.props.userFamilyEarningData.filter(
      data => data.id === id
    );

    const {
      name,
      relationDetail,
      qualificationDetail,
      occupationDetail,
      absoluteIncome,
      panNumber
    } = userFamilyData[0];

    this.setState({
      id,
      name,
      relation: relationDetail.id,
      qualification: qualificationDetail.id,
      occupation: occupationDetail.id,
      absoluteIncome,
      panNumber,
      isFormShown: true
    });
  }

  deleteClickHandler(id) {
    const deleteFamilyEarningDetails = () =>
      this.props.deleteFamilyEarningDetails({
        userid: this.props.userId,
        familyId: id
      });

    this.setState({
      deleteSuccessCallBack: deleteFamilyEarningDetails,
      showConfirmationPopup: true
    });
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    switch (type) {
      case UPDATE_FAMILY_EARNING_DETAILS_SUCCESS:
        if (this.state.goToNextPage) {
          this.props.goToNextPage("Interest");
        } else {
          this.setState(
            {
              id: "",
              name: "",
              relation: null,
              qualification: null,
              occupation: null,
              absoluteIncome: null,
              panNumber: "",
              isFormShown: false
            },
            () =>
              this.props.fetchFamilyEarningDetails({
                userid: this.props.userId
              })
          );
        }

        break;

      case FETCH_OCCUPATION_DATA_SUCCESS:
        this.props.fetchFamilyEarningDetails({ userid: this.props.userId });
        break;

      case FETCH_RULES_SUCCEEDED:
        this.props.fetchOccupationData();
        break;

      case DELETE_FAMILY_EARNING_DETAILS_SUCCESS:
        this.setState(
          {
            showConfirmationPopup: false,
            deleteSuccessCallBack: null
          },
          () =>
            this.props.fetchFamilyEarningDetails({ userid: this.props.userId })
        );

        break;
    }
  }

  addOrUpdateUserDetails(e) {
    e.preventDefault();
    const addOrUpdateApiParams = this.mapToUpdateApiParams();
    //Add validations
    if (this.checkFormValidations()) {
      this.props.addOrupdateFamilyEarningDetails(addOrUpdateApiParams);
    }
    // if (this.checkFormValidations()) {
    //   this.props.checkFormValidations({
    //     relation: this.state.relation,
    //     name: this.state.name,
    //     qualification: this.state.qualification,
    //     occupation: this.state.occupation,
    //     absoluteIncome: this.state.absoluteIncome,
    //     panNumber: this.state.panNumber
    //   });
    // }
  }

  showForm() {
    this.setState({
      isFormShown: true
    });
  }

  /************ start - validation part - rules******************* */
  formChangeHandler(event) {
    const { id, value } = event.target;
    let validationResult = {};
    if (id === "panNumber" && value === "") {
      validationResult[id] = null;
    } else {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      validationResult = ruleRunner(value, id, name, ...validationFunctions);
    }

    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];
    this.setState({
      [id]: value,
      validations
    });
  }
  /************ end - validation part - rules******************* */

  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "name":
        validationObject.name = "* Earning member name";
        validationObject.validationFunctions = [required, lengthRange(3, 30)];
        return validationObject;
      case "relation":
        validationObject.name = "* Relationship with candidate";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "qualification":
        validationObject.name = "* Qualification";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "occupation":
        validationObject.name = "* Occupation";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "absoluteIncome":
        validationObject.name = "* Annual income";
        validationObject.validationFunctions = [required, isNumeric];
        return validationObject;
      case "panNumber":
        validationObject.name = "* Pan card number";
        validationObject.validationFunctions = [isPanCard];
        return validationObject;
    }
  }
  /************ end - validation part - get message and required validation******************* */

  /************ start - validation part - check form submit validation******************* */
  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    let validationResult = {};
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      if (key === "panNumber" && this.state.panNumber === "") {
        validationResult.panNumber = null;
      } else {
        validationResult = ruleRunner(
          this.state[key],
          key,
          name,
          ...validationFunctions
        );
      }
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }
  /************ start - validation part - check form submit validation******************* */

  closeForm() {
    event.preventDefault();
    this.setState({
      id: "",
      name: "",
      relation: 0,
      qualification: 0,
      occupation: 0,
      absoluteIncome: 0,
      panNumber: "",
      isFormShown: false
    });
  }

  render() {
    const { subscriber_relation } = this.props.rulesList || [];
    const classDetails = this.props.rulesList ? this.props.rulesList.class : [];

    return (
      <section>
        <ConfirmMessagePopup
          message={confirmationMessages.GENERIC}
          showPopup={this.state.showConfirmationPopup}
          onConfirmationSuccess={this.state.deleteSuccessCallBack}
          onConfirmationFailure={this.hideConfirmationPopup}
        />
        <Loader isLoader={this.props.showLoader} />
        <article className="ctrl-wrapper widget-border">
          <h4>Family Earnings</h4>
          <article className="col-md-12">
            <article className="ctrl-wrapper">
              <article className="table-responsive dataTbl">
                <FamilyEarningsTableSection
                  userFamilyEarningData={this.props.userFamilyEarningData}
                  editClickHandler={this.editClickHandler}
                  deleteClickHandler={this.deleteClickHandler}
                />
              </article>
            </article>
          </article>
        </article>
        <article className="col-md-12 width100">
          <article className="row">
            <article className="btn-ctrl">
              <span
                onClick={() => this.props.goToNextPage("Interest")}
                className="btn pull-right but-reference"
              >
                Next
              </span>
              <span
                onClick={this.showForm}
                className="btn pull-left but-reference"
              >
                Add Family Income
              </span>
            </article>
          </article>
        </article>
        {this.state.isFormShown ? (
          <form
            name="familyEarningForm"
            autoCapitalize="off"
            onSubmit={this.addOrUpdateUserDetails}
          >
            <article className="ctrl-wrapper widget-border topM">
              <h4 className="titleMargin">Member</h4>
              <article className="row">
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="fname">Name of Earning Member*</label>
                    <input
                      type="text"
                      maxLength="100"
                      placeholder="Enter Name of Earning Member"
                      className="form-control"
                      value={this.state.name}
                      id="name"
                      onChange={this.formChangeHandler}
                      name="earningMember_name"
                    />
                    {this.state.validations.name ? (
                      <span className="error animated bounce">
                        {this.state.validations.name}
                      </span>
                    ) : null}
                  </article>
                </article>
                <article className="col-md-6">
                  <label className="control-label">
                    Relationship with Candidate*
                  </label>
                  <article className="form-group">
                    <select
                      name="relationOfCondidate"
                      className="form-control"
                      placeholder="Enter Your Email Address"
                      id="relation"
                      onChange={this.formChangeHandler}
                      value={this.state.relation}
                    >
                      <option value="">--Select Relation--</option>
                      {subscriber_relation.map(relation => (
                        <option value={relation.id}>
                          {relation.rulevalue}
                        </option>
                      ))}
                    </select>
                    {this.state.validations.relation ? (
                      <span className="error animated bounce">
                        {this.state.validations.relation}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article>
              <article className="row">
                <article className="col-md-6">
                  <label className="control-label"> Qualification*</label>
                  <article className="form-group">
                    <select
                      name="qualification"
                      className="form-control"
                      id="qualification"
                      onChange={this.formChangeHandler}
                      value={this.state.qualification}
                    >
                      <option value="">--Select Class/Degree--</option>
                      {classDetails.map(classDetail => (
                        <option value={classDetail.id}>
                          {classDetail.rulevalue}
                        </option>
                      ))}
                    </select>
                    {this.state.validations.qualification ? (
                      <span className="error">
                        {this.state.validations.qualification}
                      </span>
                    ) : null}
                    {/* <span className="error">* Qualification is required.</span> */}
                  </article>
                </article>
                <article className="col-md-6">
                  <label className="control-label"> Occupation*</label>
                  <article className="form-group">
                    <select
                      name="occupation"
                      className="form-control"
                      id="occupation"
                      onChange={this.formChangeHandler}
                      value={this.state.occupation}
                    >
                      <option value="">--Select Occupation--</option>
                      {this.props.occupationData.map(occupation => (
                        <option value={occupation.id}>
                          {occupation.occupationName}
                        </option>
                      ))}
                    </select>

                    {this.state.validations.occupation ? (
                      <span className="error animated bounce">
                        {this.state.validations.occupation}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article>
              <article className="row">
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="fname">Absolute Annual Income*</label>
                    <input
                      type="text"
                      placeholder="Enter Absolute Annual Income"
                      // pattern="/^\d+$/"
                      maxLength="10"
                      minLength="1"
                      className="form-control"
                      id="absoluteIncome"
                      onChange={this.formChangeHandler}
                      value={this.state.absoluteIncome}
                      name="absoluteIncome"
                    />
                    {this.state.validations.absoluteIncome ? (
                      <span className="error animated bounce">
                        {this.state.validations.absoluteIncome}
                      </span>
                    ) : null}
                    {/* <span className="error">* Annual Income should be digits.</span>
                    <span className="error">* Annual Income is required.</span> */}
                  </article>
                </article>
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="pan">Pan card of mother/father</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Enter Pan card of mother/father"
                      maxLength={10}
                      id="panNumber"
                      value={this.state.panNumber}
                      onChange={this.formChangeHandler}
                      name="panNumber"
                    />
                    {this.state.validations.panNumber ? (
                      <span className="error animated bounce">
                        {this.state.validations.panNumber}
                      </span>
                    ) : null}
                    {/* <span className="error">* Pan card number is too long.</span>
                    <span className="error">* Pan card number is too short.</span> */}
                  </article>
                </article>
              </article>
            </article>
            <article className="errorContainer">
              {/* <span className="error">
                Please complete all (*) mandatory fields marked in red before
                going to next step.
              </span> */}
            </article>

            <article className="col-md-12 width100">
              <article className="row">
                <article className="btn-ctrl threeBtn">
                  <button type="submit" onClick={this.addOrUpdateUserDetails}>
                    Submit
                  </button>
                  <button
                    type="button"
                    onClick={this.closeForm}
                    className="closeBtn"
                  >
                    Close
                  </button>
                  <button type="button" onClick={this.saveAndGoToNextPage}>
                    Save/Next
                  </button>
                </article>
              </article>
            </article>
          </form>
        ) : null}
      </section>
    );
  }
}

const FamilyEarningsTableSection = props => {
  const { userFamilyEarningData } = props;

  return (
    <table className="table table-striped">
      <tbody>
        <tr>
          <th className="text-center">Name</th>
          <th className="text-center">Relationship</th>
          <th className="text-center">Qualification</th>
          <th className="text-center">Occupation</th>
          <th className="text-center"> Absolute Annual Income</th>
          <th className="text-center"> PAN No</th>
          <th className="text-center">Edit</th>
          <th className="text-center">Delete</th>
        </tr>
        {userFamilyEarningData.map(userFamEarning => (
          <FamilyEarningsTableRow
            data={userFamEarning}
            editClickHandler={props.editClickHandler}
            deleteClickHandler={props.deleteClickHandler}
          />
        ))}
      </tbody>
    </table>
  );
};

const FamilyEarningsTableRow = props => (
  <tr>
    <td className="text-center">{props.data.name}</td>
    <td className="text-center">{props.data.relationDetail.value}</td>
    <td className="text-center">
      {props.data.qualificationDetail
        ? props.data.qualificationDetail.value
        : ""}
    </td>
    <td className="text-center">{props.data.occupationDetail.value} </td>
    <td className="text-center">{props.data.absoluteIncome}</td>
    <td className="text-center">{props.data.panNumber}</td>
    <td className="text-center">
      <p className="text-red text-center">
        <span
          onClick={() => props.editClickHandler(props.data.id)}
          title="Edit this file"
        >
          <i className="fa fa-pencil-square-o" aria-hidden="true" />
        </span>
      </p>
    </td>
    <td className="text-center">
      <p className="text-red text-center">
        <span
          title="delete this file"
          onClick={() => props.deleteClickHandler(props.data.id)}
        >
          <i className="fa fa-trash-o" aria-hidden="true" />
        </span>
      </p>
    </td>
  </tr>
);

export default FamilyEarnings;
