import React, { Component } from "react";
import BankInfo from "./bank-info";
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import AlertMessagePopup from "../../../common/components/alertMsg";
import Loader from "../../../common/components/loader";
import {
  FETCH_BANK_DETAIL_SUCCEDED,
  FETCH_BANK_DETAIL_FAILED,
  FETCH_BANK_SAVE_DETAIL_SUCCEDED,
  FETCH_BANK_SAVE_DETAIL_FAILED,
  POST_BANK_DETAIL_SUCCEDED,
  POST_BANK_DETAIL_FAILED
} from "../../../scholar/scholarAction";

class BankDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: ""
    };
    this.closeAlertPopup = this.closeAlertPopup.bind(this);
    // this.mapToApiParams = this.mapToApiParams.bind(this);
    // this.onFormSubmit = this.onFormSubmit.bind(this);
    // this.checkFormValidations = this.checkFormValidations.bind(this);
    // this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    // this.editClickHandler = this.editClickHandler.bind(this);
  }
  closeAlertPopup() {
    this.setState({ showAlertPopup: false, alertMsg: "" });
  }
  componentWillReceiveProps(nextProps) {
    const { bankData, bankIfscData } = nextProps;
    switch (nextProps.scholarType) {
      case FETCH_BANK_DETAIL_SUCCEDED:
        let bankDetailObj = {
          bankName: bankIfscData.bank,
          district: bankIfscData.district,
          state: bankIfscData.state,
          ifscCode: bankIfscData.ifsc,
          branchName: bankIfscData.branch,
          accountHolderName: getNestedObjKey(bankData, [
            0,
            "accountHolderName"
          ]),
          accountNumber: getNestedObjKey(bankData, [0, "accountNumber"]),
          id: getNestedObjKey(bankData, [0, "id"])
        };
        this.setState({
          bankDetail: bankDetailObj
        });
        break;
      case FETCH_BANK_DETAIL_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case FETCH_BANK_SAVE_DETAIL_SUCCEDED:
        let savedBankDetail = {
          bankName: getNestedObjKey(bankData, [0, "bankName"]),
          district: getNestedObjKey(bankData, [0, "district"]),
          state: getNestedObjKey(bankData, [0, "state"]),
          ifscCode: getNestedObjKey(bankData, [0, "ifscCode"]),
          id: getNestedObjKey(bankData, [0, "id"]),
          branchName: getNestedObjKey(bankData, [0, "branchName"]),
          accountHolderName: getNestedObjKey(bankData, [
            0,
            "accountHolderName"
          ]),
          accountNumber: getNestedObjKey(bankData, [0, "accountNumber"])
        };
        this.setState({
          bankDetail: savedBankDetail
        });
        break;
      case FETCH_BANK_SAVE_DETAIL_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case POST_BANK_DETAIL_SUCCEDED:
        if(this.props.scholarType !== nextProps.scholarType ){
        this.setState({
          showAlertPopup: true,
          alertMsg: "Data submitted successfully",
          isError: false
        },()=>{
          this.props.fetchBankSaveDetail();
        });
      }
        break;
      case POST_BANK_DETAIL_FAILED:
        const errCode = getNestedObjKey(bankData, [
          "response",
          "data",
          "errorCode"
        ]);
        const errMsg = getNestedObjKey(bankData, [
          "response",
          "data",
          "message"
        ]);
        if (errCode == 701) {
          this.setState({
            showAlertPopup: true,
            alertMsg: errMsg,
            isError: true
          });
        } else {
          this.setState({
            showAlertPopup: true,
            alertMsg: "Server error",
            isError: true
          });
        }
        break;
    }
  }

  render() {
    const { disbursalData } = this.props;
    const { bankDetail } = this.state;
    return (
      <article>
        <Loader isLoader={this.props.scholarLoader} />
        {this.state.showAlertPopup && (
          <AlertMessagePopup
            msg={this.state.alertMsg}
            isShow={this.state.showAlertPopup}
            status={!this.state.isError}
            close={this.closeAlertPopup}
          />
        )}
        <BankInfo
          bankDetail={bankDetail}
          disbursalId={getNestedObjKey(disbursalData, ["disbursalId"])}
          scholarshipId={getNestedObjKey(disbursalData, ["scholarshipId"])}
          // nextStep={this.nextStep}
          // goThanks={this.goThanks}
          // isBackHide={isBackHide}
          // hideBackBtn={this.hideBackBtn}
          // prevStep={this.prevStep}
          fetchBankSaveDetail={this.props.fetchBankSaveDetail}
          sendBankDetail={this.props.sendBankDetail}
          fetchBankDetail={this.props.fetchBankDetail}
        />
      </article>
    );
  }
}

export default BankDetails;
