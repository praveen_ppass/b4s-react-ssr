import React, { Component } from "react";

import Loader from "../../../common/components/loader";
import { ruleRunner } from "../../../../validation/ruleRunner";
import {
  required,
  isEmail,
  minLength,
  isNumeric,
  lengthRange,
  isMobileNumber
} from "../../../../validation/rules";
import {
  UPDATE_REFERENCE__DETAILS_SUCCESS,
  DELETE_REFERENCE__DETAILS_SUCCESS,
  FETCH_OCCUPATION_DATA_SUCCESS
} from "../../actions";

import { confirmationMessages } from "../../../../constants/constants";
import ConfirmMessagePopup from "../../../common/components/confirmMessagePopup";
import { FETCH_RULES_SUCCEEDED } from "../../../../constants/commonActions";

class References extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      address: "",
      mobile: "",
      name: "",
      occupation: 0,
      goToNextPage: false,
      showConfirmationPopup: null,
      deleteSuccessCallBack: null,
      relation: 0,
      validations: {
        address: null,
        name: null,
        mobile: null,
        occupation: null,
        relation: null
      },
      isFormValid: false,
      isFormShown: false
    };
    this.showForm = this.showForm.bind(this);
    this.closeForm = this.closeForm.bind(this);
    this.formChangeHandler = this.formChangeHandler.bind(this);
    this.addOrUpdateReferenceData = this.addOrUpdateReferenceData.bind(this);
    this.editClickHandler = this.editClickHandler.bind(this);
    this.deleteClickHandler = this.deleteClickHandler.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.saveAndGoToNextPage = this.saveAndGoToNextPage.bind(this);
    this.hideConfirmationPopup = this.hideConfirmationPopup.bind(this);
  }

  showForm() {
    this.setState({
      isFormShown: true
    });
  }

  saveAndGoToNextPage(e) {
    e.preventDefault();
    this.addOrUpdateReferenceData();
    // this.setState({ goToNextPage: true }, () =>
    //   this.addOrUpdateReferenceData()
    // );
  }

  hideConfirmationPopup() {
    this.setState({
      showConfirmationPopup: false,
      deleteSuccessCallBack: null
    });
  }

  closeForm() {
    this.setState({
      isFormShown: false,
      id: "",
      address: "",
      mobile: "",
      name: "",
      occupation: "",
      relation: ""
    });
  }

  editClickHandler(id) {
    const userReferenceData = this.props.referencesData.filter(
      data => data.id === id
    );

    const {
      address,
      mobile,
      name,
      occupationDetail,
      relationDetail
    } = userReferenceData[0];

    this.setState({
      id,
      address,
      mobile,
      name,
      occupation: occupationDetail.id,
      relation: relationDetail.id,
      isFormShown: true
    });
  }

  deleteClickHandler(id) {
    const deleteSuccessCallBack = () =>
      this.props.deleteReferenceDetailData({
        userid: this.props.userId,
        referenceId: id
      });

    this.setState({
      deleteSuccessCallBack,
      showConfirmationPopup: true
    });
  }

  mapToUpdateApiParams() {
    const { address, mobile, name, occupation, relation, id } = this.state;

    let params = {
      address,
      mobile,
      name,
      occupation,
      relation
    };

    if (id) {
      params.id = id;
    }

    return { userid: this.props.userId, params };
  }

  addOrUpdateReferenceData() {
    const addOrUpdateApiParams = this.mapToUpdateApiParams();
    //Add validations
    if (this.checkFormValidations()) {
      this.props.addOrUpdateReferenceDetailsData(addOrUpdateApiParams);
    }
  }

  /************ start - validation part - rules******************* */
  formChangeHandler(event) {
    const { id, value } = event.target;
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );
    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];
    this.setState({
      [id]: value,
      validations
    });
  }
  /************ end - validation part - rules******************* */

  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "name":
        validationObject.name = "* Name";
        validationObject.validationFunctions = [required, lengthRange(3, 50)];
        return validationObject;
      case "address":
        validationObject.name = "* Address";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "mobile":
        validationObject.name = "* Mobile number";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;

      case "occupation":
        validationObject.name = "* Occupation";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "relation":
        validationObject.name = "* Relation";
        validationObject.validationFunctions = [required];
        return validationObject;
    }
  }
  /************ end - validation part - get message and required validation******************* */

  /************ start - validation part - check form submit validation******************* */
  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }
  /************ start - validation part - check form submit validation******************* */

  componentDidMount() {
    if (this.props.rulesList) {
      this.props.fetchOccupationData();
    } else {
      this.props.loadRules();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case UPDATE_REFERENCE__DETAILS_SUCCESS:
        if (this.state.goToNextPage) {
          this.props.goToNextPage("BankDetails");
        } else {
          this.setState(
            {
              isFormShown: false,
              id: "",
              address: "",
              mobile: "",
              name: "",
              occupation: "",
              relation: ""
            },
            () =>
              this.props.fetchReferenceDetailsData({
                userid: this.props.userId
              })
          );
        }

        break;

      case FETCH_RULES_SUCCEEDED:
        this.props.fetchOccupationData();
        break;

      case FETCH_OCCUPATION_DATA_SUCCESS:
        this.props.fetchReferenceDetailsData({ userid: this.props.userId });
        break;

      case DELETE_REFERENCE__DETAILS_SUCCESS:
        this.setState(
          {
            showConfirmationPopup: false,
            deleteSuccessCallBack: null
          },
          () =>
            this.props.fetchReferenceDetailsData({ userid: this.props.userId })
        );

        break;
    }
  }

  render() {
    const { subscriber_relation } = this.props.rulesList || [];
    return (
      <section>
        <ConfirmMessagePopup
          message={confirmationMessages.DASHBOARD.REFERENCES}
          showPopup={this.state.showConfirmationPopup}
          onConfirmationSuccess={this.state.deleteSuccessCallBack}
          onConfirmationFailure={this.hideConfirmationPopup}
        />
        <Loader isLoader={this.props.showLoader} />
        <article className="ctrl-wrapper widget-border">
          <h4>My References </h4>
          <article className="col-md-12">
            <article className="ctrl-wrapper">
              <article className="table-responsive dataTbl">
                <ReferenceTableSection
                  referencesData={this.props.referencesData}
                  editClickHandler={this.editClickHandler}
                  deleteClickHandler={this.deleteClickHandler}
                />
              </article>
            </article>
          </article>
        </article>
        <article className="col-md-12 width100">
          <article className="row">
            <article className="btn-ctrl">
              <span
                onClick={this.showForm}
                className="btn pull-left but-reference"
              >
                Add Reference
              </span>
              {/* <span
                onClick={() => this.props.goToNextPage("BankDetails")}
                className="btn pull-right but-reference"
              >
                Next
              </span> */}
            </article>
          </article>
        </article>
        {this.state.isFormShown ? (
          <form name="referenceForm" autoCapitalize="off">
            <article className="ctrl-wrapper widget-border topM">
              <h4 className="titleMargin">Checklist of the Reference</h4>
              <article className="row">
                <article className="col-md-6">
                  <label htmlFor="name">Name*</label>
                  <article className="form-group">
                    <input
                      type="text"
                      maxLength="80"
                      minLength="1"
                      placeholder="Enter name"
                      className="form-control"
                      id="name"
                      name="name"
                      value={this.state.name}
                      onChange={this.formChangeHandler}
                      required
                    />
                    {this.state.validations.name ? (
                      <span className="error animated bounce">
                        {this.state.validations.name}
                      </span>
                    ) : null}
                  </article>
                </article>
                <article className="col-md-6">
                  <label htmlFor="mobile">Mobile*</label>
                  <article className="form-group">
                    <input
                      type="text"
                      minLength="10"
                      maxLength="10"
                      placeholder="Enter mobile number"
                      className="form-control"
                      name="mobile"
                      value={this.state.mobile}
                      id="mobile"
                      onChange={this.formChangeHandler}
                      required
                    />
                    {this.state.validations.mobile ? (
                      <span className="error animated bounce">
                        {this.state.validations.mobile}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article>
              <article className="row">
                <article className="col-md-6">
                  <label htmlFor="occupation">Occupation*</label>

                  <article className="form-group">
                    <select
                      name="occupation"
                      className="form-control"
                      id="occupation"
                      value={this.state.occupation}
                      required
                      onChange={this.formChangeHandler}
                    >
                      <option value="">--Select Occupation--</option>
                      {this.props.occupationData.map(occupation => (
                        <option value={occupation.id}>
                          {occupation.occupationName}
                        </option>
                      ))}
                    </select>
                    {this.state.validations.occupation ? (
                      <span className="error animated bounce">
                        {this.state.validations.occupation}
                      </span>
                    ) : null}
                  </article>
                </article>
                <article className="col-md-6">
                  <label htmlFor="relation">Relation*</label>
                  <article className="form-group">
                    <select
                      name="relation"
                      id="relation"
                      className="form-control"
                      required=""
                      value={this.state.relation}
                      onChange={this.formChangeHandler}
                    >
                      <option value={0}>--Select Relation--</option>
                      {subscriber_relation.map(relation => (
                        <option value={relation.id}>
                          {relation.rulevalue}
                        </option>
                      ))}
                    </select>
                    {this.state.validations.relation ? (
                      <span className="error animated bounce">
                        {this.state.validations.relation}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article>
              <article className="row">
                <article className="col-md-12">
                  <label htmlFor="address">Address*</label>
                  <article className="form-group textAreaCrtl">
                    <textarea
                      required
                      name="address"
                      className="form-control text-area"
                      placeholder="Enter address"
                      id="address"
                      value={this.state.address}
                      onChange={this.formChangeHandler}
                    />
                    {this.state.validations.address ? (
                      <span className="error animated bounce">
                        {this.state.validations.address}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article>
            </article>

            <article className="col-md-12 width100">
              <article className="row">
                <article className="btn-ctrl threeBtn">
                  <button type="button" onClick={this.addOrUpdateReferenceData}>
                    Submit
                  </button>
                  <button type="button" onClick={this.closeForm}>
                    Close
                  </button>
                  <button onClick={this.saveAndGoToNextPage}>Save</button>
                </article>
              </article>
            </article>
          </form>
        ) : null}
      </section>
    );
  }
}

const ReferenceTableSection = props => {
  const { referencesData, editClickHandler, deleteClickHandler } = props;

  return (
    <table className="table table-striped">
      <tbody>
        <tr>
          <th>Name</th>
          <th>Phone</th>
          <th className="text-center">Relation</th>
          <th className="text-center">Occupation</th>
          <th>Address</th>
          <th className="text-center">Edit</th>
          <th className="text-center">Delete</th>
        </tr>
        {referencesData.map(userRef => (
          <ReferenceTableRow
            data={userRef}
            editClickHandler={editClickHandler}
            deleteClickHandler={deleteClickHandler}
          />
        ))}
      </tbody>
    </table>
  );
};

const ReferenceTableRow = props => {
  return (
    <tr>
      <td>{props.data.name}</td>
      <td>{props.data.mobile}</td>
      <td>
        <p className="text-green text-center">
          {props.data.relationDetail.value}
        </p>
      </td>
      <td>
        <p className="text-red text-center">
          {props.data.occupationDetail.value}
        </p>
      </td>
      <td>
        <p>{props.data.address}</p>
      </td>
      <td className="text-center">
        <p className="text-red text-center">
          <span
            title="Edit"
            onClick={() => props.editClickHandler(props.data.id)}
          >
            <i className="fa fa-pencil-square-o" aria-hidden="true" />
          </span>
        </p>
      </td>
      <td className="text-center">
        <p className="text-red text-center">
          <span
            title="Delete"
            onClick={() => props.deleteClickHandler(props.data.id)}
          >
            <i className="fa fa-trash-o" aria-hidden="true" />
          </span>
        </p>
      </td>
    </tr>
  );
};

export default References;
