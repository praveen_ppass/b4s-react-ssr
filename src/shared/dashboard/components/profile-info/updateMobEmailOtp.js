import React, { Component } from "react";
import OtpInput from "react-otp-input";

class UpdateMobEmailOtp extends Component {
	render(props) {
		return (
			<section className="row updateEmailMobile">
				<article className="innerWrapper">
					<h3>Update {" "}{this.props.newMobile ? "Mobile Number" : "Email"}{" "}</h3>
					<section className="form-wrapper">
						<p>
							Enter the OTP sent to your{" "}
							{this.props.newMobile ? "Mobile Number" : "Email"}{" "}
							{!!this.props.newMobile && <span>{this.props.newMobile}</span>}
							{!!this.props.newEmail && <span>{this.props.newEmail}</span>}
							<span>Enter the correct OTP</span>
						</p>

						<article className="form-group registerOtp otp">
							<OtpInput
								onChange={otp => this.props.handleFieldChange(otp)}
								numInputs={6}
								separator={<span>-</span>}
							/>
						</article>
						<article className="btn-ctrl">
							<button
								type="button"
								className="Verify-OTP"
								onClick={this.props.verifyOtp}
							>
								Verify OTP
                          </button>
						</article>

					</section>
				</article>
			</section>
		);
	}
}

export default UpdateMobEmailOtp;
