import React from "react";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import AlertMessagePopup from "../../../common/components/alertMsg";
import Loader from "../../../common/components/loader";
import {
  getPassingYear,
  getAcadmicYear
} from "../../../../constants/constants";
import {
  GET_EDUCATION_COURSES_SUCCEDED,
  GET_EDUCATION_COURSES_FAILED,
  FETCH_RULE_SUCCEDED,
  FETCH_RULE_FAILED,
  FETCH_DISTRICT_SUCCEDED,
  FETCH_DISTRICT_FAILED,
  GET_EDUCATION_DETAIL_SUCCEDED,
  GET_EDUCATION_DETAIL_FAILED,
  POST_EDUCATION_DETAIL_SUCCEDED,
  POST_EDUCATION_DETAIL_FAILED
} from "../../../scholar/scholarAction";
const eduValidationSchema = Yup.object().shape({
  degree: Yup.string()
    .required("Required")
    .nullable(),
  subject: Yup.string()
    .nullable()
    .test("match", "Required", function (subject) {
      if (parseInt(this.parent.degree) < 12) {
        return true;
      } else if (subject) {
        return true;
      }
    }),
  year: Yup.string()
    .nullable()
    .test("match", "Required", function (year) {
      if (parseInt(this.parent.degree) <= 17) {
        return true;
      } else if (year) {
        return true;
      }
    }),
  instituteName: Yup.string()
    .nullable()
    .required("Required"),
  passingYear: Yup.string()
    .nullable()
    .test("match", "Required", function (passingYear) {
      if (parseInt(this.parent.degree) <= 17) {
        return true;
      } else if (passingYear) {
        return true;
      }
    }),
  city: Yup.string()
    .nullable()
    .test("match", "Required", function (city) {
      if (parseInt(this.parent.degree) <= 17) {
        return true;
      } else if (city) {
        return true;
      }
    }),
  district: Yup.string()
    .nullable()
    .test("match", "Required", function (district) {
      if (parseInt(this.parent.degree) <= 17) {
        return true;
      } else if (district) {
        return true;
      }
    }),
  address: Yup.string()
    .nullable()
    .test("match", "Required", function (address) {
      if (parseInt(this.parent.degree) <= 17) {
        return true;
      } else if (address) {
        return true;
      }
    }),
  state: Yup.string()
    .nullable()
    .test("match", "Required", function (state) {
      if (parseInt(this.parent.degree) < 17) {
        return true;
      } else if (state) {
        return true;
      }
    }),
  marksObtained: Yup.string()
    .nullable()
    .test("match", "Required", function (marksObtained) {
      if (this.parent.radioClassStatus == "presentClass") {
        return true;
      } else if (marksObtained) {
        return true;
      }
    }),
  totalMarks: Yup.string()
    .nullable()
    .test("match", "Required", function (totalMarks) {
      if (this.parent.radioClassStatus == "presentClass") {
        return true;
      } else if (totalMarks) {
        return true;
      }
    })
    .test(
      "match",
      "Total marks should be greater or equal than marks obtained",
      function (totalMarks) {
        if (this.parent.radioClassStatus == "presentClass") {
          return true;
        }
        var result =
          parseFloat(totalMarks) >= parseFloat(this.parent.marksObtained);
        return result;
      }
    ),
  radioClassStatus: Yup.string()
    .required("Required")
    .nullable()
});
export default class EducationDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      eduData: [],
      scholarshipId: "",
      disbursalId: "",
      isEdit: false,
      activeAdd: false,
      activeEdit: false,
      presentClassData: "",
      marksObtained: "",
      totalMarks: "",
      subject: "",
      degree: "",
      showAlertPopup: false,
      alertMsg: "",
      isError: "",
      educationData: [],
      radioClassStatus: "",
      year: "",
      isAdd: false,
      defaultAcadmic: {
        academicClass: { id: null, value: "" },
        passingYear: "",
        instituteName: "",
        stream: "",
        district: "",
        address: "",
        state: "",
        currentAcademicYear: ""
      }
    };
    this.provideFormInitialValues = this.provideFormInitialValues.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getYear = this.getYear.bind(this);
    this.handleClass = this.handleClass.bind(this);
    this.cancelEditing = this.cancelEditing.bind(this);
    this.goNextPage = this.goNextPage.bind(this);
    this.closeAlertPopup = this.closeAlertPopup.bind(this);
    this.toggle = this.toggle.bind(this);
    this.checkClass = this.checkClass.bind(this);
    this.getStateName = this.getStateName.bind(this);
  }
  componentDidMount() {
    const data = {
      filter: ["state", "class", "subject", "course", "year"]
    };
    // this.props.fetchUserDisbursalId({ scholarshipId: this.propsschId });

    this.props.fetchRule(data);
    // this.props.fetchEduDetail({ scholarshipId: this.props.scholarshipId });
  }
  componentWillReceiveProps(nextprops) {
    const {
      scholarType,
      courseData,
      ruleData,
      eduData,
      districtData
    } = nextprops;
    switch (scholarType) {
      case GET_EDUCATION_DETAIL_SUCCEDED:
        const disbursalId = getNestedObjKey(eduData, [
          "academicDetails",
          0,
          "disbursalId"
        ]);
        const educationData = getNestedObjKey(eduData, ["academicDetails"]);
        this.setState({
          //eduData: educationData,
          disbursalId,
          isAdd: false
        });
        const presentEdu = educationData.filter(x => x.presentClass == 1);
        if (!presentEdu.length) {
          this.setState({ isAdd: false });
          return;
        } else {
          educationData.map(x => {
            if (x.disbursalId == disbursalId) {
              this.setState({ isAdd: true });
              return;
            }
            return;
          });
        }
        break;
      case GET_EDUCATION_DETAIL_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case GET_EDUCATION_COURSES_SUCCEDED:
        this.setState({ courses: courseData });
        break;
      case GET_EDUCATION_COURSES_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case FETCH_RULE_SUCCEDED:
        this.setState({ ruleFilter: ruleData });
        break;
      case FETCH_RULE_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case FETCH_DISTRICT_SUCCEDED:
        this.setState({ districtData: districtData });
        break;
      case FETCH_DISTRICT_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case POST_EDUCATION_DETAIL_SUCCEDED:
        this.setState(
          {
            isEdit: false,
            showAlertPopup: true,
            alertMsg: "Data submitted successfully",
            isError: false,
            activeAdd: false,
            activeEdit: false
          },
          () => {
            this.props.fetchScholarEduDetail({
              scholarshipId: this.state.scholarshipId
            });
          }
        );
        break;
      case POST_EDUCATION_DETAIL_FAILED:
        break;
    }
    // if (nextprops.educationData) {
    //   const { educationData } = nextprops;
    //   const eduData = getNestedObjKey(educationData, ["academicDetails"]);
    //   this.setState({ isAdd: false });
    //   eduData.map(x => {
    //     if (x.disbursalId == nextprops.disbursalId || x.presentClass == 1) {
    //       this.setState({ isAdd: true });
    //       return;
    //     }
    //     return;
    //   });
    //   this.setState({ eduData: eduData });
    // }
    // if (!nextprops.isEduEditing) {
    //   this.setState({ isEdit: false });
    // }
  }
  closeAlertPopup() {
    this.setState({ showAlertPopup: false, alertMsg: "" });
  }
  provideFormInitialValues() {
    let initialValues = {
      degree: "",
      subject: "",
      marksObtained: "",
      totalMarks: "",
      year: "",
      radioClassStatus: "",
      state: "",
      district: "",
      city: "",
      address: "",
      instituteName: ""
    };
    return initialValues;
  }
  handleChange(e, cb) {
    if (e.target.name == "degree") {
      cb("degree", e.target.value);
      cb("subject", "");
      cb("year", "");
      if (e.target.value >= 12) {
        this.props.fetchCourses({ courseId: e.target.value });
      }
    } else if (e.target.name == "state") {
      this.props.fetchDistrict({ stateId: e.target.value });
      cb("state", e.target.value);
    } else if (e.target.name == "degree" && e.target.value < 12) {
      cb("subject", "");
    } else if (e.target.name == "degree" && e.target.value < 20) {
      this.setState({ year: "" });
      cb("year", "");
    } else if (
      e.target.name == "radioClassStatus" &&
      e.target.value == "presentClass"
    ) {
      cb("radioClassStatus", "presentClass");
      cb("previousClass", 0);
      cb("marksObtained", "");
      cb("totalMarks", "");
    } else if (
      e.target.name == "radioClassStatus" &&
      e.target.value == "previousClass"
    ) {
      cb("radioClassStatus", "previousClass");
      cb("presentClass", 0);
    }
    cb([e.target.name], e.target.value);
  }
  getYear(num) {
    if (num) {
      let year = Math.ceil(parseInt(num) / 2);
      return parseInt(year);
    } else {
      return 1;
    }
  }
  handleClass(cb, data) {
    this.cancelEditing(cb);
    if (data == "add") {
      let currentYear = new Date().getFullYear();
      const { eduData } = this.state;
      this.setState({
        isEdit: true,
        activeAdd: true,
        ...this.state.defaultAcadmic
      });
      cb("radioClassStatus", "presentClass");
      const preYearData = eduData.filter(x => x.presentClass == 1);
      this.setState({ presentClassData: [...preYearData][0] });
      preYearData &&
        preYearData.length &&
        preYearData.map(x => {
          if (x.passingYear > currentYear) {
            this.setState({ ...x });
            if (x.state) {
              this.props.fetchDistrict({ stateId: x.state });
            }
            cb("degree", getNestedObjKey(x, ["academicClass", "id"]));
            cb("subject", getNestedObjKey(x, ["stream"]));
            cb("instituteName", getNestedObjKey(x, ["instituteName"]));
            cb("year", getNestedObjKey(x, ["currentAcademicYear"]));
            cb("state", getNestedObjKey(x, ["state"]));
            cb("address", getNestedObjKey(x, ["address"]));
            cb("city", getNestedObjKey(x, ["city"]));
            cb("district", getNestedObjKey(x, ["district"]));
            cb("passingYear", getNestedObjKey(x, ["passingYear"]));
            cb("marksObtained", getNestedObjKey(x, ["marksObtained"]));
            cb("totalMarks", getNestedObjKey(x, ["totalMarks"]));
            if (getNestedObjKey(x, ["presentClass"]) == 1) {
              cb(
                "radioClassStatus",
                getNestedObjKey(x, ["presentClass"]) == 1 ? "presentClass" : ""
              );
            } else if (getNestedObjKey(x, ["previousClass"]) == 1) {
              cb(
                "radioClassStatus",
                getNestedObjKey(x, ["previousClass"]) == 1
                  ? "previousClass"
                  : ""
              );
            }
          }
        });
    } else {
      if (data.state) {
        this.props.fetchDistrict({ stateId: data.state });
      }
      this.setState({ isEdit: true, ...data, activeEdit: true });
      this.props.fetchCourses({
        courseId: getNestedObjKey(data, ["academicClass", "id"])
      });
      cb("id", getNestedObjKey(data, ["id"]));
      cb("degree", getNestedObjKey(data, ["academicClass", "id"]));
      cb("subject", getNestedObjKey(data, ["stream"]));
      cb("instituteName", getNestedObjKey(data, ["instituteName"]));
      cb("year", getNestedObjKey(data, ["currentAcademicYear"]));
      cb("state", getNestedObjKey(data, ["state"]));
      cb("address", getNestedObjKey(data, ["address"]));
      cb("city", getNestedObjKey(data, ["city"]));
      cb("district", getNestedObjKey(data, ["district"]));
      cb("passingYear", getNestedObjKey(data, ["passingYear"]));
      cb("marksObtained", getNestedObjKey(data, ["marksObtained"]));
      cb("totalMarks", getNestedObjKey(data, ["totalMarks"]));
      cb("radioClassStatus", "previousClass");
    }
  }
  cancelEditing(cb) {
    cb("id", "");
    cb("degree", "");
    cb("subject", "");
    cb("year", "");
    cb("marksObtained", "");
    cb("totalMarks", "");
    cb("radioClassStatus", "");
    cb("passingYear", "");
    cb("district", "");
    cb("address", "");
    cb("state", "");
    cb("instituteName", "");
    cb("city", "");
    this.setState({ isEdit: false, activeAdd: false, activeEdit: false });
  }
  goNextPage() {
    const { eduData } = this.state;
    let isGoNext = true;
    const filEdu = eduData.length && eduData.filter(x => x.presentClass == 1);
    if (!filEdu.length) {
      isGoNext = false;
      this.setState({
        showAlertPopup: true,
        alertMsg: "Please add your present class",
        isError: true
      });
      return;
    }
    eduData.length &&
      eduData.forEach(x => {
        if (x.presentClass != 1 && !x.marksObtained) {
          isGoNext = false;
          this.setState({
            showAlertPopup: true,
            alertMsg: `Please add previous marks of 
                ${x.academicClassName}`,
            isError: true
          });
          return;
        }
      });
    if (isGoNext) {
      this.props.goNext();
    }
  }
  toggle(schId) {
    if (this.state.scholarshipId == "" || this.state.scholarshipId != schId) {
      this.setState({ scholarshipId: schId, clasName: "open" });
      this.props.fetchScholarEduDetail({ scholarshipId: schId });
    } else {
      this.setState({ scholarshipId: "", clasName: "" });
    }
  }
  getStateName(id) {
    const { ruleFilter } = this.props;
    let state;
    if (id) {
      const stateNames = getNestedObjKey(ruleFilter, ["state"]);
      stateNames &&
        stateNames.length &&
        stateNames.forEach(s => {
          if (s.id == id) {
            state = s.rulevalue;
            return;
          }
        });
    }
    return state;
  }
  checkClass(classId) {
    if (!classId || this.state.activeEdit) {
      return 0;
    } else if (classId <= 20 && !this.state.activeAdd) {
      return classId;
    } else if (classId <= 20) {
      return classId + 1;
    } else {
      return classId;
    }
  }
  render() {
    const { scholarDocs } = this.props;
    const {
      eduData,
      isEdit,
      isAdd,
      scholarshipId,
      ruleFilter,
      courses,
      districtData,
      clasName
    } = this.state;
    const presClass = eduData.filter(x => x.presentClass == 1);
    const prevClass = eduData.filter(x => x.presentClass != 1);
    const classes =
      ruleFilter &&
      ruleFilter.class.filter(
        x =>
          x.id >=
          this.checkClass(getNestedObjKey(eduData, [0, "academicClass", "id"]))
      );
    return (
      <section className="ctrl-wrapper widget-border newdoclist">
        <h4>Education Details</h4>
        {this.state.showAlertPopup && (
          <AlertMessagePopup
            msg={this.state.alertMsg}
            isShow={this.state.showAlertPopup}
            status={!this.state.isError}
            close={this.closeAlertPopup}
          />
        )}
        <Loader isLoader={this.props.scholarLoader} />
        {scholarDocs &&
          scholarDocs.length &&
          scholarDocs.map(x => (
            <section className="col-md-12">
              <article className="ctrl-wrapper">
                <h6
                  className={`h6 ${clasName}`}
                  onClick={() => this.toggle(x.scholarshipId)}
                >
                  {x.title}
                </h6>
                {scholarshipId == x.scholarshipId ? (
                  <article>
                    <article>
                      <Formik
                        initialValues={this.provideFormInitialValues()}
                        onSubmit={values => {
                          let finalData = {
                            scholarshipId: this.state.scholarshipId,
                            disbursalId: this.state.disbursalId,
                            data: {
                              academicDetails: [
                                {
                                  academicClass: {
                                    id: parseInt(values.degree)
                                  },
                                  id: parseInt(values.id),
                                  stream: parseInt(values.subject),
                                  currentAcademicYear: parseInt(values.year),
                                  marksObtained: parseFloat(
                                    values.marksObtained
                                  ),
                                  totalMarks: parseInt(values.totalMarks),
                                  presentClass:
                                    values.radioClassStatus == "presentClass"
                                      ? 1
                                      : 0,
                                  previousClass:
                                    values.radioClassStatus == "previousClass"
                                      ? 1
                                      : 0,
                                  state: values.state,
                                  district: values.district,
                                  city: values.city,
                                  address: values.address,
                                  instituteName: values.instituteName,
                                  passingYear: values.passingYear
                                }
                              ]
                            }
                          };
                          const semiFinal = getNestedObjKey(finalData, [
                            "data"
                          ]);
                          if (
                            this.state.activeAdd &&
                            this.state.presentClassData &&
                            this.state.presentClassData.academicClassName
                          ) {
                            semiFinal.academicDetails.push({
                              ...this.state.presentClassData,
                              presentClass: 0,
                              previousClass: 1
                            });
                            finalData.data = semiFinal;
                          }
                          this.props.sendEduDetail(finalData);
                        }}
                        enableReinitialize
                        validationSchema={eduValidationSchema}
                      >
                        {({ errors, touched, values, setFieldValue }) => {
                          return (
                            <Form name="EduForm">
                              <article className="bankDeatils">
                                <article className="col-md-12">
                                  <button
                                    type="button"
                                    onClick={() =>
                                      this.handleClass(setFieldValue, "add")
                                    }
                                    className="btn btn-yellow marginTop2 margintop15 pull-right btnheight"
                                    disabled={isAdd}
                                  >
                                    <i className="fa fa-plus" /> &nbsp; Add
                                    class
                                  </button>
                                </article>
                                <article className="boxBorder">
                                  <article className="ctrl-wrapper">
                                    <article className="table-responsive dataTbl">
                                      <table className="table table-striped">
                                        <thead>
                                          <tr>
                                            <th scope="col">Class/Degree</th>
                                            <th scope="col">Course/Stream</th>
                                            <th scope="col">Year</th>
                                            <th scope="col">Institute Name</th>
                                            <th scope="col">Passing Year</th>
                                            <th scope="col">Institute City</th>
                                            <th scope="col">
                                              Institute Address
                                            </th>
                                            <th scope="col">State</th>
                                            <th scope="col">Obtained marks</th>
                                            <th scope="col">Total marks</th>
                                            <th scope="col">Action</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          {presClass.length ? (
                                            <tr>
                                              <td colSpan="14">
                                                <h4>Present Class</h4>
                                              </td>
                                            </tr>
                                          ) : (
                                              ""
                                            )}
                                          {presClass.length
                                            ? presClass.map(x => (
                                              <tr>
                                                <td scope="row">
                                                  {x.academicClassName}
                                                </td>
                                                <td>{x.streamName}</td>
                                                <td>
                                                  {x.academicClass.id > 16
                                                    ? x.currentAcademicYear
                                                    : "N/A"}
                                                </td>
                                                <td>{x.instituteName}</td>
                                                <td>{x.passingYear}</td>
                                                <td>{x.city}</td>
                                                <td>{x.address}</td>
                                                <td>
                                                  {this.getStateName(x.state)}
                                                </td>
                                                <td>{x.marksObtained}</td>
                                                <td>{x.totalMarks}</td>
                                                <td>
                                                  <button
                                                    type="button"
                                                    onClick={() =>
                                                      this.handleClass(
                                                        setFieldValue,
                                                        x
                                                      )
                                                    }
                                                    className="btn btnEditheight"
                                                    disabled={true}
                                                  >
                                                    Add marks
                                                    </button>
                                                </td>
                                              </tr>
                                            ))
                                            : ""}
                                        </tbody>
                                        <tbody>
                                          {prevClass.length ? (
                                            <tr>
                                              <td colSpan="14">
                                                <h4>Previous Class</h4>
                                              </td>
                                            </tr>
                                          ) : (
                                              ""
                                            )}
                                          {prevClass.length
                                            ? prevClass.map(x => (
                                              <tr>
                                                <td scope="row">
                                                  {x.academicClassName}
                                                </td>
                                                <td>{x.streamName}</td>
                                                <td>
                                                  {x.academicClass.id > 16
                                                    ? x.currentAcademicYear
                                                    : "N/A"}
                                                </td>
                                                <td>{x.instituteName}</td>
                                                <td>{x.passingYear}</td>
                                                <td>{x.city}</td>
                                                <td>{x.address}</td>
                                                <td>
                                                  {this.getStateName(x.state)}
                                                </td>
                                                <td>{x.marksObtained}</td>
                                                <td>{x.totalMarks}</td>
                                                <td>
                                                  <button
                                                    type="button"
                                                    onClick={() =>
                                                      this.handleClass(
                                                        setFieldValue,
                                                        x
                                                      )
                                                    }
                                                    className="btn btnEditheight"
                                                    disabled={
                                                      x.presentClass != 1 &&
                                                        !x.marksObtained
                                                        ? false
                                                        : true
                                                    }
                                                  >
                                                    Add marks
                                                    </button>
                                                </td>
                                              </tr>
                                            ))
                                            : ""}
                                        </tbody>
                                      </table>
                                    </article>
                                  </article>
                                </article>
                                {isEdit ? (
                                  <article className="boxBorder">
                                    <article className="ctrl-wrapper">
                                      <article className="col-md-12">
                                        {/* <article className="col-md-12 radiobtn">
                                          <label class="radio-inline">
                                            <input
                                              type="radio"
                                              checked={
                                                values.radioClassStatus ==
                                                "presentClass"
                                                  ? true
                                                  : false
                                              }
                                              value="presentClass"
                                              name="radioClassStatus"
                                              onChange={e =>
                                                this.handleChange(
                                                  e,
                                                  setFieldValue
                                                )
                                              }
                                            />Present class
                                          </label>
                                          <label class="radio-inline">
                                            <input
                                              type="radio"
                                              checked={
                                                values.radioClassStatus ==
                                                "previousClass"
                                                  ? true
                                                  : false
                                              }
                                              name="radioClassStatus"
                                              value="previousClass"
                                              onChange={e =>
                                                this.handleChange(
                                                  e,
                                                  setFieldValue
                                                )
                                              }
                                            />Previous class
                                          </label>
                                          {errors.radioClassStatus &&
                                            touched.radioClassStatus && (
                                              <span className="error">
                                                {errors.radioClassStatus}
                                              </span>
                                            )}
                                        </article> */}
                                        <article className="form-group margintop20">
                                          <label for="passed">
                                            Class/Degree *
                                          </label>
                                          <Field
                                            className="form-control"
                                            component="select"
                                            name="degree"
                                            onChange={e =>
                                              this.handleChange(
                                                e,
                                                setFieldValue
                                              )
                                            }
                                            disabled={getNestedObjKey(
                                              this.state,
                                              ["academicClass", "id"]
                                            )}
                                          >
                                            <option value="">
                                              --Class/Degree--
                                            </option>
                                            {classes && classes.length
                                              ? classes.map(c => {
                                                if (c.id != 20) {
                                                  return (
                                                    <option
                                                      key={c.id}
                                                      value={c.id}
                                                    >
                                                      {c.rulevalue}
                                                    </option>
                                                  );
                                                }
                                              })
                                              : ""}
                                          </Field>
                                          {errors.degree &&
                                            touched.degree && (
                                              <span className="error">
                                                {errors.degree}
                                              </span>
                                            )}
                                        </article>
                                        <article className="form-group margintop20">
                                          <label for="marks">
                                            Institute Name *
                                          </label>
                                          <Field
                                            type="text"
                                            className="form-control"
                                            placeholder="Institute Name"
                                            name="instituteName"
                                            disabled={this.state.instituteName}
                                          />
                                          {errors.instituteName &&
                                            touched.instituteName && (
                                              <span className="error">
                                                {errors.instituteName}
                                              </span>
                                            )}
                                        </article>
                                        {values.degree >= 12 ? (
                                          <article className="form-group margintop20">
                                            <label for="course">
                                              Course/Subject/Stream *
                                            </label>
                                            <Field
                                              className="form-control"
                                              component="select"
                                              name="subject"
                                              onChange={e =>
                                                this.handleChange(
                                                  e,
                                                  setFieldValue
                                                )
                                              }
                                              disabled={this.state.stream}
                                            >
                                              <option value="">
                                                --Course/Subject/Stream--
                                              </option>
                                              {courses && courses.length
                                                ? courses.map(c => (
                                                  <option
                                                    key={c.id}
                                                    value={c.id}
                                                  >
                                                    {c.rulevalue}
                                                  </option>
                                                ))
                                                : ""}
                                            </Field>
                                            {errors.subject &&
                                              touched.subject && (
                                                <span className="error">
                                                  {errors.subject}
                                                </span>
                                              )}
                                          </article>
                                        ) : (
                                            ""
                                          )}
                                        {values.degree > 20 && (
                                          <article>
                                            <article className="form-group margintop20">
                                              <label for="marks">
                                                Passing Year *
                                              </label>
                                              <Field
                                                component="select"
                                                className="form-control"
                                                placeholder="Passing Year"
                                                name="passingYear"
                                                disabled={
                                                  this.state.passingYear
                                                }
                                              >
                                                <option value="">
                                                  --Passing year--
                                                </option>
                                                {getPassingYear().map(y => (
                                                  <option
                                                    key={y.id}
                                                    value={y.id}
                                                  >
                                                    {y.year}
                                                  </option>
                                                ))}
                                              </Field>
                                              {errors.passingYear &&
                                                touched.passingYear && (
                                                  <span className="error">
                                                    {errors.passingYear}
                                                  </span>
                                                )}
                                            </article>
                                            <article className="form-group margintop20">
                                              <label for="passed">
                                                Current Year *
                                              </label>
                                              <Field
                                                className="form-control"
                                                component="select"
                                                name="year"
                                                onChange={e =>
                                                  this.handleChange(
                                                    e,
                                                    setFieldValue
                                                  )
                                                }
                                              >
                                                <option value="">
                                                  --Current year--
                                                </option>
                                                {getAcadmicYear().map(y => (
                                                  <option
                                                    key={y.id}
                                                    value={y.id}
                                                  >
                                                    {y.year}
                                                  </option>
                                                ))}
                                              </Field>
                                              {errors.year &&
                                                touched.year && (
                                                  <span className="error">
                                                    {errors.year}
                                                  </span>
                                                )}
                                            </article>
                                            <article className="form-group margintop20">
                                              <label for="marks">
                                                Institute City *
                                              </label>
                                              <Field
                                                type="text"
                                                className="form-control"
                                                placeholder="Institute City"
                                                name="city"
                                                disabled={this.state.city}
                                              />
                                              {errors.city &&
                                                touched.city && (
                                                  <span className="error">
                                                    {errors.city}
                                                  </span>
                                                )}
                                            </article>
                                            <article className="form-group margintop20">
                                              <label for="marks">State *</label>
                                              <Field
                                                className="form-control"
                                                component="select"
                                                name="state"
                                                onChange={e =>
                                                  this.handleChange(
                                                    e,
                                                    setFieldValue
                                                  )
                                                }
                                                disabled={this.state.state}
                                              >
                                                <option>--State--</option>
                                                {ruleFilter.state &&
                                                  ruleFilter.state.length
                                                  ? ruleFilter.state.map(s => (
                                                    <option
                                                      key={s.id}
                                                      value={s.id}
                                                    >
                                                      {s.rulevalue}
                                                    </option>
                                                  ))
                                                  : ""}
                                              </Field>
                                              {errors.state &&
                                                touched.state && (
                                                  <span className="error">
                                                    {errors.state}
                                                  </span>
                                                )}
                                            </article>
                                            <article className="form-group margintop20">
                                              <label for="marks">
                                                District *
                                              </label>
                                              <Field
                                                className="form-control"
                                                component="select"
                                                name="district"
                                                onChange={e =>
                                                  this.handleChange(
                                                    e,
                                                    setFieldValue
                                                  )
                                                }
                                                disabled={this.state.district}
                                              >
                                                <option>--District--</option>
                                                {districtData &&
                                                  districtData.length
                                                  ? districtData.map(d => (
                                                    <option
                                                      key={d.id}
                                                      value={d.id}
                                                    >
                                                      {d.districtName}
                                                    </option>
                                                  ))
                                                  : ""}
                                              </Field>
                                              {errors.district &&
                                                touched.district && (
                                                  <span className="error">
                                                    {errors.district}
                                                  </span>
                                                )}
                                            </article>
                                            <article className="form-group margintop20">
                                              <label for="marks">
                                                Institute Address *
                                              </label>
                                              <Field
                                                type="text"
                                                className="form-control"
                                                placeholder="Institute Address"
                                                name="address"
                                                disabled={this.state.address}
                                              />
                                              {errors.address &&
                                                touched.address && (
                                                  <span className="error">
                                                    {errors.address}
                                                  </span>
                                                )}
                                            </article>
                                          </article>
                                        )}
                                        {values.radioClassStatus ==
                                          "previousClass" ? (
                                            <article>
                                              <article className="form-group margintop20">
                                                <label for="marks">
                                                  Marks Obtained *
                                              </label>
                                                <Field
                                                  type="number"
                                                  className="form-control"
                                                  placeholder="Marks Obtain"
                                                  name="marksObtained"
                                                />
                                                {errors.marksObtained &&
                                                  touched.marksObtained && (
                                                    <span className="error">
                                                      {errors.marksObtained}
                                                    </span>
                                                  )}
                                              </article>
                                              <article className="form-group margintop20">
                                                <label for="totalmarks">
                                                  Total Marks *
                                              </label>
                                                <Field
                                                  type="number"
                                                  className="form-control"
                                                  placeholder="Total Marks"
                                                  name="totalMarks"
                                                />
                                                {errors.totalMarks &&
                                                  touched.totalMarks && (
                                                    <span className="error">
                                                      {errors.totalMarks}
                                                    </span>
                                                  )}
                                              </article>
                                            </article>
                                          ) : (
                                            ""
                                          )}
                                        <article className="col-md-12 pull-right">
                                          <input
                                            className="btn pull-right btnheight"
                                            type="submit"
                                            value="Save"
                                          />
                                          <input
                                            className="btn pull-right btnheight"
                                            onClick={() =>
                                              this.cancelEditing(setFieldValue)
                                            }
                                            type="button"
                                            value="Cancel"
                                          />
                                        </article>
                                      </article>
                                    </article>
                                  </article>
                                ) : null}
                              </article>
                            </Form>
                          );
                        }}
                      </Formik>
                    </article>
                    <article className="btn-ctrl margintop15">
                      <button type="button" onClick={this.goNextPage}>
                        Next
                      </button>
                    </article>
                  </article>
                ) : (
                    ""
                  )}
              </article>
            </section>
          ))}
      </section>
    );
  }
}
