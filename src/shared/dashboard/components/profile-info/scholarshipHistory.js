import React, { Component } from "react";

import Loader from "../../../common/components/loader";
import { ruleRunner } from "../../../../validation/ruleRunner";
import {
  required,
  isEmail,
  minLength,
  isNumeric,
  lengthRange
} from "../../../../validation/rules";
import {
  scholarshipHistoryTypes,
  yearArray
} from "../../../../constants/constants";
import {
  UPDATE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS,
  DELETE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS,
  FETCH_OCCUPATION_DATA_SUCCESS
} from "../../actions";
import { FETCH_RULES_SUCCEEDED } from "../../../../constants/commonActions";

import { confirmationMessages } from "../../../../constants/constants";
import ConfirmMessagePopup from "../../../common/components/confirmMessagePopup";
class ScholarshipHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      source: "",
      year: "",
      description: "",
      goToNextPage: false,
      showConfirmationPopup: false,
      deleteSuccessCallBack: null,
      validations: {
        name: null,
        source: null,
        year: null,
        description: null
      },
      isFormValid: false,
      isFormShown: false
    };

    this.showForm = this.showForm.bind(this);
    this.closeForm = this.closeForm.bind(this);
    this.formChangeHandler = this.formChangeHandler.bind(this);
    this.editClickHandler = this.editClickHandler.bind(this);
    this.deleteClickHandler = this.deleteClickHandler.bind(this);
    this.addOrUpdateScholarshipHistoryDetails = this.addOrUpdateScholarshipHistoryDetails.bind(
      this
    );
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.saveAndGoToNextPage = this.saveAndGoToNextPage.bind(this);

    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.hideConfirmationPopup = this.hideConfirmationPopup.bind(this);
  }

  addOrUpdateScholarshipHistoryDetails() {
    const addOrUpdateApiParams = this.mapToUpdateApiParams();

    //Add validations
    if (this.checkFormValidations()) {
      this.props.addOrUpdateScholarshipHistoryDetails(addOrUpdateApiParams);
    }
  }

  showConfirmationPopup() {
    this.setState({
      showConfirmationPopup: true
    });
  }

  hideConfirmationPopup() {
    this.setState({
      showConfirmationPopup: false,
      deleteSuccessCallBack: null
    });
  }

  saveAndGoToNextPage(e) {
    e.preventDefault();
    this.setState({ goToNextPage: true }, () =>
      this.addOrUpdateScholarshipHistoryDetails()
    );
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case UPDATE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS:
        if (this.state.goToNextPage) {
          this.props.goToNextPage("References");
        } else {
          this.setState(
            {
              isFormShown: false,
              id: "",
              name: "",
              source: "",
              year: "",
              description: ""
            },
            () =>
              this.props.fetchScholarshipHistoryDetails({
                userid: this.props.userId
              })
          );
        }
        break;

      case FETCH_OCCUPATION_DATA_SUCCESS:
        this.props.fetchScholarshipHistoryDetails({
          userid: this.props.userId
        });
        break;

      case FETCH_RULES_SUCCEEDED:
        this.props.fetchOccupationData();
        break;

      case DELETE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS:
        this.setState(
          {
            showConfirmationPopup: false,
            deleteSuccessCallBack: null
          },
          () =>
            this.props.fetchScholarshipHistoryDetails({
              userid: this.props.userId
            })
        );

        break;
    }
  }

  mapToUpdateApiParams() {
    const { name, source, year, description, id } = this.state;

    let params = {
      name,
      source,
      year,
      description
    };

    if (id) {
      params.id = id;
    }

    return { userid: this.props.userId, params };
  }

  editClickHandler(id) {
    const scholarshipData = this.props.scholarshipHistoryData.filter(
      data => data.id === id
    );

    const { name, source, year, description } = scholarshipData[0];

    this.setState({
      id,
      name,
      source,
      year,
      description,
      isFormShown: true
    });
  }

  deleteClickHandler(id) {
    const deleteScholarshipHistory = () =>
      this.props.deleteScholarshipHistoryDetails({
        userid: this.props.userId,
        scholarshipHistoryId: id
      });

    this.setState({
      showConfirmationPopup: true,
      deleteSuccessCallBack: deleteScholarshipHistory
    });
  }

  showForm() {
    this.setState({
      isFormShown: true
    });
  }

  /************ start - validation part - rules******************* */
  formChangeHandler(event) {
    const { id, value } = event.target;
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );
    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];
    this.setState({
      [id]: value,
      validations
    });
  }
  /************ end - validation part - rules******************* */

  closeForm() {
    this.setState({
      isFormShown: false,
      id: "",
      name: "",
      source: "",
      year: "",
      description: ""
    });
  }

  componentDidMount() {
    if (this.props.rulesList) {
      this.props.fetchOccupationData();
    } else {
      this.props.loadRules();
    }
  }

  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "name":
        validationObject.name = "* Scholarship name";
        validationObject.validationFunctions = [required, lengthRange(3, 50)];
        return validationObject;
      case "source":
        validationObject.name = "* Scholarship source";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "year":
        validationObject.name = "* Year";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "description":
        validationObject.name = "* Scholarship description";
        validationObject.validationFunctions = [required];
        return validationObject;
    }
  }
  /************ end - validation part - get message and required validation******************* */

  /************ start - validation part - check form submit validation******************* */
  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }
  /************ start - validation part - check form submit validation******************* */
  // rawMarkUp() {
  //     let rawMarkup = this.props.privacyPolicyList.privacyConditions
  //     return { __html: rawMarkup };
  // }

  render() {
    return (
      <section>
        <ConfirmMessagePopup
          message={confirmationMessages.GENERIC}
          showPopup={this.state.showConfirmationPopup}
          onConfirmationSuccess={this.state.deleteSuccessCallBack}
          onConfirmationFailure={this.hideConfirmationPopup}
        />
        <Loader isLoader={this.props.showLoader} />
        <article className="ctrl-wrapper widget-border">
          <h4>Won Scholarships</h4>
          <article className="col-md-12">
            <article className="ctrl-wrapper">
              <article className="table-responsive dataTbl">
                <ScholarshipHistoryTable
                  scholarshipHistoryData={this.props.scholarshipHistoryData}
                  editClickHandler={this.editClickHandler}
                  deleteClickHandler={this.deleteClickHandler}
                />
              </article>
            </article>
          </article>
        </article>
        <article className="col-md-12 width100">
          <article className="row">
            <article className="btn-ctrl">
              <button type="button" onClick={this.showForm}>
                Add Scholarship
              </button>
              {/* <span
                onClick={() => this.props.goToNextPage("References")}
                className="btn pull-right but-reference"
              >
                Next
              </span> */}
            </article>
          </article>
        </article>
        <AddScholarshipHistoryForm
          validations={this.state.validations}
          formChangeHandler={this.formChangeHandler}
          description={this.state.description}
          isFormShown={this.state.isFormShown}
          closeForm={this.closeForm}
          source={this.state.source}
          name={this.state.name}
          year={this.state.year}
          saveAndGoToNextPage={this.saveAndGoToNextPage}
          isFormValid={this.state.isFormValid}
          addOrUpdateScholarshipHistoryDetails={
            this.addOrUpdateScholarshipHistoryDetails
          }
        />
      </section>
    );
  }
}

const AddScholarshipHistoryForm = props => {
  const {
    isShown,
    name,
    isFormShown,
    source,
    year,
    description,
    formChangeHandler,
    closeForm,
    isFormValid,
    validations,
    saveAndGoToNextPage,
    addOrUpdateScholarshipHistoryDetails
  } = props;

  if (isFormShown) {
    return (
      <form name="receivedSchForm" autoCapitalize="off">
        <article>
          <article className="ctrl-wrapper widget-border topM">
            <h4 className="titleMargin">Have you won any scholarship?</h4>
            <article className="row">
              <article className="col-md-6">
                <label htmlFor="sname">Scholarship Name*</label>
                <article className="form-group">
                  <input
                    type="text"
                    name="sname"
                    className="form-control"
                    id="name"
                    placeholder="Enter scholarship name"
                    required
                    value={name}
                    onChange={formChangeHandler}
                  />

                  {validations.name ? (
                    <span className="error animated bounce">
                      {validations.name}
                    </span>
                  ) : null}
                </article>
              </article>
            </article>
            <article className="row">
              <article className="col-md-6">
                <label htmlFor="scholarship-source">Scholarship Source*</label>
                <article className="form-group">
                  <select
                    name="scholarshipSrc"
                    className="form-control"
                    id="source"
                    required
                    value={source}
                    onChange={formChangeHandler}
                  >
                    <option value="">--Select--</option>
                    {scholarshipHistoryTypes.map(value => (
                      <option value={value}>{value}</option>
                    ))}
                  </select>
                  {validations.source ? (
                    <span className="error animated bounce">
                      {validations.source}
                    </span>
                  ) : null}
                </article>
              </article>
              <article className="col-md-6">
                <label htmlFor="email">Year in which you've received*</label>
                <article className="form-group">
                  <select
                    name="year"
                    className="form-control"
                    id="year"
                    value={year}
                    onChange={formChangeHandler}
                    required
                  >
                    <option value="" translate="">
                      --Select Year--
                    </option>
                    {yearArray.map(year => (
                      <option value={year}>{year.toString()}</option>
                    ))}
                  </select>
                  {validations.year ? (
                    <span className="error animated bounce">
                      {validations.year}
                    </span>
                  ) : null}
                </article>
              </article>
            </article>

            <article className="row">
              <article className="col-md-12">
                <label htmlFor="description">Description*</label>
                <article className="form-group textAreaCrtl">
                  <textarea
                    name="desc"
                    className="form-control text-area"
                    id="description"
                    placeholder="Enter description"
                    onChange={formChangeHandler}
                    required
                    value={description}
                  />
                  {validations.description ? (
                    <span className="error animated bounce">
                      {validations.description}
                    </span>
                  ) : null}
                </article>
              </article>
            </article>
          </article>

          <article className="col-md-12 width100">
            <article className="row">
              <article className="btn-ctrl threeBtn">
                <button
                  type="button"
                  onClick={addOrUpdateScholarshipHistoryDetails}
                >
                  Submit
                </button>
                <button type="button" onClick={closeForm}>
                  Close
                </button>
                {/* <button onClick={saveAndGoToNextPage}>Save/Next</button> */}
              </article>
            </article>
          </article>
        </article>
      </form>
    );
  }

  return null;
};

const ScholarshipHistoryTable = props => {
  const { scholarshipHistoryData } = props;

  return (
    <table className="table table-striped">
      <tbody>
        <tr>
          <th>Scholarship Name</th>
          <th>Source</th>
          <th className="text-center">Year</th>
          <th className="text-center">Desc</th>
          <th className="text-center">Edit</th>
          <th className="text-center">Delete</th>
        </tr>
        {scholarshipHistoryData.map(scholarship => (
          <ScholarshipTableRow
            scholarship={scholarship}
            editClickHandler={props.editClickHandler}
            deleteClickHandler={props.deleteClickHandler}
          />
        ))}
      </tbody>
    </table>
  );
};

const ScholarshipTableRow = ({
  scholarship,
  editClickHandler,
  deleteClickHandler
}) => (
  <tr id={scholarship.id}>
    <td>{scholarship.name}</td>
    <td>{scholarship.source}</td>
    <td>{scholarship.year}</td>
    <td>{scholarship.description}</td>
    <td>
      <p className="text-green text-center">
        <span onClick={() => editClickHandler(scholarship.id)}>
          <i className="fa fa-pencil" aria-hidden="true" />
        </span>
      </p>
    </td>
    <td>
      <p className="text-green text-center">
        <span onClick={() => deleteClickHandler(scholarship.id)}>
          <i className="fa fa-trash-o" aria-hidden="true" />
        </span>
      </p>
    </td>
  </tr>
);

export default ScholarshipHistory;
