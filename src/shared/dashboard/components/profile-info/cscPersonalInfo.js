import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import DatePicker from "react-datepicker";
import moment from "moment";
if (typeof window !== "undefined") {
  require("react-datepicker/dist/react-datepicker.css");
}
import ServerError from "../../../common/components/serverError";
import { dependantRequests } from "../../../../constants/constants";
import {
  FETCH_USERLIST_SUCCEEDED,
  FETCH_FILTER_RULES_SUCCEEDED,
  FETCH_RULES_SUCCEEDED,
  FETCH_USER_RULES_SUCCESS,
  UPDATE_USER_RULES_SUCCESS,
  FETCH_DEPENDANT_DATA_SUCCEEDED
} from "../../../../constants/commonActions";
import {
  FETCH_UPDATE_USER_SUCCEEDED,
  FETCH_EDUCATION_INFO_SUCCESS,
  UPDATE_EDUCATION_INFO_SUCCESS,
  FETCH_DM_DC_SUCCESS,
  UPLOAD_DMDC_FAIL,
  UPLOAD_DMDC_SUCCESS
} from "../../actions";
import { ruleRunner } from "../../../../validation/ruleRunner";
import gblFunc from "../../../../globals/globalFunctions";
import Loader from "../../../common/components/loader";
import {
  required,
  isEmail,
  minLength,
  isNumeric,
  lengthRange,
  isMobileNumber,
  isPINCODE,
  isAadhar
} from "../../../../validation/rules";
import { isOneCheckBoxSelected } from "../../../../validation/errorMessages";
import { FETCH_DMDC_SUCCEEDED } from "../../../csc/actions";
import ConfirmMessagePopup from "../../../common/components/confirmMessagePopup";
import AlertMessage from "../../../common/components/alertMsg";

const hideSubject = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 25, 577, 752];
const elementryClass = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 25, 752];
const othersClass = [577];

class CscPersonalInfo extends Component {
  constructor(props) {
    super(props);

    this.currentFormChangeHandler = this.currentFormChangeHandler.bind(this);
    this.currentFormRulesHandler = this.currentFormRulesHandler.bind(this);
    this.getRuleId = this.getRuleId.bind(this);
    this.getDistrictList = this.getDistrictList.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.checkStateHandler = this.checkStateHandler.bind(this);
    this.onCheckBoxToggle = this.onCheckBoxToggle.bind(this);
    this.onClickHandler = this.onClickHandler.bind(this);
    this.createScholarshipRulesAndfetchUserRules = this.createScholarshipRulesAndfetchUserRules.bind(
      this
    );
    this.onEducationSubmitHandler = this.onEducationSubmitHandler.bind(this);
    this.saveDMDC = this.saveDMDC.bind(this);
    this.saveOtherDMDC = this.saveOtherDMDC.bind(this);

    this.state = {
      showConfirmationPopup: false,
      scholarshipCheckboxes: {},
      checkedCheckboxes: [],
      isEduValid: true,
      dmdc: null,
      otherName: null,
      isOtherName: false,
      dmdcInfo: {
        id: null,
        name: null,
        otherName: false
      },
      eduInfo: {
        userAcademicInfo: {
          academicClass: { id: null, value: null },
          board: null,
          courseDuration: "",
          currentDegreeYear: null,
          degree: null,
          // fee: null,
          id: null,
          otherBoard: "",
          passingMonth: null,
          passingYear: null,
          percentage: null,
          presentClass: 1,
          stream: null,
          marksObtained: "",
          totalMarks: null,
          grade: "",
          markingType: ""
        },
        userInstituteInfo: {
          // academicDetailId: null,
          address: "",
          city: "",
          // country: "",
          // description: "",
          district: null,
          id: null,
          instituteEmail: "",
          instituteName: "",
          //institutePhone: "",
          pincode: null,
          // principalName: "",
          state: null
        }
      },
      PERSONAL_INFO: {
        firstName: "",
        lastName: "",
        email: "",
        mobile: "",
        dob: "",
        aadharCard: "",
        familyIncome: "",
        portalId: 0,

        userAddress: {
          addressLine: "",
          city: "",
          country: "",
          district: { id: null, value: null },
          id: 0,
          pincode: "",
          state: { id: null, value: null },
          type: 1 //permanent address
        },
        userRules: []
      },

      rules: {},
      dropDownRules: {
        religion: {
          ruleId: null,
          ruleTypeId: 4
        },
        class: {
          ruleId: null,
          ruleTypeId: 1
        },
        quota: {
          ruleId: null,
          ruleTypeId: 6
        },
        gender: {
          ruleId: null,
          ruleTypeId: 5
        },
        disabled: {
          ruleId: 903,
          ruleTypeId: 48
        },
        foreign: {
          ruleId: 820,
          ruleTypeId: 46
        }
      },
      userid: null,
      isShow: false,
      onStatusCheck: null,
      dmdcMsg: "",
      ON_FIRST_LOAD: true,
      validations: {
        firstName: null,
        lastName: null,
        email: null,
        mobile: null,
        district: null,
        city: null,
        //gender: null,
        state: null,
        dmdc: null,
        dmDcOthers: null
      },
      interestValidation: null,
      isFormValid: false,
      isFilter: true
    };
  }

  componentWillReceiveProps(nextProps) {
    switch (nextProps.type) {
      case FETCH_UPDATE_USER_SUCCEEDED:
        const { firstName, lastName } = nextProps.updateUserInfo;
        localStorage.setItem("firstName", firstName);
        localStorage.setItem("lastName", lastName);
        this.props.updateUser({ firstName, lastName });
        //this.props.goToNextPage("Documents");

        // this.onEducationSubmitHandler();
        break;
      case FETCH_USERLIST_SUCCEEDED:
        const { userList } = nextProps;

        if (
          userList &&
          userList.userAddress &&
          userList.userAddress["district"]
        ) {
          this.props.fetchDmDc({
            stateId: userList.userAddress["district"]["stateId"],
            districtId: userList.userAddress["district"]["id"]
          });
        }

        if (userList && userList.userAddress && userList.userAddress["state"]) {
          if (
            userList.userAddress["state"] &&
            !this.props.district.length &&
            this.state.ON_FIRST_LOAD
          ) {
            this.getDistrictList("state", userList.userAddress["state"]["id"]);
          }
          this.setState({ ON_FIRST_LOAD: false });
        }

        this.props.fetchEducationInfoDetails({ userId: this.props.userId });
        break;
      case FETCH_EDUCATION_INFO_SUCCESS:
        this.mapUserApiToState(nextProps);
        if (this.props.rulesList) {
          const { special } = this.props.rulesList;
          this.createScholarshipRulesAndfetchUserRules(special);
        }
        break;
      // case FETCH_RULES_SUCCEEDED:
      //   const { special } = nextProps.rulesList;
      //   this.createScholarshipRulesAndfetchUserRules(special);

      //   break;
      case FETCH_USER_RULES_SUCCESS:
        if (nextProps.userRulesData) {
          let { userRules } = nextProps.userRulesData;

          let filteredUserRules = nextProps.userRulesData.userRules.filter(
            userRule => userRule.ruleTypeId === 7
          );

          userRules = filteredUserRules.reduce((acc, rule, i) => {
            acc[rule.ruleId] = true;
            return acc;
          }, {});

          let checkedCheckboxes = filteredUserRules.map(rule =>
            parseInt(rule.ruleId, 10)
          );

          let scholarshipCheckboxes = {
            ...this.state.scholarshipCheckboxes,
            ...userRules
          };

          this.setState({ scholarshipCheckboxes, checkedCheckboxes });
        }

        break;
      case UPDATE_EDUCATION_INFO_SUCCESS:
        //this.props.goToNextPage("Documents");
        gblFunc.scrollPage(this.refs.personalProfile, "personalProfile");
        break;
      case FETCH_DM_DC_SUCCESS:
        this.setState(
          {
            dmdc: nextProps.dmdcList
          },
          () =>
            this.props.getDMDC({
              userId: gblFunc.getStoreUserDetails()["userId"],
              isOther: true
            })
        );
        break;
      case FETCH_DMDC_SUCCEEDED:
        this.setState({
          dmdcInfo: nextProps.dmdcDetails,
          isOtherName:
            nextProps.dmdcDetails && nextProps.dmdcDetails.otherName
              ? true
              : false,
          otherName:
            nextProps.dmdcDetails && nextProps.dmdcDetails.otherName
              ? nextProps.dmdcDetails.name
              : null
        });
        break;
      case FETCH_DEPENDANT_DATA_SUCCEEDED:
        this.setState({
          otherName: null,
          isDMDCError: false,
          isOtherName: false,
          dmdcInfo: { id: null, name: null }
        });
        break;
    }
  }

  mapUserApiToState(nextProps) {
    const updatePersonal = { ...this.state.PERSONAL_INFO };
    const updateRules = { ...this.state.dropDownRules };
    const { eduInfo, isEduValid, isFilter } = this.state;
    const { userList, educationData, userRulesData } = nextProps;
    // const updateUserRule = { ...this.state.USER_RULE };
    if (userList) {
      for (let i in updatePersonal) {
        if (i !== "userRule") {
          updatePersonal[i] =
            i == "userAddress" && !userList[i]
              ? updatePersonal[i]
              : userList[i];

          this.setState({ userid: this.props.userId });
        }
      }

      if (userList.userRules && userList.userRules.length > 0) {
        let resRules = userList.userRules;
        resRules.map(list => {
          for (let key in updateRules) {
            if (updateRules[key].ruleTypeId == list.ruleTypeId) {
              updateRules[key].ruleId = list.ruleId;
            }
          }
        });
      }
    }

    let { serverError } = nextProps;
    if (serverError) {
      let validations = { ...this.state.validations };
      validations.city = serverError;
      this.setState({
        validations
      });
    }

    if (educationData && educationData.length > 0 && isEduValid) {
      const educationInfo = educationData.filter(
        eduInfo => eduInfo.userAcademicInfo.presentClass == 1
      );

      const { userAcademicInfo, userInstituteInfo } =
        educationInfo[0] || eduInfo;

      if (userAcademicInfo && userInstituteInfo) {
        this.mapApiToCurrentState(
          userAcademicInfo,
          "userAcademicInfo",
          nextProps.type
        );
        this.mapApiToCurrentState(
          userInstituteInfo,
          "userInstituteInfo",
          nextProps.type
        );
      }
      if (
        userAcademicInfo &&
        userAcademicInfo["academicClass"] &&
        nextProps.type == "FETCH_EDUCATION_INFO_SUCCESS" &&
        isFilter
      ) {
        this.getFilterRules(userAcademicInfo["academicClass"]["id"]);
      }
    }

    if (educationData.length === 0) {
      const rules = { ...this.state.dropDownRules };
      if (rules.class && rules.class.ruleId) {
        this.state.eduInfo.userAcademicInfo.academicClass.id =
          rules.class.ruleId;
        this.getFilterRules(
          this.state.eduInfo.userAcademicInfo.academicClass.id
        );
      }
    }

    this.setState({
      ...this.state,
      PERSONAL_INFO: updatePersonal,
      dropDownRules: updateRules,
      isFilter: false
      // USER_RULE: updateUserRule
    });
  }

  mapApiToCurrentState(userEduData, KEY, type) {
    const userUpdatedState = { ...this.state.eduInfo };

    for (let key in userUpdatedState[KEY]) {
      userUpdatedState[KEY][key] = userEduData[key];
    }

    this.setState({
      ...this.state,
      eduInfo: userUpdatedState,
      isEduValid: false
    });
  }

  componentDidMount() {
    this.props.loadRules();
    if (this.props.userId) {
      this.props.loadUserList(this.props.userId);
    }
  }

  getFilterRules(ruleId) {
    this.props.fetchFilterRuleById(ruleId);
  }

  onSubmitHandler(event) {
    event.preventDefault();
    const userRules = [];
    let userRulesApiParams = null;
    const personal_info = {
      ...this.state.PERSONAL_INFO,
      ...this.state.PERSONAL_INFO.userAddress
    };
    const address = { ...this.state.PERSONAL_INFO.userAddress };
    const updaterules = { ...this.state.dropDownRules };

    // console.log(updaterules);

    for (let val in updaterules) {
      userRules.push(updaterules[val]);
    }

    // if (this.state.checkedCheckboxes.length > 0) {
    //   userRulesApiParams = this.state.checkedCheckboxes.map(rule => ({
    //     ruleId: rule,
    //     ruleTypeId: 7
    //   }));

    //   // this.props.addOrUpdateUserRules({
    //   //   userid: this.props.userId,
    //   //   params: userRulesApiParams
    //   // });
    // } else {
    //   const interestValidation = isOneCheckBoxSelected("Scholarship interest");
    //   this.setState({
    //     interestValidation
    //   });
    //   // return;
    // }

    personal_info.userRules = userRules;

    if (this.checkFormValidations()) {
      this.props.loadSaveUser(personal_info, this.props.userId); //this.props.userId
    }
    this.setState({ isFormValid: false });
  }

  onEducationSubmitHandler() {
    const updateEducation = { ...this.state.eduInfo };
    if (this.checkFormValidations()) {
      this.props.updateEducationInfo({
        updateEducation,
        userId: this.props.userId
      });
    }
    this.setState({ isFormValid: false });
  }

  onClickHandler() {
    if (this.state.checkedCheckboxes.length > 0) {
      const userRulesApiParams = this.state.checkedCheckboxes.map(rule => ({
        ruleId: rule,
        ruleTypeId: 7
      }));

      this.props.addOrUpdateUserRules({
        userid: this.props.userId,
        params: userRulesApiParams
      });
    } else {
      const interestValidation = isOneCheckBoxSelected("Scholarship interest");
      this.setState({
        interestValidation
      });
    }
  }

  createScholarshipRulesAndfetchUserRules(scholarshipRules) {
    const scholarshipTypesObject = scholarshipRules.reduce(
      (acc, curScholarship, i) => {
        acc[curScholarship.id] = false;
        return acc;
      },
      {}
    );

    this.setState({ scholarshipCheckboxes: scholarshipTypesObject }, () =>
      this.props.fetchUserRules({ userid: this.props.userId })
    );
  }

  currentFormChangeHandler(event, KEY, SUBKEY = "") {
    let index =
      event.nativeEvent && event.nativeEvent.target
        ? event.nativeEvent.target.selectedIndex
        : null;

    let nameValue =
      event.nativeEvent &&
        event.nativeEvent.target &&
        event.nativeEvent.target[index]
        ? event.nativeEvent.target[index].text
        : null;
    const updatePersonForm = { ...this.state.PERSONAL_INFO };

    const { id, value } =
      KEY == "dob"
        ? { id: "dob", value: moment(event).format("YYYY-MM-DD") }
        : event.target;
    let validations = { ...this.state.validations };

    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      const validationResult = ruleRunner(
        value,
        id,
        name,
        ...validationFunctions
      );

      validations[id] = validationResult[id];
    }

    if (!SUBKEY) {
      updatePersonForm[KEY] = value;
    } else {
      let typeOfProp = {
        addressLine: value,
        city: value,
        country: value,
        district: {
          id: +value,
          value: nameValue
        },
        id: +value,
        pincode: value,
        state: {
          id: +value,
          value: nameValue
        },
        familyIncome: value
      };
      updatePersonForm[SUBKEY]
        ? (updatePersonForm[SUBKEY][KEY] = typeOfProp[KEY])
        : null;
    }

    if (id == "academicClass") {
      this.getFilterRules(value);
    }

    // let keyToUpperCase = KEY.toUpperCase();
    if (dependantRequests.includes(KEY)) {
      //TODO: Call dependant key request , pick from props in container
      this.getDistrictList(KEY, event.target.value);
      this.setState({
        ON_FIRST_LOAD: false
      });
    }

    if (KEY === "district") {
      this.setState(
        {
          otherName: null,
          isOtherName: false,
          isDMDCError: false,
          dmdcInfo: { id: null, name: null }
        },
        () =>
          this.props.fetchDmDc({
            stateId: updatePersonForm["userAddress"]["state"]["id"],
            districtId: value
          })
      );
    }

    this.setState({
      PERSONAL_INFO: updatePersonForm,
      validations
    });
  }

  dmdcHandler(event, KEY, SUBKEY = "") {
    let index =
      event.nativeEvent && event.nativeEvent.target
        ? event.nativeEvent.target.selectedIndex
        : null;

    let nameValue =
      event.nativeEvent &&
        event.nativeEvent.target &&
        event.nativeEvent.target[index]
        ? event.nativeEvent.target[index].text
        : null;

    const dmdcUpdate = { ...this.state.dmdcInfo };
    let otherNameValue = this.state.otherName;

    const { id, value } = event.target;
    let validations = { ...this.state.validations };

    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      const validationResult = ruleRunner(
        value,
        id,
        name,
        ...validationFunctions
      );

      validations[id] = validationResult[id];
    }

    if (KEY == "dmdcInfo" && SUBKEY === "dmdc") {
      dmdcUpdate["id"] = value;
      dmdcUpdate["name"] = nameValue;
    } else if (KEY == "dmdc-others" && SUBKEY === "dmdc") {
      otherNameValue = value;
      nameValue = "Other";
    }

    this.setState(
      {
        dmdcInfo: dmdcUpdate,
        otherName:
          this.state.dmdcInfo && !this.state.dmdcInfo.id ? "" : otherNameValue,
        isOtherName: value == "1584" ? true : false,
        isDMDCError: false,
        validations
      },
      () => {
        if (
          this.state.dmdcInfo &&
          this.state.dmdcInfo.id != "1584" &&
          this.state.dmdcInfo.name
        ) {
          this.saveDMDC();
        }
      }
    );
  }

  saveOtherDMDC() {
    if (this.state.dmdcInfo && this.state.dmdcInfo.id && this.state.otherName) {
      this.saveDMDC();
    }
  }

  eduCurrentFormChangeHandler(event, PARENT_KEY) {
    const { id, value } = event.target;
    const updateEdu = { ...this.state.eduInfo };
    let validations = { ...this.state.validations };
    let index =
      event.nativeEvent && event.nativeEvent.target
        ? event.nativeEvent.target.selectedIndex
        : null;

    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      const validationResult = ruleRunner(
        value,
        id,
        name,
        ...validationFunctions
      );

      validations[id] = validationResult[id];
    }

    updateEdu[PARENT_KEY][id] =
      id == "academicClass"
        ? {
          id: +value,
          value:
            event.nativeEvent.target && event.nativeEvent.target[index]
              ? event.nativeEvent.target[index].text
              : null
        }
        : value;

    if (id == "academicClass") {
      this.getFilterRules(value);
    }

    this.setState({ eduInfo: updateEdu, validations, isFormValid: false });
  }

  currentFormRulesHandler(event, KEY) {
    let index = event.nativeEvent.target.selectedIndex;
    const { id, value } = event.target;
    let validations = { ...this.state.validations };

    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      const validationResult = ruleRunner(
        value,
        id,
        name,
        ...validationFunctions
      );

      validations[id] = validationResult[id];
    }

    const userRule = {};
    const updateUserRules = { ...this.state.dropDownRules };

    updateUserRules[KEY].ruleId = +value;

    this.setState({
      dropDownRules: updateUserRules,
      validations
    });
  }

  getDistrictList(key, value) {
    this.props.loadDistrictList({
      KEY: key,
      DATA: value
    });
  }

  onCheckBoxToggle(event) {
    const { id, value } = event.target;
    //toggle checkbox state
    const checkBoxState = !this.state.scholarshipCheckboxes[parseInt(id, 10)];

    const scholarshipCheckboxes = {
      ...this.state.scholarshipCheckboxes,
      [parseInt(id, 10)]: checkBoxState
    };

    let checkedCheckboxes = [...this.state.checkedCheckboxes];
    checkedCheckboxes = checkedCheckboxes.includes(parseInt(id, 10))
      ? checkedCheckboxes.filter(chkBox => chkBox != parseInt(id, 10))
      : checkedCheckboxes.concat(parseInt(id, 10));

    this.setState({ scholarshipCheckboxes, checkedCheckboxes });
  }

  getRuleId(ruleTypeId) {
    const updatedrules = this.state.PERSONAL_INFO.userRules;
    let ruleid = [];
    if (updatedrules.length > 0) {
      ruleid = updatedrules.filter(list => list.ruleTypeId == ruleTypeId);
    }
    return ruleid.length > 0 ? ruleid[0].ruleId : "";
  }

  checkStateHandler(key) {
    const currentState = this.state.PERSONAL_INFO;
    const dmdc = this.state.dmdcInfo;
    const dmdcOthers = this.state.otherName;
    const eduCurrentState = this.state.eduInfo;
    let rules = { ...this.state.dropDownRules };

    let address = {
      state: "userAddress",
      district: "userAddress",
      addressLine: "userAddress",
      pincode: "userAddress",
      city: "userAddress",
      country: "userAddress"
    };

    let educationSection = {
      stream: "userAcademicInfo",
      academicClass: "userAcademicInfo"
    };

    if (address[key] && currentState[address[key]]) {
      if (key === "state" || key === "district") {
        return currentState[address[key]][key]
          ? currentState[address[key]][key].id
          : null;
      } else {
        return currentState[address[key]][key];
      }
    } else if (
      educationSection[key] &&
      eduCurrentState[educationSection[key]][key] &&
      key == "academicClass"
    ) {
      return eduCurrentState[educationSection[key]][key]
        ? eduCurrentState[educationSection[key]][key].id
        : null;
    } else if (educationSection[key] && key == "stream") {
      return eduCurrentState[educationSection[key]][key]
        ? eduCurrentState[educationSection[key]][key]
        : null;
    } else if (rules[key]) {
      return rules[key].ruleId;
    } else if (key === "dmdc" && dmdc && dmdc["id"]) {
      return dmdc["id"];
    } else if (key === "dmDcOthers" && dmdcOthers) {
      return dmdcOthers;
    } else {
      return key == "mobile" && currentState[key]
        ? currentState[key].toString()
        : currentState[key];
    }
  }

  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "firstName":
        validationObject.name = "* First name";
        validationObject.validationFunctions = [required, lengthRange(3, 40)];
        return validationObject;
      case "lastName":
        validationObject.name = "* Last name";
        validationObject.validationFunctions = [lengthRange(3, 40)];
        return validationObject;
      case "pincode":
        validationObject.name = "* Pin code";
        validationObject.validationFunctions = [required, isNumeric, isPINCODE];
        return validationObject;
      case "email":
        validationObject.name = "* Email ";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;
      case "mobile":
        validationObject.name = "* Mobile number ";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;
      case "aadharCard":
        validationObject.name = "* Aadhaar number  ";
        validationObject.validationFunctions = [
          required,
          isAadhar,
          minLength(12)
        ];
        return validationObject;
      case "addressLine":
        validationObject.name = "* Address ";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "district":
        validationObject.name = "* District ";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "city":
        validationObject.name = "* City ";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "state":
        validationObject.name = "* State ";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "gender":
        validationObject.name = "* Gender ";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "religion":
        validationObject.name = "* Religion ";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "quota":
        validationObject.name = "* Category ";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "familyIncome":
        validationObject.name = "* Annual family income ";
        validationObject.validationFunctions = [required, isNumeric];
        return validationObject;
      case "academicClass":
        validationObject.name = "* Present class/Degree";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "stream":
        validationObject.name = "* Stream";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "dmdc":
        validationObject.name = "* DMDC";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "dmDcOthers":
        validationObject.name = "* DMDC other";
        validationObject.validationFunctions = [required];
        return validationObject;
    }
  }
  /************ end - validation part - get message and required validation******************* */

  /************ start - validation part - check form submit validation******************* */
  checkFormValidations() {
    let validations = { ...this.state.validations };

    const { userAcademicInfo } = this.state.eduInfo;
    let isFormValid = true;

    if (hideSubject.indexOf(+userAcademicInfo.academicClass.id) > -1) {
      delete validations["stream"];
    }

    if (!this.state.otherName && this.state.dmdcInfo.id != "1584") {
      delete validations["dmDcOthers"];
    }

    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);

      let validationResult = ruleRunner(
        this.checkStateHandler(key),
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }
  /************ start - validation part - check form submit validation******************* */

  saveDMDC() {
    const data = { ...this.state.dmdcInfo };
    if (data && data.id == "1584") {
      data.name = this.state.otherName;
      data.otherName = true;
    } else {
      data.otherName = false;
    }

    this.props.saveDMDC({
      userId: gblFunc.getStoreUserDetails()["userId"],
      data
    });
  }

  render() {
    let personalFormContent = null;
    const userDetails = gblFunc.getStoreUserDetails();
    const { dmdc } = this.state;
    const filterRules = this.props.filterRules ? this.props.filterRules : [];
    const { addressLine, city, country, district, pincode, state, type } = this
      .state.PERSONAL_INFO.userAddress
      ? this.state.PERSONAL_INFO.userAddress
      : {
        addressLine: "",
        city: "",
        country: "",
        district: { id: null, value: null },
        pincode: "",
        state: { id: null, value: null },
        type: 1
      };

    if (
      userDetails &&
      (userDetails.cscId == "undefined" ||
        userDetails.cscId == "null" ||
        userDetails.cscId == "") &&
      userDetails.vleUser == "false"
    ) {
      return <Redirect to="/" />;
    }

    const {
      religion,
      gender,
      disabled,
      quota,
      foreign
    } = this.state.dropDownRules;

    const { rulesList, userList } = this.props;
    const { userAcademicInfo } = this.state.eduInfo;
    // if (this.props.isError) {
    //   return <ServerError errorMessage={this.props.errorMessage} />;
    // }

    if (rulesList && userList) {
      personalFormContent = (
        <article className="ctrl-wrapper widget-border">
          <h2 className="my-profile" ref="personalProfile">
            My Profile
          </h2>
          <p>
            <strong>
              We are collecting this information only to find scholarships
              matching with your profile
            </strong>
          </p>
          <article className="col-md-6">
            <article className="ctrl-wrapper">
              <article className="form-group">
                <label htmlFor="firstName">First Name*</label>
                <input
                  type="text"
                  maxLength="80"
                  minLength="1"
                  className="form-control"
                  id="firstName"
                  value={
                    this.state.PERSONAL_INFO.firstName
                      ? this.state.PERSONAL_INFO.firstName
                      : ""
                  }
                  onChange={event =>
                    this.currentFormChangeHandler(event, "firstName")
                  }
                  name="firstName"
                  placeholder="Enter your first name"
                />
                {this.state.validations.firstName ? (
                  <span className="error animated bounce">
                    {this.state.validations.firstName}
                  </span>
                ) : null}
              </article>
              <article className="form-group">
                <label htmlFor="email">Email*</label>
                <input
                  type="email"
                  name="email"
                  className="form-control"
                  id="email"
                  value={
                    this.state.PERSONAL_INFO.email
                      ? this.state.PERSONAL_INFO.email
                      : ""
                  }
                  onChange={event =>
                    this.currentFormChangeHandler(event, "email")
                  }
                  placeholder="Enter your email"
                  readOnly
                />
                {this.state.validations.email ? (
                  <span className="error animated bounce">
                    {this.state.validations.email}
                  </span>
                ) : null}
              </article>

              {/* <article className="form-group">
                <label htmlFor="dob">Date of Birth</label>
                <article className="input-group date">
                  <DatePicker
                    showYearDropdown
                    scrollableYearDropdown
                    yearDropdownItemNumber={40}
                    minDate={moment().subtract(100, "years")}
                    maxDate={moment().add(1, "years")}
                    name="dob"
                    id="dob"
                    className="form-control"
                    autoCapitalize="off"
                    selected={
                      this.state.PERSONAL_INFO.dob
                        ? moment(
                            moment(this.state.PERSONAL_INFO.dob).format(
                              "DD-MM-YYYY"
                            ),
                            "DD-MM-YYYY"
                          )
                        : null
                    }
                    onChange={event =>
                      this.currentFormChangeHandler(event, "dob")
                    }
                    dateFormat="DD-MM-YYYY"
                  />
                  <label htmlFor="dob" className="input-group-addon">
                    <i className="fa fa-calendar" aria-hidden="true" />
                  </label>
                </article>
              </article> */}
              <article className="form-group">
                <label htmlFor="state">State*</label>
                <select
                  className="form-control"
                  id="state"
                  value={state ? (state.id ? state.id : "") : ""}
                  onChange={event =>
                    this.currentFormChangeHandler(event, "state", "userAddress")
                  }
                >
                  <option value="">--Select State--</option>
                  {this.props.rulesList["state"]
                    .filter(fList => fList["id"] !== 447)
                    .map(list => {
                      return (
                        <option
                          key={list.id}
                          name={list.rulevalue}
                          value={list.id}
                        >
                          {list.rulevalue}
                        </option>
                      );
                    })}
                </select>
                {this.state.validations.state ? (
                  <span className="error animated bounce">
                    {this.state.validations.state}
                  </span>
                ) : null}
              </article>
            </article>
          </article>
          <article className="col-md-6">
            <article className="ctrl-wrapper">
              <article className="form-group">
                <label htmlFor="lastName">Last Name</label>
                <input
                  type="text"
                  maxLength="40"
                  minLength="3"
                  className="form-control"
                  name="lastName"
                  id="lastName"
                  value={
                    this.state.PERSONAL_INFO.lastName
                      ? this.state.PERSONAL_INFO.lastName
                      : ""
                  }
                  onChange={event =>
                    this.currentFormChangeHandler(event, "lastName")
                  }
                  placeholder="Enter your last name"
                />
                {this.state.validations.lastName ? (
                  <span className="error animated bounce">
                    {this.state.validations.lastName}
                  </span>
                ) : null}
              </article>
              <article className="form-group">
                <label htmlFor="mnumber">Mobile Number*</label>
                <input
                  maxLength="10"
                  minLength="10"
                  type="text"
                  name="mobile"
                  placeholder="Enter your mobile"
                  className="form-control"
                  value={
                    this.state.PERSONAL_INFO.mobile
                      ? this.state.PERSONAL_INFO.mobile
                      : ""
                  }
                  onChange={event =>
                    this.currentFormChangeHandler(event, "mobile")
                  }
                  id="mobile"
                />
                {this.state.validations.mobile ? (
                  <span className="error animated bounce">
                    {this.state.validations.mobile}
                  </span>
                ) : null}
              </article>
              {/* <article className="form-group">
                <label htmlFor="gender">Gender*</label>
                <select
                  name="gender"
                  className="form-control"
                  id="gender"
                  onChange={event =>
                    this.currentFormRulesHandler(event, "gender")
                  }
                  value={gender.ruleId ? gender.ruleId : ""}
                >
                  <option value="">--Gender--</option>
                  {this.props.rulesList["gender"]
                    .filter(gList => gList["id"] !== 229)
                    .map(list => {
                      return (
                        <option key={list.id} value={list.id}>
                          {list.rulevalue}
                        </option>
                      );
                    })}
                </select>
                {this.state.validations.gender ? (
                  <span className="error animated bounce">
                    {this.state.validations.gender}
                  </span>
                ) : null}
              </article> */}

              <article className="form-group">
                <label htmlFor="district">District*</label>
                <select
                  name="district"
                  className="form-control"
                  id="district"
                  value={district.id ? district.id : ""}
                  onChange={event =>
                    this.currentFormChangeHandler(
                      event,
                      "district",
                      "userAddress"
                    )
                  }
                >
                  <option value="">--District--</option>
                  {this.props.district.map(list => {
                    return (
                      <option key={list.id} value={list.id}>
                        {list.districtName}
                      </option>
                    );
                  })}
                </select>
                {this.state.validations.district ? (
                  <span className="error animated bounce">
                    {this.state.validations.district}
                  </span>
                ) : null}
              </article>
              <article className="form-group">
                <label htmlFor="anumber">City*</label>
                <input
                  type="text"
                  name="city"
                  className="form-control"
                  id="city"
                  placeholder="Enter your city"
                  onChange={event =>
                    this.currentFormChangeHandler(event, "city", "userAddress")
                  }
                  value={city}
                />
                {this.state.validations.city ? (
                  <span className="error animated bounce">
                    {this.state.validations.city}
                  </span>
                ) : null}
              </article>
            </article>
          </article>
          <article className="row">
            <p className="msgNotification">
              If you have changed your state or district than please click on
              Save/Next button after saving DM/DC details.
            </p>
          </article>
          {/* DM/DC controls */}
          <article className="row">
            <article className="ctrl-wrapper">
              <article className="col-md-6">
                <article className="form-group">
                  <label htmlFor="dmdc">
                    DM / DC*{" "}
                    {/* {this.state.dmdcInfo && this.state.dmdcInfo.id ? (
                    <i className="fa fa-check-circle" aria-hidden="true" />
                  ) : null} */}
                  </label>

                  <select
                    name="dmdc"
                    className="form-control"
                    id="dmdc"
                    value={
                      this.state.dmdcInfo && this.state.dmdcInfo.id
                        ? this.state.dmdcInfo.id
                        : ""
                    }
                    onChange={event =>
                      this.dmdcHandler(event, "dmdcInfo", "dmdc")
                    }
                  >
                    <option value="">--DM / DC--</option>
                    {dmdc &&
                      dmdc.map(list => {
                        return (
                          <option key={list.id} value={list.id}>
                            {list.name}
                          </option>
                        );
                      })}
                  </select>
                  {this.state.validations.dmdc ? (
                    <span className="error animated bounce">
                      {this.state.validations.dmdc}
                    </span>
                  ) : null}
                </article>
              </article>
              {this.state.dmdcInfo && this.state.dmdcInfo.id == "1584" ? (
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="dmDcOthers">DM / DC (others)*</label>
                    <input
                      type="text"
                      name="dmdc-others"
                      className="form-control"
                      id="dmDcOthers"
                      value={this.state.otherName ? this.state.otherName : ""}
                      onChange={event =>
                        this.dmdcHandler(event, "dmdc-others", "dmdc")
                      }
                      placeholder="DM / DC"
                    />
                    {this.state.validations.dmDcOthers ? (
                      <span className="error animated bounce">
                        {this.state.validations.dmDcOthers}
                      </span>
                    ) : null}
                    <article className="row">
                      <article className="btn-ctrl">
                        <button
                          value="Save DM/DC others"
                          type="button"
                          onClick={this.saveOtherDMDC}
                        >
                          Save DM/DC others
                        </button>
                      </article>
                    </article>
                  </article>
                </article>
              ) : null}

              {/* {this.props.postedDMDC ? (
                <article className="form-group">
                  <i className="fa fa-check-circle" aria-hidden="true" />
                </article>
              ) : null}
              {this.state.isDMDCError ? (
                <article className="form-group">
                  <i className="fa fa-times-circle" aria-hidden="true" />
                </article>
              ) : null} */}
            </article>
          </article>
        </article>
      );
    }
    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <form
          name="profileForm"
          autoCapitalize="off"
          onSubmit={event => this.onSubmitHandler(event)}
        >
          {personalFormContent}
          <article className="errorContainer">
            {this.props.isUpdateError ? (
              <span className="error animated bounce">
                {this.props.updateError}
              </span>
            ) : null}
          </article>
          <article className="col-md-12 width100">
            <article className="row">
              <article className="btn-ctrl">
                <button type="submit">Save</button>
              </article>
            </article>
          </article>
        </form>
      </section>
    );
  }
}

export default CscPersonalInfo;
