import React, { Component, Fragment } from "react";
import UpdateMobEmailOtp from "./updateMobEmailOtp";
import { Redirect, Link } from "react-router-dom";
import gblFunc from "../../../../globals/globalFunctions";
import {
	FETCH_OTP_MOBILE_CHANGE_REQUESTED,
	FETCH_OTP_MOBILE_CHANGE_SUCCEEDED,
	FETCH_OTP_MOBILE_CHANGE_FAILED,

	FETCH_OTP_EMAIL_CHANGE_REQUESTED,
	FETCH_OTP_EMAIL_CHANGE_SUCCEEDED,
	FETCH_OTP_EMAIL_CHANGE_FAILED,

	SUBMIT_OTP_MOB_EMAIL_CHANGE_REQUESTED,
	SUBMIT_OTP_MOB_EMAIL_CHANGE_SUCCEEDED,
	SUBMIT_OTP_MOB_EMAIL_CHANGE_FAILED
} from "../../../../constants/commonActions";

import AlertMessage from "../../../common/components/alertMsg";
import {
	required,
	isEmail,
	minLength,
	maxLength,
	isMobileNumber
} from "../../../../validation/rules";
import { ruleRunner } from "../../../../validation/ruleRunner";

class updateMobEmail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value: null,
			pathTitle: null,
			oldMobile: null,
			oldEmail: null,
			newMobile: null,
			newEmail: null,
			validations: {
				newMobile: "",
				newEmail: "",
			},
			emailToken: "",
			mobileToken: "",
			showPopup: false,
			status: false,
			msg: "",
			isSubmit: false,
			updatedEmail: "",
			updatedMobile: ""
		};
		this.onMobileUpdate = this.onMobileUpdate.bind(this);
		this.onEmailUpdate = this.onEmailUpdate.bind(this);
		this.verifyOtp = this.verifyOtp.bind(this);
		this.handleFieldChange = this.handleFieldChange.bind(this);
		this.close = this.close.bind(this);
		//this.closeFailed = this.closeFailed.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.checkFormValidations = this.checkFormValidations.bind(this);
		this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
		this.submitEmailValidations = this.submitEmailValidations.bind(this);
		this.submitMobileValidations = this.submitMobileValidations.bind(this);
	}

	onEmailUpdate(e) {
		e.preventDefault();

		let obj = {
			toEmail: this.state.newEmail
		};

		if (this.submitEmailValidations()) {
			this.state.newEmail && this.props.otpEmailChange(obj);
		}
	}

	onMobileUpdate(e) {
		e.preventDefault();
		let obj = {
			userId: this.state.userId,
			mobile: this.state.newMobile
		};
		if (this.submitMobileValidations()) {
			this.props.otpMobileChange(obj);
		}
	}

	close() {
		this.setState({
			showPopup: false
		})
		if (typeof window !== 'undefined' && this.state.isSubmit == true) {
			location.href = "/myProfile/PersonalInfo"
		}

	}

	handleChange(event) {
		const { id, value } = event.target;
		const { name, validationFunctions } = this.getValidationRulesObject(id);
		const validationResult = ruleRunner(
			value,
			id,
			name,
			...validationFunctions
		);
		let validations = { ...this.state.validations };
		validations[id] = validationResult[id];

		this.setState({
			[id]: value,
			validations
		});
		// if (10 > value.length || value.length > 10) {
		// 	this.setState({
		// 		validations: {
		// 			newMobile: "Mobile number should be 10 digit only*."
		// 		}
		// 	});
		// }
		// if (this.state.emailMobileSearch)
		// 	this.setState({
		// 		mobileError: ""
		// 	});
	}

	submitEmailValidations() {
		let validations = { ...this.state.validations };
		let isFormValid = true;
		let { name, validationFunctions } = this.getValidationRulesObject('newEmail');
		let validationResult = ruleRunner(
			this.state.newEmail,
			'newEmail',
			name,
			...validationFunctions
		);

		validations['newEmail'] = validationResult['newEmail'];

		if (validationResult['newEmail'] !== null) {
			isFormValid = false;
		} else {
			isFormValid = true;
		}

		this.setState({
			validations,
			isFormValid
		});

		return isFormValid;
	}

	submitMobileValidations() {
		let validations = { ...this.state.validations };
		let isFormValid = true;
		let { name, validationFunctions } = this.getValidationRulesObject('newMobile');
		let validationResult = ruleRunner(
			this.state.newMobile,
			'newMobile',
			name,
			...validationFunctions
		);

		validations['newMobile'] = validationResult['newMobile'];

		if (validationResult['newMobile'] !== null) {
			isFormValid = false;
		} else {
			isFormValid = true;
		}

		this.setState({
			validations,
			isFormValid
		});

		return isFormValid;
	}

	checkFormValidations() {
		let validations = { ...this.state.validations };
		let isFormValid = true;
		for (let key in validations) {
			let { name, validationFunctions } = this.getValidationRulesObject(key);
			let validationResult = ruleRunner(
				this.state[key],
				key,
				name,
				...validationFunctions
			);

			validations[key] = validationResult[key];

			if (validationResult[key] !== null) {
				isFormValid = false;
			}
		}

		this.setState({
			validations,
			isFormValid
		});

		return isFormValid;
	}

	getValidationRulesObject(fieldID) {
		let validationObject = {};
		switch (fieldID) {
			case "newMobile":
				validationObject.name = "*Mobile";
				validationObject.validationFunctions = [required, isMobileNumber, /* minLength[10], maxLength[10] */];
				return validationObject;

			case "newEmail":
				validationObject.name = "*Email";
				validationObject.validationFunctions = [required, isEmail];
				return validationObject;
		}
	}

	handleFieldChange(otp) {
		this.setState({
			otpEntered: otp
		});
	}

	verifyOtp(e) {
		let obj2 = {
			email: this.state.newEmail,
			mobile: this.state.newMobile,
			otp: this.state.otpEntered,
			token:
				(!!this.state.mobileToken && this.state.mobileToken) ||
				(!!this.state.emailToken && this.state.emailToken),
			userId: this.state.userId
		};
		this.props.submitOtpMobEmailFun(obj2);
		typeof window !== 'undefined' && localStorage.setItem(
			"email", this.state.newEmail !== null ? this.state.newEmail : this.state.oldEmail
		);

		typeof window !== 'undefined' && localStorage.setItem(
			"mobile", this.state.newMobile !== null ? this.state.newMobile : this.state.oldMobile
		);
	}

	componentDidMount() {
		/* let pathTitle = this.props.match.params.title
			? this.props.match.params.title
			: "";
		this.setState({
			pathTitle: pathTitle
		}); */

		let userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
		let oldMobileLocal = parseInt(gblFunc.getStoreUserDetails()["mobile"]);
		let oldEmailLocal = (gblFunc.getStoreUserDetails()["email"]);

		let oldEmail = typeof window !== 'undefined' && localStorage.getItem('email');
		let oldMobile = typeof window !== 'undefined' && localStorage.getItem('mobile');

		this.setState({
			oldEmail: oldEmail || oldEmailLocal,
			oldMobile: oldMobile || oldMobileLocal,
			userId: userId
		});
	}

	componentWillReceiveProps(nextProps) {
		const { type } = nextProps;
		switch (type) {
			case FETCH_OTP_EMAIL_CHANGE_SUCCEEDED:
				this.setState({
					emailToken: !!nextProps.otpEmail && nextProps.otpEmail.token
				});
				break;
			case FETCH_OTP_EMAIL_CHANGE_FAILED:
				this.setState({
					validations: {
						newEmail: !!nextProps.error && nextProps.error.message
					}
				});
				break;
			case FETCH_OTP_MOBILE_CHANGE_SUCCEEDED:
				this.setState({
					mobileToken: !!nextProps.otpMobile && nextProps.otpMobile.token
				});
			case FETCH_OTP_MOBILE_CHANGE_FAILED:
				this.setState({
					validations: {
						newMobile: !!nextProps.error && nextProps.error.message
					}
				});
				break;
			case SUBMIT_OTP_MOB_EMAIL_CHANGE_SUCCEEDED:
				if (!!nextProps.verifyOtp && nextProps.verifyOtp.verifyKey === 'emailOtp') {
					this.setState({
						updatedEmail: !!nextProps.verifyOtp && nextProps.verifyOtp.email,
						updatedToken:
							!!nextProps.verifyOtp && nextProps.verifyOtp.access_token,
						showPopup: true,
						status: true,
						isSubmit: true,
						msg: "Your Email ID has been updated"
					});
					typeof window !== 'undefined' && localStorage.setItem(
						"accessToken",
						nextProps.verifyOtp && nextProps.verifyOtp.access_token
					);
					typeof window !== 'undefined' && localStorage.setItem(
						"refreshToken",
						nextProps.verifyOtp && nextProps.verifyOtp.refresh_token
					);
				}
				if (!!nextProps.verifyOtp && nextProps.verifyOtp.verifyKey === 'mobileOtp') {
					this.setState({
						updatedMobile:
							!!nextProps.verifyOtp && nextProps.verifyOtp.mobile,
						updatedToken:
							!!nextProps.verifyOtp && nextProps.verifyOtp.access_token,
						showPopup: true,
						status: true,
						isSubmit: true,
						msg: "Your Mobile No. has been updated"
					});
					typeof window !== 'undefined' && localStorage.setItem(
						"accessToken",
						nextProps.verifyOtp.access_token
					);
					typeof window !== 'undefined' && localStorage.setItem(
						"refreshToken",
						nextProps.verifyOtp.refresh_token
					);
				}
			case SUBMIT_OTP_MOB_EMAIL_CHANGE_FAILED:
				if (nextProps.error != this.props.error) {
					let errorCode1 = nextProps.error && nextProps.error.errorCode;
					if (errorCode1 == 701) {
						this.setState({
							status: false,
							showPopup: true,
							msg: "Invalid OTP",
						});
					}
				}
		}
	}
	render() {
		const { showPopup, status, msg, isSubmit, updatedEmail, updatedMobile } = this.state;
		const isAuthenticated =
			gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
		if (!isAuthenticated) {
			return <Redirect to="/" />;
		} else {
			return (
				<Fragment>
					<AlertMessage
						close={this.close}
						isShow={showPopup}
						status={status}
						msg={msg}
						isSubmit={isSubmit}
					/>

					{!this.props.otpEmail && !this.props.otpMobile && (
						<Fragment>
							{typeof window !== 'undefined' && location.pathname.includes('UpdateEmail') && (
								<EmailUpdate
									{...this.state}
									onEmailUpdate={this.onEmailUpdate}
									handleChange={this.handleChange}
								/>
							)}
							{typeof window !== 'undefined' && location.pathname.includes('UpdateMobile') && (
								<MobileUpdate
									{...this.state}
									onMobileUpdate={this.onMobileUpdate}
									mobileError={this.state.mobileError}
									handleChange={this.handleChange}
								/>

							)}
						</Fragment>
					)}
					{(this.props.otpEmail && this.props.otpEmail.token || this.props.otpMobile && this.props.otpMobile.token) && (
						<UpdateMobEmailOtp
							newMobile={this.state.newMobile}
							newEmail={this.state.newEmail}
							handleFieldChange={this.handleFieldChange}
							verifyOtp={this.verifyOtp}
						/>
					)}
				</Fragment>
			);
		}
	}
}

export default updateMobEmail;

const EmailUpdate = props => {
	return (
		<section className="row updateEmailMobile">
			<article className="innerWrapper">
				<h3>Update Email ID</h3>
				<section className="form-wrapper">
					<form name="updateEmail" onSubmit={e => props.onEmailUpdate(e)}>
						<article className="form-group">
							<label>Old Email ID</label>
							<input
								type="text"
								value={!!props.oldEmail && props.oldEmail}
								name="oldEmail"
								className="form-control"
								placeholder="Email"
								disabled
							/>
						</article>
						<article className="form-group">
							<label>New Email ID</label>
							<input
								type="email"
								name="newEmail"
								id="newEmail"
								value={props.newEmail}
								className="form-control"
								placeholder="Enter New Email"
								onChange={e => props.handleChange(e)}
							//onClick={this.goToNextStep}
							/>
						</article>
						{!!props.validations.newEmail && (
							<p className="errorMsg">{props.validations.newEmail}</p>
						)}
						<article className="btn-ctrl">
							<button type="submit">Update</button>
							<Link
								onClick={() => [
									props.gtmEventHandler(["Cancel", `PersonalInfo`]),
									props.pushTobackState('PersonalInfo')]
								}
								to="/myProfile/PersonalInfo"
								className="Cancel">Cancel</Link>
						</article>
					</form>
				</section>
			</article>
		</section>
	);
};

const MobileUpdate = props => {
	return (
		<section className="row updateEmailMobile">
			<article className="innerWrapper">
				<h3>Update Mobile No</h3>
				<section className="form-wrapper">
					<form name="mobileUpdate" onSubmit={props.onMobileUpdate}>
						<article className="form-group">
							<label>Old Mobile Number</label>
							<input
								type="text"
								value={!!props.oldMobile && props.oldMobile}
								name="oldMobile"
								className="form-control"
								//placeholder="Email"
								disabled
							/>
						</article>
						<article className="form-group">
							<label>New Mobile Number</label>
							<input
								type="text"
								value={props.newEmail}
								maxLength="10"
								//minLength="10"
								className="form-control"
								name="newMobile"
								id="newMobile"
								placeholder="Enter new Mobile Number"
								onChange={e => props.handleChange(e)}
							/>
						</article>
						{!!props.validations.newMobile && (
							<p className="errorMsg">{props.validations.newMobile}</p>
						)}
						<article className="btn-ctrl">
							<button type="submit">Update</button>
							<Link
								onClick={() => [
									props.gtmEventHandler(["Cancel", `PersonalInfo`]),
									props.pushTobackState('PersonalInfo')]
								}
								to="/myProfile/PersonalInfo"
								className="Cancel">Cancel</Link>
						</article>
					</form>
				</section>
			</article>
		</section>
	);
};
