import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import { breadCrumObj } from "../../../../constants/breadCrum";
import { Helmet } from "react-helmet";
import BreadCrum from "../../../components/bread-crum/breadCrum";
import { dashboardFormContent } from "../importForm";
import SideNav from "../../../components/side-nav/sideNav";
import { dependantRequests } from "../../../../constants/constants";
import DashboardUI from "../../../components/dashboard/dashboardUI";
import ProfileImg from "../extras/profile_img";
import gblFunc from "../../../../globals/globalFunctions";
import { imageFileExtension } from "../../../../constants/constants";

import {
	UPLOAD_USER_PIC_SUCCESS,
	UPLOAD_DMDC_FAIL,
	UPLOAD_DMDC_SUCCESS,
	FETCH_USER_DISBURSAL_SUCCEEDED,
	FETCH_USER_DISBURSAL_FAILED
} from "../../actions";
import {
	maxFileSize,
	hasValidImageExtenstion
} from "../../../../validation/rules";
import { ruleRunner } from "../../../../validation/ruleRunner";
import {
	FETCH_USER_RULES_SUCCESS,
	FETCH_USERLIST_SUCCEEDED
} from "../../../../constants/commonActions";
import { LOG_USER_OUT } from "../../../login/actions";
import AlertMessage from "../../../common/components/alertMsg";

class ProfileInfo extends Component {
	constructor(props) {
		super(props);

		this.currentFormChangeHandler = this.currentFormChangeHandler.bind(this);
		this.mapUserApiToState = this.mapUserApiToState.bind(this);
		this.imagefileChangedHandler = this.imagefileChangedHandler.bind(this);
		this.clearSelectedFile = this.clearSelectedFile.bind(this);
		this.goToNextPage = this.goToNextPage.bind(this);
		this.updateUser = this.updateUser.bind(this);
		this.fileValidationHandler = this.fileValidationHandler.bind(this);
		this.fullNameHandler = this.fullNameHandler.bind(this);
		this.redirectToMembership = this.redirectToMembership.bind(this);
		this.onCloseHandler = this.onCloseHandler.bind(this);
		this.bookSlotPopUpHandler = this.bookSlotPopUpHandler.bind(this);
		this.urlPush = this.urlPush.bind(this);
		this.tabToggleHandler = this.tabToggleHandler.bind(this);
		this.state = {
			form: { tabName: "PersonalInfo" },
			selectedFile: null,
			isShow: false,
			onStatusCheck: null,
			dmdcMsg: "",
			src: null,
			isTabToggle: false,
			tab: "profile",
			validations: {
				docFile: null,
				fileExtention: null
			},
			updateUserConfig: {
				fullName: "",
				pic: null,
				userId: null,
				percentage: null
			},
			disbursalData: "",
			isSubmit: false,
			documentIssue: ""
		};
	}
	urlPush(urlPar) {
		this.props.history.push(
			`/myProfile/${urlPar}`
		);
	}

	bookSlotPopUpHandler(name, userId, scholarshipId, bsid) {
		const flushValidation = {
			docFile: null,
			//comments: null,
			scheduleDate: null,
			scheduleTime: null,
			mobile: null,
			prefferedLanguages: null
		};
		if (
			["Book Slot", "Slot Booked"].indexOf(name) === -1 &&
			typeof window !== "undefined"
		) {
			const redirectURL = `/application/${bsid}/instruction`;
			this.props.history.push(redirectURL);
		}

		this.getBookSlotInfo(userId, scholarshipId);
		if (typeof window !== "undefined") {
			if (window.innerWidth <= 853) {
				document.body.scrollTop = 0;
				document.documentElement.scrollTop = 0;
			}
		}
		this.setState({
			showBookSlot: true,
			userId,
			bsid,
			scholarshipId,
			validations: flushValidation
		});
	}

	/* tabFormHandler(event, tabName) {
		event.preventDefault();
		this.props.history.push("/myprofile");
		this.props.history.push(`/myProfile/${event.target.id}`);
		let updateForm = { ...this.state, ...this.state.form };

		updateForm.form["tabName"] = event.target.id;
		this.setState(updateForm);
	} */

	goToNextPage() {
		this.setState({
			form: { tabName: "MatchedScholarships" }
		});
		window.location = '/myprofile/MatchedScholarships';
	}

	currentFormChangeHandler(event, KEY, formName) {
		if (dependantRequests.includes(KEY)) {
			//TODO: Call dependant key request , pick from props in container
			this.props.makeDependantRequest(KEY, event.target.value);
		}
		// this.state.formInputs[this.state.form.tabName][KEY].value =
		//   event.target.value;

		let formIsValid = true;
		// for (let KEY in this.state.formInputs[this.state.form.tabName]) {
		//   formIsValid =
		//     this.state.formInputs[this.state.form.tabName][KEY].valid &&
		//     formIsValid;
		// }
		let currentForm = Object.assign({}, this.state.currentForm);
		currentForm[KEY] = event.target.value;
		this.setState({
			//input_form: this.state.formInputs[this.state.form.tabName],
			currentForm,
			formIsValid: formIsValid
		});
	}

	mapUserApiToState(USER, USER_RULE) {
		let setCurrentForm = { ...this.state.currentForm };

		for (let user_key in USER) {
			setCurrentForm[user_key] = USER[user_key];
		}
		for (let rule_key in USER_RULE) {
			setCurrentForm[rule_key.toUpperCase()] = USER_RULE[rule_key][0].ID;
		}
		this.setState({
			currentForm: setCurrentForm
		});
	}

	imagefileChangedHandler(e) {
		let checkValidate = null;
		e.preventDefault();
		const { id } = e.target;

		let files;
		if (e.dataTransfer) {
			files = e.dataTransfer.files;
		} else if (e.target) {
			files = e.target.files;
		}
		const reader = new FileReader();
		reader.onload = () => {
			this.setState({ src: reader.result });
		};
		reader.readAsDataURL(files[0]);

		// Check Size In MB.
		const sizeInMb = Math.round(files[0].size / (1024 * 1024));
		const fileExtension = files[0].name.slice(files[0].name.lastIndexOf("."));

		if (fileExtension) {
			checkValidate = this.fileValidationHandler(
				fileExtension,
				id,
				"File",
				hasValidImageExtenstion(imageFileExtension),
				files
			);
			if (checkValidate) return;
		}

		if (sizeInMb > 2) {
			checkValidate = this.fileValidationHandler(
				sizeInMb,
				id,
				"Uploaded file",
				maxFileSize(2),
				files
			);

			if (checkValidate) return;
		}
	}

	redirectToMembership() {
		this.props.history.push("/premium-membership");
	}

	fileValidationHandler(fileConfig, id, name, validationConfig, files) {
		let validationResult = null;
		validationResult = ruleRunner(fileConfig, id, name, validationConfig);

		const validations = { ...this.state.validations };
		validations[id] = validationResult[id];

		this.setState({
			selectedFile: validations[id] ? null : files[0],
			validations
		});
		return validations[id] ? true : false;
	}

	clearSelectedFile() {
		this.setState({
			selectedFile: null
		});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.userRulesData && nextProps.userRulesData.documentIssue !== null) {
			this.setState({
				documentIssue: nextProps.userRulesData && nextProps.userRulesData.documentIssue
			})
		}
		const { type } = nextProps;

		switch (type) {
			case UPLOAD_USER_PIC_SUCCESS:
				typeof window !== "undefined"
					? localStorage.setItem("pic", nextProps.uploadPicData.location)
					: "";
				this.setState({
					updateUserConfig: {
						...this.state.updateUserConfig,
						pic: nextProps.uploadPicData.location
					}
				});
				break;
			case FETCH_USERLIST_SUCCEEDED:
				this.props.fetchUserRules({
					userid: this.state.updateUserConfig.userId
				});
				break;
			case FETCH_USER_RULES_SUCCESS:
				const first_name =
					nextProps.userRulesData && nextProps.userRulesData.firstName
						? nextProps.userRulesData.firstName
						: "Buddy";
				const last_name =
					nextProps.userRulesData &&
						nextProps.userRulesData.lastName &&
						first_name != "Buddy"
						? nextProps.userRulesData.lastName
						: "";

				const membershipExpiry =
					nextProps.userRulesData && nextProps.userRulesData.membershipExpiry
						? nextProps.userRulesData.membershipExpiry
						: null;

				typeof window !== "undefined"
					? localStorage.setItem(
						"percentage",
						nextProps.userRulesData &&
							nextProps.userRulesData.profilePercentage
							? nextProps.userRulesData.profilePercentage
							: 0
					)
					: "";
				typeof window !== "undefined"
					? localStorage.setItem("firstName", first_name)
					: "";
				typeof window !== "undefined"
					? localStorage.setItem("lastName", last_name)
					: "";

				typeof window !== "undefined"
					? localStorage.setItem("membershipExpiry", membershipExpiry)
					: "";

				this.setState(
					{
						updateUserConfig: {
							...this.state.updateUserConfig,
							percentage:
								nextProps.userRulesData &&
									nextProps.userRulesData.profilePercentage
									? nextProps.userRulesData.profilePercentage
									: 0
						}
					},
					() => this.fullNameHandler()
				);
				break;
			case UPLOAD_DMDC_FAIL:
				this.setState({
					onStatusCheck: false,
					dmdcMsg: "Something went wrong, please try again!",
					isShow: true
				});
				break;
			case UPLOAD_DMDC_SUCCESS:
				this.setState({
					onStatusCheck: true,
					dmdcMsg: "Saved Successfully!",
					isShow: true
				});
				break;
			case FETCH_USER_DISBURSAL_SUCCEEDED:
				this.setState({ disbursalData: nextProps.disbursalData });
				const disbursalNo = getNestedObjKey(nextProps.disbursalData, [
					"disbursalNumber"
				]);
				const schlarshipName = getNestedObjKey(nextProps.disbursalData, [
					"title"
				]);
				const message = getNestedObjKey(nextProps.disbursalData, [
					"message"
				]);
				const providerName = getNestedObjKey(nextProps.disbursalData, [
					"providerName"
				]);
				const schlarshipId = getNestedObjKey(nextProps.disbursalData, [
					"scholarshipId"
				]);
				const disbursalId = getNestedObjKey(nextProps.disbursalData, [
					"disbursalId"
				]);
				if (disbursalNo) {
					this.props.history.push({
						pathname: `/scholar-profile/${encodeURIComponent(providerName)}/${schlarshipId}/${encodeURIComponent(schlarshipName)}/${disbursalNo}/${disbursalId}`,
						state: {
							message: message
						}
					});
				}
				break;
			case FETCH_USER_DISBURSAL_FAILED:
				this.setState({
					onStatusCheck: false,
					dmdcMsg: "Server Error!",
					isShow: true
				});
				break;
		}
	}

	componentDidMount() {
		const { pathname } = this.props.location;
		const { params } = this.props.match;
		const {
			userId,
			pic,
			firstName,
			lastName,
			percentage
		} = gblFunc.getStoreUserDetails();


		let array1 = ['MatchedScholarships', 'QuestionsAndAnswers', 'ApplicationStatus', 'AwardeesScholarships', 'MyFavorites', 'PersonalInfo', 'DocumentIssues', 'UpdateEmail', 'UpdateMobile']
		this.props.fetchUserDisbursalDetail();

		if (pathname === "/csc/myprofile") {
			this.setState({ form: { tabName: "CscPersonalInfo" } });
		} else {
			if (params.dashboard == "dashboard") {
				let updateTab = { ...this.state, ...this.state.form };

				updateTab.form["tabName"] = "Education";
				this.setState(updateTab);

			}

			if (!!params && !!params.dashboard && array1.includes(params.dashboard)) {
				//debugger
				let updateTab = { ...this.state, ...this.state.form };

				updateTab.form["tabName"] = params.dashboard;
				this.setState(updateTab);
			}
			else {
				let updateTab = { ...this.state, ...this.state.form };

				updateTab.form["tabName"] = "PersonalInfo";
				this.setState(updateTab);
				this.props.history.push('/myProfile')
			}
		}
		if (typeof window == 'undefined') {
			window.removeEventListener("resize", this.updateWindowDimensions.bind(this));
		}

		this.setState({
			updateUserConfig: {
				fullName: `${
					firstName != ("null" || "undefined" || "") ? firstName : "Buddy"
					} ${lastName != ("null" || "undefined" || "") ? lastName : ""}`,
				pic: pic,
				userId,
				percentage
			}
		})
		this.props.fetchUserRules({
			userid: userId
		});
		if (typeof window == 'undefined') {
			window.removeEventListener("resize", this.updateWindowDimensions.bind(this));
		}
	}

	fullNameHandler() {
		const {
			firstName,
			lastName,
			pic,
			userId,
			percentage
		} = gblFunc.getStoreUserDetails();

		this.setState({
			updateUserConfig: {
				fullName: `${firstName} ${lastName}`,
				pic,
				userId,
				percentage
			}
		});
	}

	updateUser({ firstName, lastName }) {
		this.setState(
			{
				updateUserConfig: {
					...this.state.updateUserConfig,
					fullName: `${firstName} ${lastName}`
				}
			},
			() =>
				this.props.fetchUserRules({
					userid: this.state.updateUserConfig.userId
				})
		);
	}
	onCloseHandler() {
		this.setState({
			isShow: false
		});
	}
	tabToggleHandler() {
		this.setState({
			isTabToggle: !this.state.isTabToggle
		})
	}

	updateWindowDimensions() {
		if (typeof window == 'undefined' && window.screen.width <= 991) {
			this.setState({
				isTabToggle: true
			});
		} else {
			this.setState({
				isTabToggle: false
			});
		}
	}

	render() {
		const district = this.props.district ? this.props.district : [];
		const { redirectToHomePage, disbursalData, isTabToggle, documentIssue } = this.state;
		const userUpdated = this.state.updateUserConfig;
		const userDetails = gblFunc.getStoreUserDetails();
		const isAuthenticated =
			gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
		if (!isAuthenticated) {
			return <Redirect to="/" />;
		}

		const isDocumentIssue = documentIssue;

		return (
			<section>
				<AlertMessage
					isShow={this.state.isShow}
					status={this.state.onStatusCheck}
					msg={this.state.dmdcMsg}
					close={this.onCloseHandler}
				/>
				{this.state.selectedFile ? (
					<DashboardUI
						src={this.state.src}
						selectedFile={this.state.selectedFile}
						uploadPic={this.props.uploadPic}
						clearSelectedFile={this.clearSelectedFile}
						type={this.props.type}
						userId={userUpdated.userId}
					/>
				) : null}
				<section className="dashboard newTheme">
					<Helmet> </Helmet>
					{/* <BreadCrum
            classes={breadCrumObj["dashboard"]["bgImage"]}
            listOfBreadCrum={breadCrumObj["dashboard"]["breadCrum"]}
            title={breadCrumObj["dashboard"]["title"]}
          /> */}
					<section className="conatiner-fluid">
						<article className="container dashboard-container">
							<article className="row">
								<article className="dashboard-nav-bg">
									<article className="dashboard-nav-in">
										{/* SIDE NAV TABS*/}
										<article className="col-md-3">
											<ProfileImg
												imagefileChangedHandler={this.imagefileChangedHandler}
												userUpdated={userUpdated}
												validations={this.state.validations}
												userDetails={userDetails}
												redirectToMembership={this.redirectToMembership}
												profileTitleMsg="Hurry don't miss matching scholarships, complete your profile today"
											/>
											<SideNav
												currentPage="dashboard"
												tab={this.state.tab}
												sideNavTabName={this.state.form.tabName}
												tabToggleHandler={this.tabToggleHandler}
												isTabToggle={isTabToggle}
												isDocumentIssue={isDocumentIssue}
												urlPush={this.urlPush}
											/>
										</article>
										<article className="col-md-9">
											<article className="tab-border">
												{userUpdated.userId ? (
													<FormContent
														{...this.props}
														tabName={this.state.form.tabName}
														close={this.onCloseHandler}
														district={district}
														userId={userUpdated.userId}
														goToNextPage={this.goToNextPage}
														updateUser={this.updateUser}
														fetchUserRules={this.props.fetchUserRules}
														fetchUserDisbursalId={this.props.fetchUserDisbursalId}
														fetchScholarDocs={this.props.fetchScholarDocs}
														fetchScholarOrgnlDocs={this.props.fetchScholarOrgnlDocs}
														scholarOrgnlDocs={this.props.scholarOrgnlDocs}
														disbursalCycleId={this.props.disbursalId}
														scholarsDocs={this.props.scholarsDocs}
														uploadScholarDocs={this.props.uploadScholarDocs}
														disbursalData={disbursalData}
														bookSlotPopUpHandler={this.bookSlotPopUpHandler}
													/>

												) : null}
											</article>
										</article>
									</article>
								</article>
							</article>
						</article>
					</section>
				</section>
			</section>
		);
	}
}

const FormContent = props => {
	let navTopTab = props.tab;

	let FormOnTab = dashboardFormContent[props.tabName];

	return <FormOnTab {...props} />;
};

export default ProfileInfo;
