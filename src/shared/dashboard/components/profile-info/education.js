import React, { Component } from "react";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import {
  FETCH_EDUCATION_INFO_SUCCESS,
  UPDATE_EDUCATION_INFO_SUCCESS,
  UPDATE_EDUCATION_INFO_FAILURE,
  FETCH_USER_DOCUMENTS_SUCCESS,
  FETCH_USER_DOCUMENTS_FAILURE
} from "../../actions";
import {
  GET_EDUCATION_COURSES_SUCCEDED,
  GET_EDUCATION_COURSES_FAILED,
  FETCH_RULE_SUCCEDED,
  FETCH_RULE_FAILED,
  FETCH_DISTRICT_SUCCEDED,
  FETCH_DISTRICT_FAILED
} from "../../../scholar/scholarAction";
import AlertMessagePopup from "../../../common/components/alertMsg";
import { getSemester, getAcadmicYear } from "../../../../constants/constants";
import ScholarEducation from "./scholar-education";
import Loader from "../../../common/components/loader";

const eduValidationSchema = Yup.object().shape({
  degree: Yup.string()
    .required("Required")
    .nullable(),
  subject: Yup.string()
    .nullable()
    .test("match", "Required", function(subject) {
      if (parseInt(this.parent.degree) < 12) {
        return true;
      } else if (subject) {
        return true;
      }
    }),
  // category: Yup.string()
  //   .nullable()
  //   .test("match", "Required", function(category) {
  //     if (parseInt(this.parent.degree) < 21) {
  //       return true;
  //     } else if (category) {
  //       return true;
  //     }
  //   }),
  year: Yup.string()
    .nullable()
    .test("match", "Required", function(year) {
      if (parseInt(this.parent.degree) <= 17) {
        return true;
      } else if (year) {
        return true;
      }
    }),
  instituteName: Yup.string()
    .nullable()
    .required("Required"),
  passingYear: Yup.string()
    .nullable()
    .test("match", "Required", function(passingYear) {
      if (parseInt(this.parent.degree) <= 17) {
        return true;
      } else if (passingYear) {
        return true;
      }
    }),
  city: Yup.string()
    .nullable()
    .test("match", "Required", function(city) {
      if (parseInt(this.parent.degree) <= 17) {
        return true;
      } else if (city) {
        return true;
      }
    }),
  district: Yup.string()
    .nullable()
    .test("match", "Required", function(district) {
      if (parseInt(this.parent.degree) <= 17) {
        return true;
      } else if (district) {
        return true;
      }
    }),
  address: Yup.string()
    .nullable()
    .test("match", "Required", function(address) {
      if (parseInt(this.parent.degree) <= 17) {
        return true;
      } else if (address) {
        return true;
      }
    }),
  pincode: Yup.string()
    .nullable()
    .max(6, "Invalid PIN")
    .min(6, "Invalid PIN")
    .test("match", "Required", function(pincode) {
      if (parseInt(this.parent.degree) <= 17) {
        return true;
      } else if (pincode) {
        return true;
      }
    }),
  state: Yup.string()
    .nullable()
    .test("match", "Required", function(state) {
      if (parseInt(this.parent.degree) < 17) {
        return true;
      } else if (state) {
        return true;
      }
    }),
  country: Yup.string()
    .nullable()
    .test("match", "Required", function(country) {
      if (parseInt(this.parent.degree) <= 17) {
        return true;
      } else if (country) {
        return true;
      }
    }),
  marksObtained: Yup.string()
    .nullable()
    .test("match", "Required", function(marksObtained) {
      if (this.parent.radioClassStatus == "presentClass") {
        return true;
      } else if (marksObtained) {
        return true;
      }
    }),
  totalMarks: Yup.string()
    .nullable()
    .test("match", "Required", function(totalMarks) {
      if (this.parent.radioClassStatus == "presentClass") {
        return true;
      } else if (totalMarks) {
        return true;
      }
    })
    .test(
      "match",
      "Total marks should be greater or equal than marks obtained",
      function(totalMarks) {
        if (this.parent.radioClassStatus == "presentClass") {
          return true;
        }
        var result =
          parseFloat(totalMarks) >= parseFloat(this.parent.marksObtained);
        return result;
      }
    ),
  radioClassStatus: Yup.string()
    .required("Required")
    .nullable()
});
class Education extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eduData: [],
      allEduData: [],
      isEdit: false,
      marksObtained: "",
      totalMarks: "",
      subject: "",
      degree: "",
      showAlertPopup: false,
      alertMsg: "",
      isError: "",
      educationData: [],
      radioClassStatus: "",
      year: "",
      isAdd: false,
      eduInfo: "",
      userId: "",
      defaultAcadmic: {
        academicClass: { id: null, value: "" },
        passingYear: "",
        instituteName: "",
        stream: "",
        district: "",
        address: "",
        state: "",
        pincode: "",
        currentAcademicYear: ""
      },
      isScholar: false
    };
    this.provideFormInitialValues = this.provideFormInitialValues.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClass = this.handleClass.bind(this);
    this.cancelEditing = this.cancelEditing.bind(this);
    this.closeAlertPopup = this.closeAlertPopup.bind(this);
    this.goNext = this.goNext.bind(this);
  }
  componentDidMount() {
    const userId = localStorage.getItem("userId");
    this.setState({ userId });
    this.props.fetchUserDocuments({ userid: userId });
    const data = {
      filter: ["state", "class", "subject", "course", "year"]
    };
    this.props.fetchRule(data);
  }

  componentWillReceiveProps(nextProps) {
    const {
      educationData,
      scholarType,
      courseData,
      ruleData,
      districtData
    } = nextProps;
    switch (nextProps.type) {
      case FETCH_USER_DOCUMENTS_SUCCESS:
        let scholarDocs = getNestedObjKey(nextProps.userDocumentsData, [
          "scholarDocument"
        ]);
        if (scholarDocs.length) {
          this.setState({ isScholar: true, scholarDocs: scholarDocs });
        } else {
          this.setState({ isScholar: false });
          this.props.fetchEducationInfoDetails({ userId: this.state.userId });
        }
        break;
      case FETCH_EDUCATION_INFO_SUCCESS:
        let allEduData = [];
        educationData.map(x => {
          let r = x.userAcademicInfo;
          let m = x.userInstituteInfo;
          allEduData.push({ ...r, ...m });
          return x;
        });
        this.setState({ eduInfo: educationData, allEduData: allEduData });
        break;
      case UPDATE_EDUCATION_INFO_SUCCESS:
        this.setState(
          {
            isEdit: false,
            showAlertPopup: true,
            alertMsg: "Data submitted successfully",
            isError: false
          },
          () => {
            this.props.fetchEducationInfoDetails({ userId: this.state.userId });
          }
        );
        break;
      case UPDATE_EDUCATION_INFO_FAILURE:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
    }
    switch (scholarType) {
      case GET_EDUCATION_COURSES_SUCCEDED:
        this.setState({ courses: courseData });
        break;
      case GET_EDUCATION_COURSES_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case FETCH_RULE_SUCCEDED:
        this.setState({ ruleFilter: ruleData });
        break;
      case FETCH_RULE_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case FETCH_DISTRICT_SUCCEDED:
        this.setState({ districtData: districtData });
        break;
      case FETCH_DISTRICT_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
    }
  }

  closeAlertPopup() {
    this.setState({ showAlertPopup: false, alertMsg: "" });
  }
  provideFormInitialValues() {
    let initialValues = {
      degree: "",
      subject: "",
      marksObtained: "",
      totalMarks: "",
      year: "",
      radioClassStatus: "",
      state: "",
      district: "",
      city: "",
      address: "",
      pincode: "",
      country: "",
      instituteName: ""
    };
    return initialValues;
  }
  handleChange(e, cb) {
    if (e.target.name == "degree") {
      cb("degree", e.target.value);
      cb("subject", "");
      cb("year", "");
      if (e.target.value >= 12) {
        this.props.fetchCourses({
          courseId: e.target.value
        });
      }
    } else if (e.target.name == "state") {
      this.props.fetchDistrict({ stateId: e.target.value });
      cb("state", e.target.value);
    } else if (e.target.name == "degree" && e.target.value < 12) {
      cb("subject", "");
    } else if (e.target.name == "degree" && e.target.value < 20) {
      this.setState({ year: "" });
      cb("year", "");
    } else if (
      e.target.name == "radioClassStatus" &&
      e.target.value == "presentClass"
    ) {
      cb("radioClassStatus", "presentClass");
      cb("previousClass", 0);
      cb("marksObtained", "");
      cb("totalMarks", "");
    } else if (
      e.target.name == "radioClassStatus" &&
      e.target.value == "previousClass"
    ) {
      cb("radioClassStatus", "previousClass");
      cb("presentClass", 0);
    }
    // this.setState({ [e.target.name]: e.target.value });
    cb([e.target.name], e.target.value);
  }

  handleClass(cb, data) {
    this.cancelEditing(cb);
    //this.props.editEdu();
    if (data == "add") {
      let currentYear = new Date().getFullYear();
      const { allEduData } = this.state;
      this.setState({ isEdit: true, ...this.state.defaultAcadmic });
      cb("radioClassStatus", "presentClass");
      const preYearData = allEduData.filter(x => x.presentClass == 1);
      preYearData &&
        preYearData.length &&
        preYearData.map(x => {
          if (x.passingYear > currentYear) {
            this.setState({ ...x });
            if (x.state) {
              this.props.fetchDistrict({ stateId: x.state });
            }
            cb("degree", getNestedObjKey(x, ["academicClass", "id"]));
            cb("subject", getNestedObjKey(x, ["stream"]));
            cb("instituteName", getNestedObjKey(x, ["instituteName"]));
            cb("year", getNestedObjKey(x, ["currentAcademicYear"]));
            cb("state", getNestedObjKey(x, ["state"]));
            cb("address", getNestedObjKey(x, ["address"]));
            cb("city", getNestedObjKey(x, ["city"]));
            cb("district", getNestedObjKey(x, ["district"]));
            cb("country", getNestedObjKey(x, ["country"]));
            cb("passingYear", getNestedObjKey(x, ["passingYear"]));
            cb("pincode", getNestedObjKey(x, ["pincode"]));
            cb("marksObtained", getNestedObjKey(x, ["marksObtained"]));
            cb("totalMarks", getNestedObjKey(x, ["totalMarks"]));
            if (getNestedObjKey(x, ["presentClass"]) == 1) {
              cb(
                "radioClassStatus",
                getNestedObjKey(x, ["presentClass"]) == 1 ? "presentClass" : ""
              );
            } else if (getNestedObjKey(x, ["previousClass"]) == 1) {
              cb(
                "radioClassStatus",
                getNestedObjKey(x, ["previousClass"]) == 1
                  ? "previousClass"
                  : ""
              );
            }
          }
        });
    } else {
      if (data.state) {
        this.props.fetchDistrict({ stateId: data.state });
      }
      this.setState({ isEdit: true, ...data });
      this.props.fetchCourses({
        courseId: getNestedObjKey(data, ["academicClass", "id"])
      });
      cb("id", getNestedObjKey(data, ["id"]));
      cb("degree", getNestedObjKey(data, ["academicClass", "id"]));
      cb("subject", getNestedObjKey(data, ["stream"]));
      cb("instituteName", getNestedObjKey(data, ["instituteName"]));
      cb("year", getNestedObjKey(data, ["currentAcademicYear"]));
      cb("state", getNestedObjKey(data, ["state"]));
      cb("address", getNestedObjKey(data, ["address"]));
      cb("city", getNestedObjKey(data, ["city"]));
      cb("district", getNestedObjKey(data, ["district"]));
      cb("country", getNestedObjKey(data, ["country"]));
      cb("passingYear", getNestedObjKey(data, ["passingYear"]));
      cb("pincode", getNestedObjKey(data, ["pincode"]));
      cb("marksObtained", getNestedObjKey(data, ["marksObtained"]));
      cb("totalMarks", getNestedObjKey(data, ["totalMarks"]));
      if (getNestedObjKey(data, ["presentClass"]) == 1) {
        cb(
          "radioClassStatus",
          getNestedObjKey(data, ["presentClass"]) == 1 ? "presentClass" : ""
        );
      } else if (getNestedObjKey(data, ["previousClass"]) == 1) {
        cb(
          "radioClassStatus",
          getNestedObjKey(data, ["previousClass"]) == 1 ? "previousClass" : ""
        );
      }
    }
  }
  cancelEditing(cb) {
    cb("id", "");
    cb("degree", "");
    cb("subject", "");
    cb("year", "");
    cb("marksObtained", "");
    cb("totalMarks", "");
    cb("radioClassStatus", "");
    cb("passingYear", "");
    cb("district", "");
    cb("address", "");
    cb("state", "");
    cb("instituteName", "");
    cb("city", "");
    cb("country", "");
    cb("pincode", "");
    this.setState({ isEdit: false });
  }
  goNext() {
    this.props.goToNextPage("Documents");
  }

  render() {
    const {
      eduData,
      isEdit,
      isAdd,
      allEduData,
      courses,
      districtData,
      ruleFilter,
      isScholar,
      scholarDocs
    } = this.state;
    const presClass = allEduData.filter(x => x.presentClass == 1);
    const prevClass = allEduData.filter(x => x.presentClass != 1);
    return (
      <section>
        {!isScholar ? (
          <article>
            <Loader isLoader={this.props.showLoader} />
            {this.state.showAlertPopup && (
              <AlertMessagePopup
                msg={this.state.alertMsg}
                isShow={this.state.showAlertPopup}
                status={!this.state.isError}
                close={this.closeAlertPopup}
              />
            )}
            <Formik
              initialValues={this.provideFormInitialValues()}
              onSubmit={values => {
                const { eduInfo } = this.state;
                let finalData = [];
                let updatedInstu;
                let updatedAcadmic;
                let newEduInfo = JSON.parse(JSON.stringify(eduInfo));
                let data = {
                  academicClass: {
                    id: parseInt(values.degree)
                  },
                  id: parseInt(values.id),
                  stream: parseInt(values.subject),
                  currentAcademicYear: parseInt(values.year),
                  marksObtained: parseFloat(values.marksObtained),
                  totalMarks: parseInt(values.totalMarks),
                  presentClass:
                    values.radioClassStatus == "presentClass" ? 1 : 0,
                  previousClass:
                    values.radioClassStatus == "previousClass" ? 1 : 0,
                  state: values.state,
                  district: values.district,
                  city: values.city,
                  address: values.address,
                  pincode: values.pincode,
                  country: values.country,
                  instituteName: values.instituteName,
                  passingYear: values.passingYear
                };
                if (data.id) {
                  const updateEdu = newEduInfo.find(
                    x => x.userInstituteInfo.id == data.id
                  );
                  const { userAcademicInfo, userInstituteInfo } = updateEdu;
                  updatedAcadmic = {
                    ...userAcademicInfo,
                    academicClass: data.academicClass,
                    stream: data.stream,
                    currentAcademicYear: data.currentAcademicYear,
                    marksObtained: data.marksObtained,
                    totalMarks: data.totalMarks,
                    presentClass: data.presentClass,
                    previousClass: data.previousClass,
                    passingYear: data.passingYear
                  };
                  updatedInstu = {
                    ...userInstituteInfo,
                    state: data.state,
                    district: data.district,
                    city: data.city,
                    address: data.address,
                    pincode: data.pincode,
                    country: data.country,
                    instituteName: data.instituteName
                  };
                } else {
                  updatedAcadmic = {
                    academicClass: data.academicClass,
                    stream: data.stream,
                    currentAcademicYear: data.currentAcademicYear,
                    marksObtained: data.marksObtained,
                    totalMarks: data.totalMarks,
                    presentClass: data.presentClass,
                    previousClass: data.previousClass,
                    passingYear: data.passingYear,
                    markingType: "1"
                  };
                  updatedInstu = {
                    state: data.state,
                    district: data.district,
                    city: data.city,
                    address: data.address,
                    pincode: data.pincode,
                    country: data.country,
                    instituteName: data.instituteName
                  };
                }

                finalData.push({
                  userAcademicInfo: updatedAcadmic,
                  userInstituteInfo: updatedInstu
                });
                this.props.updateEducationInfo(finalData);
              }}
              enableReinitialize
              validationSchema={eduValidationSchema}
            >
              {({ errors, touched, values, setFieldValue }) => {
                return (
                  <Form name="EduForm">
                    <article className="bankDeatils">
                      <h3>Education Details</h3>
                      <article className="boxBorder">
                        <article className="ctrl-wrapper">
                          <article className="col-xs-12 table-responsive">
                            <table className="table table-sm">
                              <thead>
                                <tr>
                                  <th scope="col">Class/Degree</th>
                                  <th scope="col">Course/Stream</th>
                                  <th scope="col">Year/Semester</th>
                                  <th scope="col">Institute Name</th>
                                  <th scope="col">Passing Year</th>
                                  <th scope="col">City</th>
                                  <th scope="col">District</th>
                                  <th scope="col">Address</th>
                                  <th scope="col">PIN Code</th>
                                  <th scope="col">State</th>
                                  <th scope="col">Country</th>
                                  <th scope="col">Obtained marks</th>
                                  <th scope="col">Total marks</th>
                                  <th scope="col">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                {presClass.length ? (
                                  <tr>
                                    <th colSpan="4">Present Class</th>
                                  </tr>
                                ) : (
                                  ""
                                )}
                                {presClass.length
                                  ? presClass.map(x => (
                                      <tr>
                                        <td scope="row">
                                          {x.academicClassName}
                                        </td>
                                        <td>{x.streamName}</td>
                                        <td>
                                          {x.academicClass.id > 16
                                            ? x.currentAcademicYear
                                            : "N/A"}
                                        </td>
                                        <td>{x.instituteName}</td>
                                        <td>
                                          {x.academicClass.id > 16
                                            ? x.passingYear
                                            : "N/A"}
                                        </td>
                                        <td>{x.city}</td>
                                        <td>{x.district}</td>
                                        <td>{x.address}</td>
                                        <td>{x.pincode}</td>
                                        <td>{x.state}</td>
                                        <td>{x.country}</td>
                                        <td>{x.marksObtained}</td>
                                        <td>{x.totalMarks}</td>
                                        <td>
                                          <button
                                            type="button"
                                            onClick={() =>
                                              this.handleClass(setFieldValue, x)
                                            }
                                            className="btn btnHeight"
                                          >
                                            Edit
                                          </button>
                                        </td>
                                      </tr>
                                    ))
                                  : ""}
                              </tbody>
                              <tbody>
                                {prevClass.length ? (
                                  <tr>
                                    <th colSpan="4">Previous Class</th>
                                  </tr>
                                ) : (
                                  ""
                                )}
                                {prevClass.length
                                  ? prevClass.map(x => (
                                      <tr>
                                        <td scope="row">
                                          {x.academicClassName}
                                        </td>
                                        <td>{x.streamName}</td>
                                        <td>
                                          {x.academicClass.id > 16
                                            ? x.currentAcademicYear
                                            : "N/A"}
                                        </td>
                                        <td>{x.instituteName}</td>
                                        <td>
                                          {" "}
                                          {x.academicClass.id > 16
                                            ? x.passingYear
                                            : "N/A"}
                                        </td>
                                        <td>{x.city}</td>
                                        <td>{x.district}</td>
                                        <td>{x.address}</td>
                                        <td>{x.pincode}</td>
                                        <td>{x.state}</td>
                                        <td>{x.country}</td>
                                        <td>{x.marksObtained}</td>
                                        <td>{x.totalMarks}</td>
                                        <td>
                                          <button
                                            type="button"
                                            onClick={() =>
                                              this.handleClass(setFieldValue, x)
                                            }
                                            className="btn btnHeight"
                                          >
                                            Edit
                                          </button>
                                        </td>
                                      </tr>
                                    ))
                                  : ""}
                              </tbody>
                            </table>
                          </article>
                        </article>
                      </article>
                      <article className="col-md-12">
                        <button
                          type="button"
                          onClick={() => this.handleClass(setFieldValue, "add")}
                          className="btn btn-yellow marginTop2 pull-right"
                          disabled={isAdd}
                        >
                          <i className="fa fa-plus" /> &nbsp; Add class
                        </button>
                      </article>
                      {isEdit ? (
                        <article className="boxBorder">
                          <article className="ctrl-wrapper">
                            <article className="col-md-12">
                              <article className="col-md-12 radiobtn">
                                <label class="radio-inline">
                                  <input
                                    type="radio"
                                    checked={
                                      values.radioClassStatus == "presentClass"
                                        ? true
                                        : false
                                    }
                                    value="presentClass"
                                    name="radioClassStatus"
                                    onChange={e =>
                                      this.handleChange(e, setFieldValue)
                                    }
                                  />Present class
                                </label>
                                <label class="radio-inline">
                                  <input
                                    type="radio"
                                    checked={
                                      values.radioClassStatus == "previousClass"
                                        ? true
                                        : false
                                    }
                                    name="radioClassStatus"
                                    value="previousClass"
                                    onChange={e =>
                                      this.handleChange(e, setFieldValue)
                                    }
                                  />Previous class
                                </label>
                                {errors.radioClassStatus &&
                                  touched.radioClassStatus && (
                                    <span className="error">
                                      {errors.radioClassStatus}
                                    </span>
                                  )}
                              </article>
                              <article className="form-group margintop20">
                                <label for="passed">Class/Degree *</label>
                                <Field
                                  className="form-control"
                                  component="select"
                                  name="degree"
                                  onChange={e =>
                                    this.handleChange(e, setFieldValue)
                                  }
                                  disabled={getNestedObjKey(this.state, [
                                    "academicClass",
                                    "id"
                                  ])}
                                >
                                  <option value="">--Class/Degree--</option>
                                  {ruleFilter.class && ruleFilter.class.length
                                    ? ruleFilter.class.map(c => {
                                        if (c.id != 20) {
                                          return (
                                            <option key={c.id} value={c.id}>
                                              {c.rulevalue}
                                            </option>
                                          );
                                        }
                                      })
                                    : ""}
                                </Field>
                                {errors.degree &&
                                  touched.degree && (
                                    <span className="error">
                                      {errors.degree}
                                    </span>
                                  )}
                              </article>
                              <article className="form-group margintop20">
                                <label for="marks">Institute Name *</label>
                                <Field
                                  type="text"
                                  className="form-control"
                                  placeholder="Institute Name"
                                  name="instituteName"
                                  disabled={this.state.instituteName}
                                />
                                {errors.instituteName &&
                                  touched.instituteName && (
                                    <span className="error">
                                      {errors.instituteName}
                                    </span>
                                  )}
                              </article>
                              {values.degree >= 12 ? (
                                <article className="form-group margintop20">
                                  <label for="course">
                                    Course/Subject/Stream *
                                  </label>
                                  <Field
                                    className="form-control"
                                    component="select"
                                    name="subject"
                                    onChange={e =>
                                      this.handleChange(e, setFieldValue)
                                    }
                                    disabled={this.state.stream}
                                  >
                                    <option value="">
                                      --Course/Subject/Stream--
                                    </option>
                                    {courses && courses.length
                                      ? courses.map(c => (
                                          <option key={c.id} value={c.id}>
                                            {c.rulevalue}
                                          </option>
                                        ))
                                      : ""}
                                  </Field>
                                  {errors.subject &&
                                    touched.subject && (
                                      <span className="error">
                                        {errors.subject}
                                      </span>
                                    )}
                                </article>
                              ) : (
                                ""
                              )}
                              {values.degree > 20 && (
                                <article>
                                  <article className="form-group margintop20">
                                    <label for="passed">Year *</label>
                                    <Field
                                      className="form-control"
                                      component="select"
                                      name="year"
                                      onChange={e =>
                                        this.handleChange(e, setFieldValue)
                                      }
                                    >
                                      <option value="">--Year--</option>
                                      {getAcadmicYear().map(y => (
                                        <option key={y.id} value={y.id}>
                                          {y.year}
                                        </option>
                                      ))}
                                    </Field>
                                    {errors.year &&
                                      touched.year && (
                                        <span className="error">
                                          {errors.year}
                                        </span>
                                      )}
                                  </article>
                                  <article className="form-group margintop20">
                                    <label for="marks">Passing Year *</label>
                                    <Field
                                      type="text"
                                      className="form-control"
                                      placeholder="Passing Year"
                                      name="passingYear"
                                      disabled={this.state.passingYear}
                                    />
                                    {errors.passingYear &&
                                      touched.passingYear && (
                                        <span className="error">
                                          {errors.passingYear}
                                        </span>
                                      )}
                                  </article>
                                  <article className="form-group margintop20">
                                    <label for="marks">City *</label>
                                    <Field
                                      type="text"
                                      className="form-control"
                                      placeholder="City"
                                      name="city"
                                      disabled={this.state.city}
                                    />
                                    {errors.city &&
                                      touched.city && (
                                        <span className="error">
                                          {errors.city}
                                        </span>
                                      )}
                                  </article>
                                  <article className="form-group margintop20">
                                    <label for="marks">State *</label>
                                    <Field
                                      className="form-control"
                                      component="select"
                                      name="state"
                                      onChange={e =>
                                        this.handleChange(e, setFieldValue)
                                      }
                                      disabled={this.state.state}
                                    >
                                      <option>--State--</option>
                                      {ruleFilter.state &&
                                      ruleFilter.state.length
                                        ? ruleFilter.state.map(s => (
                                            <option key={s.id} value={s.id}>
                                              {s.rulevalue}
                                            </option>
                                          ))
                                        : ""}
                                    </Field>
                                    {errors.state &&
                                      touched.state && (
                                        <span className="error">
                                          {errors.state}
                                        </span>
                                      )}
                                  </article>
                                  <article className="form-group margintop20">
                                    <label for="marks">District *</label>
                                    <Field
                                      className="form-control"
                                      component="select"
                                      name="district"
                                      onChange={e =>
                                        this.handleChange(e, setFieldValue)
                                      }
                                      disabled={this.state.district}
                                    >
                                      <option>--District--</option>
                                      {districtData && districtData.length
                                        ? districtData.map(d => (
                                            <option key={d.id} value={d.id}>
                                              {d.districtName}
                                            </option>
                                          ))
                                        : ""}
                                    </Field>
                                    {errors.district &&
                                      touched.district && (
                                        <span className="error">
                                          {errors.district}
                                        </span>
                                      )}
                                  </article>
                                  <article className="form-group margintop20">
                                    <label for="marks">Address *</label>
                                    <Field
                                      type="text"
                                      className="form-control"
                                      placeholder="Address"
                                      name="address"
                                      disabled={this.state.address}
                                    />
                                    {errors.address &&
                                      touched.address && (
                                        <span className="error">
                                          {errors.address}
                                        </span>
                                      )}
                                  </article>
                                  <article className="form-group margintop20">
                                    <label for="marks">Pin Code *</label>
                                    <Field
                                      type="number"
                                      className="form-control"
                                      placeholder="Pin Code"
                                      name="pincode"
                                      disabled={this.state.pincode}
                                    />
                                    {errors.pincode &&
                                      touched.pincode && (
                                        <span className="error">
                                          {errors.pincode}
                                        </span>
                                      )}
                                  </article>
                                  <article className="form-group margintop20">
                                    <label for="marks">Country *</label>
                                    <Field
                                      type="text"
                                      className="form-control"
                                      placeholder="Country"
                                      name="country"
                                      disabled={this.state.country}
                                    />
                                    {errors.country &&
                                      touched.country && (
                                        <span className="error">
                                          {errors.country}
                                        </span>
                                      )}
                                  </article>
                                </article>
                              )}
                              {values.radioClassStatus == "previousClass" ? (
                                <article>
                                  <article className="form-group margintop20">
                                    <label for="marks">Marks Obtained *</label>
                                    <Field
                                      type="number"
                                      className="form-control"
                                      placeholder="Marks Obtain"
                                      name="marksObtained"
                                    />
                                    {errors.marksObtained &&
                                      touched.marksObtained && (
                                        <span className="error">
                                          {errors.marksObtained}
                                        </span>
                                      )}
                                  </article>
                                  <article className="form-group margintop20">
                                    <label for="totalmarks">
                                      Total Marks *
                                    </label>
                                    <Field
                                      type="number"
                                      className="form-control"
                                      placeholder="Total Marks"
                                      name="totalMarks"
                                    />
                                    {errors.totalMarks &&
                                      touched.totalMarks && (
                                        <span className="error">
                                          {errors.totalMarks}
                                        </span>
                                      )}
                                  </article>
                                </article>
                              ) : (
                                ""
                              )}
                              <article className="col-md-12 pull-right">
                                <input
                                  className="btn pull-right"
                                  type="submit"
                                  value="Save"
                                />
                                <input
                                  className="btn pull-right"
                                  onClick={() =>
                                    this.cancelEditing(setFieldValue)
                                  }
                                  type="button"
                                  value="Cancel"
                                />
                              </article>
                            </article>
                          </article>
                        </article>
                      ) : null}
                    </article>
                  </Form>
                );
              }}
            </Formik>
            <article className="btn-ctrl">
              <button type="button" onClick={this.goNext}>
                Next
              </button>
            </article>
          </article>
        ) : (
          <ScholarEducation
            {...this.props}
            scholarDocs={scholarDocs}
            goNext={this.goNext}
          />
        )}
        <article className="col-md-12 width100">
          <article className="row" />
        </article>
      </section>
    );
  }
}

export default Education;
