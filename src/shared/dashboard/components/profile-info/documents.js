import React, { Component } from "react";
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import { allowedDocumentExtensions } from "../../../../constants/constants";
import { ruleRunner } from "../../../../validation/ruleRunner";
import {
	required,
	maxFileSize,
	hasValidExtension
} from "../../../../validation/rules";
import Loader from "../../../common/components/loader";
import {
	FETCH_DOCUMENT_TYPES_CATEGORY_SUCCESS,
	FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_SUCCESS,
	FETCH_USER_DOCUMENTS_SUCCESS,
	UPDATE_USER_DOCUMENTS_SUCCESS,
	FETCH_DOCUMENT_TYPES_SUCCESS,
	DELETE_USER_DOCUMENTS_SUCCESS,
	FETCH_USER_DISBURSALID_SUCCEEDED,
	FETCH_USER_DISBURSALID_FAILED,
	FETCH_SCHOLAR_DOCUMENT_SUCCEEDED,
	FETCH_SCHOLAR_DOCUMENT_FAILED,
	POST_UPLOAD_DOCUMENT_SUCCEEDED,
	POST_UPLOAD_DOCUMENT_FAILED,
	FETCH_SCHOLAR_ORGNL_DOCUMENT_SUCCEEDED,
	FETCH_SCHOLAR_ORGNL_DOCUMENT_FAILED,
	POST_UPLOAD_USER_DOCUMENT_SUCCEEDED,
	POST_UPLOAD_USER_DOCUMENT_FAILED
} from "../../actions";
import AlertMessage from "../../../common/components/alertMsg";

import { confirmationMessages } from "../../../../constants/constants";
import ConfirmMessagePopup from "../../../common/components/confirmMessagePopup";

class Documents extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isFormShown: false,
			isShow: false,
			onStatusCheck: null,
			dmdcMsg: "",
			docFile: null,
			docFileSize: 0,
			docFileName: "",
			documentType: null,
			documentTypeCategory: null,
			documentTypeCategoryOption: null,
			showCategoriesDropDown: false,
			currentUserDocTypes: [],
			errorMsgOnUpload: "Please select the document before uploading.",
			showCategoryOptionsDropDown: false,
			isFormValid: false,
			deleteSuccessCallBack: null,
			showConfirmationPopup: false,
			scholarDocs: [],
			scholarsDocs: [],
			orgnlDocs: [],
			schId: "",
			disId: "",
			disbursalCycleIds: null,
			validations: {
				docFile: null,
				documentTypeCategory: null,
				documentTypeCategoryOption: null,
				documentType: null
			}
		};
		this.showForm = this.showForm.bind(this);
		this.onFormFieldChange = this.onFormFieldChange.bind(this);
		this.onDocumentUpload = this.onDocumentUpload.bind(this);
		this.deleteUserDocuments = this.deleteUserDocuments.bind(this);
		this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
		this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
		this.hideConfirmationPopup = this.hideConfirmationPopup.bind(this);
		this.setData = this.setData.bind(this);
		this.onCloseHandler = this.onCloseHandler.bind(this);
	}

	componentDidMount() {
		this.props.fetchDocumentTypes();
	}

	showConfirmationPopup() {
		this.setState({
			showConfirmationPopup: true
		});
	}

	onCloseHandler() {
		this.setState({
			isShow: false
		});
	}

	setData(data) {
		if (data.disId) {
			this.setState({ disId: data.disId });
		} else if (data.schId) {
			this.setState({ schId: data.schId });
		}
	}

	deleteUserDocuments(id) {
		const deleteSuccessCallBack = () =>
			this.props.deleteUserDocuments({
				userid: this.props.userId,
				documentId: id
			});

		this.setState({
			showConfirmationPopup: true,
			deleteSuccessCallBack
		});
	}

	hideConfirmationPopup() {
		this.setState({
			showConfirmationPopup: false,
			deleteSuccessCallBack: null
		});
	}

	mapToApiParams() {
		let {
			documentType,
			documentTypeCategory,
			documentTypeCategoryOption
		} = this.state;
		const { docFile } = this.state;
		const nullIfFalsy = value => value || null;

		documentType = nullIfFalsy(documentType);
		documentTypeCategory = nullIfFalsy(documentTypeCategory);
		documentTypeCategoryOption = nullIfFalsy(documentTypeCategoryOption);

		return {
			documentType,
			documentTypeCategory,
			documentTypeCategoryOption,
			docFile
		};
	}

	onDocumentUpload() {
		const params = this.mapToApiParams();
		if (this.checkFormValidations()) {
			this.props.uploadUserDocs({
				params
			});
		}
	}

	componentWillReceiveProps(nextProps) {
		const { type, scholarOrgnlDocs } = nextProps;

		switch (type) {
			case FETCH_DOCUMENT_TYPES_SUCCESS:
				this.props.fetchUserDocuments({ userid: this.props.userId });
				break;

			case UPDATE_USER_DOCUMENTS_SUCCESS:
				this.setState(
					{
						isFormShown: false,
						docFile: null,
						docFileName: "",
						showCategoriesDropDown: false,
						showCategoryOptionsDropDown: false
					},
					() => this.props.fetchUserDocuments({ userid: this.props.userId })
				);

				break;
			case POST_UPLOAD_USER_DOCUMENT_SUCCEEDED:
				this.setState(
					{
						onStatusCheck: true,
						dmdcMsg: "Document uploaded successfully!",
						isShow: true,
						isFormShown: false
					},
					() => {
						this.props.fetchUserDocuments({ userid: this.props.userId });
					}
				);
				break;
			case POST_UPLOAD_USER_DOCUMENT_FAILED:
				this.setState({
					onStatusCheck: false,
					dmdcMsg: "Server Error!",
					isShow: true
				});
				break;

			case DELETE_USER_DOCUMENTS_SUCCESS:
				this.setState(
					{
						isFormShown: false,
						docFile: null,
						docFileName: "",
						deleteSuccessCallBack: null,
						showConfirmationPopup: false
					},
					() => this.props.fetchUserDocuments({ userid: this.props.userId })
				);

				break;

			case FETCH_USER_DOCUMENTS_SUCCESS:
				let currentUserDocTypes = [];
				let userDocs = getNestedObjKey(nextProps.userDocumentsData, [
					"userDocuments"
				]);
				let scholarDocs = getNestedObjKey(nextProps.userDocumentsData, [
					"scholarDocument"
				]);
				if (userDocs && userDocs.length) {
					currentUserDocTypes = userDocs.map(doc =>
						doc.documentType ? doc.documentType.id : null
					);
				} else if (scholarDocs && scholarDocs.length > 0) {
					this.setState({ scholarDocs: scholarDocs });
				}
				this.setState({
					currentUserDocTypes,
					userDocs,
					scholarDocs
				});
				break;

			case FETCH_DOCUMENT_TYPES_CATEGORY_SUCCESS:
				this.setState({
					showCategoriesDropDown: true
				});
				break;

			case FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_SUCCESS:
				this.setState({
					showCategoryOptionsDropDown: true
				});
				break;
			case FETCH_USER_DISBURSALID_SUCCEEDED:
				this.setState({ disbursalCycleIds: nextProps.disbursalCycleId });
				break;

			case FETCH_USER_DISBURSALID_FAILED:
				this.setState({
					onStatusCheck: false,
					dmdcMsg: "Server Error!",
					isShow: true
				});
				break;

			case FETCH_SCHOLAR_DOCUMENT_SUCCEEDED:
				this.setState({ scholarsDocs: nextProps.scholarsDocs });
				break;
			case FETCH_SCHOLAR_DOCUMENT_FAILED:
				this.setState({
					onStatusCheck: false,
					dmdcMsg: "Server Error!",
					isShow: true
				});
				break;
			case POST_UPLOAD_DOCUMENT_SUCCEEDED:
				this.setState(
					{
						onStatusCheck: true,
						dmdcMsg: "Document uploaded successfully!",
						isShow: true,
						isFormShown: false
					},
					() => {
						this.props.fetchScholarDocs({ disbursalCycleId: this.state.disId });
					}
				);
				break;
			case POST_UPLOAD_DOCUMENT_FAILED:
				this.setState({
					onStatusCheck: false,
					dmdcMsg: "Server Error!",
					isShow: true
				});
				break;
			case FETCH_SCHOLAR_ORGNL_DOCUMENT_SUCCEEDED:
				this.setState({ orgnlDocs: scholarOrgnlDocs });
				break;
			case FETCH_SCHOLAR_ORGNL_DOCUMENT_FAILED:
				this.setState({
					onStatusCheck: false,
					dmdcMsg: "Server Error!",
					isShow: true
				});
				break;
		}
	}

	showForm() {
		this.setState({
			isFormShown: true
		});
	}

	//Helper function to set state and apply callback
	setStateAndApplyCallback(state, callback, ...args) {
		this.setState(state, () => callback(...args));
	}

	getValidationRulesObject(fieldID) {
		let validationObject = {};

		switch (fieldID) {
			case "documentType":
				validationObject.name = "*Document type ";
				validationObject.validationFunctions = [required];
				return validationObject;

			case "documentTypeCategory":
				validationObject.name = "*Category of documents ";
				validationObject.validationFunctions = [required];
				return validationObject;

			case "documentTypeCategoryOption":
				validationObject.name = "*Category options ";
				validationObject.validationFunctions = [required];
				return validationObject;

			case "docFile":
				validationObject.name = "*File ";
				validationObject.validationFunctions = [required];
				return validationObject;
		}
	}

	checkFormValidations() {
		let validations = { ...this.state.validations };
		let isFormValid = true;

		//Take every key in validations present in state
		for (let key in validations) {
			//Get the required functions of the particular field
			let { name, validationFunctions } = this.getValidationRulesObject(key);
			let validationResult = {};

			//Special fields case which are hidden or shown
			if (
				key === "documentTypeCategory" ||
				key === "documentTypeCategoryOption" ||
				key === "docFile"
			) {
				switch (key) {
					case "documentTypeCategory":
						if (this.state.showCategoriesDropDown) {
							validationResult = ruleRunner(
								this.state[key],
								key,
								name,
								...validationFunctions
							);
							validations[key] = validationResult[key];
						} else {
							validations[key] = null;
							validationResult[key] = null;
						}
						break;

					case "documentTypeCategoryOption":
						if (this.state.showCategoryOptionsDropDown) {
							validationResult = ruleRunner(
								this.state[key],
								key,
								name,
								...validationFunctions
							);
							validations[key] = validationResult[key];
						} else {
							validations[key] = null;
							validationResult[key] = null;
						}
						break;

					case "docFile":
						const uploadedFile = this.state.docFile;
						if (uploadedFile) {
							const sizeInMb = Math.ceil(uploadedFile.size / (1024 * 1024));
							const fileExtension = uploadedFile.name.slice(
								uploadedFile.name.lastIndexOf(".")
							);

							validationResult = ruleRunner(
								fileExtension,
								key,
								"File",
								hasValidExtension(allowedDocumentExtensions)
							);

							if (validationResult[key] === null) {
								validationResult = ruleRunner(
									sizeInMb,
									key,
									"Uploaded file",
									maxFileSize(1)
								);
							}
						} else {
							//Case for document type.
							validationResult = ruleRunner(
								this.state[key],
								key,
								name,
								...validationFunctions
							);
						}
						validations[key] = validationResult[key];
				}
			} else {
				validationResult = ruleRunner(
					this.state[key],
					key,
					name,
					...validationFunctions
				);

				validations[key] = validationResult[key];
			}

			if (validationResult[key] !== null) {
				isFormValid = false;
			}
		}

		this.setState({
			validations,
			isFormValid
		});

		return isFormValid;
	}

	//FIXME: Make this code DRY
	onFormFieldChange(event) {
		const { id, value } = event.target;
		const { name, validationFunctions } = this.getValidationRulesObject(id);

		let parsedObject;
		let newState;
		let validationResult;
		let validations;

		switch (id) {
			case "documentType":
				parsedObject = JSON.parse(value);
				validationResult = ruleRunner(
					parsedObject.id,
					id,
					name,
					...validationFunctions
				);
				validations = { ...this.state.validations };
				validations[id] = validationResult[id];
				newState = { [id]: parsedObject.id, validations };
				if (parsedObject.yearSemesterStatus) {
					newState.showCategoriesDropDown = true;
					this.setStateAndApplyCallback(
						newState,
						this.props.fetchDocumentTypeCategories,
						{ documentTypeId: parsedObject.id }
					);
				} else {
					newState.showCategoriesDropDown = false;
					newState.showCategoryOptionsDropDown = false;
					validations.documentTypeCategory = null;
					validations.documentTypeCategoryOption = null;
					this.setState(newState);
				}
				break;

			case "documentTypeCategory":
				parsedObject = JSON.parse(value);

				validationResult = ruleRunner(
					parsedObject.id,
					id,
					name,
					...validationFunctions
				);
				validations = { ...this.state.validations };
				validations[id] = validationResult[id];

				newState = { [id]: parsedObject.id, validations };
				if (parsedObject.id) {
					newState.showCategoryOptionsDropDown = true;
					this.setStateAndApplyCallback(
						newState,
						this.props.fetchDocumentTypeCategoriesOptions,
						{ categoryId: parsedObject.id }
					);
				} else {
					newState.showCategoryOptionsDropDown = false;
					validations.documentTypeCategoryOption = null;
					this.setState(newState);
				}
				break;

			case "documentTypeCategoryOption":
				parsedObject = JSON.parse(value);

				validationResult = ruleRunner(
					parsedObject.id,
					id,
					name,
					...validationFunctions
				);
				validations = { ...this.state.validations };
				validations[id] = validationResult[id];
				newState = { [id]: parsedObject.id, validations };
				this.setState(newState);
				break;

			case "docFile":
				const uploadedFile = event.target.files[0];
				// let formData = new FormData();
				// formData.append("data", uploadedFile);
				//Convert to MB.

				const sizeInMb = Math.ceil(uploadedFile.size / (1024 * 1024));
				const fileExtension = uploadedFile.name.slice(
					uploadedFile.name.lastIndexOf(".")
				);

				validationResult = ruleRunner(
					fileExtension,
					id,
					"File",
					hasValidExtension(allowedDocumentExtensions)
				);

				if (validationResult[id] == null) {
					validationResult = ruleRunner(
						sizeInMb,
						id,
						"Uploaded file",
						maxFileSize(1)
					);
				}

				validations = { ...this.state.validations };
				validations[id] = validationResult[id];

				this.setState({
					[id]: uploadedFile,
					validations,
					docFileName: uploadedFile.name
				});

				break;
		}
	}

	render() {
		const { userDocs, scholarDocs = [], isFormShown } = this.state;
		return (
			<section className="documents">
				<Loader isLoader={this.props.showLoader} />
				<ConfirmMessagePopup
					message={confirmationMessages.DASHBOARD.DOCUMENTS}
					showPopup={this.state.showConfirmationPopup}
					onConfirmationSuccess={this.state.deleteSuccessCallBack}
					onConfirmationFailure={this.hideConfirmationPopup}
				/>
				<AlertMessage
					isShow={this.state.isShow}
					status={this.state.onStatusCheck}
					msg={this.state.dmdcMsg}
					close={this.onCloseHandler}
				/>
				<article className="ctrl-wrapper widget-border newdoclist">
					<h4>My Documents</h4>
					{!scholarDocs.length ? (
						<article className="col-md-12">
							<article className="ctrl-wrapper">
								<article className="table-responsive dataTbl">
									<table className="table table-striped">
										<tbody>
											<tr>
												<th>ID</th>
												<th>Document Name</th>
												<th className="text-center">Document Details</th>
												<th className="text-center">Action</th>
											</tr>
											{userDocs && userDocs.length ? (
												userDocs.map((documentData, index) => (
													<DocumentsTableDynamicRow
														index={index}
														deleteUserDocuments={this.deleteUserDocuments}
														data={documentData}
													/>
												))
											) : (
													<tr>
														<td colSpan="3" className="text-center">
															No document found
                          </td>
													</tr>
												)}
										</tbody>
									</table>
								</article>
							</article>
							<article className="btn-ctrl">
								<button id="myDocID" onClick={this.showForm}>
									Upload Document
                </button>
							</article>
						</article>
					) : (
							""
						)}
					{isFormShown && !scholarDocs.length ? (
						<article className="ctrl-wrapper widget-border paddingBottom">
							<h4>Checklist of the Documents*</h4>
							<p>
								Select document name from below dropdown menu and then browse a
								document from your computer.
              </p>
							<form name="myForm" autoCapitalize="off">
								<article className="row">
									<article className="file-upload-wrapper">
										<article className="ctrl-wrapper file-wrapper">
											<article className="form-group">
												<select
													name="documentType"
													id="documentType"
													className="form-control"
													onChange={this.onFormFieldChange}
												>
													<option value={JSON.stringify({ id: null })}>
														--Select Document Name--
                          </option>
													{this.props.documentGroupsAndTypes.map(
														documentGroup => (
															<DocumentOptionsGroup
																data={documentGroup}
																currentUserDocTypes={
																	this.state.currentUserDocTypes
																}
															/>
														)
													)}
												</select>
												{this.state.validations.documentType ? (
													<span className="error">
														{this.state.validations.documentType}
													</span>
												) : null}
												<label htmlFor="docFile">
													<span className="browse-btn">Browse</span>
													<input
														type="file"
														id="docFile"
														onChange={this.onFormFieldChange}
														className="form-control"
														name="docFile"
														accept="file_extension|image/*"
														required
													/>
													{this.state.validations.docFile ? (
														<span className="error">
															{this.state.validations.docFile}
														</span>
													) : null}
												</label>
												<span className="spacer" />
												<span
													onClick={this.onDocumentUpload}
													className="upload-btn"
												>
													Upload
                        </span>
											</article>
										</article>
									</article>
								</article>
								<article className="row">
									<article className="file-upload-wrapper ctrl-wrapper">
										<article className="file-wrapper form-group">
											<article className="col-md-12">
												{this.state.showCategoriesDropDown ? (
													<select
														id="documentTypeCategory"
														onChange={this.onFormFieldChange}
														className="form-control selectWrapper"
													>
														<option value={JSON.stringify({ id: null })}>
															--Select--
                            </option>
														{this.props.documentTypeCategories.map(
															docTypeCat => (
																<option value={JSON.stringify(docTypeCat)}>
																	{docTypeCat.categoryName}
																</option>
															)
														)}
													</select>
												) : null}
												{this.state.validations.documentTypeCategory ? (
													<span className="error">
														{this.state.validations.documentTypeCategory}
													</span>
												) : null}
												{this.state.showCategoryOptionsDropDown ? (
													<select
														name="documentTypeCategoryOption"
														id="documentTypeCategoryOption"
														className="form-control selectWrapper"
														onChange={this.onFormFieldChange}
													>
														<option value={JSON.stringify({ id: null })}>
															--Select--
                            </option>
														{this.props.documentTypeCategoriesOptions.map(
															docTypeCatOpt => (
																<option value={JSON.stringify(docTypeCatOpt)}>
																	{docTypeCatOpt.categoryName}
																</option>
															)
														)}
													</select>
												) : null}
												{this.state.validations.documentTypeCategoryOption ? (
													<span className="error">
														{this.state.validations.documentTypeCategoryOption}
													</span>
												) : null}
											</article>
											{this.state.docFileName && (
												<span className="fileName-string">
													<i>File name:</i> {this.state.docFileName}
												</span>
											)}
										</article>
									</article>
									<p className="fileExtn">
										The following file extensions are allowed:
                    <strong> {allowedDocumentExtensions.join(", ")}</strong>
									</p>
								</article>
							</form>
						</article>
					) : this.state.scholarDocs && this.state.scholarDocs.length ? (
						<ScholarDocDetails
							disbursalCycleIds={this.state.disbursalCycleIds}
							scholarDocs={this.state.scholarDocs}
							fetchUserDisbursalId={this.props.fetchUserDisbursalId}
							scholarsDocsName={this.state.scholarsDocs}
							fetchScholarDocs={this.props.fetchScholarDocs}
							uploadScholarDocs={this.props.uploadScholarDocs}
							fetchScholarOrgnlDocs={this.props.fetchScholarOrgnlDocs}
							setData={this.setData}
							orgnlDocs={this.state.orgnlDocs}
						/>
					) : null}
				</article>

				{/* <article className="btn-ctrl">
          <button onClick={() => this.props.goToNextPage("BankDetails")}>
            Next
          </button>
        </article> */}
			</section>
		);
	}
}

class ScholarDocDetails extends Component {
	constructor() {
		super();
		this.state = { scholarshipId: "", disbursalCycleId: "", id: "" };
		this.toggle = this.toggle.bind(this);
		this.disbursalToggle = this.disbursalToggle.bind(this);
		this.fileHandler = this.fileHandler.bind(this);
	}
	toggle(schId) {
		if (this.state.scholarshipId == "" || this.state.scholarshipId != schId) {
			this.setState({ scholarshipId: schId, clasName: "open" });
			this.props.setData({ schId: schId });
			this.props.fetchUserDisbursalId({ scholarshipId: schId });
			this.props.fetchScholarOrgnlDocs({ scholarshipId: schId });
		} else {
			this.setState({ scholarshipId: "", clasName: "" });
		}
	}

	disbursalToggle(disId) {
		if (
			this.state.disbursalCycleId == "" ||
			this.state.disbursalCycleId != disId
		) {
			this.setState({ disbursalCycleId: disId });
			this.props.setData({ disId: disId });
			this.props.fetchScholarDocs({ disbursalCycleId: disId });
		} else {
			this.setState({ disbursalCycleId: "" });
		}
	}

	fileHandler(e, id) {
		const nullIfFalsy = value => value || null;
		const stringifiedData = JSON.stringify({
			documentType: id,
			documentTypeCategory: nullIfFalsy(null), // pass type Year or Semester
			documentTypeCategoryOption: nullIfFalsy(null) // pass year Value or Semester Value
		});
		const file = e.target.files[0];
		this.props.uploadScholarDocs({
			docFile: file,
			scholarshipId: this.state.scholarshipId,
			disbursalId: this.state.disbursalCycleId,
			userDocRequest: stringifiedData
		});
		this.setState({ fileName: file.name, id: id });
	}

	render() {
		const { scholarshipId, disbursalCycleId, clasName } = this.state;
		const {
			scholarDocs = [],
			disbursalCycleIds = [],
			scholarsDocsName = [],
			orgnlDocs = []
		} = this.props;
		let counter = disbursalCycleIds && disbursalCycleIds.length;
		return (
			<section className="col-md-12">
				{scholarDocs &&
					scholarDocs.length &&
					scholarDocs.map(x => (
						<article className="ctrl-wrapper">
							<h6
								className={clasName}
								onClick={() => this.toggle(x.scholarshipId)}
							>
								{x.title}
							</h6>
							{scholarshipId == x.scholarshipId ? (
								<article>
									<h2 class="my-profile" style={{ marginTop: "15px" }}>
										Original Documents Details
                  </h2>
									<article className="ctrl-wrapper">
										<article className="table-responsive dataTbl">
											<table className="table table-striped">
												<thead>
													<tr>
														<th>Document Name</th>
														<th className="text-center">Action</th>
													</tr>
													<tr />
												</thead>
												<tbody>
													{orgnlDocs && orgnlDocs.length ? (
														orgnlDocs.map(d => (
															<tr>
																<td>{d.docNameLabel}</td>
																<td className="actionBtn text-center">
																	{d.location ? (
																		<article>
																			<span>Download</span>
																			<a
																				style={{
																					cursor: "default",
																					textDecoration: "none"
																				}}
																				href={d.location}
																				target="_blank"
																			>
																				<i
																					className="fa fa-download file-lbl dis-inline"
																					aria-hidden="true"
																				/>
																			</a>
																		</article>
																	) : (
																			"N/A"
																		)}
																	<article />
																</td>
															</tr>
														))
													) : (
															<p className="text-center">No document found</p>
														)}
												</tbody>
											</table>
										</article>
									</article>
									{/* //////////////Disbursal Docs////////////////// */}
									{disbursalCycleIds && disbursalCycleIds.length
										? disbursalCycleIds.map((d, index) => (
											<article className="detail">
												<button
													onClick={() =>
														this.disbursalToggle(d.disbursalCycleId)
													}
												>
													{d.title}
												</button>

												{disbursalCycleId == d.disbursalCycleId ? (
													<article className="ctrl-wrapper widget-border">
														<h2 class="my-profile">
															Disbursal Documents Details
                              </h2>
														<article className="ctrl-wrapper">
															<article className="table-responsive dataTbl">
																<table className="table table-striped">
																	<thead>
																		<tr>
																			<th>Document Name</th>
																			<th className="text-center">Action</th>
																		</tr>
																		<tr />
																	</thead>
																	<tbody>
																		{scholarsDocsName &&
																			scholarsDocsName.length ? (
																				scholarsDocsName.map(d => (
																					<tr>
																						<td>{d.docNameLabel}</td>
																						<td className="actionBtn text-center">
																							{d.location ? (
																								<article>
																									<span>Download</span>
																									<a
																										style={{
																											cursor: "default",
																											textDecoration: "none"
																										}}
																										href={d.location}
																										target="_blank"
																									>
																										<i
																											className="fa fa-download file-lbl dis-inline"
																											aria-hidden="true"
																										/>
																									</a>
																								</article>
																							) : (
																									""
																								)}
																							<article>
																								{!d.location ||
																									d.documentStatus !== 1 ? (
																										<article>
																											<span> Upload </span>

																											<label className="pos-rel">
																												<i
																													className="fa fa-upload file-lbl dis-inline"
																													aria-hidden="true"
																												/>
																												<input
																													type="file"
																													className="file-inpt"
																													onChange={e =>
																														this.fileHandler(
																															e,
																															d.documentType
																														)
																													}
																												/>
																											</label>
																										</article>
																									) : (
																										""
																									)}
																								{/* {id == d.documentType ? (
                                                  <span
                                                    style={{
                                                      width: "100%",
                                                      textAlign: "center",
                                                      display: "block"
                                                    }}
                                                  >
                                                    {fileName}
                                                  </span>
                                                ) : (
                                                  ""
                                                )} */}
																							</article>
																						</td>
																					</tr>
																				))
																			) : (
																				<p className="text-center">
																					No document found
                                        </p>
																			)}
																	</tbody>
																</table>
															</article>
														</article>
													</article>
												) : (
														""
													)}
											</article>
										))
										: ""}
								</article>
							) : (
									""
								)}
						</article>
					))}
			</section>
		);
	}
}

const DocumentOptionsGroup = props => (
	<optgroup label={props.data.groupName}>
		{props.data.documentTypeList.map(documentType => {
			//TODO:Add check for filtering document types here

			return (
				<option
					disabled={props.currentUserDocTypes.includes(documentType.id)}
					value={JSON.stringify(documentType)}
				>
					{documentType.description}
				</option>
			);
		})}
	</optgroup>
);

const DocumentsTableDynamicRow = props => {
	return (
		<tr>
			<td>{props.index + 1}</td>
			<td>
				{props.data.documentType ? props.data.documentType.description : ""}
			</td>
			<td>
				<p className="text-green text-center">
					<a href={props.data.location} target="_blank">
						<i className="fa fa-eye" aria-hidden="true" />
					</a>
				</p>
			</td>
			<td>
				{(!props.data || props.data.verified != 1) && (
					<p className="text-red text-center">
						<span
							title="delete this file"
							onClick={() => props.deleteUserDocuments(props.data.id)}
						>
							<i className="fa fa-trash-o" aria-hidden="true" />
						</span>
					</p>
				)}
			</td>
		</tr>
	);
};

export default Documents;
