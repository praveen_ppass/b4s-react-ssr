import React, { Component } from "react";

import Loader from "../../../../shared/common/components/loader";
import { ruleRunner } from "../../../../validation/ruleRunner";
import {
  required,
  isEmail,
  minLength,
  isNumeric
} from "../../../../validation/rules";
import {
  UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_SUCCESS,
  DELETE_ENTRANCE_EXAMINATION_DETAILS_SUCCESS
} from "../../actions";
import {
  entranceExaminationLevels,
  confirmationMessages
} from "../../../../constants/constants";
import { FETCH_RULES_SUCCEEDED } from "../../../../constants/commonActions";
import ConfirmMessagePopup from "../../../common/components/confirmMessagePopup";

class EntranceExam extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      entranceExamId: 0,
      level: "",
      rank: "",
      goToNextPage: false,
      rollNumber: "",
      showConfirmationPopup: false,
      deleteSuccessCallBack: null,
      validations: {
        entranceExamId: null,
        level: null,
        rank: null,
        rollNumber: null
      },
      isFormValid: false
    };

    this.showForm = this.showForm.bind(this);
    this.closeForm = this.closeForm.bind(this);
    this.formChangeHandler = this.formChangeHandler.bind(this);
    this.addOrUpdateEntranceExaminationsDetails = this.addOrUpdateEntranceExaminationsDetails.bind(
      this
    );
    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.editClickHandler = this.editClickHandler.bind(this);
    this.deleteClickHandler = this.deleteClickHandler.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.saveAndGoToNextPage = this.saveAndGoToNextPage.bind(this);
    this.hideConfirmationPopup = this.hideConfirmationPopup.bind(this);
  }

  showConfirmationPopup() {
    this.setState({
      showConfirmationPopup: true
    });
  }

  hideConfirmationPopup() {
    this.setState({
      showConfirmationPopup: false,
      deleteSuccessCallBack: null
    });
  }

  componentDidMount() {
    if (this.props.rulesList) {
      this.props.fetchEntranceExaminationsDetails({
        userid: this.props.userId
      });
    } else {
      this.props.loadRules();
    }
  }

  saveAndGoToNextPage() {
    this.setState({ goToNextPage: true }, () =>
      this.addOrUpdateEntranceExaminationsDetails()
    );
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_SUCCESS:
        if (this.state.goToNextPage) {
          this.props.goToNextPage("ScholarshipHistory");
        } else {
          this.setState(
            {
              isFormShown: false,
              id: "",
              entranceExamId: 0,
              level: "",
              rank: "",
              rollNumber: ""
            },
            () =>
              this.props.fetchEntranceExaminationsDetails({
                userid: this.props.userId
              })
          );
        }

        break;

      case FETCH_RULES_SUCCEEDED:
        this.props.fetchEntranceExaminationsDetails({
          userid: this.props.userId
        });
        break;

      case DELETE_ENTRANCE_EXAMINATION_DETAILS_SUCCESS:
        this.setState(
          {
            showConfirmationPopup: false,
            deleteSuccessCallBack: null
          },
          () =>
            this.props.fetchEntranceExaminationsDetails({
              userid: this.props.userId
            })
        );

        break;
    }
  }

  addOrUpdateEntranceExaminationsDetails() {
    const addOrUpdateApiParams = this.mapToUpdateApiParams();
    //Add validations
    if (this.checkFormValidations()) {
      this.props.addOrUpdateEntranceExaminationsDetails(addOrUpdateApiParams);
    }
  }

  mapToUpdateApiParams() {
    const { entranceExamId, level, rank, rollNumber, id } = this.state;

    let params = {
      entranceExamId,
      level,
      rank,
      rollNumber
    };

    if (id) {
      params.id = id;
    }

    return { userid: this.props.userId, params };
  }
  /************ start - validation part - rules******************* */
  formChangeHandler(event) {
    const { id, value } = event.target;
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );
    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];
    this.setState({
      [id]: value,
      validations
    });
  }
  /************ end - validation part - rules******************* */

  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "entranceExamId":
        validationObject.name = "* Entrance name is";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "level":
        validationObject.name = "* Level";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "rank":
        validationObject.name = "* Rank";
        validationObject.validationFunctions = [required, isNumeric];
        return validationObject;
      case "rollNumber":
        validationObject.name = "* Roll no";
        validationObject.validationFunctions = [required];
        return validationObject;
    }
  }
  /************ end - validation part - get message and required validation******************* */
  /************ start - validation part - check form submit validation******************* */
  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }
  /************ end - validation part - check form submit validation******************* */

  editClickHandler(id) {
    const entranceExamData = this.props.entranceExaminationData.filter(
      data => data.id === id
    );

    const { entranceExamDetail, level, rank, rollNumber } = entranceExamData[0];

    this.setState({
      id,
      entranceExamId: entranceExamDetail.id,
      level,
      rank,
      rollNumber,
      isFormShown: true
    });
  }

  deleteClickHandler(id) {
    const deleteEntranceExam = () =>
      this.props.deleteEntranceExaminationsDetails({
        userid: this.props.userId,
        entranceExamId: id
      });

    this.setState({
      deleteSuccessCallBack: deleteEntranceExam,
      showConfirmationPopup: true
    });
  }

  showForm() {
    this.setState({
      isFormShown: true
    });
  }

  closeForm() {
    this.setState({
      isFormShown: false,
      id: "",
      entranceExamId: 0,
      level: "",
      rank: "",
      rollNumber: ""
    });
  }

  // rawMarkUp() {
  //     let rawMarkup = this.props.privacyPolicyList.privacyConditions
  //     return { __html: rawMarkup };
  // }

  render() {
    const { entrance_exams } = this.props.rulesList || [];
    return (
      <section>
        <ConfirmMessagePopup
          message={confirmationMessages.GENERIC}
          showPopup={this.state.showConfirmationPopup}
          onConfirmationSuccess={this.state.deleteSuccessCallBack}
          onConfirmationFailure={this.hideConfirmationPopup}
        />
        <Loader isLoader={this.props.showLoader} />
        <article className="ctrl-wrapper widget-border margintop50">
          <h4>Entrance Exam </h4>
          <article className="col-md-12">
            <article className="ctrl-wrapper">
              <article className="table-responsive dataTbl">
                <EntranceExaminationDetailsTable
                  entranceExaminationData={this.props.entranceExaminationData}
                  editClickHandler={this.editClickHandler}
                  deleteClickHandler={this.deleteClickHandler}
                />
              </article>
            </article>
          </article>
        </article>
        <article className="col-md-12 width100">
          <article className="row">
            <article className="btn-ctrl">
              <span
                onClick={() => this.props.goToNextPage("ScholarshipHistory")}
                className="btn pull-right but-reference"
              >
                Next
              </span>
              <span
                onClick={this.showForm}
                className="btn pull-left but-reference"
              >
                Add Entrance Exam
              </span>
            </article>
          </article>
        </article>
        {this.state.isFormShown ? (
          <form name="entrancForm" autoCapitalize="off">
            <article className="ctrl-wrapper widget-border frmControl topM">
              <article className="row">
                <article className="col-md-6">
                  <label htmlFor="entranceExamId">Entrance Name*</label>
                  <article className="form-group">
                    <select
                      name="entranceExamId"
                      className="form-control"
                      id="entranceExamId"
                      value={this.state.entranceExamId}
                      onChange={this.formChangeHandler}
                    >
                      <option value="">--Select--</option>
                      {entrance_exams.map(entranceExam => (
                        <option value={entranceExam.id}>
                          {entranceExam.rulevalue}
                        </option>
                      ))}
                    </select>
                    {this.state.validations.entranceExamId ? (
                      <span className="error animated bounce">
                        {this.state.validations.entranceExamId}
                      </span>
                    ) : null}
                  </article>
                </article>
                <article className="col-md-6">
                  <label htmlFor="level">Level*</label>
                  <article className="form-group">
                    <select
                      name="level"
                      className="form-control"
                      id="level"
                      value={this.state.level}
                      onChange={this.formChangeHandler}
                    >
                      <option value="">--Select--</option>
                      {entranceExaminationLevels.map(entranceExam => (
                        <option value={entranceExam}>{entranceExam}</option>
                      ))}
                    </select>
                    {this.state.validations.level ? (
                      <span className="error animated bounce">
                        {this.state.validations.level}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article>
              <article className="row">
                <article className="col-md-6">
                  <label htmlFor="rollNumber">Roll No*</label>
                  <article className="form-group">
                    <input
                      type="text"
                      name="rollNumber"
                      placeholder="roll no"
                      className="form-control"
                      value={this.state.rollNumber}
                      id="rollNumber"
                      onChange={this.formChangeHandler}
                    />
                    {this.state.validations.rollNumber ? (
                      <span className="error animated bounce">
                        {this.state.validations.rollNumber}
                      </span>
                    ) : null}
                    {/* <span className="error animated bounce">
                      * Roll no is required.
                    </span> */}
                  </article>
                </article>
                <article className="col-md-6">
                  <label htmlFor="rank">Rank*</label>
                  <article className="form-group">
                    <input
                      type="text"
                      name="rank"
                      pattern="/^\d+$/"
                      placeholder="rank"
                      className="form-control"
                      id="rank"
                      value={this.state.rank}
                      onChange={this.formChangeHandler}
                    />
                    {this.state.validations.rank ? (
                      <span className="error animated bounce">
                        {this.state.validations.rank}
                      </span>
                    ) : null}
                    {/* <span className="error animated bounce">
                      * Rank is required.
                    </span> */}
                    {/* <span className="error">* Rank should be in digits.</span> */}
                  </article>
                </article>
              </article>
            </article>
            <article className="errorContainer">
              <span className="error" />
            </article>
            <article className="col-md-12 width100">
              <article className="row">
                <article className="btn-ctrl threeBtn">
                  <button
                    type="button"
                    onClick={this.addOrUpdateEntranceExaminationsDetails}
                  >
                    Submit
                  </button>
                  <button
                    type="button"
                    onClick={this.closeForm}
                    className="closeBtn"
                  >
                    Close
                  </button>
                  <button type="button" onClick={this.saveAndGoToNextPage}>
                    Save/Next
                  </button>
                </article>
              </article>
            </article>
          </form>
        ) : null}
      </section>
    );
  }
}

/*   entranceExamId,
      level,
      rank,
      rollNumber, */

const EntranceExaminationDetailsTable = props => {
  const { entranceExaminationData } = props;

  return (
    <table className="table table-striped">
      <tbody>
        <tr>
          <th className="text-center">Entrance Name</th>
          <th className="text-center">Level</th>
          <th className="text-center">Roll No</th>
          <th className="text-center">Rank</th>
          <th className="text-center">Edit</th>
          <th className="text-center">Delete</th>
        </tr>
        {entranceExaminationData.map(entranceExaminationDetail => (
          <EntranceExaminationDetailsTableRow
            data={entranceExaminationDetail}
            editClickHandler={props.editClickHandler}
            deleteClickHandler={props.deleteClickHandler}
          />
        ))}
      </tbody>
    </table>
  );
};

const EntranceExaminationDetailsTableRow = props => (
  <tr>
    <td className="text-center">{props.data.entranceExamDetail.value}</td>
    <td className="text-center">{props.data.level}</td>
    <td className="text-center">{props.data.rollNumber} </td>
    <td className="text-center">{props.data.rank}</td>
    <td className="text-center">
      <p className="text-red text-center">
        <span
          onClick={() => props.editClickHandler(props.data.id)}
          title="Edit this file"
        >
          <i className="fa fa-pencil-square-o" aria-hidden="true" />
        </span>
      </p>
    </td>
    <td className="text-center">
      <p className="text-red text-center">
        <span
          title="delete this file"
          onClick={() => props.deleteClickHandler(props.data.id)}
        >
          <i className="fa fa-trash-o" aria-hidden="true" />
        </span>
      </p>
    </td>
  </tr>
);

export default EntranceExam;
