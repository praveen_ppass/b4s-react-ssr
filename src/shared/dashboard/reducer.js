import {
  FETCH_UPDATE_USER_REQUESTED,
  FETCH_UPDATE_USER_SUCCEEDED,
  FETCH_UPDATE_USER_FAILED,

  //Scholarship history actions
  FETCH_SCHOLARSHIP_HISTORY_DETAILS_REQUEST,
  FETCH_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS,
  FETCH_SCHOLARSHIP_HISTORY_DETAILS_FAILURE,
  UPDATE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST,
  UPDATE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS,
  UPDATE_SCHOLARSHIP_HISTORY_DETAILS_FAILURE,
  DELETE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST,
  DELETE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS,
  DELETE_SCHOLARSHIP_HISTORY_DETAILS_FAILURE,

  //Family earnings actions
  FETCH_FAMILY_EARNING_DETAILS_REQUEST,
  FETCH_FAMILY_EARNING_DETAILS_SUCCESS,
  FETCH_FAMILY_EARNING_DETAILS_FAILURE,
  UPDATE_FAMILY_EARNING_DETAILS_REQUEST,
  UPDATE_FAMILY_EARNING_DETAILS_SUCCESS,
  UPDATE_FAMILY_EARNING_DETAILS_FAILURE,
  DELETE_FAMILY_EARNING_DETAILS_REQUEST,
  DELETE_FAMILY_EARNING_DETAILS_SUCCESS,
  DELETE_FAMILY_EARNING_DETAILS_FAILURE,

  //Entrance examinations actions
  FETCH_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST,
  FETCH_ENTRANCE_EXAMINATIONS_DETAILS_SUCCESS,
  FETCH_ENTRANCE_EXAMINATIONS_DETAILS_FAILURE,
  UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST,
  UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_SUCCESS,
  UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_FAILURE,
  DELETE_ENTRANCE_EXAMINATION_DETAILS_FAILURE,
  DELETE_ENTRANCE_EXAMINATION_DETAILS_REQUEST,
  DELETE_ENTRANCE_EXAMINATION_DETAILS_SUCCESS,

  //Reference actions
  FETCH_REFERENCE__DETAILS_REQUEST,
  FETCH_REFERENCE__DETAILS_SUCCESS,
  FETCH_REFERENCE__DETAILS_FAILURE,
  UPDATE_REFERENCE__DETAILS_REQUEST,
  UPDATE_REFERENCE__DETAILS_SUCCESS,
  UPDATE_REFERENCE__DETAILS_FAILURE,
  DELETE_REFERENCE__DETAILS_REQUEST,
  DELETE_REFERENCE__DETAILS_SUCCESS,
  DELETE_REFERENCE__DETAILS_FAILURE,
  FETCH_OCCUPATION_DATA_REQUEST,
  FETCH_OCCUPATION_DATA_SUCCESS,
  FETCH_OCCUPATION_DATA_FAILURE,
  FETCH_USER_BANK_DETAILS_SUCCESS,
  FETCH_USER_BANK_DETAILS_REQUEST,
  FETCH_USER_BANK_DETAILS_FAILURE,
  UPDATE_USER_BANK_DETAILS_REQUEST,
  UPDATE_USER_BANK_DETAILS_FAILURE,
  UPDATE_USER_BANK_DETAILS_SUCCESS,

  //Education actions
  FETCH_EDUCATION_INFO_REQUEST,
  FETCH_EDUCATION_INFO_SUCCESS,
  FETCH_EDUCATION_INFO_FAILURE,
  UPDATE_EDUCATION_INFO_REQUEST,
  UPDATE_EDUCATION_INFO_SUCCESS,
  UPDATE_EDUCATION_INFO_FAILURE,

  //Documents actions
  FETCH_DOCUMENT_TYPES_CATEGORY_REQUEST,
  FETCH_DOCUMENT_TYPES_CATEGORY_SUCCESS,
  FETCH_DOCUMENT_TYPES_CATEGORY_FAILURE,
  FETCH_DOCUMENT_TYPES_REQUEST,
  FETCH_DOCUMENT_TYPES_SUCCESS,
  FETCH_DOCUMENT_TYPES_FAILURE,
  FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_REQUEST,
  FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_SUCCESS,
  FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_FAILURE,
  UPDATE_USER_DOCUMENTS_REQUEST,
  UPDATE_USER_DOCUMENTS_SUCCESS,
  UPDATE_USER_DOCUMENTS_FAILURE,
  FETCH_USER_DOCUMENTS_REQUEST,
  FETCH_USER_DOCUMENTS_SUCCESS,
  FETCH_USER_DOCUMENTS_FAILURE,
  POST_UPLOAD_USER_DOCUMENT_REQUESTED,
  POST_UPLOAD_USER_DOCUMENT_SUCCEEDED,
  POST_UPLOAD_USER_DOCUMENT_FAILED,

  //Student actions
  UPDATE_STUDENT_FAIL,
  UPDATE_STUDENT_REQUEST,
  UPDATE_STUDENT_SUCCESS,
  FETCH_STUDENT_DETAILS_REQUEST,
  FETCH_STUDENT_DETAILS_SUCCESS,
  FETCH_STUDENT_DETAILS_FAIL,
  FETCH_STUDENT_LIST_REQUEST,
  FETCH_STUDENT_LIST_SUCCESS,
  FETCH_STUDENT_LIST_FAIL,
  EDIT_STUDENT_DETAILS_REQUEST,
  EDIT_STUDENT_DETAILS_SUCCESS,
  EDIT_STUDENT_DETAILS_FAIL,

  // Matching Scholarship actions
  FETCH_MATCHING_SCHOLARSHIPS_REQUEST,
  FETCH_MATCHING_SCHOLARSHIPS_SUCCESS,
  FETCH_MATCHING_SCHOLARSHIPS_FAIL,

  // My Favorite
  FETCH_MY_FAVORITE_REQUEST,
  FETCH_MY_FAVORITE_SUCCESS,
  FETCH_MY_FAVORITE_FAIL,

  // Add to Fav
  ADD_TO_FAV_REQUEST,
  ADD_TO_FAV_SUCCESS,
  ADD_TO_FAV_FAIL,

  // Delete Favs
  DELETE_FAV_REQUEST,
  DELETE_FAV_SUCCESS,
  DELETE_FAV_FAIL,

  // Persist Fav
  FETCH_FAV_PERSIST_REQUEST,
  FETCH_FAV_PERSIST_SUCCESS,
  FETCH_FAV_PERSIST_FAIL,

  // Application Status
  FETCH_APPLICATION_STATUS_REQUEST,
  FETCH_APPLICATION_STATUS_SUCCESS,
  FETCH_APPLICATION_STATUS_FAIL,

  // Application Doc Status
  FETCH_APPLICATION_STATUS_DOC_REQUEST,
  FETCH_APPLICATION_STATUS_DOC_SUCCESS,
  FETCH_APPLICATION_STATUS_DOC_FAIL,

  // Awardee Doc List
  FETCH_AWARDEE_DOC_REQUEST,
  FETCH_AWARDEE_DOC_SUCCESS,
  FETCH_AWARDEE_DOC_FAIL,

  FETCH_DOCUMENT_ISSUE_REQUEST,
  FETCH_DOCUMENT_ISSUE_SUCCESS,
  FETCH_DOCUMENT_ISSUE_FAIL,

  FETCH_DOCUMENT_ISSUE_UPLOAD_REQUEST,
  FETCH_DOCUMENT_ISSUE_UPLOAD_SUCCESS,
  FETCH_DOCUMENT_ISSUE_UPLOAD_FAIL,

  FETCH_QNA_DASHBOARD_REQUEST,
  FETCH_QNA_DASHBOARD_SUCCESS,
  FETCH_QNA_DASHBOARD_FAIL,

  FETCH_QNA_NOTIFICATION_REQUEST,
  FETCH_QNA_NOTIFICATION_SUCCESS,
  FETCH_QNA_NOTIFICATION_FAIL,

  // Upload User  Pic
  UPLOAD_USER_PIC_REQUEST,
  UPLOAD_USER_PIC_SUCCESS,
  UPLOAD_USER_PIC_FAIL,

  // fetch slot booked
  FETCH_BOOK_SLOT_REQUEST,
  FETCH_BOOK_SLOT_SUCCESS,
  FETCH_BOOK_SLOT_FAIL,
  // update slot booked
  UPDATE_BOOK_SLOT_REQUEST,
  UPDATE_BOOK_SLOT_SUCCESS,
  UPDATE_BOOK_SLOT_FAIL,

  // fetch preferred language
  FETCH_PREFERRED_LANG_REQUEST,
  FETCH_PREFERRED_LANG_SUCCESS,
  FETCH_PREFERRED_LANG_FAIL,
  DELETE_USER_DOCUMENTS_REQUEST,
  DELETE_USER_DOCUMENTS_SUCCESS,
  DELETE_USER_DOCUMENTS_FAILURE,

  // fetch csc payment details
  FETCH_CSC_PAYMENT_DETAILS_REQUEST,
  FETCH_CSC_PAYMENT_DETAILS_SUCCESS,
  FETCH_CSC_PAYMENT_DETAILS_FAIL,

  // csc redirect
  FETCH_CSC_REDIRECT_REQUEST,
  FETCH_CSC_REDIRECT_SUCCESS,
  FETCH_CSC_REDIRECT_FAIL,

  // dm / dc redirect
  FETCH_DM_DC_REQUEST,
  FETCH_DM_DC_SUCCESS,
  FETCH_DM_DC_FAIL,

  // save DM / DC
  UPLOAD_DMDC_REQUEST,
  UPLOAD_DMDC_SUCCESS,
  UPLOAD_DMDC_FAIL,

  // Book Slot Dates
  FETCH_BOOK_SLOT_DATES_REQUEST,
  FETCH_BOOK_SLOT_DATES_SUCCESS,
  FETCH_BOOK_SLOT_DATES_FAIL,

  // Scholar - scholarship won
  FETCH_SCHOLAR_SCHOLARSHIP_WON_REQUEST,
  FETCH_SCHOLAR_SCHOLARSHIP_WON_SUCCESS,
  FETCH_SCHOLAR_SCHOLARSHIP_WON_FAIL,

  // Scholar - scholarship won Certificate
  FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_REQUEST,
  FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_SUCCESS,
  FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_FAIL,

  //Scholar- Disbursal
  FETCH_USER_DISBURSAL_REQUESTED,
  FETCH_USER_DISBURSAL_SUCCEEDED,
  FETCH_USER_DISBURSAL_FAILED,

  //Scholar- Disbursal id
  FETCH_USER_DISBURSALID_REQUESTED,
  FETCH_USER_DISBURSALID_SUCCEEDED,
  FETCH_USER_DISBURSALID_FAILED,

  //Scholar- Documents
  FETCH_SCHOLAR_DOCUMENT_REQUESTED,
  FETCH_SCHOLAR_DOCUMENT_SUCCEEDED,
  FETCH_SCHOLAR_DOCUMENT_FAILED,
  FETCH_SCHOLAR_ORGNL_DOCUMENT_REQUESTED,
  FETCH_SCHOLAR_ORGNL_DOCUMENT_SUCCEEDED,
  FETCH_SCHOLAR_ORGNL_DOCUMENT_FAILED,

  //Upload Scholar Docs
  POST_UPLOAD_DOCUMENT_REQUESTED,
  POST_UPLOAD_DOCUMENT_SUCCEEDED,
  POST_UPLOAD_DOCUMENT_FAILED,

  //Disbursal Acknowledgement
  POST_DISBURSAL_ACK_REQUESTED,
  POST_DISBURSAL_ACK_SUCCEEDED,
  POST_DISBURSAL_ACK_FAILED
} from "./actions";

import {
  FETCH_RULES_SUCCEEDED,
  FETCH_RULES_FAILED,
  FETCH_USER_RULES_SUCCESS,
  UPDATE_USER_RULES_SUCCESS,
  FETCH_USERLIST_SUCCEEDED,
  FETCH_USERLIST_FAILED,
  UPDATE_USER_RULES_REQUEST,
  FETCH_USER_RULES_REQUEST,
  FETCH_USER_MATCHING_RULES_REQUESTED,
  FETCH_USER_MATCHING_RULES_SUCCEEDED,
  FETCH_DEPENDANT_DATA_SUCCEEDED,

  FETCH_OTP_MOBILE_CHANGE_REQUESTED,
  FETCH_OTP_MOBILE_CHANGE_SUCCEEDED,
  FETCH_OTP_MOBILE_CHANGE_FAILED,

  FETCH_OTP_EMAIL_CHANGE_REQUESTED,
  FETCH_OTP_EMAIL_CHANGE_SUCCEEDED,
  FETCH_OTP_EMAIL_CHANGE_FAILED,

  SUBMIT_OTP_MOB_EMAIL_CHANGE_REQUESTED,
  SUBMIT_OTP_MOB_EMAIL_CHANGE_SUCCEEDED,
  SUBMIT_OTP_MOB_EMAIL_CHANGE_FAILED,
} from "../../constants/commonActions";
import { LOG_USER_OUT } from "../login/actions";
import {
  FETCH_DMDC_REQUESTED,
  FETCH_DMDC_SUCCEEDED,
  FETCH_DMDC_FAILED
} from "../csc/actions";

const initialState = {
  userFamilyEarningData: [],
  entranceExaminationData: [],
  scholarshipHistoryData: [],
  referencesData: [],
  occupationData: [],
  userBankDetailsData: [],
  documentGroupsAndTypes: [],
  educationData: [],
  userDocumentsData: [],
  updatedUserDocumentsData: [],
  documentTypeCategories: [],
  documentTypeCategoriesOptions: [],
  scholarScholarshipWonData: [],
  scholarScholarshipCertificatePdfData: [],
  scholarMessageData: [],
  bookSlotData: "",
  isError: false,
  errorMessage: "",
  updateError: "",
  isUpdateError: false,
  isAuthenticated: true,
  showLoader: true,
  isUploadDMDC: false,
  serverError: "",
  isServerError: false,
  disbursalData: "",
  disbursalId: "",
  scholarsDocs: "",
  uploadedDocs: "",
  orgnlDocs: "",
  userDocs: "",
  otpEmail: [],
  otpMobile: [],
  verifyOtp: [],
  disbursalAcknowledgementData: []
};

const dashboardReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_UPDATE_USER_REQUESTED:
      return {
        ...state,
        payload,
        showLoader: true,
        type,
        updateError: "",
        isUpdateError: false
      };

    case LOG_USER_OUT:
      return { ...state, isAuthenticated: false, type };

    case FETCH_UPDATE_USER_SUCCEEDED:
      return Object.assign({}, state, {
        updateUserInfo: payload,
        showLoader: false,
        isUpdateError: false,
        updateError: "",
        type
      });

    case FETCH_UPDATE_USER_FAILED:
      return {
        ...state,
        showLoader: false,
        isUpdateError: true,
        updateError: payload
      };

    /* Common rules section */
    case FETCH_RULES_SUCCEEDED:
      return { ...state, showLoader: false, type };

    case FETCH_RULES_FAILED:
      return { ...state, showLoader: false, type, isError: true };

    /* Common User List section */
    case FETCH_USERLIST_SUCCEEDED:
      return { ...state, showLoader: false, type };
    case FETCH_USERLIST_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };
    /* Scholarship history section */
    case FETCH_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        scholarshipHistoryData: payload
      };

    case FETCH_SCHOLARSHIP_HISTORY_DETAILS_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    case UPDATE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case UPDATE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        updatedScholarshipHistoryData: payload
      };

    case UPDATE_SCHOLARSHIP_HISTORY_DETAILS_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    case DELETE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case DELETE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS:
      return { ...state, showLoader: false, type };

    case DELETE_SCHOLARSHIP_HISTORY_DETAILS_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    /* Family earning details section */

    case FETCH_FAMILY_EARNING_DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_FAMILY_EARNING_DETAILS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        userFamilyEarningData: payload
      };

    case FETCH_FAMILY_EARNING_DETAILS_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    case UPDATE_FAMILY_EARNING_DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case UPDATE_FAMILY_EARNING_DETAILS_SUCCESS:
      return { ...state, showLoader: false, type };

    case UPDATE_FAMILY_EARNING_DETAILS_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    case DELETE_FAMILY_EARNING_DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case DELETE_FAMILY_EARNING_DETAILS_SUCCESS:
      return { ...state, showLoader: false, type };

    case DELETE_FAMILY_EARNING_DETAILS_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    /*  References section */

    case FETCH_REFERENCE__DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_REFERENCE__DETAILS_SUCCESS:
      return { ...state, showLoader: false, type, referencesData: payload };

    case FETCH_REFERENCE__DETAILS_FAILURE:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true
      };

    case UPDATE_REFERENCE__DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case UPDATE_REFERENCE__DETAILS_SUCCESS:
      return {
        ...state,
        showLoader: true,
        type,
        updatedReferenceData: payload
      };

    case UPDATE_REFERENCE__DETAILS_FAILURE:
      return {
        ...state,
        showLoader: false,
        type
      };

    case DELETE_REFERENCE__DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case DELETE_REFERENCE__DETAILS_SUCCESS:
      return { ...state, showLoader: false, type };

    case DELETE_REFERENCE__DETAILS_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    /* Entrance examination section*/

    case FETCH_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST:
      return {
        ...state,
        showLoader: true,
        type
      };

    case FETCH_ENTRANCE_EXAMINATIONS_DETAILS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        entranceExaminationData: payload
      };

    case UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST:
      return {
        ...state,
        showLoader: true,
        type
      };

    case UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        updatedEntranceExaminationData: payload
      };

    case DELETE_ENTRANCE_EXAMINATION_DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case DELETE_ENTRANCE_EXAMINATION_DETAILS_SUCCESS:
      return { ...state, showLoader: false, type };

    /* Bank details form section */
    case FETCH_USER_BANK_DETAILS_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        errorMessage: "",
        isError: false
      };

    case FETCH_USER_BANK_DETAILS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        userBankDetailsData: payload,
        isError: false,
        errorMessage: "",
        type
      };

    case FETCH_USER_BANK_DETAILS_FAILURE:
      return {
        ...state,
        showLoader: false,
        isError: true,
        errorMessage: payload,
        type
      };

    case UPDATE_USER_BANK_DETAILS_REQUEST:
      return {
        ...state,
        showLoader: true,
        errorMessage: "",
        isError: false,
        type
      };

    case UPDATE_USER_BANK_DETAILS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        updatedUserBankDetailsData: payload
      };

    case UPDATE_USER_BANK_DETAILS_FAILURE:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true
      };

    /* Occupation data */
    case FETCH_OCCUPATION_DATA_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_OCCUPATION_DATA_SUCCESS:
      return { ...state, showLoader: false, type, occupationData: payload };

    case FETCH_OCCUPATION_DATA_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    /* Interest form user rules request*/
    case FETCH_USER_RULES_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_USER_RULES_SUCCESS:
      return { ...state, showLoader: false, userRulesData: payload, type };

    case UPDATE_USER_RULES_REQUEST:
      return { ...state, showLoader: true, type };

    case UPDATE_USER_RULES_SUCCESS:
      return { ...state, showLoader: false, type };

    /* Education data */

    case FETCH_EDUCATION_INFO_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_EDUCATION_INFO_SUCCESS:
      return { ...state, showLoader: false, type, educationData: payload };

    case FETCH_EDUCATION_INFO_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    case UPDATE_EDUCATION_INFO_REQUEST:
      return { ...state, showLoader: true, type };

    case UPDATE_EDUCATION_INFO_SUCCESS:
      return { ...state, showLoader: false, type, educationData: payload };

    case UPDATE_EDUCATION_INFO_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    /* Document type actions */

    case FETCH_DOCUMENT_TYPES_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_DOCUMENT_TYPES_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        documentGroupsAndTypes: payload
      };

    case FETCH_DOCUMENT_TYPES_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    case FETCH_DOCUMENT_TYPES_CATEGORY_REQUEST:
      return { ...state, showLoader: true, type, isError: false };

    case FETCH_DOCUMENT_TYPES_CATEGORY_SUCCESS:
      return {
        ...state,
        documentTypeCategories: payload,
        showLoader: false,
        type,
        isError: false
      };

    case FETCH_DOCUMENT_TYPES_CATEGORY_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    case FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_REQUEST:
      return { ...state, showLoader: true, type, isError: false };

    case FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        documentTypeCategoriesOptions: payload,
        type,
        isError: false
      };

    case FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_FAILURE:
      return { ...state, showLoader: false, type, isError: true };

    // User document actions

    case FETCH_USER_DOCUMENTS_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_USER_DOCUMENTS_SUCCESS:
      return { ...state, showLoader: false, type, userDocumentsData: payload };

    case FETCH_USER_DOCUMENTS_FAILURE:
      return { ...state, showLoader: false, type, isError: false };

    case UPDATE_USER_DOCUMENTS_REQUEST:
      return { ...state, showLoader: true, type };

    case UPDATE_USER_DOCUMENTS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        updatedUserDocuments: payload
      };

    case UPDATE_USER_DOCUMENTS_FAILURE:
      return {
        ...state,
        showLoader: false,
        type,
        isError: false
      };

    case DELETE_USER_DOCUMENTS_REQUEST:
      return { ...state, showLoader: true, type };

    case DELETE_USER_DOCUMENTS_SUCCESS:
      return { ...state, showLoader: true, type };

    case DELETE_USER_DOCUMENTS_FAILURE:
      return { ...state, showLoader: false, type };

    case UPDATE_STUDENT_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        serverError: "",
        isServerError: false
      };

    case UPDATE_STUDENT_SUCCESS:
      return { ...state, showLoader: false, type, studentData: payload };

    case UPDATE_STUDENT_FAIL:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        isServerError: payload.isServerError,
        serverError: payload.errorMessage
      };

    case FETCH_STUDENT_DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_STUDENT_DETAILS_SUCCESS:
      return { ...state, showLoader: false, type, studentData: payload };

    case FETCH_STUDENT_DETAILS_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    case EDIT_STUDENT_DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case EDIT_STUDENT_DETAILS_SUCCESS:
      return { ...state, showLoader: false, type, studentData: payload };

    case EDIT_STUDENT_DETAILS_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    case FETCH_STUDENT_LIST_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_STUDENT_LIST_SUCCESS:
      return { ...state, showLoader: false, type, studentData: payload };

    case FETCH_STUDENT_LIST_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // Matching Scholarship
    case FETCH_MATCHING_SCHOLARSHIPS_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_MATCHING_SCHOLARSHIPS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        matchingScholarships: payload
      };

    case FETCH_MATCHING_SCHOLARSHIPS_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    case FETCH_USER_MATCHING_RULES_REQUESTED:
      return { ...state, showLoader: true, type };
    case FETCH_USER_MATCHING_RULES_SUCCEEDED:
      return { ...state, showLoader: false, type, matchingRules: payload };

    // My Favorites

    case FETCH_MY_FAVORITE_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_MY_FAVORITE_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        favScholarships: payload
      };

    case FETCH_MY_FAVORITE_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // Add To Fav
    case ADD_TO_FAV_REQUEST:
      return { ...state, showLoader: true, type };

    case ADD_TO_FAV_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        addToFavScholarships: payload
      };

    case ADD_TO_FAV_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // Delete Fav
    case DELETE_FAV_REQUEST:
      return { ...state, showLoader: true, type };

    case DELETE_FAV_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        deleteFavScholarships: payload
      };

    case DELETE_FAV_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // Persist Fav
    case FETCH_FAV_PERSIST_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_FAV_PERSIST_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        persistFav: payload
      };

    case FETCH_FAV_PERSIST_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // Application Status
    case FETCH_APPLICATION_STATUS_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_APPLICATION_STATUS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        applicationStatus: payload
      };

    case FETCH_APPLICATION_STATUS_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // Application Doc Status
    case FETCH_APPLICATION_STATUS_DOC_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_APPLICATION_STATUS_DOC_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        applicationStatusDoc: payload
      };

    case FETCH_APPLICATION_STATUS_DOC_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    case FETCH_DOCUMENT_ISSUE_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_DOCUMENT_ISSUE_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        documentIssueData: payload
      };

    case FETCH_DOCUMENT_ISSUE_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // Awardee Doc list
    case FETCH_AWARDEE_DOC_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_AWARDEE_DOC_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        awardeeDocData: payload
      };

    case FETCH_AWARDEE_DOC_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    case FETCH_DOCUMENT_ISSUE_UPLOAD_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_DOCUMENT_ISSUE_UPLOAD_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        documentIssueUpload: payload
      };

    case FETCH_DOCUMENT_ISSUE_UPLOAD_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    case FETCH_QNA_DASHBOARD_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_QNA_DASHBOARD_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        dashQna: payload
      };

    case FETCH_QNA_DASHBOARD_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    case FETCH_QNA_NOTIFICATION_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_QNA_NOTIFICATION_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        qnaNotification: payload
      };

    case FETCH_QNA_NOTIFICATION_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // Upload User Pic
    case UPLOAD_USER_PIC_REQUEST:
      return { ...state, showLoader: true, type };

    case UPLOAD_USER_PIC_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        uploadPicData: payload
      };

    case UPLOAD_USER_PIC_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // Get Slot Booked
    case FETCH_BOOK_SLOT_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        isUpdateError: false,
        updateError: ""
      };

    case FETCH_BOOK_SLOT_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        isUpdateError: false,
        updateError: "",
        bookSlot: payload
      };

    case FETCH_BOOK_SLOT_FAIL:
      return {
        ...state,
        showLoader: false,
        type,
        updateError: payload,
        isUpdateError: true
      };

    // update Slot Booked
    case UPDATE_BOOK_SLOT_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        updateError: "",
        isUpdateError: false
      };

    case UPDATE_BOOK_SLOT_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        updateError: "",
        isUpdateError: false,
        bookSlot: payload
      };

    case UPDATE_BOOK_SLOT_FAIL:
      return {
        ...state,
        showLoader: false,
        type,
        updateError: payload,
        isUpdateError: true
      };

    // preferred language
    case FETCH_PREFERRED_LANG_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_PREFERRED_LANG_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        preferredLang: payload
      };

    case FETCH_PREFERRED_LANG_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // CSC PAYMENT DETAILS
    case FETCH_CSC_PAYMENT_DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_CSC_PAYMENT_DETAILS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        cscPaymentDts: payload
      };

    case FETCH_CSC_PAYMENT_DETAILS_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // csc redirect

    case FETCH_CSC_REDIRECT_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_CSC_REDIRECT_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        cscRedirect: payload
      };

    case FETCH_CSC_REDIRECT_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // DM / DC

    case FETCH_DM_DC_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_DM_DC_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        dmdcList: payload
      };

    case FETCH_DM_DC_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // Get DM/DC info
    case FETCH_DMDC_REQUESTED:
      return Object.assign({}, state, {
        type: type
      });
    case FETCH_DMDC_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        dmdcDetails: payload,
        type: type
      });
    case FETCH_DMDC_FAILED:
      return { ...state, showLoader: false, type, isError: true };

    // Post DM/DC info
    case UPLOAD_DMDC_REQUEST:
      return Object.assign({}, state, {
        isUploadDMDC: true,
        type: type
      });
    case UPLOAD_DMDC_SUCCESS:
      return Object.assign({}, state, {
        isUploadDMDC: false,
        postedDMDC: payload,
        type: type
      });
    case FETCH_DEPENDANT_DATA_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type: type,
        district: payload
      });
    case UPLOAD_DMDC_FAIL:
      return { ...state, isUploadDMDC: false, type, isError: true };
    // Book Slot Dates
    case FETCH_BOOK_SLOT_DATES_REQUEST:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type,
        bookSlotData: ""
      };
    case FETCH_BOOK_SLOT_DATES_SUCCESS:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type: FETCH_BOOK_SLOT_DATES_SUCCESS,
        bookSlotData: payload
      };
    case FETCH_BOOK_SLOT_DATES_FAIL:
      return {
        ...state,
        showLoader: false,
        isError: true,
        type: FETCH_BOOK_SLOT_DATES_FAIL,
        bookSlotData: payload
      };

    /* Scholar - Scholarship Won */
    case FETCH_SCHOLAR_SCHOLARSHIP_WON_REQUEST:
      return {
        ...state,
        showLoader: true,
        scholarScholarshipWonData: null,
        type,
        errorMessage: "",
        isError: false
      };

    case FETCH_SCHOLAR_SCHOLARSHIP_WON_SUCCESS:
      return {
        ...state,
        showLoader: false,
        scholarScholarshipWonData: payload,
        isError: false,
        errorMessage: "",
        type
      };

    case FETCH_SCHOLAR_SCHOLARSHIP_WON_FAIL:
      return {
        ...state,
        showLoader: false,
        isError: true,
        errorMessage: payload,
        type,
        scholarScholarshipWonData: null
      };

    /* Scholar - Scholarship Certificate Pdf */
    case FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_REQUEST:
      return {
        ...state,
        showLoader: true,
        scholarScholarshipCertificatePdfData: null,
        type,
        errorMessage: "",
        isError: false
      };

    case FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_SUCCESS:
      return {
        ...state,
        showLoader: false,
        scholarScholarshipCertificatePdfData: payload,
        isError: false,
        errorMessage: "",
        type,
        disbursalData: ""
      };

    case FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_FAIL:
      return {
        ...state,
        showLoader: false,
        isError: true,
        errorMessage: payload,
        type,
        scholarScholarshipCertificatePdfData: null
      };
    // Scholar Disbursal
    case FETCH_USER_DISBURSAL_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        disbursalData: ""
      };

    case FETCH_USER_DISBURSAL_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        disbursalData: payload
      };

    case FETCH_USER_DISBURSAL_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        disbursalData: payload
      };
    // scholar disbursal id
    case FETCH_USER_DISBURSALID_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        disbursalId: ""
      };

    case FETCH_USER_DISBURSALID_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        disbursalId: payload
      };

    case FETCH_USER_DISBURSALID_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        disbursalId: payload
      };

    // scholar documents
    case FETCH_SCHOLAR_DOCUMENT_REQUESTED:
      return {
        ...state,
        showLoader: false,
        type,
        scholarsDocs: ""
      };
    case FETCH_SCHOLAR_DOCUMENT_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        scholarsDocs: payload
      };
    case FETCH_SCHOLAR_DOCUMENT_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        scholarsDocs: payload
      };
    // Upload scholar documents
    case POST_UPLOAD_DOCUMENT_REQUESTED:
      return {
        ...state,
        showLoader: false,
        type,
        uploadedDocs: ""
      };
    case POST_UPLOAD_DOCUMENT_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        uploadedDocs: payload
      };
    case POST_UPLOAD_DOCUMENT_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        uploadedDocs: payload
      };
    case FETCH_SCHOLAR_ORGNL_DOCUMENT_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        orgnlDocs: ""
      };
    case FETCH_SCHOLAR_ORGNL_DOCUMENT_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        orgnlDocs: payload
      };
    case FETCH_SCHOLAR_ORGNL_DOCUMENT_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        orgnlDocs: payload
      };

    case POST_UPLOAD_USER_DOCUMENT_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        userDocs: ""
      };
    case POST_UPLOAD_USER_DOCUMENT_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        userDocs: payload
      };
    case POST_UPLOAD_USER_DOCUMENT_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        userDocs: payload
      };

    /* OTP mobile and email change  */
    case FETCH_OTP_EMAIL_CHANGE_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        otpEmail: ""
      };
    case FETCH_OTP_EMAIL_CHANGE_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        otpEmail: payload
      };
    case FETCH_OTP_EMAIL_CHANGE_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        otpEmail: payload
      };

    case FETCH_OTP_MOBILE_CHANGE_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        otpMobile: ""
      };
    case FETCH_OTP_MOBILE_CHANGE_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        otpMobile: payload
      };
    case FETCH_OTP_MOBILE_CHANGE_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        otpMobile: payload
      };

    case SUBMIT_OTP_MOB_EMAIL_CHANGE_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        verifyOtp: ""
      };
    case SUBMIT_OTP_MOB_EMAIL_CHANGE_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        verifyOtp: payload
      };
    case SUBMIT_OTP_MOB_EMAIL_CHANGE_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        verifyOtp: payload
      };

    //Disbursal Acknowledgement      
    case POST_DISBURSAL_ACK_REQUESTED:
      return {
        ...state,
        showLoader: false,
        type,
        disbursalAcknowledgementData: ""
      };
    case POST_DISBURSAL_ACK_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        disbursalAcknowledgementData: payload
      };
    case POST_DISBURSAL_ACK_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        disbursalAcknowledgementData: payload
      };

    default:
      return state;
  }
};
export default dashboardReducer;
