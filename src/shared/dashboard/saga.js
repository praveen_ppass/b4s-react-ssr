import axios from "axios";

import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import {
  apiUrl,
  apiCallTypes,
  baseUrls,
  messages
} from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_UPDATE_USER_REQUESTED,
  FETCH_UPDATE_USER_SUCCEEDED,
  FETCH_UPDATE_USER_FAILED,

  // Scholarship history actions
  FETCH_SCHOLARSHIP_HISTORY_DETAILS_REQUEST,
  FETCH_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS,
  FETCH_SCHOLARSHIP_HISTORY_DETAILS_FAILURE,
  UPDATE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST,
  UPDATE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS,
  UPDATE_SCHOLARSHIP_HISTORY_DETAILS_FAILURE,
  DELETE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST,
  DELETE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS,
  DELETE_SCHOLARSHIP_HISTORY_DETAILS_FAILURE,

  // Family earning actions
  FETCH_FAMILY_EARNING_DETAILS_REQUEST,
  FETCH_FAMILY_EARNING_DETAILS_SUCCESS,
  FETCH_FAMILY_EARNING_DETAILS_FAILURE,
  UPDATE_FAMILY_EARNING_DETAILS_REQUEST,
  UPDATE_FAMILY_EARNING_DETAILS_SUCCESS,
  UPDATE_FAMILY_EARNING_DETAILS_FAILURE,
  DELETE_FAMILY_EARNING_DETAILS_REQUEST,
  DELETE_FAMILY_EARNING_DETAILS_SUCCESS,
  DELETE_FAMILY_EARNING_DETAILS_FAILURE,

  //Reference actions
  FETCH_REFERENCE__DETAILS_REQUEST,
  FETCH_REFERENCE__DETAILS_SUCCESS,
  UPDATE_REFERENCE__DETAILS_REQUEST,
  UPDATE_REFERENCE__DETAILS_SUCCESS,
  DELETE_REFERENCE__DETAILS_REQUEST,
  DELETE_REFERENCE__DETAILS_SUCCESS,
  FETCH_REFERENCE__DETAILS_FAILURE,
  UPDATE_REFERENCE__DETAILS_FAILURE,
  DELETE_REFERENCE__DETAILS_FAILURE,

  //Entrance examination actions
  FETCH_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST,
  FETCH_ENTRANCE_EXAMINATIONS_DETAILS_SUCCESS,
  UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST,
  DELETE_ENTRANCE_EXAMINATION_DETAILS_REQUEST,
  UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_SUCCESS,
  DELETE_ENTRANCE_EXAMINATION_DETAILS_SUCCESS,
  FETCH_ENTRANCE_EXAMINATIONS_DETAILS_FAILURE,
  UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_FAILURE,
  DELETE_ENTRANCE_EXAMINATION_DETAILS_FAILURE,

  //Occupation data
  FETCH_OCCUPATION_DATA_REQUEST,
  FETCH_OCCUPATION_DATA_SUCCESS,
  FETCH_OCCUPATION_DATA_FAILURE,

  //Bank details data actions
  FETCH_USER_BANK_DETAILS_REQUEST,
  UPDATE_USER_BANK_DETAILS_REQUEST,
  FETCH_USER_BANK_DETAILS_SUCCESS,
  UPDATE_USER_BANK_DETAILS_SUCCESS,
  FETCH_USER_BANK_DETAILS_FAILURE,
  UPDATE_USER_BANK_DETAILS_FAILURE,

  //edcation data
  FETCH_EDUCATION_INFO_REQUEST,
  FETCH_EDUCATION_INFO_SUCCESS,
  FETCH_EDUCATION_INFO_FAILURE,
  UPDATE_EDUCATION_INFO_REQUEST,
  UPDATE_EDUCATION_INFO_SUCCESS,
  UPDATE_EDUCATION_INFO_FAILURE,

  //User document calls
  FETCH_USER_DOCUMENTS_REQUEST,
  UPDATE_USER_DOCUMENTS_REQUEST,
  DELETE_USER_DOCUMENTS_REQUEST,
  FETCH_DOCUMENT_TYPES_CATEGORY_REQUEST,
  FETCH_DOCUMENT_TYPES_CATEGORY_FAILURE,
  FETCH_DOCUMENT_TYPES_CATEGORY_SUCCESS,
  FETCH_DOCUMENT_TYPES_REQUEST,
  FETCH_DOCUMENT_TYPES_SUCCESS,
  FETCH_DOCUMENT_TYPES_FAILURE,
  FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_FAILURE,
  FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_REQUEST,
  FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_SUCCESS,
  FETCH_USER_DOCUMENTS_SUCCESS,
  UPDATE_USER_DOCUMENTS_SUCCESS,
  FETCH_USER_DOCUMENTS_FAILURE,
  UPDATE_USER_DOCUMENTS_FAILURE,
  DELETE_USER_DOCUMENTS_FAILURE,
  DELETE_USER_DOCUMENTS_SUCCESS,
  POST_UPLOAD_USER_DOCUMENT_REQUESTED,
  POST_UPLOAD_USER_DOCUMENT_SUCCEEDED,
  POST_UPLOAD_USER_DOCUMENT_FAILED,

  //my subscriber
  UPDATE_STUDENT_REQUEST,
  UPDATE_STUDENT_SUCCESS,
  UPDATE_STUDENT_FAIL,
  FETCH_STUDENT_DETAILS_REQUEST,
  FETCH_STUDENT_DETAILS_SUCCESS,
  FETCH_STUDENT_DETAILS_FAIL,
  FETCH_STUDENT_LIST_REQUEST,
  FETCH_STUDENT_LIST_SUCCESS,
  FETCH_STUDENT_LIST_FAIL,
  EDIT_STUDENT_DETAILS_REQUEST,
  EDIT_STUDENT_DETAILS_SUCCESS,
  EDIT_STUDENT_DETAILS_FAIL,

  // Matching Scholarship
  FETCH_MATCHING_SCHOLARSHIPS_REQUEST,
  FETCH_MATCHING_SCHOLARSHIPS_SUCCESS,
  FETCH_MATCHING_SCHOLARSHIPS_FAIL,

  // Favorite Scholarship
  FETCH_MY_FAVORITE_REQUEST,
  FETCH_MY_FAVORITE_SUCCESS,
  FETCH_MY_FAVORITE_FAIL,

  // Add To Fav
  ADD_TO_FAV_REQUEST,
  ADD_TO_FAV_SUCCESS,
  ADD_TO_FAV_FAIL,

  // Delete Favs
  DELETE_FAV_REQUEST,
  DELETE_FAV_SUCCESS,
  DELETE_FAV_FAIL,

  // Persist Fav
  FETCH_FAV_PERSIST_REQUEST,
  FETCH_FAV_PERSIST_SUCCESS,
  FETCH_FAV_PERSIST_FAIL,

  // Application Status
  FETCH_APPLICATION_STATUS_REQUEST,
  FETCH_APPLICATION_STATUS_SUCCESS,
  FETCH_APPLICATION_STATUS_FAIL,

  FETCH_APPLICATION_STATUS_DOC_REQUEST,
  FETCH_APPLICATION_STATUS_DOC_SUCCESS,
  FETCH_APPLICATION_STATUS_DOC_FAIL,

  FETCH_DOCUMENT_ISSUE_REQUEST,
  FETCH_DOCUMENT_ISSUE_SUCCESS,
  FETCH_DOCUMENT_ISSUE_FAIL,

  FETCH_DOCUMENT_ISSUE_UPLOAD_REQUEST,
  FETCH_DOCUMENT_ISSUE_UPLOAD_SUCCESS,
  FETCH_DOCUMENT_ISSUE_UPLOAD_FAILED,

  FETCH_QNA_DASHBOARD_REQUEST,
  FETCH_QNA_DASHBOARD_SUCCESS,
  FETCH_QNA_DASHBOARD_FAILED,

  FETCH_QNA_NOTIFICATION_REQUEST,
  FETCH_QNA_NOTIFICATION_SUCCESS,
  FETCH_QNA_NOTIFICATION_FAILED,

  FETCH_AWARDEE_DOC_REQUEST,
  FETCH_AWARDEE_DOC_SUCCESS,
  FETCH_AWARDEE_DOC_FAILED,

  // Upload User  Pic
  UPLOAD_USER_PIC_REQUEST,
  UPLOAD_USER_PIC_SUCCESS,
  UPLOAD_USER_PIC_FAIL,

  // fetch slot booked
  FETCH_BOOK_SLOT_REQUEST,
  FETCH_BOOK_SLOT_SUCCESS,
  FETCH_BOOK_SLOT_FAIL,

  // update slot booked
  UPDATE_BOOK_SLOT_REQUEST,
  UPDATE_BOOK_SLOT_SUCCESS,
  UPDATE_BOOK_SLOT_FAIL,

  // fetch preferred language
  FETCH_PREFERRED_LANG_REQUEST,
  FETCH_PREFERRED_LANG_SUCCESS,
  FETCH_PREFERRED_LANG_FAIL,

  // csc payment details
  FETCH_CSC_PAYMENT_DETAILS_REQUEST,
  FETCH_CSC_PAYMENT_DETAILS_SUCCESS,
  FETCH_CSC_PAYMENT_DETAILS_FAIL,

  // csc redirect
  FETCH_CSC_REDIRECT_REQUEST,
  FETCH_CSC_REDIRECT_SUCCESS,
  FETCH_CSC_REDIRECT_FAIL,

  // dm / dc
  FETCH_DM_DC_REQUEST,
  FETCH_DM_DC_SUCCESS,
  FETCH_DM_DC_FAIL,
  UPLOAD_DMDC_SUCCESS,
  UPLOAD_DMDC_FAIL,
  UPLOAD_DMDC_REQUEST,

  // Book Slot Dates
  FETCH_BOOK_SLOT_DATES_REQUEST,
  FETCH_BOOK_SLOT_DATES_SUCCESS,
  FETCH_BOOK_SLOT_DATES_FAIL,

  // Scholar - scholarship won
  FETCH_SCHOLAR_SCHOLARSHIP_WON_REQUEST,
  FETCH_SCHOLAR_SCHOLARSHIP_WON_SUCCESS,
  FETCH_SCHOLAR_SCHOLARSHIP_WON_FAIL,

  // Scholar - scholarship won certificate
  FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_REQUEST,
  FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_SUCCESS,
  FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_FAIL,

  //Scholar- Disbursal
  FETCH_USER_DISBURSAL_REQUESTED,
  FETCH_USER_DISBURSAL_SUCCEEDED,
  FETCH_USER_DISBURSAL_FAILED,

  //Scholar- Disbursal id
  FETCH_USER_DISBURSALID_REQUESTED,
  FETCH_USER_DISBURSALID_SUCCEEDED,
  FETCH_USER_DISBURSALID_FAILED,

  //Scholar- Documents
  FETCH_SCHOLAR_DOCUMENT_REQUESTED,
  FETCH_SCHOLAR_DOCUMENT_SUCCEEDED,
  FETCH_SCHOLAR_DOCUMENT_FAILED,
  FETCH_SCHOLAR_ORGNL_DOCUMENT_REQUESTED,
  FETCH_SCHOLAR_ORGNL_DOCUMENT_SUCCEEDED,
  FETCH_SCHOLAR_ORGNL_DOCUMENT_FAILED,

  //Upload Scholar Docs
  POST_UPLOAD_DOCUMENT_REQUESTED,
  POST_UPLOAD_DOCUMENT_SUCCEEDED,
  POST_UPLOAD_DOCUMENT_FAILED,

  //Disbursal Acknowledgement
  POST_DISBURSAL_ACK_REQUESTED,
  POST_DISBURSAL_ACK_SUCCEEDED,
  POST_DISBURSAL_ACK_FAILED

} from "./actions";
import { UPDATE_USER_RULES_REQUEST } from "../../constants/commonActions";
import gblFunc from "../../globals/globalFunctions";

const fetchUpdateUrl = input =>
  fetchClient
    .post(apiUrl.updateUser + `/${input.userid}/personalInfo`, input.params)
    .then(res => {
      return res.data;
    });

function* fetchUpdateUser(input) {
  try {
    const userList = yield call(fetchUpdateUrl, input.payload);

    yield put({
      type: FETCH_UPDATE_USER_SUCCEEDED,
      payload: userList
    });
  } catch (error) {
    yield put({
      type: FETCH_UPDATE_USER_FAILED,
      payload: error.errorMessage
    });
  }
}

/* Scholarship history sagas */

/* scholarshipHistory */

const scholarshipHistoryApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.scholarshipHistory}/${
    inputData.userid
    }/scholarshipHistory`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case apiCallTypes.POST:
      return fetchClient.post(url, inputData.params).then(res => {
        return res.data;
      });

    case apiCallTypes.DELETE:
      return fetchClient
        .delete(`${url}/${inputData.scholarshipHistoryId}`)
        .then(res => {
          return res.data;
        });
  }
};

function* scholarshipHistory(input) {
  try {
    switch (input.type) {
      case FETCH_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
        const scholarshipHistoryData = yield call(
          scholarshipHistoryApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS,
          payload: scholarshipHistoryData
        });

        break;

      case UPDATE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
        const updatedScholarshipHistoryData = yield call(
          scholarshipHistoryApi,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: UPDATE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS,
          payload: updatedScholarshipHistoryData
        });

        break;

      case DELETE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
        yield call(scholarshipHistoryApi, input.payload, apiCallTypes.DELETE);

        yield put({
          type: DELETE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
        yield put({
          type: FETCH_SCHOLARSHIP_HISTORY_DETAILS_FAILURE,
          payload: error
        });
        break;

      case UPDATE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
        yield put({
          type: UPDATE_SCHOLARSHIP_HISTORY_DETAILS_FAILURE,
          payload: error
        });
        break;

      case DELETE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
        yield put({
          type: DELETE_SCHOLARSHIP_HISTORY_DETAILS_FAILURE,
          payload: error
        });
        break;
    }
  }
}

/* Family Earning Sagas */
/* GET /user/{userId}/family */
const familyEarningDetailsApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.familyEarningDetails}/${inputData.userid}/family`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case apiCallTypes.POST:
      return fetchClient.post(url, inputData.params).then(res => {
        return res.data;
      });

    case apiCallTypes.DELETE:
      return fetchClient.delete(`${url}/${inputData.familyId}`).then(res => {
        return res.data;
      });
  }
};

function* familyEarningDetails(input) {
  try {
    switch (input.type) {
      case FETCH_FAMILY_EARNING_DETAILS_REQUEST:
        const userFamilyEarningData = yield call(
          familyEarningDetailsApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_FAMILY_EARNING_DETAILS_SUCCESS,
          payload: userFamilyEarningData
        });

        break;

      case UPDATE_FAMILY_EARNING_DETAILS_REQUEST:
        const updatedUserFamilyEarningData = yield call(
          familyEarningDetailsApi,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: UPDATE_FAMILY_EARNING_DETAILS_SUCCESS,
          payload: updatedUserFamilyEarningData
        });

        break;

      case DELETE_FAMILY_EARNING_DETAILS_REQUEST:
        const deletedFamilyData = yield call(
          familyEarningDetailsApi,
          input.payload,
          apiCallTypes.DELETE
        );

        yield put({
          type: DELETE_FAMILY_EARNING_DETAILS_SUCCESS,
          payload: deletedFamilyData
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_FAMILY_EARNING_DETAILS_REQUEST:
        yield put({
          type: FETCH_FAMILY_EARNING_DETAILS_FAILURE,
          payload: error
        });
        break;

      case UPDATE_FAMILY_EARNING_DETAILS_REQUEST:
        yield put({
          type: UPDATE_FAMILY_EARNING_DETAILS_FAILURE,
          payload: error
        });
        break;

      case DELETE_FAMILY_EARNING_DETAILS_REQUEST:
        yield put({
          type: DELETE_FAMILY_EARNING_DETAILS_FAILURE,
          payload: error
        });
        break;
    }
  }
}

/* References section saga */

const referenceDetailsApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.referenceDetail}/${inputData.userid}/reference`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case apiCallTypes.POST:
      return fetchClient.post(url, inputData.params).then(res => {
        return res.data;
      });

    case apiCallTypes.DELETE:
      return fetchClient.delete(`${url}/${inputData.referenceId}`).then(res => {
        return res.data;
      });
  }
};

function* referenceDetails(input) {
  try {
    switch (input.type) {
      case FETCH_REFERENCE__DETAILS_REQUEST:
        const referencesData = yield call(
          referenceDetailsApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_REFERENCE__DETAILS_SUCCESS,
          payload: referencesData
        });

        break;

      case UPDATE_REFERENCE__DETAILS_REQUEST:
        const updatedReferenceData = yield call(
          referenceDetailsApi,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: UPDATE_REFERENCE__DETAILS_SUCCESS,
          payload: updatedReferenceData
        });

        break;

      case DELETE_REFERENCE__DETAILS_REQUEST:
        yield call(referenceDetailsApi, input.payload, apiCallTypes.DELETE);

        yield put({
          type: DELETE_REFERENCE__DETAILS_SUCCESS
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_REFERENCE__DETAILS_REQUEST:
        yield put({
          type: FETCH_REFERENCE__DETAILS_FAILURE,
          payload: error
        });
        break;

      case UPDATE_REFERENCE__DETAILS_REQUEST:
        yield put({
          type: UPDATE_REFERENCE__DETAILS_FAILURE,
          payload: error
        });
        break;

      case DELETE_REFERENCE__DETAILS_REQUEST:
        yield put({
          type: DELETE_REFERENCE__DETAILS_FAILURE,
          payload: error
        });
        break;
    }
  }
}

/* Entrance examination section saga */
const entranceExaminationApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.entranceExamination}/${inputData.userid}/entranceExam`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case apiCallTypes.POST:
      return fetchClient.post(url, inputData.params).then(res => {
        return res.data;
      });

    case apiCallTypes.DELETE:
      return fetchClient
        .delete(`${url}/${inputData.entranceExamId}`)
        .then(res => {
          return res.data;
        });
  }
};

function* entranceExaminationDetails(input) {
  try {
    switch (input.type) {
      case FETCH_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST:
        const entranceExaminationData = yield call(
          entranceExaminationApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_ENTRANCE_EXAMINATIONS_DETAILS_SUCCESS,
          payload: entranceExaminationData
        });

        break;

      case UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST:
        const updatedEntranceExaminationData = yield call(
          entranceExaminationApi,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_SUCCESS,
          payload: updatedEntranceExaminationData
        });

        break;

      case DELETE_ENTRANCE_EXAMINATION_DETAILS_REQUEST:
        yield call(entranceExaminationApi, input.payload, apiCallTypes.DELETE);

        yield put({
          type: DELETE_ENTRANCE_EXAMINATION_DETAILS_SUCCESS
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST:
        yield put({
          type: FETCH_ENTRANCE_EXAMINATIONS_DETAILS_FAILURE,
          payload: error
        });
        break;

      case UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST:
        yield put({
          type: UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_FAILURE,
          payload: error
        });
        break;

      case DELETE_ENTRANCE_EXAMINATION_DETAILS_REQUEST:
        yield put({
          type: DELETE_ENTRANCE_EXAMINATION_DETAILS_FAILURE,
          payload: error
        });
        break;
    }
  }
}

/* Occupation type saga */

const occupationDataApi = inputData => {
  return fetchClient.get(apiUrl.occupationData).then(res => {
    return res.data;
  });
};

function* occupationData(input) {
  try {
    const occupationData = yield call(
      occupationDataApi,
      input.payload,
      apiCallTypes.GET
    );

    yield put({
      type: FETCH_OCCUPATION_DATA_SUCCESS,
      payload: occupationData
    });
  } catch (error) {
    yield put({
      type: FETCH_OCCUPATION_DATA_FAILURE,
      payload: error
    });
  }
}

/* Education type saga */

const educationDataApi = ({ inputData }, TYPE) => {
  const userId = localStorage.getItem("userId");
  const url = `${apiUrl.educationInfo}/${userId}/educationalInfo`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case apiCallTypes.POST:
      return fetchClient.post(url, inputData).then(res => {
        return res.data;
      });
  }
};

function* educationData(input) {
  try {
    switch (input.type) {
      case FETCH_EDUCATION_INFO_REQUEST:
        const educationData = yield call(
          educationDataApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_EDUCATION_INFO_SUCCESS,
          payload: educationData
        });
        break;
      case UPDATE_EDUCATION_INFO_REQUEST:
        const saveEducationData = yield call(
          educationDataApi,
          input.payload,
          apiCallTypes.POST
        );
        yield put({
          type: UPDATE_EDUCATION_INFO_SUCCESS,
          payload: saveEducationData
        });
        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_EDUCATION_INFO_FAILURE:
        yield put({
          type: FETCH_EDUCATION_INFO_FAILURE,
          payload: error
        });
        break;
      case UPDATE_EDUCATION_INFO_FAILURE:
        yield put({
          type: FETCH_EDUCATION_INFO_FAILURE,
          payload: error
        });
        break;
    }
  }
}

/* ADD STUDENT (MY SUBSCRIBER) */
//${apiUrl.studentDetails}
const studentDetailsApi = ({ inputData }, TYPE) => {
  switch (TYPE) {
    case FETCH_STUDENT_DETAILS_REQUEST:
      return fetchClient
        .get(
          `${apiUrl.studentDetails}/${inputData.userId}/student/${
          inputData.studentId
          }`
        )
        .then(res => {
          return res.data;
        });

    case FETCH_STUDENT_LIST_REQUEST:
      return fetchClient
        .get(
          `${apiUrl.studentDetails}/${inputData.userId}/student?page=${
          inputData.page
          }&length=${inputData.length}`
        )
        .then(res => {
          return res.data;
        });

    case UPDATE_STUDENT_REQUEST:
      return fetchClient
        .post(
          `${apiUrl.studentDetails}/${inputData.userId}/student`,
          inputData.params
        )
        .then(res => {
          return res.data;
        });
    case EDIT_STUDENT_DETAILS_REQUEST:
      return fetchClient
        .post(
          `${apiUrl.studentDetails}/${inputData.userId}/student/${
          inputData.studentId
          }`,
          inputData.params
        )
        .then(res => {
          return res.data;
        });
  }
};

function* studentDetails(input) {
  try {
    switch (input.type) {
      case FETCH_STUDENT_DETAILS_REQUEST:
        const fetchStudentData = yield call(
          studentDetailsApi,
          input.payload,
          input.type
        );

        yield put({
          type: FETCH_STUDENT_DETAILS_SUCCESS,
          payload: fetchStudentData
        });

        break;

      case FETCH_STUDENT_LIST_REQUEST:
        const fetchStudentList = yield call(
          studentDetailsApi,
          input.payload,
          input.type
        );

        yield put({
          type: FETCH_STUDENT_LIST_SUCCESS,
          payload: fetchStudentList
        });

        break;

      case UPDATE_STUDENT_REQUEST:
        const updatedStudentData = yield call(
          studentDetailsApi,
          input.payload,
          input.type
        );

        yield put({
          type: UPDATE_STUDENT_SUCCESS,
          payload: updatedStudentData
        });

        break;
      case EDIT_STUDENT_DETAILS_REQUEST:
        const editStudentData = yield call(
          studentDetailsApi,
          input.payload,
          input.type
        );

        yield put({
          type: EDIT_STUDENT_DETAILS_SUCCESS,
          payload: editStudentData
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_STUDENT_DETAILS_REQUEST:
        yield put({
          type: FETCH_STUDENT_DETAILS_FAIL,
          payload: error
        });
        break;
      case FETCH_STUDENT_LIST_REQUEST:
        yield put({
          type: FETCH_STUDENT_LIST_FAIL,
          payload: error
        });
        break;

      case UPDATE_STUDENT_REQUEST:
        if (error.response && error.response.data.errorCode === 702) {
          yield put({
            type: UPDATE_STUDENT_FAIL,
            payload: {
              errorMessage: error.response.data.message,
              isServerError: true
            }
          });
        } else {
          yield put({
            type: UPDATE_STUDENT_FAIL,
            payload: {
              errorMessage: error.errorMessage,
              isServerError: false
            }
          });
        }
        break;
      case EDIT_STUDENT_DETAILS_REQUEST:
        yield put({
          type: EDIT_STUDENT_DETAILS_FAIL,
          payload: error
        });
        break;
    }
  }
}

/* MATCHING SCHOLARSHIPS */
const matchingscholarhipApi = ({ inputData }, TYPE) => {
  let url = `${apiUrl.matchingScholarships}/recommendation`;

  if (inputData.pool) {
    url = url + "?pool=assisted";
  }
  const { type, Rule_Type, userId } = inputData;

  switch (TYPE) {
    case FETCH_MATCHING_SCHOLARSHIPS_REQUEST:
      return fetchClient.post(url, { type, Rule_Type, userId }).then(res => {
        return res.data;
      });
  }
};

function* matchingScholarshipsDt(input) {
  try {
    switch (input.type) {
      case FETCH_MATCHING_SCHOLARSHIPS_REQUEST:
        const fetchMatchScholarships = yield call(
          matchingscholarhipApi,
          input.payload,
          input.type
        );

        yield put({
          type: FETCH_MATCHING_SCHOLARSHIPS_SUCCESS,
          payload: fetchMatchScholarships
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_MATCHING_SCHOLARSHIPS_REQUEST:
        yield put({
          type: FETCH_MATCHING_SCHOLARSHIPS_FAIL,
          payload: error
        });
        break;
    }
  }
}

/* FAVORITE SCHOLARSHIPS */
const favScholarhipApi = ({ inputData }, TYPE) => {
  switch (TYPE) {
    case FETCH_MY_FAVORITE_REQUEST:
      return fetchClient
        .get(`${apiUrl.favScholarships}/${inputData.userId}/favscholarship`)
        .then(res => {
          return res.data;
        });
    case ADD_TO_FAV_REQUEST:
      return fetchClient
        .post(
          `${apiUrl.favScholarships}/${inputData.userId}/favscholarship/${
          inputData.scholarshipId
          }`
        )
        .then(res => {
          return res.data;
        });
    case DELETE_FAV_REQUEST:
      return fetchClient
        .delete(
          `${apiUrl.favScholarships}/${inputData.userId}/favscholarship/${
          inputData.scholarshipId
          }`
        )
        .then(res => {
          return res.data;
        });
    case FETCH_FAV_PERSIST_REQUEST:
      return fetchClient
        .get(
          `${apiUrl.favScholarships}/${inputData.userId}/favscholarship/${
          inputData.nid
          }`
        )
        .then(res => {
          return res.data;
        });
  }
};

function* favScholarships(input) {
  try {
    switch (input.type) {
      case FETCH_MY_FAVORITE_REQUEST:
        const fetchFavScholarships = yield call(
          favScholarhipApi,
          input.payload,
          input.type
        );

        yield put({
          type: FETCH_MY_FAVORITE_SUCCESS,
          payload: fetchFavScholarships
        });

        break;
      case ADD_TO_FAV_REQUEST:
        const addFavScholarships = yield call(
          favScholarhipApi,
          input.payload,
          input.type
        );

        yield put({
          type: ADD_TO_FAV_SUCCESS,
          payload: addFavScholarships
        });

        break;
      case DELETE_FAV_REQUEST:
        const deleteFavScholarships = yield call(
          favScholarhipApi,
          input.payload,
          input.type
        );

        yield put({
          type: DELETE_FAV_SUCCESS,
          payload: deleteFavScholarships
        });

        break;
      case FETCH_FAV_PERSIST_REQUEST:
        const favPersistData = yield call(
          favScholarhipApi,
          input.payload,
          input.type
        );

        yield put({
          type: FETCH_FAV_PERSIST_SUCCESS,
          payload: favPersistData
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_MY_FAVORITE_REQUEST:
        yield put({
          type: FETCH_MY_FAVORITE_FAIL,
          payload: error
        });
        break;
      case ADD_TO_FAV_REQUEST:
        yield put({
          type: ADD_TO_FAV_FAIL,
          payload: error.errorMessage
        });
        break;
      case DELETE_FAV_REQUEST:
        yield put({
          type: DELETE_FAV_FAIL,
          payload: error
        });
        break;
      case FETCH_FAV_PERSIST_REQUEST:
        yield put({
          type: FETCH_FAV_PERSIST_FAIL,
          payload: error
        });
        break;
    }
  }
}

/* APPLICATION STATUS */

const fetchApplStatusApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.applicationStatus}/${inputData.userId}/scholarships`;
  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });
  }
};

function* fetchApplicationStatus(input) {
  try {
    switch (input.type) {
      case FETCH_APPLICATION_STATUS_REQUEST:
        const fetchApplicationStatus = yield call(
          fetchApplStatusApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_APPLICATION_STATUS_SUCCESS,
          payload: fetchApplicationStatus
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_APPLICATION_STATUS_REQUEST:
        yield put({
          type: FETCH_APPLICATION_STATUS_FAIL,
          payload: error
        });
        break;
    }
  }
}

/* Application status doc list */
const fetchApplStatusDocApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.applicationStatusDoc}/${inputData.userId}/applications?awardee=false`;
  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });
  }
};

function* fetchApplicationStatusDoc(input) {
  try {
    switch (input.type) {
      case FETCH_APPLICATION_STATUS_DOC_REQUEST:
        const fetchApplicationStatusDoc = yield call(
          fetchApplStatusDocApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_APPLICATION_STATUS_DOC_SUCCESS,
          payload: fetchApplicationStatusDoc
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_APPLICATION_STATUS_DOC_REQUEST:
        yield put({
          type: FETCH_APPLICATION_STATUS_DOC_FAIL,
          payload: error
        });
        break;
    }
  }
}

const fetchDocumentIssueApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.docIssue}/${inputData.userId}/applicationDoc/${inputData.scholarshipId}?includeGlobal=false&documentIssue=true`;
  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });
  }
};

function* fetchDocumentIssue(input) {
  try {
    switch (input.type) {
      case FETCH_DOCUMENT_ISSUE_REQUEST:
        const fetchApplicationStatus = yield call(
          fetchDocumentIssueApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_DOCUMENT_ISSUE_SUCCESS,
          payload: fetchApplicationStatus
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_DOCUMENT_ISSUE_REQUEST:
        yield put({
          type: FETCH_DOCUMENT_ISSUE_FAIL,
          payload: error
        });
        break;
    }
  }
}

const sendDocumentIssueUploadApi = payload => {
  let dataParams = new FormData();
  dataParams.append("docFile", payload.inputData.docFile);
  dataParams.append("userDocRequest", payload.inputData.userDocRequest);
if(payload.inputData.disbursementCycleId){
  return fetchClient
    .post(
      `${apiUrl.educationInfo}/${payload.inputData.userId}/scholarship/${
      payload.inputData.scholarshipId}/disbursal/${
      payload.inputData.disbursementCycleId}/document?userDocRequest=${encodeURIComponent(payload.inputData.userDocRequest)}`,
      dataParams,
      {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }
    )
    .then(res => res.data);
}else{
  return fetchClient
    .post(
      `${apiUrl.educationInfo}/${payload.inputData.userId}/applicationDoc/${
      payload.inputData.scholarshipId}`,
      dataParams,
      {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }
    )
    .then(res => res.data);
}
};

function* sendDocumentIssueUpload(input) {
  try {
    switch (input.type) {
      case FETCH_DOCUMENT_ISSUE_UPLOAD_REQUEST:
        const fetchApplicationStatus = yield call(
          sendDocumentIssueUploadApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_DOCUMENT_ISSUE_UPLOAD_SUCCESS,
          payload: fetchApplicationStatus
        });
        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_DOCUMENT_ISSUE_UPLOAD_REQUEST:
        yield put({
          type: FETCH_DOCUMENT_ISSUE_UPLOAD_FAILED,
          payload: error
        });
        break;
    }
  }
}

/* Awared scholarship doc list */
const fetchAwardeeDocApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.awardeeDocApi}/${inputData.userId}/applications?awardee=true`;
  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });
  }
};

function* fetchAwardeeDoc(input) {
  try {
    switch (input.type) {
      case FETCH_AWARDEE_DOC_REQUEST:
        const awardeeDocData = yield call(
          fetchAwardeeDocApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_AWARDEE_DOC_SUCCESS,
          payload: awardeeDocData
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_AWARDEE_DOC_REQUEST:
        yield put({
          type: FETCH_AWARDEE_DOC_FAIL,
          payload: error
        });
        break;
    }
  }
}

/* Disbursal Acknowledgement */
const disbursalAcknowledgementApi = ({ inputData }, TYPE) => {
  switch (TYPE) {
    case POST_DISBURSAL_ACK_REQUESTED:
      return fetchClient
        .post(
          `${apiUrl.disbursalAcknowledgement}${inputData.userId}/disbursal/${
          inputData.disbursementCycleId}/payment/acknowledgement`, {
          userId: inputData.userId, disbursementCycleId: inputData.disbursementCycleId
        }
        )
        .then(res => {
          return res.data;
        });
  }
};

function* disbursalAcknowledgementPost(input) {
  try {
    switch (input.type) {
      case POST_DISBURSAL_ACK_REQUESTED:
        const disbursalAcknowledgement = yield call(
          disbursalAcknowledgementApi,
          input.payload,
          input.type
        );

        yield put({
          type: POST_DISBURSAL_ACK_SUCCEEDED,
          payload: disbursalAcknowledgement
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case POST_DISBURSAL_ACK_REQUESTED:
        yield put({
          type: POST_DISBURSAL_ACK_FAILED,
          payload: error.errorMessage
        });
        break;
    }
  }
}


const fetchDashQnaApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.qnaDash}/${inputData.userId}/?page=${inputData.page}&length=${inputData.length}`;
  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });
  }
};

function* fetchDashQna(input) {
  try {
    switch (input.type) {
      case FETCH_QNA_DASHBOARD_REQUEST:
        const fetchApplicationStatus = yield call(
          fetchDashQnaApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_QNA_DASHBOARD_SUCCESS,
          payload: fetchApplicationStatus
        });
        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_QNA_DASHBOARD_REQUEST:
        yield put({
          type: FETCH_QNA_DASHBOARD_FAILED,
          payload: error
        });
        break;
    }
  }
}

const fetchQnaNotificationApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.qnaNotification}/user/${inputData.userId}/qna-question-answer/notification/?page=${inputData.page}&length=${inputData.length}`;
  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });
  }
};

function* fetchQnaNotification(input) {
  try {
    switch (input.type) {
      case FETCH_QNA_NOTIFICATION_REQUEST:
        const fetchApplicationStatus = yield call(
          fetchQnaNotificationApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_QNA_NOTIFICATION_SUCCESS,
          payload: fetchApplicationStatus
        });
        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_QNA_NOTIFICATION_REQUEST:
        yield put({
          type: FETCH_QNA_NOTIFICATION_FAILED,
          payload: error
        });
        break;
    }
  }
}

/* Upload User Pic Saga */

const uploadUserPicApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.uploadUserPic}/${inputData.userId}/pic`;
  switch (TYPE) {
    case apiCallTypes.POST:
      return fetchClient
        .post(url, inputData.formData, {
          headers: { "Content-Type": "multipart/form-data" }
        })
        .then(res => {
          return res.data;
        });
  }
};

function* uploadUserPic(input) {
  try {
    switch (input.type) {
      case UPLOAD_USER_PIC_REQUEST:
        const uploadUserPicData = yield call(
          uploadUserPicApi,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: UPLOAD_USER_PIC_SUCCESS,
          payload: uploadUserPicData
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case UPLOAD_USER_PIC_REQUEST:
        yield put({
          type: UPLOAD_USER_PIC_FAIL,
          payload: error
        });
        break;
    }
  }
}
/* Book Slot */
const fetchBookSlotApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.slotBook}/${inputData.scholarshipId}/user/${
    inputData.userId
    }/slot`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case apiCallTypes.POST:
      return fetchClient
        .post(
          `${apiUrl.slotBook}/${inputData.scholarshipId}/user/${
          inputData.userId
          }/slot`,
          inputData.book_slot
        )
        .then(res => {
          return res.data;
        });
  }
};

function* fetchBookSlot(input) {
  try {
    switch (input.type) {
      case FETCH_BOOK_SLOT_REQUEST:
        const fetchBookSlotData = yield call(
          fetchBookSlotApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_BOOK_SLOT_SUCCESS,
          payload: fetchBookSlotData
        });

        break;
      case UPDATE_BOOK_SLOT_REQUEST:
        const updateBookSlotData = yield call(
          fetchBookSlotApi,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: UPDATE_BOOK_SLOT_SUCCESS,
          payload: updateBookSlotData
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_BOOK_SLOT_REQUEST:
        yield put({
          type: FETCH_BOOK_SLOT_FAIL,
          payload: error.errorMessage
        });
        break;

      case UPDATE_BOOK_SLOT_REQUEST:
        yield put({
          type: UPDATE_BOOK_SLOT_FAIL,
          payload: error.errorMessage
        });
        break;
    }
  }
}

/* PREFERRED LANGUAGE */
const fetchPreferredLangApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.preferredLanguages}`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });
  }
};

function* fetchPreferredLang(input) {
  try {
    switch (input.type) {
      case FETCH_PREFERRED_LANG_REQUEST:
        const fetchPreferredLangData = yield call(
          fetchPreferredLangApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_PREFERRED_LANG_SUCCESS,
          payload: fetchPreferredLangData
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_PREFERRED_LANG_REQUEST:
        yield put({
          type: FETCH_PREFERRED_LANG_FAIL,
          payload: error
        });
        break;
    }
  }
}

/* CSC PAYMENT DETAILS */
//
const fetchUserCscPaymt = ({ inputData }, TYPE) => {
  const url = `${apiUrl.cscUserPayment}`;

  switch (TYPE) {
    case apiCallTypes.POST:
      return fetchClient.post(url, inputData).then(res => {
        return res.data;
      });
  }
};

function* fetchCscPaymentDetails(input) {
  try {
    switch (input.type) {
      case FETCH_CSC_PAYMENT_DETAILS_REQUEST:
        const fetchCscPayment = yield call(
          fetchUserCscPaymt,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: FETCH_CSC_PAYMENT_DETAILS_SUCCESS,
          payload: fetchCscPayment
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_CSC_PAYMENT_DETAILS_REQUEST:
        yield put({
          type: FETCH_CSC_PAYMENT_DETAILS_FAIL,
          payload: error
        });
        break;
    }
  }
}

/* CSC REDIRECT */
const fetchRedirectCscAPI = ({ inputData }, TYPE) => {
  switch (TYPE) {
    case apiCallTypes.POST:
      return axios
        .post(inputData.url, inputData.formData)
        .then(res => res.data);
  }
};

function* fetchRedirectCsc(input) {
  try {
    switch (input.type) {
      case FETCH_CSC_REDIRECT_REQUEST:
        const fetchCscRedirect = yield call(
          fetchRedirectCscAPI,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: FETCH_CSC_REDIRECT_SUCCESS,
          payload: fetchCscRedirect
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_CSC_REDIRECT_REQUEST:
        yield put({
          type: FETCH_CSC_REDIRECT_FAIL,
          payload: error
        });
        break;
    }
  }
}
/* User bank details saga */

const userBankDetailsApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.entranceExamination}/${inputData.userid}/bankDetail`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case apiCallTypes.POST:
      return fetchClient.post(url, inputData.params).then(res => {
        return res.data;
      });
  }
};

function* userBankDetails(input) {
  try {
    switch (input.type) {
      case FETCH_USER_BANK_DETAILS_REQUEST:
        const userBankDetailsData = yield call(
          userBankDetailsApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_USER_BANK_DETAILS_SUCCESS,
          payload: userBankDetailsData
        });

        break;

      case UPDATE_USER_BANK_DETAILS_REQUEST:
        const updatedUserBankDetailsData = yield call(
          userBankDetailsApi,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: UPDATE_USER_BANK_DETAILS_SUCCESS,
          payload: updatedUserBankDetailsData
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_USER_BANK_DETAILS_REQUEST:
        yield put({
          type: FETCH_USER_BANK_DETAILS_FAILURE,
          payload: error
        });
        break;

      case UPDATE_USER_BANK_DETAILS_REQUEST:
        yield put({
          type: UPDATE_USER_BANK_DETAILS_FAILURE,
          payload: error
        });
        break;
    }
  }
}

/* Document types section */

//All document groups and types
const getDocumentGroupsAndTypesApi = inputData =>
  fetchClient.get(apiUrl.documentGroupsAndTypes).then(res => res.data);

function* documentGroupsAndTypes(input) {
  try {
    const documentGroupsAndTypes = yield call(getDocumentGroupsAndTypesApi);

    yield put({
      type: FETCH_DOCUMENT_TYPES_SUCCESS,
      payload: documentGroupsAndTypes
    });
  } catch (error) {
    yield put({
      type: FETCH_DOCUMENT_TYPES_FAILURE,
      payload: error.response
    });
  }
}

// Document type categories

const getDocumentTypeCategoriesApi = ({ inputData }) =>
  fetchClient
    .get(`${apiUrl.documentTypeCategories}/${inputData.documentTypeId}`)
    .then(res => res.data);

function* documentTypeCategories(input) {
  try {
    const documentTypeCategories = yield call(
      getDocumentTypeCategoriesApi,
      input.payload
    );

    yield put({
      type: FETCH_DOCUMENT_TYPES_CATEGORY_SUCCESS,
      payload: documentTypeCategories
    });
  } catch (error) {
    yield put({
      type: FETCH_DOCUMENT_TYPES_CATEGORY_FAILURE,
      payload: error.response
    });
  }
}

// Document type category options

const getDocumentTypeCategoryOptionsApi = ({ inputData }) =>
  fetchClient
    .get(`${apiUrl.documentTypeCategoryOptions}/${inputData.categoryId}`)
    .then(res => res.data);

function* documentTypeCategoryOptions(input) {
  try {
    const documentTypeCategoriesOptions = yield call(
      getDocumentTypeCategoryOptionsApi,
      input.payload
    );

    yield put({
      type: FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_SUCCESS,
      payload: documentTypeCategoriesOptions
    });
  } catch (error) {
    yield put({
      type: FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_FAILURE,
      payload: error.response
    });
  }
}

// User documents update/delete

/* Documents saga */
const userDocumentsApi = ({ inputData }, TYPE) => {
  // const url = `
  // http://192.168.0.154:8887/${inputData.userid}/documents`;
  const url = `${apiUrl.documents}/${inputData.userid}/document`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case apiCallTypes.POST:
      const {
        documentType,
        documentTypeCategory,
        documentTypeCategoryOption,
        docFile
      } = inputData.params;

      const stringifiedData = JSON.stringify({
        documentType,
        documentTypeCategory,
        documentTypeCategoryOption
      });

      let data = new FormData();
      data.append("docFile", docFile);
      data.append("userDocRequest", stringifiedData);

      return fetchClient
        .post(url, data, {
          headers: {
            "Content-Type": "multipart/form-data"
          }
        })
        .then(res => res.data);

      break;

    case apiCallTypes.DELETE:
      return fetchClient.delete(`${url}/${inputData.documentId}`).then(res => {
        return res.data;
      });
  }
};

function* documents(input) {
  try {
    switch (input.type) {
      case FETCH_USER_DOCUMENTS_REQUEST:
        const userDocumentsData = yield call(
          userDocumentsApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_USER_DOCUMENTS_SUCCESS,
          payload: userDocumentsData
        });

        break;

      case UPDATE_USER_DOCUMENTS_REQUEST:
        const updatedUserDocuments = yield call(
          userDocumentsApi,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: UPDATE_USER_DOCUMENTS_SUCCESS,
          payload: updatedUserDocuments
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_USER_DOCUMENTS_REQUEST:
        yield put({
          type: FETCH_USER_DOCUMENTS_FAILURE,
          payload: error
        });
        break;

      case UPDATE_USER_DOCUMENTS_REQUEST:
        yield put({
          type: UPDATE_USER_DOCUMENTS_FAILURE,
          payload: error
        });
        break;
    }
  }
}

const fetchDMDCListApi = ({ stateId, districtId }) => {
  const url = `${
    apiUrl.dmdcList
    }/state/${stateId}/district/${districtId}/vle/dmdc`;

  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchDMDCList(input) {
  try {
    const userList = yield call(fetchDMDCListApi, input.payload.inputData);

    yield put({
      type: FETCH_DM_DC_SUCCESS,
      payload: userList
    });
  } catch (error) {
    yield put({
      type: FETCH_DM_DC_FAIL,
      payload: error.errorMessage
    });
  }
}

// save dmdc

function saveDMDCAPI({ userId, data }) {
  const url = `${apiUrl.dmdcApi}/${userId}/vle/dmdc`;

  return fetchClient.post(url, data).then(res => {
    return res.data;
  });
}
function* saveDMDC(input) {
  try {
    const save_dmdc = yield call(saveDMDCAPI, input.payload.inputData);

    yield put({
      type: UPLOAD_DMDC_SUCCESS,
      payload: save_dmdc
    });
  } catch (error) {
    yield put({
      type: UPLOAD_DMDC_FAIL,
      payload: error.errorMessage
    });
  }
}

// Fetch Book Slot Dates

function bookSlotDateAPI({ inputData }) {
  return fetchClient
    .get(`${apiUrl.bookSlotDate}/${inputData}/slot`)
    .then(res => {
      return res.data;
    });
}

function* fetchBookSlotDate({ payload }) {
  try {
    const bookSlot = yield call(bookSlotDateAPI, payload);
    yield put({
      type: FETCH_BOOK_SLOT_DATES_SUCCESS,
      payload: bookSlot
    });
  } catch (error) {
    yield put({
      type: FETCH_BOOK_SLOT_DATES_FAIL,
      payload: error.errorMessage
    });
  }
}

/* Scholar */
/* Scholarship Won Sagas -  GET /user/{userId}/scholarship-won */
const fetchScholarScholarshipWonUrl = ({ inputData }, TYPE) => {
  const url = `${apiUrl.scholarApi}/${inputData.userId}/scholarship-won`;
  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });
  }
};

function* fetchScholarScholarshipWon(input) {
  try {
    switch (input.type) {
      case FETCH_SCHOLAR_SCHOLARSHIP_WON_REQUEST:
        const slugList = yield call(
          fetchScholarScholarshipWonUrl,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_SCHOLAR_SCHOLARSHIP_WON_SUCCESS,
          payload: slugList
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_SCHOLAR_SCHOLARSHIP_WON_REQUEST:
        yield put({
          type: FETCH_SCHOLAR_SCHOLARSHIP_WON_FAIL,
          payload: error
        });
        break;
    }
  }
}

/* Scholarship Won Certificate Sagas -  GET /user/{userId}/scholarship/{scholarshipId} */
const fetchScholarshipCertificatePdfUrl = ({ inputData }, TYPE) => {
  const url = `${apiUrl.scholarApi}/${inputData.userId}/scholarship/${
    inputData.scholarshipId
    }/certificate`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });
  }
};

function* fetchScholarshipCertificatePdf(input) {
  try {
    switch (input.type) {
      case FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_REQUEST:
        const slugList = yield call(
          fetchScholarshipCertificatePdfUrl,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_SUCCESS,
          payload: slugList
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_REQUEST:
        yield put({
          type: FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_FAIL,
          payload: error
        });
        break;
    }
  }
}
function fetchDisbursalApi() {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .get(`${apiUrl.dmdcApi}/${userId}/scholarship/disbursal`)
    .then(res => res.data);
}
function* fetchDisbursal({ payload }) {
  try {
    const res = yield call(fetchDisbursalApi, payload);
    yield put({
      type: FETCH_USER_DISBURSAL_SUCCEEDED,
      payload: res
    });
  } catch (err) {
    yield put({
      type: FETCH_USER_DISBURSAL_FAILED,
      payload: err
    });
  }
}

function fetchDisbursalIdApi(payload) {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .get(
      `${apiUrl.dmdcApi}/${userId}/scholarship/${
      payload.scholarshipId
      }/disbursal/document`
    )
    .then(res => res.data);
}

function* fetchDisbursalId({ payload }) {
  try {
    const res = yield call(fetchDisbursalIdApi, payload);
    yield put({
      type: FETCH_USER_DISBURSALID_SUCCEEDED,
      payload: res
    });
  } catch (err) {
    yield put({
      type: FETCH_USER_DISBURSALID_FAILED,
      payload: err
    });
  }
}

function fetchScholarDocsApi(payload) {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .get(
      `${apiUrl.dmdcApi}/${userId}/disburse-cycle/${
      payload.disbursalCycleId
      }/document`
    )
    .then(res => res.data);
}

function* fetchScholarDocs({ payload }) {
  try {
    const res = yield call(fetchScholarDocsApi, payload);
    yield put({
      type: FETCH_SCHOLAR_DOCUMENT_SUCCEEDED,
      payload: res
    });
  } catch (err) {
    yield put({
      type: FETCH_SCHOLAR_DOCUMENT_FAILED,
      payload: err
    });
  }
}

function uploadScholarDocsAPI(payload) {
  const userId = localStorage.getItem("userId");
  let dataParams = new FormData();
  dataParams.append("docFile", payload.docFile);
  dataParams.append("userDocRequest", payload.userDocRequest);
  return fetchClient
    .post(
      `${apiUrl.educationInfo}/${userId}/scholarship/${
      payload.scholarshipId
      }/disbursal/${payload.disbursalId}/document`,
      dataParams,
      {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }
    )
    .then(res => res.data);
}

function* uploadScholarDocs({ payload }) {
  try {
    const res = yield call(uploadScholarDocsAPI, payload);

    yield put({
      type: POST_UPLOAD_DOCUMENT_SUCCEEDED,
      payload: res
    });
  } catch (error) {
    yield put({
      type: POST_UPLOAD_DOCUMENT_FAILED,
      payload: error
    });
  }
}

function getScholarOrgnlDocsApi(payload) {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .get(
      `${apiUrl.dmdcApi}/${userId}/scholarship/${
      payload.scholarshipId
      }/document`
    )
    .then(res => res.data);
}

function* getScholarOrgnlDocs({ payload }) {
  try {
    const res = yield call(getScholarOrgnlDocsApi, payload);
    yield put({
      type: FETCH_SCHOLAR_ORGNL_DOCUMENT_SUCCEEDED,
      payload: res
    });
  } catch (err) {
    yield put({
      type: FETCH_SCHOLAR_ORGNL_DOCUMENT_FAILED,
      payload: err
    });
  }
}

function postUserDocsApi(inputData) {
  const {
    documentType,
    documentTypeCategory,
    documentTypeCategoryOption,
    docFile
  } = inputData.params;

  const stringifiedData = JSON.stringify({
    documentType,
    documentTypeCategory,
    documentTypeCategoryOption
  });

  let data = new FormData();
  data.append("docFile", docFile);
  data.append("userDocRequest", stringifiedData);

  const userId = localStorage.getItem("userId");
  return fetchClient
    .post(`${apiUrl.documents}/${userId}/documents`, data)
    .then(res => res.data);
}

function* postUserDocs({ payload }) {
  try {
    const res = yield call(postUserDocsApi, payload);
    yield put({
      type: POST_UPLOAD_USER_DOCUMENT_SUCCEEDED,
      payload: res
    });
  } catch (err) {
    yield put({
      type: POST_UPLOAD_USER_DOCUMENT_FAILED,
      payload: err
    });
  }
}

function userDeleteDocumentsApi({ inputData }) {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .delete(`${apiUrl.documents}/${userId}/documents/${inputData.documentId}`)
    .then(res => res.data);
}
function* deleteUserDocs({ payload }) {
  try {
    const res = yield call(userDeleteDocumentsApi, payload);
    yield put({
      type: DELETE_USER_DOCUMENTS_SUCCESS,
      payload: res
    });
  } catch (err) {
    yield put({
      type: DELETE_USER_DOCUMENTS_FAILURE,
      payload: err
    });
  }
}

export default function* userProfileSaga() {
  yield takeEvery(FETCH_UPDATE_USER_REQUESTED, fetchUpdateUser);
  yield takeEvery(POST_UPLOAD_USER_DOCUMENT_REQUESTED, postUserDocs);

  /* Scholarship History */
  yield takeEvery(
    FETCH_SCHOLARSHIP_HISTORY_DETAILS_REQUEST,
    scholarshipHistory
  );
  yield takeEvery(
    UPDATE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST,
    scholarshipHistory
  );
  yield takeEvery(
    DELETE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST,
    scholarshipHistory
  );

  /* Family  Earning */
  yield takeEvery(FETCH_FAMILY_EARNING_DETAILS_REQUEST, familyEarningDetails);
  yield takeEvery(UPDATE_FAMILY_EARNING_DETAILS_REQUEST, familyEarningDetails);
  yield takeEvery(DELETE_FAMILY_EARNING_DETAILS_REQUEST, familyEarningDetails);

  /* Reference  Details */
  yield takeEvery(FETCH_REFERENCE__DETAILS_REQUEST, referenceDetails);
  yield takeEvery(UPDATE_REFERENCE__DETAILS_REQUEST, referenceDetails);
  yield takeEvery(DELETE_REFERENCE__DETAILS_REQUEST, referenceDetails);

  /* Entrance examination details */
  yield takeEvery(
    FETCH_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST,
    entranceExaminationDetails
  );
  yield takeEvery(
    UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST,
    entranceExaminationDetails
  );
  yield takeEvery(
    DELETE_ENTRANCE_EXAMINATION_DETAILS_REQUEST,
    entranceExaminationDetails
  );

  /* Occupation data */
  yield takeEvery(FETCH_OCCUPATION_DATA_REQUEST, occupationData);

  /* Bank details action */
  yield takeEvery(FETCH_USER_BANK_DETAILS_REQUEST, userBankDetails);
  yield takeEvery(UPDATE_USER_BANK_DETAILS_REQUEST, userBankDetails);
  /* Education data */
  yield takeEvery(FETCH_EDUCATION_INFO_REQUEST, educationData);
  yield takeEvery(UPDATE_EDUCATION_INFO_REQUEST, educationData);

  /* Add Student (My Subscriber) */
  yield takeEvery(UPDATE_STUDENT_REQUEST, studentDetails);
  yield takeEvery(FETCH_STUDENT_DETAILS_REQUEST, studentDetails);
  yield takeEvery(EDIT_STUDENT_DETAILS_REQUEST, studentDetails);
  yield takeEvery(FETCH_STUDENT_LIST_REQUEST, studentDetails);

  /* Matching Scholarships (My Scholarships)*/

  yield takeEvery(FETCH_MATCHING_SCHOLARSHIPS_REQUEST, matchingScholarshipsDt);

  /* My Favorite Scholarships */

  yield takeEvery(FETCH_MY_FAVORITE_REQUEST, favScholarships);

  /* Add To Fav */
  yield takeEvery(ADD_TO_FAV_REQUEST, favScholarships);

  /* Delete Favs */
  yield takeEvery(DELETE_FAV_REQUEST, favScholarships);

  /* Persist fav  */
  yield takeEvery(FETCH_FAV_PERSIST_REQUEST, favScholarships);

  // /* Documents form actions */
  yield takeEvery(FETCH_USER_DOCUMENTS_REQUEST, documents);
  yield takeEvery(UPDATE_USER_DOCUMENTS_REQUEST, documents);
  yield takeEvery(DELETE_USER_DOCUMENTS_REQUEST, deleteUserDocs);

  // Document categories actions
  yield takeEvery(FETCH_DOCUMENT_TYPES_REQUEST, documentGroupsAndTypes);

  yield takeEvery(
    FETCH_DOCUMENT_TYPES_CATEGORY_REQUEST,
    documentTypeCategories
  );

  yield takeEvery(
    FETCH_DOCUMENT_TYPE_CATEGORY_OPTIONS_REQUEST,
    documentTypeCategoryOptions
  );

  // Application Status
  yield takeEvery(FETCH_APPLICATION_STATUS_REQUEST, fetchApplicationStatus);
  yield takeEvery(FETCH_APPLICATION_STATUS_DOC_REQUEST, fetchApplicationStatusDoc);

  yield takeEvery(FETCH_AWARDEE_DOC_REQUEST, fetchAwardeeDoc);

  yield takeEvery(FETCH_DOCUMENT_ISSUE_REQUEST, fetchDocumentIssue);
  yield takeEvery(FETCH_DOCUMENT_ISSUE_UPLOAD_REQUEST, sendDocumentIssueUpload);

  yield takeEvery(FETCH_QNA_DASHBOARD_REQUEST, fetchDashQna);

  yield takeEvery(FETCH_QNA_NOTIFICATION_REQUEST, fetchQnaNotification);

  // User Upload Pic
  yield takeEvery(UPLOAD_USER_PIC_REQUEST, uploadUserPic);

  // Get Book Slot
  yield takeEvery(FETCH_BOOK_SLOT_REQUEST, fetchBookSlot);

  // update book slot
  yield takeEvery(UPDATE_BOOK_SLOT_REQUEST, fetchBookSlot);

  // fetch preferred lang
  yield takeEvery(FETCH_PREFERRED_LANG_REQUEST, fetchPreferredLang);

  //yield takeEvery(FETCH_DO);

  // fetch csc payment details
  yield takeEvery(FETCH_CSC_PAYMENT_DETAILS_REQUEST, fetchCscPaymentDetails);

  // csc redirect
  yield takeEvery(FETCH_CSC_REDIRECT_REQUEST, fetchRedirectCsc);

  // DM / DC
  yield takeEvery(FETCH_DM_DC_REQUEST, fetchDMDCList);

  // save dm / dc
  yield takeEvery(UPLOAD_DMDC_REQUEST, saveDMDC);

  // Fetch Book Slot Dates
  yield takeEvery(FETCH_BOOK_SLOT_DATES_REQUEST, fetchBookSlotDate);

  // Scholar Scholarship Won
  yield takeEvery(
    FETCH_SCHOLAR_SCHOLARSHIP_WON_REQUEST,
    fetchScholarScholarshipWon
  );

  // Scholar Scholarship Certificate Pdf
  yield takeEvery(
    FETCH_SCHOLAR_SCHOLARSHIP_CERTIFICATE_PDF_REQUEST,
    fetchScholarshipCertificatePdf
  );

  // Scholar Disbursal
  yield takeEvery(FETCH_USER_DISBURSAL_REQUESTED, fetchDisbursal);

  //Scholar disbursal id
  yield takeEvery(FETCH_USER_DISBURSALID_REQUESTED, fetchDisbursalId);

  //Scholar documents
  yield takeEvery(FETCH_SCHOLAR_DOCUMENT_REQUESTED, fetchScholarDocs);

  //Upload Scholar documents
  yield takeEvery(POST_UPLOAD_DOCUMENT_REQUESTED, uploadScholarDocs);
  yield takeEvery(FETCH_SCHOLAR_ORGNL_DOCUMENT_REQUESTED, getScholarOrgnlDocs);

  //Disbursal Acknowledgement
  yield takeEvery(POST_DISBURSAL_ACK_REQUESTED, disbursalAcknowledgementPost);



}
