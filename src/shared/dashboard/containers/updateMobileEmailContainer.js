import { connect } from "react-redux";
import updateMobEmail from '../components/profile-info/updateMobEmail';
import {
	/* fetchOtpEmailChange as fetchOtpEmailChangeAction,
	fetchOtpMobileChange as fetchOtpMobileChangeAction,
	submitOtpMobEmailChange as submitOtpMobEmailChangeAction */
} from "../../../constants/commonActions";

const mapStateToProps = ({ common }) => ({
	/* otpMobile: common.otpMobileChange,
	otpEmail: common.otpEmailChange,
	type: common.type,
	verifyOtp: common.submitOtp,
	error: common.error */
});

const mapDispatchToProps = dispatch => ({
	/* otpMobileChange: data => dispatch(fetchOtpMobileChangeAction(data)),
	otpEmailChange: data => dispatch(fetchOtpEmailChangeAction(data)),
	submitOtpChange: data => dispatch(submitOtpMobEmailChangeAction(data)) */
});

export default connect(mapStateToProps, mapDispatchToProps)(updateMobEmail);