import { connect } from "react-redux";
// import {fetchFeaturedScholarships as fetchFeaturedScholarshipsAction, fetchScholarships as fetchScholarshipsAction, fetchClosedScholarships as fetchClosedScholarshipsAction } from '../actions';

import MySubscriber from "../components/mySubscriber/mySubscriberComponent";
import StudentList from "../components/mySubscriber/studentList";
import {
  fetchRules as fetchRulesAction,
  fetchDependantData as fetchDependantAction,
  fetchUserMatchingRules as fetchUserMatchingRulesAction,
  fetchUserRules as fetchUserRulesAction
} from "../../../constants/commonActions";

import {
  fetchMatchingScholarships as fetchScholarshipMatchingAction,
  updateStudent as updateStduentAction,
  fetchStudentDetails as fetchStudentDetailsAction,
  editStudentDetails as editStudentDetailsActions,
  fetchStudentLists as fetchStudentListAction,
  fetchMyFavScholarships as fetchMyFavScholarshipsActions,
  addToFavScholarship as addToFavScholarshipActions,
  // Upload User pic
  uploadUserPic as uploadUserPicAction,
  cscPaymentDetails as cscPaymentDetailsAction,
  cscRedirect as cscRedirectAction
} from "../actions";

const mapStateToProps = ({ common, dashboard, loginOrRegister }) => ({
  rulesList: common.rulesList,
  district: common.district,
  userRulesData: common.userRulesData,
  matchingRules: common.matchingRules,
  matchingScholarships: dashboard.matchingScholarships,
  dependantData: common.dependantData,
  studentData: dashboard.studentData,
  favScholarships: dashboard.favScholarships,
  showLoader: dashboard.showLoader,
  type: dashboard.type,
  showLoader: dashboard.showLoader,
  uploadPicData: dashboard.uploadPicData,
  isAuthenticated: loginOrRegister.isAuthenticated,
  cscPayment: dashboard.cscPaymentDts,
  isServerError: dashboard.isServerError,
  serverError: dashboard.serverError
});

const mapDispatchToProps = dispatch => ({
  loadRules: inputData => dispatch(fetchRulesAction()),
  fetchMatchRules: inputData =>
    dispatch(fetchUserMatchingRulesAction(inputData)),
  fetchMatchingScholarships: inputData =>
    dispatch(fetchScholarshipMatchingAction(inputData)),
  loadDistrictList: depntObj => dispatch(fetchDependantAction(depntObj)),
  requestDependantData: inputData => dispatch(fetchDependantData(inputData)),
  updateOrAddStudent: studentObj => dispatch(updateStduentAction(studentObj)),

  fetchMyFavScholarships: inputData =>
    dispatch(fetchMyFavScholarshipsActions(inputData)),
  fetchStudentDetails: inputData =>
    dispatch(fetchStudentDetailsAction(inputData)),
  editStudentDetails: inputData =>
    dispatch(editStudentDetailsActions(inputData)),
  fetchStudentList: inputData => dispatch(fetchStudentListAction(inputData)),
  uploadPic: inputData => dispatch(uploadUserPicAction(inputData)),
  addToFavScholarships: inputData =>
    dispatch(addToFavScholarshipActions(inputData)),
  fetchCscPaymentDt: inputData => dispatch(cscPaymentDetailsAction(inputData)),
  redirectCSC: inputData => dispatch(cscRedirectAction(inputData)),
  fetchUserRules: inputData => dispatch(fetchUserRulesAction(inputData))
});

const mySpecialContainerCreator = connect(mapStateToProps, mapDispatchToProps);

export const studentContainer = mySpecialContainerCreator(StudentList);

export default connect(mapStateToProps, mapDispatchToProps)(MySubscriber);
