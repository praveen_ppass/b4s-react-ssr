import { connect } from "react-redux";
import {
	fetchMatchingScholarships as fetchScholarshipMatchingAction,
	fetchMyFavScholarships as fetchMyFavScholarshipsActions,
	addToFavScholarship as addToFavScholarshipActions,
	deleteFavScholarship as deleteFavScholarshipActions,
	applicationStatus as applicationStatusActions,
	applicationStatusDoc as applicationStatusDocActions,

	/* Document View & Upload Issue */
	documentViewIssue as documentViewIssueAction,
	documentUploadIssue as documentUploadIssueAction,

	/* Awardee List */
	awardeeDoc as awardeeDocActions,

	qnaDashboard as qnaDashboardAction,
	qnaNotification as qnaNotificationAction,
	fetchBookSlot as fetchBookSlotInfoAction,
	updateBookingSlot as updateBookSlotAction,
	fetchPreferLang as fetchPreferLangAction,
	fetchBookSlotDates as fetchBookSlotDatesAction,
	fetchUpdateUser as fetchUpdateUserAction,

	//Family earning actions
	fetchFamilyEarningDetails as fetchFamilyEarningDetailsAction,
	addOrUpdateFamilyEarningDetails as addOrupdateFamilyEarningDetailsAction,
	deleteFamilyEarningDetails as deleteFamilyEarningDetailsAction,

	//Reference details actions
	fetchReferenceDetailsData as fetchReferenceDetailsDataAction,
	addOrUpdateReferenceDetailsData as addOrUpdateReferenceDetailsDataAction,
	deleteReferenceDetailData as deleteReferenceDetailDataAction,

	//Entrance examination actions
	fetchEntranceExaminationsDetails as fetchEntranceExaminationsDetailsAction,
	addOrUpdateEntranceExaminationsDetails as addOrUpdateEntranceExaminationsDetailsAction,
	deleteEntranceExaminationsDetails as deleteEntranceExaminationsDetailsAction,

	//Scholarship history actions
	fetchScholarshipHistoryDetails as fetchScholarshipHistoryDetailsAction,
	addOrUpdateScholarshipHistoryDetails as addOrUpdateScholarshipHistoryDetailsAction,
	deleteScholarshipHistoryDetails as deleteScholarshipHistoryDetailsAction,

	//Interests actions
	fetchUserScholarshipInterests as fetchUserScholarshipInterestsAction,
	addOrUpdateScholarshipInterests as addOrUpdateScholarshipInterestsAction,

	//Occupation data
	fetchOccupationData as fetchOccupationDataAction,

	//Bank details action
	fetchUserBankDetails as fetchUserBankDetailsAction,
	addOrUpdateBankDetails as addOrUpdateBankDetailsAction,

	//Edcation Data
	fetchEducationInfo as fetchEducationInfoAction,
	updateEducationDetails as updateEducationDetailsActions,

	//Document form actions
	fetchDocumentTypes as fetchDocumentTypesAction,
	fetchDocumentTypeCategories as fetchDocumentTypeCategoriesAction,
	fetchDocumentTypeCategoriesOptions as fetchDocumentTypeCategoriesOptionsAction,
	fetchUserDocuments as fetchUserDocumentsAction,
	addOrUpdateUserDocuments as addOrUpdateUserDocumentsAction,
	deleteUserDocuments as deleteUserDocumentsAction,
	uploadUserDocs as uploadUserDocsAction,

	// Upload User pic
	uploadUserPic as uploadUserPicAction,

	// DM DC list
	fetchDmdcList as fetchDmdcListAction,

	// Save DM / DC
	uploadDMDC as fetchSaveDMDCAction,

	// Scholar Disbursal
	fetchUserDisbursal as fetchUserDisbursalAction,
	// Scholar Disbursal Id
	fetchUserDisbursalId as fetchUserDisbursalIdAction,
	// Scholar Documents
	fetchScholarDocs as fetchScholarDocsAction,
	fetchOrgnlScholarDocs as fetchScholarOrgnlDocsAction,
	// Upload Documents
	uploadScholarDocs as uploadScholarDocsAction,

	//Disbursal Acknowledgement
	disbursalAcknowledgement as disbursalAcknowledgementActions

} from "../actions";

import {
	sendEduDetail as sendEduDetailAction,
	getEduDetail as getEduDetailAction,
	fetchRule as fetchRuleAction,
	getEduCourses as getEduCoursesAction,
	fetchDistrict as fetchDistrictAction,
	fetchBankDetail as fetchBankDetailAction,
	fetchBankSaveDetail as fetchBankSaveDetailAction,
	sendBankDetail as sendBankDetailAction
} from "../../scholar/scholarAction";

import {
	fetchUserMatchingRules as fetchUserMatchingRulesAction,
	fetchUserList as fetchUserListAction,
	fetchRules as fetchRulesAction,
	fetchDependantData as fetchDependantAction,
	fetchUserRules as fetchUserRulesAction,
	addOrUpdateUserRules as addOrUpdateUserRulesAction,
	fetchFilterRule as fetchFilterRuleAction,
	fetchBoardList as fetchBoardListAction,

	/* Email & Mobile & OTP Change in MyProfile */
	fetchOtpEmailChange as fetchOtpEmailChangeAction,
	fetchOtpMobileChange as fetchOtpMobileChangeAction,
	submitOtpMobEmail as submitOtpMobEmailAction
} from "../../../constants/commonActions";

import { fetchCscDMDM as fetchCscDMDMAction } from "../../csc/actions";

import ProfileInfo from "../components/profile-info/profileInfoComponent";
import PersonalInfo from "../components/profile-info/personalinfo";

const mapStateToProps = ({
	common,
	dashboard,
	loginOrRegister,
	scholarReducers
}) => ({
	/* Email & Mobile & OTP Change in MyProfile */
	type: common.type,
	otpMobile: common.otpMobileChange,
	otpEmail: common.otpEmailChange,
	verifyOtp: common.submitOtp,
	error: common.error,

	rulesList: common.rulesList,
	userList: common.userList,
	district: common.district,
	boardLists: common.boardList,
	userRulesData: common.userRulesData,
	updatedUserRulesData: common.updatedUserRulesData,
	filterRules: common.filterRules,

	updateUserInfo: dashboard.updateUserInfo,
	type: dashboard.type,
	userFamilyEarningData: dashboard.userFamilyEarningData,
	scholarshipHistoryData: dashboard.scholarshipHistoryData,
	entranceExaminationData: dashboard.entranceExaminationData,
	occupationData: dashboard.occupationData,
	referencesData: dashboard.referencesData,
	userBankDetailsData: dashboard.userBankDetailsData,
	updatedUserBankDetailsData: dashboard.updatedUserBankDetailsData,
	updatedEntranceExaminationData: dashboard.updatedEntranceExaminationData,
	educationData: dashboard.educationData,
	isError: dashboard.isError,
	errorMessage: dashboard.errorMessage,
	isUpdateError: dashboard.isUpdateError,
	updateError: dashboard.updateError,
	disbursalData: dashboard.disbursalData,
	disbursalId: dashboard.disbursalId,
	scholarsDocs: dashboard.scholarsDocs,
	//Document dropdown calls
	userDocumentsData: dashboard.userDocumentsData,
	documentGroupsAndTypes: dashboard.documentGroupsAndTypes,
	documentTypeCategories: dashboard.documentTypeCategories,
	documentTypeCategoriesOptions: dashboard.documentTypeCategoriesOptions,
	scholarOrgnlDocs: dashboard.orgnlDocs,
	//upload pic
	uploadPicData: dashboard.uploadPicData,

	//if user logs out,
	isAuthenticated: loginOrRegister.isAuthenticated,

	showLoader: dashboard.showLoader,

	// DMDC list
	dmdcList: dashboard.dmdcList,
	dmdcDetails: dashboard.dmdcDetails,
	// DMDC POST
	postedDMDC: dashboard.postedDMDC,
	isUploadDMDC: dashboard.isUploadDMDC,

	//Scholar data
	scholarType: scholarReducers.type,
	courseData: scholarReducers.courseData,
	ruleData: scholarReducers.ruleData,
	districtData: scholarReducers.districtData,
	eduData: scholarReducers.eduData,
	scholarLoader: scholarReducers.showLoader,
	bankData: scholarReducers.bankData,
	bankIfscData: scholarReducers.bankIfscData,

	/* My Scholarship */
	matchingRules: common.matchingRules,
	studentData: dashboard.studentData,
	userRulesData: common.userRulesData,
	type: dashboard.type,
	matchingScholarships: dashboard.matchingScholarships,
	favScholarships: dashboard.favScholarships,
	deleteFavSch: dashboard.deleteFavScholarships,
	applStatus: dashboard.applicationStatus,
	applStatusDoc: dashboard.applicationStatusDoc,

	documentIssue: dashboard.documentIssueData,
	documentUpload: dashboard.documentIssueUpload,

	awardeeDocData: dashboard.awardeeDocData,

	dashQna: dashboard.dashQna,
	qnaNotification: dashboard.qnaNotification,
	showLoader: dashboard.showLoader,
	bookSlot: dashboard.bookSlot,
	isUpdateError: dashboard.isUpdateError,
	updateError: dashboard.updateError,
	preferLang: dashboard.preferredLang,
	showLoader: dashboard.showLoader,
	uploadPicData: dashboard.uploadPicData,
	isAuthenticated: dashboard.isAuthenticated,
	bookSlotData: dashboard.bookSlotData,
	disbursalAcknowledgementData: dashboard.disbursalAcknowledgementData
});


const mapDispatchToProps = dispatch => ({
	loadRules: inputData => dispatch(fetchRulesAction()),
	loadDistrictList: depntObj => dispatch(fetchDependantAction(depntObj)),
	loadSaveUser: (userdata, userid) => dispatch(fetchUpdateUserAction(userdata, userid)),
	loadUserList: userid => dispatch(fetchUserListAction(userid)),
	fetchOccupationData: inputData => dispatch(fetchOccupationDataAction(inputData)),

	/* Reference details form actions */
	fetchReferenceDetailsData: inputData => dispatch(fetchReferenceDetailsDataAction(inputData)),
	addOrUpdateReferenceDetailsData: inputData => dispatch(addOrUpdateReferenceDetailsDataAction(inputData)),
	deleteReferenceDetailData: inputData => dispatch(deleteReferenceDetailDataAction(inputData)),

	/* Family earning details form actions */
	fetchFamilyEarningDetails: inputData => dispatch(fetchFamilyEarningDetailsAction(inputData)),
	addOrupdateFamilyEarningDetails: inputData => dispatch(addOrupdateFamilyEarningDetailsAction(inputData)),
	deleteFamilyEarningDetails: inputData => dispatch(deleteFamilyEarningDetailsAction(inputData)),

	/* Entrance exam details form actions */
	fetchEntranceExaminationsDetails: inputData => dispatch(fetchEntranceExaminationsDetailsAction(inputData)),
	addOrUpdateEntranceExaminationsDetails: inputData => dispatch(addOrUpdateEntranceExaminationsDetailsAction(inputData)),
	deleteEntranceExaminationsDetails: inputData => dispatch(deleteEntranceExaminationsDetailsAction(inputData)),

	/* Scholarship history  details form actions */
	fetchScholarshipHistoryDetails: inputData => dispatch(fetchScholarshipHistoryDetailsAction(inputData)),
	addOrUpdateScholarshipHistoryDetails: inputData => dispatch(addOrUpdateScholarshipHistoryDetailsAction(inputData)),
	deleteScholarshipHistoryDetails: inputData => dispatch(deleteScholarshipHistoryDetailsAction(inputData)),

	/* Interests form actions */
	fetchUserRules: inputData => dispatch(fetchUserRulesAction(inputData)),
	addOrUpdateUserRules: inputData => dispatch(addOrUpdateUserRulesAction(inputData)),

	/* Bank details form actions */
	fetchUserBankDetails: inputData => dispatch(fetchUserBankDetailsAction(inputData)),
	addOrUpdateBankDetails: inputData => dispatch(addOrUpdateBankDetailsAction(inputData)),

	/* education info details form actions */
	fetchEducationInfoDetails: inputData => dispatch(fetchEducationInfoAction(inputData)),
	updateEducationInfo: inputData => dispatch(updateEducationDetailsActions(inputData)),
	fetchFilterRuleById: inputData => dispatch(fetchFilterRuleAction(inputData)),

	/* Documents form actions */
	fetchDocumentTypes: inputData => dispatch(fetchDocumentTypesAction(inputData)),
	fetchDocumentTypeCategories: inputData => dispatch(fetchDocumentTypeCategoriesAction(inputData)),
	fetchDocumentTypeCategoriesOptions: inputData => dispatch(fetchDocumentTypeCategoriesOptionsAction(inputData)),
	fetchUserDocuments: inputData => dispatch(fetchUserDocumentsAction(inputData)),
	addOrUpdateUserDocuments: inputData => dispatch(addOrUpdateUserDocumentsAction(inputData)),
	deleteUserDocuments: inputData => dispatch(deleteUserDocumentsAction(inputData)),
	uploadUserDocs: inputData => dispatch(uploadUserDocsAction(inputData)),
	// Upload User Pic action
	uploadPic: inputData => dispatch(uploadUserPicAction(inputData)),

	// board list
	fetchBoard: inputData => dispatch(fetchBoardListAction(inputData)),

	// DMDC LIST
	fetchDmDc: inputData => dispatch(fetchDmdcListAction(inputData)),

	// getDMDC
	getDMDC: inputData => dispatch(fetchCscDMDMAction(inputData)),

	// post dmdc
	saveDMDC: inputData => dispatch(fetchSaveDMDCAction(inputData)),

	// Scholar Disbursal
	fetchUserDisbursalDetail: inputData =>
		dispatch(fetchUserDisbursalAction(inputData)),

	// Scholar Disbursal Id
	fetchUserDisbursalId: inputData =>
		dispatch(fetchUserDisbursalIdAction(inputData)),

	// Scholar Documents
	fetchScholarDocs: inputData => dispatch(fetchScholarDocsAction(inputData)),
	fetchScholarOrgnlDocs: inputData => dispatch(fetchScholarOrgnlDocsAction(inputData)),

	// Scholar Bank detail
	fetchBankDetail: data => dispatch(fetchBankDetailAction(data)),
	fetchBankSaveDetail: data => dispatch(fetchBankSaveDetailAction(data)),
	sendBankDetail: data => dispatch(sendBankDetailAction(data)),

	// Upload documents
	uploadScholarDocs: inputData => dispatch(uploadScholarDocsAction(inputData)),

	// Scholar Education
	fetchDistrict: data => dispatch(fetchDistrictAction(data)),
	fetchRule: data => dispatch(fetchRuleAction(data)),
	fetchCourses: data => dispatch(getEduCoursesAction(data)),
	fetchScholarEduDetail: data => dispatch(getEduDetailAction(data)),
	sendEduDetail: data => dispatch(sendEduDetailAction(data)),

	/* My Scholarship */
	fetchMatchRules: inputData =>
		dispatch(fetchUserMatchingRulesAction(inputData)),
	fetchMatchingScholarships: inputData =>
		dispatch(fetchScholarshipMatchingAction(inputData)),
	fetchMyFavScholarships: inputData =>
		dispatch(fetchMyFavScholarshipsActions(inputData)),
	addToFavScholarships: inputData =>
		dispatch(addToFavScholarshipActions(inputData)),
	deleteFavScholarships: inputData =>
		dispatch(deleteFavScholarshipActions(inputData)),

	fetchApplicationStatus: inputData => dispatch(applicationStatusActions(inputData)),
	fetchApplicationStatusDoc: inputData => dispatch(applicationStatusDocActions(inputData)),

	fetchAwardeeDocList: inputData => dispatch(awardeeDocActions(inputData)),

	fetchDocumentViewIssue: inputData => dispatch(documentViewIssueAction(inputData)),
	fetchDocumentUploadIssue: inputData => dispatch(documentUploadIssueAction(inputData)),

	//Disbursal Acknowledgement
	disbursalAcknowledgementCycle: inputData => dispatch(disbursalAcknowledgementActions(inputData)),

	fetchQnaDash: inputData =>
		dispatch(qnaDashboardAction(inputData)),
	fetchQnaNotification: inputData =>
		dispatch(qnaNotificationAction(inputData)),
	fetchBookSlotInfo: inputData => dispatch(fetchBookSlotInfoAction(inputData)),
	updatingBookSlot: inputData => dispatch(updateBookSlotAction(inputData)),
	fetchPreferLanguage: inputData => dispatch(fetchPreferLangAction(inputData)),
	uploadPic: inputData => dispatch(uploadUserPicAction(inputData)),
	fetchUserRules: inputData => dispatch(fetchUserRulesAction(inputData)),
	fetchBookSlotDates: inputData => dispatch(fetchBookSlotDatesAction(inputData)),

	// Scholar Disbursal
	fetchUserDisbursalDetail: inputData => dispatch(fetchUserDisbursalAction(inputData)),
	// Scholar Disbursal Id
	fetchUserDisbursalId: inputData => dispatch(fetchUserDisbursalIdAction(inputData)),

	/* My Scholarship */
	fetchMatchRules: inputData => dispatch(fetchUserMatchingRulesAction(inputData)),
	fetchMatchingScholarships: inputData => dispatch(fetchScholarshipMatchingAction(inputData)),
	fetchMyFavScholarships: inputData => dispatch(fetchMyFavScholarshipsActions(inputData)),
	addToFavScholarships: inputData => dispatch(addToFavScholarshipActions(inputData)),
	deleteFavScholarships: inputData => dispatch(deleteFavScholarshipActions(inputData)),

	/* QnA */
	fetchQnaDash: inputData => dispatch(qnaDashboardAction(inputData)),
	fetchQnaNotification: inputData => dispatch(qnaNotificationAction(inputData)),

	/* Email & Mobile & OTP Change in MyProfile */
	otpMobileChange: data => dispatch(fetchOtpMobileChangeAction(data)),
	otpEmailChange: data => dispatch(fetchOtpEmailChangeAction(data)),

	submitOtpMobEmailFun: data => dispatch(submitOtpMobEmailAction(data))
});

const mySpecialContainerCreator = connect(mapStateToProps, mapDispatchToProps);

export const profileContainer = mySpecialContainerCreator(PersonalInfo);

export const profile_info = mySpecialContainerCreator(ProfileInfo);
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(profile_info);
