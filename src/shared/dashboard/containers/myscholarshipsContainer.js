import { connect } from "react-redux";
// import {fetchFeaturedScholarships as fetchFeaturedScholarshipsAction, fetchScholarships as fetchScholarshipsAction, fetchClosedScholarships as fetchClosedScholarshipsAction } from '../actions';

import MyScholarship from "../components/myScholarship/myScholarshipComponent";
import MatchScholarship from "../components/extras/matchedScholarship";
import {
  fetchRules as fetchRulesAction,
  fetchDependantData as fetchDependantAction,
  fetchUserMatchingRules as fetchUserMatchingRulesAction,
  fetchUserRules as fetchUserRulesAction
} from "../../../constants/commonActions";

import {
  fetchMatchingScholarships as fetchScholarshipMatchingAction,
  fetchMyFavScholarships as fetchMyFavScholarshipsActions,
  addToFavScholarship as addToFavScholarshipActions,
  deleteFavScholarship as deleteFavScholarshipActions,
  applicationStatus as applicationStatusActions,
  fetchBookSlot as fetchBookSlotInfoAction,
  updateBookingSlot as updateBookSlotAction,
  fetchPreferLang as fetchPreferLangAction,
  fetchBookSlotDates as fetchBookSlotDatesAction,
  // Upload User pic
  uploadUserPic as uploadUserPicAction
} from "../actions";

const mapStateToProps = ({ common, dashboard, loginOrRegister }) => ({
  matchingRules: common.matchingRules,
  studentData: dashboard.studentData,
  userRulesData: common.userRulesData,
  type: dashboard.type,
  matchingScholarships: dashboard.matchingScholarships,
  favScholarships: dashboard.favScholarships,
  deleteFavSch: dashboard.deleteFavScholarships,
  applStatus: dashboard.applicationStatus,
  showLoader: dashboard.showLoader,
  bookSlot: dashboard.bookSlot,
  isUpdateError: dashboard.isUpdateError,
  updateError: dashboard.updateError,
  preferLang: dashboard.preferredLang,
  showLoader: dashboard.showLoader,
  uploadPicData: dashboard.uploadPicData,
  isAuthenticated: dashboard.isAuthenticated,
  bookSlotData: dashboard.bookSlotData
});

const mapDispatchToProps = dispatch => ({
  fetchMatchRules: inputData =>
    dispatch(fetchUserMatchingRulesAction(inputData)),
  fetchMatchingScholarships: inputData =>
    dispatch(fetchScholarshipMatchingAction(inputData)),
  fetchMyFavScholarships: inputData =>
    dispatch(fetchMyFavScholarshipsActions(inputData)),
  addToFavScholarships: inputData =>
    dispatch(addToFavScholarshipActions(inputData)),
  deleteFavScholarships: inputData =>
    dispatch(deleteFavScholarshipActions(inputData)),
  fetchApplicationStatus: inputData =>
    dispatch(applicationStatusActions(inputData)),
  fetchBookSlotInfo: inputData => dispatch(fetchBookSlotInfoAction(inputData)),
  updatingBookSlot: inputData => dispatch(updateBookSlotAction(inputData)),
  fetchPreferLanguage: inputData => dispatch(fetchPreferLangAction(inputData)),
  uploadPic: inputData => dispatch(uploadUserPicAction(inputData)),
  fetchUserRules: inputData => dispatch(fetchUserRulesAction(inputData)),
  fetchBookSlotDates: inputData => dispatch(fetchBookSlotDatesAction(inputData))
});

const mySpecialContainerCreator = connect(mapStateToProps, mapDispatchToProps);

export const studentContainer = mySpecialContainerCreator(MatchScholarship);

export default connect(mapStateToProps, mapDispatchToProps)(MyScholarship);
