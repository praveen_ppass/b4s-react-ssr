import { connect } from "react-redux";

import Scholar from "../components/scholar/scholarComponent";

import {
  fetchScholarScholarshipWon as fetchScholarScholarshipWonAction,
  fetchScholarScholarshipCertificatePdf as fetchScholarScholarshipCertificatePdfAction
} from "../actions";

const mapStateToProps = ({ dashboard, loginOrRegister }) => ({
  scholarScholarshipWonlistData: dashboard.scholarScholarshipWonData,
  scholarScholarshipCertificateListPdfData:
    dashboard.scholarScholarshipCertificatePdfData,
  showLoader: dashboard.showLoader,
  type: dashboard.type,
  isAuthenticated: loginOrRegister.isAuthenticated,
  isServerError: dashboard.isServerError,
  serverError: dashboard.serverError
});

const mapDispatchToProps = dispatch => ({
  scholarScholarshipWonData: inputData =>
    dispatch(fetchScholarScholarshipWonAction(inputData)),
  scholarScholarshipCertificatePdfData: inputData =>
    dispatch(fetchScholarScholarshipCertificatePdfAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Scholar);
