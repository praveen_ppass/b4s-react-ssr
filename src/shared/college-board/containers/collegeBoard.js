import React from "react";
import { connect } from "react-redux";
import collegeBoard from "../components/collegeBoardPages";

import { fetchPaymentPlans as fetchPaymentPlansAction } from "../../vle/actions";

const mapStateToProps = ({ collegeBoard }) => ({
  vlePaymentPlans: collegeBoard.paymentPlans,
  type: collegeBoard.type,
  errorCode: collegeBoard.errorCode,
  message: collegeBoard.message,
  showLoader: collegeBoard.showLoader
});
const mapDispatchToProps = dispatch => ({
  fetchPaymentPlan: (inputData, type) =>
    dispatch(fetchPaymentPlansAction(inputData, type))
});

export default connect(mapStateToProps, mapDispatchToProps)(collegeBoard);
