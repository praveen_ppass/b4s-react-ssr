import {
  FETCH_VLE_PAYMENT_PLANS_REQUEST,
  FETCH_VLE_PAYMENT_PLANS_SUCCESS,
  FETCH_VLE_PAYMENT_PLANS_FAILURE
} from "../vle/actions";

const initialState = {
  showLoader: false,
  paymentPlans: [],
  isError: false,
  message: "",
  paymentStatusMode: []
};

const collegeBoardReducer = (state = initialState, { payload, type }) => {
  switch (type) {
    /**** VLE Payment plans started********** */
    case FETCH_VLE_PAYMENT_PLANS_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_VLE_PAYMENT_PLANS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        paymentPlans: payload
      };

    case FETCH_VLE_PAYMENT_PLANS_FAILURE:
      return { ...state, showLoader: false, type, isError: true };
    /**** VLE Payment plans Ends********** */

    default:
      return state;
  }
};

export default collegeBoardReducer;
