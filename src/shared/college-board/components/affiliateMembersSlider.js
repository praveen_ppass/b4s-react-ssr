import React, { Component } from "react";
import Slider from "react-slick";
import { imgBaseUrl } from "../../../constants/constants";

export default class Responsive extends Component {
  render() {
    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 853,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            initialSlide: 2,
            dots: true
          }
        },
        {
          breakpoint: 603,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 1,
            dots: true
          }
        }
      ]
    };
    return (
      <Slider {...settings}>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="https://www.columbia.edu/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img src={`${imgBaseUrl}columbia-university.gif`} alt="" />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="http://www.mit.edu/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img src={`${imgBaseUrl}mit.gif`} alt="" />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="https://www.mcgill.ca/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img src={`${imgBaseUrl}mcgill.jpg`} alt="" />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="https://www.pomona.edu/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img src={`${imgBaseUrl}pomona.jpg`} alt="" />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="https://www.purdue.edu/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img src={`${imgBaseUrl}purdue.gif`} alt="" />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="https://www.cam.ac.uk/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img src={`${imgBaseUrl}university-of-cambridge.gif`} alt="" />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody cellHeight">
              <a
                href="https://www.hku.hk/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img src={`${imgBaseUrl}university-of-hong-kong.jpg`} alt="" />
              </a>
            </article>
          </article>
        </article>
      </Slider>
    );
  }
}
