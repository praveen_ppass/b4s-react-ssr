import React, { Component } from "react";
import Slider from "react-slick";
import { imgBaseUrl } from "../../../constants/constants";
import YoutubeVideo from "./youtubeVideo";

export default class Responsive extends Component {
  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 853,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            initialSlide: 2,
            dots: true
          }
        },
        {
          breakpoint: 603,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 1,
            dots: true
          }
        }
      ]
    };
    return (
      <Slider {...settings}>
        <article className="col-md-4">
          <article className="cellWrapper">
            <article className="contentBody">
              <YoutubeVideo videoId="L6lKMTWD1OA" />
            </article>
            <article className="contentFooter">
              <p> Why Take the SAT in India?</p>
            </article>
          </article>
        </article>
        <article className="col-md-4">
          <article className="cellWrapper">
            <article className="contentBody">
              <YoutubeVideo videoId="lOMC63e0OO8" />
            </article>
            <article className="contentFooter">
              <p> SAT Test Fee Reduction in India</p>
            </article>
          </article>
        </article>
        <article className="col-md-4">
          <article className="cellWrapper">
            <article className="contentBody">
              <YoutubeVideo videoId="D3vHhXMQPmE" />
            </article>
            <article className="contentFooter">
              <p>Official SAT practice on khan academy</p>
            </article>
          </article>
        </article>
        <article className="col-md-4">
          <article className="cellWrapper">
            <article className="contentBody">
              <YoutubeVideo videoId="x-Li4bDwJLc" />
            </article>
            <article className="contentFooter">
              <p> Using the SAT in India – Aranya</p>
            </article>
          </article>
        </article>
        <article className="col-md-4">
          <article className="cellWrapper">
            <article className="contentBody">
              <YoutubeVideo videoId="lNGdyVHSsds" />
            </article>
            <article className="contentFooter">
              <p> SAT: One Admissions Test, Many Colleges</p>
            </article>
          </article>
        </article>
        <article className="col-md-4">
          <article className="cellWrapper">
            <article className="contentBody">
              <YoutubeVideo videoId="1hNwLc5br6o" />
            </article>
            <article className="contentFooter">
              <p> SAT Opens Doors in India</p>
            </article>
          </article>
        </article>
        <article className="col-md-4">
          <article className="cellWrapper">
            <article className="contentBody">
              <YoutubeVideo videoId="fUmmkivwr8U" />
            </article>
            <article className="contentFooter">
              <p> SAT Opens Doors to Scholarships</p>
            </article>
          </article>
        </article>
        <article className="col-md-4">
          <article className="cellWrapper">
            <article className="contentBody">
              <YoutubeVideo videoId="FRXgxe3HzYc" />
            </article>
            <article className="contentFooter">
              <p>
                {" "}
                College Board's Linda Liu talks about the India Global Alliance
                and more
              </p>
            </article>
          </article>
        </article>
      </Slider>
    );
  }
}
