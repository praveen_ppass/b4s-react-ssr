import React, { Component } from "react";
import Slider from "react-slick";
import { imgBaseUrl } from "../../../constants/constants";

export default class Responsive extends Component {
  render() {
    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 853,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            initialSlide: 2,
            dots: true
          }
        },
        {
          breakpoint: 603,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 1,
            dots: true
          }
        }
      ]
    };
    return (
      <Slider {...settings}>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="https://ahduni.edu.in/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img
                  src={`${imgBaseUrl}ahmedabad-university.gif`}
                  alt="Ahmedabad university"
                />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="https://www.ashoka.edu.in/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img src={`${imgBaseUrl}ashoka.jpg`} alt="Ashoka univertsity" />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="http://www.azimpremjiuniversity.edu.in/SitePages/index.aspx"
                target="_blank"
                rel="noopener nofollow"
              >
                <img
                  src={`${imgBaseUrl}azim-premji.gif`}
                  alt="Azim Premji university"
                />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="https://www.bennett.edu.in/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img
                  src={`${imgBaseUrl}bennett-university.jpg`}
                  alt="Bennett university"
                />
              </a>
            </article>
          </article>
        </article>

        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="https://www.flame.edu.in/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img
                  src={`${imgBaseUrl}flame-univ.gif`}
                  alt="Flame university"
                />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="https://krea.edu.in/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img
                  src={`${imgBaseUrl}krea-university.jpg`}
                  alt="krea university"
                />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="http://manavrachna.edu.in/international-university/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img
                  src={`${imgBaseUrl}manav.gif`}
                  alt="Manav Rachna vidyanatariksha"
                />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="https://manipal.edu/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img
                  src={`${imgBaseUrl}manipal.gif`}
                  alt="Manipal academy of higher education"
                />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="http://www.msruas.ac.in/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img
                  src={`${imgBaseUrl}ramaiahUniversity.jpg`}
                  alt="MS Ramaiah university"
                />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="http://www.srmuniv.ac.in/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img
                  src={`${imgBaseUrl}srm.jpg`}
                  alt="SRM institute science and technology"
                />
              </a>
            </article>
          </article>
        </article>
        <article className="col-md-3">
          <article className="cellWrapper">
            <article className="contentBody">
              <a
                href="http://www.nmims.edu/"
                target="_blank"
                rel="noopener nofollow"
              >
                <img
                  src={`${imgBaseUrl}svkms-nmims.gif`}
                  alt="NMIMS deemed to be university"
                />
              </a>
            </article>
          </article>
        </article>
      </Slider>
    );
  }
}
