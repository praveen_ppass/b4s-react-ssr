import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import Responsive from "./studentVideosSlider";
import Slider from "react-slick";
import {
  defaultImageLogo,
  prodEnv,
  applicationFormsUrl,
  PRODUCTION_URL,
  STAGING_URL
} from "../../../constants/constants";
import Loader from "../../common/components/loader";
import { imgBaseUrl } from "../../../constants/constants";
import { imgBaseUrlDev } from "../../../constants/constants";
import { FETCH_VLE_PAYMENT_PLANS_SUCCESS } from "../../vle/actions";
var timerId;
class CollegeBoard extends Component {
  constructor() {
    super();
    this.state = {
      showItem: null,
      activeTab: "AboutSATExam",
      universitiesTab: "universitylogo",
      isFlag: true,
      stickClass: "",
      scrollClass: false,
      paymentPlanStructure: {
        6: null,
        7: null
      }
    };

    this.frequestAskedQuesHandler = this.frequestAskedQuesHandler.bind(this);
    this.onApplyHandler = this.onApplyHandler.bind(this);
    this.toggleTab = this.toggleTab.bind(this);
    this.logotoggleTab = this.logotoggleTab.bind(this);
    this.closeAd = this.closeAd.bind(this);
    this.onScrollHandler = this.onScrollHandler.bind(this);
    this.scroll = this.scroll.bind(this);
  }

  toggleTab(e) {
    if (e.target.nodeName === "LI") {
      const { id } = e.target;
      this.setState({
        activeTab: id
      });
    }
  }
  logotoggleTab(e) {
    if (e.target.nodeName === "LI") {
      const { id } = e.target;
      this.setState({
        universitiesTab: id
      });
    }
  }

  frequestAskedQuesHandler(index) {
    this.setState({
      showItem: index
    });
  }

  onApplyHandler(pathName) {
    if (window) {
      const location = (prodEnv ? PRODUCTION_URL : STAGING_URL) + pathName;
      window.open(location, "_blank");
    }
  }

  componentDidMount() {
    this.props.fetchPaymentPlan({}, "CB");
    window.addEventListener("scroll", this.scroll);
    // var g = document.createElement("script");	
    // g.type = "text/javascript";	
    // g.id = "chatID";	
    // g.src =	
    //   location.protocol + "//crm2.cloud-connect.in/hc_plugin/hc_script.js";	
    // g.onload = function() {	
    //   hc.initialize("8b9db7d02484c4677a69fbab83d525b1", "#f97352");	
    //   hc.setConfiguration({	
    //     defaultTitle: "Click 2 Call   ",	
    //     defaultState: "open",	
    //     eStyle: { right: "24px", bottom: "24px" }	
    //   });	
    // };	
    // document.getElementsByTagName("body")[0].appendChild(g);	
  }
  componentWillUnmount() {
    window.addEventListener("scroll", this.scroll);
    window.removeEventListener("scroll", this.scroll);
    clearTimeout(timerId);
    // var oP = document.getElementById("chatID");	
    // document.getElementsByClassName("hc_connect_widget")[0].style.display =	
    //   "none";	
    // return oP.parentNode.removeChild(oP);	
  }


  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case FETCH_VLE_PAYMENT_PLANS_SUCCESS:
        let paymentPlanStructure = {
          6: null,
          7: null
        };
        const { vlePaymentPlans } = nextProps;
        if (vlePaymentPlans && vlePaymentPlans.length === 2) {
          vlePaymentPlans.map(plan => {
            paymentPlanStructure[plan.id] = plan;
          });
          this.setState({
            paymentPlanStructure
          });
        }

        break;
    }
  }

  scroll(e) {
    if (window.scrollY >= 300) {
      this.setState({
        stickClass: "stickeyfix"
      });
    }
    if (window.scrollY <= 300) {
      this.setState({
        stickClass: ""
      });
    }
  }

  closeAd() {
    this.setState({
      isFlag: false
    });
  }
  onScrollHandler(element, flag) {
    this.setState({ scrollClass: flag });

    //if (!timerId) {
    timerId = setTimeout(() => {
      this.setState({ scrollClass: false });
    }, 20000);
    //}
    element.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }

  render() {
    const { paymentPlanStructure } = this.state;
    return (
      <section>
        <section className="gray collegeBoardBg MarginRow">
          <section className="container-fluid bannerBh">
            <section className="container">
              <section className="row">
                <section className="col-md-3">
                  <Link to="/">
                    <img
                      src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/white-logo.png"
                      className="logo"
                      alt="buddy4study-logo"
                    />
                  </Link>
                </section>
                <section className="col-md-9">
                  <a
                    href="https://signup.collegeboard.org/india-global-alliance/"
                    target="_blank"
                    rel="noopener nofollow"
                  >
                    <img
                      src={`${imgBaseUrl}collegeboard-logo.jpg`}
                      className="cbLogo"
                      alt="College board"
                      rel="noopener nofollow"
                    />
                  </a>
                </section>
              </section>
              <section className="row">
                <section className="col-md-12">
                  <article className="centerAlign">
                    <h1>College Board India Scholars Program</h1>
                    <p>
                      <span>
                        Up to 100% Tuition Fee Scholarships at Leading
                        Universities of
                      </span>
                      <span>India Global Higher Education Alliance</span>
                      <span>Take the SAT at discounted prices in India</span>
                    </p>
                    <button
                      onClick={() => this.onScrollHandler(this.refs.applyNow)}
                    >
                      Apply Now for SAT Discount
                    </button>
                  </article>
                </section>
              </section>
            </section>
          </section>
          <section
            className={`collegeBoardStepsFollowedContent containerfluid lightGrayBg ${this.state.stickClass}`}
            ref="sticky"
          >
            <section className="container">
              <section className="row">
                <article className="content">
                  <ul>
                    <li
                      onClick={() =>
                        this.onScrollHandler(this.refs.aboutSAT, 1)
                      }
                    >
                      Home
                    </li>
                    <li
                      onClick={() =>
                        this.onScrollHandler(this.refs.applyNow, 2)
                      }
                    >
                      Avail SAT Discount Voucher
                    </li>
                    <li
                      onClick={() =>
                        this.onScrollHandler(this.refs.university, 3)
                      }
                    >
                      Participating Universities
                    </li>
                    <li onClick={() => this.onScrollHandler(this.refs.faqs, 4)}>
                      FAQs
                    </li>
                    <li
                      onClick={() =>
                        this.onScrollHandler(this.refs.applyNow, 5)
                      }
                    >
                      Apply Now
                    </li>
                  </ul>
                </article>
              </section>
            </section>
          </section>
          <section
            className={`${
              this.state.scrollClass == 1 ? "scrollClass " : ""
              }container-fluid collegeBoardEvent`}
            ref="aboutSAT"
          >
            <section className="container">
              <section className="row">
                <article className="col-md-12">
                  <article className="tabWrapper">
                    <ul onClick={this.toggleTab} className="tablist">
                      <li
                        id="AboutSATExam"
                        className={
                          this.state.activeTab === "AboutSATExam"
                            ? "active"
                            : ""
                        }
                      >
                        Videos
                      </li>
                      <li
                        className={
                          this.state.activeTab === "OfficialSATPractice"
                            ? "active"
                            : ""
                        }
                        id="OfficialSATPractice"
                      >
                        In the Press
                      </li>
                    </ul>
                    <article className="tabContent">
                      {this.state.activeTab === "AboutSATExam" ? (
                        <article className="AboutSATExam">
                          <article className="row speech">
                            <Responsive />
                          </article>
                        </article>
                      ) : (
                          <article className="OfficialSATPractice">
                            <article className="row">
                              <article className="col-md-12">
                                <article className="scrollbar scrollbarColor">
                                  <p>
                                    <a
                                      href="https://www.indiatoday.in/education-today/news/story/sat-creators-college-board-announces-india-global-higher-education-alliance-html-1247952-2018-06-01"
                                      target="_blank"
                                      rel="noopener nofollow"
                                    >
                                      <article className="imgWrapper">
                                        <article className="imgContent">
                                          <img
                                            src={`${imgBaseUrl}indiatoday_coll.jpg`}
                                            alt="India today"
                                          />
                                        </article>
                                      </article>
                                      <span>
                                        SAT creators College Board announces India
                                        Global Higher Education Alliance to make
                                        world class education easily available
                                    </span>
                                    </a>
                                  </p>

                                  <p>
                                    <a
                                      href="https://www.thehindubusinessline.com/news/us-board-to-help-indian-varsities-process-admission/article23971851.ece"
                                      target="_blank"
                                      rel="noopener nofollow"
                                    >
                                      <article className="imgWrapper">
                                        <article className="imgContent">
                                          <img
                                            src={`${imgBaseUrl}bll_coll.jpg`}
                                            alt="Business Line"
                                          />
                                        </article>
                                      </article>
                                      <span>
                                        US Board to help Indian varsities process
                                        admission
                                    </span>
                                    </a>
                                  </p>

                                  <p>
                                    <a
                                      href="http://www.bbc.com/storyworks/the-college-board/an-equal-academic-playing-field-in-india"
                                      target="_blank"
                                      rel="noopener nofollow"
                                    >
                                      <article className="imgWrapper">
                                        <article className="imgContent">
                                          <img
                                            src={`${imgBaseUrl}bbc_coll.jpg`}
                                            alt="BBC"
                                          />
                                        </article>
                                      </article>
                                      <span>
                                        An Equal Academic Playing Field in India
                                    </span>
                                    </a>
                                  </p>

                                  <p>
                                    <a
                                      href="https://www.thestatesman.com/supplements/career/sat-iit-students-viewpoint-1502677166.html"
                                      target="_blank"
                                      rel="noopener nofollow"
                                    >
                                      <article className="imgWrapper">
                                        <article className="imgContent">
                                          <img
                                            src={`${imgBaseUrl}the_coll.jpg`}
                                            alt="The Statement"
                                          />
                                        </article>
                                      </article>
                                      <span>
                                        SAT or IIT? A student’s viewpoint
                                    </span>
                                    </a>
                                  </p>
                                  <p>
                                    <a
                                      href="https://timesofindia.indiatimes.com/home/education/news/sat-test-2019-important-dates-registration-and-test-fee/articleshow/68529178.cms"
                                      target="_blank"
                                      rel="noopener nofollow"
                                    >
                                      <article className="imgWrapper">
                                        <article className="imgContent">
                                          <img
                                            src={`${imgBaseUrl}toi_logo.jpg`}
                                            alt="The Statement"
                                          />
                                        </article>
                                      </article>
                                      <span>
                                        SAT Test 2019: Important Dates,
                                        Registration and Test Fee
                                    </span>
                                    </a>
                                  </p>

                                  <p>
                                    <a
                                      href="https://www.indiatoday.in/magazine/supplement/story/20190401-getting-ready-for-future-1482924-2019-03-20"
                                      target="_blank"
                                      rel="noopener nofollow"
                                    >
                                      <article className="imgWrapper">
                                        <article className="imgContent">
                                          <img
                                            src={`${imgBaseUrl}indiatoday_coll.jpg`}
                                            alt="India today"
                                          />
                                        </article>
                                      </article>
                                      <span>Getting ready for future</span>
                                    </a>
                                  </p>
                                </article>
                              </article>
                            </article>
                          </article>
                        )}
                    </article>
                  </article>
                </article>

                {/* <article className="col-md-4 col-sm-4">
                  <h3>Upcoming Events</h3>

                  <article className="videos right">
                    <article className="date">
                      <span>06</span>
                      <span className="month">July</span>
                    </article>
                    <p>Webinar: Using the SAT in India</p>
                    <p>
                      <b>Time: 10:00 AM </b>
                    </p>
                    <a
                      href="https://collegeboardtraining.webex.com/mw3300/mywebex/default.do?nomenu=true&siteurl=collegeboardtraining&service=6&rnd=0.031044567434353798&main_url=https%3A%2F%2Fcollegeboardtraining.webex.com%2Fec3300%2Feventcenter%2Fevent%2FeventAction.do%3FtheAction%3Ddetail%26%26%26EMK%3D4832534b00000004ecc1ed5a42e0b3d19f260cfb100c677f40c043e10767bba722089a2a8418166a%26siteurl%3Dcollegeboardtraining%26confViewID%3D130688828409165074%26encryptTicket%3DSDJTSwAAAASSMquXepENz-KFWkQ68a2Ao8UCSZ-DkIZQ6kKiGRN5FQ2%26"
                      target="_blank"
                      rel="noopener nofollow"
                    >
                      Join now
                    </a>
                  </article>
                </article> */}
              </section>
            </section>
          </section>

          <section
            className={`${
              this.state.scrollClass == 2 || this.state.scrollClass == 5
                ? "scrollClass "
                : ""
              }container-fluid collegeBoardiscount`}
            ref="applyNow"
          >
            <section className="container">
              <h3>Avail Steep Discounts for the SAT Exam Fee</h3>
              <section className="row">
                <section className="col-md-6 col-sm-6">
                  <article className="detailsbox">
                    <h4>
                      SAT 90% Discount Voucher & 100% tuition fees scholarship{" "}
                    </h4>
                    <article className="amount">
                      <p>Eligibility for Voucher</p>
                      <ul>
                        <li>Resident of India </li>
                        <li>Student in Class XI or XII</li>
                        <li>Annual Family Income less than ₹8 Lakhs </li>
                      </ul>

                      <p>Additional Eligibility For Scholarship</p>
                      <ul>
                        <li>
                          Take the SAT in Class XI/ Class XII in the
                          session2019-20{" "}
                        </li>
                        <li>
                          Top Performer in SAT exam (minimum score of 1300 out
                          of 1600)
                        </li>
                        <li>
                          {" "}
                          Must apply to and enroll in any of the participating
                          Indian universities
                        </li>
                      </ul>
                      <p>Award </p>
                      <ul className="award">
                        <li>Up to 100% Tuition Fee Scholarship </li>
                      </ul>
                      <h5>
                        Actual Amount:
                        <span>
                          &#x20B9;
                          {paymentPlanStructure[6] &&
                            paymentPlanStructure[6].totalFees}
                        </span>
                      </h5>
                      <h6>
                        After Discount:
                        <span>
                          &#x20B9;
                          {paymentPlanStructure[6] &&
                            paymentPlanStructure[6].reducedAmount}
                        </span>
                      </h6>
                      <p>
                        You save:
                        <span>
                          &#x20B9;
                          {paymentPlanStructure[6] &&
                            paymentPlanStructure[6].discountFees}
                          {paymentPlanStructure[6] &&
                            paymentPlanStructure[6].discount
                            ? `(${paymentPlanStructure[6].discount}%)`
                            : ""}
                        </span>
                      </p>
                      <button
                        onClick={() =>
                          this.onApplyHandler("application/CBI2/instruction")
                        }
                      >
                        Avail 90% discount voucher
                      </button>
                    </article>
                  </article>
                </section>
                <section className="col-md-6 col-sm-6">
                  <article className="detailsbox">
                    <h4>SAT 50% Discount Voucher & Upto $1500 scholarship</h4>
                    <article className="amount">
                      <p>Eligibility for Voucher</p>
                      <ul>
                        <li>Resident of India</li>
                        <li>Student in Class XI or XII</li>
                        <li>
                          Annual Family Income between ₹8 Lakhs to ₹15 Lakhs{" "}
                        </li>
                      </ul>

                      <p>Additional Eligibility For Scholarship</p>
                      <ul>
                        <li>
                          Take the SAT in Class XI/ Class XII in the session
                          2019-20{" "}
                        </li>
                        <li>
                          Top Performer in SAT exam (minimum score of 1300 out
                          of 1600)
                        </li>
                        <li>
                          Must apply to and enroll in any of the participating
                          Indian universities
                        </li>
                      </ul>
                      <p>Award </p>
                      <ul className="award">
                        <li>Up to $1500 Tuition Fee Scholarship </li>
                      </ul>
                      <h5>
                        Actual Amount:
                        <span>
                          &#x20B9;
                          {paymentPlanStructure[7] &&
                            paymentPlanStructure[7].totalFees}
                        </span>
                      </h5>
                      <h6>
                        After Discount:
                        <span>
                          &#x20B9;
                          {paymentPlanStructure[7] &&
                            paymentPlanStructure[7].reducedAmount}
                        </span>
                      </h6>
                      <p>
                        You save:
                        <span>
                          &#x20B9;
                          {paymentPlanStructure[7] &&
                            paymentPlanStructure[7].discountFees}
                          {paymentPlanStructure[7] &&
                            paymentPlanStructure[7].discount
                            ? `(${paymentPlanStructure[7].discount}%)`
                            : ""}
                        </span>
                      </p>
                      <button
                        onClick={() =>
                          this.onApplyHandler("application/CBS3/instruction")
                        }
                      >
                        Avail 50% discount voucher
                      </button>
                    </article>
                  </article>
                </section>
              </section>
            </section>
          </section>
          <section
            className={`${
              this.state.scrollClass == 3 ? "scrollClass " : ""
              }collegeBoardFoundingMembersRow container-fluid`}
            ref="university"
          >
            <section className="container">
              <h3>
                Tuition Fee Scholarships at Participating Indian Universities
              </h3>
              <section className="row">
                <article className="col-md-12">
                  <article className="tabWrapper">
                    <ul onClick={this.logotoggleTab} className="tablist">
                      <li
                        id="universitylogo"
                        className={
                          this.state.universitiesTab === "universitylogo"
                            ? "active"
                            : ""
                        }
                      >
                        Up to 100 % Tuition Fee Scholarship
                      </li>
                      <li
                        className={
                          this.state.universitiesTab === "universitylogo1"
                            ? "active"
                            : ""
                        }
                        id="universitylogo1"
                      >
                        Up to $1,500 Tuition Fee Scholarship
                      </li>
                    </ul>
                    <article className="tabContent">
                      {this.state.universitiesTab === "universitylogo" ? (
                        <article className="AboutSATExam">
                          <article className="row">
                            <article className="col-md-3 col-sm-6">
                              <article className="contentBody">
                                <a
                                  href="https://ahduni.edu.in/"
                                  target="_blank"
                                  rel="noopener nofollow"
                                >
                                  <img
                                    src={`${imgBaseUrl}ahmedabad-university.gif`}
                                    alt="Ahmedabad university"
                                  />
                                </a>
                                <p>Ahmedabad University</p>
                              </article>
                            </article>

                            <article className="col-md-3 col-sm-6">
                              <article className="contentBody">
                                <a
                                  href="https://www.ashoka.edu.in/"
                                  target="_blank"
                                  rel="noopener nofollow"
                                >
                                  <img
                                    src={`${imgBaseUrl}ashoka.jpg`}
                                    alt="Ashoka univertsity"
                                  />
                                </a>
                                <p>Ashoka University</p>
                              </article>
                            </article>

                            <article className="col-md-3 col-sm-6">
                              <article className="contentBody">
                                <a
                                  href="http://www.azimpremjiuniversity.edu.in/SitePages/index.aspx"
                                  target="_blank"
                                  rel="noopener nofollow"
                                >
                                  <img
                                    src={`${imgBaseUrl}azim-premji.gif`}
                                    alt="Azim Premji university"
                                  />
                                </a>
                                <p>Azim Premji University</p>
                              </article>
                            </article>
                            <article className="col-md-3 col-sm-6">
                              <article className="contentBody">
                                <a
                                  href="https://www.flame.edu.in/"
                                  target="_blank"
                                  rel="noopener nofollow"
                                >
                                  <img
                                    src={`${imgBaseUrl}flame-univ.gif`}
                                    alt="Flame university"
                                  />
                                </a>
                                <p>Flame University</p>
                              </article>
                            </article>

                            <article className="col-md-3 col-sm-6">
                              <article className="contentBody">
                                <a
                                  href="https://krea.edu.in/"
                                  target="_blank"
                                  rel="noopener nofollow"
                                >
                                  <img
                                    src={`${imgBaseUrl}krea-university.jpg`}
                                    alt="krea university"
                                  />
                                </a>
                                <p>krea University</p>
                              </article>
                            </article>

                            <article className="col-md-3 col-sm-6">
                              <article className="contentBody">
                                <a
                                  href="http://www.msruas.ac.in/"
                                  target="_blank"
                                  rel="noopener nofollow"
                                >
                                  <img
                                    src={`${imgBaseUrl}ramaiahUniversity.jpg`}
                                    alt="MS Ramaiah university"
                                  />
                                </a>
                                <p>
                                  M. S. Ramaiah University of Applied Sciences
                                </p>
                              </article>
                            </article>
                            <article className="col-md-3 col-sm-6">
                              <article className="contentBody">
                                <a
                                  href="http://manavrachna.edu.in/"
                                  target="_blank"
                                  rel="noopener nofollow"
                                >
                                  <img
                                    src={`${imgBaseUrl}manav.gif`}
                                    alt="Manav Rachna vidyanatariksha"
                                  />
                                </a>
                                <p>Manav Rachna Vidyanatariksha</p>
                              </article>
                            </article>

                            <article className="col-md-3 col-sm-6">
                              <article className="contentBody">
                                <a
                                  href="https://manipal.edu/"
                                  target="_blank"
                                  rel="noopener nofollow"
                                >
                                  <img
                                    src={`${imgBaseUrl}manipal.gif`}
                                    alt="Manipal academy of higher education"
                                  />
                                </a>
                                <p>Manipal academy of higher education</p>
                              </article>
                            </article>
                            <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://jgu.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}OP-JGU-logo.png`}
                                      alt="O.P. Jindal Global University"
                                    />
                                  </a>
                                  <p>O.P. Jindal Global University</p>
                                </article>
                              </article>
                            <article className="col-md-3 col-sm-6">
                              <article className="contentBody">
                                <a
                                  href="https://srisriuniversity.edu.in/"
                                  target="_blank"
                                  rel="noopener nofollow"
                                >
                                  <img
                                    src={`${imgBaseUrl}sri-sri-logo.jpg`}
                                    alt="Sri Sri University"
                                  />
                                </a>
                                <p>Sri Sri University </p>
                              </article>
                            </article>

                            <article className="col-md-3 col-sm-6">
                              <article className="contentBody">
                                <a
                                  href="http://www.srmuniv.ac.in/"
                                  target="_blank"
                                  rel="noopener nofollow"
                                >
                                  <img
                                    src={`${imgBaseUrl}srmuniv.jpg`}
                                    alt="SRM institute science and technology"
                                  />
                                </a>
                                <p>SRM Institute of Science and Technology </p>
                              </article>
                            </article>
                            <article className="col-md-3 col-sm-6">
                              <article className="contentBody">
                                <a
                                  href="http://www.srmuniversity.ac.in/"
                                  target="_blank"
                                  rel="noopener nofollow"
                                >
                                  <img
                                    src={`${imgBaseUrl}srmuh-logo.jpg`}
                                    alt="SRM University Delhi-NCR, Sonepat"
                                  />
                                </a>
                                <p>SRM University Delhi-NCR, Sonepat </p>
                              </article>
                            </article>
                            <article className="col-md-3 col-sm-6">
                              <article className="contentBody">
                                <a
                                  href="http://www.nmims.edu/"
                                  target="_blank"
                                  rel="noopener nofollow"
                                >
                                  <img
                                    src={`${imgBaseUrl}svkms-nmims.gif`}
                                    alt="NMIMS deemed to be university"
                                  />
                                </a>
                                <p>NMIMS deemed to be university </p>
                              </article>
                            </article>
                            <article className="col-md-3 col-sm-6">
                              <article className="contentBody">
                                <a
                                  href="http://worlduniversityofdesign.ac.in/"
                                  target="_blank"
                                  rel="noopener nofollow"
                                >
                                  <img
                                    src={`${imgBaseUrl}world-university-design-logo.jpg`}
                                    alt="World University of Design"
                                  />
                                </a>
                                <p>World University of Design </p>
                              </article>
                            </article>
                          </article>
                        </article>
                      ) : (
                          <article className="OfficialSATPractice">
                            <article className="row">
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://ahduni.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}ahmedabad-university.gif`}
                                      alt="Ahmedabad university"
                                    />
                                  </a>
                                  <p>Ahmedabad University</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://anu.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}Anant_National_University_logo.png`}
                                      alt="Anant National University"
                                    />
                                  </a>
                                  <p>Anant National University</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://www.ashoka.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}ashoka.jpg`}
                                      alt="Ashoka univertsity"
                                    />
                                  </a>
                                  <p>Ashoka University</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://www.atriauniversity.org/top-university-in-india/?gclid=CjwKCAjwguzzBRBiEiwAgU0FT0DsMLemWlBGgdyStfL4-vbmKyVfvt-BLWU6w_P6wbfQ7LTf3yD1XRoCwQUQAvD_BwE"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}atria1.jpg`}
                                      alt="Atria univertsity"
                                    />
                                  </a>
                                  <p>Atria University</p>
                                </article>
                              </article>

                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="http://www.azimpremjiuniversity.edu.in/SitePages/index.aspx"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}azim-premji.gif`}
                                      alt="Azim Premji university"
                                    />
                                  </a>
                                  <p>Azim Premji University</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://www.bennett.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}bennett-university.jpg`}
                                      alt="Bennett university"
                                    />
                                  </a>
                                  <p>Bennett University</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://www.bml.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}bml.gif`}
                                      alt="BML Munjal University"
                                    />
                                  </a>
                                  <p>BML Munjal University</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://www.flame.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}flame-univ.gif`}
                                      alt="Flame university"
                                    />
                                  </a>
                                  <p>Flame University</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://www.gitam.edu/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}gitam-univ-logo_0.gif`}
                                      alt="Gandhi Institute of Technology and Management"
                                    />
                                  </a>
                                  <p>
                                    Gandhi Institute of Technology and Management
                                </p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://www.gdgoenkauniversity.com/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}CD-Goenka-logo-resize1.jpg`}
                                      alt="GD Goenka University"
                                    />
                                  </a>
                                  <p>GD Goenka University</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="http://www.jaihindcollege.com/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}JaiHindUni.jpg`}
                                      alt="Jai Hind College (University of Mumbai)"
                                    />
                                  </a>
                                  <p>Jai Hind College (University of Mumbai)</p>
                                </article>
                              </article>

                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://www.jainuniversity.ac.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}Jain.jpg`}
                                      alt="Jain University"
                                    />
                                  </a>
                                  <p>Jain University</p>
                                </article>
                              </article>

                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://krea.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}krea-university.jpg`}
                                      alt="krea university"
                                    />
                                  </a>
                                  <p>krea University</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="http://manavrachna.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}manav.gif`}
                                      alt="Manav Rachna vidyanatariksha"
                                    />
                                  </a>
                                  <p>Manav Rachna Vidyanatariksha</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://manipal.edu/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}manipal.gif`}
                                      alt="Manipal academy of higher education"
                                    />
                                  </a>
                                  <p>Manipal academy of higher education</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://jaipur.manipal.edu/muj/about-us.html"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}manipal-jaipur.jpg`}
                                      alt="Manipal Jaipur"
                                    />
                                  </a>
                                  <p>Manipal Jaipur</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="http://www.msruas.ac.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}ramaiahUniversity.jpg`}
                                      alt="MS Ramaiah university"
                                    />
                                  </a>
                                  <p>
                                    M. S. Ramaiah University of Applied Sciences
                                </p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://jgu.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}OP-JGU-logo.png`}
                                      alt="O.P. Jindal Global University"
                                    />
                                  </a>
                                  <p>O.P. Jindal Global University</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://plaksha.org/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}plaksha-uni.png`}
                                      alt="Plaksha University"
                                    />
                                  </a>
                                  <p>Plaksha University</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://www.sastra.edu/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}sastralogo.png`}
                                      alt="SASTRA (Deemed University)"
                                    />
                                  </a>
                                  <p>SASTRA (Deemed University)</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://snu.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}shiv-nadar-university-logo_0.jpg`}
                                      alt="Shiv Nadar University"
                                    />
                                  </a>
                                  <p>Shiv Nadar University</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://shooliniuniversity.com/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}Shoolini.jpg`}
                                      alt="Shoolini University"
                                    />
                                  </a>
                                  <p>Shoolini University</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://srmap.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}SRM_AP_Logo_Nov17.jpg`}
                                      alt="SRM University, AP - Amaravati"
                                    />
                                  </a>
                                  <p>SRM University, AP - Amaravati</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="http://www.srmuniv.ac.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}srmuniv.jpg`}
                                      alt="SRM institute science and technology"
                                    />
                                  </a>
                                  <p>SRM Institute of Science and Technology </p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="http://www.srmuniversity.ac.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}srmuh-logo.jpg`}
                                      alt="SRM University Delhi-NCR, Sonepat"
                                    />
                                  </a>
                                  <p>SRM University Delhi-NCR, Sonepat </p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://www.sophiacollegemumbai.com/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}sophia-coll.jpg`}
                                      alt="Sophia College For Women"
                                    />
                                  </a>
                                  <p>Sophia College For Women </p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://srisriuniversity.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}sri-sri-logo.jpg`}
                                      alt="Sri Sri University"
                                    />
                                  </a>
                                  <p>Sri Sri University </p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="http://srishti.ac.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}srishti-school-of-art-design-technology-logo_0.png`}
                                      alt="Srishti School of Art Design and Technology"
                                    />
                                  </a>
                                  <p>
                                    Srishti School of Art Design and Technology
                                </p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://www.nmims.edu/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}svkms-nmims.gif`}
                                      alt="SVKM's NMIMS"
                                    />
                                  </a>
                                  <p>SVKM's NMIMS</p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://www.ssla.edu.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}Symbiosis-logo1.jpg`}
                                      alt="Symbiosis International (Deemed University)"
                                    />
                                  </a>
                                  <p>
                                    Symbiosis International (Deemed University){" "}
                                  </p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://www.upes.ac.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}UPES.jpg`}
                                      alt="University of Petroleum and Energy Science"
                                    />
                                  </a>
                                  <p>
                                    University of Petroleum and Energy Science{" "}
                                  </p>
                                </article>
                              </article>
                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="https://vit.ac.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}vitlogo.png`}
                                      alt="Vellore Institute Of Technology "
                                    />
                                  </a>
                                  <p>Vellore Institute Of Technology </p>
                                </article>
                              </article>

                              <article className="col-md-3 col-sm-6">
                                <article className="contentBody">
                                  <a
                                    href="http://worlduniversityofdesign.ac.in/"
                                    target="_blank"
                                    rel="noopener nofollow"
                                  >
                                    <img
                                      src={`${imgBaseUrl}world-university-design-logo.jpg`}
                                      alt="World University of Design"
                                    />
                                  </a>
                                  <p>World University of Design </p>
                                </article>
                              </article>
                            </article>
                          </article>
                        )}
                    </article>
                  </article>
                </article>
              </section>
            </section>
          </section>

          <section
            className={`${
              this.state.scrollClass == 4 ? "scrollClass " : ""
              }collegeBoardFAQsRow container-fluid`}
            ref="faqs"
          >
            <section className="container">
              <section className="row">
                <article className="col-md-12">
                  <h3>Frequently Asked Question</h3>
                  <article className="content">
                    <FrequestAskedQuestion
                      frequestAskedQuesHandler={this.frequestAskedQuesHandler}
                      showItem={this.state.showItem}
                    />
                  </article>
                </article>
              </section>
            </section>
          </section>

          <section className="container-fluid collegeBoardSupport">
            <section className="container">
              <section className="row">
                <article className="col-md-12">
                  <h3>Support</h3>

                  <p>In case of queries, please reach out to us:</p>
                  <ul className="supportList">
                    <li>
                      <dd>+91-11-430-92248 (Ext: 118) </dd>
                      <span>(Monday to Friday &ndash; 10:00AM to 6PM)</span>
                    </li>
                    <li>
                      <a href="mailto:iga-help@buddy4study.com">
                        iga-help@buddy4study.com
                      </a>
                    </li>
                  </ul>
                </article>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

const FrequestAskedQuestion = props => {
  const frequenstAskedQuestion = [
    {
      title: "What is the SAT?",

      desc:
        "The SAT is a globally available college entrance exam accepted by all U.S. and many international colleges and universities. All Indian universities that are part of the College Board’s India Higher Education Global Alliance initiative accept SAT as an entrance exam for students in India. The SAT is administered four times a year in India in the months of March, May, October, and December. You can take the SAT in any of these admins. The SAT measures what students learn in school and what they need to succeed in college."
    },
    {
      title: "What is College Board India Scholars Programme?",

      desc:
        "Students who score 1300 or more out of a total score of 1600 on the SAT exam will be recognized by the College Board as College Board India Top Performers. Top performers from families with significant financial need (see eligibility criteria above) will be eligible to apply for a 100% college tuition scholarship at any of the participating Indian universities (see list of participating Indian universities above). Students need to be in Grade 12 when they take the SAT to qualify for the scholarships. <br/><br/> Also, those students who belong to a family income group of INR 8-15 lakh will get financial reward of up to $1,500 (~INR 1 lakh) towards the college tuition fee at participating Indian universities."
    },
    {
      title:
        "Can I participate in the College Board India Scholars Programme if I take the SAT exam in Grade 11?",

      desc:
        "Students of Class 11 can avail the SAT fee reduction voucher, and they may use their SAT scores to apply to Indian Alliance member universities for admissions. To qualify for the scholarships, students need to take the SAT exam in Grade 12 for the 2020-2021 session. Any student taking the SAT exam while in Grade 11 in 2020-21 session will avail scholarship for the admission year 2022."
    },

    {
      title:
        "To which Indian colleges and universities can I apply for admission with scholarship using the SAT exam?",

      desc:
        "Refer to the list of participating universities above. More universities request to join the scholarship program. Please visit this website to get the latest list of participating Indian universities."
    },
    {
      title:
        "Are SAT scores accepted for all programmes offered by member institutions of India Global Higher Education Alliance?",

      desc:
        "Many institutions of India Global Higher Education Alliance accept SAT scores for admission to all programmes run by them.However, there are also some member institutions which accept SAT scores for admission to their specific programmes only.  Below is the list of institutions which accept SAT scores for admission to all courses run by them:<ul><li>Ashoka University</li><li>Azim Premji University</li><li>FLAME University</li> <li>KREA University</li><li> Manav Rachna University</li><li> Sri Sri University</li><li> Srishti School of Art Design and Technology</li><li> SRM Institute of Science and Technology</li><li>SRM University, AP - Amaravati</li><li>SRM University Delhi-NCR, Sonepat</li><li>World University of Design</li><li>Plaksha University </li><p>Further, colleges/universities which offer admission to their specific courses on the basis of SAT scores are listed below:</p><br /><br /><table style='border:1px solid #ccc;padding:10px;'><tbody><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>Institution Name</strong></p></td><td style='padding: 10px;'><p>Programs for which SAT scores are accepted &nbsp;</p></td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>Ahmedabad University</strong></p></td><td style='padding: 10px;'><p>All except B.Tech.</td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>Atria University</strong></p></td><td style='padding: 10px;'><p>All courses except Law and Architecture.</td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>Bennett University</strong></p></td><td style='padding: 10px;'><p>B.Tech.<br />B.B.A.</p></td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>BML Munjal University</strong></p></td><td style='padding: 10px;'><p>B.Tech.<br />B.B.A.<br />B.Sc. (Economics)</p></td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>Gandhi Institute of Technology and Management (GITAM)</strong></p></td><td style='padding: 10px;'><p>All B.Sc./B.C.A./B.E.M. programs<br />B.Phama<br />All B.B.A. Programs<br />B.Com. (Hons)<br />All BA programs</p></td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>M.S. Ramaiah University of Applied Sciences</strong></p></td><td style='padding: 10px;'><p>B.Tech.<br />B.B.A.<br />B.Pharma<br />Pharma D.<br />B. Des.<br />B.H.M.<br />B.Sc. (Hons)<br />B.Com. (Hons)<br />B.Sc. (Bio-Tech)<br />B.Sc. (Food Processing and Technology)<br />B.Sc.&nbsp; (Dialysis Therapy Technology)<br />B.Sc. (Medical Radiology and Imaging Technology)<br />B.Sc. (Cardiac Care Technology)<br />B.Sc. (Sports and Exercise Science)<br />B.Sc. (Optometry)<br />B.Sc. (Operation Theatre Technology)</p></td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>Manipal Academy of Higher Education</strong></p></td><td style='padding: 10px;'><p>B.Sc. (Exercise and Sports Sciences)<br />B.Sc. (Health Information Management)<br />B.Sc. (Medical Imaging Technology)<br />B.Sc. (Perfusion Technology)<br />B.Sc. (Renal Replacement Therapy &amp; Dialysis Technology)<br />B.Com. (Professional)<br />B.Com. (Business Process Services)</p></td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>Manipal Jaipur University</strong></p></td><td style='padding: 10px;'><p>All courses except Law and Architecture.</p></td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>OP Jindal Global University</strong></p></td><td style='padding: 10px;'><p>All programs except Law</p></td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>Shiv Nadar University</strong></p></td><td style='padding: 10px;'><p>B.Tech.<br />B.A. - History, English, Sociology, International Relations, Economics<br />B.Sc. - Math, Physics, Chemistry, Biotech<br />B.M.S.</p></td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>Sophia College For Women</strong></p></td><td style='padding: 10px;'><p>All courses except Law and Architecture.</p></td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>SVKM's NMIMS</strong></p></td><td style='padding: 10px;'><p>All Courses in the following streams (schools):<br />Engineering<br />Liberal Arts<br />Maths<br />Economics<br />Commerce</p></td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>Symbiosis International (Deemed University)</strong></p></td><td style='padding: 10px;'><p>Liberal Arts</p></td></tr><tr style='border:1px solid #ccc'><td style='padding: 10px;'><p><strong>Vellore Institute of Technology</strong></p></td><td style='padding: 10px;'><p>All except Engineering, Law and Architecture programs</p></td></tr></tbody></table><br/><p>Please note: A few member institutions have not yet published their policy and we will update you as soon as it is available.</p>"
    },

    {
      title: "In which cities are the SAT exams administered?",

      desc:
        "The SAT can be taken at over 80 test centres in 40+ cities across India. Find a test centre near you <a href='https://collegereadiness.collegeboard.org/sat/register/find-test-centers' target='_blank' rel='noopener nofollow'> here</a>. Note: Change test dates on the search form to search for all available test centres."
    },
    {
      title: "How can you request for a SAT fee reduction voucher?",

      desc:
        "Students who meet the eligibility criteria for 50% or 90% discount, should click on “Avail SAT Discount Voucher” to apply for the SAT fee reduction voucher. Upon receipt of payment of discounted SAT Fee, you will receive your voucher code within 2 days. Student should register and redeem the SAT voucher at the  <a href='https://collegereadiness.collegeboard.org/sat/register/international' target='_blank' rel='noopener nofollow'> College Board SAT registration website</a>  and register for the May, Oct, or Dec SAT exam in 2020. Redeem the fee reduction voucher within 15 days of receipt of voucher from Buddy4Study or prior to registration deadline, whichever date is earlier."
    },
    {
      title: "Why request for a SAT fee reduction voucher from Buddy4Study?",

      desc:
        "Buddy4Study has been selected by the College Board to verify eligibility of students (criteria as listed above) for the SAT fee reduction voucher, and distribute the vouchers to the eligible applicants. Student needs this SAT fee reduction voucher code to register for the SAT test on the <a href='https://collegereadiness.collegeboard.org/sat/register/international' target='_blank' rel='noopener nofollow'>College Board's SAT registration website.</a>"
    },
    {
      title: "How do I redeem a discount Voucher?",
      desc:
        "Students who receive the SAT fee reduction voucher from Buddy4Study can redeem it on the  <a href='https://collegereadiness.collegeboard.org/sat/register/international' target='_blank' rel='noopener nofollow'>College Board's SAT registration website</a>."
    },
    {
      title: "Do I need to take the Essay option with the SAT exam?",
      desc:
        "Indian universities that are part of the  <a href='https://international.collegeboard.org/global-alliance' target='_blank' rel='noopener nofollow'>College Board's India Global Higher Education Alliance</a>. do not require the essay for admissions. However, some colleges and universities in US and around the world may require the essay for their admission processes. If you plan to apply to colleges and universities in the US, please refer to their admission websites for essay requirement. For admission to Indian universities, essay is not required."
    },

    {
      title: "How long is the SAT test and what is tested in the exam?",
      desc:
        "The SAT is three hours long, with an added 50 minutes for the essay (optional). The SAT consists of two sections: Math and Evidence-Based Reading and Writing. The Essay is an optional component of the SAT. The test focusses on skills and knowledge that the student learns in high schools and what they need to succeed in college. Visit  <a href='https://collegereadiness.collegeboard.org/sat/inside-the-test' target='_blank' rel='noopener nofollow'>this link</a>. to find out what kinds of questions you'll see on the SAT and what they measure. You can also watch videos for an overview of each test."
    },
    {
      title: "How to apply for the scholarship?",
      desc:
        "If you are recognized by the College Board as a Top Performer on the SAT exam (score of 1300 or more out of a total of 1600) AND if you meet eligibility criteria for the College Board India Scholars Programme, you should contact Buddy4Study to begin the application process for the scholarship."
    },

    {
      title: "How to prepare for SAT Exam?",
      desc:
        "Visit <a href='https://www.khanacademy.org/sat' target='_blank' rel='noopener nofollow'>this link</a> to get free, world-class, personalized practice curriculum – Official SAT Practice on Khan Academy. Students can also visit – <a href='https://collegereadiness.collegeboard.org/sat/practice?excmpid=VT-00054' target='_blank' rel='noopener nofollow'>this link</a> to access a free official SAT practice app, and full-length SAT practice tests. These free, world-class practice resources have been developed by Khan Academy and the College Board, makers of the SAT<sup>&reg;</sup> test."
    },

    {
      title: "How to send your SAT scores directly to colleges?",
      desc:
        "Colleges require you to send official score reports directly from College Board. Before or after you take the SAT, you can choose to send your scores for free to all the India Global Alliance members. Additionally, during registration, you can choose up to four international universities for sending your scores for free. Watch this  <a href='https://www.youtube.com/watch?v=zkEibtF7y54&t=3s' target='_blank' rel='noopener nofollow'>video</a> to see how."
    },
    {
      title: "If I have any queries, who should I contact?",
      desc:
        "In case of queries, please reach out to Buddy4Study at  <a href='mailto:iga-help@buddy4study.com'> iga-help@buddy4study.com  </a> or  <a href='tel:+91-11-43092248'>+91-11-430-92248 (Ext: 118)</a> (Monday to Friday – 10:00AM to 6PM)"
    }
  ];

  return (
    <ul>
      {frequenstAskedQuestion.map((list, index) => (
        <li
          key={index}
          onClick={() => props.frequestAskedQuesHandler(index)}
          className={props.showItem == index ? "active" : ""}
        >
          <h5>{list.title}</h5>
          <p
            dangerouslySetInnerHTML={{
              __html: list.desc
            }}
          />
        </li>
      ))}
    </ul>
  );
};

export default CollegeBoard;
