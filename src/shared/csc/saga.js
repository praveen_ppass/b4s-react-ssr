import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_CSC_URL_REQUESTED,
  FETCH_CSC_URL_SUCCEEDED,
  FETCH_CSC_URL_FAILED,
  FETCH_CSC_USER_DETAILS_REQUESTED,
  FETCH_CSC_USER_DETAILS_SUCCEEDED,
  FETCH_CSC_USER_DETAILS_FAILED,
  FETCH_DMDC_REQUESTED,
  FETCH_DMDC_SUCCEEDED,
  FETCH_DMDC_FAILED,
  FETCH_USER_ID_BY_TRANSACTION_ID_REQUESTED,
  FETCH_USER_ID_BY_TRANSACTION_ID_FAILED,
  FETCH_USER_ID_BY_TRANSACTION_ID_SUCCEEDED
} from "./actions";
//apiUrl.getCscUrl
const fetchCSCUrl = () =>
  fetchClient.get(apiUrl.getCscUrl).then(res => {
    return res.data;
  });
function* fetchCSC(input) {
  try {
    const cscUrl = yield call(fetchCSCUrl);
    yield put({
      type: FETCH_CSC_URL_SUCCEEDED,
      payload: cscUrl
    });
  } catch (error) {
    yield put({
      type: FETCH_CSC_URL_FAILED,
      payload: error
    });
  }
}
//apiUrl.cscUserDetails
const fetchUserDetails = ({ inputData }) =>
  fetchClient
    .post(apiUrl.cscUserDetails, {
      code: inputData.code
    })
    .then(res => {
      return res.data;
    });

function* fetchUserCscDt({ payload }) {
  try {
    const cscUserDt = yield call(fetchUserDetails, payload);
    yield put({
      type: FETCH_CSC_USER_DETAILS_SUCCEEDED,
      payload: cscUserDt
    });
  } catch (error) {
    yield put({
      type: FETCH_CSC_USER_DETAILS_FAILED,
      payload: error
    });
  }
}

//apiUrl.fetchDMDC
const fetchDMDCAPI = ({ userId, isOther }) =>
  fetchClient
    .get(`${apiUrl.dmdcApi}/${userId}/vle/dmdc?other=${isOther}`)
    .then(res => {
      return res.data;
    });

function* fetchDMDC({ payload }) {
  try {
    const dmdc = yield call(fetchDMDCAPI, payload.inputData);
    yield put({
      type: FETCH_DMDC_SUCCEEDED,
      payload: dmdc
    });
  } catch (error) {
    yield put({
      type: FETCH_DMDC_FAILED,
      payload: error
    });
  }
}

// Get Student Info Using Payment Transaction Id
const fetchCscStudentInfoAPI = ({ decodedTransactionId }) =>
  fetchClient
    .get(
      `${
        apiUrl.cscPaymntSuccTransactionId
      }/?merchantTransactionNumber=${decodedTransactionId}`
    )
    .then(res => {
      return res.data;
    });

function* fetchCscStudentInfo({ payload }) {
  try {
    const trancInfo = yield call(fetchCscStudentInfoAPI, payload.inputData);
    yield put({
      type: FETCH_USER_ID_BY_TRANSACTION_ID_SUCCEEDED,
      payload: trancInfo
    });
  } catch (error) {
    yield put({
      type: FETCH_USER_ID_BY_TRANSACTION_ID_FAILED,
      payload: error
    });
  }
}

export default function* fetchCSCSaga() {
  yield takeEvery(FETCH_CSC_URL_REQUESTED, fetchCSC);
  yield takeEvery(FETCH_CSC_USER_DETAILS_REQUESTED, fetchUserCscDt);
  yield takeEvery(FETCH_DMDC_REQUESTED, fetchDMDC);
  yield takeEvery(
    FETCH_USER_ID_BY_TRANSACTION_ID_REQUESTED,
    fetchCscStudentInfo
  );
}
