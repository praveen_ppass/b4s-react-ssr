import {
  FETCH_CSC_URL_REQUESTED,
  FETCH_CSC_URL_SUCCEEDED,
  FETCH_CSC_URL_FAILED,
  FETCH_CSC_USER_DETAILS_REQUESTED,
  FETCH_CSC_USER_DETAILS_SUCCEEDED,
  FETCH_CSC_USER_DETAILS_FAILED,
  FETCH_USER_ID_BY_TRANSACTION_ID_REQUESTED,
  FETCH_USER_ID_BY_TRANSACTION_ID_SUCCEEDED,
  FETCH_USER_ID_BY_TRANSACTION_ID_FAILED,
  FETCH_DMDC_REQUESTED,
  FETCH_DMDC_SUCCEEDED,
  FETCH_DMDC_FAILED,
  DEFAULT_ACTION_REQUESTED
} from "./actions";

import {
  SOCIAL_LOGIN_USER_REQUESTED,
  LOGIN_USER_SUCCEEDED,
  SOCIAL_LOGIN_USER_FAILED
} from "../login/actions";

const initialState = { showLoader: true, cscURL: null };
const cscReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_CSC_URL_REQUESTED:
      return Object.assign({}, state, {
        type: type
      });
    case FETCH_CSC_URL_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        cscURL: payload,
        type: type
      });
    case FETCH_CSC_URL_FAILED:
      return { ...state, showLoader: false, type, isError: true };

    // csc user details

    case FETCH_CSC_USER_DETAILS_REQUESTED:
      return Object.assign({}, state, {
        type: type
      });
    case FETCH_CSC_USER_DETAILS_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        cscUserDetails: payload,
        type: type
      });
    case FETCH_CSC_USER_DETAILS_FAILED:
      return { ...state, showLoader: false, type, isError: true };

    // TOKEN GENERATE
    case SOCIAL_LOGIN_USER_REQUESTED:
      return { ...state, type, showLoader: true };

    case LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload,
        isAuthenticated: true
      };

    case SOCIAL_LOGIN_USER_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        type,
        serverErrorLogin: payload
      };

    // dmdc details

    case FETCH_DMDC_REQUESTED:
      return Object.assign({}, state, {
        type: type
      });
    case FETCH_DMDC_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        dmdcDetails: payload,
        type: type
      });
    case FETCH_DMDC_FAILED:
      return { ...state, showLoader: false, type, isError: true };

    // transaction details

    case FETCH_USER_ID_BY_TRANSACTION_ID_REQUESTED:
      return Object.assign({}, state, {
        type: type
      });
    case FETCH_USER_ID_BY_TRANSACTION_ID_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        cscPaymentUserInfo: payload,
        type: type
      });
    case FETCH_USER_ID_BY_TRANSACTION_ID_FAILED:
      return { ...state, showLoader: false, type, isError: true };
    case DEFAULT_ACTION_REQUESTED:
      return { ...state, showLoader: false, type, isError: true };
    default:
      return state;
  }
};
export default cscReducer;
