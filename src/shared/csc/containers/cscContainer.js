import React from "react";
import { connect } from "react-redux";
import {
  fetchCscUrl as fetchCscUrlAction,
  fetchCscUserDetails as fetchCscUserDetailsAction,
  fetchCscDMDM as fetchDMDCAction,
  fetchStudentUserId as fetchStudentUserIdAction,
  defualtCSC as defualtCSCAction
} from "../actions";

import { socialLoginUser as socialLoginUserAction } from "../../login/actions";
import CSC from "../components/csc";

const mapStateToProps = ({ csc, loginOrRegister }) => ({
  cscUrl: csc.cscURL,
  showLoader: csc.showLoader,
  userDtCSC: csc.cscUserDetails,
  cscTokenInfo: loginOrRegister.userLoginData,
  type: csc.type,
  dmdcDetails: csc.dmdcDetails,
  CscTransDts: csc.cscPaymentUserInfo
});
const mapDispatchToProps = dispatch => ({
  loadCscURl: () => dispatch(fetchCscUrlAction("pageData")),
  loadCscUserDt: data => dispatch(fetchCscUserDetailsAction(data)),
  loadCscToken: data => dispatch(socialLoginUserAction(data)),
  getDMDC: data => dispatch(fetchDMDCAction(data)),
  getCscStudentTransInfo: data => dispatch(fetchStudentUserIdAction(data)),
  defualtAction: data => dispatch(defualtCSCAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(CSC);
