import React, { Component } from "react";
import { Link } from "react-router-dom";
import queryString from "query-string";
import BreadCrum from "../../components/bread-crum/breadCrum";
import { breadCrumObj } from "../../../constants/breadCrum";
import { imgBaseUrl, decode } from "../../../constants/constants";
import {
  FETCH_CSC_USER_DETAILS_SUCCEEDED,
  FETCH_DMDC_SUCCEEDED,
  FETCH_USER_ID_BY_TRANSACTION_ID_SUCCEEDED,
  FETCH_USER_ID_BY_TRANSACTION_ID_FAILED
} from "../actions";
import { LOGIN_USER_SUCCEEDED } from "../../login/actions";
import gblFunc from "../../../globals/globalFunctions";
import ThankSubscription from "../../common/subscription-successful";
import ConfirmSubscription from "../../common/confirm-subscription";
import NotfoundPage from "../../common/error-404";
import AlertMessage from "../../common/components/alertMsg";

class CSC extends Component {
  constructor(props) {
    super(props);
    this.onCscUserDetailHandler = this.onCscUserDetailHandler.bind(this);
    this.showPopup = this.showPopup.bind(this);
    this.hidePopup = this.hidePopup.bind(this);
    this.getDMDC = this.getDMDC.bind(this);
    this.onShowPaymentMsgHandle = this.onShowPaymentMsgHandle.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.state = {
      onFirstLoad: true,
      isShowPopup: false,
      cscSuccess: null,
      showAlert: false,
      msg: ""
    };
  }

  componentDidMount() {
    if (this.props.match.path == "/csc/login") {
      this.props.loadCscURl();
    }
    if (this.props.match.path === "/csc/payment-success") {
      const transactionId = location.search.replace("?transactionId=", "");

      const decodedTransactionId = decode(transactionId);
      this.props.getCscStudentTransInfo({
        decodedTransactionId
      });
    }
  }

  onCscUserDetailHandler(code) {
    this.setState({ onFirstLoad: false }, () =>
      this.props.loadCscUserDt({ code })
    );
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    switch (type) {
      case FETCH_CSC_USER_DETAILS_SUCCEEDED:
        let objectData = {
          username: nextProps.userDtCSC.email,
          grant_type: "password",
          socialLogin: true,
          csc: true
        };
        gblFunc.storeUserDetails(nextProps.userDtCSC);
        this.props.loadCscToken(objectData);
        break;
      case "VERIFY_TOKEN":
        if (
          nextProps.userLoginData &&
          nextProps.userLoginData.data &&
          nextProps.userLoginData.data.data &&
          nextProps.userLoginData.data.data.mobileVerified == "0"
        ) {
          this.props.history.push({
            pathname: "/otp",
            state: {
              userId: nextProps.userLoginData.data.data.userId,
              mobile: nextProps.userLoginData.data.data.mobile,
              countryCode: nextProps.userLoginData.data.data.countryCode,
              temp_Token: nextProps.userLoginData.data.data.token,
              location:
                this.props.location && this.props.location.pathname
                  ? this.props.location.pathname
                  : null
            }
          });
        }
        break;
      case LOGIN_USER_SUCCEEDED:
        gblFunc.storeAuthDetails(nextProps.cscTokenInfo);
        if (this.props.match.path == "/csc/login") {
          this.getDMDC();
        }
        break;
      case FETCH_DMDC_SUCCEEDED:
        if (nextProps.dmdcDetails && nextProps.dmdcDetails.id) {
          this.props.history.push("/subscribers");
        } else {
          this.props.history.push("/csc/myprofile");
        }
        break;
      case FETCH_USER_ID_BY_TRANSACTION_ID_SUCCEEDED:
        const { paidForUserId } = nextProps.CscTransDts;
        this.setState(
          {
            userId: paidForUserId
          },
          () => this.props.defualtAction("default")
        );
        break;
      case FETCH_USER_ID_BY_TRANSACTION_ID_FAILED:
        this.props.defualtAction("default");
        break;
    }
  }
  showPopup() {
    this.setState({
      isShowPopup: true
    });
  }
  hidePopup() {
    this.setState({
      isShowPopup: false
    });
  }

  getDMDC(userId) {
    this.props.getDMDC({
      userId: gblFunc.getStoreUserDetails()["userId"],
      isOther: true
    });
  }

  hideAlert() {
    this.setState({ msg: "", showAlert: false }, () =>
      this.props.history.push("/myscholarship/?tab=application-status")
    );
  }

  onShowPaymentMsgHandle() {
    this.setState({
      showAlert: true,
      msg:
        "Your request for SAT Fee Discount Voucher is accepted. You will receive your voucher on your registered e-mail ID in next 2 working days."
    });
  }

  render() {
    const { match, location, cscUrl, cscUserDetails } = this.props;
    let cscPage = null;
    switch (match.path) {
      case "/csc/login":
        if (location.search != "") {
          cscPage = <CSC_FRONT />;
          let splitQuery = location.search.split("&");
          const code = splitQuery[0].replace("?code=", "");
          if (this.state.onFirstLoad) {
            this.onCscUserDetailHandler(code);
          }
        } else {
          cscPage = (
            <CSC_FRONT
              hidePopup={this.hidePopup}
              showPopup={this.showPopup}
              isShowPopup={this.state.isShowPopup}
              cscurl={cscUrl}
            />
          );
        }
        break;
      case "/csc/payment-success":
        if (location.search != "") {
          const transactionId = location.search.replace("?transactionId=", "");

          if (this.state.userId) {
            cscPage = (
              <CSC_SUCCESS
                transactionId={decode(transactionId)}
                userId={this.state.userId}
              />
            );
          }
        }
        break;
      case "/csc/payment-cancel":
        if (location.search != "") {
          const transactionId = location.search.replace("?transactionId=", "");
          cscPage = <CSC_CANCEL transactionId={decode(transactionId)} />;
        }

        break;
      case "/payment-complete":
        if (location.search) {
          const transactionId = queryString.parse(location.search)[
            "payment_id"
          ];
          const transactionStatus = queryString.parse(location.search)[
            "status"
          ];
          //userFlag - 0: Normal user..
          cscPage =
            transactionStatus == "success" ? (
              <CSC_SUCCESS
                onShowPaymentMsgHandle={this.onShowPaymentMsgHandle}
                transactionId={transactionId}
                userFlag="0"
              />
            ) : (
                <CSC_CANCEL transactionId={transactionId} userFlag="0" />
              );
        }
        break;

        break;
      case "/thank-your-subscription":
        cscPage = <ThankSubscription />;
        break;
      case "/confirm-newsletter-subscription":
        cscPage = <ConfirmSubscription />;
        break;
      case "/404":
        cscPage = <NotfoundPage />;
        break;
    }

    return (
      <section>
        <AlertMessage
          isShow={this.state.showAlert}
          msg={this.state.msg}
          status={true}
          close={this.hideAlert}
        />
        {match.path === "/csc/login" ? null : ""}
        {cscPage}
      </section>
    );
  }
}

const CSC_FRONT = ({ cscurl, isShowPopup, hidePopup, showPopup }) => {
  return (
    <section>
      <article className={isShowPopup ? "videoOverlay" : "hide"}>
        <article className="videoContainer">
          <button type="button" className="close" onClick={() => hidePopup()}>
            <i>&times;</i>
          </button>
          <video controls>
            <source
              src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/CSC.mp4"
              type="video/mp4"
            />
            Display this message, if your browser does not support video tag.
          </video>
        </article>
      </article>
      <section className="container-fuild">
        <section className="container">
          <section className="row">
            <section className="col-md-12 cell-padding">
              <article className="text-center">
                <a href="/">
                  <img
                    height="60"
                    src={`${imgBaseUrl}white-logo.png`}
                    alt="Buddy4study – Gateway to scholarship world"
                    id="logo-b4s"
                    className="logo"
                  />
                </a>
                <h2>INDIA'S LARGEST SCHOLARSHIP PLATFORM</h2>
                <p>Making Education Affordable</p>
              </article>
              <article className="listtyoP">
                <p>Start adding students and find them scholarships</p>
                <ul>
                  <li>
                    {" "}
                    We hand pick every scholarship listed on our portal for you
                  </li>
                  <li>
                    {" "}
                    You will only see scholarships you stand a chance to win{" "}
                  </li>
                  <li>
                    {" "}
                    We will help you fill out the scholarship application
                    step-by-step{" "}
                  </li>
                  <li>
                    {" "}
                    We track your applications so that you can apply for more
                    scholarships{" "}
                  </li>
                  <li>
                    {" "}
                    You will never miss out on a matching scholarship again
                  </li>
                </ul>
                <article className="buttonWrapper">
                  {cscurl && cscurl.url ? (
                    <a href={cscurl.url}>Login with csc portal</a>
                  ) : null}
                  <button className="cscHelpVideo" onClick={() => showPopup()}>
                    Help
                  </button>
                </article>
              </article>
            </section>
          </section>
        </section>
      </section>
    </section>
  );
};

const CSC_SUCCESS = ({
  transactionId,
  userFlag,
  userId,
  onShowPaymentMsgHandle
}) => {
  const scholarshipId = gblFunc.getStoreApplicationScholarshipId();
  const schArray = ["10936", "10939"];
  let okLink = null;

  if (schArray.indexOf(scholarshipId) !== -1) {
    okLink = (
      <a onClick={() => onShowPaymentMsgHandle()} className="btn noShadow">
        OK
      </a>
    );
  } else {
    okLink = (
      <Link
        to={userFlag == "0" ? "/myscholarship" : "/subscribers"}
        className="btn noShadow"
      >
        OK
      </Link>
    );
  }
  return (
    <section className="thankspage text-center">
      <img src={`${imgBaseUrl}icon-thanks.jpg`} alt="" />
      <h2>Payment Successful!</h2>
      <p>
        Thank you! We have successfully received your payment.
        <br />
        Your Transaction ID is {transactionId}.
      </p>
      {userId ? (
        <p>
          Please{" "}
          <Link
            to={{ pathname: "/subscribers", state: { cscUser: { userId } } }}
          >
            click here
          </Link>{" "}
          to view matching scholarships for this payment.
        </p>
      ) : null}
      {okLink}
    </section>
  );
};

const CSC_CANCEL = ({ transactionId, userFlag }) => {
  return (
    <section className="thankspage text-center">
      <img src={`${imgBaseUrl}icon-thanks1.jpg`} alt="" />
      <h2>Payment Cancelled</h2>
      <p>
        Your payment has been cancelled. Your Transaction ID is {transactionId}.
        <br /> Please email us at info@buddy4study.com in case of any query.
      </p>
      <Link
        to={userFlag == "0" ? "/myscholarship" : "/subscribers"}
        className="btn noShadow"
      >
        Make another payment
      </Link>
    </section>
  );
};

export default CSC;
