export const FETCH_CSC_URL_REQUESTED = "FETCH_CSC_URL_REQUESTED";
export const FETCH_CSC_URL_SUCCEEDED = "FETCH_CSC_URL_SUCCEEDED";
export const FETCH_CSC_URL_FAILED = "FETCH_CSC_URL_FAILED";

export const fetchCscUrl = data => ({
  type: FETCH_CSC_URL_REQUESTED,
  payload: { data: data }
});

/** CSC USER DETAILS */
export const FETCH_CSC_USER_DETAILS_REQUESTED =
  "FETCH_CSC_USER_DETAILS_REQUESTED";
export const FETCH_CSC_USER_DETAILS_SUCCEEDED =
  "FETCH_CSC_USER_DETAILS_SUCCEEDED";
export const FETCH_CSC_USER_DETAILS_FAILED = "FETCH_CSC_USER_DETAILS_FAILED";

export const fetchCscUserDetails = data => ({
  type: FETCH_CSC_USER_DETAILS_REQUESTED,
  payload: {
    inputData: data
  }
});

/** DMDC details */
export const FETCH_DMDC_REQUESTED = "FETCH_DMDC_REQUESTED";
export const FETCH_DMDC_SUCCEEDED = "FETCH_DMDC_SUCCEEDED";
export const FETCH_DMDC_FAILED = "FETCH_DMDC_FAILED";

export const fetchCscDMDM = data => ({
  type: FETCH_DMDC_REQUESTED,
  payload: {
    inputData: data
  }
});

/** PAYMENT TRANSACTION ID API */

export const FETCH_USER_ID_BY_TRANSACTION_ID_REQUESTED =
  "FETCH_USER_ID_BY_TRANSACTION_ID_REQUESTED";
export const FETCH_USER_ID_BY_TRANSACTION_ID_SUCCEEDED =
  "FETCH_USER_ID_BY_TRANSACTION_ID_SUCCEEDED";
export const FETCH_USER_ID_BY_TRANSACTION_ID_FAILED =
  "FETCH_USER_ID_BY_TRANSACTION_ID_FAILED";

export const fetchStudentUserId = data => ({
  type: FETCH_USER_ID_BY_TRANSACTION_ID_REQUESTED,
  payload: {
    inputData: data
  }
});

/** DEFAULT ACTION */

export const DEFAULT_ACTION_REQUESTED = "DEFAULT_ACTION_REQUESTED";
export const DEFAULT_ACTION_SUCCEEDED = "DEFAULT_ACTION_SUCCEEDED";
export const DEFAULT_ACTION_FAILED = "DEFAULT_ACTION_FAILED";

export const defualtCSC = data => ({
  type: DEFAULT_ACTION_REQUESTED,
  payload: {
    inputData: data
  }
});
