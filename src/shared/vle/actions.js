// /* Interest form actions */

// export const FETCH_USER_SCHOLARSHIP_INTERESTS_REQUEST =
//   "FETCH_USER_SCHOLARSHIP_INTERESTS_REQUEST";

// export const FETCH_USER_SCHOLARSHIP_INTERESTS_SUCCESS =
//   "FETCH_USER_SCHOLARSHIP_INTERESTS_SUCCESS";

// export const FETCH_USER_SCHOLARSHIP_INTERESTS_FAILURE =
//   "FETCH_USER_SCHOLARSHIP_INTERESTS_FAILURE";

// export const fetchUserScholarshipInterests = data => ({
//   type: FETCH_USER_SCHOLARSHIP_INTERESTS_REQUEST,
//   payload: { inputData: data }
// });

/* MY SUBSCRIBER (ADD STUDENT) */

export const VLE_UPDATE_STUDENT_REQUEST = "VLE_UPDATE_STUDENT_REQUEST";

export const VLE_UPDATE_STUDENT_SUCCESS = "VLE_UPDATE_STUDENT_SUCCESS";

export const VLE_UPDATE_STUDENT_FAIL = "VLE_UPDATE_STUDENT_FAIL";

export const updateStudent = data => ({
  type: VLE_UPDATE_STUDENT_REQUEST,
  payload: { inputData: data }
});

/* ADD STUDENT (GET STUDENT DETAILS USING PARENT USERID AND STUDENTID) */

export const VLE_FETCH_STUDENT_DETAILS_REQUEST =
  "VLE_FETCH_STUDENT_DETAILS_REQUEST";

export const VLE_FETCH_STUDENT_DETAILS_SUCCESS =
  "VLE_FETCH_STUDENT_DETAILS_SUCCESS";

export const VLE_FETCH_STUDENT_DETAILS_FAIL = "VLE_FETCH_STUDENT_DETAILS_FAIL";

export const fetchStudentDetails = data => ({
  type: VLE_FETCH_STUDENT_DETAILS_REQUEST,
  payload: { inputData: data }
});

/* GET STUDENT LIST */
export const VLE_FETCH_STUDENT_LIST_REQUEST = "VLE_FETCH_STUDENT_LIST_REQUEST";
export const VLE_FETCH_STUDENT_LIST_SUCCESS = "VLE_FETCH_STUDENT_LIST_SUCCESS";
export const VLE_FETCH_STUDENT_LIST_FAIL = "VLE_FETCH_STUDENT_LIST_FAIL";

export const fetchStudentLists = data => ({
  type: VLE_FETCH_STUDENT_LIST_REQUEST,
  payload: { inputData: data }
});

/* EDIT STUDENT LIST */
export const VLE_EDIT_STUDENT_DETAILS_REQUEST =
  "VLE_EDIT_STUDENT_DETAILS_REQUEST";
export const VLE_EDIT_STUDENT_DETAILS_SUCCESS =
  "VLE_EDIT_STUDENT_DETAILS_SUCCESS";
export const VLE_EDIT_STUDENT_DETAILS_FAIL = "VLE_EDIT_STUDENT_DETAILS_FAIL";

export const editStudentDetails = data => ({
  type: VLE_EDIT_STUDENT_DETAILS_REQUEST,
  payload: { inputData: data }
});

/* Matched Scholarships */

export const VLE_FETCH_MATCHING_SCHOLARSHIPS_REQUEST =
  "VLE_FETCH_MATCHING_SCHOLARSHIPS_REQUEST";
export const VLE_FETCH_MATCHING_SCHOLARSHIPS_SUCCESS =
  "VLE_FETCH_MATCHING_SCHOLARSHIPS_SUCCESS";
export const VLE_FETCH_MATCHING_SCHOLARSHIPS_FAIL =
  "VLE_FETCH_MATCHING_SCHOLARSHIPS_FAIL";

export const fetchMatchingScholarships = data => ({
  type: VLE_FETCH_MATCHING_SCHOLARSHIPS_REQUEST,
  payload: { inputData: data }
});

/*MEARCHANT ID API */
export const FETCH_MARCHANT_ID_REQUEST = "FETCH_MARCHANT_ID_REQUEST";
export const FETCH_MARCHANT_ID_SUCCESS = "FETCH_MARCHANT_ID_SUCCESS";
export const FETCH_MARCHANT_ID_FAIL = "FETCH_MARCHANT_ID_FAIL";

export const merchantId = input => ({
  type: FETCH_MARCHANT_ID_REQUEST,
  payload: {
    inputData: input
  }
});

/*************************************************************** */
/********** Start code for VLE payament plans @ Pushpendra****************** */
/*************************************************************** */
export const FETCH_VLE_PAYMENT_PLANS_REQUEST =
  "FETCH_VLE_PAYMENT_PLANS_REQUEST";
export const FETCH_VLE_PAYMENT_PLANS_SUCCESS =
  "FETCH_VLE_PAYMENT_PLANS_SUCCESS";
export const FETCH_VLE_PAYMENT_PLANS_FAILURE =
  "FETCH_VLE_PAYMENT_PLANS_FAILURE";
export const fetchPaymentPlans = (input, type) => ({
  type: FETCH_VLE_PAYMENT_PLANS_REQUEST,
  payload: { inputData: input, type }
});
/*************************************************************** */
/************ End code for VLE payment plans @ Pushpendra******************** */
/*************************************************************** */
