import {
  //Student actions
  VLE_UPDATE_STUDENT_FAIL,
  VLE_UPDATE_STUDENT_REQUEST,
  VLE_UPDATE_STUDENT_SUCCESS,
  VLE_FETCH_STUDENT_DETAILS_REQUEST,
  VLE_FETCH_STUDENT_DETAILS_SUCCESS,
  VLE_FETCH_STUDENT_DETAILS_FAIL,
  VLE_FETCH_STUDENT_LIST_REQUEST,
  VLE_FETCH_STUDENT_LIST_SUCCESS,
  VLE_FETCH_STUDENT_LIST_FAIL,

  // Matching Scholarship actions
  VLE_FETCH_MATCHING_SCHOLARSHIPS_REQUEST,
  VLE_FETCH_MATCHING_SCHOLARSHIPS_SUCCESS,
  VLE_FETCH_MATCHING_SCHOLARSHIPS_FAIL,
  FETCH_MARCHANT_ID_REQUEST,
  FETCH_MARCHANT_ID_SUCCESS,
  FETCH_MARCHANT_ID_FAIL,

  /** Vle payment plans...................... */
  FETCH_VLE_PAYMENT_PLANS_REQUEST,
  FETCH_VLE_PAYMENT_PLANS_SUCCESS,
  FETCH_VLE_PAYMENT_PLANS_FAILURE,
  VLE_EDIT_STUDENT_DETAILS_REQUEST,
  VLE_EDIT_STUDENT_DETAILS_SUCCESS,
  VLE_EDIT_STUDENT_DETAILS_FAIL
} from "./actions";

import {
  GET_ALL_DOCUMENT_REQUEST,
  GET_ALL_DOCUMENT_SUCCESS,
  GET_ALL_DOCUMENT_FAILURE,
  GET_ALL_DOCUMENT_UPLOAD_REQUEST,
  GET_ALL_DOCUMENT_UPLOAD_SUCCESS,
  GET_ALL_DOCUMENT_UPLOAD_FAILURE,
  GET_ALL_DOCUMENT_UPLOAD_DELETE_REQUEST,
  GET_ALL_DOCUMENT_UPLOAD_DELETE_SUCCESS,
  GET_ALL_DOCUMENT_UPLOAD_DELETE_FAILURE,
  GET_ALL_DOCUMENT_UPLOAD_COMPLETE_REQUEST,
  GET_ALL_DOCUMENT_UPLOAD_COMPLETE_SUCCESS,
  GET_ALL_DOCUMENT_UPLOAD_COMPLETE_FAILURE
} from "../application/actions/applicationDocumentAction";

import {
  FETCH_RULES_REQUESTED,
  FETCH_RULES_SUCCEEDED,
  FETCH_RULES_FAILED,
  FETCH_USER_RULES_SUCCESS,
  UPDATE_USER_RULES_REQUEST,
  FETCH_USER_RULES_REQUEST,
  FETCH_USER_MATCHING_RULES_REQUESTED,
  FETCH_USER_MATCHING_RULES_SUCCEEDED
} from "../../constants/commonActions";

import {
  SAVE_SCHOLARSHIP_TYPE_REQUEST,
  SAVE_SCHOLARSHIP_TYPE_SUCCESS,
  SAVE_SCHOLARSHIP_TYPE_FAILURE
} from "../application/actions/applicationInstructionActions";

import { LOG_USER_OUT } from "../login/actions";

const initialState = {
  userFamilyEarningData: [],
  entranceExaminationData: [],
  scholarshipHistoryData: [],
  referencesData: [],
  occupationData: [],
  userBankDetailsData: [],
  documentGroupsAndTypes: [],
  educationData: [],
  userDocumentsData: [],
  updatedUserDocumentsData: [],
  documentTypeCategories: [],
  documentTypeCategoriesOptions: [],
  isError: false,
  errorMessage: "",
  updateError: "",
  isUpdateError: false,
  isAuthenticated: true,
  showLoader: true,
  serverError: "",
  isServerError: false,
  paymentPlans: []
};

const marchantReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    /* Common rules section */
    case FETCH_RULES_REQUESTED:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });
    case FETCH_RULES_SUCCEEDED:
      return { ...state, showLoader: false, type };

    case FETCH_RULES_FAILED:
      return { ...state, showLoader: false, type, isError: true };

    /* Interest form user rules request*/
    case FETCH_USER_RULES_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_USER_RULES_SUCCESS:
      return { ...state, showLoader: false, userRulesData: payload, type };

    case UPDATE_USER_RULES_REQUEST:
      return { ...state, showLoader: true, type };

    case VLE_UPDATE_STUDENT_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        serverError: "",
        isServerError: false
      };

    case VLE_UPDATE_STUDENT_SUCCESS:
      return { ...state, showLoader: false, type, studentData: payload };

    case VLE_UPDATE_STUDENT_FAIL:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        isServerError: payload.isServerError,
        serverError: payload.errorMessage,
        errorCode: payload.errorCode
      };

    case VLE_FETCH_STUDENT_DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case VLE_FETCH_STUDENT_DETAILS_SUCCESS:
      return { ...state, showLoader: false, type, studentData: payload };

    case VLE_FETCH_STUDENT_DETAILS_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    case VLE_FETCH_STUDENT_LIST_REQUEST:
      return { ...state, showLoader: true, type };

    case VLE_FETCH_STUDENT_LIST_SUCCESS:
      return { ...state, showLoader: false, type, studentData: payload };

    case VLE_FETCH_STUDENT_LIST_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    // Matching Scholarship

    case VLE_FETCH_MATCHING_SCHOLARSHIPS_REQUEST:
      return { ...state, showLoader: true, type };

    case VLE_FETCH_MATCHING_SCHOLARSHIPS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        matchingScholarships: payload
      };

    case VLE_FETCH_MATCHING_SCHOLARSHIPS_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    case FETCH_USER_MATCHING_RULES_REQUESTED:
      return { ...state, showLoader: true, type };
    case FETCH_USER_MATCHING_RULES_SUCCEEDED:
      return { ...state, showLoader: false, type, matchingRules: payload };

    case FETCH_MARCHANT_ID_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_MARCHANT_ID_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        marchantId: payload
      };

    case FETCH_MARCHANT_ID_FAIL:
      return {
        ...state,
        showLoader: false,
        type,
        isError: true,
        errorCode: payload.errorCode ? payload.errorCode : null
      };
    /**** VLE Payment plans started********** */
    case FETCH_VLE_PAYMENT_PLANS_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_VLE_PAYMENT_PLANS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        type,
        paymentPlans: payload
      };

    case FETCH_VLE_PAYMENT_PLANS_FAILURE:
      return { ...state, showLoader: false, type, isError: true };
    /**** VLE Payment plans Ends********** */

    /* VLE DOCS UPLOAD */
    case GET_ALL_DOCUMENT_REQUEST:
      return {
        ...state,

        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case GET_ALL_DOCUMENT_SUCCESS:
      return {
        ...state,
        documentDataList: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case GET_ALL_DOCUMENT_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case GET_ALL_DOCUMENT_UPLOAD_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_SUCCESS:
      return {
        ...state,
        education: payload,
        showLoader: false,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_DELETE_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_DELETE_SUCCESS:
      return {
        ...state,
        education: payload,
        showLoader: false,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_DELETE_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_COMPLETE_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_COMPLETE_SUCCESS:
      return {
        ...state,
        education: payload,
        showLoader: false,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_COMPLETE_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type
      };
    case VLE_EDIT_STUDENT_DETAILS_REQUEST:
      return { ...state, showLoader: true, type };

    case VLE_EDIT_STUDENT_DETAILS_SUCCESS:
      return { ...state, showLoader: false, type, studentData: payload };

    case VLE_EDIT_STUDENT_DETAILS_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    case SAVE_SCHOLARSHIP_TYPE_REQUEST:
      return {
        ...state,
        showLoader: true,
        type
      };

    case SAVE_SCHOLARSHIP_TYPE_SUCCESS:
      return {
        ...state,
        schType: payload,
        type,
        showLoader: false
      };
    case SAVE_SCHOLARSHIP_TYPE_FAILURE:
      return {
        ...state,
        schType: payload,
        showLoader: false,
        isError: true,
        type
      };
    default:
      return state;
  }
};
export default marchantReducer;
