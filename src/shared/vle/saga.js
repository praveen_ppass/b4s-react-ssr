import axios from "axios";

import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import {
  apiUrl,
  apiCallTypes,
  baseUrls,
  messages
} from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  //my subscriber
  VLE_UPDATE_STUDENT_REQUEST,
  VLE_UPDATE_STUDENT_SUCCESS,
  VLE_UPDATE_STUDENT_FAIL,
  VLE_FETCH_STUDENT_DETAILS_REQUEST,
  VLE_FETCH_STUDENT_DETAILS_SUCCESS,
  VLE_FETCH_STUDENT_DETAILS_FAIL,
  VLE_FETCH_STUDENT_LIST_REQUEST,
  VLE_FETCH_STUDENT_LIST_SUCCESS,
  VLE_FETCH_STUDENT_LIST_FAIL,

  // Matching Scholarship
  VLE_FETCH_MATCHING_SCHOLARSHIPS_REQUEST,
  VLE_FETCH_MATCHING_SCHOLARSHIPS_SUCCESS,
  VLE_FETCH_MATCHING_SCHOLARSHIPS_FAIL,
  FETCH_MARCHANT_ID_REQUEST,
  FETCH_MARCHANT_ID_SUCCESS,
  FETCH_MARCHANT_ID_FAIL,

  /** Vle payment plans...................... */
  FETCH_VLE_PAYMENT_PLANS_REQUEST,
  FETCH_VLE_PAYMENT_PLANS_SUCCESS,
  FETCH_VLE_PAYMENT_PLANS_FAILURE,
  VLE_EDIT_STUDENT_DETAILS_FAIL,
  VLE_EDIT_STUDENT_DETAILS_REQUEST,
  VLE_EDIT_STUDENT_DETAILS_SUCCESS
} from "./actions";
import { UPDATE_USER_RULES_REQUEST } from "../../constants/commonActions";
import gblFunc from "../../globals/globalFunctions";

const fetchUpdateUrl = input =>
  fetchClient
    .post(apiUrl.updateUser + `/${input.userid}/personalInfo`, input.params)
    .then(res => {
      return res.data;
    });

function* fetchUpdateUser(input) {
  try {
    const userList = yield call(fetchUpdateUrl, input.payload);

    yield put({
      type: FETCH_UPDATE_USER_SUCCEEDED,
      payload: userList
    });
  } catch (error) {
    yield put({
      type: FETCH_UPDATE_USER_FAILED,
      payload: error.errorMessage
    });
  }
}

/* Scholarship history sagas */

/* scholarshipHistory */

const scholarshipHistoryApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.scholarshipHistory}/${
    inputData.userid
  }/scholarshipHistory`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case apiCallTypes.POST:
      return fetchClient.post(url, inputData.params).then(res => {
        return res.data;
      });

    case apiCallTypes.DELETE:
      return fetchClient
        .delete(`${url}/${inputData.scholarshipHistoryId}`)
        .then(res => {
          return res.data;
        });
  }
};

function* scholarshipHistory(input) {
  try {
    switch (input.type) {
      case FETCH_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
        const scholarshipHistoryData = yield call(
          scholarshipHistoryApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS,
          payload: scholarshipHistoryData
        });

        break;

      case UPDATE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
        const updatedScholarshipHistoryData = yield call(
          scholarshipHistoryApi,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: UPDATE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS,
          payload: updatedScholarshipHistoryData
        });

        break;

      case DELETE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
        yield call(scholarshipHistoryApi, input.payload, apiCallTypes.DELETE);

        yield put({
          type: DELETE_SCHOLARSHIP_HISTORY_DETAILS_SUCCESS
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
        yield put({
          type: FETCH_SCHOLARSHIP_HISTORY_DETAILS_FAILURE,
          payload: error
        });
        break;

      case UPDATE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
        yield put({
          type: UPDATE_SCHOLARSHIP_HISTORY_DETAILS_FAILURE,
          payload: error
        });
        break;

      case DELETE_SCHOLARSHIP_HISTORY_DETAILS_REQUEST:
        yield put({
          type: DELETE_SCHOLARSHIP_HISTORY_DETAILS_FAILURE,
          payload: error
        });
        break;
    }
  }
}

/* Family Earning Sagas */
/* GET /user/{userId}/family */
const familyEarningDetailsApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.familyEarningDetails}/${inputData.userid}/family`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case apiCallTypes.POST:
      return fetchClient.post(url, inputData.params).then(res => {
        return res.data;
      });

    case apiCallTypes.DELETE:
      return fetchClient.delete(`${url}/${inputData.familyId}`).then(res => {
        return res.data;
      });
  }
};

function* familyEarningDetails(input) {
  try {
    switch (input.type) {
      case FETCH_FAMILY_EARNING_DETAILS_REQUEST:
        const userFamilyEarningData = yield call(
          familyEarningDetailsApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_FAMILY_EARNING_DETAILS_SUCCESS,
          payload: userFamilyEarningData
        });

        break;

      case UPDATE_FAMILY_EARNING_DETAILS_REQUEST:
        const updatedUserFamilyEarningData = yield call(
          familyEarningDetailsApi,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: UPDATE_FAMILY_EARNING_DETAILS_SUCCESS,
          payload: updatedUserFamilyEarningData
        });

        break;

      case DELETE_FAMILY_EARNING_DETAILS_REQUEST:
        const deletedFamilyData = yield call(
          familyEarningDetailsApi,
          input.payload,
          apiCallTypes.DELETE
        );

        yield put({
          type: DELETE_FAMILY_EARNING_DETAILS_SUCCESS,
          payload: deletedFamilyData
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_FAMILY_EARNING_DETAILS_REQUEST:
        yield put({
          type: FETCH_FAMILY_EARNING_DETAILS_FAILURE,
          payload: error
        });
        break;

      case UPDATE_FAMILY_EARNING_DETAILS_REQUEST:
        yield put({
          type: UPDATE_FAMILY_EARNING_DETAILS_FAILURE,
          payload: error
        });
        break;

      case DELETE_FAMILY_EARNING_DETAILS_REQUEST:
        yield put({
          type: DELETE_FAMILY_EARNING_DETAILS_FAILURE,
          payload: error
        });
        break;
    }
  }
}

/* References section saga */

const referenceDetailsApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.referenceDetail}/${inputData.userid}/reference`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case apiCallTypes.POST:
      return fetchClient.post(url, inputData.params).then(res => {
        return res.data;
      });

    case apiCallTypes.DELETE:
      return fetchClient.delete(`${url}/${inputData.referenceId}`).then(res => {
        return res.data;
      });
  }
};

function* referenceDetails(input) {
  try {
    switch (input.type) {
      case FETCH_REFERENCE__DETAILS_REQUEST:
        const referencesData = yield call(
          referenceDetailsApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_REFERENCE__DETAILS_SUCCESS,
          payload: referencesData
        });

        break;

      case UPDATE_REFERENCE__DETAILS_REQUEST:
        const updatedReferenceData = yield call(
          referenceDetailsApi,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: UPDATE_REFERENCE__DETAILS_SUCCESS,
          payload: updatedReferenceData
        });

        break;

      case DELETE_REFERENCE__DETAILS_REQUEST:
        yield call(referenceDetailsApi, input.payload, apiCallTypes.DELETE);

        yield put({
          type: DELETE_REFERENCE__DETAILS_SUCCESS
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_REFERENCE__DETAILS_REQUEST:
        yield put({
          type: FETCH_REFERENCE__DETAILS_FAILURE,
          payload: error
        });
        break;

      case UPDATE_REFERENCE__DETAILS_REQUEST:
        yield put({
          type: UPDATE_REFERENCE__DETAILS_FAILURE,
          payload: error
        });
        break;

      case DELETE_REFERENCE__DETAILS_REQUEST:
        yield put({
          type: DELETE_REFERENCE__DETAILS_FAILURE,
          payload: error
        });
        break;
    }
  }
}

/* Entrance examination section saga */
const entranceExaminationApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.entranceExamination}/${inputData.userid}/entranceExam`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case apiCallTypes.POST:
      return fetchClient.post(url, inputData.params).then(res => {
        return res.data;
      });

    case apiCallTypes.DELETE:
      return fetchClient
        .delete(`${url}/${inputData.entranceExamId}`)
        .then(res => {
          return res.data;
        });
  }
};

function* entranceExaminationDetails(input) {
  try {
    switch (input.type) {
      case FETCH_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST:
        const entranceExaminationData = yield call(
          entranceExaminationApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_ENTRANCE_EXAMINATIONS_DETAILS_SUCCESS,
          payload: entranceExaminationData
        });

        break;

      case UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST:
        const updatedEntranceExaminationData = yield call(
          entranceExaminationApi,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_SUCCESS,
          payload: updatedEntranceExaminationData
        });

        break;

      case DELETE_ENTRANCE_EXAMINATION_DETAILS_REQUEST:
        yield call(entranceExaminationApi, input.payload, apiCallTypes.DELETE);

        yield put({
          type: DELETE_ENTRANCE_EXAMINATION_DETAILS_SUCCESS
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST:
        yield put({
          type: FETCH_ENTRANCE_EXAMINATIONS_DETAILS_FAILURE,
          payload: error
        });
        break;

      case UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_REQUEST:
        yield put({
          type: UPDATE_ENTRANCE_EXAMINATIONS_DETAILS_FAILURE,
          payload: error
        });
        break;

      case DELETE_ENTRANCE_EXAMINATION_DETAILS_REQUEST:
        yield put({
          type: DELETE_ENTRANCE_EXAMINATION_DETAILS_FAILURE,
          payload: error
        });
        break;
    }
  }
}

/* Occupation type saga */

const occupationDataApi = inputData => {
  return fetchClient.get(apiUrl.occupationData).then(res => {
    return res.data;
  });
};

function* occupationData(input) {
  try {
    const occupationData = yield call(
      occupationDataApi,
      input.payload,
      apiCallTypes.GET
    );

    yield put({
      type: FETCH_OCCUPATION_DATA_SUCCESS,
      payload: occupationData
    });
  } catch (error) {
    yield put({
      type: FETCH_OCCUPATION_DATA_FAILURE,
      payload: error
    });
  }
}

/* Education type saga */

const educationDataApi = ({ inputData }, TYPE) => {
  const url = `${apiUrl.educationInfo}/${inputData.userId}/educationalInfo`;

  switch (TYPE) {
    case apiCallTypes.GET:
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case apiCallTypes.POST:
      return fetchClient.post(url, [inputData.updateEducation]).then(res => {
        return res.data;
      });
  }
};

function* educationData(input) {
  try {
    switch (input.type) {
      case FETCH_EDUCATION_INFO_REQUEST:
        const educationData = yield call(
          educationDataApi,
          input.payload,
          apiCallTypes.GET
        );

        yield put({
          type: FETCH_EDUCATION_INFO_SUCCESS,
          payload: educationData
        });
        break;
      case UPDATE_EDUCATION_INFO_REQUEST:
        const saveEducationData = yield call(
          educationDataApi,
          input.payload,
          apiCallTypes.POST
        );
        yield put({
          type: UPDATE_EDUCATION_INFO_SUCCESS,
          payload: saveEducationData
        });
        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_EDUCATION_INFO_FAILURE:
        yield put({
          type: FETCH_EDUCATION_INFO_FAILURE,
          payload: error
        });
        break;
      case UPDATE_EDUCATION_INFO_FAILURE:
        yield put({
          type: FETCH_EDUCATION_INFO_FAILURE,
          payload: error
        });
        break;
    }
  }
}

/* ADD STUDENT (MY SUBSCRIBER) */
//${apiUrl.studentDetails}
const studentDetailsApi = ({ inputData }, TYPE) => {
  switch (TYPE) {
    case VLE_FETCH_STUDENT_DETAILS_REQUEST:
      return fetchClient
        .get(
          `${apiUrl.studentDetails}/${inputData.userId}/student/${
            inputData.studentId
          }`
        )
        .then(res => {
          return res.data;
        });

    case VLE_FETCH_STUDENT_LIST_REQUEST:
      let url = null;
      if (inputData && inputData.paymentPartnerId) {
        url = `${apiUrl.studentDetails}/${
          inputData.userId
        }/student?paymentPlanId=1&paymentPartner=VLE&page=${
          inputData.page
        }&length=${inputData.length}`;
      } else {
        url = `${apiUrl.studentDetails}/${
          inputData.userId
        }/student?&paymentPartner=VLE&page=${inputData.page}&length=${
          inputData.length
        }`;
      }
      return fetchClient.get(url).then(res => {
        return res.data;
      });

    case VLE_UPDATE_STUDENT_REQUEST:
      return fetchClient
        .post(
          `${apiUrl.studentDetails}/${inputData.userId}/student`,
          inputData.params
        )
        .then(res => {
          return res.data;
        });
    case VLE_EDIT_STUDENT_DETAILS_REQUEST:
      return fetchClient
        .post(
          `${apiUrl.studentDetails}/${inputData.userId}/student/${
            inputData.studentId
          }`,
          inputData.params
        )
        .then(res => {
          return res.data;
        });
  }
};

function* studentDetails(input) {
  try {
    switch (input.type) {
      case VLE_FETCH_STUDENT_DETAILS_REQUEST:
        const fetchStudentData = yield call(
          studentDetailsApi,
          input.payload,
          input.type
        );

        yield put({
          type: VLE_FETCH_STUDENT_DETAILS_SUCCESS,
          payload: fetchStudentData
        });

        break;

      case VLE_FETCH_STUDENT_LIST_REQUEST:
        const fetchStudentList = yield call(
          studentDetailsApi,
          input.payload,
          input.type
        );

        yield put({
          type: VLE_FETCH_STUDENT_LIST_SUCCESS,
          payload: fetchStudentList
        });

        break;

      case VLE_UPDATE_STUDENT_REQUEST:
        const updatedStudentData = yield call(
          studentDetailsApi,
          input.payload,
          input.type
        );

        yield put({
          type: VLE_UPDATE_STUDENT_SUCCESS,
          payload: updatedStudentData
        });

        break;
      case VLE_EDIT_STUDENT_DETAILS_REQUEST:
        const editStudentData = yield call(
          studentDetailsApi,
          input.payload,
          input.type
        );

        yield put({
          type: VLE_EDIT_STUDENT_DETAILS_SUCCESS,
          payload: editStudentData
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case VLE_FETCH_STUDENT_DETAILS_REQUEST:
        yield put({
          type: VLE_FETCH_STUDENT_DETAILS_FAIL,
          payload: error
        });
        break;
      case VLE_FETCH_STUDENT_LIST_REQUEST:
        yield put({
          type: VLE_FETCH_STUDENT_LIST_FAIL,
          payload: error
        });
        break;

      case VLE_UPDATE_STUDENT_REQUEST:
        if (error.response && error.response.data.errorCode) {
          yield put({
            type: VLE_UPDATE_STUDENT_FAIL,
            payload: {
              errorMessage: error.response.data.message,
              errorCode: error.response.data.errorCode,
              isServerError: true
            }
          });
        } else {
          yield put({
            type: VLE_UPDATE_STUDENT_FAIL,
            payload: {
              errorMessage: error.errorMessage,
              isServerError: false
            }
          });
        }
        break;
      case VLE_EDIT_STUDENT_DETAILS_REQUEST:
        yield put({
          type: VLE_EDIT_STUDENT_DETAILS_FAIL,
          payload: error
        });
        break;
    }
  }
}

/* MATCHING SCHOLARSHIPS */
const matchingscholarhipApi = ({ inputData }, TYPE) => {
  switch (TYPE) {
    case VLE_FETCH_MATCHING_SCHOLARSHIPS_REQUEST:
      return fetchClient
        .post(
          `${apiUrl.matchingScholarships}/recommendation?pool=assisted`,
          inputData
        )
        .then(res => {
          return res.data;
        });
  }
};

function* matchingScholarshipsDt(input) {
  try {
    switch (input.type) {
      case VLE_FETCH_MATCHING_SCHOLARSHIPS_REQUEST:
        const fetchMatchScholarships = yield call(
          matchingscholarhipApi,
          input.payload,
          input.type
        );

        yield put({
          type: VLE_FETCH_MATCHING_SCHOLARSHIPS_SUCCESS,
          payload: fetchMatchScholarships
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case VLE_FETCH_MATCHING_SCHOLARSHIPS_REQUEST:
        yield put({
          type: VLE_FETCH_MATCHING_SCHOLARSHIPS_FAIL,
          payload: error
        });
        break;
    }
  }
}

/* marchent id */

const fetchMarchantAPI = ({ inputData }, TYPE) => {
  const url = `${apiUrl.marchentCall}${inputData.userId}/payment`;

  switch (TYPE) {
    case apiCallTypes.POST:
      return fetchClient.post(url, inputData.marchentObj).then(res => res.data);
  }
};

function* fetchMarchant(input) {
  try {
    switch (input.type) {
      case FETCH_MARCHANT_ID_REQUEST:
        const fetchMarchant = yield call(
          fetchMarchantAPI,
          input.payload,
          apiCallTypes.POST
        );

        yield put({
          type: FETCH_MARCHANT_ID_SUCCESS,
          payload: fetchMarchant
        });

        break;
    }
  } catch (error) {
    switch (input.type) {
      case FETCH_MARCHANT_ID_REQUEST:
        yield put({
          type: FETCH_MARCHANT_ID_FAIL,
          payload: error.response.data
        });
        break;
    }
  }
}

/************************************************************** */
/************* start VLE payment plans@Pushpendra****************/
/************************************************************** */
function paymentPlansApi({ inputData, type }) {
  const url = inputData.parentPlanId
    ? `${apiUrl.vlePaymentPlans}/${type}?parentPlanId=${inputData.parentPlanId}`
    : `${apiUrl.vlePaymentPlans}/${type}`;
  return fetchClient.get(url).then(res => res.data);
}
function* fetchVlePaymentPlans(input) {
  try {
    const paymentPlans = yield call(paymentPlansApi, input.payload);
    yield put({ type: FETCH_VLE_PAYMENT_PLANS_SUCCESS, payload: paymentPlans });
  } catch (error) {
    yield put({ type: FETCH_VLE_PAYMENT_PLANS_FAILURE, payload: error });
  }
}
/************************************************************** */
/************* End VLE payment plans@Pushpendra******************/
/************************************************************** */

export default function* userProfileSaga() {
  /* Add Student (My Subscriber) */
  yield takeEvery(VLE_UPDATE_STUDENT_REQUEST, studentDetails);
  yield takeEvery(VLE_FETCH_STUDENT_DETAILS_REQUEST, studentDetails);
  yield takeEvery(VLE_EDIT_STUDENT_DETAILS_REQUEST, studentDetails);

  yield takeEvery(VLE_FETCH_STUDENT_LIST_REQUEST, studentDetails);

  /* Matching Scholarships (My Scholarships)*/

  yield takeEvery(
    VLE_FETCH_MATCHING_SCHOLARSHIPS_REQUEST,
    matchingScholarshipsDt
  );

  // marchant id
  yield takeEvery(FETCH_MARCHANT_ID_REQUEST, fetchMarchant);
  //vle payment plans....
  yield takeEvery(FETCH_VLE_PAYMENT_PLANS_REQUEST, fetchVlePaymentPlans);
}
