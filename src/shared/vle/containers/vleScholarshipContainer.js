import { connect } from "react-redux";
import vleScholarship from "../components/vleScholarshipComponent";

import {
  fetchRules as fetchRulesAction,
  fetchDependantData as fetchDependantAction,
  fetchUserMatchingRules as fetchUserMatchingRulesAction
} from "../../../constants/commonActions";

import {
  fetchMatchingScholarships as fetchScholarshipMatchingAction,
  fetchMyFavScholarships as fetchMyFavScholarshipsActions,
  addToFavScholarship as addToFavScholarshipActions,
  deleteFavScholarship as deleteFavScholarshipActions,
  applicationStatus as applicationStatusActions,
  fetchBookSlot as fetchBookSlotInfoAction,
  updateBookingSlot as updateBookSlotAction,
  fetchPreferLang as fetchPreferLangAction,
  // Upload User pic
  uploadUserPic as uploadUserPicAction,
  merchantId as getMerchantIdAction
} from "../actions";

const mapStateToProps = ({ common, marchant, loginOrRegister }) => ({
  matchingRules: common.matchingRules,
  studentData: marchant.studentData,
  type: marchant.type,
  matchingScholarships: marchant.matchingScholarships,
  favScholarships: marchant.favScholarships,
  deleteFavSch: marchant.deleteFavScholarships,
  applStatus: marchant.applicationStatus,
  showLoader: marchant.showLoader,
  bookSlot: marchant.bookSlot,
  isUpdateError: marchant.isUpdateError,
  updateError: marchant.updateError,
  preferLang: marchant.preferredLang,
  showLoader: marchant.showLoader,
  uploadPicData: marchant.uploadPicData,
  isAuthenticated: loginOrRegister.isAuthenticated,
  marchantID: marchant.marchantId
});

const mapDispatchToProps = dispatch => ({
  fetchMatchRules: inputData =>
    dispatch(fetchUserMatchingRulesAction(inputData)),
  fetchMatchingScholarships: inputData =>
    dispatch(fetchScholarshipMatchingAction(inputData)),
  fetchMyFavScholarships: inputData =>
    dispatch(fetchMyFavScholarshipsActions(inputData)),
  addToFavScholarships: inputData =>
    dispatch(addToFavScholarshipActions(inputData)),
  deleteFavScholarships: inputData =>
    dispatch(deleteFavScholarshipActions(inputData)),
  fetchApplicationStatus: inputData =>
    dispatch(applicationStatusActions(inputData)),
  fetchBookSlotInfo: inputData => dispatch(fetchBookSlotInfoAction(inputData)),
  updatingBookSlot: inputData => dispatch(updateBookSlotAction(inputData)),
  fetchPreferLanguage: inputData => dispatch(fetchPreferLangAction(inputData)),
  uploadPic: inputData => dispatch(uploadUserPicAction(inputData)),
  getMerchantId: inputData => dispatch(getMerchantIdAction(inputData))
});
export default connect(mapStateToProps, mapDispatchToProps)(vleScholarship);
