import { connect } from "react-redux";
import VleSubscriber from "../components/vleSubscriberComponent";

import {
  fetchRules as fetchRulesAction,
  fetchDependantData as fetchDependantAction,
  fetchUserMatchingRules as fetchUserMatchingRulesAction
} from "../../../constants/commonActions";

import {
  getAllDocuement as getAllDocuementAction,
  uploadAllDocuement as uploadAllDocuementAction,
  deleteDocuement as deleteDocuementAction,
  completeDocument as completeDocumentAction
} from "../../application/actions/applicationDocumentAction";

import {
  fetchMatchingScholarships as fetchScholarshipMatchingAction,
  updateStudent as updateStduentAction,
  fetchStudentDetails as fetchStudentDetailsAction,
  editStudentDetails as editStudentDetailsActions,
  fetchStudentLists as fetchStudentListAction,
  fetchMyFavScholarships as fetchMyFavScholarshipsActions,
  addToFavScholarship as addToFavScholarshipActions,
  // Upload User pic
  uploadUserPic as uploadUserPicAction,
  cscPaymentDetails as cscPaymentDetailsAction,
  cscRedirect as cscRedirectAction,
  merchantId as getMerchantIdAction,
  fetchPaymentPlans as fetchPaymentPlansAction
} from "../actions";

import { saveScholarshipType as saveScholarshipTypeAction } from "../../application/actions/applicationInstructionActions";

const mapStateToProps = ({
  common,
  marchant,
  loginOrRegister,
  applicationDocument,
  appInstr
}) => ({
  rulesList: common.rulesList,
  district: common.district,
  matchingRules: common.matchingRules,
  matchingScholarships: marchant.matchingScholarships,
  dependantData: common.dependantData,
  studentData: marchant.studentData,
  favScholarships: marchant.favScholarships,
  type: marchant.type,
  districtType: common.type,
  showLoader: marchant.showLoader,
  uploadPicData: marchant.uploadPicData,
  isAuthenticated: loginOrRegister.isAuthenticated,
  cscPayment: marchant.cscPaymentDts,
  isServerError: marchant.isServerError,
  serverError: marchant.serverError,
  errorCode: marchant.errorCode,
  marchantID: marchant.marchantId,
  vlePaymentPlans: marchant.paymentPlans,
  allDocument: applicationDocument,
  schApply: appInstr.schType
});

const mapDispatchToProps = dispatch => ({
  loadRules: inputData => dispatch(fetchRulesAction()),
  fetchMatchRules: inputData =>
    dispatch(fetchUserMatchingRulesAction(inputData)),
  fetchMatchingScholarships: inputData =>
    dispatch(fetchScholarshipMatchingAction(inputData)),
  loadDistrictList: depntObj => dispatch(fetchDependantAction(depntObj)),
  requestDependantData: inputData => dispatch(fetchDependantData(inputData)),
  updateOrAddStudent: studentObj => dispatch(updateStduentAction(studentObj)),

  fetchMyFavScholarships: inputData =>
    dispatch(fetchMyFavScholarshipsActions(inputData)),
  fetchStudentDetails: inputData =>
    dispatch(fetchStudentDetailsAction(inputData)),
  editStdDetails: inputData => dispatch(editStudentDetailsActions(inputData)),
  fetchStudentList: inputData => dispatch(fetchStudentListAction(inputData)),
  uploadPic: inputData => dispatch(uploadUserPicAction(inputData)),
  addToFavScholarships: inputData =>
    dispatch(addToFavScholarshipActions(inputData)),
  fetchCscPaymentDt: inputData => dispatch(cscPaymentDetailsAction(inputData)),
  redirectCSC: inputData => dispatch(cscRedirectAction(inputData)),
  getMerchantId: inputData => dispatch(getMerchantIdAction(inputData)),
  fetchPaymentPlans: (inputData, type) =>
    dispatch(fetchPaymentPlansAction(inputData, type)),
  getAllDocuement: inputData => dispatch(getAllDocuementAction(inputData)),
  uploadAllDocuement: inputData =>
    dispatch(uploadAllDocuementAction(inputData)),
  deleteDocuement: inputData => dispatch(deleteDocuementAction(inputData)),
  completeDocument: inputData => dispatch(completeDocumentAction(inputData)),
  applicationInstructionStep: inputData =>
    dispatch(applicationInstructionStepAction(inputData)),
  vleSaveSchType: inputData => dispatch(saveScholarshipTypeAction(inputData))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VleSubscriber);
