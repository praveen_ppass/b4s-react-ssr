import { connect } from "react-redux";

import VlePay from "../components/vlePayment";

const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(VlePay);
