import React, { Component } from "react";
import { Link } from "react-router-dom";
import { imgBaseUrl, decode } from "../../../constants/constants";

import gblFunc from "../../../globals/globalFunctions";

import NotfoundPage from "../../common/error-404";

class VlePayment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      onFirstLoad: true
    };
  }

  render() {
    const { match, location, cscUrl, cscUserDetails } = this.props;
    let vlePage = null;
    switch (match.path) {
      case "/vle/payment-success":
        if (location.search != "") {
          const transactionId = location.search
            .replace("?payment_id=", "")
            .split("&");
          if (transactionId[1] === "status=success") {
            vlePage = <VLE_SUCCESSS transactionId={transactionId} />;
          } else {
            vlePage = <VLE_CANCEL transactionId={transactionId} />;
          }
        }
        break;
      case "/vle/payment-cancel":
        if (location.search != "") {
          const transactionId = location.search.replace("?transactionId=", "");
          vlePage = <VLE_CANCEL transactionId={decode(transactionId)} />;
        }

        break;
    }

    return <section>{vlePage}</section>;
  }
}

const VLE_SUCCESSS = ({ transactionId }) => {
  return (
    <section className="thankspage text-center">
      <img src={`${imgBaseUrl}icon-thanks.jpg`} alt="" />
      <h2>Payment Successful!</h2>
      <p>
        Thank you! We have successfully received your payment.
        <br />Your Transaction ID is {transactionId[0]}.
      </p>
      <Link to="/vle/student-list" className="btn noShadow">
        OK
      </Link>
    </section>
  );
};

const VLE_CANCEL = ({ transactionId }) => {
  return (
    <section className="thankspage text-center">
      <img src={`${imgBaseUrl}icon-thanks1.jpg`} alt="" />
      <h2>Payment Cancelled</h2>
      <p>
        Your payment has been cancelled. Your Transaction ID is {transactionId}.
        <br /> Please email us at info@buddy4study.com in case of any query.
      </p>
      <Link to="/vle/student-list" className="btn noShadow">
        Make another payment
      </Link>
    </section>
  );
};

export default VlePayment;
