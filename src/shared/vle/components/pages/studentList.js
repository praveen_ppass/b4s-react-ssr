import React from "react";
import { Link } from "react-router-dom";
import Loader from "../../../common/components/loader";
import gblFunc from "../../../../globals/globalFunctions";
import {
  FETCH_CSC_PAYMENT_DETAILS_SUCCESS,
  FETCH_CSC_REDIRECT_SUCCESS
} from "../../actions";
import {
  SAVE_SCHOLARSHIP_TYPE_SUCCESS,
  SAVE_SCHOLARSHIP_TYPE_FAILURE
} from "../../../application/actions/applicationInstructionActions";
import Pagination from "rc-pagination";

export class StudentList extends React.Component {
  constructor(props) {
    super(props);

    this.onViewStudentHandler = this.onViewStudentHandler.bind(this);
    this.state = {
      hideStudentList: true,
      paypop: false,
      payload: null,
      url: null,
      method: "POST",
      userId: "",
      studentId: null,
      ITEMS_PER_PAGE: 10,
      currentPage: 1
    };
    // this.openpaypop = this.openpaypop.bind(this);
    // this.closepaypop = this.closepaypop.bind(this);
    this.onBackHandler = this.onBackHandler.bind(this);
    this.redirectToGenericForm = this.redirectToGenericForm.bind(this);
    this.onPageChangeHandler = this.onPageChangeHandler.bind(this);
    this.getStudentList = this.getStudentList.bind(this);
  }

  componentDidMount() {
    const { ITEMS_PER_PAGE, currentPage } = this.state;
    let page = currentPage - 1;
    this.getStudentList({ page, length: ITEMS_PER_PAGE });
  }

  getStudentList({ page, length }) {
    const auth = gblFunc.getStoreUserDetails();
    if (this.props && this.props.matchurl === "/vle/upgrade") {
      this.setState({ userId: auth.userId }, () =>
        this.props.fetchStudentList({
          userId: auth.userId,
          paymentPartnerId: 1,
          page,
          length
        })
      );
    } else {
      this.setState({ userId: auth.userId }, () =>
        this.props.fetchStudentList({
          userId: auth.userId,
          page,
          length
        })
      );
    }
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case SAVE_SCHOLARSHIP_TYPE_SUCCESS:
        if (typeof window !== "undefined" && window.localStorage) {
          localStorage.setItem("vleStudentId", this.state.studentId);
          gblFunc.storeApplicationScholarshipId(10502);
        }
        this.props.history.push({
          pathname: "/application/SGA3/instruction"
        });
        break;
      case SAVE_SCHOLARSHIP_TYPE_FAILURE:
        if (typeof window !== "undefined" && window.localStorage) {
          localStorage.setItem("vleStudentId", this.state.studentId);
          gblFunc.storeApplicationScholarshipId(10502);
        }
        this.props.history.push({
          pathname: "/application/SGA3/instruction"
        });

        break;
    }
  }

  onViewStudentHandler(studentId) {
    this.setState(
      {
        hideStudentList: false
      },
      () =>
        this.props.fetchStudentDetails({
          userId: this.state.userId,
          studentId
        })
    );
  }

  onBackHandler() {
    this.setState(
      {
        hideStudentList: true
      },
      () => {
        this.props.fetchStudentList({
          userId: this.state.userId
        });
      }
    );
  }

  redirectToGenericForm(list) {
    this.setState(
      {
        studentId: list.id
      },
      () => {
        if (typeof window !== "undefined" && window.localStorage) {
          localStorage.setItem("vleStudentId", this.state.studentId);
          //gblFunc.storeApplicationScholarshipId(10502);
        }
        this.props.history.push({
          pathname: "/application/SGA3/instruction"
        });
      }
    );
  }

  onPageChangeHandler(currentPage) {
    const { ITEMS_PER_PAGE } = this.state;
    let page = currentPage - 1;

    const length = ITEMS_PER_PAGE;

    this.getStudentList({
      page,
      length
    });
    this.setState({
      currentPage
    });
  }

  render() {
    const {
      studentList,
      ITEMS_PER_PAGE,
      currentPage,
      url,
      payload
    } = this.state;
    const { matchurl } = this.props;
    let stdList = null;
    let viewStudent = null;

    let action = null;

    const totalStudents =
      this.props.studentData && this.props.studentData.total
        ? this.props.studentData.total
        : 0;

    if (
      this.props.studentData &&
      this.props.studentData.data &&
      this.props.studentData.data.length > 0
    ) {
      stdList = this.props.studentData.data.map(list => {
        if (matchurl === "/vle/upgrade") {
          action = (
            <button
              className="payNowBtn"
              onClick={() =>
                this.props.history.push({
                  pathname: "/vle/plans",
                  state: { studentData: list }
                })
              }
            >
              Upgrade
            </button>
          );
        } else {
          if (
            list.paymentplan &&
            list.paymentplan.permissions &&
            list.paymentplan.permissions.allowDocumentUpload
          ) {
            action = (
              <button
                style={{ cursor: "pointer" }}
                className="payNowBtn uploadDocs"
                onClick={() => this.redirectToGenericForm(list)}
              >
                Fill Form
              </button>
            );
          } else if (
            list.paymentplan &&
            list.paymentplan.permissions &&
            !list.paymentplan.permissions.allowDocumentUpload &&
            list.membershipExpiry
          ) {
            action = (
              <button
                className="payNowBtn"
                onClick={() =>
                  this.props.history.push({
                    pathname: "/vle/plans",
                    state: { studentData: list }
                  })
                }
              >
                Upgrade
              </button>
            );
          } else {
            action = (
              <button
                className="payNowBtn"
                onClick={() =>
                  this.props.history.replace({
                    pathname: "/vle/plans",
                    state: { studentData: list }
                  })
                }
              >
                Pay Now
              </button>
            );
          }
        }
        return (
          <tr key={list.id}>
            <td>
              <span>
                <a onClick={() => this.onViewStudentHandler(list.id)}>
                  {list.firstName} {list.lastName}
                </a>
                {/* {list.firstName} {list.lastName} */}
                <br /> {list.email}
              </span>
            </td>
            <td>{list.academicClass.value}</td>
            <td>
              {`${list.matchedScholarship.full +
                list.matchedScholarship.partial}` == "0" ? (
                "NA"
              ) : (
                <span
                  style={{ cursor: "pointer" }}
                  className="label label-primary recommend"
                  onClick={() =>
                    this.props.history.push({
                      pathname: "/vle/matched-scholarship",
                      state: { studentList: list, paid: "100" }
                    })
                  }
                >
                  {`${list.matchedScholarship.full +
                    list.matchedScholarship.partial}`}
                </span>
              )}
            </td>
            <td>
              {list.membershipExpiry ? (
                <span className="label label-primary cellWidth">
                  {list.membershipExpiry}
                </span>
              ) : (
                <p>NA</p>
              )}
            </td>
            <td>
              {/* <button className="btn btn-success hide">PAID</button> */}
              {action}
            </td>
          </tr>
        );
      });
    }

    if (this.props.studentData && this.props.studentData["personalInfo"]) {
      viewStudent = (
        <section className="row">
          <section className="col-xs-12 col-sm-12 col-md-6 tblMob boxBg">
            <article className="box-primary">
              <article className="box-body box-profile">
                <h3 className="profile-username text-center">
                  {this.props.studentData["personalInfo"]["firstName"]}{" "}
                  {this.props.studentData["personalInfo"]["lastName"]}
                </h3>
                <p className="text-muted text-center">
                  {this.props.studentData["personalInfo"]["email"]}
                </p>
              </article>
            </article>
            <article className="box-primary">
              <article className="box-header with-border">
                <h3 className="box-title">About Me</h3>
              </article>
              <article className="box-body">
                {" "}
                <strong>
                  <i className="fa fa-book margin-r-5" /> Education
                </strong>
                <p className="text-muted">
                  {
                    this.props.studentData["educationalInfo"][
                      "userAcademicInfo"
                    ]["academicClass"].value
                  }
                </p>
                <hr />
                <strong>
                  <i className="fa fa-map-marker margin-r-5" /> Location
                </strong>
                <p className="text-muted">
                  {
                    this.props.studentData["personalInfo"]["userAddress"][
                      "state"
                    ].value
                  }
                </p>
                <hr />
                <strong>
                  <i className="fa fa-pencil margin-r-5" /> Area of Interest
                </strong>
                {this.props.studentData["personalInfo"]["userRules"].map(
                  list => {
                    if (list.ruleTypeValue == "special") {
                      return `${list.ruleValue}, `;
                    }
                  }
                )}
                <hr />
                <p>
                  <Link
                    to={{
                      pathname: "/vle/edit-student",
                      state: {
                        id: this.props.studentData["personalInfo"]["id"],
                        userId: this.state.userId,
                        edit: true
                      }
                    }}
                    // onClick={() =>
                    //   this.props.editDetailHandler(
                    //     this.props.studentData["personalInfo"]["id"],
                    //     this.state.userId,
                    //     true
                    //   )
                    // }
                    className="btn btn-danger"
                  >
                    Edit Details
                  </Link>
                </p>
              </article>
            </article>
          </section>
          <section className="col-xs-12 col-sm-12 col-md-6 tblMob">
            <section className="table-responsive studentList border">
              <table className="table table-striped table-bordered table-hover">
                <tbody>
                  <tr>
                    <td>NAME</td>
                    <td>
                      {this.props.studentData["personalInfo"]["firstName"]}{" "}
                      {this.props.studentData["personalInfo"]["lastName"]}
                    </td>
                  </tr>
                  <tr>
                    <td>EMAIL</td>
                    <td>{this.props.studentData["personalInfo"]["email"]}</td>
                  </tr>
                  <tr>
                    <td>MOBILE</td>
                    <td>{this.props.studentData["personalInfo"]["mobile"]}</td>
                  </tr>
                  <tr>
                    <td>PASSING YEAR</td>
                    <td>
                      {
                        this.props.studentData["educationalInfo"][
                          "userAcademicInfo"
                        ]["passingYear"]
                      }
                    </td>
                  </tr>
                  <tr>
                    <td>PERCENTAGE</td>
                    <td>
                      {
                        this.props.studentData["personalInfo"][
                          "profilePercentage"
                        ]
                      }
                    </td>
                  </tr>
                  <tr>
                    <td>SPECIAL</td>
                    {this.props.studentData["personalInfo"]["userRules"].map(
                      list => {
                        if (list.ruleTypeValue == "special") {
                          return `${list.ruleValue}, `;
                        }
                      }
                    )}
                  </tr>
                  <tr>
                    <td>GENDER</td>
                    {this.props.studentData["personalInfo"]["userRules"].map(
                      list => {
                        if (list.ruleTypeValue == "gender") {
                          return <td>{list.ruleValue}</td>;
                        }
                      }
                    )}
                  </tr>
                  {/* <tr>
                    <td>SUBSCRIBER ROLE</td>
                    {this.props.studentData["personalInfo"]["userRules"].map(
                      list => {
                        if (list.ruleTypeValue == "subscriber_role") {
                          return <td>{list.ruleValue}</td>;
                        }
                      }
                    )}
                    
                  </tr>*/}
                  <tr>
                    <td>CATEGORY</td>
                    {this.props.studentData["personalInfo"]["userRules"].map(
                      list => {
                        if (list.ruleTypeValue == "quota") {
                          return <td>{list.ruleValue}</td>;
                        }
                      }
                    )}
                  </tr>
                  <tr>
                    <td>INSTITUTE/SCHOOL/COLLEGE NAME</td>
                    <td>
                      {
                        this.props.studentData["educationalInfo"][
                          "userInstituteInfo"
                        ]["instituteName"]
                      }
                    </td>
                  </tr>
                  <tr>
                    <td>STATE</td>
                    <td>
                      {
                        this.props.studentData["personalInfo"]["userAddress"][
                          "state"
                        ].value
                      }
                    </td>
                  </tr>
                  <tr>
                    <td>STUDENT RELATION</td>
                    {this.props.studentData["personalInfo"]["userRules"].map(
                      list => {
                        if (list.ruleTypeValue == "subscriber_relation") {
                          return <td>{list.ruleValue}</td>;
                        }
                      }
                    )}
                  </tr>
                  <tr>
                    <td>CLASS</td>
                    <td>
                      {
                        this.props.studentData["educationalInfo"][
                          "userAcademicInfo"
                        ]["academicClass"].value
                      }
                    </td>
                  </tr>
                  <tr>
                    <td>FAMILY INCOME</td>
                    <td>
                      {this.props.studentData["personalInfo"]["familyIncome"]}
                    </td>
                  </tr>
                  <tr>
                    <td>RELIGION</td>
                    {this.props.studentData["personalInfo"]["userRules"].map(
                      list => {
                        if (list.ruleTypeValue == "religion") {
                          return <td>{list.ruleValue}</td>;
                        }
                      }
                    )}
                  </tr>
                </tbody>
              </table>
            </section>
          </section>
        </section>
      );
    }

    return (
      <section>
        {this.state.hideStudentList ? (
          <section>
            {url && payload ? (
              <form
                style={{ display: "none" }}
                name="cscForm"
                id="cscForm"
                method="POST"
                action={url}
              >
                <input
                  type="hidden"
                  id="message"
                  name="message"
                  value={payload}
                />
              </form>
            ) : null}
            <h4 className="titletext marginTop">Student List </h4>
            {/* <div className="upload-btn-wrapper">
              <button className="btn">Upload a file</button>
              <input type="file" name="myfile" />
            </div> */}
            <section className="row col msch">
              <section className="col-xs-12 col-sm-12 col-md-12 tblMob">
                {this.props.studentData &&
                this.props.studentData.data &&
                this.props.studentData.data.length > 0 ? (
                  <section className="table-responsive studentList">
                    <table className="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>Name/Email</th>
                          <th>Class</th>
                          <th>Recommended</th>
                          <th>Expiry</th>
                          <th>Paid</th>
                        </tr>
                      </thead>
                      <tbody>{stdList}</tbody>
                    </table>
                  </section>
                ) : (
                  <p>NO STUDENT FOUND.</p>
                )}
              </section>
            </section>
            <article className="col-md-9 col-sm-9 col-xs-12 marginRight flex-container-list">
              <article className="pagination">
                {totalStudents > 10 ? (
                  <Pagination
                    defaultPageSize={ITEMS_PER_PAGE}
                    pageSize={ITEMS_PER_PAGE}
                    defaultCurrent={1}
                    current={currentPage}
                    onChange={this.onPageChangeHandler}
                    total={totalStudents}
                    showTitle={false}
                  />
                ) : null}
              </article>
            </article>
          </section>
        ) : (
          <section>
            <section>
              <h4 className="titletext marginTop">
                VIEW STUDENT DATA
                <span onClick={this.onBackHandler}>
                  <i className="fa fa-long-arrow-left" aria-hidden="true" />
                  Back
                </span>
              </h4>
            </section>
            {viewStudent}
          </section>
        )}
      </section>
    );
  }
}
