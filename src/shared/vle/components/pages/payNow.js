import React from "react";
import { IMButtonJS } from "../../../../constants/constants";
import gblFunc from "../../../../globals/globalFunctions";
export class PayNowPopUP extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    gblFunc.loadJsScript(IMButtonJS);
  }

  render() {
    return (
      <article className="popupSubscribe">
        <article className="popup_inner">
          <article className="subscribePopup">
            <span id="payPreFormSubs" />
            <button
              type="button"
              className="close"
              onClick={this.props.closePaymentPopUp}
            >
              <i>&times;</i>
            </button>
            <section className="row">
              <article className="col-md-12 text-center">
                <h1 className="premium">{this.props.payment_type}</h1>
                <p
                  className="subtitle"
                  dangerouslySetInnerHTML={{
                    __html: this.props.htmlContent
                  }}
                />
                {/* <span>
                  <i className="fa fa-inr" aria-hidden="true" />
                </span> */}
                <article className="btn-ctrl">
                  <article id="preAnnMem">
                    <article className="im-checkout">
                      <PayButtonLink
                        url={this.props.paymentConfig.url}
                        text={this.props.paymentConfig.text}
                      />
                    </article>
                  </article>
                </article>
              </article>
            </section>
          </article>
        </article>
      </article>
    );
  }
}

const PayButtonLink = props => {
  return (
    <a
      href={props.url}
      rel="im-checkout"
      data-behaviour="remote"
      data-style="flat"
      data-text={props.text}
      className="btn"
    />
  );
};
