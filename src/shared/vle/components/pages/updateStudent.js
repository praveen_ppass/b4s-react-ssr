import React from "react";
import DatePicker from "react-datepicker";
import moment from "moment";
import { Link } from "react-router-dom";
import Loader from "../../../common/components/loader";
import { ruleRunner } from "../../../../validation/ruleRunner";
import { VLE_FETCH_STUDENT_DETAILS_SUCCESS } from "../../actions";
import {
  FETCH_RULES_SUCCEEDED,
  FETCH_DEPENDANT_DATA_SUCCEEDED
} from "../../../../constants/commonActions";

import {
  getYearOrMonth,
  dependantRequests
} from "../../../../constants/constants";

import {
  VLE_UPDATE_STUDENT_SUCCESS,
  VLE_EDIT_STUDENT_DETAILS_SUCCESS,
  VLE_UPDATE_STUDENT_FAIL
} from "../../actions";

import {
  required,
  isEmail,
  minLength,
  isNumeric,
  isMobileNumber,
  lengthRange,
  isGreaterThan,
  isLessThan
} from "../../../../validation/rules";
import { VleStudentState } from "../defaultState";
import gblFunc from "../../../../globals/globalFunctions";

if (typeof window !== "undefined") {
  require("react-datepicker/dist/react-datepicker.css");
}

export class UpdateStudent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      educationalInfo: {
        userAcademicInfo: {
          academicClass: { id: null, value: null },
          marksObtained: null,
          passingMonth: null,
          passingYear: null,
          percentage: null,
          presentClass: 1,
          totalMarks: null,
          id: null,
          markingType: "1"
        },
        userInstituteInfo: {
          academicDetailId: null,
          id: null,
          instituteName: ""
        }
      },
      personalInfo: {
        dob: "",
        email: "",
        familyIncome: null,
        firstName: "",
        id: null,
        lastName: "",
        mobile: null,
        portalId: 1,
        userAddress: {
          district: { id: null, value: null },
          id: null,
          state: { id: null, value: null },
          type: null
        },
        userRules: []
      },
      rules: {},
      dropDownRules: {
        religion: {
          ruleId: null,
          ruleTypeId: 4
        },
        quota: {
          ruleId: null,
          ruleTypeId: 6
        },
        gender: {
          ruleId: null,
          ruleTypeId: 5
        },
        disabled: {
          ruleId: null,
          ruleTypeId: 48
        },
        foreign: {
          ruleId: 820,
          ruleTypeId: 46
        },
        subscriber_relation: {
          ruleId: null,
          ruleTypeId: 32
        },
        special: {}
      },
      interest: [],
      districts: [],
      userid: null,
      ON_FIRST_LOAD: true,
      validations: {
        firstName: null,
        mobile: null,
        dob: null,
        gender: null,
        religion: null,
        academicClass: null,
        dob: null,
        email: null,
        quota: null,
        district: null,
        // totalMarks: null,
        // marksObtained: null,
        state: null,
        instituteName: null,
        passingYear: null,
        subscriber_relation: null,
        familyIncome: null,
        foreign: null,
        interest: null,
        disabled: null
      },

      serverError: null,
      isFormValid: false,
      isCheckTNC: false,
      checkTNCValidation: null
    };

    this.currentEduFormChangeHandler = this.currentEduFormChangeHandler.bind(
      this
    );
    this.currentProfileFormChangeHandler = this.currentProfileFormChangeHandler.bind(
      this
    );
    this.currentFormRulesHandler = this.currentFormRulesHandler.bind(this);
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this.onCheckBoxToggleHandler = this.onCheckBoxToggleHandler.bind(this);
    this.onCheckStateHandler = this.onCheckStateHandler.bind(this);
    this.onChangeTNC = this.onChangeTNC.bind(this);
    this.onCheckLocationHandler = this.onCheckLocationHandler.bind(this);
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "firstName":
        validationObject.name = "* First name";
        validationObject.validationFunctions = [required, lengthRange(3, 50)];
        return validationObject;
      case "mobile":
        validationObject.name = "* Mobile number";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isMobileNumber,
          minLength(10)
        ];
        return validationObject;
      case "email":
        validationObject.name = "* Email";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;
      case "dob":
        validationObject.name = "* Date of birth";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "quota":
        validationObject.name = "* Category";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "gender":
        validationObject.name = "* Gender";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "religion":
        validationObject.name = "* Religion";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "state":
        validationObject.name = "* State";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "city":
        validationObject.name = "* City";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "district":
        validationObject.name = "* District";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "academicClass":
        validationObject.name = "* Present class";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "instituteName":
        validationObject.name = "* Institute/School/College name";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "passingYear":
        validationObject.name = "* Student passing year";
        validationObject.validationFunctions = [required, isNumeric];
        return validationObject;
      case "subscriber_relation":
        validationObject.name = "* Student relation";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "familyIncome":
        validationObject.name = "* Annual family income";
        validationObject.validationFunctions = [required, isNumeric];
        return validationObject;
      case "pincode":
        validationObject.name = "* Pincode";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          lengthRange(6, 6)
        ];
        return validationObject;
      case "foreign":
        validationObject.name = "* Need international scholarships";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "disabled":
        validationObject.name = "* Disabled field";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "totalMarks":
        const {
          marksObtained,
          totalMarks
        } = this.state.educationalInfo.userAcademicInfo;
        const marks =
          marksObtained && marksObtained.length > 0 ? marksObtained : 0;
        validationObject.name = "*Total marks";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isGreaterThan(marks)
        ];

        return validationObject;
      case "marksObtained":
        const totalmarks = this.state.educationalInfo.userAcademicInfo
          .totalMarks;
        validationObject.name = "* Marks obtained";
        validationObject.validationFunctions = [
          required,
          isNumeric,
          isLessThan(totalmarks)
        ];
        return validationObject;

      case "interest":
        validationObject.name = "* Interest";
        validationObject.validationFunctions = [required];
        return validationObject;
    }
  }
  /************ end - validation part - get message and required validation******************* */

  componentWillReceiveProps(nextProps) {
    const updatePersonal = { ...this.state.personalInfo };
    const updateEducationInfo = { ...this.state.educationalInfo };
    const updateRules = { ...this.state.dropDownRules };
    const { studentData } = nextProps;
    // const updateUserRule = { ...this.state.USER_RULE };
    switch (nextProps.type) {
      case VLE_UPDATE_STUDENT_SUCCESS:
        this.props.history.push("/vle/student-list");
        break;
      case VLE_EDIT_STUDENT_DETAILS_SUCCESS:
        this.props.history.push("/vle/student-list");
        break;

      case VLE_UPDATE_STUDENT_FAIL:
        const validations = { ...this.state.validations };
        if (nextProps.isServerError) {
          if (nextProps.errorCode == 701) {
            validations.mobile = nextProps.serverError;
          }

          if (nextProps.errorCode == 702) {
            validations.email = nextProps.serverError;
          }
          this.setState({
            validations
          });
        } else {
          this.setState({
            serverError: nextProps.serverError
          });
        }
        break;
      case VLE_FETCH_STUDENT_DETAILS_SUCCESS:
        const { userAddress } = nextProps.studentData.personalInfo;
        if (userAddress && userAddress["state"] && this.state.ON_FIRST_LOAD) {
          this.getDistrictList("state", userAddress["state"]["id"]);
        }

        if (
          studentData &&
          studentData.personalInfo &&
          studentData.educationalInfo
        ) {
          const { personalInfo } = studentData;
          const {
            userAcademicInfo,
            userInstituteInfo
          } = studentData.educationalInfo;

          for (let i in updatePersonal) {
            if (i !== "userRule") {
              updatePersonal[i] =
                i == "userAddress" && !personalInfo[i]
                  ? updatePersonal[i]
                  : personalInfo[i];
            }
          }

          if (personalInfo.userRules && personalInfo.userRules.length > 0) {
            let resRules = personalInfo.userRules;
            resRules.map(list => {
              for (let key in updateRules) {
                if (
                  list.ruleTypeId != 7 &&
                  updateRules[key].ruleTypeId == list.ruleTypeId
                ) {
                  updateRules[key].ruleId = list.ruleId;
                }
                if (key == "special" && list.ruleTypeId == 7) {
                  updateRules["special"][list.ruleId] = true;
                }
              }
            });
          }

          // if (
          //   personalInfo &&
          //   personalInfo.userAddress &&
          //   personalInfo.userAddress["state"] &&
          //   nextProps.type === VLE_FETCH_STUDENT_DETAILS_SUCCESS
          // ) {
          //   if (
          //     personalInfo.userAddress["state"] &&
          //     !this.props.district.length
          //   ) {
          //     this.getDistrictList(
          //       "state",
          //       personalInfo.userAddress["state"]["id"]
          //     );
          //   }
          // }

          if (userAcademicInfo && userInstituteInfo) {
            this.mapApiToCurrentState(
              userAcademicInfo,
              "userAcademicInfo",
              nextProps.type
            );
            this.mapApiToCurrentState(
              userInstituteInfo,
              "userInstituteInfo",
              nextProps.type
            );
          }
        }

        this.setState({
          personalInfo: updatePersonal,
          dropDownRules: updateRules,
          ON_FIRST_LOAD: false
        });
        break;
    }

    if (
      nextProps.type !== "VLE_EDIT_STUDENT_DETAILS_REQUEST" &&
      nextProps.type !== "VLE_UPDATE_STUDENT_REQUEST"
    ) {
    }
  }

  mapApiToCurrentState(userEduData, KEY, type) {
    const userEducationInfo = { ...this.state.educationalInfo };

    for (let key in userEducationInfo[KEY]) {
      userEducationInfo[KEY][key] = userEduData[key];
    }
    this.setState({ educationalInfo: userEducationInfo });
  }

  currentEduFormChangeHandler(event, PARENT_KEY) {
    const { id, value } = event.target;
    const updateEduInfo = { ...this.state.educationalInfo };
    let validations = { ...this.state.validations };
    let index =
      event.nativeEvent && event.nativeEvent.target
        ? event.nativeEvent.target.selectedIndex
        : null;

    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      const validationResult = ruleRunner(
        value,
        id,
        name,
        ...validationFunctions
      );
      validations[id] = validationResult[id];
      if (
        id == "totalMarks" &&
        parseInt(value) >
          parseInt(this.state.educationalInfo.userAcademicInfo.marksObtained)
      ) {
        validations["marksObtained"] = null;
        validations["totalMarks"] = null;
      } else if (
        id == "marksObtained" &&
        parseInt(value) <
          parseInt(this.state.educationalInfo.userAcademicInfo.totalMarks)
      ) {
        validations["marksObtained"] = null;
        validations["totalMarks"] = null;
      } else {
        if (
          this.state.educationalInfo.userAcademicInfo.marksObtained >
          this.state.educationalInfo.userAcademicInfo.totalMarks
        )
          validations["marksObtained"] =
            "* Marks obtained must be less than total marks.";
      }
    }
    if (PARENT_KEY == "userAcademicInfo" && id === "academicClass") {
      updateEduInfo[PARENT_KEY][id] = {
        id: +value,
        value:
          event.nativeEvent.target && event.nativeEvent.target[index]
            ? event.nativeEvent.target[index].text
            : null
      };
    } else {
      updateEduInfo[PARENT_KEY][id] = value;
    }
    if (dependantRequests.includes(id)) {
      //TODO: Call dependant key request , pick from props in container
      this.getDistrictList(id, value);
      //this.setState({ ON_FIRST_LOAD: false });
    }

    if (
      updateEduInfo.userAcademicInfo.marksObtained &&
      updateEduInfo.userAcademicInfo.totalMarks
    ) {
      updateEduInfo.userAcademicInfo.percentage = (
        updateEduInfo.userAcademicInfo.marksObtained /
        updateEduInfo.userAcademicInfo.totalMarks *
        100
      ).toFixed(2);
    } else {
      updateEduInfo.userAcademicInfo.percentage = "";
    }

    this.setState({ educationalInfo: updateEduInfo, validations });
  }

  currentProfileFormChangeHandler(event, KEY, SUBKEY = "") {
    const updatePersonForm = { ...this.state.personalInfo };

    let validations = { ...this.state.validations };
    let index =
      event.nativeEvent && event.nativeEvent.target
        ? event.nativeEvent.target.selectedIndex
        : null;

    const { id, value } =
      KEY == "dob"
        ? { id: "dob", value: moment(event).format("YYYY-MM-DD") }
        : event.target;

    //validations
    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      const validationResult = ruleRunner(
        value,
        id,
        name,
        ...validationFunctions
      );
      validations[id] = validationResult[id];
    }

    if (!SUBKEY) {
      updatePersonForm[KEY] = value;
    } else {
      let typeOfProp = {
        addressLine: value,
        city: value,
        country: value,
        district: {
          id: +value,
          value:
            event.nativeEvent.target && event.nativeEvent.target[index]
              ? event.nativeEvent.target[index].text
              : null
        },
        id: +value,
        pincode: value,
        state: {
          id: +value,
          value:
            event.nativeEvent.target && event.nativeEvent.target[index]
              ? event.nativeEvent.target[index].text
              : null
        },
        familyIncome: value
      };
      updatePersonForm[SUBKEY]
        ? (updatePersonForm[SUBKEY][KEY] = typeOfProp[KEY])
        : null;
    }

    // let keyToUpperCase = KEY.toUpperCase();
    if (dependantRequests.includes(KEY)) {
      //TODO: Call dependant key request , pick from props in container

      this.getDistrictList(KEY, value);
      this.setState({ ON_FIRST_LOAD: false });
    }

    this.setState({ personalInfo: updatePersonForm, validations });
  }

  currentFormRulesHandler(event, KEY) {
    const { id, value } = event.target;
    let validations = { ...this.state.validations };
    const updateUserRules = { ...this.state.dropDownRules };

    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      const validationResult = ruleRunner(
        value,
        id,
        name,
        ...validationFunctions
      );
      validations[id] = validationResult[id];
    }
    if (KEY === "special") {
      if (interest.indexOf(value) > -1) {
        interest.splice(interest.indexOf(value), 1);
        delete updateUserRules["special"][value];
      } else {
        interest.push(value);
      }

      this.setState({ interest, dropDownRules: updateUserRules, validations });
    } else {
      if (value !== "") {
        updateUserRules[KEY].ruleId = +value;
      } else {
        updateUserRules[KEY].ruleId = value;
      }
      this.setState({
        dropDownRules: updateUserRules,
        validations
      });
    }
  }

  onCheckBoxToggleHandler(event) {
    const { id, value } = event.target;
    let validations = { ...this.state.validations };
    //toggle checkbox state
    const checkBoxState = !this.state.dropDownRules["special"][value];

    const dropDownRules = {
      ...this.state.dropDownRules
    };

    dropDownRules["special"][value] = checkBoxState;

    //validations

    this.setState({ dropDownRules });
  }

  getFilterRules(ruleId) {
    this.props.fetchFilterRuleById(ruleId);
  }

  getDistrictList(key, value) {
    this.props.loadDistrictList({
      KEY: key,
      DATA: value
    });
  }
  onChangeTNC(e) {
    const { checked } = e.target;

    if (!checked) {
      this.setState({
        checkTNCValidation:
          "Please accept the terms and conditions before proceeding further.",
        isCheckTNC: false
      });
    } else {
      this.setState({
        checkTNCValidation: null,
        isCheckTNC: true
      });
    }
  }

  onCheckLocationHandler() {
    const { location } = this.props;

    return location.state && location.state.id ? location.state : 0;
  }

  onSubmitHandler(event) {
    event.preventDefault();
    let user_data = gblFunc.getStoreUserDetails();
    let params = {};
    const { personalInfo, educationalInfo, interest, isCheckTNC } = {
      ...this.state
    };

    const { studentData } = this.props;

    const updaterules = { ...this.state.dropDownRules };

    if (!isCheckTNC) {
      this.setState({
        checkTNCValidation:
          "Please accept the terms and conditions before proceeding further."
      });
      return;
    }

    for (let val in updaterules["special"]) {
      if (updaterules["special"][val]) {
        interest.push({
          ruleId: +val,
          ruleTypeId: 7
        });
      }
    }

    for (let key in updaterules) {
      if (key != "special" && updaterules[key].ruleId !== null)
        interest.push(updaterules[key]);
    }

    personalInfo.userRules = interest;
    params = {
      personalInfo,
      educationalInfo
    };

    if (this.checkFormValidations()) {
      const studentValue = this.onCheckLocationHandler();
      if (studentValue) {
        this.props.editStdDetails({
          params,
          userId: studentValue.userId,
          studentId: studentValue.id
        });
      } else {
        this.props.updateOrAddStudent({ params, userId: this.props.userId });
        this.setState({ interest: [], serverError: null });
      }
      // this.props.updateOrAddStudent({ params, userId: user_data.userId });
      // this.setState({ interest: [], serverError: null });
    }
  }

  onCheckStateHandler(key) {
    const { educationalInfo, personalInfo } = this.state;
    let rules = { ...this.state.dropDownRules };

    let address = {
      state: "userAddress",
      district: "userAddress",
      addressLine: "userAddress",
      pincode: "userAddress",
      city: "userAddress",
      country: "userAddress"
    };

    if (rules[key]) {
      return rules[key].ruleId;
    }

    switch (key) {
      case "interest":
        if (Object.keys(this.state.dropDownRules["special"]).length == 0)
          return "";
        else return Object.keys(this.state.dropDownRules["special"]).length;
      case "state":
        if (address[key] && personalInfo[address[key]]) {
          return personalInfo[address[key]][key].id;
        }
      case "district":
        if (address[key] && personalInfo[address[key]]) {
          return personalInfo[address[key]][key].id;
        }
      case "academicClass":
        return educationalInfo["userAcademicInfo"][key].id;
    }

    if (personalInfo[address[key]] && (key !== "state" || key !== "district")) {
      return personalInfo[address[key]][key];
    }

    if (personalInfo[key]) {
      return key == "mobile" ? personalInfo[key].toString() : personalInfo[key];
    }
    if (
      educationalInfo["userAcademicInfo"] ||
      educationalInfo["userInstituteInfo"]
    ) {
      return (
        educationalInfo["userAcademicInfo"][key] ||
        educationalInfo["userInstituteInfo"][key]
      );
    }
  }

  /************ start - validation part - check form submit validation******************* */
  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    let validationResult = {};
    for (let key in validations) {
      if (validations.hasOwnProperty(key)) {
        if (
          key === "foreign" &&
          this.state.dropDownRules.foreign.ruleId === 0
        ) {
          validationResult[key] = null;
        } else {
          let { name, validationFunctions } = this.getValidationRulesObject(
            key
          );
          validationResult = ruleRunner(
            this.onCheckStateHandler(key),
            key,
            name,
            ...validationFunctions
          );
        }

        validations[key] = validationResult[key];
        if (validationResult[key] !== null) {
          isFormValid = false;
        }
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }
  /************ end - validation part - check form submit validation******************* */
  componentDidMount() {
    if (!this.props.rulesList) {
      this.props.loadRules();
    }
    const { location } = this.props;

    if (location && location.state && location.state.id) {
      const { id, userId, edit } = location.state;
      this.props.editDetailHandler(id, userId, edit);
    }

    // if (!this.props.educationData || this.props.educationData.length == 0) {
    //   this.props.fetchEducationInfoDetails({ userId: this.props.userId });
    // }
  }

  render() {
    let studentFormContent = null;
    const year = getYearOrMonth("year", [1950, 2024]);
    const { userAcademicInfo, userInstituteInfo } = this.state.educationalInfo;
    const { district, state } = this.state.personalInfo.userAddress
      ? this.state.personalInfo.userAddress
      : {
          district: { id: null, value: null },
          state: { id: null, value: null }
        };
    const { personalInfo, isCheckTNC } = this.state;
    const {
      religion,
      gender,
      quota,
      foreign,
      disabled,
      subscriber_relation
    } = this.state.dropDownRules;

    if (!userAcademicInfo.percentage) {
      userAcademicInfo.percentage = "";
    } else {
      userAcademicInfo.percentage = Number(userAcademicInfo.percentage).toFixed(
        2
      );
    }

    const { rulesList } = this.props;
    const { location } = this.props;

    if (rulesList) {
      studentFormContent = (
        <article className="addstudent">
          <article className="ctrl-wrapper widget-border">
            <h2 className="my-profile">Student Details</h2>
            <article className="ctrl-wrapper ">
              <article className="row">
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="fname">First Name*</label>
                    <input
                      type="text"
                      maxLength="50"
                      minLength="1"
                      className="form-control"
                      id="firstName"
                      name="first_name"
                      value={
                        personalInfo.firstName ? personalInfo.firstName : ""
                      }
                      onChange={event =>
                        this.currentProfileFormChangeHandler(event, "firstName")
                      }
                      placeholder="Enter your first name"
                    />

                    {this.state.validations.firstName ? (
                      <span className="error animated bounce">
                        {this.state.validations.firstName}
                      </span>
                    ) : null}
                  </article>
                </article>
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="fname">Last Name</label>
                    <input
                      type="text"
                      maxLength="50"
                      minLength="1"
                      className="form-control"
                      name="last_name"
                      id="lname"
                      value={personalInfo.lastName ? personalInfo.lastName : ""}
                      onChange={event =>
                        this.currentProfileFormChangeHandler(event, "lastName")
                      }
                      placeholder="Enter your last name"
                    />

                    {/* <span className="error">* Last name is too long.</span>
            <span className="error">* Last name is too short.</span> */}
                  </article>
                </article>
              </article>
              <article className="row">
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="email">Email *</label>
                    <input
                      type="email"
                      name="email"
                      className="form-control"
                      id="email"
                      placeholder="Enter your email"
                      value={personalInfo.email ? personalInfo.email : ""}
                      onChange={event =>
                        this.currentProfileFormChangeHandler(event, "email")
                      }
                      readOnly={
                        location && location.state && location.state.edit
                          ? true
                          : false
                      }
                    />

                    {this.state.validations.email ? (
                      <span className="error animated bounce">
                        {this.state.validations.email}
                      </span>
                    ) : null}
                  </article>
                </article>
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="mnumber">Mobile Number*</label>
                    <input
                      maxLength="10"
                      minLength="10"
                      maxLength="10"
                      type="text"
                      name="mobile"
                      placeholder="Enter your mobile"
                      className="form-control"
                      id="mobile"
                      value={personalInfo.mobile ? personalInfo.mobile : ""}
                      onChange={event =>
                        this.currentProfileFormChangeHandler(event, "mobile")
                      }
                    />

                    {this.state.validations.mobile ? (
                      <span className="error animated bounce">
                        {this.state.validations.mobile}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article>

              <article className="row">
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="dob">Date of Birth*</label>
                    <article className="input-group dateCal">
                      <DatePicker
                        showYearDropdown
                        scrollableYearDropdown
                        yearDropdownItemNumber={40}
                        minDate={moment().subtract(100, "years")}
                        maxDate={moment().add(1, "years")}
                        name="dob"
                        id="dob"
                        className="form-control"
                        autoCapitalize="off"
                        selected={
                          personalInfo.dob
                            ? moment(
                                moment(personalInfo.dob).format("DD-MM-YYYY"),
                                "DD-MM-YYYY"
                              )
                            : null
                        }
                        onChange={event =>
                          this.currentProfileFormChangeHandler(event, "dob")
                        }
                        dateFormat="DD-MM-YYYY"
                      />
                      <label htmlFor="dob" className="input-group-addon">
                        <i className="fa fa-calendar" aria-hidden="true" />
                      </label>
                      {this.state.validations.dob ? (
                        <span className="error animated bounce">
                          {this.state.validations.dob}
                        </span>
                      ) : null}
                    </article>
                  </article>
                </article>
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="category">Category*</label>
                    <select
                      name="category"
                      className="form-control"
                      id="quota"
                      required=""
                      value={quota.ruleId ? quota.ruleId : ""}
                      onChange={event =>
                        this.currentFormRulesHandler(event, "quota")
                      }
                    >
                      <option value="">--Category--</option>
                      {rulesList["quota"].map(list => {
                        return (
                          <option key={list.id} value={list.id}>
                            {list.rulevalue}
                          </option>
                        );
                      })}
                    </select>

                    {this.state.validations.quota ? (
                      <span className="error animated bounce">
                        {this.state.validations.quota}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article>

              <article className="row">
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="gender">Gender*</label>
                    <select
                      name="gender"
                      value={gender.ruleId ? gender.ruleId : ""}
                      onChange={event =>
                        this.currentFormRulesHandler(event, "gender")
                      }
                      className="form-control"
                      id="gender"
                    >
                      <option value="">--Gender--</option>
                      {rulesList["gender"]
                        .filter(gList => gList["id"] !== 229)
                        .map(list => {
                          return (
                            <option key={list.id} value={list.id}>
                              {list.rulevalue}
                            </option>
                          );
                        })}
                    </select>

                    {this.state.validations.gender ? (
                      <span className="error animated bounce">
                        {this.state.validations.gender}
                      </span>
                    ) : null}
                  </article>
                </article>
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="religion">Religion*</label>
                    <select
                      name="religion"
                      className="form-control"
                      id="religion"
                      value={religion.ruleId ? religion.ruleId : ""}
                      onChange={event =>
                        this.currentFormRulesHandler(event, "religion")
                      }
                    >
                      <option value="">--Religion--</option>
                      {rulesList["religion"].map(list => {
                        return (
                          <option key={list.id} value={list.id}>
                            {list.rulevalue}
                          </option>
                        );
                      })}
                    </select>

                    {this.state.validations.religion ? (
                      <span className="error animated bounce">
                        {this.state.validations.religion}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article>

              <article className="row">
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="state">State*</label>
                    <select
                      className="form-control"
                      id="state"
                      value={state.id ? state.id : ""}
                      onChange={event =>
                        this.currentProfileFormChangeHandler(
                          event,
                          "state",
                          "userAddress"
                        )
                      }
                    >
                      <option value="">--Select State--</option>
                      {rulesList["state"]
                        .filter(fList => fList["id"] !== 447)
                        .map(list => {
                          return (
                            <option
                              key={list.id}
                              name={list.rulevalue}
                              value={list.id}
                            >
                              {list.rulevalue}
                            </option>
                          );
                        })}
                    </select>
                    {this.state.validations.state ? (
                      <span className="error animated bounce">
                        {this.state.validations.state}
                      </span>
                    ) : null}

                    {/* <span className="error">* State is required.</span> */}
                  </article>
                </article>
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="district">District*</label>
                    <select
                      name="district"
                      className="form-control"
                      id="district"
                      value={district.id ? district.id : ""}
                      onChange={event =>
                        this.currentProfileFormChangeHandler(
                          event,
                          "district",
                          "userAddress"
                        )
                      }
                    >
                      <option value="">--District--</option>
                      {this.props.district.map(list => {
                        return (
                          <option key={list.id} value={list.id}>
                            {list.districtName}
                          </option>
                        );
                      })}
                    </select>
                    {this.state.validations.district ? (
                      <span className="error animated bounce">
                        {this.state.validations.district}
                      </span>
                    ) : null}

                    {/* <span className="error">* District is required.</span> */}
                  </article>
                </article>
              </article>

              <article className="row">
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="state">
                      Select Present Class<i>*</i>
                    </label>
                    <select
                      name="academicClass"
                      className="form-control"
                      id="academicClass"
                      value={
                        userAcademicInfo.academicClass.id
                          ? userAcademicInfo.academicClass.id
                          : ""
                      }
                      onChange={event =>
                        this.currentEduFormChangeHandler(
                          event,
                          "userAcademicInfo"
                        )
                      }
                    >
                      <option value="">--Select Class/Degree--</option>
                      {rulesList["class"].map(list => (
                        <option key={list.id} value={list.id}>
                          {list.rulevalue}
                        </option>
                      ))}
                    </select>

                    {this.state.validations.academicClass ? (
                      <span className="error animated bounce">
                        {this.state.validations.academicClass}
                      </span>
                    ) : null}
                  </article>
                </article>
                <article className="col-md-6 ">
                  <article className="form-group">
                    <label htmlFor="acadSub" className="control-label">
                      Institute/School/College Name*
                    </label>
                    <input
                      type="text"
                      name="preSchoolName"
                      maxLength="100"
                      minLength="2"
                      placeholder="Enter your institute/schoolcollege name"
                      className="form-control"
                      id="instituteName"
                      value={
                        userInstituteInfo.instituteName
                          ? userInstituteInfo.instituteName
                          : ""
                      }
                      onChange={event =>
                        this.currentEduFormChangeHandler(
                          event,
                          "userInstituteInfo"
                        )
                      }
                    />

                    {this.state.validations.instituteName ? (
                      <span className="error animated bounce">
                        {this.state.validations.instituteName}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article>

              <article className="row">
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="state">
                      Student Passing Year<i>*</i>
                    </label>
                    <select
                      name="academicClass"
                      className="form-control"
                      id="passingYear"
                      value={userAcademicInfo.passingYear}
                      onChange={event =>
                        this.currentEduFormChangeHandler(
                          event,
                          "userAcademicInfo"
                        )
                      }
                    >
                      <option value="">--Select Year--</option>
                      {year.map(y => (
                        <option key={y} value={y}>
                          {y}
                        </option>
                      ))}
                    </select>

                    {this.state.validations.passingYear ? (
                      <span className="error animated bounce">
                        {this.state.validations.passingYear}
                      </span>
                    ) : null}
                  </article>
                </article>
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="state">
                      Student Relation
                      <i>*</i>
                    </label>
                    <select
                      name="relationOfCondidate"
                      className="form-control"
                      id="subscriber_relation"
                      value={
                        subscriber_relation.ruleId
                          ? subscriber_relation.ruleId
                          : ""
                      }
                      onChange={event =>
                        this.currentFormRulesHandler(
                          event,
                          "subscriber_relation"
                        )
                      }
                    >
                      <option value="">--Select Relation--</option>

                      {rulesList["subscriber_relation"].map(list => (
                        <option key={list.id} value={list.id}>
                          {list.rulevalue}
                        </option>
                      ))}
                    </select>

                    {this.state.validations.subscriber_relation ? (
                      <span className="error animated bounce">
                        {this.state.validations.subscriber_relation}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article>

              {/* <article className="row">
                <article className="col-md-6">
                  <label className="control-label">Marks Obtained*</label>
                  <article className="form-group">
                    <input
                      name="preMarksoptain"
                      maxLength="4"
                      placeholder="Enter your marks obtained"
                      type="text"
                      id="marksObtained"
                      value={
                        userAcademicInfo.marksObtained
                          ? userAcademicInfo.marksObtained
                          : ""
                      }
                      onChange={event =>
                        this.currentEduFormChangeHandler(
                          event,
                          "userAcademicInfo"
                        )
                      }
                      className="form-control"
                    />

                    {this.state.validations.marksObtained ? (
                      <span className="error">
                        {this.state.validations.marksObtained}
                      </span>
                    ) : null}
                  </article>
                </article>
                <article className="col-md-6 radioGroupHeight">
                  <label className="control-label">Total Marks*</label>
                  <article className="form-group">
                    <input
                      name="preTotalmarksoptain"
                      type="text"
                      maxLength="4"
                      placeholder="Enter your total marks"
                      className="form-control"
                      id="totalMarks"
                      value={
                        userAcademicInfo.totalMarks
                          ? userAcademicInfo.totalMarks
                          : ""
                      }
                      onChange={event =>
                        this.currentEduFormChangeHandler(
                          event,
                          "userAcademicInfo"
                        )
                      }
                    />
                    {this.state.validations.totalMarks ? (
                      <span className="error">
                        {this.state.validations.totalMarks}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article> */}

              <article className="row">
                {/* <article className="col-md-6 ">
                  <article className="form-group">
                    <label htmlFor="state">Previous Class Percentage</label>
                    <input
                      placeholder="Enter previous class percentage"
                      className="form-control"
                      id="percentage"
                      name="percentage"
                      type="text"
                      readOnly={true}
                      value={
                        userAcademicInfo.percentage
                          ? userAcademicInfo.percentage
                          : ""
                      }
                      onChange={event =>
                        this.currentEduFormChangeHandler(
                          event,
                          "userAcademicInfo"
                        )
                      }
                    />
                  </article>
                </article> */}
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="physically-challenged">
                      Physically Challenged*
                    </label>
                    <select
                      name="physically-challenged"
                      className="form-control"
                      id="physically-challenged"
                      value={disabled.ruleId}
                      onChange={event =>
                        this.currentFormRulesHandler(event, "disabled")
                      }
                    >
                      <option value="">--Select--</option>
                      <option value="903">NO</option>
                      <option value="700">YES</option>
                    </select>
                    {this.state.validations.disabled ? (
                      <span className="error animated bounce">
                        {this.state.validations.disabled}
                      </span>
                    ) : null}
                  </article>
                </article>
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="family_income">Annual Family Income*</label>
                    <article className="input-group inputWrapper">
                      <span className="input-group-addon preContent">INR</span>
                      <input
                        type="text"
                        name="annual_family_income"
                        className="form-control"
                        aria-label="Amount (to the nearest dollar)"
                        id="familyIncome"
                        value={
                          personalInfo.familyIncome
                            ? personalInfo.familyIncome
                            : ""
                        }
                        onChange={event =>
                          this.currentProfileFormChangeHandler(
                            event,
                            "familyIncome"
                          )
                        }
                      />
                      <span className="input-group-addon postContent">
                        Annual
                      </span>
                    </article>
                    {this.state.validations.familyIncome ? (
                      <span className="error animated bounce">
                        {this.state.validations.familyIncome}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article>

              <article className="row">
                <article className="col-md-6">
                  <article className="form-group">
                    <label htmlFor="state">
                      Need International Scholarships
                    </label>
                    <select
                      name="foreign"
                      className="form-control"
                      id="foreign"
                      value={foreign.ruleId}
                      onChange={event =>
                        this.currentFormRulesHandler(event, "foreign")
                      }
                    >
                      {/* <option value="">--Select--</option> */}
                      <option value="820">No</option>
                      {/* <option value="698">Yes</option> */}
                    </select>

                    {this.state.validations.foreign ? (
                      <span className="error animated bounce">
                        {this.state.validations.foreign}
                      </span>
                    ) : null}
                  </article>
                </article>
              </article>

              <article className="row">
                <article className="col-md-12">
                  <article className="form-group multiOptionCrtl">
                    <label htmlFor="state">
                      Select Interest
                      <i>*</i>
                    </label>
                    <article className="ctrl-wrapper">
                      <article className="form-group interest">
                        {rulesList["special"].map(list => (
                          <label htmlFor={list.id} key={list.id}>
                            <input
                              type="checkbox"
                              id={list.id}
                              value={list.id ? list.id : ""}
                              checked={
                                this.state.dropDownRules["special"][list.id]
                              }
                              onChange={event =>
                                this.onCheckBoxToggleHandler(event)
                              }
                            />
                            {list.rulevalue}
                            <dd className="chkbox" />
                          </label>
                        ))}
                        {this.state.validations.interest ? (
                          <span className="error animated bounce">
                            {this.state.validations.interest}
                          </span>
                        ) : null}
                      </article>
                    </article>
                  </article>
                </article>
              </article>
            </article>
          </article>
          <article className="tnc">
            <article className="ctrl-wrapper widget-border bg">
              <article className="row">
                <article className="col-md-12">
                  <article className="form-group multiOptionCrtl">
                    <article className="ctrl-wrapper">
                      <article className="form-group interest agree ">
                        <label htmlFor="1">
                          <input
                            onClick={event => this.onChangeTNC(event)}
                            type="checkbox"
                            id="1"
                          />

                          <dd className="chkbox" />
                        </label>
                        I agree to Buddy4Study's{" "}
                        <Link to="/terms-and-conditions" target="_blank">
                          <strong>Terms & conditions</strong>
                        </Link>{" "}
                        {this.state.checkTNCValidation ? (
                          <span className="error animated bounce">
                            {this.state.checkTNCValidation}
                          </span>
                        ) : null}
                      </article>
                    </article>
                  </article>
                </article>
              </article>
            </article>
          </article>
        </article>
      );
    }
    return (
      <section>
        <Loader isLoader={this.props.showLoader} />

        <form
          onSubmit={event => this.onSubmitHandler(event)}
          name="profileForm"
          autoCapitalize="off"
        >
          {studentFormContent}
          <article className="row">
            <article className="col-md-12">
              <article className="btn-ctrl">
                <button type="submit">Save</button>
              </article>
            </article>
          </article>
        </form>
      </section>
    );
  }
}
