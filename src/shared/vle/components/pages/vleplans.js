import React, { Component } from "react";
import { IMButtonJS, messages } from "../../../../constants/constants";
import gblFunc from "../../../../globals/globalFunctions";
import { PayNowPopUP } from "./payNow";
import {
  FETCH_MARCHANT_ID_SUCCESS,
  FETCH_MARCHANT_ID_FAIL
} from "../../actions";
import AlertMessage from "../../../common/components/alertMsg";
import Loader from "../../../common/components/loader";
class VlePaymentPlans extends Component {
  constructor(props) {
    super(props);
    this.state = {
      studentData: null,
      studentPaymentConfig: {
        url: "",
        text: "Checkout"
      },

      showAlert: false,
      msg: "",
      selectedPlanId: null,
      createPayButton: false
    };
    this.closePaymentPopUp = this.closePaymentPopUp.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
  }

  componentDidMount() {
    console.log(this.props, this.state);
    const type = "VLE";
    if (this.props.location.state && this.props.location.state.studentData) {
      const inputData = this.props.location.state.studentData.paymentPlanId
        ? { parentPlanId: this.props.location.state.studentData.paymentPlanId }
        : {};
      this.props.fetchPaymentPlans(inputData, type);
      this.setState({ studentData: this.props.location.state.studentData });
    } else {
      this.props.history.push("/vle/student-list");
    }
  }
  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case FETCH_MARCHANT_ID_SUCCESS:
        const updateStudentPayment = { ...this.state.studentPaymentConfig };
        if (this.state.studentData) {
          const studentData = { ...this.state.studentData };
          updateStudentPayment.url = `https://www.instamojo.com/buddy4study/smp-premium-application/?data_name=${
            studentData.firstName
            } ${studentData.lastName}&data_email=${
            studentData.email
            }&data_phone=${
            studentData.mobile
            }&data_hidden=data_Field_4466&data_readonly=data_amount`;
        }
        const that = this;
        this.setState({
          studentPaymentConfig: updateStudentPayment,
          createPayButton: true
        });
        try {
          var promise = gblFunc.loadJsScript(IMButtonJS, true);
          promise.then(
            /**************************************************************************** */
            /* I know this is dirty approach - exploring for standard solution@pushpendra
            /****************************************************************************** */

            function () {
              try {
                [
                  ...document.getElementsByClassName(
                    "im-checkout-btn btn--flat"
                  )
                ]
                  .filter(item => {
                    return (
                      item.parentNode.parentNode.id == that.state.selectedPlanId
                    );
                  })[0]
                  .click();
                that.setState({ createPayButton: false });
              } catch (error) {
                //fallback handling......Payment will be processed in every case
                that.setState({ paypopup: true });
              }
              /**************************************************************************** */
              /* I know this is dirty approach - exploring for standard solution@pushpendra
            /****************************************************************************** */
            },
            function (error) {
              gblFunc.logMessage(error);
            }
          );
        } catch (error) {
          gblFunc.logMessage(error);
        }
        break;
      case FETCH_MARCHANT_ID_FAIL:
        this.setState({
          showAlert: true,
          msg: nextProps.errorCode
            ? messages.generic.httpErrors[nextProps.errorCode]
            : "Sorry! Something went wrong."
        });
        break;
    }
  }
  openPaymentPopUp(plan) {
    const userData = gblFunc.getStoreUserDetails();
    this.setState({ selectedPlanId: plan.id });
    this.props.getMerchantId({
      userId: userData.userId,
      marchentObj: {
        amount: plan.amount,
        paymentPlanId: plan.id,
        currency: "inr",
        initiatorUserId: userData.userId,
        paidForUserId: this.state.studentData
          ? this.state.studentData.id
          : null,
        paymentPartner: "VLE",
        transactionMode: "D",
        transactionType: "D"
      }
    });
  }
  hideAlert() {
    this.setState({ msg: "", showAlert: false });
  }
  closePaymentPopUp() {
    this.setState({ paypopup: false });
  }
  render() {
    var that = this;
    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          isShow={this.state.showAlert}
          msg={this.state.msg}
          status={false}
          close={this.hideAlert}
        />
        {that.state.paypopup ? (
          <PayNowPopUP
            closePaymentPopUp={that.closePaymentPopUp}
            paymentConfig={that.state.studentPaymentConfig}
            htmlContent={`Access Matched Scholarships<br/>Receive Timely Alerts<br/>Dedicated Application Support`}
            payment_type="PREMIUM PROFILE"
          />
        ) : null}
        <article>
          <article className="row vleplans">
            <div className="col-md-12">
              <h4 className="titletext marginTop">
                Association Proposal Plans
              </h4>

              {this.props.vlePaymentPlans ? (
                <div className="price">
                  {this.props.vlePaymentPlans.map(function (plan) {
                    return (
                      <div
                        className={`${
                          plan.id == 2
                            ? "basic"
                            : plan.id == 3
                              ? "ultimite"
                              : ""
                          } plan col-md-4 col-xs-12`}
                        key={plan.id}
                      >
                        <div className="plan-inner">
                          <div className="entry-title">
                            <h3>{plan.planName}</h3>
                            <div
                              className="price"
                              dangerouslySetInnerHTML={{
                                __html: plan.amountDescription
                              }}
                            />
                          </div>

                          <div className="entry-content">
                            <p>
                              <strong>Services Offered</strong>
                            </p>
                            <ul
                              dangerouslySetInnerHTML={{
                                __html: plan.servicesOffered
                              }}
                            />
                          </div>
                          {that.state.createPayButton ? (
                            <div id={plan.id} className="hide">
                              <PayButtonLink
                                url={`${
                                  that.state.studentPaymentConfig.url
                                  }&data_amount=${plan.amount}&data_Field_4466=${
                                  that.props.marchantID
                                    ? that.props.marchantID
                                      .merchantTransactionNumber
                                    : ""
                                  }`}
                                text={that.state.studentPaymentConfig.text}
                              />
                            </div>
                          ) : null}

                          <button
                            className="btn"
                            onClick={() => that.openPaymentPopUp(plan)}
                          >
                            Pay Now
                          </button>
                        </div>
                      </div>
                    );
                  })}
                </div>
              ) : null}
            </div>
          </article>
        </article>
      </section>
    );
  }
}
const PayButtonLink = props => {
  return (
    <a
      href={props.url}
      rel="im-checkout"
      data-behaviour="remote"
      data-style="flat"
      data-text={props.text}
      className="btn"
    />
  );
};

export default VlePaymentPlans;
