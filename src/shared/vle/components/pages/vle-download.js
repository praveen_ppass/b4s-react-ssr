import React, { Component } from "react";

class Vledownloads extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <article className="vledownload">
        <h4 className="titletext marginTop">Download Creative </h4>
        <p>
          Use this ready-to-print poster to spread information about the program
          in your office, during seminars or other requirements.
        </p>
        <a
          href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/files/vle-creatives.zip"
          className="btn"
        >
          Download Creative
        </a>
      </article>
    );
  }
}

export default Vledownloads;
