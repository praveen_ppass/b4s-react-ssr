import React, { Component } from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import Loader from "../../../common/components/loader";
import AlertMessage from "../../../common/components/alertMsg";
import { messages, imgBaseUrl } from "../../../../constants/constants";
import { Redirect } from "react-router-dom";
import {
  ADD_TO_FAV_SUCCESS,
  FETCH_MY_FAVORITE_SUCCESS,
  VLE_FETCH_MATCHING_SCHOLARSHIPS_SUCCESS
} from "../../actions";
import { FETCH_USER_MATCHING_RULES_SUCCEEDED } from "../../../../constants/commonActions";
import gblFunc from "../../../../globals/globalFunctions";

export class Scholarship extends Component {
  constructor(props) {
    super(props);

    this.onMyFavHandler = this.onMyFavHandler.bind(this);
    this.onMatchingSchApi = this.onMatchingSchApi.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.mapFavWithMatchedSch = this.mapFavWithMatchedSch.bind(this);
    this.onPopUpHandler = this.onPopUpHandler.bind(this);
    this.state = {
      ON_FIRST_LOAD: true,
      exception: null,
      isPartialOrFull: true,
      active: true,
      isShowAlert: false,
      matchingScholarships: null,
      isMatchPage: false,
      title: "FULL MATCHED SCHOLARSHIPS",
      PAID: "",
      studentList: null,
      userId: null
    };
  }

  componentDidMount() {
    const percentage = localStorage.getItem("percentage");
    const { state } = this.props.history.location;
    // if (parseInt(percentage) == 100) {
    if (state && state.studentList) {
      this.props.fetchMatchRules({ userId: state.studentList.id });
      this.setState({
        ON_FIRST_LOAD: true,
        PAID: 100,
        userId: state.studentList.id,
        studentList: state.studentList,
        studentName: state.studentList
          ? state.studentList.firstName
            ? state.studentList.firstName
            : "Student"
          : "Student"
      });
    } else {
      this.setState({
        studentList: {}
      });
    }

    // }
  }

  componentWillReceiveProps(nextProps) {
    const {
      matchingRules,
      type,
      matchingScholarships,
      favScholarships
    } = nextProps;

    const { isMatchPage } = this.state;

    switch (type) {
      case ADD_TO_FAV_SUCCESS:
        if (isMatchPage) {
          this.setState({
            isShowAlert: true
          });
          //this.props.fetchMyFavScholarships({ userId: this.props.userId });
        }

        break;
      case FETCH_USER_MATCHING_RULES_SUCCEEDED:
        if (this.state.studentList && this.state.studentList.id) {
          this.onMatchingSchApi(matchingRules, parseInt(this.state.userId, 10));
        }
        break;
      case VLE_FETCH_MATCHING_SCHOLARSHIPS_SUCCESS:
        //this.props.fetchMyFavScholarships({ userId: this.props.userId });

        this.setState({
          matchingScholarships
        });

        break;
      case FETCH_MY_FAVORITE_SUCCESS:
        const matchedScholarships = {
          ...this.state.matchingScholarships
        };

        for (let key in matchedScholarships) {
          this.mapFavWithMatchedSch(favScholarships, key, matchedScholarships);
        }

        break;
    }

    this.setState({
      ON_FIRST_LOAD: false
    });
  }

  onPopUpHandler() { }

  mapFavWithMatchedSch(favoriteSch, key, matchedSch) {
    favoriteSch.forEach(favSch => {
      if (matchedSch[key].length > 0) {
        matchedSch[key].map((matchSch, index) => {
          return favSch.id === matchSch.id
            ? (matchedSch[key][index].isfav = true)
            : matchedSch[key][index].isfav != true
              ? (matchedSch[key][index].isfav = false)
              : (matchedSch[key][index].isfav = true);
        });
      }
    });

    this.setState({
      matchingScholarships: { ...matchedSch }
    });
  }

  onMatchingSchApi(matchRules, userId) {
    const ruleType = matchRules;
    this.props.fetchMatchingScholarships({
      type: "user",
      Rule_Type: ruleType,
      userId
    });
    this.setState({
      ON_FIRST_LOAD: false
    });
  }
  toggleMatchedScholarship(tab, title) {
    var tabs = {
      full: true,
      partials: false
    };

    this.setState({
      isPartialOrFull: tabs[tab],
      active: tab == "full" ? true : false,
      title
    });
  }

  onMyFavHandler(schId) {
    this.props.addToFavScholarships({
      userId: this.props.userId,
      scholarshipId: schId
    });

    this.setState({
      isMatchPage: true
    });
  }

  hideAlert() {
    this.setState({
      isShowAlert: false
    });
  }
  render() {
    const { matchingScholarships, title, studentList } = this.state;

    if (studentList && Object.keys(studentList).length === 0) {
      return <Redirect to="/" />;
    }
    let totalSch = [];
    if (matchingScholarships && Object.keys(matchingScholarships).length > 0) {
      totalSch = matchingScholarships.full.concat(matchingScholarships.partial);
    }
    const { isPartialOrFull } = this.state;
    let matchSchTag = null;
    // if (matchingScholarships && Object.keys(matchingScholarships).length > 0) {
    //   const { full, partial } = matchingScholarships;
    //   matchSchTag = isPartialOrFull ? (
    //     <Full
    //       full={full}
    //       activeScholarship={this.state.active}
    //       addToFav={this.onMyFavHandler}
    //     />
    //   ) : (
    //     <Partials
    //       partial={partial}
    //       activeScholarship={this.state.active}
    //       addToFav={this.onMyFavHandler}
    //     />
    //   );
    // }
    return (
      <section>
        <AlertMessage
          isShow={this.state.isShowAlert}
          msg={messages.addMyFavScholarship.success}
          status={true}
          close={this.hideAlert}
        />
        <h1 className="matchedScholar">
          Matched scholarships for {this.state.studentName}{" "}
          <Link to="/vle/student-list">
            <i className="fa fa-long-arrow-left" aria-hidden="true" /> Back
          </Link>
        </h1>
        <article className="row">
          <article className="col-md-12 col-sm-12 schlarshipmatching flex-container-list">
            <article className="floatBox">
              <Full
                full={totalSch}
                studentList={studentList}
                activeScholarship={this.state.active}
                addToFav={this.onMyFavHandler}
              />
            </article>
          </article>
        </article>
      </section>
    );
  }
}

const Partials = ({ partial, activeScholarship, addToFav }) => {
  return (
    <section>
      {partial.length > 0 ? (
        <ScholarshipItem
          scholarships={partial}
          activeScholarship={activeScholarship}
          addToFav={addToFav}
        />
      ) : (
          <p className="msgTextWrapper">No Scholarship Found</p>
        )}
    </section>
  );
};

const Full = ({ full, activeScholarship, addToFav, studentList }) => {
  return (
    <section>
      {full.length > 0 ? (
        <ScholarshipItem
          scholarships={full}
          activeScholarship={activeScholarship}
          addToFav={addToFav}
        />
      ) : null}
    </section>
  );
};

const ScholarshipItem = ({ scholarships, activeScholarship, addToFav }) => {
  return scholarships.map((list, index) => {
    let daysToGO =
      parseInt(
        moment(list.deadlineDate, "YYYY-MM-DD").diff(
          moment().startOf("day"),
          "days"
        )
      ) + 1;

    let daysLabel;
    if (daysToGO === 1) {
      daysLabel = "Last";
    } else if (daysToGO === 2) {
      daysLabel = "1";
    } else {
      daysLabel = daysToGO;
    }
    // let viewCounter;

    // if (
    //   list.scholarshipMultilinguals &&
    //   list.scholarshipMultilinguals[0] &&
    //   list.scholarshipMultilinguals[0].viewCounter
    // ) {
    //   viewCounter = list.scholarshipMultilinguals[0].viewCounter;
    // }
    return (
      <article className="box posR" key={index}>
        <article className="data-row flex-container">
          <section className="col-md-2 col-sm-12 flex-item">
            <article className="tablecell">
              <article className="logotext">
                <Link to={`/scholarship/${list.slug}`}>
                  <img
                    alt={list.scholarshipName}
                    src={list.logoFid ? list.logoFid : ""}
                    className="img-responsive"
                  />
                </Link>
                {/* <p>
                  {parseInt(viewCounter)
                    ? `${
                    parseInt(viewCounter) > 1
                      ? parseInt(viewCounter)
                      : "view"
                    } views`
                    : 0 + "view"}
                </p> */}
              </article>
            </article>
          </section>
          <section className="col-md-7 col-sm-9 flex-item">
            <article className="tablecell">
              <article className="contentdisplay">
                <h2>
                  <Link className="ellipsis" to={`/scholarship/${list.slug}`}>
                    <span
                      dangerouslySetInnerHTML={{
                        __html: list.scholarshipName
                          ? gblFunc.replaceWithLoreal(list.scholarshipName)
                          : ""
                      }}
                    />
                  </Link>
                  <article className="namelistTooltip">
                    <span
                      dangerouslySetInnerHTML={{
                        __html: list.scholarshipName
                          ? gblFunc.replaceWithLoreal(list.scholarshipName)
                          : ""
                      }}
                    />
                    <i className="arrow" />
                  </article>
                </h2>
                <p>
                  <img
                    src={`${imgBaseUrl}scholarship-icon.png`}
                    alt="scholarship-icon"
                  />
                  {list.scholarshipMultilinguals.length > 0
                    ? list.scholarshipMultilinguals[0].applicableFor
                    : ""}
                </p>
                <p>
                  <img src={`${imgBaseUrl}awards-icon.png`} alt="awards-icon" />
                  {list.scholarshipMultilinguals.length > 0
                    ? list.scholarshipMultilinguals[0].purposeAward
                    : ""}
                </p>
              </article>
            </article>
          </section>

          {/* <article className="col-md-3 col-sm-3 flex-item">
            <article className="tablecell">
              {daysToGO < 16 ? (
                <article>
                  <article
                    className={daysToGO < 8 ? "daysgo pink" : "daysgo yellow"}
                  >
                    <span>{daysLabel}</span>
                    <p>{`day${daysToGO > 1 ? "s" : ""} to go`}</p>
                  </article>
                </article>
              ) : (
                <article>
                  <article className="calender">
                    <p className="text-center">Last Date to apply</p>
                    <dd>{moment(list.deadlineDate).format("D")}</dd>
                    <p className="text-center date">
                      {moment(list.deadlineDate).format("MMM, YY")}
                    </p>
                  </article>
                </article>
              )}

              
            </article>
          </article> */}
          {daysToGO >= 1 ? (
            list.deadlineDate ? (
              <article className="col-md-3 col-sm-3 flex-item">
                <article className="tablecell">
                  {daysToGO < 17 ? (
                    <article>
                      <article
                        className={
                          daysToGO < 8 ? "daysgo pink" : "daysgo yellow"
                        }
                      >
                        <span>{daysLabel}</span>
                        <p>{`day${daysToGO > 1 ? "s" : ""} to go`}</p>
                      </article>
                    </article>
                  ) : (
                      <article>
                        <article className="calender">
                          <p className="text-center">Last Date to apply</p>
                          <dd>{moment(list.deadlineDate).format("D")}</dd>
                          <p className="text-center date">
                            {moment(list.deadlineDate).format("MMM, YY")}
                          </p>
                        </article>
                      </article>
                    )}
                </article>
              </article>
            ) : (
                ""
              )
          ) : list.deadlineDate ? (
            <article className="col-md-3 col-sm-3 flex-item">
              <article className="tablecell">
                <article className="daysgo gray">
                  <span>Closed</span>
                </article>
              </article>
            </article>
          ) : (
                <article className="col-md-3 col-sm-3 flex-item">
                  <article className="tablecell">
                    <article className="daysgo pink always">
                      <p>Always Open</p>
                    </article>
                  </article>
                </article>
              )}
        </article>
      </article>
    );
  });
};
