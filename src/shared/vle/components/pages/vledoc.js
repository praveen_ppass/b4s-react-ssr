import React, { Component } from "react";
import {
  required,
  maxFileSize,
  hasValidExtension
} from "../../../../validation/rules";
import { ruleRunner } from "../../../../validation/ruleRunner";
import AlertMessage from "../../../common/components/alertMsg";
import Loader from "../../../common/components/loader";
import { Route, Link, Redirect } from "react-router-dom";
import {
  allowedDocumentExtensions,
  imageFileExtension
} from "../../../../constants/constants";
import Cropper from "react-cropper";
if (typeof window !== "undefined") {
  require("cropperjs/dist/cropper.css");
}
import ConfirmMessagePopup from "../../../common/components/confirmMessagePopup";

class DocumentNeeded extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPopup: false,
      userId: null,
      categoryId: null,
      documentList: [],
      showLoader: false,
      disabilityId: null,
      stateWiseId: null,
      msg: "",
      status: "",
      isDropDownCategory: {},
      isDropDownOption: {},
      isFormValid: false,
      year: [
        { id: "28", value: "1st Year" },
        { id: "29", value: "2nd Year" },
        { id: "32", value: "3rd Year" },
        { id: "33", value: "4th Year" },
        { id: "34", value: "5th Year" },
        { id: "35", value: "6th Year" }
      ],
      semester: [
        { id: "36", value: "1st Semester" },
        { id: "37", value: "2nd Semester" },
        { id: "38", value: "3rd Semester" },
        { id: "39", value: "4th Semester" },
        { id: "40", value: "5th Semester" },
        { id: "41", value: "6th Semester" },
        { id: "42", value: "7th Semester" },
        { id: "43", value: "8th Semester" },
        { id: "44", value: "9th Semester" },
        { id: "45", value: "10th Semester" }
      ],
      validations: {},
      docFile: null,
      isShowCropPup: false,
      cropSrc: "",
      cropResult: null,
      showConfirmationPopup: false,
      deleteSuccessCallBack: null,
      isNext: false,
      isCompleted: false
    };

    this.cropImage = this.cropImage.bind(this);
    this.dataURLtoFile = this.dataURLtoFile.bind(this);
    this.checkValidation = this.checkValidation.bind(this);
    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.hideConfirmationPopup = this.hideConfirmationPopup.bind(this);
    this.fetchUserMatchingRules = this.fetchUserMatchingRules.bind(this);
  }

  uploadDocumentWithOutCrop(docObj, files) {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = this.state.userId;
      scholarshipId = 7835;
    }

    const nullIfFalsy = value => value || null;

    if ([6, 7].indexOf(docObj.documentType["id"]) > -1) {
      const stringifiedData = JSON.stringify({
        documentType: nullIfFalsy(docObj["documentType"]["id"]),
        documentTypeCategory: nullIfFalsy(
          this.state.isDropDownCategory[docObj.documentType["id"]]
        ), // pass type Year or Semester
        documentTypeCategoryOption: nullIfFalsy(
          this.state.isDropDownOption[docObj.documentType["id"]]
        ) // pass year Value or Semester Value
      });

      this.props.uploadAllDocuement({
        docFile: files[0],
        userDocRequest: stringifiedData,
        userId,
        isPosted: this.state.isCompleted
      });
    } else {
      const stringifiedData = JSON.stringify({
        documentType: nullIfFalsy(docObj["documentType"]["id"]),
        documentTypeCategory: nullIfFalsy(null), // pass type Year or Semester
        documentTypeCategoryOption: nullIfFalsy(null) // pass year Value or Semester Value
      });

      this.props.uploadAllDocuement({
        docFile: files[0],
        userDocRequest: stringifiedData,
        userId,
        isPosted: this.state.isCompleted
      });
    }
  }

  getDocData(event, docObj) {
    let validations = { ...this.state.validations };
    let validationResult = null;
    let isFormValid = true;
    const { id, value, files } = event.target;
    const uploadedFile = files[0];
    const sizeInMb = Math.ceil(uploadedFile.size / (1024 * 1024));
    const fileExtension = uploadedFile.name.slice(
      uploadedFile.name.lastIndexOf(".")
    );
    if (docObj.documentType["id"] == 1) {
      validationResult = ruleRunner(
        fileExtension,
        docObj.documentType["id"],
        "File",
        hasValidExtension(imageFileExtension)
      );
    } else {
      validationResult = ruleRunner(
        fileExtension,
        docObj.documentType["id"],
        "File",
        hasValidExtension(allowedDocumentExtensions)
      );
    }

    if (validationResult[docObj.documentType["id"]] === null) {
      validationResult = ruleRunner(
        sizeInMb,
        docObj.documentType["id"],
        "Uploaded file",
        maxFileSize(1)
      );
    }
    validations[docObj.documentType["id"]] =
      validationResult[docObj.documentType["id"]];
    if (validationResult[docObj.documentType["id"]] !== null) {
      // if invalid extention and size
      isFormValid = false;
    }
    //show croper if document type would be 1
    if (isFormValid) {
      // if form should be valid

      if (docObj.documentType["id"] == 1) {
        this.state.isShowCropPup = true;
      }

      let reader = new FileReader(); // read upload file
      reader.onload = e => {
        this.setState({ cropSrc: e.target.result });
      };
      reader.readAsDataURL(event.target.files[0]);
    }

    this.setState({
      validations,
      isShowCropPup: this.state.isShowCropPup,
      docFile: files
    });

    if (docObj.documentType["id"] != 1 && isFormValid) {
      // only those document having no 1 Id
      this.uploadDocumentWithOutCrop(docObj, files);
    }
  }

  deleteDocument(id, docObj) {
    let userId = "";
    if (window != undefined) {
      userId = this.state.userId;
    }

    const deleteSuccessCallBack = () =>
      this.props.deleteDocuement({
        docId: id,
        userId,
        scholarshipId: 7835,
        isDeleted: true
      });

    this.setState({
      showConfirmationPopup: true,
      deleteSuccessCallBack
    });

    delete this.state.isDropDownCategory[docObj.documentType["id"]];
    delete this.state.isDropDownOption[docObj.documentType["id"]];
  }

  showConfirmationPopup() {
    this.setState({
      showConfirmationPopup: true
    });
  }

  hideConfirmationPopup() {
    this.setState({
      showConfirmationPopup: false,
      deleteSuccessCallBack: null
    });
  }

  getAllDocument() {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = this.state.userId;
      scholarshipId = 7835;
    }
    this.props.getAllDocuement({
      userId,
      scholarshipId
    });
  }

  componentDidMount() {
    const { state } = this.props.history.location;
    this.setState(
      {
        userId: state && state.studentData ? state.studentData.id : null
      },
      () => this.fetchUserMatchingRules()
    );
  }

  fetchUserMatchingRules() {
    this.props.fetchMatchRules({
      userId: this.state.userId
    });
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    let scholarshipId = ""; // PASS SCHOLARSHIP ID FOR COMPLETED STEPS
    if (window != undefined) {
      scholarshipId = 7835;
    }

    switch (type) {
      case "FETCH_USER_MATCHING_RULES_SUCCEEDED":
        const categoryId = nextProps.matchingRules[6][0];
        const disabilityId = nextProps.matchingRules[48]
          ? nextProps.matchingRules[48][0]
          : null;
        const stateWiseId = nextProps.matchingRules[21]
          ? nextProps.matchingRules[21][0]
          : null;

        this.setState(
          {
            categoryId,
            disabilityId,
            stateWiseId
          },
          () => this.getAllDocument()
        );

        break;
      case "GET_ALL_DOCUMENT_SUCCESS":
        const { studentData } = this.props.history.location.state;

        const common = [4, 5, 6, 7];
        let checkOnClassIdBasedValdiation = {
          1: common,
          2: common,
          3: common,
          4: common,
          5: common,
          6: common,
          7: common,
          8: common,
          9: common,
          10: common,
          11: [5, 6, 7],
          12: [5, 6, 7],
          20: [6, 7],
          16: [6, 7],
          21: [6, 7],
          22: [7],
          23: [],
          24: [],
          840: [4, 5, 6, 7]
        };

        let nonMandotaryFields = [36, 32, 12];

        nextProps.allDocument["documentDataList"].map(res => {
          let validatedClassIDs =
            checkOnClassIdBasedValdiation[studentData.academicClass.id];
          validatedClassIDs = validatedClassIDs.concat(nonMandotaryFields);
          if (
            studentData.academicClass.id &&
            validatedClassIDs.indexOf(res.documentType["id"]) === -1
          ) {
            this.state.validations[res.documentType["id"]] = null;
          }
          if (this.state.categoryId === 117) {
            delete this.state.validations[14];
          }
          if (this.state.disabilityId && this.state.disabilityId == 903) {
            delete this.state.validations[60];
          }
          if (!this.state.disabilityId) {
            delete this.state.validations[60];
          }
          if (
            this.state.stateWiseId &&
            (this.state.stateWiseId !== 467 || this.state.stateWiseId !== 476)
          ) {
            delete this.state.validations[61];
          }
          if ([6, 7].indexOf(res.documentType["id"]) > -1) {
            //return (this.state.isDropDownCategory[res.documentType["id"]] = "");
          } else {
            return {};
          }
        });
        this.setState({
          documentList: nextProps.allDocument["documentDataList"],
          isDropDownCategory: this.state.isDropDownCategory,
          validations: this.state.validations
        });

        //VALIDATE THE DOCUMENT HAS BEEN COMPLETED
        let tempArray = [];
        if (
          nextProps.allDocument["documentDataList"] != null &&
          nextProps.allDocument["documentDataList"].length > 0
        ) {
          nextProps.allDocument["documentDataList"].map((res, i) => {
            if (res.userDocument != null) {
              tempArray.push(i);
            }
          });
        }

        if (
          tempArray.length + 1 ==
          nextProps.allDocument["documentDataList"].length
        ) {
          this.setState({
            isCompleted: true
          });
        } else {
          this.setState({
            isCompleted: false
          });
        }

        break;
      case "GET_ALL_DOCUMENT_UPLOAD_SUCCESS":
        this.setState(
          {
            status: true,
            msg: "Document has been Uploaded",
            showLoader: true
          },
          () => this.getAllDocument()
        );

        if (this.state.isCompleted) {
          //UPDATE THE SETP CALL
          this.props.applicationInstructionStep({
            scholarshipId
          });
        }

        break;
      case "GET_ALL_DOCUMENT_UPLOAD_DELETE_SUCCESS":
        this.setState(
          {
            status: true,
            msg: "Document has been Deleted",
            showLoader: true,
            showConfirmationPopup: false,
            deleteSuccessCallBack: null
          },
          () => this.getAllDocument()
        );

        // if (this.state.isCompleted) {
        //   //UPDATE THE SETP CALL
        //   this.props.applicationInstructionStep({
        //     scholarshipId
        //   });
        // }
        break;

      case "GET_ALL_DOCUMENT_UPLOAD_DELETE_FAILURE":
        this.setState({
          status: false,
          msg: "No Record Deleted",
          showLoader: true,
          deleteSuccessCallBack: null,
          showConfirmationPopup: false
        });
        break;
      case "GET_ALL_DOCUMENT_UPLOAD_COMPLETE_SUCCESS":
        this.setState(
          {
            status: true,
            msg: "All Document has been Uploaded",
            showLoader: true,
            showConfirmationPopup: false,
            deleteSuccessCallBack: null
          },
          () => this.getAllDocument()
        );

        break;
    }
  }

  getCategory(event, docId) {
    if (event.target.value == 7) {
      // for year
      this.state.isDropDownCategory[docId] = event.target.value;
    } else if (event.target.value == 8) {
      this.state.isDropDownCategory[docId] = event.target.value;
    } else {
      this.state.isDropDownCategory[docId] = "";
    }
    this.setState({
      isDropDownCategory: this.state.isDropDownCategory
    });
  }

  getCategoryOption(event, docId) {
    this.state.isDropDownOption[docId] = event.target.value;
    this.setState({
      isDropDownOption: this.state.isDropDownOption
    });
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    if (this.state.validations.hasOwnProperty(fieldID)) {
      validationObject.name = "*Field";
      validationObject.validationFunctions = [required];
    }
    return validationObject;
  }

  checkValidation() {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = this.state.userId;
      scholarshipId = 7835;
    }
    let isValidate = this.checkFormValidations();
    if (isValidate) {
      this.props.showAlertMessage();
    }
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);

      let documentList = this.state.documentList
        .filter(res => {
          return res.documentType["id"] == key;
        })
        .map(item => {
          return item.userDocument;
        });

      let validationResult = ruleRunner(
        documentList[0],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
  }

  dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(","),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }

  cropImage(docObj) {
    if (typeof this.cropper.getCroppedCanvas() === "undefined") {
      return;
    }

    const selectedFile = this.state.docFile[0];

    let customizeFileName =
      selectedFile.name.substr(0, selectedFile.name.lastIndexOf(".")) +
      selectedFile.name
        .substr(selectedFile.name.lastIndexOf("."))
        .toLowerCase();

    var file = this.dataURLtoFile(
      this.cropper
        .getCroppedCanvas({ width: 200, height: 200 })
        .toDataURL("image/jpeg", 0.8),
      customizeFileName
    );

    this.setState(
      {
        cropResult: file,
        isShowCropPup: false
      },
      () => this.uploadDocumentWithOutCrop(docObj, Array(file))
    );
  }

  closePopUp() {
    this.setState({
      isShowCropPup: false
    });
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  render() {
    return (
      <section className="sectionwhite">
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <ConfirmMessagePopup
          message={"Are you sure want to delete ?"}
          showPopup={this.state.showConfirmationPopup}
          onConfirmationSuccess={this.state.deleteSuccessCallBack}
          onConfirmationFailure={this.hideConfirmationPopup}
        />
        <article className="form">
          <article className="row">
            <article className="col-md-12 ">
              {this.state.documentList != null &&
              this.state.documentList.length > 0
                ? this.state.documentList.map((res, i) => {
                    if ([6, 7].indexOf(res.documentType["id"]) > -1) {
                      // if the category and option are availables
                      return (
                        <article key={i} className="graystrip">
                          <span>{res["docNameLabel"]}</span>
                          <article className="pull-right docWidth">
                            <article>
                              <article className="document">
                                <label className="custom-file-upload pull-right">
                                  <input
                                    onClick={event => {
                                      event.target.value = null;
                                    }}
                                    key={i}
                                    type="file"
                                    disabled={
                                      this.state.isDropDownCategory[
                                        res.documentType["id"]
                                      ] == ""
                                        ? true
                                        : false
                                    }
                                    className="btn top0"
                                    onChange={event =>
                                      this.getDocData(event, res)
                                    }
                                  />
                                  Browse
                                </label>

                                {this.state.validations != null &&
                                this.state.validations[
                                  res.documentType["id"]
                                ] ? (
                                  <span className="error errordoc animated bounce">
                                    {
                                      this.state.validations[
                                        res.documentType["id"]
                                      ]
                                    }
                                  </span>
                                ) : null}

                                <span className="doc-need">
                                  <select
                                    className="selectVle"
                                    onClick={event =>
                                      this.getCategory(
                                        event,
                                        res.documentType["id"]
                                      )
                                    }
                                    className="selectVle"
                                  >
                                    <option value="">Select Category</option>
                                    <option
                                      value="7"
                                      disabled={
                                        res.userDocument != null &&
                                        res.userDocument[0][
                                          "documentTypeCategory"
                                        ] &&
                                        res.userDocument[0][
                                          "documentTypeCategory"
                                        ]["id"] == 8
                                          ? true
                                          : false
                                      }
                                    >
                                      Year
                                    </option>
                                    <option
                                      value="8"
                                      disabled={
                                        res.userDocument != null &&
                                        res.userDocument[0][
                                          "documentTypeCategory"
                                        ] &&
                                        res.userDocument[0][
                                          "documentTypeCategory"
                                        ]["id"] == 7
                                          ? true
                                          : false
                                      }
                                    >
                                      Semester
                                    </option>
                                  </select>
                                </span>

                                {this.state.isDropDownCategory[
                                  res.documentType["id"]
                                ] == 7 ? (
                                  <span className="doc-need">
                                    <select
                                      className="selectVle"
                                      onChange={event =>
                                        this.getCategoryOption(
                                          event,
                                          res.documentType["id"]
                                        )
                                      }
                                    >
                                      <option value="">Select Year</option>
                                      {this.state.year.map((year, i) => {
                                        if (res.userDocument == null) {
                                          return (
                                            <option key={i} value={year.id}>
                                              {year.value}
                                            </option>
                                          );
                                        } else {
                                          let filterYr = res.userDocument.filter(
                                            filterYear => {
                                              return (
                                                filterYear[
                                                  "documentTypeCategoryOption"
                                                ] != null &&
                                                filterYear[
                                                  "documentTypeCategoryOption"
                                                ]["id"] == year.id
                                              );
                                            }
                                          );

                                          return (
                                            <option
                                              disabled={
                                                filterYr.length > 0 &&
                                                filterYr[0][
                                                  "documentTypeCategoryOption"
                                                ] != null &&
                                                filterYr[0][
                                                  "documentTypeCategoryOption"
                                                ]["id"] == year.id
                                                  ? true
                                                  : false
                                              }
                                              key={i}
                                              value={year.id}
                                            >
                                              {year.value}
                                            </option>
                                          );
                                        }
                                      })}
                                    </select>
                                  </span>
                                ) : (
                                  ""
                                )}

                                {this.state.isDropDownCategory[
                                  res.documentType["id"]
                                ] == 8 ? (
                                  <span className="doc-need">
                                    <select
                                      className="selectVle"
                                      onChange={event =>
                                        this.getCategoryOption(
                                          event,
                                          res.documentType["id"]
                                        )
                                      }
                                    >
                                      <option value="">Select Semester</option>
                                      {this.state.semester.map((sem, i) => {
                                        if (res.userDocument == null) {
                                          return (
                                            <option key={i} value={sem.id}>
                                              {sem.value}
                                            </option>
                                          );
                                        } else {
                                          let filterSem = res.userDocument.filter(
                                            filterSem => {
                                              return (
                                                filterSem[
                                                  "documentTypeCategoryOption"
                                                ] != null &&
                                                filterSem[
                                                  "documentTypeCategoryOption"
                                                ]["id"] == sem.id
                                              );
                                            }
                                          );
                                          return (
                                            <option
                                              disabled={
                                                filterSem.length > 0 &&
                                                filterSem[0][
                                                  "documentTypeCategoryOption"
                                                ] != null &&
                                                filterSem[0][
                                                  "documentTypeCategoryOption"
                                                ]["id"] == sem.id
                                                  ? true
                                                  : false
                                              }
                                              key={i}
                                              value={sem.id}
                                            >
                                              {sem.value}
                                            </option>
                                          );
                                        }
                                      })}
                                    </select>
                                  </span>
                                ) : (
                                  ""
                                )}
                              </article>
                            </article>
                          </article>

                          {res.userDocument != null ? (
                            <article className="docicon listItem">
                              {res.userDocument.map((doc, i) => {
                                return (
                                  <article key={i}>
                                    {/* <Link
                                          target="_blank"
                                          to={`${
                                            doc["location"]
                                              ? doc["location"]
                                              : ""
                                          }`}
                                        >
                                          {doc["documentDescription"]
                                            ? doc["documentDescription"]
                                            : ""}
                                        </Link> */}
                                    {/* <strong>
                                          {doc["documentTypeCategory"] !=
                                            null &&
                                            doc["documentTypeCategory"][
                                              "categoryName"
                                            ]}
                                        </strong> */}

                                    <article className="docList">
                                      <strong>
                                        {doc["documentTypeCategoryOption"] !=
                                          null &&
                                          doc["documentTypeCategoryOption"][
                                            "categoryName"
                                          ]}
                                      </strong>
                                      {doc.verified == 0 && (
                                        <span
                                          onClick={event =>
                                            this.deleteDocument(doc["id"], res)
                                          }
                                          key={i}
                                          className="deleteicon"
                                        />
                                      )}
                                      <Link
                                        target="_blank"
                                        to={`${
                                          doc["location"] ? doc["location"] : ""
                                        }`}
                                      >
                                        {/* {res.userDocument[0]["documentDescription"]}{" "} */}
                                        <i
                                          className="fa fa-download Checkicon"
                                          title={
                                            doc["documentDescription"]
                                              ? doc["documentDescription"]
                                              : ""
                                          }
                                        />
                                      </Link>
                                    </article>
                                  </article>
                                );
                              })}
                            </article>
                          ) : (
                            ""
                          )}
                        </article>
                      );
                    } else {
                      // if category and option are not available
                      return (
                        <article key={i}>
                          <article className="graystrip">
                            <span>{res["docNameLabel"]}</span>
                            <article className="docicon">
                              {res.userDocument == null ? (
                                <article>
                                  <article className="document">
                                    <label className="custom-file-upload pull-right">
                                      <input
                                        onClick={event => {
                                          event.target.value = null;
                                        }}
                                        key={i}
                                        type="file"
                                        disabled={
                                          this.state.isDropDownCategory[
                                            res.documentType["id"]
                                          ] == ""
                                            ? true
                                            : false
                                        }
                                        className="btn top0"
                                        onChange={event =>
                                          this.getDocData(event, res)
                                        }
                                      />
                                      Browse
                                    </label>

                                    {this.state.validations != null &&
                                    this.state.validations[
                                      res.documentType["id"]
                                    ] ? (
                                      <span className="error errordoc animated bounce">
                                        {
                                          this.state.validations[
                                            res.documentType["id"]
                                          ]
                                        }
                                      </span>
                                    ) : null}
                                  </article>
                                </article>
                              ) : (
                                <article className="iconCtrlWrapper">
                                  <Link
                                    target="_blank"
                                    to={`${res.userDocument[0]["location"]}`}
                                  >
                                    {/* {res.userDocument[0]["documentDescription"]}{" "} */}
                                    <i className="fa fa-download Checkicon" />
                                  </Link>

                                  {res.verificationStatus != 1 && (
                                    <span
                                      onClick={event =>
                                        this.deleteDocument(
                                          res.userDocument[0]["id"],
                                          res
                                        )
                                      }
                                      key={i}
                                      className="deleteicon"
                                    />
                                  )}
                                </article>
                              )}
                            </article>
                          </article>

                          {this.state.isShowCropPup &&
                          res.documentType["id"] == 1 ? (
                            <section className="cropImg">
                              <section className="cropImgbg">
                                <article className="popup-header">
                                  <button
                                    onClick={() => this.closePopUp()}
                                    type="button"
                                    className="close"
                                  >
                                    ×
                                  </button>
                                  <h3 className="popup-title">CROP IMAGE</h3>
                                </article>
                                <article className="popup-body">
                                  <article className="row">
                                    <article className="col-md-12">
                                      <article className="cropArea">
                                        <Cropper
                                          style={{
                                            height: "50vh",
                                            width: "100%"
                                          }}
                                          aspectRatio={1}
                                          zoomable={true}
                                          autoCropArea={1}
                                          preview=".img-preview"
                                          guides={false}
                                          src={this.state.cropSrc}
                                          ref={cropper => {
                                            this.cropper = cropper;
                                          }}
                                        />
                                      </article>
                                    </article>
                                  </article>
                                  <article className="row">
                                    <article className="col-md-12 text-center">
                                      <button
                                        className="updatePicBtn"
                                        onClick={event => this.cropImage(res)}
                                      >
                                        Update Picture
                                      </button>
                                    </article>
                                  </article>
                                </article>
                                <article className="popup-footer" />
                              </section>
                            </section>
                          ) : (
                            ""
                          )}
                        </article>
                      );
                    }
                  })
                : ""}

              <article className="row">
                <article className="col-md-12">
                  <input
                    type="submit"
                    value="Save"
                    onClick={this.checkValidation}
                    className="btn  pull-right"
                  />
                </article>
              </article>
            </article>
          </article>
          <article />
        </article>
      </section>
    );
  }
}

// <article popup-box-effect="" onClick={this.togglePopup.bind(this)}>
//   <article className="teamData box-height boxEffect border-bottom lineHover position">
//     {/* <img src={manjeet} /> */}
//     <h3>Manjeet Singh (Director &amp; Co-CEO)</h3>
//     <h4>Co-Founder</h4>
//   </article>
// </article>
// {this.state.showPopup ? (
//   <CropImg text="Close Me" closePopup={this.togglePopup.bind(this)} />

class CropImg extends React.Component {
  render() {
    return (
      <article className="popup">
        <article className="popupsms">
          <article className="popup_inner loginpopup">
            <article className="LoginRegPopup">
              <section className="modal fade modelAuthPopup1">
                <section className="modal-dialog">
                  <section className="modal-content modelBg  emailverification">
                    <article className="modal-header">
                      <h3 className="modal-title">Forgot Password</h3>
                      <button type="button" className="close btnPos">
                        <i>&times;</i>
                      </button>
                    </article>

                    <article className="modal-body forgot">
                      <article className="row">
                        <article className="col-md-12">
                          <article className="cropArea">
                            <Cropper
                              style={{ height: "50vh", width: "100%" }}
                              aspectRatio={1}
                              zoomable={true}
                              autoCropArea={1}
                              preview=".img-preview"
                              guides={false}
                              src={this.state.cropSrc}
                              ref={cropper => {
                                this.cropper = cropper;
                              }}
                            />
                          </article>
                        </article>
                      </article>
                    </article>
                  </section>
                </section>
              </section>
            </article>
          </article>
        </article>
      </article>
    );
  }
}

export default DocumentNeeded;
