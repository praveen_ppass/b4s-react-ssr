import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { breadCrumObj } from "../../../constants/breadCrum";
import { Helmet } from "react-helmet";
import BreadCrum from "../../components/bread-crum/breadCrum";
import gblFunc from "../../../globals/globalFunctions";
import { ruleRunner } from "../../../validation/ruleRunner";
import { Scholarship } from "../components/pages/scholarship";
import { AddStudent } from "../components/pages/addStudent";
import Loader from "../../common/components/loader";
import { FETCH_MARCHANT_ID_SUCCESS } from "../actions";
import { PayNowPopUP } from "./pages/payNow";

class MySubscriber extends Component {
  constructor(props) {
    super(props);

    this.state = {
      form: { tabName: "StudentList" },
      tab: "mySubscriber",
      isEdit: false,
      selectedFile: null,
      src: null,
      studentId: null,
      userId: null,
      paypop: false,
      studentList: null,
      validations: {
        docFile: null
      },
      studentPaymentConfig: {
        url: "",
        text: "PAY NOW"
      },
      updateUserConfig: {
        fullName: null,
        pic: null,
        userId: null,
        percentage: null
      }
    };

    this.openpaypop = this.openpaypop.bind(this);
    this.closepaypop = this.closepaypop.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    if (!this.props.isAuthenticated) {
      return <Redirect to="/" />;
    }
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case FETCH_MARCHANT_ID_SUCCESS:
        const updateStudentPayment = { ...this.state.studentPaymentConfig };
        const studentData = this.state.studentList;
        updateStudentPayment.url = `https://www.instamojo.com/buddy4study/smp-premium-application/?data_name=${
          studentData.firstName
          } ${studentData.lastName}&data_email=${studentData.email}&data_phone=${
          studentData.mobile
          }&data_Field_4466=${
          nextProps.marchantID.merchantTransactionNumber
          }&data_hidden=data_Field_4466`;

        this.setState({
          paypop: true,
          studentPaymentConfig: updateStudentPayment
        });
        break;
    }
  }

  openpaypop(userid = "", list) {
    const userList = gblFunc.getStoreUserDetails();

    this.setState(
      {
        studentList: { ...list }
      },
      () =>
        this.props.getMerchantId({
          userId: userList.userId,
          marchentObj: {
            amount: 300,
            currency: "inr",
            initiatorUserId: userList.userId,
            paidForUserId: userid,
            paymentPartner: "VLE",
            transactionMode: "D",
            transactionType: "D"
          }
        })
    );
  }

  closepaypop() {
    this.setState({
      paypop: false
    });
  }

  render() {
    const { match } = this.props.history;
    const isUserAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    if (!isUserAuthenticated) {
      return <Redirect to="/" />;
    }

    return (
      <section>
        <BreadCrum
          classes={breadCrumObj["vleMatchedSch"]["bgImage"]}
          listOfBreadCrum={breadCrumObj["vleMatchedSch"]["breadCrum"]}
          title={breadCrumObj["vleMatchedSch"]["title"]}
          hideBanner={breadCrumObj["vleMatchedSch"]["hideBanner"]}
        />
        {this.state.paypop ? (
          <PayNowPopUP
            closepaypop={this.closepaypop}
            paymentConfig={this.state.studentPaymentConfig}
            htmlContent={`Access Matched Scholarships<br/>Receive Timely Alerts<br/>Dedicated Application Support`}
            payment_type="PREMIUM PROFILE"
          />
        ) : null}
        <section className="dashboard">
          <Loader isLoader={this.props.showLoader} />
          <section className="conatiner-fluid graybg">
            <article className="container dashboard-container">
              <article className="row">
                <article className="dashboard-nav-bg">
                  <article className="dashboard-nav-in">
                    <article className="col-md-9 boxPos list">
                      <article className="tab-border">
                        <Scholarship
                          {...this.props}
                          openpaypop={this.openpaypop}
                        />
                        {/* <AddStudent /> */}
                      </article>
                    </article>
                  </article>
                </article>
              </article>
            </article>
          </section>
        </section>
      </section>
    );
  }
}

export default MySubscriber;
