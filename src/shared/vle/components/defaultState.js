export const VleStudentState = {
  educationalInfo: {
    userAcademicInfo: {
      academicClass: { id: null, value: null },
      marksObtained: null,
      passingMonth: null,
      passingYear: null,
      percentage: null,
      presentClass: 1,
      totalMarks: null,
      id: null,
      markingType: "1"
    },
    userInstituteInfo: {
      academicDetailId: null,
      id: null,
      instituteName: ""
    }
  },
  personalInfo: {
    dob: "",
    email: "",
    familyIncome: null,
    firstName: "",
    id: null,
    lastName: "",
    mobile: null,
    portalId: 1,
    userAddress: {
      district: { id: null, value: null },
      id: null,
      state: { id: null, value: null },
      type: null
    },
    userRules: []
  },
  rules: {},
  dropDownRules: {
    religion: {
      ruleId: null,
      ruleTypeId: 4
    },
    quota: {
      ruleId: null,
      ruleTypeId: 6
    },
    gender: {
      ruleId: null,
      ruleTypeId: 5
    },
    disabled: {
      ruleId: null,
      ruleTypeId: 48
    },
    foreign: {
      ruleId: 0,
      ruleTypeId: 46
    },
    subscriber_relation: {
      ruleId: null,
      ruleTypeId: 32
    },
    special: {}
  },
  interest: [],
  districts: [],
  userid: null,
  ON_FIRST_LOAD: true,
  validations: {
    firstName: null,
    mobile: null,
    dob: null,
    gender: null,
    religion: null,
    academicClass: null,
    dob: null,
    email: null,
    quota: null,
    district: null,
    totalMarks: null,
    marksObtained: null,
    state: null,
    instituteName: null,
    passingYear: null,
    subscriber_relation: null,
    familyIncome: null,
    foreign: null,
    interest: null
  },

  serverError: null,
  isFormValid: false
};
