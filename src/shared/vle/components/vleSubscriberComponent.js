import React, { Component, Fragment } from "react";
import { Redirect, Link } from "react-router-dom";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import gblFunc from "../../../globals/globalFunctions";
import { StudentList } from "../components/pages/studentList";
import { AddStudent } from "../components/pages/addStudent";
import Vledemo from "../components/pages/vle-demo-video";
import Vledownloads from "../components/pages/vle-download";
import VlePaymentPlans from "../components/pages/vleplans";
import VleDocuments from "./pages/vledoc";
import { PayNowPopUP } from "./pages/payNow";
import { FETCH_MARCHANT_ID_SUCCESS } from "../actions";
import Loader from "../../common/components/loader";
import { UpdateStudent } from "./pages/updateStudent";
import AlertMessage from "../../common/components/alertMsg";

class MySubscriber extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tabName: "",
      tab: "mySubscriber",
      isDocsCompleted: false,
      isEdit: false,
      path: "",
      msg: "",
      showLoader: false,
      status: "",
      selectedFile: null,
      src: null,
      studentId: null,
      userId: null,
      isClose: true,
      validations: {
        docFile: null
      },
      paypop: false,
      studentList: null,
      studentPaymentConfig: {
        url: "",
        text: "PAY NOW"
      },
      updateUserConfig: {
        fullName: null,
        pic: null,
        userId: null,
        percentage: null
      },
      isTabToggle: false
    };

    this.tabSwitchHandler = this.tabSwitchHandler.bind(this);
    this.onTabChange = this.onTabChange.bind(this);
    this.editDetailHandler = this.editDetailHandler.bind(this);
    this.openpaypop = this.openpaypop.bind(this);
    this.closepaypop = this.closepaypop.bind(this);
    this.studentMatchingSch = this.studentMatchingSch.bind(this);
    this.closePopUp = this.closePopUp.bind(this);
    this.showAlertMessage = this.showAlertMessage.bind(this);
    this.tabToggleHandler = this.tabToggleHandler.bind(this);
  }

  tabToggleHandler() {
    this.setState({
      isTabToggle: !this.state.isTabToggle
    })
  }
  updateWindowDimensions() {
    if (typeof window == 'undefined' && window.screen.width <= 991) {
      this.setState({
        isTabToggle: true
      });
    } else {
      this.setState({
        isTabToggle: false
      });
    }
  }

  componentDidMount() { }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case FETCH_MARCHANT_ID_SUCCESS:
        const studentData = this.state.studentList;
        if (
          this.state.isClose &&
          studentData &&
          Object.keys(studentData).length > 0
        ) {
          const studentPaymentConfig = { ...this.state.studentPaymentConfig };
          studentPaymentConfig.url = `https://www.instamojo.com/buddy4study/smp-premium-profile/?data_name=${
            studentData.firstName
            } ${studentData.lastName}&data_email=${
            studentData.email
            }&data_phone=${studentData.mobile}&data_Field_59909=${
            nextProps.marchantID.merchantTransactionNumber
            }&data_hidden=data_Field_59909`;

          this.setState({
            studentPaymentConfig,
            paypop: true
          });
          break;
        }
    }
  }

  tabSwitchHandler(isEdit) {
    const match = this.props.match;

    switch (match.url) {
      case "/vle/student-list":
        return (
          <StudentList
            {...this.props}
            matchurl={match.url}
            openpaypop={this.openpaypop}
            editDetailHandler={this.editDetailHandler}
          />
        );
      case "/vle/add-student":
        return (
          <AddStudent
            // editDetailHandler={this.editDetailHandler}
            {...this.props}
          />
        );
      case "/vle/edit-student":
        return (
          <UpdateStudent
            editDetailHandler={this.editDetailHandler}
            {...this.props}
          />
        );

      case "/vle/demo":
        return <Vledemo {...this.props} />;

      case "/vle/download":
        return <Vledownloads {...this.props} />;
      case "/vle/upgrade":
        return (
          <StudentList
            openpaypop={this.openpaypop}
            matchurl={match.url}
            editDetailHandler={this.editDetailHandler}
            {...this.props}
          />
        );

      case "/vle/plans":
        return <VlePaymentPlans {...this.props} />;
      case "/vle/document":
        return (
          <VleDocuments
            {...this.props}
            showAlertMessage={this.showAlertMessage}
          />
        );
    }
  }

  openpaypop(userid = "", list) {
    const userList = gblFunc.getStoreUserDetails();

    this.setState(
      {
        studentList: { ...list },
        isClose: true
      },
      () =>
        this.props.getMerchantId({
          userId: userList.userId,
          marchentObj: {
            amount: 100,
            currency: "inr",
            initiatorUserId: userList.userId,
            paidForUserId: userid,
            paymentPartner: "VLE",
            transactionMode: "D",
            transactionType: "D"
          }
        })
    );
  }

  closepaypop() {
    this.setState({
      paypop: false,
      isClose: false
    });
  }

  showAlertMessage() {
    this.setState({
      isDocsCompleted: true,
      msg: "All documents have been uploaded successfully.",
      status: "success-icon",
      showLoader: true
    });
  }

  editDetailHandler(studentId, userId, isEdit) {
    this.setState({ studentId, isEdit }, () =>
      this.props.fetchStudentDetails({
        userId,
        studentId
      })
    );
  }

  onTabChange(path) {
    this.props.history.push(path);
    this.setState({ paypop: false, isEdit: false });
  }

  componentDidUpdate(prevProps, prevState) {
    if (!this.props.isAuthenticated) {
      return <Redirect to="/" />;
    }
  }

  studentMatchingSch(userId) {
    const userConfig = { ...this.state.updateUserConfig };
    userConfig.userId = userId;
    this.setState({
      form: { tabName: "MatchedScholarships" },
      updateUserConfig: userConfig
    });
  }

  closePopUp() {
    this.setState({
      isShowCropPup: false
    });
  }

  close() {
    if (this.state.isDocsCompleted) {
      this.props.history.push("/vle/student-list");
    } else {
      this.setState({
        status: "",
        showLoader: false,
        msg: ""
      });
    }
  }

  render() {
    const { isTabToggle } = this.state;
    const match = this.props.match;

    const stored_list = gblFunc.getStoreUserDetails();
    const path = match.url;
    let breadCrumPath = null;
    const isUserAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    if (!isUserAuthenticated) {
      return <Redirect to="/" />;
    }
    switch (match.url) {
      case "/vle/student-list":
        breadCrumPath = "studentlist";
        break;
      case "/vle/add-student":
        breadCrumPath = "addStudent";
        break;
      case "/vle/edit-student":
        breadCrumPath = "addStudent";
        break;
      case "/vle/download":
        breadCrumPath = "download";
        break;
      case "/vle/demo":
        breadCrumPath = "tutorials";
        break;
      case "/vle/upgrade":
        breadCrumPath = "upgrade";
        break;
      case "/vle/plans":
        breadCrumPath = "vlePaymentPlans";
        break;
      case "/vle/document":
        breadCrumPath = "vleDocuments";
        break;
    }
    return (
      <Fragment>
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <BreadCrum
          classes={breadCrumObj[breadCrumPath]["bgImage"]}
          listOfBreadCrum={breadCrumObj[breadCrumPath]["breadCrum"]}
          hideBanner={breadCrumObj[breadCrumPath]["hideBanner"]}
          title={breadCrumObj[breadCrumPath]["title"]}
        />
        {match.url == "/vle/student-list" || match.url == "/vle/add-student" ? (
          <Loader isLoader={this.props.showLoader} />
        ) : null}

        {this.state.paypop ? (
          <PayNowPopUP
            closepaypop={this.closepaypop}
            paymentConfig={this.state.studentPaymentConfig}
            htmlContent={`Discover the number of matched scholarships for your profile.<br />
            Pay INR <i className="fa fa-inr" aria-hidden="true" />
            100 (excluding 18% GST) to view.`}
            payment_type=""
          />
        ) : null}
        <section className="dashboard newTheme">
          <section className="conatiner-fluid">
            <article className="container dashboard-container">
              <article className="row">
                <article className="dashboard-nav-bg">
                  <article className="dashboard-nav-in">
                    {/* SIDE NAV TABS*/}
                    <article className="col-md-3">
                      <article className="page-nav listNav page-nav-scholarships">
                        <ul className={`listNav listNavdashboardApp ${isTabToggle ? "mobo" : ""}`}>

                          <li
                            className={`student-list ${typeof window !== 'undefined' && location.pathname.includes('student-list') ? "active" : ""}`}
                          >
                            <Link
                              to="/vle/student-list"
                            >Student List</Link>

                          </li>

                          <li
                            className={`student-add ${typeof window !== 'undefined' && location.pathname.includes('add-student') ? "active" : ""}`}
                          >
                            <Link
                              to="/vle/add-student"
                            >Add Student</Link>

                          </li>
                          {stored_list.vleUser === "true" ? (
                            <li
                              className={`student-list ${typeof window !== 'undefined' && location.pathname.includes('download') ? "active" : ""}`}
                            >
                              <Link
                                to="/vle/download"
                              >Download</Link>

                            </li>
                          ) : null}

                          <li
                            className={`demo ${typeof window !== 'undefined' && location.pathname.includes('demo') ? "active" : ""}`}
                          >
                            <Link
                              to="/vle/demo"
                            >Tutorials</Link>

                          </li>
                          <li
                            className={` student-list ${typeof window !== 'undefined' && location.pathname.includes('upgrade') ? "active" : ""}`}
                          >
                            <Link
                              to="/vle/upgrade"
                            >Upgrade</Link>

                          </li>
                          <span className="signSymbol" onClick={this.tabToggleHandler}>
                            <i className="line-one"></i>
                            {!isTabToggle && <i className="line-two"></i>}
                          </span>
                        </ul>
                      </article>
                    </article>
                    <article className="col-md-9 boxPos">
                      <article className="tab-border">
                        {/* {this.state.isEdit ? (
                          <AddStudent
                            {...this.props}
                            isEdit={this.state.isEdit}
                          />
                        ) : ( )*/}
                        {this.tabSwitchHandler(this.state.isEdit)}
                      </article>
                    </article>
                  </article>
                </article>
              </article>
            </article>
          </section>
        </section>
      </Fragment>
    );
  }
}

export default MySubscriber;
