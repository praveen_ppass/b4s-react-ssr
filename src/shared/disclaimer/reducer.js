import {
  FETCH_PRIVACY_POLICY_SUCCEEDED,
  FETCH_PRIVACY_POLICY_REQUESTED,
  FETCH_PRIVACY_POLICY_FAILED
} from "./actions";

const initialState = {
  showLoader: false,
  isError: false,
  errorMessage: ""
};
const privacyConditionReducer = (
  previousState = initialState,
  { type, payload }
) => {
  switch (type) {
    case FETCH_PRIVACY_POLICY_SUCCEEDED:
      return {
        ...previousState,
        payload,
        showLoader: false,
        isError: false,
        errorMessage: ""
      };

    case FETCH_PRIVACY_POLICY_REQUESTED:
      return {
        ...previousState,
        payload: null,
        showLoader: true,
        isError: false,
        errorMessage: ""
      };

    case FETCH_PRIVACY_POLICY_FAILED:
      return {
        ...previousState,
        payload,
        showLoader: false,
        isError: true,
        errorMessage: payload
      };

    default:
      return previousState;
  }
};
export default privacyConditionReducer;
