import React, { Component } from "react";
import { breadCrumObj } from "../../../constants/breadCrum";
import { Helmet } from "react-helmet";
import gblFunc from "../../../globals/globalFunctions";
import BreadCrum from "../../components/bread-crum/breadCrum";
import Loader from "../../common/components/loader";
import { imgBaseUrl } from "../../../constants/constants";
import ServerError from "../../common/components/serverError";

class Disclaimer extends Component {
  render() {
    if (this.props.isError) {
      return (
        <section>
          <section className="gray">
            <section className="tnc topPos">
              <BreadCrum
                classes={breadCrumObj["disclaimer"]["bgImage"]}
                listOfBreadCrum={breadCrumObj["disclaimer"]["breadCrum"]}
                title={breadCrumObj["disclaimer"]["title"]}
              />
              <ServerError errorMessage={this.props.errorMessage} />
            </section>
          </section>
        </section>
      );
    }

    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <section className="gray">
          <section className="tnc topPos">
            <Helmet> </Helmet>
            <BreadCrum
              classes={breadCrumObj["disclaimer"]["bgImage"]}
              listOfBreadCrum={breadCrumObj["disclaimer"]["breadCrum"]}
              title={breadCrumObj["disclaimer"]["title"]}
            />
            <article className="disclaimer">
              <section className="conatiner-fluid graybg">
                <article className="container">
                  <article className="row ">
                    <article className="col-md-4 col-sm-4 col-xs-12 right">
                      <h1>disclaimer</h1>
                      <img
                        src={`${imgBaseUrl}arrowr.png`}
                        alt="arrow"
                        alt="buddy4study-arrow"
                      />
                    </article>
                    <article className="col-md-8 col-sm-8 col-xs-12 left disclaimerWrapper">
                      <article className="row">
                        <h3>Use of Information</h3>
                        <p>
                          The data and information published on this website is
                          for reference purpose only. The detailed information
                          regarding any scholarship can be gathered from the
                          official website of scholarship providers. Buddy4Study
                          is not responsible for any action taken by users of
                          this website based on the information published here.
                        </p>
                      </article>
                      <article className="row">
                        <h3>Validity of Information</h3>
                        <p>
                          The validity of the information published in
                          scholarship postings and other articles may become
                          irrelevant owing to multiple factors including, but
                          not limited to, the passage of time, change in law,
                          change in official data and technical issues. Thus,
                          users are advised to check with official sources
                          before taking any action based on the information
                          available on this website.
                        </p>
                        <p>
                          The external links available for guiding students and
                          making their navigation easier, may become
                          inaccessible owing to the technical issues in external
                          websites. Buddy4Study is in no way responsible for
                          maintenance of the external links available on this
                          website.
                        </p>
                        <p>
                          Also, Buddy4Study welcomes any suggestion feedback on
                          unwanted information that might be available on the
                          website due to technical malfunctioning, human error
                          or external bugs.{" "}
                        </p>
                      </article>
                      <article className="row">
                        <h3>Accuracy of information</h3>
                        <p>
                          Buddy4Study does not guarantee accuracy of the
                          information published here. The data available here
                          aims to spread broad awareness around listed
                          scholarships based either on the official information
                          or historical data available in public domain. Thus,
                          Buddy4Study does not guarantee accuracy of the data in
                          the scholarship items and related articles published
                          on its portal buddy4study.com.
                        </p>
                        <p>
                          We take reasonable steps to ensure that the personal
                          information we process is accurate, complete, and
                          current, but we depend on our users to update or
                          correct their personal information whenever necessary.
                        </p>
                      </article>
                      <article className="row">
                        <h3>
                          Buddy4Study Partnership with Scholarship providers
                        </h3>
                        <p>
                          Buddy4Study is a platform to inform and aware students
                          about the various kind of scholarships available to
                          students. Thus, thousands on scholarships are listed
                          on the website. This does not, in any way, necessarily
                          mean any partnership with scholarship providers. We
                          also don’t attach our credibility to any scholarship
                          provider just because it is listed on our website.
                        </p>
                        <p>
                          We particularly mention our partnership or alliance
                          with a particular scholarship provider in case it is
                          engaged with us for its Scholarship disbursement.
                        </p>
                      </article>
                      <article className="row">
                        <h3>Buddy4Study Liability</h3>
                        <p>
                          Any use of the information and data available at
                          buddy4study.com is at your own risk. The website and
                          its administrators are not liable for any action taken
                          by users or any loss of opportunity to the users based
                          on the information available on this site.
                        </p>
                        <p>
                          Buddy4Study (owners, investors, media partners,
                          scholarship partners, consultants, advertisers,
                          affiliates, employees or any other associated
                          entities, all collectively referred to as associated
                          entities hereafter) shall not be liable to user or any
                          member or any third party should Buddy4Study exercise
                          its right to modify or discontinue any or all of the
                          contents, information, data, software, products,
                          features and services available on this website.
                        </p>
                        <p>
                          In no event shall Buddy4Study and/or its associated
                          entities be liable for any direct or indirect
                          punitive, incidental, special or consequential damages
                          arising out of or in any way connected with the use of
                          this website.
                        </p>
                      </article>
                    </article>
                  </article>
                </article>
              </section>
            </article>
          </section>
        </section>
      </section>
    );
  }
}

export default Disclaimer;
