import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchPrivacyPolicy as fetchPrivacyPolicyAction } from "./actions";
import { Helmet } from "react-helmet";

import BreadCrum from "../components/bread-crum/breadCrum";

const breadCrumObj = [
  {
    url: "/",
    name: "Home"
  },
  {
    url: "#",
    name: "Privacy Policy"
  }
];
export class PrivacyPolicy extends Component {
  componentDidMount() {
    this.props.loadPrivacyPolicy();
  }
  rawMarkUp() {
    let rawMarkup = this.props.privacyPolicyList.privacyConditions;
    return { __html: rawMarkup };
  }
  render() {
    return (
      <section className="tnc">
        <Helmet> </Helmet>
        <BreadCrum
          classes={["bgtnc"]}
          listOfBreadCrum={breadCrumObj}
          title="Privacy Policy"
        />

        <section className="conatiner-fluid graybg">
          <article className="container">
            <article className="row">
              <article
                className="col-md-8 left"
                dangerouslySetInnerHTML={this.rawMarkUp()}
              />
              <article className="col-md-4 right">
                <h1>PRIVACY POLICY</h1>
                <img src={`${imgBaseUrl}arrowr.png`} alt="arrow" />
              </article>
            </article>
          </article>
        </section>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  privacyPolicyList: state
});
const mapDispatchToProps = dispatch => ({
  loadPrivacyPolicy: () => dispatch(fetchPrivacyPolicyAction("pageData"))
});

export default connect(mapStateToProps, mapDispatchToProps)(PrivacyPolicy);
