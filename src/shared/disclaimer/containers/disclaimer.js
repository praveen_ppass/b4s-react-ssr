import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchPrivacyPolicy as fetchPrivacyPolicyAction } from "../actions";
import disclaimer from "../components/disclaimerPage";

const mapStateToProps = ({ disclaimer }) => ({
  /* privacyPolicyList: privacyConditions.payload,
  isError: privacyConditions.isError,
  errorMessage: privacyConditions.errorMessage */
});

const mapDispatchToProps = dispatch => ({
  loadPrivacyPolicy: () => dispatch(fetchPrivacyPolicyAction("pageData"))
});

export default connect(mapStateToProps, mapDispatchToProps)(disclaimer);
