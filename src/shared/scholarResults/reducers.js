import {
  FETCH_RESULT_LOOKUP_REQUESTED,
  FETCH_RESULT_LOOKUP_SUCCEDED,
  FETCH_RESULT_LOOKUP_FAILED,
  FETCH_SCHOLARSHIP_CURRENT_REQUESTED,
  FETCH_SCHOLARSHIP_CURRENT_SUCCEDED,
  FETCH_SCHOLARSHIP_CURRENT_FAILED
} from "./actions";
import {
  FETCH_SCHOLARSHIPS_SUCCEEDED,
  FETCH_SCHOLARSHIPS_FAILED,
  FETCH_SCHOLARSHIPS_REQUESTED
} from "../scholarship/actions";

const defaultState = {
  schResultLookUp: null,
  showLoader: true,
  isError: false,
  errorMessage: "",
  currentSch: []
};

const resultsLookReducers = (state = defaultState, { type, payload }) => {
  switch (type) {
    case FETCH_RESULT_LOOKUP_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        isError: false
      };
    case FETCH_RESULT_LOOKUP_SUCCEDED:
      return {
        ...state,
        type,
        schResultLookUp: payload,
        showLoader: false,
        isError: false
      };
    case FETCH_RESULT_LOOKUP_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        isError: true,
        errorMessage: payload
      };
    case FETCH_SCHOLARSHIP_CURRENT_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        isError: false
      };
    case FETCH_SCHOLARSHIP_CURRENT_SUCCEDED:
      return {
        ...state,
        type,
        currentSch: payload,
        showLoader: false,
        isError: false
      };
    case FETCH_SCHOLARSHIP_CURRENT_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        isError: true,
        errorMessage: payload
      };

    case FETCH_SCHOLARSHIPS_REQUESTED:
      return Object.assign({}, state, {
        scholarshipList: null,
        showLoader: true,
        type
      });

    case FETCH_SCHOLARSHIPS_SUCCEEDED:
      const { total, scholarships } = payload;
      let scholarshipList = scholarships;

      let newState = {
        scholarshipList,
        showLoader: false,
        type
      };

      newState.numScholarships = total;

      return Object.assign({}, state, newState);

    case FETCH_SCHOLARSHIPS_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        isError: true,
        errorMessage: payload
      };
    default:
      return state;
  }
};

export default resultsLookReducers;
