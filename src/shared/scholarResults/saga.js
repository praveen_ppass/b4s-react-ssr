import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_RESULT_LOOKUP_REQUESTED,
  FETCH_RESULT_LOOKUP_SUCCEDED,
  FETCH_RESULT_LOOKUP_FAILED,
  FETCH_SCHOLARSHIP_CURRENT_REQUESTED,
  FETCH_SCHOLARSHIP_CURRENT_SUCCEDED,
  FETCH_SCHOLARSHIP_CURRENT_FAILED
} from "./actions";

const fetchResultLookUpApi = payload =>
  fetchClient.post(apiUrl.resultLookUpUrl, payload).then(res => res.data);

function* fetchResultLookup(input) {
  try {
    const resultLookUp = yield call(fetchResultLookUpApi, input.payload);
    yield put({
      type: FETCH_RESULT_LOOKUP_SUCCEDED,
      payload: resultLookUp
    });
  } catch (err) {
    yield put({
      type: FETCH_RESULT_LOOKUP_FAILED,
      payload: err
    });
  }
}

const fetchCurrentSchApi = payload =>
  fetchClient.get(apiUrl.currentSchLookUp).then(res => res.data);

function* fetchCurrentSch(input) {
  try {
    const currentScholarship = yield call(fetchCurrentSchApi, input.payload);
    yield put({
      type: FETCH_SCHOLARSHIP_CURRENT_SUCCEDED,
      payload: currentScholarship
    });
  } catch (err) {
    yield put({
      type: FETCH_SCHOLARSHIP_CURRENT_FAILED,
      payload: err
    });
  }
}

export default function* resultLookUpSaga() {
  yield takeEvery(FETCH_RESULT_LOOKUP_REQUESTED, fetchResultLookup);
  yield takeEvery(FETCH_SCHOLARSHIP_CURRENT_REQUESTED, fetchCurrentSch);
}
