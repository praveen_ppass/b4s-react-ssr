import React, { Component } from "react";
import { Link } from "react-router-dom";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import Loader from "../../common/components/loader";
import DatePicker from "react-datepicker";
import moment from "moment";
import { required, isEmail } from "../../../validation/rules";

import { ruleRunner } from "../../../validation/ruleRunner";
import {
  FETCH_SCHOLARSHIP_CURRENT_SUCCEDED,
  FETCH_RESULT_LOOKUP_SUCCEDED,
  FETCH_RESULT_LOOKUP_FAILED
} from "../actions";
import Select from "react-select";
import { FETCH_SCHOLARSHIPS_SUCCEEDED } from "../../scholarship/actions";
import AlertMessage from "../../common/components/alertMsg";
import { messages } from "../../../constants/constants";

if (typeof window !== "undefined") {
  require("react-datepicker/dist/react-datepicker.css");
}

class ScholarResults extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {},
      validations: {
        scholarshipId: null,
        email: null,
        dob: null
      },
      selectedOption: null,
      options: [],
      schList: [],
      checkResult: null,
      statusAlert: false,
      isShowAlert: false,
      alertMsg: "",
      showResult: "",
      scholarshipName: ""
    };

    this.formChangeHandler = this.formChangeHandler.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this.onSelectHandler = this.onSelectHandler.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.checkSchValidation = this.checkSchValidation.bind(this);
  }

  componentDidMount() {
    this.props.getCurrentSch("currentSch");
    this.props.loadScholarships({ page: 0, length: 10, mode: "OPEN" });
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    switch (type) {
      case FETCH_SCHOLARSHIP_CURRENT_SUCCEDED:
        if (nextProps && nextProps.currentSch && nextProps.currentSch.length) {
          const { currentSch } = nextProps;
          const dropdownSelectOptn = currentSch.map(sch => {
            return {
              label: sch.title,
              id: sch.scholarshipId,
              slug: sch.slug
            };
          });
          this.setState({
            options: dropdownSelectOptn
          });
        }
        break;

      case FETCH_SCHOLARSHIPS_SUCCEEDED:
        if (nextProps && nextProps.scholarshipList.length) {
          const { scholarshipList } = nextProps;
          this.setState({
            schList: scholarshipList
          });
        }
        break;
      case FETCH_RESULT_LOOKUP_SUCCEDED:
        const { resultLookUp } = nextProps;
        const { selectedOption } = this.state;
        if (
          resultLookUp &&
          (resultLookUp.status === "CONGRATULATIONS" ||
            resultLookUp.status === "SORRY" ||
            resultLookUp.status === "NOT-AVAILABLE")
        ) {
          this.setState({
            checkResult: true,
            showResult: resultLookUp.status,
            scholarshipName: selectedOption.label,
            selectedOption: null,
            formData: {}
          });
        } else {
          this.setState({
            checkResult: true,
            scholarshipName: "",
            showResult: resultLookUp.status,
            selectedOption: null,
            formData: {}
          });
        }

        break;
      case FETCH_RESULT_LOOKUP_FAILED:
        const { error } = nextProps;
        if (error.response && error.response.data.errorCode !== 801) {
          this.setState({
            isShowAlert: true,
            alertMsg: errorMsg,
            status: false,
            formData: {},
            selectedOption: null
          });
        } else {
          this.setState({
            showResult: "ENTRY-NOT-FOUND",
            checkResult: true,
            selectedOption: null,
            formData: {},
            scholarshipName: null
          });
        }

        break;
    }
  }

  formChangeHandler(event, id = undefined) {
    const updateFormData = { ...this.state.formData };
    let value = "";

    if (id === "dob") {
      value = moment(event).format("YYYY-MM-DD");
      updateFormData[id] = value;
    } else {
      updateFormData[id] = event.target.value;
    }

    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      updateFormData[id],
      id,
      name,
      ...validationFunctions
    );
    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    this.setState({
      formData: updateFormData,
      validations
    });
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };

    let isFormValid = true;

    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);

      let validationResult = ruleRunner(
        this.state.formData[key],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  checkSchValidation() {
    let validations = { ...this.state.validations };

    let { name, validationFunctions } = this.getValidationRulesObject(
      "scholarshipId"
    );

    let validationResult = ruleRunner(
      this.state.formData["scholarshipId"],
      "scholarshipId",
      name,
      ...validationFunctions
    );
    validations["scholarshipId"] = validationResult["scholarshipId"];

    this.setState({
      validations
    });
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "scholarshipId":
        validationObject.name = "* Scholarship name";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "dob":
        validationObject.name = "* Date of birth";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "email":
        validationObject.name = "* Email ";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;
    }
  }

  onSubmitHandler(event) {
    event.preventDefault();
    if (this.checkFormValidations()) {
      const { formData } = this.state;
      this.props.saveResultLookUp(formData);
    }
  }

  onSelectHandler(selectedOption) {
    this.setState(
      {
        selectedOption,
        formData: {
          ...this.state.formData,
          scholarshipId:
            selectedOption && Object.keys(selectedOption).length
              ? selectedOption.id
              : ""
        }
      },
      () => this.checkSchValidation()
    );
  }

  hideAlert() {
    this.setState({
      alertMsg: "",
      isShowAlert: false
    });
  }

  render() {
    const {
      formData,
      validations,
      selectedOption,
      options,
      schList,
      showResult,
      scholarshipName
    } = this.state;

    return (
      <section className="result">
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          isShow={this.state.isShowAlert}
          msg={this.state.alertMsg}
          close={this.hideAlert}
          status={this.state.statusAlert}
        />
        <BreadCrum
          classes={breadCrumObj["results"].bgImage}
          listOfBreadCrum={breadCrumObj["results"].breadCrum}
          subTitle={breadCrumObj["results"].subTitle}
          title={breadCrumObj["results"].title}
        />
        <section className="results">
          <section className="container-fluid resultWrapper equial-padding">
            <section className="container">
              {this.state.checkResult ? (
                <article className="row">
                  <h2>Scholarship Results</h2>
                  <article className=" col-md-12 msgWrapper">
                    {showResult && showResult === "CONGRATULATIONS" ? (
                      <article>
                        <h5 className="cong">
                          <i className="fa fa-trophy" /> Congratulations
                        </h5>
                        <p>
                          Congratulations! You have been selected for the
                          <b>
                            {" "}
                            {this.state.scholarshipName
                              ? this.state.scholarshipName
                              : ""}
                          </b>. Buddy4Study and
                          <b>
                            {" "}
                            {this.state.scholarshipName
                              ? this.state.scholarshipName
                              : ""}
                          </b>{" "}
                          team will get in touch with you soon with more
                          details! Meanwhile, you can explore more scholarship
                          opportunities available{" "}
                          <Link to="/scholarships"> HERE </Link>.
                        </p>
                      </article>
                    ) : (
                        ""
                      )}

                    {showResult && showResult === "ENTRY-NOT-FOUND" ? (
                      <article>
                        <h5 className="sorry">
                          <i className="fa fa-frown-o" /> Entry not Found
                        </h5>
                        <p>
                          We’re sorry! the details you’ve entered do not match
                          any application in our database. Kindly re-enter the
                          correct details and submit request again. Please
                          contact
                          <a href="mailto:info@buddy4study.com">
                            {" "}
                            info@buddy4study.com
                          </a>{" "}
                          if your results are still not found. Good Luck!
                        </p>
                      </article>
                    ) : null}

                    {showResult && showResult === "NOT-AVAILABLE" ? (
                      <article>
                        <h5 className="not">
                          <i className="fa fa-meh-o" /> Results to be Announced
                        </h5>
                        <p>
                          Thank you for applying to the{" "}
                          <b>
                            {" "}
                            {this.state.scholarshipName
                              ? this.state.scholarshipName
                              : ""}
                          </b>{" "}
                          . Your application is under review process by the{" "}
                          <b>
                            {" "}
                            {this.state.scholarshipName
                              ? this.state.scholarshipName
                              : ""}
                          </b>{" "}
                          selection panel. You can visit this page later to
                          check result status. Meanwhile you can explore more
                          scholarship opportunities available{" "}
                          <Link to="/scholarships"> HERE </Link>.
                        </p>
                      </article>
                    ) : null}
                    {showResult && showResult === "SORRY" ? (
                      <article>
                        <h5 className="sorry">
                          {" "}
                          <i className="fa fa-frown-o" /> Sorry!
                        </h5>

                        <p>
                          We regret to inform you that after careful review,
                          your application did not make it to the final
                          selection list. We appreciate your interest in taking
                          an important step by applying for{" "}
                          <b>{this.state.scholarshipName} </b>. We have other
                          scholarships that will match your profile. Continue to
                          explore for such scholarships
                          <Link to="/scholarships"> HERE </Link>.
                        </p>
                      </article>
                    ) : null}
                  </article>
                </article>
              ) : null}

              <article className="row">
                <article className="col-md-6 col-sm-6 vdivider">
                  <form onSubmit={this.onSubmitHandler} autoCapitalize="off">
                    <article className="form-group sel">
                      <label>Applied Scholarships</label>

                      <Select
                        value={selectedOption}
                        onChange={this.onSelectHandler}
                        options={options}
                        isSearchable={true}
                        isClearable={true}
                        placeholder=""
                      />
                      {validations.scholarshipId ? (
                        <span className="error animated bounce">
                          {validations.scholarshipId}
                        </span>
                      ) : null}
                    </article>

                    <article className="form-group">
                      <label htmlFor="email">Registered Email</label>
                      <input
                        type="text"
                        id="email"
                        name="email"
                        className="form-control"
                        placeholder="abc@xyz.com"
                        value={formData && formData.email ? formData.email : ""}
                        onChange={event =>
                          this.formChangeHandler(event, "email")
                        }
                      />

                      {validations.email ? (
                        <span className="error animated bounce">
                          {validations.email}
                        </span>
                      ) : null}
                    </article>
                    <article className="form-group date">
                      <label htmlFor="email">DOB</label>

                      <DatePicker
                        showYearDropdown
                        scrollableYearDropdown
                        readOnly
                        yearDropdownItemNumber={40}
                        minDate={moment().subtract(100, "years")}
                        maxDate={moment().add(1, "years")}
                        name="dob"
                        id="dob"
                        className="icon-date"
                        autoComplete="off"
                        selected={
                          this.state.formData.dob
                            ? moment(
                              moment(this.state.formData.dob).format(
                                "DD-MM-YYYY"
                              ),
                              "DD-MM-YYYY"
                            )
                            : null
                        }
                        onChange={event => this.formChangeHandler(event, "dob")}
                        dateFormat="DD-MM-YYYY"
                      />
                      {validations.dob ? (
                        <span className="error animated bounce">
                          {validations.dob}
                        </span>
                      ) : null}
                    </article>
                    <article className="pull-right">
                      <input type="submit" className="btn" />
                    </article>
                  </form>
                </article>
                <article className="col-md-6 col-sm-6">
                  <article className="list">
                    <h4>Closing Soon Scholarship, Apply Now</h4>
                    <article className="scrollbar scrollbarColor">
                      {schList.map(list => (
                        <p key={list.id}>
                          <a
                            href={`/scholarship/${list.slug}`}
                            target="_blank"
                            rel="noopener nofollow"
                          >
                            {list.scholarshipName}
                          </a>
                        </p>
                      ))}
                    </article>
                  </article>
                </article>
              </article>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default ScholarResults;
