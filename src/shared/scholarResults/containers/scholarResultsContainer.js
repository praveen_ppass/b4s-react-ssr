import { connect } from "react-redux";

import ScholarResults from "../components/results";
import {
  fetchCurrentScholarship as fetchCurrentScholarshipAction,
  fetchResultLookUp as fetchResultLookUpAction
} from "../actions";
import { fetchScholarships as fetchScholarshipsAction } from "../../scholarship/actions";

const mapStateToProps = ({ resultLookUp }) => ({
  currentSch: resultLookUp.currentSch,
  scholarshipList: resultLookUp.scholarshipList,
  type: resultLookUp.type,
  showLoader: resultLookUp.showLoader,
  error: resultLookUp.errorMessage,
  resultLookUp: resultLookUp.schResultLookUp
});
const mapDispatchToProps = dispatch => ({
  getCurrentSch: data => dispatch(fetchCurrentScholarshipAction(data)),
  loadScholarships: inputData => dispatch(fetchScholarshipsAction(inputData)),
  saveResultLookUp: inputData => dispatch(fetchResultLookUpAction(inputData))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScholarResults);
