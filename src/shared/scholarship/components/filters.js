import React, { Component } from "react";
import { object } from "prop-types";

class ScholarshipFiltersNew extends Component {
  constructor(props) {
    super(props);
    this.selectfilter = this.selectfilter.bind(this);
    this.selectfilter2 = this.selectfilter2.bind(this);
    this.onChangeOtherButton = this.onChangeOtherButton.bind(this);

    this.state = {
      filterOption: "",
      isChecked: true,
      checkedFilterList: [],
      selectedFiltersValues: [],
      filterOption2: "",
      otherButton: false,
      activeClasss: ""
    };
  }

  async selectfilter(type, index) {
    var optionList = this.props.scholarshipFilters.filter(function(el) {
      return el.TYPE === type;
    });

    var userNames = this.props.filterState[type];
    this.props.resetSelected();

    await this.setState({
      filterOption: optionList,
      checkedFilterList: userNames,
      filterOption2: "",
      activeClasss: type
    });
  }

  async selectfilter2(id) {
    let { countryList } = this.props;

    let array = countryList;
    this.props.resetSelected();
    await this.setState({
      filterOption2: array.slice(0, 6),
      filterOption: "",
      otherButton: false,
      activeClasss: "Country"
    });
  }

  componentDidMount() {
    this.selectfilter("Class");
  }

  async onChangeOtherButton() {
    let { countryList } = this.props;

    await this.setState({
      filterOption2: countryList,
      filterOption: "",
      otherButton: true
    });
  }

  render() {
    let {
      numScholarships,
      scholarshipFilters,
      onFilterChange,
      selected,
      clearSelected,
      filterState,
      resetFilters,
      applyFilters,
      resetAndApplyFilters,
      title,
      areFiltersPulledDown,
      filterList,
      countryList,
      countryListRules
    } = this.props;
    let selectedValues = [];
    let mappedSelectedValues = [];
    for (let key in filterState) {
      if (filterState[key].length > 0) {
        filterState[key].map(selectedFilter => {
          mappedSelectedValues.push({
            label: selectedFilter.label,
            value: selectedFilter.value,
            TYPE: selectedFilter.TYPE,
            checked: selectedFilter.checked
          });
        });

        selectedValues = mappedSelectedValues;
      }
    }
    if (!!selectedValues) {
      var data = selectedValues.map(item => {
        return item.value;
      });
    }
    return (
      <section>
        <section className="wrapperfullsearch">
          <section className="col-md-12 col-sm-12 isMobileshow filterMobileWrapper open">
            <article className="row flitterHeader">
              <article className="col-md-6">
                <article className="keyword">
                  {/* <span>Male</span>
                  <span>Class 1</span>
                  <span>Clear All</span> */}
                  <SelectedFilters
                    selected={filterState}
                    clearAll={resetAndApplyFilters}
                    clearSelected={clearSelected}
                    countryListRules={countryListRules}
                  />
                </article>
              </article>
              <article className="col-md-6">
                <button
                  type="button"
                  className="close"
                  onClick={this.props.resetAndApplyFiltersClose}
                >
                  <i>&times;</i>
                </button>
              </article>
            </article>
            <article className="row flitterBody">
              <article className="col-md-4 col-sm-4">
                <article className="filterBox">
                  <ul>
                    {scholarshipFilters.map((item, index) => {
                      return (
                        
                        <li
                          id={index}
                          className={
                            item.TYPE === this.state.activeClasss
                              ? "active"
                              : "inactive-class"
                          }
                          onClick={() => this.selectfilter(item.TYPE)}
                        >
                          {item.TYPE}
                          <i className={(data).some(r=>(item.options.map(item1=> item1.value).indexOf(r)>=0)) ?"bullet fa fa-circle":""} />
                        </li>
                      );
                    })}
                    <li
                      className={
                        "Country" === this.state.activeClasss
                          ? "active"
                          : "inactive-class"
                      }
                      onClick={id => this.selectfilter2(id)}
                    >
                      Country <i className={!!countryListRules && countryListRules.length>0?"bullet fa fa-circle":""} />
                    </li>
                  </ul>
                </article>
              </article>
              <article className="col-md-8 col-sm-8">
                <article className="second scrollbarColor">
                  <article className="ctrl-wrapper ">
                    <article className="form-group interest">
                      {!!this.state.filterOption
                        ? this.state.filterOption[0].options.map(
                            (options, index) => {
                              return (
                                
                                <label id={options.value}>
                                  {options.label}
                                  <input
                                    type="checkbox"
                                    id={options.value}
                                    name={options.label}
                                    value={options.value}
                                    // checked={Array.isArray((selectedValues.filter(obj=>obj.value==options.value))) && (selectedValues.filter(obj=>obj.value==options.value)).length}
                                    checked={data.includes(options.value)}
                                    onChange={e =>
                                      this.props.onFilterChange(
                                        e,
                                        this.state.filterOption[0].TYPE,
                                        this.state.isChecked
                                      )
                                    }
                                  />
                                  <dd className="chkbox"></dd>
                                </label>
                              );
                            }
                          )
                        : ""}
                      {!!this.state.filterOption2
                        ? this.state.filterOption2.map((options, index) => {
                            return (
                              <label id={options.id}>
                                {options.shortName}
                                <input
                                  type="checkbox"
                                  id={options.id}
                                  name={options.shortName}
                                  value={options.shortName}
                                  // checked={Array.isArray((selectedValues.filter(obj=>obj.value==options.value))) && (selectedValues.filter(obj=>obj.value==options.value)).length}
                                  // checked={data.includes(options.value)}
                                  checked={countryListRules.includes(
                                    options.id
                                  )}
                                  onChange={e =>
                                    this.props.onFilterChange2(
                                      e,
                                      this.state.isChecked
                                    )
                                  }
                                />
                                <dd className="chkbox"></dd>
                              </label>
                            );
                          })
                        : ""}
                      {!!this.state.filterOption2 && !this.state.otherButton ? (
                        <button
                          className="others"
                          onClick={this.onChangeOtherButton}
                        >
                          Other
                        </button>
                      ) : (
                        ""
                      )}
                    </article>
                  </article>
                </article>
              </article>
              <article className="flitterFooter">
                <article className="col-md-12">
                  <article className="wrapperBtn">
                    <span onClick={applyFilters} className="apply-but">
                      Apply
                    </span>
                  </article>
                </article>
              </article>
            </article>
          </section>
        </section>
      </section>
    );
  }
}

const SelectedFilters = props => {
  let { selected, clearAll, clearSelected, countryListRules } = props;

  let selectedValues = [];
  let mappedSelectedValues = [];
  for (let key in selected) {
    if (selected[key].length > 0) {
      selected[key].map(selectedFilter => {
        mappedSelectedValues.push({
          label: selectedFilter.label,
          value: selectedFilter.value,
          TYPE: selectedFilter.TYPE
        });
      });

      selectedValues = mappedSelectedValues;
    }
  }
  if (selectedValues.length > 0 || countryListRules.length > 0) {
    return (
      <article className="keyword">
        {/* {selectedValues.map(item => (
          <span
            key={item.value}
            onClick={() => clearSelected(item.value, item.TYPE)}
          >
            {item.label}
          </span>
        ))} */}
        <span onClick={clearAll}>Clear All</span>
      </article>
    );
  } else {
    return <article className="keyword" />;
  }
};

export default ScholarshipFiltersNew;
