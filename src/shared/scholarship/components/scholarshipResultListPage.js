import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Helmet from "react-helmet";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import Loader from "../../common/components/loader";
import gblFunc from "../../../globals/globalFunctions";
import { defaultImageLogo } from "../../../constants/constants";
import ServerError from "../../common/components/serverError";
import { imgBaseUrl } from "../../../constants/constants";
class ScholarshipResultListPage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.loadAwardees();
  }

  showResult() {
    if (this.props.awardees != null) {
      return this.props.awardees.map((item, index) => {
        return (
          <article className="col-lg-3 col-xs-12 col-sm-6 col-md-3 text-center box">
            <Link to={"/scholarship-result/" + item.slug}>
              <article className="resultBox">
                <article className="imgWrapper">
                  <article className="imginnerWrapper">
                    <img
                      className="img-responsive"
                      src={item.logo ? item.logo : defaultImageLogo}
                      onError={e => {
                        e.target.src = defaultImageLogo;
                      }}
                      alt={item.title}
                    />
                  </article>
                </article>
                <h2
                  dangerouslySetInnerHTML={{
                    __html: gblFunc.replaceWithLoreal(item.title)
                  }}
                />
                <p>
                  Publish Date1: {item.publishDate}
                  <br /> Selected Candidates: {item.scholars}
                </p>
              </article>
            </Link>
          </article>
        );
      });
    }
  }

  componentWillMount() {
    if (breadCrumObj["scholar_result"]["breadCrum"].length == 2) {
      breadCrumObj["scholar_result"]["breadCrum"].pop();
    }
  }

  render() {
    if (this.props.isError) {
      return (
        <section>
          <Helmet> </Helmet>
          <BreadCrum
            classes={breadCrumObj["scholar_result"]["bgImage"]}
            listOfBreadCrum={breadCrumObj["scholar_result"]["breadCrum"]}
            title={breadCrumObj["scholar_result"]["title"]}
            hideBanner={breadCrumObj["scholar_result"]["hideBanner"]}
            subTitle={breadCrumObj["scholar_result"]["subTitle"]}
          />
          <ServerError errorMessage={this.props.errorMessage} />
        </section>
      );
    }

    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <section>
          <section>
            <Helmet> </Helmet>
            <BreadCrum
              classes={breadCrumObj["scholar_result"]["bgImage"]}
              listOfBreadCrum={breadCrumObj["scholar_result"]["breadCrum"]}
              title={breadCrumObj["scholar_result"]["title"]}
              hideBanner={breadCrumObj["scholar_result"]["hideBanner"]}
              subTitle={breadCrumObj["scholar_result"]["subTitle"]}
            />
            <section className="container-fluid scholar-result">
              <section className="container equial-padding scholar-result-container">
                <section className="row">
                  <article className="text-center buddy-heading">
                    <h1 className="title">
                      Recent results for the scholarships
                      <article className="announce">
                        To Check Latest Scholarship Results
                      </article>
                    </h1>
                  </article>
                </section>
                <section className="row listItem">{this.showResult()}</section>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default ScholarshipResultListPage;
