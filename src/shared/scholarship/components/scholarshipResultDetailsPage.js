import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Helmet from "react-helmet";
import Pagination from "rc-pagination";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import Slider from "react-slick";
import gblFunc from "../../../globals/globalFunctions";
import { imgError } from "../../../constants/constants";
import Loader from "../../common/components/loader";
import {
  required,
  isEmail,
  minLength,
  isEmailMobile
} from "../../../validation/rules";
import { ruleRunner } from "../../../validation/ruleRunner";
import MaskedInput from "react-text-mask";
import Autosuggest from "react-autosuggest";
//import ScholarshipResultSearchFilter from "./scholarshipResultSearchFilter";

class ScholarshipResultDetailsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ITEMS_PER_PAGE: 20,
      currentPage: 1,
      dobSearch: "",
      emailMobileSearch: null,
      emailSearch: "",
      mobileSearch: "",
      isFormValid: false,
			awardeesSuccess: null,
      validations: {
        dobSearch: null,
        emailMobileSearch: null
      }
    };

    this.onPageChange = this.onPageChange.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleScholarshipAwardeesFilter = this.handleScholarshipAwardeesFilter.bind(
      this
    );
    this.onBlurChangeMethod = this.onBlurChangeMethod.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
		this.handlePageReset = this.handlePageReset.bind(this);
  }

  componentDidMount() {
    let slug = this.props.match.params.slug;
    this.setState({ slug: slug }, () =>
      this.props.awardeesDetails({ slug, page: 0, size: 20 })
    );
    this.props.loadAwardees();
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;

    if (type == "FETCH_AWARDEES_DETAILS_SCHOLARSHIPS_SUCCEEDED") {
      this.setState({
        awardeesDetailsResult: nextProps.awardeesDetailsResult,
        awardees:
          nextProps.awardeesDetailsResult &&
          nextProps.awardeesDetailsResult.awardees,
        scholarshipTitle:
          nextProps.awardeesDetailsResult &&
          nextProps.awardeesDetailsResult.title
      });
    }

    if (type == "FETCH_AWARDEES_FILTER_SUCCEEDED") {	
      if ( 
        !!nextProps.awardeesFiltered && 
        !!nextProps.awardeesFiltered.awardees &&
        nextProps.awardeesFiltered.awardees.length > 0
      ) {
					this.setState({
          awardeesDetailsResult: null,
          awardees: nextProps.awardeesFiltered.awardees,
          awardeesSuccess: true
        });
      } else {
        this.setState({
          awardeesDetailsResult: null,
          awardees: [],
          awardeesSuccess: false
        });
      }  
    }
    if (type == "FETCH_AWARDEES_FILTER_FAILED") {
      this.setState({
        mobileError: "Please Enter a valid mobile number"
      });
    } else {
      this.setState({
        mobileError: ""
      });
    }
  }

  handleScholarshipAwardeesFilter(e) {
    e.preventDefault();
    const { slug, emailMobileSearch, dobSearch } = this.state;

    let mobileSearch;
    let emailSearch;
    if (emailMobileSearch && emailMobileSearch.includes("@")) {
      emailSearch = emailMobileSearch;
    } else {
      mobileSearch = emailMobileSearch;
    }

    if (this.checkFormValidations()) {
      this.props.awardeesFilter(
        {
          slug,
          filter: {
            dob: dobSearch,
            mobile: mobileSearch,
            email: emailSearch
          }
        },
        () => {
          this.setState({
            ...this.state,
            emailMobileSearch: ""
            // validations: {
            //   nameSearch: null
            // }
          });
        }
      );
    }
  }

	handlePageReset(){
		let slug = this.props.match.params.slug;
    this.setState({ 
			slug: slug,
			dobSearch: "",
			mobileSearch: "",
			emailSearch: "",
			emailMobileSearch: "",
			awardeesSuccess: null,
			currentPage: 1,
		}, () =>
      this.props.awardeesDetails({ slug, page: 0, size: 20 })
    );
	}

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "emailMobileSearch":
        validationObject.name = "*Email address / Mobile";
        validationObject.validationFunctions = [required, isEmailMobile];
        return validationObject;

      case "dobSearch":
        validationObject.name = "*Date of Birth ";
        validationObject.validationFunctions = [required];
        return validationObject;
    }
  }

  async onBlurChangeMethod(e) {
    const { id, value } = e.target;
    var date = value
      .split("/")
      .reverse()
      .join("-");
    var updatedate1 = date;
    if (
      /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/.test(value)
    ) {
      await this.setState({
        dobSearch: updatedate1 // return new object jasper object
      });
    } else {
      this.setState({
        dobSearch: "" // return new object jasper object
      });
    }
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );

    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    this.setState({
      validations
    });
  }

  async handleKeyPress(e) {
    await e.target.blur();
    if (this.state.dobSearch) {
      this.handleScholarshipAwardeesFilter(e);
    }
  }

  handleChange(event) {
    const { id, value } = event.target;
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );

    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    this.setState({
      [id]: value,
      validations
    });

    if (this.state.emailMobileSearch)
      this.setState({
        mobileError: ""
      });
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );

      validations[key] = validationResult[key];

      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }

    this.setState({
      validations,
      isFormValid
    });

    return isFormValid;
  }

  getDate() {
    if (this.props.awardeesDetailsResult != null) {
      return this.props.awardeesDetailsResult.publishDate;
    }
  }

  getTitle() {
    if (this.props.awardeesDetailsResult != null) {
      return gblFunc.replaceWithLoreal(this.props.awardeesDetailsResult.title);
    }
  }

  getCondidate() {
    if (this.props.awardeesDetailsResult != null) {
      return this.props.awardeesDetailsResult.count;
    }
  }

  getSlider() {
    if (this.props.awardeeslisting != null) {
      return this.props.awardeeslisting.map((item, index) => {
        return (
          <article className="col-lg-3 col-xs-12 col-sm-6 col-md-3 text-center">
            <Link to={"/scholarship-result/" + item.slug}>
              <article className="resultBox">
                <article className="imgWrapper">
                  <article className="imginnerWrapper">
                    <img
                      className="img-responsive"
                      src={item.logo}
                      alt="buddy4study-awardee"
                    />
                  </article>
                </article>
                <h2
                  dangerouslySetInnerHTML={{
                    __html: gblFunc.replaceWithLoreal(item.title)
                  }}
                />
                <p>
                  Publish Date: {item.publishDate}
                  <br /> Selected Candidates: {item.scholars}
                </p>
              </article>
            </Link>
          </article>
        );
      });
    }
  }
  getOffset(page) {
    return page - 1;
  }

  onPageChange(currentPage) {
    const { ITEMS_PER_PAGE } = this.state;
    const { awardeesDetails } = this.props;

    let page = this.getOffset(currentPage);

    const size = ITEMS_PER_PAGE;

    let params = {
      page,
      size,
      slug: this.props.match.params.slug
    };

    awardeesDetails(params);

    this.setState({
      currentPage
    });
  }

  // getAwardees() {
  //   if (this.props.awardeesDetailsResult != null) {
  //     if (this.props.awardeesDetailsResult.awardees.length > 0) {
  //       console.log(this.props.awardeesDetailsResult.awardees)
  //       const defaultProfilePic =
  //         "https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/profile-img.png";
  //       return this.props.awardeesDetailsResult.awardees.map((item, index) => {
  //         return (
  //           <section>
  //             <section className="awardees-cell">
  //               <article className="media">
  //                 <article className="media-body">
  //                   <img
  //                     src={item.pic || defaultProfilePic}
  //                     alt={item.firstName + " " + item.lastName}
  //                     onError={e => imgError(e)}
  //                   />
  //                   <h3>
  //                     {item.firstName} {item.lastName}
  //                   </h3>
  //                   <p>{item.dob}</p>
  //                   {/*  <p>Schindler Igniting Minds Scholarship Program 2018</p> */}
  //                 </article>
  //               </article>
  //             </section>
  //           </section>
  //         );
  //       });
  //     }
  //   }
  // }

  getAwardeesTable() {
    if (!!this.state.awardees) {
      if (this.state.awardees.length > 0) {
        return (
          <section className="scholar-resultNew">
            <table role="table" className="table table-hover table-striped">
              <thead role="rowgroup">
                <tr role="row">
                  <th role="columnheader">Application No.</th>
                  <th role="columnheader">Name</th>
                  <th role="columnheader">Date of Birth</th>
                  <th role="columnheader">Gender</th>
                  <th role="columnheader">State</th>
                  <th role="columnheader">Mobile Number</th>
                </tr>
              </thead>
              <tbody role="rowgroup">
                {this.state.awardees.map((item, index) => {
                  const dob = item.dob ? item.dob : "N/A";
                  return (
                    <tr role="row">
                      <td role="cell">{item.applicationNumber}</td>
                      <td role="cell">
                        {item.firstName + " " + item.lastName}
                      </td>
                      <td role="cell">{dob}</td>
                      <td role="cell">{item.gender}</td>
                      <td role="cell">{item.state}</td>
                      <td role="cell">{item.mobile}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </section>
        );
      }
    }
  }

  render() {
    const { awardeesDetailsResult } = this.props;

    var updatedate1 = "";

    if (!!this.state.dobSearch) {
      let date = this.state.dobSearch
        .split("-")
        .reverse()
        .join("/");
      updatedate1 = date;
    }

    const settings = {
      centerMode: true,
      infinite: true,
      autoplay: true,
      centerPadding: "10px",
      slidesToShow: 3,
      speed: 500,
      speed: 2000,
      autoplaySpeed: 4000,

      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 853,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true
          }
        },
        {
          breakpoint: 603,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    const { match } = this.props;
    if (
      breadCrumObj["scholar_result"]["breadCrum"].length == 1 &&
      match &&
      match.params &&
      match.params.slug
    ) {
      breadCrumObj["scholar_result"]["breadCrum"].push({
        url: "#",
        name: match.params.slug
      });
    }

    if (
      breadCrumObj["scholar_result"]["breadCrum"].length > 1 &&
      match &&
      match.params &&
      match.params.slug
    ) {
      breadCrumObj["scholar_result"]["breadCrum"].pop();
      breadCrumObj["scholar_result"]["breadCrum"].push({
        url: "#",
        name: match.params.slug
      });
    }

    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <section>
          <BreadCrum
            classes={breadCrumObj["scholar_result"]["bgImage"]}
            listOfBreadCrum={breadCrumObj["scholar_result"]["breadCrum"]}
            title={breadCrumObj["scholar_result"]["title"]}
            hideBanner={breadCrumObj["scholar_result"]["hideBanner"]}
            subTitle={breadCrumObj["scholar_result"]["subTitle"]}
          />
          <section className="container-fluid scholar-result">
            <section className="container equial-padding">
              <section className="row">
                <article className="text-center result-heading">
                  <h1
                    dangerouslySetInnerHTML={{
                      __html: this.getTitle()
                    }}
                  />
                  <article className="resultDetails text-center">
                    <span>Publish Date:</span> {this.getDate()}
                    <span>Selected Candidates:</span> {this.getCondidate()}
                  </article>
                </article>
              </section>
              <section className="row ">
                <article className="col-md-12 resultDetails">
                  {/*   <p>
                  “If you educate a man, you educate an individual. But if you
                  educate a woman, you educate a nation.” Adhering to this
                  African proverb, LÓréal India Pvt. Ltd. in partnership with
                  Buddy4Study has introduced the L’Oréal India for Young Women
                  in Science Scholarships 2017. Initiated in the year 2003, the
                  scheme has consistently encouraged young women to pursue a
                  career in scientific studies. Introduced with an aim to
                  provide financial assistance to promising young women
                  belonging to financially weaker sections of the society to
                  cover their college fees, the scheme will offer INR 2,50,000
                  worth of scholarship to each selected scholar. Being
                  applicable for girl students who have passed class 12
                  (PCB/PCM) in 2017 with 85% marks and are below 19 years of
                  age, this year, a total of 50 meritorious but underprivileged
                  girls have been awarded, who, not only fulfilled the
                  eligibility criteria but also came across the stringent
                  selection process. Buddy4Study played a crucial role in
                  managing the overall process of scholarship announcement,
                  inviting applications and selection too. Having received a
                  large number of scholarship applications, the process of
                  selecting only 50 students was really a tedious task. However,
                  with the help of a stringent selection process through
                  interview, Buddy4Study has successfully helped L’Oréal India
                  Pvt. Ltd. to announce the winners of this year’s Young Women
                  in Science Scholarship.
                  <br />
                  <br />
                  Congratulations! To all the winners of L’Oréal India for Young
                  Women in Science Scholarship 2017. Buddy4Study wishes the best
                  for all the winners and hopes that they achieve all their
                  aspirations with ease, especially in the field of scientific
                  studies. The students, who have, unfortunately not been able
                  to make up for the selection need not to get disappointed as
                  there are a gamut of scholarship opportunities available for
                  them at Buddy4Study. Just browse through our scholarship
                  section and apply for the scholarships for which you are
                  eligible to stand a chance of winning.
                </p> */}
                </article>
              </section>
            </section>
          </section>
          {awardeesDetailsResult &&
          awardeesDetailsResult.awardees.length > 0 ? (
            <section className="members-winner">
              <section className="box bgRcolor">
                <section className="container">
                  {/* <ScholarshipResultSearchFilter
                      {...this.props}
                    /> */}

                  <section className="row">
                    {!!this.state.awardees && this.state.awardees.length > 0 && (
                      <article className="text-center result-heading ">
                        <h1>Congratulations Scholarship Awardees</h1>
                      </article>
                    )}
                    <section className="row resultSearch">
                      <form onSubmit={this.handleScholarshipAwardeesFilter}>
                        <article className="search-ctrl">
                          <article className="form-group">
                            <label> Mobile Number / Email </label>
                            <input
                              type="text"
                              value={this.state.emailMobileSearch}
                              name="emailMobileSearch"
                              id="emailMobileSearch"
                              className="form-control"
                              placeholder="Search by Mobile Number or Email"
                              autoComplete={"off"}
                              //required
                              onChange={this.handleChange}
                              onFocus={() =>
                                this.setState({ showMsg: "", isShowMsg: false })
                              }
                            />
                            {!!this.state.validations &&
                            !!this.state.validations.emailMobileSearch ? (
                              <span className="errormsg">
                                {this.state.validations.emailMobileSearch}
                              </span>
                            ) : null}
                            {!!this.state.validations.emailMobileSearch &&
                              !!this.state.mobileError && <br />}

                            {!!this.state.validations &&
                            !!this.state.validations.emailMobileSearch ? (
                              <span className="errormsg">
                                {this.state.validations.emailMobileSearch}
                              </span>
                            ) : null}
                            {!!this.state.validations.emailMobileSearch &&
                              !!this.state.mobileError && <br />}
                            {!!this.state.mobileError &&
                              !this.state.validations.emailMobileSearch && (
                                <span className="errormsg">
                                  {this.state.mobileError}
                                </span>
                              )}
                          </article>

                          <article className="form-group">
                            <label>Date of Birth </label>
                            <MaskedInput
                              type="text"
                              mask={[
                                /\d/,
                                /\d/,
                                "/",
                                /\d/,
                                /\d/,
                                "/",
                                /\d/,
                                /\d/,
                                /\d/,
                                /\d/
                              ]}
                              className="form-control"
                              keepCharPositions={true}
                              guide={true}
                              id="dobSearch"
                              key={`${Math.floor(Math.random() * 1000)}-min`}
                              placeholder={"dd/mm/yyyy"}
                              defaultValue={updatedate1}
                              onBlur={e => this.onBlurChangeMethod(e)}
                              onKeyDown={e => {
                                if (e.keyCode == 13) {
                                  this.handleKeyPress(e);
                                }
                              }}
                              //onChange={this.handleChange}
                            />
                            {this.state.validations.dobSearch ? (
                              <span className="errormsg">
                                {this.state.validations.dobSearch}
                              </span>
                            ) : null}
                          </article>

                          <article className="btnwrapper">
                            <button
                              type="submit"
                              //onClick={this.handleApplicationNumberSearch}
                              className="btnsearch"
                            >
                              Search
                            </button>
                            <button
                              type="submit"
                              onClick={this.handlePageReset}
                              className="btnReset"
                            >
                              Reset
                            </button>
                          </article>
                        </article>
                      </form>
                    </section>

                    {!!this.state.awardeesSuccess && (
                      <section className="row messagescholarship">
                        <h4>
                          Congratulations! You are one of the awardees of the{" "}
                          <b>{this.state.scholarshipTitle}</b>.{" "}
                        </h4>
                        <p>
                          As a next step, please login into your Buddy4Study
                          account and provide us information to facilitate
                          disbursal of scholarship funds to your account.{" "}
                        </p>
                        <p>
                          {" "}
                          You can login into your account using your registered
                          email id or mobile number and password.{" "}
                        </p>
                        <p>
                          {" "}
                          In case you have forgotten password, you can change
                          password using Forgot Password Functionality.
                        </p>
                      </section>
                    )}

                    {this.state.awardeesSuccess == false && (
                      <section className="row messagescholarship">
                        <p>
                          We regret to inform you that you have not been
                          selected for <b>{this.state.scholarshipTitle} </b>.
                          <br />
                          But don’t get disheartened as we have your information
                          and we will keep you informed about upcoming
                          scholarships matching your profile. Keep applying.
                        </p>
                      </section>
                    )}

                    <section className="row ">
                      <section className="col-md-12">
                        {/* this.getAwardees() */}
                        {this.getAwardeesTable()}
                        <article className="pagination">
                          {this.state.awardeesDetailsResult &&
                          this.state.awardeesDetailsResult.awardees &&
                          this.state.awardeesDetailsResult.count > 20 ? (
                            <Pagination
                              defaultPageSize={this.state.ITEMS_PER_PAGE}
                              pageSize={this.state.ITEMS_PER_PAGE}
                              defaultCurrent={1}
                              current={this.state.currentPage}
                              onChange={this.onPageChange}
                              total={this.props.awardeesDetailsResult.count}
                              showTitle={false}
                            />
                          ) : null}
                        </article>
                        <section className="row hide ">
                          <section className="col-md-12 text-center ">
                            <a href="# " className="btn">
                              EXPLORE MORE WINNERS
                            </a>
                          </section>
                        </section>
                      </section>
                    </section>
                  </section>
                </section>
              </section>
            </section>
          ) : null}
          <section className="container-fluid scholar-result">
            <section className="container equial-padding">
              <section className="row ">
                <article className="arrowicon">
                  <Slider {...settings}>{this.getSlider()}</Slider>
                </article>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default ScholarshipResultDetailsPage;
