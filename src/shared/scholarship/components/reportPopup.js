import React, { Component } from "react";
import Loader from "../../common/components/loader";

const ReportPopup = props => {
  if (props.isOpen) {
    return (
      <article className="popup">
        <article className="popupsms">
          <article className="popup_inner loginpopup">
            <article className="LoginRegPopup">
              <section className="modal fade modelAuthPopup1">
                <section className="modal-dialog">
                  <section className="modal-content modelBg  emailverification scholreport">
                    <article className="modal-header">
                      <h3 className="modal-title">Report an issue</h3>

                      <button
                        type="button"
                        className="close btnPos"
                        onClick={props.close}
                      >
                        <i>&times;</i>
                      </button>
                    </article>

                    <article className="modal-body forgot report">
                      <article className="row">
                        <article className="col-md-12">
                          <form
                            name="forgotForm"
                            className="formelemt"
                            autocapitalize="off"
                          >
                            <h4>Please tell us about the issue</h4>
                            <section className="row">
                              <section className="ctrl-wrapper">
                                <article className="form-group">
                                  <select
                                    name="page"
                                    className="form-control"
                                    value=""
                                  >
                                    <option value="">
                                      Which part of the page{" "}
                                    </option>
                                    <option value="">
                                      Deadline/ Elegibility{" "}
                                    </option>
                                    <option value="">
                                      Award/ How to apply{" "}
                                    </option>
                                    <option value="">Contact Info </option>
                                    <option value="">Other </option>
                                  </select>
                                </article>
                              </section>
                            </section>
                            <section className="row">
                              <section className="ctrl-wrapper">
                                <article class="form-group textAreaCrtl">
                                  <textarea
                                    class="form-control"
                                    placeholder="comment"
                                    id="comment"
                                    name="comment"
                                  />
                                </article>
                              </section>
                            </section>
                            <section className="row">
                              <button
                                type="submit"
                                className="btn-block greenBtn reportbtn"
                              >
                                submit
                              </button>
                            </section>
                          </form>
                        </article>
                      </article>
                    </article>
                  </section>
                </section>
              </section>
            </article>
          </article>
        </article>
      </article>
    );
  } else {
    return "";
  }
};

export default ReportPopup;
