import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
import { Redirect } from "react-router-dom";
import UserLoginRegistrationPopup from "../../login/containers/userLoginRegistrationContainer";
import FeaturedScholarships from "../../components/featured-scholarship/featuredScholarship";
import NeedHelp from "../../components/need-help/needHelp";
import moment from "moment";
import {
  FacebookShareButton,
  TwitterShareButton,
  LinkedinShareButton,
  FacebookShareCount,
  LinkedinShareCount,
  WhatsappShareButton
} from "react-share";
import gblFunc from "../../../globals/globalFunctions";
import Loader from "../../common/components/loader";
import {
  FETCH_SCHOLARSHIP_DETAILS_SUCCEEDED,
  FETCH_FAV_SCHOLARSHIP_SUCCEEDED,
  FETCH_LIKE_FOLLOW_SUCCEEDED,
  FETCH_ARCHIVE_FAILED,
  FETCH_RELATED_ARTICLE_FAILED,
  FETCH_FAQ_SUCCEEDED,
  ADD_LIKE_FOLLOW_SUCCEEDED,
  FETCH_ARCHIVE_SUCCEEDED,
  FETCH_SCHOLAR_SUCCEEDED,
  FETCH_RELATED_ARTICLE_SUCCEEDED,
  FETCH_FAQ_FAILED,
  FETCH_SCHOLAR_FAILED
} from "../actions";
import {
  ADD_TO_FAV_SUCCESS,
  FETCH_FAV_PERSIST_SUCCESS,
  ADD_TO_FAV_FAIL
} from "../../dashboard/actions";
import {
  isMobile
} from "react-device-detect";
import {
  messages,
  prodEnv,
  applicationFormsUrl,
  setCookie,
  getCookie,
  imgBaseUrl,
  imgBaseUrlDev,
  defaultImageLogo,
  ADMISSION_PRODUCTION_URL,
  ADMISSION_STAGING_URL
} from "../../../constants/constants";
import AlertMessage from "../../common/components/alertMsg";
import LoginRegisterSection from "../../application/components/loginRegisterSection";
import {
  FETCH_CONTACTUS_SUCCEEDED,
  FETCH_CONTACTUS_FAILED
} from "../../../constants/commonActions";
import ReportPopup from "./reportPopup";
import {
  LOGIN_USER_SUCCEEDED,
  SOCIAL_LOGIN_USER_SUCCEEDED
} from "../../login/actions";
import TrackVisibility from "react-on-screen";
import { FETCH_FAQS_SUCCEEDED, FETCH_FAQS_FAILED } from "../../faqs/actions";
import Winners from "../../scholarship-conclave/components/winner";

class ScholarshipDetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      isFaqLoad: false,
      archive: null,
      scholars: null,
      faqlist: null,
      location: null,
      relatedSch: null,
      isArchiveSchLoad: false,
      isRelatedScholarship: false,
      isLiked: false,
      isFollowed: false,
      likedFollowCount: null,
      showButton: true,
      onFirstLoad: true,
      isShowAlert: false,
      showLoginPopup: false,
      statusAlert: false,
      isNeedHelp: true,
      redirectToPage: "",
      hideCloseButton: true,
      hideForgetCloseBtn: true,
      alertMsg: "",
      addToFavIcon: "fa fa-heart-o",
      isSocialShare: false,
      activeTab: 1,
      isReportPopUpOpen: false,
      faqID: null,
      faq_failed_msg: false,
      scholar_failed_msg: false,
      archive_failed_msg: false,
      origin: "",
      originId: null,
      redirectionId: null,
      redirectionUrl: null,
      redirectStatus: false,
      page: 0
    };

    this.showNeedHelp = this.showNeedHelp.bind(this);
    this.gtmEvent = this.gtmEvent.bind(this);
    this.pageUrl = typeof window !== "undefined" ? window.location.href : "";
    this.hideAlert = this.hideAlert.bind(this);
    this.closeLoginPopup = this.closeLoginPopup.bind(this);
    this.showLoginPopup = this.showLoginPopup.bind(this);
    this.submitContact = this.submitContact.bind(this);
    this.scrollHandler = this.scrollHandler.bind(this);
    this.validateLoginPopUp = this.validateLoginPopUp.bind(this);
    this.importantLinksHandler = this.importantLinksHandler.bind(this);
    this.handleStickySocialShareLoad = this.handleStickySocialShareLoad.bind(
      this
    );
    this.closeReportPopup = this.closeReportPopup.bind(this);
    this.onFaqHandler = this.onFaqHandler.bind(this);
    this.changeTab = this.changeTab.bind(this);
    this.onArchiveScholarshipTab = this.onArchiveScholarshipTab.bind(this);
    this.onRelatedSchHandler = this.onRelatedSchHandler.bind(this);
    this.onLikeFollowHandler = this.onLikeFollowHandler.bind(this);
    this.onclickFaqHandler = this.onclickFaqHandler.bind(this);
    this.onAdmissionUrlHandler = this.onAdmissionUrlHandler.bind(this);
    this.onAdmissionDefaultUrlHandler = this.onAdmissionDefaultUrlHandler.bind(
      this
    );
    this.admissionBanner = this.admissionBanner.bind(this);
    this.articleNextPrev = this.articleNextPrev.bind(this);
    this.onLikeFollowHandlerNotLogin = this.onLikeFollowHandlerNotLogin.bind(this);
  }
  changeTab(tab) {
    const { scholarshipDetail } = this.props;
    if (tab) {
      switch (tab) {
        case 1:
          this.setState({
            activeTab: tab
          });
          break;
        case 2:
          if (scholarshipDetail && scholarshipDetail.usid) {
            this.setState(
              {
                activeTab: tab
              },
              () =>
                !this.state.scholars
                  ? this.props.fetchScholars({
                    usid: scholarshipDetail.usid
                  })
                  : ""
            );
          }
          break;
      }
    }
  }
  onclickFaqHandler(id) {
    this.setState({
      faqID: id
    });
  }

  onAdmissionUrlHandler(courses, pathName) {
    if (window) {
      const location =
        (prodEnv ? ADMISSION_PRODUCTION_URL : ADMISSION_STAGING_URL) +
        (courses + pathName);

      window.open(location, "_blank");
    }
  }
  onAdmissionDefaultUrlHandler() {
    if (window) {
      const location = prodEnv
        ? ADMISSION_PRODUCTION_URL
        : ADMISSION_STAGING_URL;

      window.open(location, "_blank");
    }
  }
  onLikeFollowHandlerNotLogin(type, id) {
    this.setState({
      hideCloseButton: false,
      showLoginPopup: true,
      origin: type,
      originId: id
    })
  }
  onLikeFollowHandler(scholarshipId, likeOrFollow, type = "") {
    if (likeOrFollow === "Like") {
      this.setState(
        {
          isLiked: type === "Login" ? true : !this.state.isLiked,
          loginStatus: false,
          origin: "",
          originId: null
        },
        () =>
          this.props.postLikeFollow({
            userId:
              gblFunc.getStoreUserDetails()["userId"] ||
              this.props.userLoginData.userId,
            scholarshipId,
            like: type === "Login" ? true : this.state.isLiked
          })
      );
    } else if (likeOrFollow === "Follow") {
      this.setState(
        {
          isFollowed: type === "Login" ? true : !this.state.isFollowed,
          loginStatus: false,
          origin: "",
          originId: null
        },
        () =>
          this.props.postLikeFollow({
            userId:
              gblFunc.getStoreUserDetails()["userId"] ||
              this.props.userLoginData.userId,
            scholarshipId,
            follow: type === "Login" ? true : this.state.isFollowed
          })
      );
    }
  }

  onArchiveScholarshipTab() {
    const { scholarshipDetail } = this.props;
    if (
      scholarshipDetail &&
      scholarshipDetail.id &&
      !this.state.archive &&
      !this.state.isArchiveSchLoad &&
      this.state.activeTab == 1
    ) {
      this.setState(
        {
          isArchiveSchLoad: true
        },
        () =>
          this.props.fetchArchive({
            scholarshipId: scholarshipDetail.id
          })
      );
    }

    return;
  }

  onRelatedSchHandler() {
    const { scholarshipDetail } = this.props;

    if (
      scholarshipDetail &&
      scholarshipDetail.id &&
      !this.state.relatedSch &&
      !this.state.isRelatedScholarship
    ) {
      this.setState(
        {
          isRelatedScholarship: true
        },
        () =>
          this.props.getRelatedScholarship({
            bsid: scholarshipDetail.bsid,
            len: 3
          })
      );
    }

    return;
  }

  closeLoginPopup(e) {
    // e.preventDefault();
    this.setState({ location: null })
    const isAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    if (
      isAuthenticated &&
      this.state.redirectStatus &&
      this.state.redirectionUrl &&
      this.state.redirectionId
    ) {
      this.gtmEvent(
        1,
        this.state.redirectionId,
        this.state.redirectionUrl,
        "apply now"
      );
      this.setState({
        redirectStatus: false,
        redirectionUrl: null,
        redirectionId: null
      });
    } else {
      this.setState({
        redirectionUrl: null,
        redirectStatus: false,
        redirectionId: null
      });
    }
    if (e && e.target && e.target.innerHTML !== "OK") {
      this.setState(
        {
          showLoginPopup: false
        },
        //() => setCookie("schDetailLogin", "1 day", 24)
      );
    } else {
      this.setState({
        showLoginPopup: false
      });
    }
  }

  submitContact(data) {
    this.props.loadContact(data);
    this.setState({
      ...this.state,
      isNeedHelp: true
    });
  }

  closeReportPopup() {
    this.setState({
      isReportPopUpOpen: false
    });
  }

  gtmEvent(option, scholarshipId, userList, type) {
    this.setState({ location: this.props.history.location.pathname })
    switch (option) {
      case 1:
        if (type == "apply now" && this.validateLoginPopUpForApplyNow()) {
          this.setState({ hideCloseButton: false });
          return;
        } else if (this.validateLoginPopUp()) {
          this.setState({ hideCloseButton: false });
          return;
        }
        let userData = gblFunc.getStoreUserDetails();
        gblFunc.gaTrack.trackEvent(["Scholarship", "Apply Now"]);
        const { userId } = userData;
        if (userId && scholarshipId) {
          this.props.applyNowHit({ userId, scholarshipId });
        }
        const original_link = [];

        const BUDDY4STUDY_DOMAIN_ONE = "www.buddy4study.com";
        const BUDDY4STUDY_DOMAIN_TWO = "buddy4study.com";

        const extractHostName = url => {
          var hostname;

          if (url.indexOf("://") > -1) {
            hostname = url.split("/")[2];
          } else {
            hostname = url.split("/")[0];
          }

          hostname = hostname.split(":")[0];
          hostname = hostname.split("?")[0];

          return hostname;
        };

        const extractRootDomain = url => {
          var domain = extractHostName(url),
            splitArr = domain.split("."),
            arrLen = splitArr.length;

          //extracting the root domain here
          //if there is a subdomain
          if (arrLen > 2) {
            domain = splitArr[arrLen - 2] + "." + splitArr[arrLen - 1];
            //check to see if it's using a Country Code Top Level Domain (ccTLD) (i.e. ".me.uk")
            if (
              splitArr[arrLen - 2].length == 2 &&
              splitArr[arrLen - 1].length == 2
            ) {
              //this is using a ccTLD
              domain = splitArr[arrLen - 3] + "." + domain;
            }
          }

          return domain;
        };

        const extractDomainAndRedirect = url => {
          const rootDomain = extractRootDomain(url);
          if (
            rootDomain === BUDDY4STUDY_DOMAIN_ONE ||
            rootDomain === BUDDY4STUDY_DOMAIN_TWO
          ) {
            // ("/applications/:slug/:pageName");

            /*
(window.location =
                (prodEnv ? applicationFormsUrl.prod : applicationFormsUrl.qa) +
                location.pathname +
                "/" +
                gblFunc.getRefreshToken() +
                "/b4s")


            */
            const B4S_BASE_URL = prodEnv
              ? applicationFormsUrl.prod
              : applicationFormsUrl.qa;

            if (window !== undefined) {
              window.open(url, "_blank");
            }
          } else {
            if (window !== undefined) {
              window.open(url, "_blank");
            }
          }
        };

        const applyOnlineLink = userList.filter(
          links => links.type.name === "Apply online link"
        );

        if (applyOnlineLink.length !== 0) {
          extractDomainAndRedirect(applyOnlineLink[0].url);
        } else {
          const scholarshipLink = userList.filter(
            links => links.type.name === "Latest scholarship link"
          );

          if (scholarshipLink.length !== 0) {
            extractDomainAndRedirect(scholarshipLink[0].url);
          } else {
            const originalWebsiteLink = userList.filter(
              links => links.type.name === "Original website"
            );

            if (originalWebsiteLink.length !== 0) {
              extractDomainAndRedirect(originalWebsiteLink[0].url);
            }
          }
        }

        break;
      case 2:
        gblFunc.gaTrack.trackEvent(["Scholarship", "Share"]);
        break;
      case 3:
        gblFunc.gaTrack.trackEvent(["Scholarship", "Save"]);
        if (type == "addToFav" && userList.userId && scholarshipId) {
          this.props.addToFavScholarships({
            userId: userList.userId,
            scholarshipId
          });
        }
        break;
      case 4:
        gblFunc.gaTrack.trackEvent(["Scholarship", "Apply online link"]);
        break;
    }
  }
  /* Social share Icon Animation */

  handleStickySocialShareLoad() {
    if (typeof window !== "undefined") {
      if (window.scrollY > 60) {
        this.setState({
          isSocialShare: true
        });
      } else {
        this.setState({
          isSocialShare: false
        });
      }
    }
  }

  importantLinksHandler(url) {
    let isOriginalWebsite = null;
    if (
      this.props.scholarshipDetail &&
      this.props.scholarshipDetail.scholarshipWebsiteLinks
    ) {
      const { scholarshipWebsiteLinks } = this.props.scholarshipDetail;
      isOriginalWebsite = scholarshipWebsiteLinks.filter(
        link => link.type.id === 2
      );
    }

    if (this.validateLoginPopUp()) {
      return;
    } else if (isOriginalWebsite.length > 0 && !gblFunc.isUserAuthenticated()) {
      this.showLoginPopup();
      this.setState(() => ({
        hideCloseButton: true
      }));
    } else {
      window ? window.open(url, "_blank") : null;
    }
  }

  validateLoginPopUp() {
    const { applicationLocation } = this.props.scholarshipDetail;
    const isAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;

    if (
      !isAuthenticated &&
      !this.state.showLoginPopup &&
      applicationLocation === 0
    ) {
      this.showLoginPopup();
      this.setState(() => ({
        hideCloseButton: true
      }));
      return true;
    } else {
      return false;
    }
  }

  validateLoginPopUpForApplyNow() {
    const isAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;

    if (!isAuthenticated && !this.state.showLoginPopup) {
      this.showLoginPopup();
      this.setState(() => ({
        hideCloseButton: true
      }));
      return true;
    } else {
      return false;
    }
  }

  showNeedHelp() {
    this.setState({
      ...this.state,
      showModal: !this.state.showModal,
      showButton: !this.state.showButton
    });
  }

  componentDidMount() {
    if (
      this.props.match.params &&
      this.props.match.params.slug.includes(
        "fair-and-lovely-foundation-womens-education-scholarship-2018"
      )
    ) {
      this.props.history.push(
        `/fair-and-lovely-career-foundation-nepal-scholarship`
      );
      return;
    }
    if (this.props.match.params && this.props.match.params.slug) {
      this.getScholarshipDetails(this.props.match.params.slug);
    }

    if (  typeof window !== "undefined" && window) {
      window.addEventListener("scroll", this.scrollHandler);
      window.addEventListener("scroll", this.handleStickySocialShareLoad)
    }
  }
  // componentWillUnmount() {
  //   if (window) {
  //     window.addEventListener("scroll", this.handleStickySocialShareLoad);
  //   }
  // }
  scrollHandler() {
    if (window.scrollY > 550) {
      let auth = gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
      if (
        !auth &&
        !this.state.showLoginPopup &&
        this.props.scholarshipDetail.applicationLocation === 0 &&
        this.props.scholarshipDetail &&
        !getCookie("schDetailLogin")
      ) {
        //  this.showLoginPopup();
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    const { isNeedHelp } = this.state;
    const { type } = nextProps;
    if (nextProps.match.params.slug != this.props.match.params.slug) {
      this.props.history.push(`/scholarship/${nextProps.match.params.slug}`);
    }
    switch (type) {
      case FETCH_RELATED_ARTICLE_SUCCEEDED:
        this.setState(
          {
            relatedSch: nextProps.relatedSch
          }
        );
        break;
      case ADD_LIKE_FOLLOW_SUCCEEDED:
        this.props.likeFollow({
          userId: gblFunc.getStoreUserDetails()["userId"],
          scholarshipId:
            this.props.scholarshipDetail && this.props.scholarshipDetail.id
              ? this.props.scholarshipDetail.id
              : ""
        });
        this.props.getFavScholarship({
          scholarshipId:
            this.props.scholarshipDetail && this.props.scholarshipDetail.id
              ? this.props.scholarshipDetail.id
              : ""
        });

        break;
      case FETCH_LIKE_FOLLOW_SUCCEEDED:
        // nextProps.likeAndFollow.follow
        this.setState(
          {
            isLiked: nextProps.likeAndFollow.favourite,
            isFollowed: nextProps.likeAndFollow.follow
          },
          () => {
            if (
              this.state.loginStatus &&
              !this.state.isLiked &&
              this.state.origin === "Like" &&
              this.state.originId
            ) {
              this.onLikeFollowHandler(
                this.state.originId,
                this.state.origin,
                "Login"
              );
            } else if (
              this.state.loginStatus &&
              !this.state.isFollowed &&
              this.state.origin === "Follow" &&
              this.state.originId
            ) {
              this.onLikeFollowHandler(
                this.state.originId,
                this.state.origin,
                "Login"
              );
            }
          }
        );
        break;
      case FETCH_FAV_SCHOLARSHIP_SUCCEEDED:
        this.setState({
          likedFollowCount: nextProps.favSch
        });
        break;
      case FETCH_RELATED_ARTICLE_FAILED:
        this.setState({
          isRelatedScholarship: true
        });
        break;
      case FETCH_ARCHIVE_SUCCEEDED:
        this.setState({
          archive: nextProps.archive
        });
        break;
      case FETCH_SCHOLAR_SUCCEEDED:
        this.setState({
          scholars: nextProps.scholars
        });
        break;
      case FETCH_SCHOLAR_FAILED:
        this.setState({
          scholar_failed_msg: true
        });
        break;
      case FETCH_ARCHIVE_FAILED:
        this.setState({
          archive_failed_msg: true,
          isArchiveSchLoad: true
        });
        break;
      case FETCH_FAQ_SUCCEEDED:
        this.setState({
          faqlist: nextProps.faq
        });
        break;
      case FETCH_FAQ_FAILED:
        this.setState({
          faq_failed_msg: true,
          isFaqLoad: true
        });
        break;
      case "VERIFY_TOKEN":
        if (
          nextProps.userLoginData &&
          nextProps.userLoginData.data &&
          nextProps.userLoginData.data.data &&
          nextProps.userLoginData.data.data.mobileVerified == "0"
        ) {
          this.props.history.push({
            pathname: "/otp",
            state: {
              userId: nextProps.userLoginData.data.data.userId,
              mobile: nextProps.userLoginData.data.data.mobile,
              countryCode: nextProps.userLoginData.data.data.countryCode,
              temp_Token: nextProps.userLoginData.data.data.token,
              location:
                this.props.location && this.props.location.pathname
                  ? this.props.location.pathname
                  : null
            }
          });
        }
        break;
      case LOGIN_USER_SUCCEEDED:
        this.setState({ loginStatus: true }, () => {
          if (this.props.userLoginData && this.props.userLoginData.userId) {
            this.props.likeFollow({
              userId: this.props.userLoginData.userId,
              scholarshipId:
                this.props.scholarshipDetail && this.props.scholarshipDetail.id
                  ? this.props.scholarshipDetail.id
                  : ""
            });
          }
        });
        break;

      case FETCH_SCHOLARSHIP_DETAILS_SUCCEEDED:
        const userList = gblFunc.getStoreUserDetails();
        if (
          nextProps.scholarshipDetail &&
          nextProps.scholarshipDetail.status == 0
        ) {
          this.props.history.push("/scholarships");
          return;
        }
        // if (userList.userId) {
        //   this.props.persistFavSch({
        //     userId: userList.userId,
        //     nid: nextProps.scholarshipDetail.id
        //   });
        // }

        this.props.hitCounter({
          userId: userList && userList.userId ? userList.userId : null,
          nid: nextProps.scholarshipDetail.id
        });

        this.props.getFavScholarship({
          scholarshipId: nextProps.scholarshipDetail.id
        });

        if (!(gblFunc.isUserAuthenticated() || this.props.isAuthenticated)) {
          const { scholarshipDetail } = nextProps;
          if (window && !getCookie("schDtCounter")) {
            setCookie("schDtCounter", 1, 24);
          } else {
            if (
              parseInt(getCookie("schDtCounter"), 10) >= 2 &&
              scholarshipDetail &&
              scholarshipDetail.applicationLocation === 0
            ) {
            //  this.showLoginPopup();
            } else {
              setCookie(
                "schDtCounter",
                parseInt(getCookie("schDtCounter"), 10) + 1,
                24
              );
            }
          }
        }

        if (
          this.props.isAuthenticated &&
          nextProps.scholarshipDetail &&
          nextProps.scholarshipDetail.id
        ) {
          this.props.likeFollow({
            userId: gblFunc.getStoreUserDetails()["userId"],
            scholarshipId:
              nextProps.scholarshipDetail && nextProps.scholarshipDetail.id
                ? nextProps.scholarshipDetail.id
                : ""
          });
        }
        this.props.loadFeaturedScholarshipDetail();
        this.props.getRelatedArticle({
          tags: "scholarships",
          page: this.state.page,
          length: 3,
          excludeSlug: null
        });
        break;

      case FETCH_CONTACTUS_SUCCEEDED:
        if (isNeedHelp) {
          this.setState({
            isShowAlert: true,
            showModal: false,
            showButton: true,
            statusAlert: true,
            alertMsg: messages.generic.success
          });
          break;
        }

      case FETCH_CONTACTUS_FAILED:
        if (isNeedHelp) {
          this.setState({
            isShowAlert: true,
            showModal: false,
            showButton: true,
            statusAlert: false,
            alertMsg: nextProps.contactUs.contactUs
          });
          break;
        }
    }

    if (nextProps.dashType == ADD_TO_FAV_SUCCESS) {
      if (isNeedHelp) {
        this.setState({
          addToFavIcon: "fa fa-heart",
          isShowAlert: true,
          showModal: false,
          showButton: true,
          statusAlert: true,
          alertMsg: messages.addMyFavScholarship.success
        });
      }
    } else if (nextProps.dashType == ADD_TO_FAV_FAIL) {
      if (isNeedHelp) {
        this.setState({
          isShowAlert: true,
          showModal: false,
          showButton: true,
          statusAlert: false,
          alertMsg: nextProps.favScholarships
        });
      }
    }
    if (nextProps.dashType == FETCH_FAV_PERSIST_SUCCESS) {
      this.setState({
        addToFavIcon: nextProps.persistData ? "fa fa-heart" : "fa fa-heart-o"
      });
    }
  }

  rawMarkUp(contact) {
    let rawMarkup = contact;
    return { __html: rawMarkup };
  }

  getScholarshipDetails(slug) {
    this.props.loadScholarshipDetail(slug);
    //this.props.loadFeaturedScholarshipDetail();
  }

  hideAlert() {
    //  close popup

    this.setState({
      isShowAlert: false,
      isNeedHelp: false
    });
  }

  showLoginPopup() {
    this.setState({
      showLoginPopup: true
    });
  }

  onFaqHandler() {
    const { scholarshipDetail } = this.props;

    if (
      scholarshipDetail &&
      scholarshipDetail.usid &&
      !this.state.faqlist &&
      !this.state.isFaqLoad
    ) {
      this.setState(
        {
          isFaqLoad: true
        },
        () =>
          this.props.fetchFaq({
            usid: scholarshipDetail.usid
          })
      );
    }

    return;
  }
  /* if(this.props.scholarshipDetail.scholarshipSourceCountries.length > 0 {
      
    return this.setState({scholarshipSourceCountries:this.props.scholarshipDetail.scholarshipSourceCountries})
    
} */
  admissionBanner() {
    const scholarshipSourceCountries = this.props.scholarshipDetail
      .scholarshipSourceCountries;
    if (scholarshipSourceCountries) {
      if (
        scholarshipSourceCountries &&
        scholarshipSourceCountries.length > 0 &&
        scholarshipSourceCountries[0].country.id !== 1001 &&
        (scholarshipSourceCountries &&
          scholarshipSourceCountries.length > 0 &&
          scholarshipSourceCountries[0].country.id !== 102) &&
        (scholarshipSourceCountries &&
          scholarshipSourceCountries.length > 0 &&
          scholarshipSourceCountries[0].country.id !== 1003)
      ) {
        return (
          <article className="data-row">
            <article className="bannerRow">
              <img src="https://buddy4study.s3-ap-southeast-1.amazonaws.com/static/images/Study-Abroad-Banner-thumb.jpg" />
              <article className="bannerContent">
                <h4>
                  Looking for Study in
                  {scholarshipSourceCountries &&
                    scholarshipSourceCountries.length > 0 && (
                      <i>
                        {scholarshipSourceCountries &&
                          scholarshipSourceCountries.length > 0 &&
                          scholarshipSourceCountries[0].country.name}
                      </i>
                    )}
                  ?
                </h4>
                <p>
                  Find Top University and Courses Now!
                  {scholarshipSourceCountries &&
                    scholarshipSourceCountries.length > 0 &&
                    scholarshipSourceCountries[0].country.id !== 1002 ? (
                      <span
                        onClick={() =>
                          this.onAdmissionUrlHandler(
                            "courses/",
                            scholarshipSourceCountries &&
                            scholarshipSourceCountries.length > 0 &&
                            scholarshipSourceCountries[0].country.name
                              .split(" ")
                              .join("-")
                          )
                        }
                      >
                        Explore Here
                    </span>
                    ) : (
                      <span onClick={this.onAdmissionDefaultUrlHandler}>
                        Explore Here
                    </span>
                    )}
                </p>
              </article>
            </article>
          </article>
        );
      }
      if (
        scholarshipSourceCountries &&
        scholarshipSourceCountries.length > 0 &&
        scholarshipSourceCountries[0].country.id === 1001 &&
        (scholarshipSourceCountries &&
          scholarshipSourceCountries.length > 0 &&
          scholarshipSourceCountries[0].country.id === 102) &&
        (scholarshipSourceCountries &&
          scholarshipSourceCountries.length > 0 &&
          scholarshipSourceCountries[0].country.id === 1003)
      ) {
        return (
          <article className="data-row">
            <article className="bannerRow">
              <img src="https://buddy4study.s3-ap-southeast-1.amazonaws.com/static/images/Study-Abroad-Banner-thumb.jpg" />
              <article className="bannerContent">
                <h4>
                  Looking for Study in{" "}
                  <i>
                    {scholarshipSourceCountries &&
                      scholarshipSourceCountries.length > 0 &&
                      scholarshipSourceCountries[1].country.name}
                  </i>
                  ?
                </h4>
                <p>
                  Find Top University and Courses Now!
                  {scholarshipSourceCountries &&
                    scholarshipSourceCountries.length > 0 &&
                    scholarshipSourceCountries[0].country.id !== 1002 ? (
                      <span
                        onClick={() =>
                          this.onAdmissionUrlHandler(
                            "courses/",
                            scholarshipSourceCountries &&
                            scholarshipSourceCountries.length > 0 &&
                            scholarshipSourceCountries[1].country.name
                              .split(" ")
                              .join("-")
                          )
                        }
                      >
                        Explore Here
                    </span>
                    ) : (
                      <span onClick={this.onAdmissionDefaultUrlHandler}>
                        Explore Here
                    </span>
                    )}
                </p>
              </article>
            </article>
          </article>
        );
      }
    }
  }
  articleNextPrev(count, type) {
    switch (type) {
      case "prev":
        const prevCount = count <= 0 ? 0 : count - 1;
        this.setState({ page: prevCount }, () => {
          this.props.getRelatedArticle({
            tags: "scholarships",
            page: this.state.page,
            length: 3,
            excludeSlug: null
          });
        });
        break;
      case "next":
        const nextCount = count + 1;
        this.setState({ page: nextCount }, () => {
          this.props.getRelatedArticle({
            tags: "scholarships",
            page: this.state.page,
            length: 3,
            excludeSlug: null
          });
        });
        break;
    }
  }
  render() {
    // if (this.state.redirectToPage) {
    //   // applications/:slug/:pageName"
    //   return <Redirect to={this.state.redirectToPage} />;
    // }

    let scholarshipDetail = null;
    let featuredSch = null;
    let relatedArticles = null;
    let moboRelatedArticles = null;
    let timeFormat = null;
    let mobo_featuredSch = null;
    let isInternational = false;
    let socialCount = null;
    const { showModal } = this.state;
    const userList = gblFunc.getStoreUserDetails();
    const scholarshipResponse = this.props.scholarshipDetail
      ? this.props.scholarshipDetail
      : {};
    const scholarshipList = this.props.scholarshipList
      ? this.props.scholarshipList
      : [];

    const isAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    /******************* Schema ****************************** */
    const schema = {
      type: "application/ld+json",
      innerHTML: JSON.stringify({
        "@context": "https://schema.org",
        "@type": "https://schema.org/Article",
        name: scholarshipResponse.scholarshipName,
        datePublished: scholarshipResponse.announcementDate,
        image: scholarshipResponse.logoFid,
        author: {
          "@type": "Person",
          name: "Buddy4Study"
        },
        headline: scholarshipResponse.scholarshipName,
        publisher: {
          "@type": "Organization",
          name: "Buddy4Study",
          logo: {
            "@type": "ImageObject",
            name: "buddy4studyLogo",
            url:
              "https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/logo.png"
          }
        },
        dateModified: scholarshipResponse.lastUpdatedAt,
        mainEntityOfPage: `https://www.buddy4study.com/scholarship/${scholarshipResponse.slug}`
      })
    };
    /******************* Schema ****************************** */

    if (this.props.featuredScholarshipList || scholarshipList.length > 0) {
      featuredSch = (
        <FeaturedScholarships
          headerTitle="List of our featured scholarships"
          classes={["mobo-hide"]}
          featured={this.props.featuredScholarshipList || scholarshipList || []}
          fromListPage={false}
        />
      );
      /* Small Device */
      mobo_featuredSch = (
        <FeaturedScholarships
          headerTitle="List of our featured scholarships"
          classes={["mobo-panel mobo-show desk-hide"]}
          featured={this.props.featuredScholarshipList || []}
          fromListPage={false}
        />
      );
    }

    if (
      this.props.relatedArticle ||
      (this.props.relatedArticle && this.props.relatedArticle.length > 0)
    ) {
      relatedArticles = (
        <article className="left-cell-3 article-item mobo-hide">
          <article className="relatedArticle">
            {this.props.relatedArticle && this.props.relatedArticle.length > 0 && (
              <article>
                <article>
                  <h2>Related Articles</h2>
                  {this.props.relatedArticle.map((list, index) => (
                    <article key={index} className="boxArticle">
                      <a
                        href={`https://www.buddy4study.com/article/${list.slug}`}
                        target="_blank"
                      >
                        <img
                          property="image"
                          alt="buddy4study-features"
                          src={list.logoUrl}
                          onError={e => {
                            e.target.src = defaultImageLogo;
                          }}
                        />
                      </a>
                      <p>
                        {list.title} &nbsp;
                        <a
                          href={`https://www.buddy4study.com/article/${list.slug}`}
                          target="_blank"
                        >
                          read more
                        </a>
                      </p>
                    </article>
                  ))}
                </article>
                <article className="buttonAlign">
                  <button
                    className="btn-yellow paddingbtn"
                    disabled={this.state.page == 0 ? true : false}
                    onClick={e => {
                      this.articleNextPrev(this.state.page, "prev");
                    }}
                  >
                    Prev
                  </button>
                  <button
                    className="btn-yellow paddingbtn"
                    disabled={
                      this.props.relatedArticle.length > 0 ? false : true
                    }
                    onClick={e => {
                      this.articleNextPrev(this.state.page, "next");
                    }}
                  >
                    Next
                  </button>
                </article>
              </article>
            )}
          </article>
        </article>
      );
      /* Small Device */
      moboRelatedArticles = (
        <article className="left-cell-3 article-item">
          <article className="relatedArticle">
            {this.props.relatedArticle && this.props.relatedArticle.length > 0 && (
              <article>
                <h2>Related Articles</h2>
                {this.props.relatedArticle.map(list => (
                  <article className="boxArticle">
                    <a
                      href={`https://www.buddy4study.com/article/${list.slug}`}
                      target="_blank"
                    >
                      <img
                        property="image"
                        alt="buddy4study-features"
                        src={list.logoUrl}
                        onError={e => {
                          e.target.src = defaultImageLogo;
                        }}
                      />
                    </a>

                    <p>
                      {list.title} &nbsp;
                      <a
                        href={`https://www.buddy4study.com/article/${list.slug}`}
                        target="_blank"
                      >
                        read more
                      </a>
                    </p>
                  </article>
                ))}
                <article className="buttonAlign">
                  <button
                    className="btn-yellow paddingbtn"
                    disabled={this.state.page == 0 ? true : false}
                    onClick={e => {
                      this.articleNextPrev(this.state.page, "prev");
                    }}
                  >
                    Prev
                  </button>
                  <button
                    className="btn-yellow paddingbtn"
                    disabled={
                      this.props.relatedArticle.length > 0 ? false : true
                    }
                    onClick={e => {
                      this.articleNextPrev(this.state.page, "next");
                    }}
                  >
                    Next
                  </button>
                </article>
              </article>
            )}
          </article>
        </article>
      );
    }
    socialCount = (
      <article className="socialSharedCounter scrollPosition">
        <p>
          <a href="javascript:void(0)" onClick={() => this.gtmEvent(2)}>
            <FacebookShareButton url={this.pageUrl}>
              <i className="fa fa-facebook" aria-hidden="true" />
            </FacebookShareButton>
          </a>
          {/* <FacebookShareCount url={this.pageUrl}>
            {shareCount => (
              <span className="myShareCountWrapper">{shareCount + 100}</span>
            )}
          </FacebookShareCount> */}
          <a href="javascript:void(0)" onClick={() => this.gtmEvent(2)}>
            <TwitterShareButton url={this.pageUrl}>
              <i className="fa fa-twitter" aria-hidden="true" />
            </TwitterShareButton>
          </a>
          {/* <section>
            <a
              href="https://twitter.com/intent/tweet?screen_name=TwitterDev&ref_src=twsrc%5Etfw"
              className="twitter-mention-button"
              data-show-count="true"
            >
              Tweet to @TwitterDev
            </a>

            <span id="showTwittFollow" />
          </section> */}
          <a href="javascript:void(0)" onClick={() => this.gtmEvent(2)}>
            <LinkedinShareButton url={this.pageUrl}>
              <i className="fa fa-linkedin" aria-hidden="true" />
            </LinkedinShareButton>
          </a>
          {/* <LinkedinShareCount url={this.pageUrl}>
            {shareCount => (
              <span className="myShareCountWrapper">{shareCount + 100}</span>
            )}
          </LinkedinShareCount> */}
          <a href="javascript:void(0)" onClick={() => this.gtmEvent(2)}>
            <article className="whatsapp">
              <WhatsappShareButton url={this.pageUrl}>
                <i className="fa fa-whatsapp" aria-hidden="true" />
              </WhatsappShareButton>
            </article>
          </a>
        </p>
      </article>
    );

    if (scholarshipResponse && Object.keys(scholarshipResponse).length > 0) {
      const {
        scholarshipName,
        deadlineDate,
        logoFid,
        slug,
        scholarshipMultilinguals,
        scholarshipBodyMultilinguals,
        scholarshipSourceCountries,
        scholarshipDocuments,
        scholarshipWebsiteLinks,
        id,
        bsid,
        applicationLocation,
        scholarshipRules,
        lastUpdatedAt,
        verifiedStatus,
        completedInfo
      } = scholarshipResponse;

      /* Show login popup if user is not authenticated */
      // if (
      //   !isAuthenticated &&
      //   !this.state.showLoginPopup &&
      //   applicationLocation === 0
      // ) {
      //   this.showLoginPopup();
      // }
      scholarshipRules
        ? scholarshipRules.map(ruleObj => {
          if (ruleObj.rule.id === 698) {
            isInternational = true;
          }
        })
        : null;
      const { addToFavIcon } = this.state;

      timeFormat = deadlineDate
        ? moment(deadlineDate, "YYYY-MM-DD").format("DD-MMM-YYYY")
        : " Always Open";
      scholarshipDetail = (
        <section className="row">
          <h1 className="titleText">
            {scholarshipName
              ? gblFunc.replaceWithLoreal(scholarshipName, true)
              : ""}
            {verifiedStatus ? (
              <div className="tooltip check">
                <i className="fa fa-check" />
                <div className="top">
                  <p>Verified</p>
                  <i />
                </div>
              </div>
            ) : (
                ""
              )}

            {completedInfo ? (
              <div className="tooltip info">
                <i className="fa fa-info" />
                <div className="top">
                  <p>Complete Info</p>
                  <i />
                </div>
              </div>
            ) : (
                ""
              )}
          </h1>

          <section className="moboCellOrder">
            <article className="row flex-item">
              <section className="col-md-9 col-sm-9">
                <article className="followWrapper">
                  <button
                    onClick={() =>
                      isAuthenticated
                        ? this.onLikeFollowHandler(id, "Like")
                        : this.onLikeFollowHandlerNotLogin("Like", id)
                    }
                    className={`btn-yellow paddingbtn ${
                      isAuthenticated && this.state.isLiked ? "active" : ""
                      }`}
                  >
                    {isAuthenticated && this.state.isLiked ? "Liked" : "Like"}
                  </button>
                  <button
                    onClick={() =>
                      isAuthenticated
                        ? this.onLikeFollowHandler(id, "Follow")
                        : this.onLikeFollowHandlerNotLogin("Like", id)
                    }
                    className={`btn-yellow paddingbtn ${
                      isAuthenticated && this.state.isFollowed ? "active" : ""
                      }`}
                  >
                    {isAuthenticated && this.state.isFollowed
                      ? "Followed"
                      : "Follow"}
                  </button>
                  <article className="right">
                    <ul>
                      {this.state.likedFollowCount &&
                        (this.state.likedFollowCount.totalFavourite ||
                          this.state.likedFollowCount.totalFavourite == 0) ? (
                          <li>
                            {" "}
                            {this.state.likedFollowCount.totalFavourite} Likes
                        </li>
                        ) : null}
                      {this.state.likedFollowCount &&
                        (this.state.likedFollowCount.totalFollowed ||
                          this.state.likedFollowCount.totalFollowed == 0) ? (
                          <li>
                            {" "}
                            {this.state.likedFollowCount.totalFollowed} Followers
                        </li>
                        ) : null}
                    </ul>
                  </article>
                </article>
                <article> {socialCount}</article>
              </section>
              <section className="col-md-3 col-sm-3">
                <article className="col-md-12 date">
                  Last updated on&nbsp;
                  <br />
                  <span>
                    <i className="fa fa-calendar" /> &nbsp;
                    {moment(lastUpdatedAt, "YYYY-MM-DD").format("DD-MMM-YYYY")}
                  </span>
                </article>
              </section>
            </article>
            <article className="row flex-item">
              <article className="moboCellOrderIn">
                <section className="col-md-9 col-sm-9 flex-item">
                  <article className="panel boxPadding posR">
                    {scholarshipBodyMultilinguals.length > 0 &&
                      scholarshipBodyMultilinguals[0].introduction ? (
                        <article className="data-row">
                          <h2
                            dangerouslySetInnerHTML={{
                              __html:
                                "What is " +
                                gblFunc.replaceWithLoreal(scholarshipName) +
                                "?"
                            }}
                          />

                          <div
                            dangerouslySetInnerHTML={this.rawMarkUp(
                              scholarshipBodyMultilinguals[0].introduction
                            )}
                          />
                        </article>
                      ) : null}
                    {/* {scholarshipMultilinguals.length > 0 &&
                    scholarshipMultilinguals[0].purposeAward ? (
                      <article className="data-row">
                        <h2>Purpose and awards </h2>
                        <div
                          dangerouslySetInnerHTML={this.rawMarkUp(
                            scholarshipMultilinguals[0].purposeAward
                          )}
                        />
                      </article>
                    ) : (
                      ""
                    )} */}
                    {scholarshipBodyMultilinguals.length > 0 &&
                      scholarshipBodyMultilinguals[0].offeredBy ? (
                        <article className="data-row">
                          <h2>Who is offering this scholarship ?</h2>
                          <div
                            dangerouslySetInnerHTML={this.rawMarkUp(
                              scholarshipBodyMultilinguals[0].offeredBy
                            )}
                          />
                        </article>
                      ) : (
                        ""
                      )}
                    {scholarshipBodyMultilinguals.length > 0 &&
                      scholarshipBodyMultilinguals[0].eligibility ? (
                        <article className="data-row">
                          <h2>Who can apply for this scholarship ?</h2>
                          <div
                            dangerouslySetInnerHTML={this.rawMarkUp(
                              scholarshipBodyMultilinguals[0].eligibility
                            )}
                          />
                        </article>
                      ) : (
                        ""
                      )}
                    {/* Start International study content banner*/}
                    {isInternational && this.admissionBanner()}

                    {scholarshipBodyMultilinguals.length > 0 &&
                      scholarshipBodyMultilinguals[0].benifit ? (
                        <article className="data-row">
                          <h2>What are the benefits ?</h2>
                          <div
                            dangerouslySetInnerHTML={this.rawMarkUp(
                              scholarshipBodyMultilinguals[0].benifit
                            )}
                          />
                        </article>
                      ) : (
                        ""
                      )}

                    {scholarshipBodyMultilinguals.length > 0 &&
                      scholarshipBodyMultilinguals[0].howToApply ? (
                        <article className="data-row">
                          <h2>How can you apply ?</h2>
                          <div
                            dangerouslySetInnerHTML={this.rawMarkUp(
                              scholarshipBodyMultilinguals[0].howToApply
                            )}
                          />
                        </article>
                      ) : (
                        ""
                      )}
                    {scholarshipBodyMultilinguals.length > 0 &&
                      scholarshipBodyMultilinguals[0].benefit ? (
                        <article className="data-row">
                          <h2>What are the benefits ?</h2>
                          <div
                            dangerouslySetInnerHTML={this.rawMarkUp(
                              scholarshipBodyMultilinguals[0].benefit
                            )}
                          />
                        </article>
                      ) : (
                        ""
                      )}
                    {scholarshipBodyMultilinguals.length > 0 &&
                      scholarshipBodyMultilinguals[0].required_document ? (
                        <article className="data-row">
                          <h2
                            dangerouslySetInnerHTML={{
                              __html:
                                "What are the documents required for the " +
                                gblFunc.replaceWithLoreal(scholarshipName) +
                                "?"
                            }}
                          />
                          <div
                            dangerouslySetInnerHTML={this.rawMarkUp(
                              scholarshipBodyMultilinguals[0].required_document
                            )}
                          />
                        </article>
                      ) : (
                        ""
                      )}
                    {scholarshipBodyMultilinguals.length > 0 &&
                      scholarshipBodyMultilinguals[0].selectionCriteria ? (
                        <article className="data-row">
                          <h2>What are the selection criteria ?</h2>
                          <div
                            dangerouslySetInnerHTML={this.rawMarkUp(
                              scholarshipBodyMultilinguals[0].selectionCriteria
                            )}
                          />
                        </article>
                      ) : (
                        ""
                      )}
                    {scholarshipBodyMultilinguals.length > 0 &&
                      scholarshipBodyMultilinguals[0].importantDates ? (
                        <article className="data-row">
                          <h2>What are the important dates ?</h2>
                          <div
                            dangerouslySetInnerHTML={this.rawMarkUp(
                              scholarshipBodyMultilinguals[0].importantDates
                            )}
                          />
                        </article>
                      ) : (
                        ""
                      )}

                    {scholarshipBodyMultilinguals.length > 0 &&
                      scholarshipBodyMultilinguals[0].faq ? (
                        <article className="data-row">
                          <h2>FAQs</h2>
                          <div
                            dangerouslySetInnerHTML={this.rawMarkUp(
                              scholarshipBodyMultilinguals[0].faq
                            )}
                          />
                        </article>
                      ) : (
                        ""
                      )}
                    {scholarshipBodyMultilinguals.length > 0 &&
                      scholarshipBodyMultilinguals[0].moreDetails ? (
                        <article className="data-row">
                          <h2>Terms and Conditions</h2>
                          <div
                            dangerouslySetInnerHTML={this.rawMarkUp(
                              scholarshipBodyMultilinguals[0].moreDetails
                            )}
                          />
                        </article>
                      ) : (
                        ""
                      )}
                    {scholarshipDocuments && scholarshipDocuments.length > 0 ? (
                      <article className="data-row">
                        <h2>Important documents</h2>
                        {scholarshipDocuments.map(doc => {
                          if (doc.fileManaged) {
                            return (
                              <p className="btnTag" key={doc.id}>
                                <a href={doc.fileManaged.uri} rel="nofollow">
                                  {gblFunc.replaceWithLoreal(doc.title, 1)}
                                </a>
                              </p>
                            );
                          }
                        })}
                      </article>
                    ) : (
                        ""
                      )}
                    {scholarshipWebsiteLinks &&
                      scholarshipWebsiteLinks.length > 0 ? (
                        <article className="data-row">
                          <article className="col-md-6 nopadding">
                            <h2>Important Links</h2>

                            <ul className="optional-links-text">
                              {scholarshipWebsiteLinks.map((link, index) => {
                                return (
                                  <li key={index}>
                                    <a
                                      href="javascript:void(0)"
                                      onClick={() =>
                                        this.importantLinksHandler(link.url)
                                      }
                                    >
                                      <strong>{link.type.name}</strong>
                                    </a>
                                    {/* <a href={link.url} target="_blank">
        <strong>{link.type.name}</strong>
      </a> */}
                                  </li>
                                );
                              })}
                            </ul>
                          </article>
                          <article className="col-md-6 nopadding">
                            <h2>Contact details</h2>

                            {scholarshipBodyMultilinguals.length > 0 &&
                              scholarshipBodyMultilinguals[0].contactDetails ? (
                                <article>
                                  <p
                                    dangerouslySetInnerHTML={this.rawMarkUp(
                                      scholarshipBodyMultilinguals[0].contactDetails
                                    )}
                                  />
                                </article>
                              ) : null}
                          </article>
                        </article>
                      ) : (
                        ""
                      )}
                    {scholarshipWebsiteLinks &&
                      scholarshipWebsiteLinks.length > 0 &&
                      (moment().isSameOrBefore(deadlineDate, "day") ||
                        !deadlineDate) ? (
                        <article className="data-row">
                          <article className="matchingscho">
                            <article className="col-md-6 nopadding">
                              <button
                                onClick={() => {
                                  this.setState(
                                    {
                                      redirectionUrl: scholarshipWebsiteLinks,
                                      redirectStatus: true,
                                      redirectionId: id
                                    },
                                    () => {
                                      this.gtmEvent(
                                        1,
                                        id,
                                        scholarshipWebsiteLinks,
                                        "apply now"
                                      );
                                    }
                                  );
                                }}
                              >
                                Apply Now
                            </button>
                            </article>
                          </article>
                        </article>
                      ) : null}

                    <TrackVisibility>
                      <FrequestAskedQuestion
                        onFaqHandler={this.onFaqHandler}
                        faqList={this.state.faqlist}
                        faqID={this.state.faqID}
                        onclickFaqHandler={this.onclickFaqHandler}
                      />
                    </TrackVisibility>
                  </article>

                  <article className="panel boxPadding disclaimer">
                    <article className="data-row">
                      <h2>Disclaimer</h2>
                      <p>
                        All the information provided here is for reference
                        purpose only. While we strive to list all scholarships
                        for benefit of students, Buddy4Study does not guarantee
                        the accuracy of the data published here. For official
                        information, please refer to the official website.{" "}
                        <Link to="/disclaimer">read more</Link>
                      </p>
                    </article>
                  </article>
                  {/* <TrackVisibility>
                    <TABS
                      activeTab={this.state.activeTab}
                      changeTab={this.changeTab}
                      onArchiveScholarshipTab={this.onArchiveScholarshipTab}
                      archive={this.state.archive}
                      scholars={this.state.scholars}
                      archive_failed_msg={this.state.archive_failed_msg}
                      scholar_failed_msg={this.state.scholar_failed_msg}
                    />
                  </TrackVisibility> */}

                  {/* Start Tags Panel */}

                  <article className="detailscate">
                    <TrackVisibility>
                      <RelatedScholarship
                        {...this.props}
                        onRelatedSchHandler={this.onRelatedSchHandler}
                      />
                    </TrackVisibility>
                  </article>
                  {mobo_featuredSch}
                  <article className="mobile-device">
                    {moboRelatedArticles}
                  </article>
                  {/* <article className="panel boxPadding disclaimer loginComment">
                    <article className="data-row">
                      <Link to="/disclaimer">Login to leave your comment</Link>
                    </article>
                  </article>
                  <article className="usercomment">
                    <article className="data-row">
                      <form autocapitalize="off">
                        <article className="groping">
                          <article className="imgcomment">
                            <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/COLLEGE-SCHOLARSHIPS.jpg" />
                          </article>
                          <article className="ctrl-wrapper">
                            <h5>Saurabh Kumar</h5>
                            <span> Need full information</span>
                            <input
                              type="button"
                              value="Like"
                              className="transparentbtn"
                            />
                            -
                            <input
                              type="button"
                              value="Reply"
                              className="transparentbtnR"
                            />
                            -
                            <input
                              type="button"
                              value="8w"
                              className="transparentbtnc"
                            />
                          </article>
                        </article>

                        <article className="groping">
                          <article className="imgcomment">
                            <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/COLLEGE-SCHOLARSHIPS.jpg" />
                          </article>
                          <article className="ctrl-wrapper">
                            <article className="form-group textAreaCrtlfaq">
                              <textarea
                                name="comment"
                                id="comment"
                                className="form-control"
                                placeholder="write your comment"
                              />
                              <label for="comment">Be the first comment</label>
                            </article>
                            <button
                              type="submit"
                              id="submit"
                              name="submit"
                              className="btn top"
                            >
                              Submit
                            </button>
                          </article>
                        </article>
                      </form>
                    </article>
                  </article> */}
                </section>
                <section className="col-md-3 col-sm-3 flex-item">
                  <section className="panel ">
                    <section className="left-cell-1">
                      <Link to={`/scholarship/${slug}`}>
                        <img
                          property="image"
                          src={logoFid}
                          alt={scholarshipName}
                          title=""
                          className="school-logo"
                        />
                      </Link>
                      <article className="deadlineBox">
                        <dd>
                          <span>Deadline:</span>

                          {timeFormat}
                        </dd>

                        {scholarshipWebsiteLinks &&
                          scholarshipWebsiteLinks.length > 0 &&
                          (moment().isSameOrBefore(deadlineDate, "day") ||
                            !deadlineDate) ? (
                            <button
                              onClick={() => {
                                this.setState(
                                  {
                                    redirectionUrl: scholarshipWebsiteLinks,
                                    redirectStatus: true,
                                    redirectionId: id
                                  },
                                  () => {
                                    // if (!isMobile) {
                                    //   typeof window !== undefined &&
                                    //     window.open("https://admission.buddy4study.com/study-abroad/why-study-in-uk", '_blank', 'location=yes,height=100,width=100,scrollbars=yes,status=yes');
                                    // }
                                    this.gtmEvent(
                                      1,
                                      id,
                                      scholarshipWebsiteLinks,
                                      "apply now"
                                    )
                                  }
                                );
                              }}
                            >
                              Apply Now
                          </button>
                          ) : null}
                        {/* {userList.userId ? (
                          <p>
                            Add to Favorites
                            <a
                              href="javascript:void(0);"
                              className="likeLink"
                              onClick={() =>
                                addToFavIcon == "fa fa-heart-o"
                                  ? this.gtmEvent(3, id, userList, "addToFav")
                                  : null
                              }
                            >
                              <i
                                className={`${addToFavIcon}`}
                                          aria-hidden="true"
                                        />
                            </a>
                          </p>
                              ) : null} */}
                        {/* <p>
                          {scholarshipMultilinguals.length > 0 &&
                            scholarshipMultilinguals[0].viewCounter
                            ? scholarshipMultilinguals[0].viewCounter
                            : 0}
                          &nbsp;Views
                        </p> */}

                        {/* <p>
                      Share:
                      <a
                        href="javascript:void(0)"
                        onClick={() => this.gtmEvent(2)}
                      >
                        <FacebookShareButton url={this.pageUrl}>
                          <i className="fa fa-facebook" aria-hidden="true" />
                        </FacebookShareButton>
                      </a>
                      <a
                        href="javascript:void(0)"
                        onClick={() => this.gtmEvent(2)}
                      >
                        <TwitterShareButton url={this.pageUrl}>
                          <i className="fa fa-twitter" aria-hidden="true" />
                        </TwitterShareButton>
                      </a>
                      <a
                        href="javascript:void(0)"
                        onClick={() => this.gtmEvent(2)}
                      >
                        <LinkedinShareButton url={this.pageUrl}>
                          <i className="fa fa-linkedin" aria-hidden="true" />
                        </LinkedinShareButton>
                      </a>
                    </p> */}
                      </article>
                    </section>
                    <section className="left-cell-2">
                      <article className="cellBox">
                        {/* <!-- Start Award Eligibility Country panel --> */}
                        <article>
                          <dd>
                            <span className="eleAlign">
                              <i>Award</i>
                              {scholarshipMultilinguals.length > 0 &&
                                scholarshipMultilinguals[0].purposeAward ? (
                                  <i>
                                    {scholarshipMultilinguals[0].purposeAward}
                                  </i>
                                ) : null}
                            </span>
                          </dd>
                          <dd>
                            <span className="eleAlign">
                              <i>Eligibility</i>
                              {scholarshipMultilinguals.length > 0 &&
                                scholarshipMultilinguals[0].applicableFor ? (
                                  <i>
                                    {scholarshipMultilinguals[0].applicableFor}
                                  </i>
                                ) : null}
                            </span>
                          </dd>
                          <dd>
                            <span>
                              <i>Region</i>
                              {scholarshipSourceCountries &&
                                scholarshipSourceCountries.length > 0 &&
                                scholarshipSourceCountries.map(list => {
                                  return <i>{`${list.country.name}`}</i>;
                                })}
                            </span>

                            {/* {scholarshipSourceCountries.length > 0 &&
                              scholarshipSourceCountries[0].country
                              ? scholarshipSourceCountries[0].country.name
                            : null} */}

                            {scholarshipSourceCountries &&
                              scholarshipSourceCountries.length > 0 &&
                              scholarshipSourceCountries[0].country &&
                              scholarshipRules &&
                              Array.isArray(scholarshipRules) &&
                              scholarshipSourceCountries[0].country.name ==
                              "India"
                              ? scholarshipRules
                                .filter(item => item.rule.ruletype.id === 21)
                                .map(
                                  list =>
                                    list.rule.id !== 447 && (
                                      <li key={list.rule.ruleValue}>
                                        {list.rule.ruleValue}
                                      </li>
                                    )
                                )
                              : null}
                          </dd>
                        </article>

                        {/* <!-- Start Tags Panel --> */}
                        {featuredSch}
                        {/* <!-- Start Scholarhip provider contact Panel --> */}
                        {/* Audio List */}
                        {bsid === "BKS1" ? <AudioListKindScholarship /> : null}
                        {bsid === "IST1" ? <AudioListScholarshipExam /> : null}
                        {bsid === "HUS2" ? <AudioListHPUdaan /> : null}
                      </article>
                    </section>
                    {relatedArticles}
                  </section>
                </section>
              </article>
            </article>
          </section>
        </section>
      );
    }

    return (
      <section>
        <Helmet script={[schema]}>
          <title>
            {gblFunc.replaceWithLoreal(scholarshipResponse.scholarshipName, 1)}
          </title>
        </Helmet>
        {this.state.showLoginPopup ? (
          <UserLoginRegistrationPopup
            closePopup={this.closeLoginPopup}
            hideCloseButton={this.state.hideCloseButton}
            hideForgetCloseBtn={this.state.hideForgetCloseBtn}
            location={this.state.location}
          />
        ) : null}
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          isShow={this.state.isShowAlert}
          msg={this.state.alertMsg}
          close={this.hideAlert}
          status={this.state.statusAlert}
        />
        <section className="container-fluid row-bg equial-padding-b font-family details-page-wrapper">
          <section className="container">
            <section className="row">
              <section className="col-md-12">
                <ul className="breadcrumb">
                  <li>
                    <Link to="/">Home</Link>
                  </li>
                  <li>
                    <Link to="/scholarships">Scholarship</Link>
                  </li>
                  <li>
                    <a
                      property="url"
                      dangerouslySetInnerHTML={{
                        __html: gblFunc.replaceWithLoreal(
                          scholarshipResponse.scholarshipName
                        )
                      }}
                    />
                  </li>
                </ul>
              </section>
            </section>
            <section className="row">
              {this.props.staticContext ? (
                <h1>{this.props.staticContext.appData}</h1>
              ) : (
                  ""
                )}

              <section className="col-md-12">{scholarshipDetail}</section>
            </section>
          </section>
        </section>
        {/* Start NeedHelp Popup */}
        <section
          className={showModal ? "needHelpBtn posChange" : "needHelpBtn"}
        >
          {this.state.showButton ? (
            <span
              className={showModal ? "fadeOut" : "fadeIn"}
              onClick={this.showNeedHelp}
            />
          ) : null}
          {this.state.showModal ? (
            <NeedHelp
              onClose={this.showNeedHelp}
              submitContact={data => this.submitContact(data)}
            />
          ) : null}
        </section>

        {/* Start Comment Popup */}
        <section className="commentOverLay">
          <section className="c-winPopup">
            <i className="c-closeBtn" />
            <h5>Add Comment</h5>
            <article className="ctrl-wrapper">
              <article className="form-group">
                <input
                  type="text"
                  name=""
                  id=""
                  className="form-control"
                  placeholder="Add Headline"
                  required=""
                />
              </article>
              <article className="form-group textAreaCrtl">
                <textarea
                  name=""
                  id=""
                  className="form-control"
                  placeholder="Add Your Comments"
                />
              </article>
              <button type="submit">Submit</button>
            </article>
          </section>
        </section>
        <ReportPopup
          isOpen={this.state.isReportPopUpOpen}
          close={this.closeReportPopup}
        />
      </section>
    );
  }
}

const TABS = ({
  activeTab,
  changeTab,
  isVisible,
  onArchiveScholarshipTab,
  archive,
  scholars,
  archive_failed_msg,
  scholar_failed_msg
}) => {
  isVisible ? onArchiveScholarshipTab() : null;
  return (
    <article className=" boxPadding disclaimer">
      <article className="scholarshipnew">
        <ul className="optional-links">
          <li
            className={activeTab == 1 ? "active" : ""}
            onClick={() => changeTab(1)}
          >
            Archives Scholarships
          </li>
          <li
            className={activeTab == 2 ? "active" : ""}
            onClick={() => changeTab(2)}
          >
            Scholars
          </li>
          {/* <li
            className={activeTab == 3 ? "active" : ""}
            onClick={() => changeTab(3)}
          >
            Events & Media
          </li> */}
        </ul>
        <TAB
          activeTab={activeTab}
          archive={archive}
          scholars={scholars}
          archive_failed_msg={archive_failed_msg}
          scholar_failed_msg={scholar_failed_msg}
        />
      </article>
    </article>
  );
};

const TAB = ({
  activeTab,
  archive,
  scholars,
  archive_failed_msg,
  scholar_failed_msg
}) => {
  let onTabBasis = null;
  switch (activeTab) {
    case 1:
      onTabBasis = (
        <article className="sholarship-list">
          {archive && archive.scholarships && archive.scholarships.length ? (
            archive.scholarships.map((arc, index) => {
              let daysToGO;
              // let viewCounter;
              // if (props.activeScholarship) {

              daysToGO =
                parseInt(
                  moment(arc.deadlineDate, "YYYY-MM-DD").diff(
                    moment().startOf("day"),
                    "days"
                  )
                ) + 1;
              // }

              let daysLabel;
              if (daysToGO === 1) {
                daysLabel = "Last";
              } else if (daysToGO === 2) {
                daysLabel = "1";
              } else {
                daysLabel = daysToGO;
              }

              // if (
              //   arc.scholarshipMultilinguals &&
              //   arc.scholarshipMultilinguals[0] &&
              //   arc.scholarshipMultilinguals[0].viewCounter
              // ) {
              //   viewCounter = arc.scholarshipMultilinguals[0].viewCounter;
              // }

              return (
                <article className="box" key={arc.id}>
                  <article className="data-row flex-container">
                    <article className="col-md-2 col-sm-12 flex-item">
                      <article className="tablecell">
                        <article className="logotext">
                          <img
                            src={`${imgBaseUrl}b4scal.png`}
                            alt="scholarship-icon"
                          />

                          <p>
                            {/* {parseInt(viewCounter)
                              ? `${
                              parseInt(viewCounter) > 1
                                ? parseInt(viewCounter)
                                : "view"
                              } views`
                              : 0 + "view"} */}
                            {arc.deadlineDate.split("-")[0]}
                          </p>
                        </article>
                      </article>
                    </article>
                    <article className="col-md-7 col-sm-9 flex-item">
                      <article className="tablecell">
                        <article className="contentdisplay">
                          <h2>
                            <Link
                              to={`/scholarship/${arc.slug}`}
                              className="ellipsis"
                            >
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: arc.scholarshipName
                                    ? gblFunc.replaceWithLoreal(
                                      arc.scholarshipName
                                    )
                                    : ""
                                }}
                              />
                            </Link>
                            <article className="namelistTooltip">
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: arc.scholarshipName
                                    ? gblFunc.replaceWithLoreal(
                                      arc.scholarshipName
                                    )
                                    : ""
                                }}
                              />
                              <i className="arrow" />
                            </article>
                          </h2>
                          <p>
                            <img
                              src={`${imgBaseUrl}scholarship-icon.png`}
                              alt="scholarship-icon"
                            />
                            {arc.scholarshipMultilinguals.length > 0
                              ? arc.scholarshipMultilinguals[0].applicableFor
                              : ""}
                          </p>
                          <p>
                            <img
                              src={`${imgBaseUrl}awards-icon.png`}
                              alt="awards-icon"
                            />
                            {arc.scholarshipMultilinguals.length > 0
                              ? arc.scholarshipMultilinguals[0].purposeAward
                              : ""}
                          </p>
                        </article>
                      </article>
                    </article>
                    {/* {daysToGO >= 1 ? (
                        arc.deadlineDate ? (
                          <article className="col-md-3 col-sm-3 flex-item">
                            <article className="tablecell">
                              {daysToGO < 17 ? (
                                <article
                                  className={
                                    daysToGO < 9
                                      ? "daysgo pink"
                                      : "daysgo yellow"
                                  }
                                >
                                  <span>{daysLabel}</span>
                                  <p>{`day${daysToGO > 2 ? "s" : ""} to go`}</p>
                                </article>
                              ) : (
                                <article className="calender">
                                  <p className="text-center">
                                    Last Date to apply
                                  </p>
                                  <dd>
                                    {moment(arc.deadlineDate).format("D")}
                                  </dd>
                                  <p className="text-center date">
                                    {moment(arc.deadlineDate).format("MMM, YY")}
                                  </p>
                                </article>
                              )}
                            </article>
                          </article>
                        ) : (
                          ""
                        )
                      ) : arc.deadlineDate ? (
                        <article className="col-md-3 col-sm-3 flex-item">
                          <article className="tablecell">
                            <article className="daysgo">
                              <Link to={`/scholarship-result/${arc.slug}`}>
                                Result
                              </Link>
                            </article>
                          </article>
                        </article>
                      ) : (
                        <article className="col-md-3 col-sm-3 flex-item">
                          <article className="tablecell">
                            <article className="daysgo pink always">
                              <p>Always Open</p>
                            </article>
                          </article>
                        </article>
                      )} */}
                    <article className="col-md-3 col-sm-3 flex-item">
                      <article className="tablecell">
                        <article className="daysgo">
                          <Link to={`/scholarship-result/${arc.slug}`}>
                            Result
                          </Link>
                        </article>
                      </article>
                    </article>
                  </article>
                </article>
              );
            })
          ) : archive_failed_msg ? (
            <p className="heading">Something went wrong, please try again!</p>
          ) : (
                <p className="heading">No scholarship found.</p>
              )}
        </article>
      );

      break;
    case 2:
      if (scholars && scholars.length) {
        onTabBasis = scholars.map(scholar => (
          <section className="col-md-4" key={scholar.id}>
            <article className="profile">
              <img
                src={`${
                  scholar.pic
                    ? scholar.pic
                    : "https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/profile-img.png"
                  }`}
              />
              <article className="contentWrapper">
                <h5>
                  {scholar.firstName} {scholar.lastName}
                </h5>
                <p>{scholar.description}</p>
                {/* <p> Indian Institute Of Management Ahmedabad</p>
          <p> Scholarship – 2018</p> */}
              </article>
            </article>
          </section>
        ));
      } else {
        onTabBasis = scholar_failed_msg ? (
          <p className="heading">Something went wrong, please try again!</p>
        ) : (
            <p className="heading">No scholar found.</p>
          );
      }
      break;
    case 3:
      onTabBasis = (
        <section className="col-md-4">
          <article className="eventWrapper">
            <article className="newsevent">
              <article className="imgWrapper">
                <img
                  src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/income-based-scholarships.jpg"
                  alt=""
                />
                <article className="detail">
                  <dd>November 8, 2018</dd>
                  <h3>
                    Buddy4Study Raises $3 Mn in Series A funding From CBA
                    Capital
                  </h3>
                  <p>
                    Buddy4Study Raises $3 Mn in Series A funding From CBA
                    Capital
                  </p>
                  <a>Read More</a>
                </article>
              </article>
            </article>
          </article>
        </section>
      );
      break;
  }
  return <section className="borderbox">{onTabBasis}</section>;
};

const FrequestAskedQuestion = props => {
  let faqTemplate = null;
  props.isVisible ? props.onFaqHandler() : null;

  if (props.faqList && props.faqList.length) {
    faqTemplate = (
      <article className="data-row">
        <section className="faqs">
          <h3>Frequently Asked Questions </h3>
          <section className="accordion">
            {props.faqList.map(list => (
              <article className="accordion-item " key={list.id}>
                <h4 onClick={() => props.onclickFaqHandler(list.id)}>
                  <i
                    className={`fa ${
                      props.faqID && props.faqID == list.id
                        ? "fa-minus"
                        : "fa-plus"
                      }`}
                  />{" "}
                  {list.title}
                </h4>
                {props.faqID && props.faqID == list.id ? (
                  <article className="content">
                    <p>{list.detail}</p>
                  </article>
                ) : null}
              </article>
            ))}
          </section>
        </section>
      </article>
    );
  }
  return <section>{faqTemplate}</section>;
};

const RelatedScholarship = props => {
  let Articles = null;

  props.isVisible ? props.onRelatedSchHandler() : null;

  Articles =
    props.relatedSch &&
      props.relatedSch.scholarships &&
      props.relatedSch.scholarships.length > 0 ? (
        <div>
          <h2>Related scholarships</h2>
          {props.relatedSch.scholarships.map(item => {
            let daysToGO;
            let viewCounter;
            // if (props.activeScholarship) {

            daysToGO =
              parseInt(
                moment(item.deadlineDate, "YYYY-MM-DD").diff(
                  moment().startOf("day"),
                  "days"
                )
              ) + 1;
            // }

            let daysLabel;
            if (daysToGO === 1) {
              daysLabel = "Last";
            } else if (daysToGO === 2) {
              daysLabel = "1";
            } else {
              daysLabel = daysToGO;
            }

            if (
              item.scholarshipMultilinguals &&
              item.scholarshipMultilinguals[0] &&
              item.scholarshipMultilinguals[0].viewCounter
            ) {
              viewCounter = item.scholarshipMultilinguals[0].viewCounter;
            }
            return (
              <article className="col-md-4" key={item.id}>
                <article className="relatedCat">
                  <article className="logotext">
                    <Link to={`/scholarship/${item.slug}`}>
                      <img
                        src={item.logoFid ? item.logoFid : defaultImageLogo}
                        onError={e => {
                          e.target.src = defaultImageLogo;
                        }}
                        className="img-responsive"
                        alt={item.scholarshipName}
                      />
                    </Link>
                  </article>

                  <article className="contentdisplay">
                    <h2>
                      <Link
                        // className="ellipsis"
                        to={`/scholarship/${item.slug}`}
                      >
                        <span
                          dangerouslySetInnerHTML={{
                            __html: item.scholarshipName
                              ? gblFunc.replaceWithLoreal(item.scholarshipName)
                              : ""
                          }}
                        />
                      </Link>
                      <article className="namelistTooltip">
                        <span
                          dangerouslySetInnerHTML={{
                            __html: item.scholarshipName
                              ? gblFunc.replaceWithLoreal(item.scholarshipName)
                              : ""
                          }}
                        />
                        <i className="arrow" />
                      </article>
                    </h2>
                    <p>
                      <img
                        src={`${imgBaseUrl}scholarship-icon.png`}
                        alt="scholarship-icon"
                      />
                      {item.scholarshipMultilinguals &&
                        item.scholarshipMultilinguals.length > 0 &&
                        item.scholarshipMultilinguals[0].applicableFor}
                    </p>
                    <p>
                      <img
                        src={`${imgBaseUrl}awards-icon.png`}
                        alt="awards-icon"
                      />{" "}
                      {item.scholarshipMultilinguals &&
                        item.scholarshipMultilinguals.length > 0 &&
                        item.scholarshipMultilinguals[0].purposeAward}
                    </p>
                    {daysToGO >= 1 ? (
                      item.deadlineDate ? (
                        <article>
                          <article className="tablecell">
                            {daysToGO < 17 ? (
                              <article
                                className={
                                  daysToGO < 9 ? "daysgo pink" : "daysgo yellow"
                                }
                              >
                                <i className="fa fa-calendar calicon" /> &nbsp;
                              <span>
                                  {daysLabel}&nbsp;
                                {`day${daysToGO > 2 ? "s" : ""} to go`}
                                </span>
                              </article>
                            ) : (
                                <article className="calender">
                                  <p>
                                    <i className="fa fa-calendar calicon" /> &nbsp;
                                    Last date to apply&nbsp;
                                <span className="Cbold">
                                      {moment(item.deadlineDate).format("D")}&nbsp;
                                  {moment(item.deadlineDate).format("MMM, YY")}
                                    </span>
                                  </p>
                                </article>
                              )}
                          </article>
                        </article>
                      ) : (
                          ""
                        )
                    ) : item.deadlineDate ? (
                      <article>
                        <article className="tablecell">
                          <article className="daysgo">
                            <span>Closed</span>
                          </article>
                        </article>
                      </article>
                    ) : (
                          <article>
                            <article className="tablecell">
                              <article className="daysgo pink open">
                                <i className="fa fa-calendar calicon" /> &nbsp;
                          <span>Open</span>
                              </article>
                            </article>
                          </article>
                        )}
                    <p className="deadline">
                      <Link to={`/scholarship/${item.slug}`}>View details</Link>
                    </p>
                  </article>
                </article>
              </article>
            );
          })}
        </div>
      ) : null;
  return Articles;
};

export const AudioListHPUdaan = props => {
  return (
    <article className="audioWrapper">
      <dd>Audio</dd>
      <p>
        <span>
          Hindi &ndash; HP Udaan
          <audio
            controls
            preload="metadata"
            controlsList="download"
            src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/scholarships/audio/hindi-hp-udaan.m4a"
            type="audio/mp4"
          />
        </span>
        <span>
          English &ndash; HP Udaan
          <audio
            controls
            preload="metadata"
            controlsList="download"
            src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/scholarships/audio/english-hp-udaan.m4a"
            type="audio/mp4"
          />
        </span>
      </p>
    </article>
  );
};

export const AudioListKindScholarship = props => {
  return (
    <article className="audioWrapper">
      <dd>Audio</dd>
      <p>
        <span>
          Hindi &ndash; Kind Scholarship
          <audio
            controls
            preload="metadata"
            controlsList="download"
            src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/scholarships/audio/Kind_Scholarship_for_Young_Women_Hindi.mp3"
            type="audio/mp4"
          />
        </span>
        <span>
          English &ndash; Kind Scholarship
          <audio
            controls
            preload="metadata"
            controlsList="download"
            src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/scholarships/audio/Kind_Scholarship_for_Young_Women_English.mp3"
            type="audio/mp4"
          />
        </span>
      </p>
    </article>
  );
};

export const AudioListScholarshipExam = props => {
  return (
    <article className="audioWrapper">
      <dd>Audio</dd>
      <p>
        <span>
          Hindi &ndash; Scholarship Exam
          <audio
            controls
            preload="metadata"
            controlsList="download"
            src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/scholarships/audio/Scholarship_Exam_for_International_Higher_Education_2019_Hindi.mp3"
            type="audio/mp4"
          />
        </span>
        <span>
          English &ndash; Scholarship Exam
          <audio
            controls
            preload="metadata"
            controlsList="download"
            src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/scholarships/audio/Scholarship_Exam_for_International_Higher_Education_2019_English.mp3"
            type="audio/mp4"
          />
        </span>
      </p>
    </article>
  );
};

export default ScholarshipDetailPage;
