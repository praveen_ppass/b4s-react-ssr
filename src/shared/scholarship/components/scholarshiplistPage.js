import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link, Redirect } from "react-router-dom";
import Helmet from "react-helmet";
import Pagination from "rc-pagination";
import ScholarshipItem from "../../components/schoarshipitem";
import ScholarshipFilters from "./scholarshipFilters";
import { scholarshipLabelsMap } from "../../../constants/constants";
import { paramsList } from "../../../constants/schForParamsList";
import FeaturedScholarships from "../../components/featured-scholarship/featuredScholarship";
import Loader from "../../common/components/loader";
import { isAlphaNumericOnly } from "../../../validation/rules";
import { ruleRunner } from "../../../validation/ruleRunner";
import AlertMessage from "../../common/components/alertMsg";
import ScholarshipFiltersNew from "./filters";
import {
	FETCH_SCHOLARSHIPS_REQUESTED,
	FETCH_SCHOLARSHIP_BYUSID_REQUESTED,
	FETCH_SCHOLARSHIPS_SUCCEEDED,
	FETCH_SCHOLARSHIP_BYUSID_SUCCEEDED,
	FETCH_FACET_SCHOLARSHIP_SUCCEEDED
} from "../actions";
import { FETCH_RULES_SUCCEEDED } from "../../../constants/commonActions";
import { isMobile } from "react-device-detect";

class ScholarshipListPage extends Component {
	constructor(props) {
		super(props);
		this.resetFilters = this.resetFilters.bind(this);
		this.onFilterChange = this.onFilterChange.bind(this);
		this.onPageChange = this.onPageChange.bind(this);
		this.clearSelectedFilter = this.clearSelectedFilter.bind(this);
		this.applyFilters = this.applyFilters.bind(this);
		this.mapFiltersToApiCall = this.mapFiltersToApiCall.bind(this);
		this.alwaysOpenScholarships = this.alwaysOpenScholarships.bind(this);
		this.closedScholarships = this.closedScholarships.bind(this);
		this.nonClosedScholarships = this.nonClosedScholarships.bind(this);
		this.resetAndApplyFilters = this.resetAndApplyFilters.bind(this);
		this.showFilterButton = this.showFilterButton.bind(this);
		this.loadSchOnParamsHandler = this.loadSchOnParamsHandler.bind(this);
		this.goToScholarshipDetails = this.goToScholarshipDetails.bind(this);
		this.onListPageScroll = this.onListPageScroll.bind(this);
		this.toggleFilters = this.toggleFilters.bind(this);
		this.resetFilterDropDown = this.resetFilterDropDown.bind(this);
		this.hideAlert = this.hideAlert.bind(this);
		this.loadFacetBasedScholarship = this.loadFacetBasedScholarship.bind(this);
		this.setListTitle = this.setListTitle.bind(this);
		this.filterTabOpen = this.filterTabOpen.bind(this);
		this.onFilterChange2 = this.onFilterChange2.bind(this);
		this.resetAndApplyFiltersClose = this.resetAndApplyFiltersClose.bind(this);
		this.resetSelected=this.resetSelected.bind(this);

		this.state = {
			scholarshipList: null,
			scholarshipListTitle: null,
			mode: "OPEN",
			filterState: {
				RELIGION: [],
				GENDER: [],
				EDUCATION: [],
				SCHOLARSHIPS: [],
				COUNTRY: []
			},
			active: true,
			activeClose: false,
			activeAlwaysOpen: false,
			activeComingSoon: true,
			areFiltersPulledDown: false,
			appliedFilters: [],
			paramsList,
			ITEMS_PER_PAGE: 20,
			currentPage: 1,
			selectedFilters: [],
			showFilterButton: false,
			isSearchPage: false,
			isUSID: false,
			searchTerm: "",
			isShowAlert: false,
			alertMsg: "",
			statusAlert: false,
			showfilterTab: false,
			selected: [],
			countryListRules: []
		};
	}

	resetFilterDropDown() {
		this.setState({
			showFilterButton: false,
			areFiltersPulledDown: false
		});
	}
	hideAlert() {
		//  close popup

		this.setState({
			isShowAlert: false
		});
	}
	showFilterButton() {
		if (typeof window !== "undefined") {
			if (
				this.scholarshipListContainer &&
				!this.state.isSearchPage &&
				!this.state.isUSID
			) {
				const listPageTop = this.scholarshipListContainer.getBoundingClientRect()
					.top;
				if (window.scrollY <= listPageTop && this.state.showFilterButton) {
					this.setState({
						showFilterButton: false
					});
				} else if (
					window.scrollY > listPageTop &&
					!this.state.showFilterButton
				) {
					this.setState({
						showFilterButton: true
					});
				} else if (window.scrollY === 0) {
					this.resetFilterDropDown();
				}
			}
		}
	}
	/* Fetch scholarships with closed filter */

	closedScholarships() {
		this.setState(
			{
				active: false,
				activeClose: true,
				mode: "CLOSED",
				activeAlwaysOpen: false,
				activeComingSoon: false
			},
			() => this.applyFilters()
		);
	}
	alwaysOpenScholarships() {
		this.setState(
			{
				active: false,
				activeClose: false,
				mode: "ALWAYS_OPEN",
				activeAlwaysOpen: true,
				activeComingSoon: false
			},
			() => this.applyFilters()
		);
	}

	toggleFilters(event) {
		const { areFiltersPulledDown } = this.state;
		this.setState({
			areFiltersPulledDown: !areFiltersPulledDown
		});
	}

	onListPageScroll(event) {
		//   if (this.scholarshipListContainer) {
		//   }
	}

	/* Fetch scholarships with closed filter */

	nonClosedScholarships() {
		this.setState(
			{
				active: true,
				activeClose: false,
				activeAlwaysOpen: false,
				mode: "OPEN",
				activeComingSoon: true
			},
			() => this.applyFilters()
		);
	}

	applyFilters() {
		//Set current page to 1 before calculating new offset to reset pagination.
		let { loadScholarships } = this.props;
		let { ITEMS_PER_PAGE, mode, countryListRules } = this.state;
		let currentPage = 1;
		// Calculate offset based on new filter
		let page = this.getOffset(currentPage, ITEMS_PER_PAGE);
		let length = ITEMS_PER_PAGE;

		let appliedFilters = this.mapFiltersToApiCall();

		let scholarshipCallParams = {
			page,
			length,
			mode
		};

		if (appliedFilters.length > 0) {
			scholarshipCallParams.rules = appliedFilters;
		}
		if (countryListRules.length > 0) {
			scholarshipCallParams.countries = countryListRules;
		}

		this.setState(
			{
				appliedFilters,
				currentPage,
				showfilterTab: false
			},
			() => {
				loadScholarships(scholarshipCallParams);
			}
		);
	}

	resetAndApplyFilters() {
		this.setState({
			filterState: {
				RELIGION: [],
				GENDER: [],
				EDUCATION: [],
				SCHOLARSHIPS: [],
				COUNTRY: []
			},
			appliedFilters: [],
			selectedFilters: [],
			selected: [],
			countryListRules: []
		});
	}

	resetAndApplyFiltersClose() {
		this.setState(
			{
				filterState: {
					RELIGION: [],
					GENDER: [],
					EDUCATION: [],
					SCHOLARSHIPS: [],
					COUNTRY: []
				},
				appliedFilters: [],
				selectedFilters: [],
				selected: [],
				countryListRules: [],
				showfilterTab: !this.state.showfilterTab
			},
			() => this.applyFilters()
		);
	}

	/* Reset filters and frozen filters */
	resetFilters() {
		this.setState({
			filterState: {
				RELIGION: [],
				GENDER: [],
				EDUCATION: [],
				SCHOLARSHIPS: [],
				COUNTRY: []
			},
			active: true,
			appliedFilters: [],
			selectedFilters: []
		});
	}

	resetSelected(){
		this.setState({
			selected:[]
		})
	}

	/* On change of filter */

	async onFilterChange(e, TYPE, isChecked) {
		var b = {
			label: e.target.name,
			value: parseInt(e.target.value),
			TYPE: TYPE,
			checked: isChecked
		};
		if (e.target.checked) {
			await this.setState({
				selected: [...this.state.selected, b]
			});
		} else {
			var myArray = this.state.selected;

			//  var myArray = this.state.selected.filter(function( obj ) {
			//     return obj.value !== e.target.value;
			// });
			for (var i = myArray.length - 1; i >= 0; --i) {
				if (myArray[i].value == e.target.value) {
					myArray.splice(i, 1);
				}
			}
			// const result = this.state.selected.filter(({value}) => !value.includes(e.target.value));

			await this.setState({
				selected: myArray
			});
		}
		// selected.push(b)
		let filterState = Object.assign({}, this.state.filterState, {
			[TYPE]: this.state.selected
		});
		this.setState(
			{
				filterState
			}
			// () => this.applyFilters()
		);

		// this.applyFilters();
	}

	async onFilterChange2(e, isChecked) {
		if (!!e.target.checked) {
			await this.setState({
				countryListRules: [
					...this.state.countryListRules,
					parseInt(e.target.id)
				]
			});
		} else {
			var myArray2 = this.state.countryListRules;

			//  var myArray = this.state.selected.filter(function( obj ) {
			//     return obj.value !== e.target.value;
			// });
			for (var i = myArray2.length - 1; i >= 0; --i) {
				if (myArray2[i] == e.target.id) {
					myArray2.splice(i, 1);
				}
			}
			// const result = this.state.selected.filter(({value}) => !value.includes(e.target.value));

			await this.setState({
				countryList: myArray2
			});
		}
	}

	/* Load scholarship on base of getting parameter from url */

	loadSchOnParamsHandler(selected, TYPE) {
		let filterState = Object.assign({}, this.state.filterState, {
			[TYPE]: selected
		});

		this.setState(
			{
				filterState
			},
			() => this.applyFilters()
		);

		// this.setState(
		//   (previousState, currentProps) => {
		//     return { ...previousState, filterState: filterState };
		//   },
		//   () => this.applyFilters()
		// );
	}

	loadFacetBasedScholarship(selectedFilters) {
		let facetFilterState = Object.assign({}, this.state.filterState);

		for (let key in selectedFilters) {
			facetFilterState[key] = selectedFilters[key];
		}

		this.setState(
			{
				filterState: facetFilterState
			},
			() => {
				this.mapFiltersToApiCall("FACET-SCHOLARSHIP");
			}
		);
	}

	/* Map applied filters to Api Call */

	mapFiltersToApiCall(TYPE_FILTERED) {
		// Freeze filters with the following format, to make API calls with the frozen filters
		// "rules":[{"rule":[110,111]},{"rule":[10,11,12,13]},{"rule":[90,91,92,93]}]
		let filterState = Object.assign({}, this.state.filterState);
		let rules = [];
		let schoolArray = [];
		for (let key in filterState) {
			let ruleFilters = filterState[key];
			if (ruleFilters.length > 0) {
				ruleFilters = ruleFilters.map(rule => {
					if (rule.value && rule.value.length > 0) {
						schoolArray = rule.value;
					} else {
						return rule.value;
					}
				});
				rules.push({
					rule: schoolArray.length > 0 ? schoolArray : ruleFilters
				});
			}
		}

		if (TYPE_FILTERED && TYPE_FILTERED == "FACET-SCHOLARSHIP") {
			this.setState({
				appliedFilters: rules
			});
		} else {
			return rules;
		}
	}

	/* Clear selected filter type */

	clearSelectedFilter(value, TYPE) {
		//Get filter state
		let filterState = Object.assign({}, this.state.filterState);
		//Get filter array based on type
		let filterArray = filterState[TYPE];
		//Remove value other than passed value (id)
		let filtered = filterArray.filter(filter => filter.value !== value);

		filterState[TYPE] = filtered;

		this.setState(
			{
				filterState
			},
			() => this.applyFilters()
		);
	}
	componentDidUpdate() {
		if (this.state.showFilterButton) {
			if (isMobile) {
				window.scrollTo(0, 300);
			} else if (typeof window !== "Undefined") {
				window.scrollTo(0, 157);
			}
		}
	}

	componentDidMount() {
		/******************************************************************** */
		if (this.props.history.location.search) {
			const urlParams = new URLSearchParams(this.props.history.location.search);
			const q = urlParams.get("q");
			if (q) {
				var searchTerm = unescape(q);
			}
			if (searchTerm !== this.state.searchTerm) {
				const { searchTermvalidation } = ruleRunner(
					searchTerm,
					"searchTermvalidation",
					"",
					isAlphaNumericOnly
				);

				if (!searchTermvalidation && q) {
					this.setState(
						{
							filterState: {
								RELIGION: [],
								GENDER: [],
								EDUCATION: [],
								SCHOLARSHIPS: [],
								COUNTRY: []
							},
							active: true,
							appliedFilters: [],
							selectedFilters: [],
							currentPage: 1,
							isSearchPage: true,
							searchTerm
						},
						() => {
							this.props.loadScholarships({
								searchText: decodeURIComponent(searchTerm)
							});
						},
						() => this.props.loadFeaturedScholarships()
					);
				} else if (searchTermvalidation) {
					this.setState({
						isShowAlert: true,
						statusAlert: false,
						alertMsg: searchTermvalidation + " in search string."
					});
				} else {
					this.props.loadScholarships({ search: "" });
					this.props.loadFeaturedScholarships();
				}
			}
		}
		/******************************************************************** */

		this.props.loadRules();
		this.props.loadCountry();

		this.setListTitle();
		// FIXME: Add in componentWillRecieveProps;
		typeof window !== "undefined"
			? window.addEventListener("scroll", this.showFilterButton)
			: "";
	}

	getLabelFromFilterRule(key, filterArray, TYPE) {
		// { label: "Class 3", value: 4, TYPE: "EDUCATION" }
		let setFilters = [];
		if (
			filterArray &&
			filterArray.length &&
			this.props.rulesList &&
			Object.keys(this.props.rulesList).length
		) {
			const ruleListArray = this.props.rulesList[key];

			if (ruleListArray && ruleListArray.length)
				filterArray.map(filteredID => {
					ruleListArray.map(rules => {
						if (rules.id == filteredID) {
							setFilters.push({
								label: rules.rulevalue,
								value: rules.id,
								TYPE: TYPE
							});
						}
					});
				});
			return setFilters;
		}
	}
	setListTitle() {
		const { params } = this.props.match;
		if (params.class) {
			this.setState({
				scholarshipListTitle: paramsList[params.class.toLowerCase()]["TITLE"]
			});
		}
	}
	filterTabOpen() {
		this.setState({
			showfilterTab: !this.state.showfilterTab
		});
	}

	componentWillReceiveProps(nextProps) {
		//TODO: Add check for page reload
		const { ITEMS_PER_PAGE, currentPage, mode, paramsList } = this.state;
		const { params } = this.props.match;
		this.setListTitle(params);
		let schParams = params && params.class ? params.class : null;
		let page = this.getOffset(currentPage, ITEMS_PER_PAGE);

		switch (nextProps.type) {
			case FETCH_RULES_SUCCEEDED:
				if (this.props.history.location.search) {
					const urlParams = new URLSearchParams(
						this.props.history.location.search
					);
					const q = urlParams.get("q");
					if (q) {
						var searchTerm = unescape(q);
						const { searchTermvalidation } = ruleRunner(
							searchTerm,
							"searchTermvalidation",
							"",
							isAlphaNumericOnly
						);
						if (!searchTermvalidation) {
							this.setState(
								{
									isSearchPage: true,
									searchTerm
								},
								() =>
									this.props.loadScholarships({
										searchText: decodeURIComponent(searchTerm)
									}),
								this.props.loadFeaturedScholarships()
							);
						} else {
							this.setState({
								isShowAlert: true,
								statusAlert: false,
								alertMsg: searchTermvalidation + " in search text."
							});
						}
					}
				} else if (params.filters) {
					this.props.fetchFacetSchls({ slug: params.filters });
				} else if (params.usid) {
					this.setState(
						{
							isUSID: true
						},
						() => this.props.fetchSchByUSID({ usid: params.usid })
					);
				} else if (
					!this.props.featuredScholarshipList ||
					!this.props.scholarshipList ||
					schParams
				) {
					if (schParams) {
						if (
							params &&
							params.class &&
							paramsList.hasOwnProperty(params.class.toLowerCase())
						) {
							const { TYPE, FILTER } = paramsList[schParams.toLowerCase()];
							this.loadSchOnParamsHandler(FILTER, TYPE);
							this.props.loadFeaturedScholarships();
						}
					} else {
						this.props.loadScholarships({ page, length: ITEMS_PER_PAGE, mode });
						this.props.loadFeaturedScholarships();
					}
				} else {
					this.props.loadScholarships({ page, length: ITEMS_PER_PAGE, mode });
					this.props.loadFeaturedScholarships();
				}

				break;
			case FETCH_FACET_SCHOLARSHIP_SUCCEEDED:
				const { scholarships, filters, title } = nextProps.facetSchResponse;
				let getFilteredIds = {};

				if (filters) {
					for (let key in filters) {
						switch (key) {
							case "class":
								filters[key].length
									? (getFilteredIds["EDUCATION"] = this.getLabelFromFilterRule(
										key,
										filters[key],
										"EDUCATION"
									))
									: null;

								break;
							case "religion":
								filters[key].length
									? (getFilteredIds["RELIGION"] = this.getLabelFromFilterRule(
										key,
										filters[key],
										"RELIGION"
									))
									: null;
								break;
							case "gender":
								filters[key].length
									? (getFilteredIds["GENDER"] = this.getLabelFromFilterRule(
										key,
										filters[key],
										"GENDER"
									))
									: null;

								break;
							case "special":
								filters[key].length
									? (getFilteredIds[
										"SCHOLARSHIPS"
									] = this.getLabelFromFilterRule(
										key,
										filters[key],
										"SCHOLARSHIPS"
									))
									: null;
								break;
							case "country":
								filters[key].length
									? (getFilteredIds["COUNTRY"] = this.getLabelFromFilterRule(
										key,
										filters[key],
										"COUNTRY"
									))
									: null;
								break;
							default:
								break;
						}
					}
				}

				if (scholarships.scholarships && scholarships.scholarships.length) {
					this.setState(
						{
							scholarshipList: scholarships.scholarships,
							scholarshipListTitle: title
						},
						() => {
							if (getFilteredIds && Object.keys(getFilteredIds).length) {
								this.loadFacetBasedScholarship(getFilteredIds);
							}
							this.props.loadFeaturedScholarships();
						}
					);
				}

				break;
			case FETCH_SCHOLARSHIP_BYUSID_SUCCEEDED:
				if (!nextProps.scholarshipListUsid) {
					this.props.history.push("/404");
					return;
				}
				this.setState(
					{
						scholarshipList: nextProps.scholarshipListUsid,
						isUSID: true
					},
					() => this.props.loadFeaturedScholarships()
				);
				break;
			default:
		}
	}

	getOffset(page) {
		return page - 1;
	}

	onPageChange(currentPage) {
		console.log(this.state.countryListRules);
		const {
			ITEMS_PER_PAGE,
			active,
			isSearchPage,
			isUSID,
			mode,
			countryListRules
		} = this.state;
		const { loadScholarships } = this.props;

		let page = this.getOffset(currentPage);

		const length = ITEMS_PER_PAGE;
		let rules = this.state.appliedFilters;
		let scholarshipCallParameters = {
			page,
			length,
			mode
		};

		//Only make request with rules property if it exists
		if (rules.length > 0) {
			scholarshipCallParameters.rules = rules;
		}
		if (countryListRules.length > 0) {
			scholarshipCallParameters.countries = countryListRules;
		}

		if (isSearchPage) {
			scholarshipCallParameters.searchText = decodeURIComponent(
				this.state.searchTerm
			);
		}

		loadScholarships(scholarshipCallParameters);

		this.setState({
			currentPage
		});
	}

	fetchFilteredData() {
		loadScholarships({ page, length, rules });
	}

	/* The component understand and shows these filters */
	mapRawFilterToComponentData(filtersObject) {
		const placeholder = "Select";

		let mappedFiltersArray = [];
		for (let key in filtersObject) {
			let TYPE = key;
			let filters = filtersObject[key];
			let options = filters.map(f => ({
				label: f.rulevalue,
				value: f.id,
				TYPE
			}));
			mappedFiltersArray.push({ placeholder, options, TYPE });
		}

		return mappedFiltersArray;
	}

	mapFilterDataToArray(filterRules) {
		let { gender, religion, foreign, special } = filterRules;
		let scholarships = special.filter(s => s.weight <= 7);
		let Class = this.props.rulesList.class;

		for (let scholarship of scholarships) {
			if (scholarshipLabelsMap.has(scholarship.ID)) {
				scholarship.rulevalue = scholarshipLabelsMap.get(scholarship.ID);
			}
		}

		//Value 698 is foreign - yes (map to international)
		//Change the rulevalue to international for foreign-yes
		foreign[0].rulevalue = "International";
		foreign[1].rulevalue = "National";
		const customizeGender = gender.filter(gList => gList["id"] !== 229);
		let filterObject = {
			Class,
			Gender: customizeGender,
			Religion: religion
			// Foreign: foreign,
		};

		let filterComponentData = this.mapRawFilterToComponentData(filterObject);
		return filterComponentData;
	}

	goToScholarshipDetails(slug) {
		this.props.history.push({
			pathname: `/scholarship/${slug}`
		});
	}

	render() {
		let scholarshipList = null;
		const { params } = this.props.match;
		if (params.usid && !this.props.scholarshipList) {
			scholarshipList = this.state.scholarshipList
				? this.state.scholarshipList
				: [];
		} else if (params.filters && !params.usid && !this.props.scholarshipList) {
			scholarshipList = this.state.scholarshipList
				? this.state.scholarshipList
				: [];
		} else {
			scholarshipList = this.props.scholarshipList
				? this.props.scholarshipList
				: [];
		}

		const numScholarships = this.props.numScholarships || 0;
		const { ITEMS_PER_PAGE, currentPage, paramsList } = this.state;
		if (
			params &&
			params.class &&
			!paramsList.hasOwnProperty(params.class.toLowerCase())
		) {
			return <Redirect to="/404" />;
		}

		let mappedFilters;

		if (this.props.rulesList) {
			mappedFilters = this.mapFilterDataToArray(this.props.rulesList);
		}

		var filterCount = 0;
		if (this.state.appliedFilters.length > 0) {
			filterCount =
				filterCount + parseInt(this.state.appliedFilters[0].rule.length);
		}
		if (!!this.state.countryListRules) {
			filterCount = filterCount + parseInt(this.state.countryListRules.length);
		}
		return (
			<section className="sholarship-list">
				<Loader isLoader={this.props.showLoader} />
				<AlertMessage
					isShow={this.state.isShowAlert}
					msg={this.state.alertMsg}
					close={this.hideAlert}
					status={this.state.statusAlert}
				/>
				{/* {this.state.showFilterButton ? (
          <article
            className={`filterBtn  ${
              this.state.areFiltersPulledDown ? "upFilter" : "downFilter"
              }`}
            onClick={this.toggleFilters}
          >
            <span>Filters</span>
            <i />
          </article>
        ) : null} */}

				{/* breacrum trail */}
				<section className="container-fluid row-bg font-family list">
					<section className="container">
						<ul className="breadcrumb">
							<li>
								<Link to="/">Home</Link>
							</li>
							<li>
								<a href="javascript:void(0);">
									{params.class
										? paramsList[params.class.toLowerCase()]["TITLE"]
										: params.usid || this.props.match.path === "/scholarships"
											? "Scholarships"
											: "Search Result"}
								</a>
							</li>
						</ul>
					</section>
				</section>

				{/* End */}
				<section className="container-fluid boxfliter">
					<section className="container">
						<section className="row">
							<article className="col-md-12">
								<article className="flitterRow">
									<h1 className="pull-left">
										<button className="apply-but" onClick={this.filterTabOpen}>
											Filters
                      <i className={filterCount > 0 ? "bulletIcon fa fa-circle" : ""} />
										</button>
										{/* {(this.state.appliedFilters.length > 0 ? parseInt(this.state.appliedFilters[0].rule.length) : 0) + (!!this.state.countryListRules ? parseInt(this.state.countryListRules.length) : 0)}<span> Fliters Applied</span> */}
										{/* {filterCount > 0 ? (
                      <span>{filterCount + " Filters Applied"}</span>
                    ) : (
                      ""
                    )} */}
									</h1>
									{!!numScholarships ? (
										<h2 className="pull-right leftmobile">
											{numScholarships} scholarships found.
                    </h2>
									) : (
											<h2 className="pull-right leftmobile">
												No scholarships found.
                    </h2>
										)}
								</article>
							</article>
						</section>
						<section id="someID" className="row">
							{this.state.showfilterTab ? (
								<ScholarshipFiltersNew
									filterTabOpen={this.filterTabOpen}
									scholarshipFilters={mappedFilters}
									numScholarships={numScholarships}
									resetSelected={this.resetSelected}
									onFilterChange={this.onFilterChange}
									onFilterChange2={this.onFilterChange2}
									countryListRules={this.state.countryListRules}
									selected={this.state.selectedFilters}
									filterState={this.state.filterState}
									resetFilters={this.resetFilters}
									applyFilters={this.applyFilters}
									resetAndApplyFilters={this.resetAndApplyFilters}
									resetAndApplyFiltersClose={this.resetAndApplyFiltersClose}
									areFiltersPulledDown={this.state.areFiltersPulledDown}
									clearSelected={this.clearSelectedFilter}
									countryList={this.props.countryList}
									title={
										this.state.scholarshipListTitle ||
										"All Scholarships for Students"
									}
								/>
							) : (
									""
								)}
							{/* <ScholarshipFiltersNew /> */}
						</section>
					</section>
				</section>

				{/* list of scholarship */}
				<section className="list marginBox">
					<article className="container">
						<article className="row flex-container-wrap">
							{/* button panel */}
							{this.state.isSearchPage || this.state.isUSID ? null : (
								<article className="col-md-12 col-sm-12 noPaddingFromLeft">
									<article className="pull-left buttonpanel">
										<span
											onClick={this.nonClosedScholarships}
											className={`buttonleft ${
												this.state.activeComingSoon ? "activebutton" : ""
												}`}
										>
											Closing soon
                    </span>
										<span
											onClick={this.closedScholarships}
											className={`buttonmiddle ${
												this.state.activeClose ? "activebutton" : ""
												}`}
										>
											Closed
                    </span>
										<span
											onClick={this.alwaysOpenScholarships}
											className={`buttonright ${
												this.state.activeAlwaysOpen ? "activebutton" : ""
												}`}
										>
											Always open
                    </span>
									</article>
									<article className="pull-right buttonpanel hide sorting">
										<span>Sort by:</span>
										<span className="radius ase" />
										<span
											className="buttonright radius"
											onClick={this.resetAndApplyFilters}
										>
											Reset
                    </span>
									</article>
								</article>
							)}

							<article
								ref={ref => (this.scholarshipListContainer = ref)}
								className="col-md-9 col-sm-9 col-xs-12 marginRight flex-container-list"
							>
								{scholarshipList.length ? (
									<ScholarshipItem
										scholarshipList={scholarshipList}
										activeScholarship={this.state.active}
										goToScholarshipDetails={this.goToScholarshipDetails}
									/>
								) : this.props.scholarshipList &&
									this.props.scholarshipList.length === 0 ? (
											"No Scholarship Found."
										) : (
											""
										)}
								{this.state.isSearchPage || this.state.isUSID ? null : (
									<article className="pagination">
										{numScholarships > 20 ? (
											<Pagination
												defaultPageSize={ITEMS_PER_PAGE}
												pageSize={ITEMS_PER_PAGE}
												defaultCurrent={1}
												current={currentPage}
												onChange={this.onPageChange}
												total={numScholarships}
												showTitle={false}
											/>
										) : null}
									</article>
								)}
							</article>
							{/* right panel */}
							<article className="col-md-3 col-sm-3 col-xs-12 marginLeft flex-container-list">
								<FeaturedScholarships
									headerTitle="Featured scholarships"
									classes={["mobo-hide"]}
									fromListPage
									featured={this.props.featuredScholarshipList || []}
								/>
							</article>
						</article>
					</article>
				</section>
			</section>
		);
	}
}

export default ScholarshipListPage;
