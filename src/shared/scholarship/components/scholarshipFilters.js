import React from "react";
import Select from "react-select";

const ScholarshipFilters = props => {
  let {
    numScholarships,
    scholarshipFilters,
    onFilterChange,
    selected,
    clearSelected,
    filterState,
    resetFilters,
    applyFilters,
    resetAndApplyFilters,
    title,
    areFiltersPulledDown
  } = props;

  scholarshipFilters = scholarshipFilters || [];

  return (
    <section
      className={`container-fluid filter ${
        areFiltersPulledDown ? "filterDown" : "filterUp resetPos"
        }`}
    >
      <section className="container">
        <section className="row">
          <article className="col-md-12">
            <article>
              <h1 className="pull-left">{title}</h1>
              <h2 className="pull-right leftmobile">{`${
                numScholarships ? numScholarships : "0"
                } scholarships found.`}</h2>
            </article>

            <article className="float-none">
              <h3>Select Filters</h3>
            </article>

            <article className="float-none filterLabelPos">
              <SelectedFilters
                selected={filterState}
                clearAll={resetAndApplyFilters}
                clearSelected={clearSelected}
              />

              <ScholarshipFilterList
                filterList={scholarshipFilters}
                filterState={filterState}
                onFilterChange={onFilterChange}
              />

              <span className="apply-but" onClick={applyFilters}>
                Apply
              </span>
            </article>
          </article>
        </section>
      </section>
    </section>
  );
};

const ScholarshipFilterList = props => {
  const { filterList, onFilterChange, filterState } = props;
  console.log(filterList, filterState)
  return (
    <article>
      {filterList.map(filter => (
        <Select
          key={filter.TYPE}
          options={filter.options}
          multi
          closeOnSelect={false}
          value={filterState[filter.TYPE]}
          valueComponent={() => null}
          removeSelected={true}
          placeholder={filter.placeholder}
          onChange={selected => onFilterChange(selected, filter.TYPE)}
        />
      ))}
    </article>
  );
};

const SelectedFilters = props => {
  let { selected, clearAll, clearSelected } = props;

  let selectedValues = [];
  let mappedSelectedValues = [];
  for (let key in selected) {
    if (selected[key].length > 0) {
      selected[key].map(selectedFilter => {
        mappedSelectedValues.push({
          label: selectedFilter.label,
          value: selectedFilter.value,
          TYPE: selectedFilter.TYPE
        });
      });

      selectedValues = mappedSelectedValues;
    }
  }
  if (selectedValues.length > 0) {
    return (
      <article className="keyword">
        {selectedValues.map(item => (
          <span
            key={item.value}
            onClick={() => clearSelected(item.value, item.TYPE)}
          >
            {item.label}
          </span>
        ))}
        <span className="apply-but" onClick={clearAll}>Clear All</span>
      </article>
    );
  } else {
    return <article className="keyword" />;
  }
};
export default ScholarshipFilters;
