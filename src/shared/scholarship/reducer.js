import {
  FETCH_SCHOLARSHIPS_SUCCEEDED,
  FETCH_SCHOLARSHIPS_REQUESTED,
  FETCH_SCHOLARSHIPS_FAILED,
  FETCH_FEATURED_SCHOLARSHIPS_SUCCEEDED,
  FETCH_FEATURED_SCHOLARSHIPS_REQUESTED,
  FETCH_FEATURED_SCHOLARSHIPS_FAILED,
  FETCH_SCHOLARSHIP_DETAILS_REQUESTED,
  FETCH_SCHOLARSHIP_DETAILS_SUCCEEDED,
  FETCH_SCHOLARSHIP_DETAILS_FAILED,
  FETCH_CLOSED_SCHOLARSHIPS_SUCCEEDED,
  FETCH_CLOSED_SCHOLARSHIPS_REQUESTED,
  FETCH_CLOSED_SCHOLARSHIPS_FAILED,
  FETCH_AWARDEES_SCHOLARSHIPS_REQUESTED,
  FETCH_AWARDEES_SCHOLARSHIPS_SUCCEEDED,
  FETCH_AWARDEES_SCHOLARSHIPS_FAILED,
  FETCH_AWARDEES_DETAILS_SCHOLARSHIPS_REQUESTED,
  FETCH_AWARDEES_DETAILS_SCHOLARSHIPS_SUCCEEDED,
  FETCH_AWARDEES_DETAILS_SCHOLARSHIPS_FAILED,
  FETCH_AWARDEES_FILTER_REQUESTED,
  FETCH_AWARDEES_FILTER_SUCCEEDED,
  FETCH_AWARDEES_FILTER_FAILED,
  HIT_COUNTER_SCHOLARSHIP_DETAILS_REQUESTED,
  HIT_COUNTER_SCHOLARSHIP_DETAILS_SUCCEEDED,
  HIT_COUNTER_SCHOLARSHIP_DETAILS_FAILED,
  APPLY_NOW_HIT_COUNTER_REQUESTED,
  APPLY_NOW_HIT_COUNTER_SUCCEEDED,
  APPLY_NOW_HIT_COUNTER_FAILED,
  FETCH_SCHOLARSHIP_BYUSID_FAILED,
  FETCH_SCHOLARSHIP_BYUSID_SUCCEEDED,
  FETCH_SCHOLARSHIP_BYUSID_REQUESTED,
  FETCH_RELATED_SCHOLARSHIP_FAILED,
  FETCH_RELATED_SCHOLARSHIP_SUCCEEDED,
  FETCH_RELATED_SCHOLARSHIP_REQUESTED,
  FETCH_RELATED_ARTICLE_REQUESTED,
  FETCH_RELATED_ARTICLE_SUCCEEDED,
  FETCH_RELATED_ARTICLE_FAILED,
  FETCH_FAV_SCHOLARSHIP_REQUESTED,
  FETCH_LIKE_FOLLOW_REQUESTED,
  FETCH_LIKE_FOLLOW_SUCCEEDED,
  FETCH_LIKE_FOLLOW_FAILED,
  FETCH_FAQ_REQUESTED,
  FETCH_FAQ_SUCCEEDED,
  FETCH_FAQ_FAILED,
  FETCH_ARCHIVE_REQUESTED,
  FETCH_ARCHIVE_SUCCEEDED,
  FETCH_ARCHIVE_FAILED,
  FETCH_SCHOLAR_REQUESTED,
  FETCH_SCHOLAR_SUCCEEDED,
  FETCH_SCHOLAR_FAILED,
  FETCH_EVENT_MEDIA_REQUESTED,
  FETCH_EVENT_MEDIA_SUCCEEDED,
  FETCH_EVENT_MEDIA_FAILED,
  ADD_LIKE_FOLLOW_FAILED,
  ADD_LIKE_FOLLOW_SUCCEEDED,
  ADD_LIKE_FOLLOW_REQUESTED,
  FETCH_FACET_SCHOLARSHIP_REQUESTED,
  FETCH_FACET_SCHOLARSHIP_SUCCEEDED,
  FETCH_FACET_SCHOLARSHIP_FAILED,
  FETCH_COUNTRY_REQUESTED,
  FETCH_COUNTRY_SUCCEEDED,
  FETCH_COUNTRY_FAILED

} from "./actions";

import {
  FETCH_CONTACTUS_REQUESTED,
  FETCH_CONTACTUS_SUCCEEDED,
  FETCH_CONTACTUS_FAILED,
  FETCH_RULES_REQUESTED,
  FETCH_RULES_SUCCEEDED,
  FETCH_RULES_FAILED
} from "../../constants/commonActions";

import {
  // Persist Fav
  FETCH_FAV_PERSIST_REQUEST,
  FETCH_FAV_PERSIST_FAIL
} from "../dashboard/actions";
import {
  LOGIN_USER_SUCCEEDED,
  SOCIAL_LOGIN_USER_SUCCEEDED
} from "../login/actions";

const initialState = {
  showLoader: true,
  isError: false,
  errorMessage: ""
};

const scholarshipReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_SCHOLARSHIPS_REQUESTED:
      return Object.assign({}, state, {
        scholarshipList: null,
        showLoader: true,
        type
      });

    case FETCH_SCHOLARSHIPS_SUCCEEDED:
      const { total, scholarships } = payload;
      let scholarshipList = scholarships;

      let newState = {
        scholarshipList,
        showLoader: false,
        type
      };

      newState.numScholarships = total;

      return Object.assign({}, state, newState);

    case FETCH_SCHOLARSHIPS_FAILED:
      return payload;

    case FETCH_FEATURED_SCHOLARSHIPS_REQUESTED:
      return Object.assign({}, state, {
        featuredScholarshipList: [],
        showLoader: true,
        type
      });

    case FETCH_FEATURED_SCHOLARSHIPS_SUCCEEDED:
      return Object.assign({}, state, {
        featuredScholarshipList: payload,
        showLoader: false,
        type
      });

    case FETCH_FEATURED_SCHOLARSHIPS_FAILED:
      return payload;

    case FETCH_CLOSED_SCHOLARSHIPS_REQUESTED:
      return Object.assign({}, state, {
        closedScholarshipList: [],
        showLoader: true,
        type
      });

    case FETCH_CLOSED_SCHOLARSHIPS_SUCCEEDED:
      return Object.assign({}, state, {
        closedScholarshipList: payload.BODY.DATA,
        showLoader: false,
        type
      });

    case FETCH_CLOSED_SCHOLARSHIPS_FAILED:
      return payload;

    case FETCH_SCHOLARSHIP_DETAILS_REQUESTED:
      return Object.assign({}, state, {
        scholarshipDetail: {},
        showLoader: true,
        type
      });

    case FETCH_SCHOLARSHIP_DETAILS_SUCCEEDED:
      return Object.assign({}, state, {
        scholarshipDetail: payload,
        showLoader: false,
        type
      });

    case FETCH_SCHOLARSHIP_DETAILS_FAILED:
      return payload;

    case FETCH_AWARDEES_SCHOLARSHIPS_REQUESTED:
      return {
        ...state,
        showLoader: true,
        list: null,
        type,
        isError: false,
        errorMessage: ""
      };

    case FETCH_AWARDEES_SCHOLARSHIPS_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        list: payload,
        type,
        isError: false,
        errorMessage: ""
      };

    case FETCH_AWARDEES_SCHOLARSHIPS_FAILED:
      return {
        ...state,
        showLoader: false,
        list: null,
        type,
        isError: true,
        errorMessage: payload
      };

    case FETCH_AWARDEES_DETAILS_SCHOLARSHIPS_REQUESTED:
      return { ...state, showLoader: true, details: null, type };

    case FETCH_AWARDEES_DETAILS_SCHOLARSHIPS_SUCCEEDED:
      return { ...state, showLoader: false, details: payload, type };

    case FETCH_AWARDEES_DETAILS_SCHOLARSHIPS_FAILED:
      return { ...state, showLoader: false, details: null, type };

    case FETCH_AWARDEES_FILTER_REQUESTED:
      return { ...state, showLoader: true, awardeesFiltered: null, type };

    case FETCH_AWARDEES_FILTER_SUCCEEDED:
      return { ...state, showLoader: false, awardeesFiltered: payload, type };

    case FETCH_AWARDEES_FILTER_FAILED:
      return { ...state, showLoader: false, awardeesFiltered: null, type, error: payload, isError: true };

    case HIT_COUNTER_SCHOLARSHIP_DETAILS_REQUESTED:
      return Object.assign({}, state, {
        hitCounter: {},
        showLoader: true,
        type
      });

    case HIT_COUNTER_SCHOLARSHIP_DETAILS_SUCCEEDED:
      return Object.assign({}, state, {
        hitCounter: payload,
        showLoader: false,
        type
      });

    case HIT_COUNTER_SCHOLARSHIP_DETAILS_FAILED:
      return { ...state, showLoader: false, type };

    case APPLY_NOW_HIT_COUNTER_REQUESTED:
      return Object.assign({}, state, {
        hitCounter: {},
        showLoader: true,
        type
      });

    case APPLY_NOW_HIT_COUNTER_SUCCEEDED:
      return Object.assign({}, state, {
        applyHitCounter: payload,
        showLoader: false,
        type
      });

    case APPLY_NOW_HIT_COUNTER_FAILED:
      return { ...state, showLoader: false };

    case FETCH_FAV_PERSIST_REQUEST:
      return { ...state, showLoader: true, type };

    case FETCH_FAV_PERSIST_FAIL:
      return { ...state, showLoader: false, type, isError: true };

    case FETCH_CONTACTUS_REQUESTED:
      return { ...state, showLoader: true, type };

    case FETCH_CONTACTUS_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        contactUs: payload,
        isError: true
      };

    case FETCH_CONTACTUS_SUCCEEDED:
      return {
        ...state,
        contactUs: payload,
        showLoader: false,
        type
      };

    case FETCH_SCHOLARSHIP_BYUSID_REQUESTED:
      return Object.assign({}, state, {
        scholarshipListUsid: [],
        showLoader: true,
        type
      });

    case FETCH_SCHOLARSHIP_BYUSID_SUCCEEDED:
      const scholarshipsUsid = payload.scholarships;
      let scholarshipListUsid = scholarshipsUsid;

      // let newStateUsid = {
      //   scholarshipListUsid,
      //   showLoader: false,
      //   type
      // };

      //newStateUsid.numScholarships = total;

      return Object.assign({}, state, {
        scholarshipListUsid,
        type,
        showLoader: false,
        numScholarships: payload.total
      });

    case FETCH_SCHOLARSHIP_BYUSID_FAILED:
      return payload;
    case FETCH_RULES_REQUESTED:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });

    case FETCH_RULES_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        rulesList: payload
      });

    case FETCH_RULES_FAILED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });

    case FETCH_COUNTRY_REQUESTED:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });

    case FETCH_COUNTRY_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        countryList: payload
      });

    case FETCH_COUNTRY_FAILED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });

    case FETCH_RELATED_SCHOLARSHIP_REQUESTED:
      return Object.assign({}, state, { showLoader: true, isError: false });

    case FETCH_RELATED_SCHOLARSHIP_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        relatedSch: payload
      });

    case FETCH_RELATED_SCHOLARSHIP_FAILED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        error: payload
      });
    case FETCH_RELATED_ARTICLE_REQUESTED:
      return Object.assign({}, state, { showLoader: true, isError: false });

    case FETCH_RELATED_ARTICLE_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        relatedArticle: payload
      });

    case FETCH_RELATED_ARTICLE_FAILED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        error: payload
      });

    case "FETCH_FAV_SCHOLARSHIP_REQUESTED":
      return Object.assign({}, state, { showLoader: true, isError: false });

    case "FETCH_FAV_SCHOLARSHIP_SUCCEEDED":
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        favSch: payload
      });

    case "FETCH_FAV_SCHOLARSHIP_FAILED":
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        error: payload
      });
    case "VERIFY_TOKEN":
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload,
        //  isAuthenticated: true,
        userRulesPresent: false
      };
    case LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload
      };
    case SOCIAL_LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type,
        userLoginData: payload,
        isAuthenticated: true
      };
    case FETCH_LIKE_FOLLOW_REQUESTED:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });

    case FETCH_LIKE_FOLLOW_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        likeFollow: payload
      });

    case FETCH_LIKE_FOLLOW_FAILED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });
    case FETCH_FAQ_REQUESTED:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });

    case FETCH_FAQ_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        faq: payload
      });

    case FETCH_FAQ_FAILED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });

    case FETCH_ARCHIVE_REQUESTED:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });

    case FETCH_ARCHIVE_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        archive: payload
      });

    case FETCH_ARCHIVE_FAILED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });

    case FETCH_SCHOLAR_REQUESTED:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });

    case FETCH_SCHOLAR_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        scholars: payload
      });

    case FETCH_SCHOLAR_FAILED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });

    case FETCH_EVENT_MEDIA_REQUESTED:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });

    case FETCH_EVENT_MEDIA_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        eventMedia: payload
      });

    case FETCH_EVENT_MEDIA_FAILED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });

    case ADD_LIKE_FOLLOW_REQUESTED:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });

    case ADD_LIKE_FOLLOW_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        likefollowResponse: payload
      });

    case ADD_LIKE_FOLLOW_FAILED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });

    case FETCH_FACET_SCHOLARSHIP_REQUESTED:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });

    case FETCH_FACET_SCHOLARSHIP_SUCCEEDED:
      const facetScholarships = payload;
      let facetScholarshipsResponse = facetScholarships;
      const facetTotalSch = facetScholarshipsResponse.scholarships;

      // let newStateUsid = {
      //   scholarshipListUsid,
      //   showLoader: false,
      //   type
      // };

      //newStateUsid.numScholarships = total;

      return Object.assign({}, state, {
        facetScholarshipsResponse,
        type,
        showLoader: false,
        numScholarships: facetTotalSch.total ? facetTotalSch.total : 0
      });

    case FETCH_FACET_SCHOLARSHIP_FAILED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });

    default:
      return state;
  }
};
export default scholarshipReducer;
