import { connect } from "react-redux";
import {
  fetchFeaturedScholarships as fetchFeaturedScholarshipsDetailAction,
  fetchScholarships as fetchScholarshipsAction,
  fetchScholarshipDetail as fetchScholarshipsDetailAction,
  fetchHitCounter as hitCounterAction,
  applyNowHitCounter as applyNowHitCounterAction,
  fetchRelatedScholarship as fetchRelatedSchAction,
  fetchRelatedArticle as fetchRelatedArticleAction,
  fetchFavScholarship as fetchFavScholarshipAction,
  fetchLikeFollow as fetchLikeFollowAction,
  fetchFAQ as fetchFaqAction,
  fetchArchive as fetchArchiveAction,
  fetchScholar as fetchScholarsAction,
  fetchEventMedia as fetchEventMediaAction,
  addLikeFollow as addLikeFollowAction
} from "../actions";

import {
  addToFavScholarship as addToFavScholarshipActions,
  persistFavScholarship as persistFavScholarshipAction
} from "../../dashboard/actions";
import { fetchContactUs as fetchContactUsAction } from "../../../constants/commonActions";
import ScholarshipDetailPage from "../components/schorshipDetailPage";

const mapStateToProps = ({
  scholarship,
  dashboard,
  common,
  loginOrRegister
}) => ({
  scholarshipDetail: scholarship.scholarshipDetail,
  featuredScholarshipList: scholarship.featuredScholarshipList,
  showLoader: scholarship.showLoader,
  contactUs: common,
  type: scholarship.type,
  dashType: dashboard.type,
  loginRegType: loginOrRegister.type,
  userLoginData: loginOrRegister.userLoginData,
  hitCounter: scholarship.hitCounter,
  persistData: dashboard.persistFav,
  favScholarships: dashboard.favScholarships,
  isAuthenticated: loginOrRegister.isAuthenticated,
  applyHit: scholarship.applyHitCounter,
  relatedSch: scholarship.relatedSch,
  relatedArticle: scholarship.relatedArticle,
  favSch: scholarship.favSch,
  likeAndFollow: scholarship.likeFollow,
  faq: scholarship.faq,
  archive: scholarship.archive,
  scholars: scholarship.scholars
});

const mapDispatchToProps = dispatch => ({
  loadScholarshipDetail: slug => dispatch(fetchScholarshipsDetailAction(slug)),

  loadFeaturedScholarshipDetail: () =>
    dispatch(fetchFeaturedScholarshipsDetailAction()),
  loadContact: data => dispatch(fetchContactUsAction(data)),
  hitCounter: input => dispatch(hitCounterAction(input)),
  addToFavScholarships: inputData =>
    dispatch(addToFavScholarshipActions(inputData)),
  persistFavSch: inputData => dispatch(persistFavScholarshipAction(inputData)),
  applyNowHit: inputData => dispatch(applyNowHitCounterAction(inputData)),
  getRelatedScholarship: inputData =>
    dispatch(fetchRelatedSchAction(inputData)),
  getRelatedArticle: inputData =>
    dispatch(fetchRelatedArticleAction(inputData)),
  getFavScholarship: inputData =>
    dispatch(fetchFavScholarshipAction(inputData)),
  likeFollow: inputData => dispatch(fetchLikeFollowAction(inputData)),
  fetchFaq: inputData => dispatch(fetchFaqAction(inputData)),
  fetchArchive: inputData => dispatch(fetchArchiveAction(inputData)),
  fetchScholars: inputData => dispatch(fetchScholarsAction(inputData)),
  fetchEventMedia: inputData => dispatch(fetchEventMediaAction(inputData)),
  postLikeFollow: inputData => dispatch(addLikeFollowAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  ScholarshipDetailPage
);
