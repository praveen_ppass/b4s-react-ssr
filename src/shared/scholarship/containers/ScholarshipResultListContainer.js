import React from "react";
import { connect } from "react-redux";
import { fetchScholarshipsAwardees as fetchScholarshipsAwardeesAction } from "../actions";
import scholarshipResultListPage from "../components/scholarshipResultListPage";

const mapStateToProps = ({ scholarshipDetail }) => ({
  awardees: scholarshipDetail.list,
  showLoader: scholarshipDetail.showLoader,
  isError: scholarshipDetail.isError,
  errorMessage: scholarshipDetail.errorMessage
});
const mapDispatchToProps = dispatch => ({
  loadAwardees: () => dispatch(fetchScholarshipsAwardeesAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(
  scholarshipResultListPage
);
