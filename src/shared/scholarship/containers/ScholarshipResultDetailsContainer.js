import React from "react";
import { connect } from "react-redux";
import { fetchScholarshipsAwardees as fetchScholarshipsAwardeesAction } from "../actions";
import { fetchAwardeesDetails as fetchAwardeesDetailsAction } from "../actions";
import { fetchAwardeesFilter as fetchAwardeesFilterAction } from "../actions";
import scholarshipResultDetailsPage from "../components/scholarshipResultDetailsPage";
import scholarshipDetailsContainer from "./scholarshipDetailsContainer";

const mapStateToProps = ({ scholarshipDetail }) => ({
  awardeesDetailsResult: scholarshipDetail.details,
  awardeeslisting: scholarshipDetail.list,
  showLoader: scholarshipDetail.showLoader,
  type: scholarshipDetail.type,
  awardeesFiltered: scholarshipDetail.awardeesFiltered,
  error: scholarshipDetail.error,
});
const mapDispatchToProps = dispatch => ({
  awardeesDetails: data => dispatch(fetchAwardeesDetailsAction(data)),
  loadAwardees: () => dispatch(fetchScholarshipsAwardeesAction()),
  awardeesFilter: (data) => dispatch(fetchAwardeesFilterAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  scholarshipResultDetailsPage
);
