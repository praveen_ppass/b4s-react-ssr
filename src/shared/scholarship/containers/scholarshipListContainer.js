import React from "react";
import { connect } from "react-redux";

import {
  fetchFeaturedScholarships as fetchFeaturedScholarshipsAction,
  fetchScholarships as fetchScholarshipsAction,
  fetchClosedScholarships as fetchClosedScholarshipsAction,
  fetchSchByUsid as fetchSchByUsidAction,
  fetchFacetScholarship as fetchFacetScholarshipAction,
  fetchCountry as fetchCountryAction

} from "../actions";
import { fetchRules as fetchRulesAction } from "../../../constants/commonActions";
import scholarshipList from "../components/scholarshiplistPage";

const mapStateToProps = ({ scholarship, common }) => ({
  scholarshipList: scholarship.scholarshipList,
  scholarshipListUsid: scholarship.scholarshipListUsid,
  numScholarships: scholarship.numScholarships,
  featuredScholarshipList: scholarship.featuredScholarshipList,
  closedScholarshipList: scholarship.closedScholarshipList,
  rulesList: common.rulesList,
  countryList:scholarship.countryList,
  showLoader: scholarship.showLoader,
  type: scholarship.type,
  facetSchResponse: scholarship.facetScholarshipsResponse
});

const mapDispatchToProps = dispatch => ({
  loadScholarships: inputData => dispatch(fetchScholarshipsAction(inputData)),
  loadFeaturedScholarships: inputData =>
    dispatch(fetchFeaturedScholarshipsAction(inputData)),
  loadClosedScholarships: inputData =>
    dispatch(fetchClosedScholarshipsAction(inputData)),
  loadRules: inputData => dispatch(fetchRulesAction()),
  loadCountry: () => dispatch(fetchCountryAction()),
  fetchSchByUSID: inputData => dispatch(fetchSchByUsidAction(inputData)),
  fetchFacetSchls: inputData => dispatch(fetchFacetScholarshipAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(scholarshipList);
