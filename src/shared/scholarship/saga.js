import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
// import { showLoading, hideLoading } from "react-redux-loading-bar";

import { apiUrl, getApi, endPoint, baseUrls } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

/************   All actions required in this module starts *********** */
import {
  FETCH_SCHOLARSHIPS_SUCCEEDED,
  FETCH_SCHOLARSHIPS_REQUESTED,
  FETCH_SCHOLARSHIPS_FAILED,
  FETCH_FEATURED_SCHOLARSHIPS_SUCCEEDED,
  FETCH_FEATURED_SCHOLARSHIPS_REQUESTED,
  FETCH_FEATURED_SCHOLARSHIPS_FAILED,
  FETCH_CLOSED_SCHOLARSHIPS_REQUESTED,
  FETCH_CLOSED_SCHOLARSHIPS_SUCCEEDED,
  FETCH_CLOSED_SCHOLARSHIPS_FAILED,
  FETCH_SCHOLARSHIP_DETAILS_REQUESTED,
  FETCH_SCHOLARSHIP_DETAILS_SUCCEEDED,
  FETCH_SCHOLARSHIP_DETAILS_FAILED,
  FETCH_AWARDEES_SCHOLARSHIPS_REQUESTED,
  FETCH_AWARDEES_SCHOLARSHIPS_SUCCEEDED,
  FETCH_AWARDEES_SCHOLARSHIPS_FAILED,
  FETCH_AWARDEES_DETAILS_SCHOLARSHIPS_REQUESTED,
  FETCH_AWARDEES_DETAILS_SCHOLARSHIPS_SUCCEEDED,
  FETCH_AWARDEES_DETAILS_SCHOLARSHIPS_FAILED,
  FETCH_AWARDEES_FILTER_REQUESTED,
  FETCH_AWARDEES_FILTER_SUCCEEDED,
  FETCH_AWARDEES_FILTER_FAILED,
  HIT_COUNTER_SCHOLARSHIP_DETAILS_REQUESTED,
  HIT_COUNTER_SCHOLARSHIP_DETAILS_SUCCEEDED,
  HIT_COUNTER_SCHOLARSHIP_DETAILS_FAILED,
  APPLY_NOW_HIT_COUNTER_REQUESTED,
  APPLY_NOW_HIT_COUNTER_SUCCEEDED,
  APPLY_NOW_HIT_COUNTER_FAILED,
  FETCH_SCHOLARSHIP_BYUSID_REQUESTED,
  FETCH_SCHOLARSHIP_BYUSID_SUCCEEDED,
  FETCH_SCHOLARSHIP_BYUSID_FAILED,
  FETCH_RELATED_SCHOLARSHIP_FAILED,
  FETCH_RELATED_SCHOLARSHIP_SUCCEEDED,
  FETCH_RELATED_SCHOLARSHIP_REQUESTED,
  FETCH_RELATED_ARTICLE_REQUESTED,
  FETCH_RELATED_ARTICLE_SUCCEEDED,
  FETCH_RELATED_ARTICLE_FAILED,
  FETCH_FAV_SCHOLARSHIP_REQUESTED,
  FETCH_LIKE_FOLLOW_REQUESTED,
  FETCH_FAQ_REQUESTED,
  FETCH_ARCHIVE_REQUESTED,
  FETCH_EVENT_MEDIA_REQUESTED,
  FETCH_SCHOLAR_REQUESTED,
  ADD_LIKE_FOLLOW_REQUESTED,
  FETCH_FACET_SCHOLARSHIP_REQUESTED,
  FETCH_COUNTRY_REQUESTED,
  FETCH_COUNTRY_SUCCEEDED,
  FETCH_COUNTRY_FAILED
} from "./actions";

//awardees call

const fetchAwardeesDetails = input => {
  let url = `${apiUrl.awardeesDetails2}/${input.slug}/result`;
  if (input.size && input.page >= 0) {
    url = `${url}?page=${input.page}&size=${input.size}`;
  }
  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchAwardeesScholarshipsDetails(input) {
  try {
    const Awardees = yield call(fetchAwardeesDetails, input.payload);
    yield put({
      type: FETCH_AWARDEES_DETAILS_SCHOLARSHIPS_SUCCEEDED,
      payload: Awardees
    });
  } catch (error) {
    yield put({
      type: FETCH_AWARDEES_DETAILS_SCHOLARSHIPS_FAILED,
      payload: error
    });
  }
}

const fetchAwardeesFilter = input => {
  let url = `${apiUrl.awardeesDetails2}/${input.slug}/result/filter`;

  return fetchClient.post(url,
    input.filter)
    .then(res => {
      return res.data;
    });
};

function* fetchAwardeesFilterDetails(input) {
  try {
    const awardeesFilter = yield call(fetchAwardeesFilter, input.payload);
    yield put({
      type: FETCH_AWARDEES_FILTER_SUCCEEDED,
      payload: awardeesFilter
    });
  } catch (error) {
    yield put({
      type: FETCH_AWARDEES_FILTER_FAILED,
      payload: error.response.data
    });
  }
}

const fetchAwardees = () =>
  fetchClient.get(apiUrl.awardeesList).then(res => {
    return res.data;
  });


function* fetchAwardeesScholarships() {
  try {
    const Awardees = yield call(fetchAwardees);

    yield put({
      type: FETCH_AWARDEES_SCHOLARSHIPS_SUCCEEDED,
      payload: Awardees
    });
  } catch (error) {
    yield put({
      type: FETCH_AWARDEES_SCHOLARSHIPS_FAILED,
      payload: error.errorMessage
    });
  }
}
const fetchCountryURL = () =>
  fetchClient.get(apiUrl.countryList).then(res => {
    return res.data;
  });

function* fetchCountry() {
  try {
    const Country = yield call(fetchCountryURL);
    yield put({
      type: FETCH_COUNTRY_SUCCEEDED,
      payload: Country
    });
  } catch (error) {
    yield put({
      type: FETCH_COUNTRY_FAILED,
      payload: error.errorMessage
    });
  }
}

/************   All actions required in this module starts *********** */

/************************************************************************************** */
/*** Fetch All scholarships Start***/
/************************************************************************************** */

const fetchScholarshipsApi = ({ page, length, countries, rules, mode, searchText }) =>
  fetchClient
    .post(apiUrl.scholarshipDetail, {
      page,
      length,
      countries,
      rules,
      mode,
      searchText
    })
    .then(res => {
      return res.data;
    });

function* fetchScholarships(input) {
  try {
    const { page, length, rules, mode, searchText, countries } = input.payload.inputData;
    const scholarships = yield call(fetchScholarshipsApi, {
      page,
      length,
      countries,
      rules,
      mode,
      searchText
    });
    yield put({
      type: FETCH_SCHOLARSHIPS_SUCCEEDED,
      payload: scholarships
    });
  } catch (error) {
    yield put({
      type: FETCH_SCHOLARSHIPS_FAILED,
      payload: error
    });
  }
}

/************   All actions required in this module starts *********** */

/************************************************************************************** */
/*** Fetch All scholarships Start With USID***/
/************************************************************************************** */

const fetchScholarshipsUsidApi = ({ usid }) =>
  fetchClient.get(`${apiUrl.scholarshipListByUsid}/${usid}`).then(res => {
    return res.data;
  });

function* fetchScholarshipsByUsid(input) {
  try {
    const { usid } = input.payload;
    const scholarshipsUsid = yield call(fetchScholarshipsUsidApi, {
      usid
    });
    yield put({
      type: FETCH_SCHOLARSHIP_BYUSID_SUCCEEDED,
      payload: scholarshipsUsid
    });
  } catch (error) {
    yield put({
      type: FETCH_SCHOLARSHIP_BYUSID_FAILED,
      payload: error
    });
  }
}

/************************************************************************************** */
/*** Fetch All scholarships End***/
/************************************************************************************** */

/************   All actions required in this module starts *********** */

/************************************************************************************** */
/*** Fetch Related Scholarship***/
/************************************************************************************** */

const fetchRelatedSchApi = ({ bsid, len }) => {
  let url = `${apiUrl.getRelatedScholarship}/${bsid}`;

  if (len) {
    url = `${url}?len=${len}`;
  }
  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchRelatedScholarship(input) {
  try {
    const { bsid, len } = input.payload;
    const relatedSchBasedOnBsid = yield call(fetchRelatedSchApi, {
      bsid,
      len
    });
    yield put({
      type: FETCH_RELATED_SCHOLARSHIP_SUCCEEDED,
      payload: relatedSchBasedOnBsid
    });
  } catch (error) {
    yield put({
      type: FETCH_RELATED_SCHOLARSHIP_FAILED,
      payload: error
    });
  }
}

/************************************************************************************** */
/*** Fetch Related Scholarship end ***/
/************************************************************************************** */

/************   All actions required in this module starts *********** */

/************************************************************************************** */
/*** Fetch Related Article***/
/************************************************************************************** */

const fetchRelatedArticleApi = ({ tags, page, length, excludeSlug }) => {
  // let url = `${apiUrl.getRelatedArticle}/${usid}/article?size=${size}`;
  let url = `${
    apiUrl.getRelatedArticle
    }?tags=${tags}&excludeSlug=${excludeSlug}&page=${page}&length=${length}`;
  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchRelatedArticle(input) {
  try {
    const { tags, page, length, excludeSlug } = input.payload;
    const relatedArticle = yield call(fetchRelatedArticleApi, {
      tags,
      page,
      length,
      excludeSlug
    });
    yield put({
      type: FETCH_RELATED_ARTICLE_SUCCEEDED,
      payload: relatedArticle
    });
  } catch (error) {
    yield put({
      type: FETCH_RELATED_ARTICLE_FAILED,
      payload: error
    });
  }
}
/************************************************************************************** */
/*** Fetch Related Article end ***/
/************************************************************************************** */

/************************************************************************************** */
/*** Fetch Favourite Scholarship***/
/************************************************************************************** */

const fetchFavSchApi = ({ scholarshipId }) => {
  let url = `${apiUrl.userFavScholarship}/favscholarship/${scholarshipId}`;

  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchFavSch(input) {
  try {
    const { scholarshipId } = input.payload;
    const favSch = yield call(fetchFavSchApi, {
      scholarshipId
    });
    yield put({
      type: "FETCH_FAV_SCHOLARSHIP_SUCCEEDED",
      payload: favSch
    });
  } catch (error) {
    yield put({
      type: "FETCH_FAV_SCHOLARSHIP_FAILED",
      payload: error
    });
  }
}
/************************************************************************************** */
/*** Fetch Favourite Scholarship ***/
/************************************************************************************** */

/************************************************************************************** */
/*** Fetch Favourite Scholarship***/
/************************************************************************************** */

const fetchLikeFollowApi = ({ userId, scholarshipId }) => {
  let url = `${
    apiUrl.userFavScholarship
    }/${userId}/favourite/scholarship/${scholarshipId}`;

  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchLikeFollow(input) {
  try {
    const { userId, scholarshipId } = input.payload;
    const likeFollow = yield call(fetchLikeFollowApi, {
      scholarshipId,
      userId
    });
    yield put({
      type: "FETCH_LIKE_FOLLOW_SUCCEEDED",
      payload: likeFollow
    });
  } catch (error) {
    yield put({
      type: "FETCH_LIKE_FOLLOW_FAILED",
      payload: error
    });
  }
}
/************************************************************************************** */
/*** Fetch Favourite Scholarship ***/
/************************************************************************************** */
/************************************************************************************** */
/*** Fetch FAQ Scholarship***/
/************************************************************************************** */

const fetchFAQApi = ({ usid }) => {
  let url = `${apiUrl.scholarshipFAQ}/faq?usid=${usid}`;

  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchFAQ(input) {
  try {
    const { usid } = input.payload;
    const faq = yield call(fetchFAQApi, {
      usid
    });
    yield put({
      type: "FETCH_FAQ_SUCCEEDED",
      payload: faq
    });
  } catch (error) {
    yield put({
      type: "FETCH_FAQ_FAILED",
      payload: error
    });
  }
}
/************************************************************************************** */
/*** Fetch FAQ Scholarship ***/
/************************************************************************************** */

/************************************************************************************** */
/*** Fetch Archive Scholarship***/
/************************************************************************************** */

const fetchArchiveApi = ({ scholarshipId }) => {
  let url = `${apiUrl.getArchiveScholarship}/${scholarshipId}/archive`;

  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchArchive(input) {
  try {
    const { scholarshipId } = input.payload;
    const archive = yield call(fetchArchiveApi, {
      scholarshipId
    });
    yield put({
      type: "FETCH_ARCHIVE_SUCCEEDED",
      payload: archive
    });
  } catch (error) {
    yield put({
      type: "FETCH_ARCHIVE_FAILED",
      payload: error
    });
  }
}
/************************************************************************************** */
/*** Fetch Archive Scholarship ***/
/************************************************************************************** */

/************************************************************************************** */
/*** Fetch Scholar Scholarship***/
/************************************************************************************** */

const fetchScholarApi = ({ usid }) => {
  let url = `${apiUrl.getArchiveScholars}/${usid}/archiveScholar`;

  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchScholar(input) {
  try {
    const { usid } = input.payload;
    const scholars = yield call(fetchScholarApi, {
      usid
    });
    yield put({
      type: "FETCH_SCHOLAR_SUCCEEDED",
      payload: scholars
    });
  } catch (error) {
    yield put({
      type: "FETCH_SCHOLAR_FAILED",
      payload: error
    });
  }
}
/************************************************************************************** */
/*** Fetch Scholar Scholarship ***/
/************************************************************************************** */

/************************************************************************************** */
/*** Fetch Event Media Scholarship***/
/************************************************************************************** */

const fetchEventMediaApi = ({ scholarshipId }) => {
  let url = `${apiUrl.getArchiveScholarship}/${scholarshipId}/archive`;

  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchEventMedia(input) {
  try {
    const { scholarshipId } = input.payload;
    const eventMedia = yield call(fetchEventMediaApi, {
      scholarshipId
    });
    yield put({
      type: "FETCH_EVENT_MEDIA_SUCCEEDED",
      payload: eventMedia
    });
  } catch (error) {
    yield put({
      type: "FETCH_EVENT_MEDIA_FAILED",
      payload: error
    });
  }
}
/************************************************************************************** */
/*** Fetch Event Media Scholarship ***/
/************************************************************************************** */

/************************************************************************************** */
/*** Fetch Add Like and Follow Scholarship***/
/************************************************************************************** */

const fetchPostLikeFollowApi = ({ scholarshipId, userId, like, follow }) => {
  let url = `${apiUrl.addLikeFollow}/${userId}/favscholarship/${scholarshipId}`;

  return fetchClient
    .post(url, {
      favourite: like,
      follow: follow
    })
    .then(res => {
      return res.data;
    });
};

function* fetchPostLikeFollow(input) {
  try {
    const { scholarshipId, userId, like, follow } = input.payload;
    const postLikeFollow = yield call(fetchPostLikeFollowApi, {
      scholarshipId,
      userId,
      like,
      follow
    });
    yield put({
      type: "ADD_LIKE_FOLLOW_SUCCEEDED",
      payload: postLikeFollow
    });
  } catch (error) {
    yield put({
      type: "ADD_LIKE_FOLLOW_FAILED",
      payload: error
    });
  }
}
/************************************************************************************** */
/*** Fetch Add Like and Follow Scholarship ***/
/************************************************************************************** */

/************************************************************************************** */
/*** Facet Scholarship***/
/************************************************************************************** */

const fetchFacetSchApi = ({ slug }) => {
  let url = `${baseUrls.ssms}/scholarship/${slug}/facetlookup`;

  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchFacetSch(input) {
  try {
    const { slug } = input.payload;
    const facetSch = yield call(fetchFacetSchApi, {
      slug
    });
    yield put({
      type: "FETCH_FACET_SCHOLARSHIP_SUCCEEDED",
      payload: facetSch
    });
  } catch (error) {
    yield put({
      type: "FETCH_FACET_SCHOLARSHIP_FAILED",
      payload: error
    });
  }
}
/************************************************************************************** */
/*** Facet Scholarship ***/
/************************************************************************************** */
/************************************************************************************** */
/*** Fetch  Featured scholarships Start***/
/************************************************************************************** */

const fetchFeaturedScholarshipsUrl = () =>
  fetchClient.get(`${apiUrl.featuredScholarships}=true`, {}).then(res => {
    return res.data;
  });

function* fetchFeaturedScholarships(input) {
  try {
    const scholarships = yield call(fetchFeaturedScholarshipsUrl);

    yield put({
      type: FETCH_FEATURED_SCHOLARSHIPS_SUCCEEDED,
      payload: scholarships
    });
  } catch (error) {
    yield put({
      type: FETCH_FEATURED_SCHOLARSHIPS_FAILED,
      payload: error
    });
  }
}

/************************************************************************************** */
/*** Fetch Featured scholarships End***/
/************************************************************************************** */

/************************************************************************************** */
/*** Fetch scholarships detail Start***/
/************************************************************************************** */

const fetchScholarshipDetailUrl = input =>
  fetchClient.get(`${apiUrl.scholarshipDetail}${input}`, {}).then(res => {
    return res.data;
  });

function* fetchScholarshipDetail(input) {
  try {
    const scholarships = yield call(
      fetchScholarshipDetailUrl,
      input.payload["slug"]
    );

    yield put({
      type: FETCH_SCHOLARSHIP_DETAILS_SUCCEEDED,
      payload: scholarships
    });
  } catch (error) {
    yield put({
      type: FETCH_SCHOLARSHIP_DETAILS_FAILED,
      payload: error
    });
  }
}

/************************************************************************************** */
/*** Fetch scholarships detail Ends***/
/************************************************************************************** */

/************************************************************************************** */
/*** Fetch  closed scholarships Start***/
/************************************************************************************** */

// const fetchClosedScholarshipsUrl = () =>{fetchClient.post(apiUrl.closedScholarships,{"OFFSET":"0","LENGTH":"10"})
// .then(res => {
// return res.data;
// });
// }
//  function* fetchClosedScholarships(input) {

//     try {
//         const scholarships = yield call(fetchClosedScholarshipsUrl);

//         yield put({
//             type: FETCH_CLOSED_SCHOLARSHIPS_SUCCEEDED,
//             payload: scholarships
//         });
//     } catch (error) {
//         yield put({
//             type: FETCH_CLOSED_SCHOLARSHIPS_FAILED,
//             payload: error,
//         });
//     }
// }

const fetchClosedScholarshipsUrl = () =>
  fetchClient
    .post(apiUrl.closedScholarships, { OFFSET: "0", LENGTH: "10" })
    .then(res => {
      return res.data;
    });

function* fetchClosedScholarships(input) {
  try {
    const closedScholarships = yield call(fetchClosedScholarshipsUrl);

    yield put({
      type: FETCH_CLOSED_SCHOLARSHIPS_SUCCEEDED,
      payload: closedScholarships
    });
  } catch (error) {
    yield put({
      type: FETCH_CLOSED_SCHOLARSHIPS_FAILED,
      payload: error
    });
  }
}

/************************************************************************************** */
/*** Fetch Hit Counter ***/
/************************************************************************************** */

const fetchHitCounterSchDetailsUrl = input => {
  const url = input.userId
    ? `${apiUrl.hitCounterSchDetail}/${input.nid}/view?userId=${input.userId}`
    : `${apiUrl.hitCounterSchDetail}/${input.nid}/view`;
  fetchClient.post(url).then(res => {
    return res.data;
  });
};

function* fetchHitCounterSchDetails(input) {
  try {
    const hitCounterSch = yield call(
      fetchHitCounterSchDetailsUrl,
      input.payload
    );

    yield put({
      type: HIT_COUNTER_SCHOLARSHIP_DETAILS_SUCCEEDED,
      payload: hitCounterSch
    });
  } catch (error) {
    yield put({
      type: HIT_COUNTER_SCHOLARSHIP_DETAILS_FAILED,
      payload: error
    });
  }
}

/************************************************************************************** */
/*** Hit Counter ***/
/************************************************************************************** */

const fetchApplyNowHitCounter = input =>
  fetchClient
    .post(
      `${apiUrl.hitCounterSchDetail}/${input.scholarshipId}/user/${
      input.userId
      }/apply`
    )
    .then(res => {
      return res.data;
    });

function* fetchApplyNowHitCounterSchDetails(input) {
  try {
    const applyNowHitCounter = yield call(
      fetchApplyNowHitCounter,
      input.payload
    );

    yield put({
      type: APPLY_NOW_HIT_COUNTER_SUCCEEDED,
      payload: applyNowHitCounter
    });
  } catch (error) {
    yield put({
      type: APPLY_NOW_HIT_COUNTER_FAILED,
      payload: error
    });
  }
}

/************************************************************************************** */
/*** Fetch Closed scholarships End***/
/************************************************************************************** */

export default function* fetchScholarshipsSaga() {
  FETCH_SCHOLARSHIP_DETAILS_REQUESTED;
  yield takeEvery(FETCH_SCHOLARSHIPS_REQUESTED, fetchScholarships);
  yield takeEvery(FETCH_SCHOLARSHIP_DETAILS_REQUESTED, fetchScholarshipDetail);
  yield takeEvery(
    HIT_COUNTER_SCHOLARSHIP_DETAILS_REQUESTED,
    fetchHitCounterSchDetails
  );
  yield takeEvery(
    FETCH_FEATURED_SCHOLARSHIPS_REQUESTED,
    fetchFeaturedScholarships
  );
  yield takeEvery(FETCH_CLOSED_SCHOLARSHIPS_REQUESTED, fetchClosedScholarships);
  yield takeEvery(
    FETCH_AWARDEES_SCHOLARSHIPS_REQUESTED,
    fetchAwardeesScholarships
  );
  yield takeEvery(
    FETCH_COUNTRY_REQUESTED,
    fetchCountry
  );
  yield takeEvery(
    FETCH_AWARDEES_DETAILS_SCHOLARSHIPS_REQUESTED,
    fetchAwardeesScholarshipsDetails
  );
  yield takeEvery(
    FETCH_AWARDEES_FILTER_REQUESTED,
    fetchAwardeesFilterDetails
  );
  yield takeEvery(
    APPLY_NOW_HIT_COUNTER_REQUESTED,
    fetchApplyNowHitCounterSchDetails
  );
  yield takeEvery(FETCH_SCHOLARSHIP_BYUSID_REQUESTED, fetchScholarshipsByUsid);
  yield takeEvery(FETCH_RELATED_SCHOLARSHIP_REQUESTED, fetchRelatedScholarship);
  yield takeEvery(FETCH_RELATED_ARTICLE_REQUESTED, fetchRelatedArticle);
  yield takeEvery(FETCH_FAV_SCHOLARSHIP_REQUESTED, fetchFavSch);
  yield takeEvery(FETCH_LIKE_FOLLOW_REQUESTED, fetchLikeFollow);
  yield takeEvery(FETCH_FAQ_REQUESTED, fetchFAQ);
  yield takeEvery(FETCH_ARCHIVE_REQUESTED, fetchArchive);
  yield takeEvery(FETCH_SCHOLAR_REQUESTED, fetchScholar);
  yield takeEvery(FETCH_EVENT_MEDIA_REQUESTED, fetchEventMedia);
  yield takeEvery(ADD_LIKE_FOLLOW_REQUESTED, fetchPostLikeFollow);
  yield takeEvery(FETCH_FACET_SCHOLARSHIP_REQUESTED, fetchFacetSch);
}
