import React from "react";
import { connect } from "react-redux";
import {
  fetchDashData as fetchDashDataAction,
  fetchPromotionalBanner as fetchPromotionalBannerAction
} from "../actions";
import { fetchUserRules as fetchUserRulesAction } from "../../../constants/commonActions";
import {
  closeLoginPagePopup as closeLoginPagePopupAction,
  logUserOut as logUserOutAction
} from "../../login/actions";
import Home from "../components/HomePage";

const mapStateToProps = ({ home, common, loginOrRegister }) => ({
  dashData: home.dashData,
  promotionalBanner: home.promotionalBanner,
  showLoader: home.showLoader,
  userDetails: common,
  type: home.type,
  isAuthenticated: loginOrRegister.isAuthenticated
});

const mapDispatchToProps = dispatch => ({
  loadDashData: () => dispatch(fetchDashDataAction()),
  loadUserData: userId => dispatch(fetchUserRulesAction(userId)),
  closeLoginPagePopup: () => dispatch(closeLoginPagePopupAction()),
  logUserOut: inputData => dispatch(logUserOutAction(inputData)),
  loadPromotionalBanner: input => dispatch(fetchPromotionalBannerAction(input))
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
