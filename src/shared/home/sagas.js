import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

/************   All actions required in this module starts *********** */
import {
  FETCH_DASHDATA_REQUESTED,
  FETCH_DASHDATA_SUCCEEDED,
  FETCH_DASHDATA_FAILED,
  FETCH_PROMOTIONAL_BANNER_REQUESTED,
  FETCH_PROMOTIONAL_BANNER_SUCCEEDED,
  FETCH_PROMOTIONAL_BANNER_FAILED
} from "./actions";

/************   All actions required in this module starts *********** */

/************************************************************************************** */
/*** Fetch Whole dash data Start***/
/************************************************************************************** */

const fetchDashDataUrl = () =>
  fetchClient.get(apiUrl.dashDetails, {}).then(res => {
    return res.data;
  });
function* fetchDashData(input) {
  try {
    const dashData = yield call(fetchDashDataUrl);
    yield put({
      type: FETCH_DASHDATA_SUCCEEDED,
      payload: dashData
    });
  } catch (error) {
    yield put({
      type: FETCH_DASHDATA_FAILED,
      payload: error
    });
  }
}
/************************************************************************************** */
/*** Fetch whole dash data End***/
/************************************************************************************** */

/************************************************************************************** */
/*** Fetch Promotional Banner Start ***/
/************************************************************************************** */

const fetchPromotionalBannerUrl = promotionalBanner =>
  fetchClient
    .get(`${apiUrl.promotionalBannerApi}${promotionalBanner}`)
    .then(res => res.data);

function* fetchPromotionalBanner(input) {
  try {
    const promotionalBanner = yield call(
      fetchPromotionalBannerUrl,
      input.payload
    );
    yield put({
      type: FETCH_PROMOTIONAL_BANNER_SUCCEEDED,
      payload: promotionalBanner
    });
  } catch (error) {
    yield put({
      type: FETCH_PROMOTIONAL_BANNER_FAILED,
      payload: error
    });
  }
}
/************************************************************************************** */
/*** Fetch Promotional Banner End ***/
/************************************************************************************** */

export default function* homeSaga() {
  yield takeEvery(FETCH_DASHDATA_REQUESTED, fetchDashData);
  yield takeEvery(FETCH_PROMOTIONAL_BANNER_REQUESTED, fetchPromotionalBanner);
}
