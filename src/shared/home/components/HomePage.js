import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { Redirect } from "react-router-dom";
import { Link } from "react-router-dom";
import moment from "moment";
import "url-search-params-polyfill";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import TestimonialSlider from "./testimonial-slider";
import PromotionalBannerSlider from "./banner-slider";
import UserLoginRegistrationPopup from "../../login/containers/userLoginRegistrationContainer";
import Loader from "../../common/components/loader";
import gblFunc from "../../../globals/globalFunctions";

import {
  defaultHomePageStats,
  myScholarshipsRoute,
  LOGIN_PAGE_ROUTE,
  LOGIN_VLE_ROUTE,
  LOGOUT_PAGE_ROUTE,
  prodEnv,
  PRODUCTION_URL,
  STAGING_URL,
  imgBaseUrl,
  imgBaseUrlDev
} from "../../../constants/constants";

import { refreshTokenCall } from "../../../globals/globalApiCalls";
import {
  FETCH_USER_RULES_REQUEST,
  FETCH_USER_RULES_SUCCESS,
  FETCH_USER_RULES_FAILURE
} from "../../../constants/commonActions";
import { setTimeout } from "timers";
import Auth from "./auth";

import {
  OPEN_LOGIN_PAGE_POPUP,
  LOG_USER_OUT_SUCCEEDED
} from "../../login/actions";
import { FETCH_PROMOTIONAL_BANNER_SUCCEEDED } from "./../actions";
import AlertMessage from "../../common/components/alertMsg";

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.openLoginPopup = this.openLoginPopup.bind(this);
    this.closePopup = this.closePopup.bind(this);
    this.onSearchTermChange = this.onSearchTermChange.bind(this);
    this.onSearchSubmit = this.onSearchSubmit.bind(this);
    this.closeAlert = this.closeAlert.bind(this);

    this.state = {
      isFirstLoad: true,
      currentTab: "REGISTER",
      isVLElogin: false,
      redirectToMyProfile: false,
      showLoginPopup: false,
      searchTerm: "",
      isRedirecting: false,
      redirectTo: "",
      redirectToListPage: false,
      showMsg: "",
      redirectToMyscholarships: false,
      isShowMsg: false,
      isAuth: false,
      showAlert: false,
      msg: "",
      promotionalBannerList: []
    };
  }

  componentDidMount() {
    // get queryString and show alert msg
    const params = new URLSearchParams(location.search);
    const session = params.get("sessionExpired");
    const filterParams = { page: 0, length: 10, status: 1 };
    this.setState({
      showAlert: session === "true" ? true : false,
      msg: "Your session has expired. Please login again."
    });

    //validate auth from application form
    if (this.props.match.params != null && this.props.match.params.token) {
      this.setState({ isAuth: true });
      refreshTokenCall(atob(this.props.match.params.token)).then(response => {
        gblFunc.storeAuthDetails(response.data);
        let userid = response.data.userId;
        this.props.loadUserData({ userid });
      });
    }

    const isAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;

    const isLoginRoute =
      this.props.history.location.pathname === LOGIN_PAGE_ROUTE;
    const isLoginVLERoute =
      this.props.history.location.pathname === LOGIN_VLE_ROUTE;
    const isLogoutRoute = this.props.location.pathname === LOGOUT_PAGE_ROUTE;
    const isRegisterRouter =
      this.props.history.location.pathname === "/user/register";
    if (isLoginVLERoute) {
      this.setState({ isVLElogin: true });
    } else {
      this.setState({ isVLElogin: false });
    }

    if (
      (isAuthenticated && isLoginRoute) ||
      (isAuthenticated && isLoginVLERoute)
    ) {
      this.setState({
        redirectToMyProfile: true
      });
    } else if (isLogoutRoute) {
      if (isAuthenticated) {
        this.props.logUserOut();
      } else {
        if (typeof window !== undefined) {
          window.location.href = prodEnv ? PRODUCTION_URL : STAGING_URL;
        }
      }
    } else if (
      (!isAuthenticated && !this.state.showLoginPopup && isLoginRoute) ||
      (!isAuthenticated && !this.state.showLoginPopup && isLoginVLERoute)
    ) {
      this.setState({
        showLoginPopup: true,
        isFirstLoad: false,
        currentTab: "LOGIN",
        isRedirecting: true,
        redirectTo: "/myprofile"
      });
    } else if (
      !isAuthenticated &&
      !this.state.showLoginPopup &&
      isRegisterRouter
    ) {
      this.setState({
        showLoginPopup: true,
        currentTab: "REGISTER",
        isFirstLoad: false,
        redirectTo: "/myprofile",
        isRedirecting: true
      });
    } else if (this.props.history.location.pathname !== LOGIN_PAGE_ROUTE) {
      this.setState({
        showLoginPopup: false
      });
    }
    this.props.loadDashData();
    this.props.loadPromotionalBanner(encodeURI(JSON.stringify(filterParams)));
  }

  componentWillReceiveProps(nextProps) {
    const { location } = nextProps;
    //TODO: Add check if user has redirected from another page.
    const { userDetails } = nextProps;
    if (nextProps && nextProps.type === "FETCH_PROMOTIONAL_BANNER_SUCCEEDED") {
      if (nextProps.promotionalBanner.promotionContent.length > 0) {
        this.setState({
          promotionalBannerList: nextProps.promotionalBanner.promotionContent
        });
      }
    }
    if (
      userDetails.userRulesData &&
      this.props.match.params != null &&
      this.props.match.params.token
    ) {
      // if get lite call
      gblFunc.storeUserDetails(userDetails.userRulesData);

      //let params = new URLSearchParams(this.props.location.search);
      setTimeout(() => {
        if (
          gblFunc.getParameterByName("page_name") != null &&
          gblFunc.getParameterByName("slug") != null
        ) {
          window.location.href =
            "/" +
            gblFunc.getParameterByName("page_name") +
            "/" +
            gblFunc.getParameterByName("slug");
        } else if (gblFunc.getParameterByName("page_name") != null) {
          const pageName = gblFunc.getParameterByName("page_name");
          if (pageName === myScholarshipsRoute) {
            this.setState({
              redirectToMyscholarships: true
            });
          } else {
            window.location.href =
              "/" + gblFunc.getParameterByName("page_name");
          }
        } else {
          /********************************************* */
          if (
            nextProps.location.state &&
            nextProps.location.state.redirectToApplicationDetail
          ) {
            this.props.history.push(
              nextProps.location.state.redirectToApplicationDetail
            );
            return true;
          } else {
            /********************************************* */
            window.location.href = "/";
          }
        }
      }, 500);
    }

    if (
      nextProps.type === OPEN_LOGIN_PAGE_POPUP &&
      this.props.history.location.pathname === LOGIN_PAGE_ROUTE &&
      !this.state.showLoginPopup
    ) {
      this.setState({
        showLoginPopup: true,
        isFirstLoad: false,
        currentTab: "LOGIN",
        isRedirecting: true,
        redirectTo: "/myprofile"
      });
    } else if (
      nextProps.type === LOG_USER_OUT_SUCCEEDED &&
      this.props.location.pathname === LOGOUT_PAGE_ROUTE
    ) {
      if (typeof window !== undefined) {
        window.location.href = prodEnv ? PRODUCTION_URL : STAGING_URL;
      }
    } else if (
      nextProps.type === LOG_USER_OUT_SUCCEEDED &&
      ["CBI2", "CBS3"].includes(this.props.match.params.bsid)
    ) {
      this.props.history.push(
        `/application/${this.props.match.params.bsid}/instruction`
      );
    }

    const isRedirecting = nextProps.location.state
      ? nextProps.location.state.isRedirecting
      : false;

    if (isRedirecting) {
      const openLoginPopup = nextProps.location.state.openLoginPopup || false;
      if (openLoginPopup) {
        // Only open login popup if current pathname and pathname redirected from differ.

        const redirectTo = nextProps.location.state.from.pathname;
        if (redirectTo !== this.props.location.pathname) {
          this.setState({
            showLoginPopup: true,
            isRedirecting: true,
            redirectTo
          });
        }
      }
    }

    if (
      location &&
      location.state &&
      location.state.from === "resetPassword" &&
      location.state.isTrue &&
      this.props.history &&
      this.props.history.action === "PUSH"
    ) {
      this.setState({
        showLoginPopup: true,
        currentTab: "LOGIN"
      });
    }
  }

  openLoginPopup() {
    if (this.props.history.location.pathname === LOGIN_PAGE_ROUTE) {
      this.setState({
        showLoginPopup: true,
        currentTab: "REGISTER",
        redirectTo: "/myprofile",
        isRedirecting: true
      });
    } else {
      this.setState({
        showLoginPopup: true,
        currentTab: "REGISTER"
      });
    }
  }

  onSearchSubmit(e) {
    e.preventDefault();
    if (this.state.searchTerm == "" || this.state.searchTerm.trim() === "") {
      this.setState({
        showMsg: "Cannot Be Empty",
        isShowMsg: true
      });
      return false;
    } else {
      this.setState({
        redirectToListPage: true,
        showMsg: "",
        isShowMsg: false
      });
    }
  }

  closePopup() {
    if (this.props.location.pathname === LOGIN_PAGE_ROUTE) {
      this.props.closeLoginPagePopup();
    }
    this.setState({
      showLoginPopup: false,
      currentTab: "REGISTER"
    });
  }

  closeAlert() {
    this.setState({
      showAlert: false,
      msg: ""
    });
  }

  gtmEvent(option) {
    switch (option) {
      case 1:
        gblFunc.gaTrack.trackEvent(["Home-button", "Scholarships List"]);
        break;
      case 2:
        gblFunc.gaTrack.trackEvent(["Home-button", "Find my scholarship"]);
        break;
      case 3:
        gblFunc.gaTrack.trackEvent(["Home-link", "Class 10"]);
        break;
      case 4:
        gblFunc.gaTrack.trackEvent(["Home-link", "Class 12"]);
        break;
      case 5:
        gblFunc.gaTrack.trackEvent(["Home-link", "Graduation"]);
        break;
      case 6:
        gblFunc.gaTrack.trackEvent(["Home-link", "International"]);
        break;
      case 7:
        gblFunc.gaTrack.trackEvent(["Home-link", "Mean"]);
        break;
      case 8:
        gblFunc.gaTrack.trackEvent(["Home-link", "Merit"]);
      case 9:
        gblFunc.gaTrack.trackEvent(["Home-link", "Specially abled"]);
        break;
      case 10:
        gblFunc.gaTrack.trackEvent(["Home-link", "School"]);
        break;
      case 11:
        gblFunc.gaTrack.trackEvent(["Home-link", "college"]);
        break;
      case 12:
        gblFunc.gaTrack.trackEvent(["Home-link", "International"]);
        break;
      case 13:
        gblFunc.gaTrack.trackEvent(["Home-link", "minorities"]);
        break;
      case 14:
        gblFunc.gaTrack.trackEvent(["Home-link", "talent"]);
        break;
    }
  }

  onSearchTermChange(e) {
    this.setState({
      [e.target.id]: e.target.value
    });
  }

  render() {
    const { showLoginPopup, redirectToMyProfile, showAlert, msg } = this.state;
    const dashData = this.props.dashData
      ? this.props.dashData
      : defaultHomePageStats;
    const { userId, membershipExpiry } = gblFunc.getStoreUserDetails();
    const isValidMember = moment().isSameOrBefore(membershipExpiry, "day");
    if (this.state.redirectToMyscholarships) {
      return (
        <Redirect
          from="/"
          to={{
            pathname: `/${myScholarshipsRoute}`,
            state: { switchTab: "Redirect-To-ApplicationStatus" }
          }}
        />
      );
    }

    if (this.state.redirectToMyProfile) {
      return <Redirect to="/myprofile" />;
    }

    if (this.state.redirectToListPage) {
      return (
        <Redirect to={`/scholarships?q=${this.state.searchTerm}`} push={true} />
      );
    }

    return (
      <section>
        {this.state.isAuth ? <Auth /> : ""}
        <AlertMessage
          close={this.closeAlert}
          isShow={showAlert}
          status={false}
          msg={msg}
        />
        <Loader isLoader={this.props.showLoader} />
        <section className="home">
          <article className="popup">
            {showLoginPopup ? (
              <UserLoginRegistrationPopup
                text="Close Me"
                closePopup={this.closePopup}
                currentTab={this.state.currentTab}
                redirectTo={this.state.redirectTo}
                isRedirecting={this.state.isRedirecting}
                isVLElogin={this.state.isVLElogin}
              />
            ) : null}
          </article>
          {/* breacrum trail */}
          <Helmet />
          {/* <section className="container-fluid bannerSlider">
            <article class="imgBanner">
              <img src={`${imgBaseUrlDev}bannerImg.jpg`}/>
              <article class="container">
                <article class="bannerText">                
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>               
                </article>
              </article>               
            </article>
          </section> */}
          {/* Start promotional banners */}
          {this.state.promotionalBannerList.length > 0 ? (
            <PromotionalBannerSlider
              bannerList={this.state.promotionalBannerList}
              targetBlank="_blank"
            />
          ) : (
            ""
          )}
          <section className="container-fluid affordableWrapper equial-padding text-center">
            <section className="container">
              <section className="row">
                <article className="col-md-12">
                  <BreadCrum
                    classes={breadCrumObj["home"]["bgImage"]}
                    listOfBreadCrum={breadCrumObj["home"]["breadCrum"]}
                    title={breadCrumObj["home"]["title"]}
                    subTitle={breadCrumObj["home"]["subTitle"]}
                  />

                  <Link
                    to={{
                      pathname: "/scholarships"
                    }}
                    className="btn noShadow"
                    onClick={() => this.gtmEvent(1)}
                  >
                    Search Scholarship
                  </Link>
                  {/* <Link
                          to="/educationloan"
                          className="btn noShadow"
                          target="_blank"
                        >
                          Get Education Loan
                        </Link>  */}
                  {/* <a
                          className="btn noShadow"
                          href="https://docs.google.com/forms/d/e/1FAIpQLSevLqErMmpPUqzRewBafYes7tfjvzwXBHP5o1xHyqz5aO-k4w/viewform?usp=sf_link"
                          target="_blank"
                        >
                          {" "}
                          Education Loan
                        </a> */}
                  {/* <Link
                    to="/scholarship-conclave?utm_source=Home&utm_medium=WebButton"
                    className="btn sake noShadow"
                    target="_blank"
                  >
                    Study Abroad Exam & Fair
                  </Link> */}

                  <a
                    href="https://admission.buddy4study.com"
                    className="btn noShadow width"
                    target="_blank"
                    onClick={() =>
                      gblFunc.gaTrack.trackEvent([
                        "Study Abroad",
                        "admission.buddy4study.com"
                      ])
                    }
                  >
                    Study Abroad
                  </a>
                </article>
              </section>
            </section>
          </section>

          {/* Start Scholarship for every student section Done */}
          <section className="container-fluid scholarship-container equial-padding">
            <section className="container">
              <section className="row">
                <article className="col-md-12 text-center buddy-heading">
                  <h2 className="scholarship">Find the best-fit scholarship</h2>
                  <p>
                    Choosing the right scholarship is a daunting task. Pick
                    relevant scholarships and stand a chance to win.
                  </p>
                </article>
              </section>

              <section className="row">
                <article className="col-md-3">
                  <section className="Scholarship-block">
                    <section className="Scholarship-gd-top">
                      <img
                        src={`${imgBaseUrl}income-based-scholarships.jpg`}
                        alt="Means Based Scholarships"
                      />
                      <h2>
                        <Link
                          to="/scholarship-for/means-based-scholarships"
                          onClick={() => this.gtmEvent(7)}
                        >
                          MEANS BASED SCHOLARSHIPS
                        </Link>
                      </h2>
                    </section>
                  </section>
                </article>

                <article className="col-md-3">
                  <section className="Scholarship-block">
                    <section className="Scholarship-gd-top">
                      <img
                        src={`${imgBaseUrl}merit-based-scholarship.jpg`}
                        alt="MERIT BASED SCHOLARSHIPS"
                      />
                      <h2>
                        <Link
                          to="/scholarship-for/merit-based-scholarships"
                          onClick={() => this.gtmEvent(8)}
                        >
                          MERIT BASED SCHOLARSHIPS
                        </Link>
                      </h2>
                    </section>
                  </section>
                </article>
                <article className="col-md-3">
                  <section className="Scholarship-block">
                    <section className="Scholarship-gd-top">
                      <img
                        src={`${imgBaseUrl}PHYSICALLY-CHALLENGED-SCHOLARSHIPS.jpg`}
                        alt="Need Based Scholarships"
                      />
                      <h2>
                        <Link
                          to="/scholarship-for/physically-challenged-scholarships"
                          onClick={() => this.gtmEvent(9)}
                        >
                          NEED BASED SCHOLARSHIPS
                        </Link>
                      </h2>
                    </section>
                  </section>
                </article>

                <article className="col-md-3">
                  <section className="Scholarship-block">
                    <section className="Scholarship-gd-top">
                      <img
                        src={`${imgBaseUrl}school-scholarship.jpg`}
                        alt="SCHOOL SCHOLARSHIPS"
                      />
                      <h2>
                        <Link
                          to="/scholarship-for/school"
                          onClick={() => this.gtmEvent(10)}
                        >
                          SCHOOL SCHOLARSHIPS
                        </Link>
                      </h2>
                    </section>
                  </section>
                </article>

                <article className="col-md-3">
                  <section className="Scholarship-block">
                    <section className="Scholarship-gd-top">
                      <img
                        src={`${imgBaseUrl}COLLEGE-SCHOLARSHIPS.jpg`}
                        alt="COLLEGE SCHOLARSHIPS"
                      />
                      <h2>
                        <Link
                          to="/scholarship-for/Graduation"
                          onClick={() => this.gtmEvent(11)}
                        >
                          COLLEGE SCHOLARSHIPS
                        </Link>
                      </h2>
                    </section>
                  </section>
                </article>

                <article className="col-md-3">
                  <section className="Scholarship-block">
                    <section className="Scholarship-gd-top">
                      <img
                        src={`${imgBaseUrl}INTERNATIONAL-SCHOLARSHIPS.jpg`}
                        alt="INTERNATIONAL SCHOLARSHIPS"
                      />
                      <h2>
                        <Link
                          to="/scholarship-for/International"
                          onClick={() => this.gtmEvent(12)}
                        >
                          INTERNATIONAL SCHOLARSHIPS
                        </Link>
                      </h2>
                    </section>
                  </section>
                </article>

                <article className="col-md-3">
                  <section className="Scholarship-block">
                    <section className="Scholarship-gd-top">
                      <img
                        src={`${imgBaseUrl}MINORITIES-SCHOLARSHIPS.jpg`}
                        alt="MINORITIES SCHOLARSHIPS"
                      />
                      <h2>
                        <Link
                          to="/scholarship-for/minorities-scholarships"
                          onClick={() => this.gtmEvent(13)}
                        >
                          MINORITIES SCHOLARSHIPS
                        </Link>
                      </h2>
                    </section>
                  </section>
                </article>
                <article className="col-md-3">
                  <section className="Scholarship-block">
                    <section className="Scholarship-gd-top">
                      <img
                        src={`${imgBaseUrl}TALENT-BASED-SCHOLARSHIPS.jpg`}
                        alt="TALENT BASED SCHOLARSHIPS"
                      />
                      <h2>
                        <Link
                          to="/scholarship-for/talent-based-scholarships"
                          onClick={() => this.gtmEvent(14)}
                        >
                          TALENT BASED SCHOLARSHIPS
                        </Link>
                      </h2>
                    </section>
                  </section>
                </article>
              </section>

              {/* <UserLoginRegistrationPopup
              text="Close Me"
              closePopup={() => null}
              userIsLoggedIn={() => null}
              logUserOut={() => null}
            /> */}

              <section className="row">
                <section className="col-md-12 text-center">
                  <Link
                    to="/scholarships"
                    className="btn"
                    onClick={() => this.gtmEvent(2)}
                  >
                    Suggested Scholarships
                  </Link>
                </section>
              </section>
            </section>
          </section>

          {/* Start Our Service section */}
          <section className="container-fluid Our-Service equial-padding">
            <section className="container">
              <section className="row">
                <article className="col-md-12 text-center buddy-heading">
                  <i className="services" />
                  <h3>Services</h3>
                  <p>
                    Connecting scholarship seekers with scholarship providers
                    using a robust scholarship search engine.
                  </p>
                  <section className="row how-it-work text-center">
                    <article className="col-md-3">
                      <article className="single-work">
                        <i className="fa fa-list-alt" aria-hidden="true" />
                        <h4>Scholarship Listing</h4>
                        <p>Access to listed and recommended scholarships</p>
                      </article>
                    </article>

                    <article className="col-md-3">
                      <article className="single-work">
                        <i className="fa fa-handshake-o" aria-hidden="true" />
                        <h4>Application Support</h4>
                        <p>Form filling and application support</p>
                      </article>
                    </article>

                    <article className="col-md-3">
                      <article className="single-work">
                        <i className="fa fa-envelope" aria-hidden="true" />
                        <h4>Email &amp; SMS Alerts</h4>
                        <p>Email and SMS alerts of matching scholarships</p>
                      </article>
                    </article>

                    <article className="col-md-3">
                      <article className="single-work">
                        <i className="fa fa-university" aria-hidden="true" />
                        <h4 className="services">Institutional Services</h4>
                        <p>
                          Profile all students and apply with a single click
                        </p>
                      </article>
                    </article>
                  </section>
                </article>
              </section>
            </section>
          </section>

          {/* Start members winner section */}
          <section className="container-fluid members-winner equial-padding">
            <section className="container">
              <section className="row">
                <article className="col-md-12  text-center buddy-heading">
                  <h3>Scholarship Awardees</h3>
                  <p>
                    Beacons of hope, these scholars successfully applied for and
                    earned their scholarships from Buddy4Study platform
                  </p>
                </article>
              </section>
              <section className="row">
                <section className="col-md-4 col-sm-6">
                  <section className="awardees-cell">
                    <article className="media">
                      <article className="media-body">
                        <article className="imgWrapper">
                          <img
                            src={`${imgBaseUrl}anju2018.jpg`}
                            alt="Anju Sai Easwari-Buddy4study Scholar
                          "
                          />
                        </article>

                        <h4>Anju Sai Easwari</h4>
                        <p>Capital First MBA Scholarship 2018-2019</p>
                        <p>Indian Institute Of Management Ahmedabad</p>
                        <p>Scholarship &ndash; 2018</p>
                      </article>
                    </article>
                  </section>
                </section>
                <section className="col-md-4 col-sm-6">
                  <section className="awardees-cell">
                    <article className="media">
                      <article className="media-body">
                        <article className="imgWrapper">
                          <img
                            src={`${imgBaseUrl}viren2018.jpg`}
                            alt="Viren Dargar-Buddy4study Scholar"
                          />
                        </article>
                        <h4>Viren Dargar</h4>
                        <p>Capital First MBA Scholarship 2018-2019</p>
                        <p>Indian Institute Of Management Lucknow</p>
                        <p>Scholarship &ndash; 2018</p>
                      </article>
                    </article>
                  </section>
                </section>

                <section className="col-md-4 col-sm-6">
                  <section className="awardees-cell">
                    <article className="media">
                      <article className="media-body">
                        <article className="imgWrapper">
                          <img
                            src={`${imgBaseUrl}nisha2018.jpg`}
                            alt="Nisha Kumari-Buddy4study Scholar"
                          />
                        </article>
                        <h4>Nisha Kumari </h4>
                        <p>
                          L'Oréal India For Young Women in Science Scholarship
                          2018
                        </p>
                        <p>IIT Kharagpur </p>
                        <p>Scholarship &ndash; 2018</p>
                      </article>
                    </article>
                  </section>
                </section>
                <section className="col-md-4 col-sm-6">
                  <section className="awardees-cell">
                    <article className="media">
                      <article className="media-body">
                        <article className="imgWrapper">
                          <img
                            src={`${imgBaseUrl}vrushali2018.jpg`}
                            alt="Vrushali Varude-Buddy4study Scholar"
                          />
                        </article>
                        <h4>Vrushali Varude </h4>
                        <p>
                          L'Oréal India For Young Women in Science Scholarship
                          2018
                        </p>
                        <p>Institute of Chemical Technology</p>
                        <p>Scholarship &ndash; 2018</p>
                      </article>
                    </article>
                  </section>
                </section>
                <section className="col-md-4 col-sm-6">
                  <section className="awardees-cell">
                    <article className="media">
                      <article className="media-body">
                        <article className="imgWrapper">
                          <img
                            src={`${imgBaseUrl}arzoo2018.jpg`}
                            alt="Arzoo Priya-Buddy4study Scholar"
                          />
                        </article>
                        <h4>Arzoo Priya</h4>
                        <p>UGAM - Legrand Scholarship Program 2018-19</p>
                        <p>Chitkara University</p>
                        <p>Scholarship - 2018</p>
                      </article>
                    </article>
                  </section>
                </section>
                <section className="col-md-4 col-sm-6">
                  <section className="awardees-cell">
                    <article className="media">
                      <article className="media-body">
                        <article className="imgWrapper">
                          <img
                            src={`${imgBaseUrl}Shravani2018.jpg`}
                            alt="Shravani Shridhar Pawar-Buddy4study Scholar"
                          />
                        </article>
                        <h4>Shravani Shridhar Pawar </h4>
                        <p>
                          Merck India Charitable Trust (MICT) Scholarship
                          Program 2018-19
                        </p>
                        <p>Wilson College, Mumbai</p>
                        <p>Scholarship - 2018</p>
                      </article>
                    </article>
                  </section>
                </section>
              </section>
              <section className="row">
                <section className="col-md-12 text-center">
                  <Link to="/scholarship-result" className="btn">
                    Explore More Winners
                  </Link>
                </section>
              </section>
            </section>
          </section>

          {/* Start buddy4study Scholarship statiststics section */}
          <section className="container-fluid Scholarship-statiststics equial-padding">
            <section className="container">
              <section className="row">
                <article className="col-md-12  text-center buddy-heading">
                  <h5>Buddy4Study Scholarship Ecosystem</h5>
                  <p>
                    How Buddy4Study is bringing about a transformation in the
                    scholarship ecosystem
                  </p>
                </article>
              </section>
              <section className="row text-center">
                <section className="col-md-4">
                  <section className="helped">
                    <i className="fa fa-graduation-cap" aria-hidden="true" />
                    <span className="first">
                      {parseInt(dashData.usersHelped / 1000)}K
                    </span>
                    <p>Students Helped</p>
                  </section>
                </section>
                <section className="col-md-4">
                  <section className="helped">
                    <i className="fa fa-inr" aria-hidden="true" />
                    <span className="sec">
                      {dashData.scholarshipFundDisbursed}Cr
                    </span>
                    <p>Scholarships Disbursed</p>
                  </section>
                </section>
                <section className="col-md-4">
                  <section className="helped">
                    <i className="fa fa-users" aria-hidden="true" />
                    <span className="third">
                      {dashData.totalUser
                        ? parseInt(dashData.totalUser) +
                          parseInt(dashData.totalLegacyUsers)
                        : " "}
                    </span>
                    <p>Buddies</p>
                  </section>
                </section>
              </section>
            </section>
          </section>

          {/* Start testimonial section */}
          <section className="container-fluid testimonial team">
            <section className="container">
              <section className="row  equial-padding">
                <article className="col-md-12 text-center buddy-heading ">
                  <h3>Testimonials</h3>
                </article>
              </section>
              <section className="row">
                <article className="col-md-12">
                  <section id="testimonials">
                    <TestimonialSlider />
                  </section>
                </article>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default HomePage;
