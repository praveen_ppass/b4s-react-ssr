import React, { Component } from "react";
import Slider from "react-slick";
import gblFunc from "../../../globals/globalFunctions";
class bannerSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bannerList: props.bannerList
    };
  }

  bannerUrlRedirect(url, target) {
    window.open(url, target);
  }

  render() {
    const settings = {
      dots: true,
      className: "center",
      infinite: true,
      autoplay: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      cssEase: "linear",
      speed: 500,
      pauseOnHover: true
    };
    const targetBlank = this.props.targetBlank;
    const { bannerList, isMobileSlideBanner } = this.state;
    gblFunc.gaTrack.trackEvent(["Scholarship", "Scholarship Details"]);
    return (
      <section className="homepage-banner">
        {bannerList != null && (
          <Slider {...settings}>
            {this.state.bannerList.map(banner => {
              return (
                <article key={banner.id}>
                  <span
                    className="cursorPointer"
                    onClick={this.bannerUrlRedirect.bind(
                      this,
                      banner.targetUrl,
                      targetBlank
                    )}
                  >
                    <img src={banner.logoUrl} alt={banner.name} />
                  </span>
                </article>
              );
            })}
          </Slider>
        )}
      </section>
    );
  }
}

export default bannerSlider;
