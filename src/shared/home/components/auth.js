import React, { Component } from "react";

const Auth = props => (
  <section className="momentWrapper">
    <article className="tblCell">
      <p>Please wait a moment, we are verifying your details</p>
      <article className="lds-ripple">
        <article />
        <article />
      </article>
    </article>
  </section>
);
export default Auth;
