import React, { Component } from "react";
import Slider from "react-slick";
import { imgBaseUrl } from "../../../constants/constants";

class TestimonialSlider extends Component {
  render() {
    const settings = {
      className: "center",
      centerMode: true,
      infinite: true,
      autoplay: true,
      centerPadding: "60px",
      slidesToShow: 3,
      slidesToScroll: 1,
      speed: 500,
      cssEase: "linear",
      speed: 1000,
      autoplaySpeed: 2000,

      responsive: [
        {
          breakpoint: 1001,
          settings: {
            className: "center",
            centerMode: true,
            infinite: true,
            autoplay: true,
            centerPadding: "60px",
            slidesToShow: 3,
            slidesToScroll: 1,
            speed: 500,
            cssEase: "linear",
            speed: 1000,
            autoplaySpeed: 2000
          }
        }
      ]
    };
    return (
      <section className="container-fluid team marginleft padding50">
        <section className="container ">
          <article className="row">
            <article className="col-md-12">
              <Slider {...settings}>
                <div className="box1">
                  <img
                    src={`${imgBaseUrl}shritikaBagchi.png`}
                    alt="student-pic"
                  />
                  <p>
                    <i className="fa fa-quote-left icon" aria-hidden="true" />
                    CFL scholarship of Rs 2,00,000 comes as a great financial
                    help to me. <br />
                    My father is retired and it was tough for me to get into MBA
                    <br /> programme with low family income.
                    <br />
                    I am now determined to pursue my career dreams.
                    <br />
                    <i className="fa fa-quote-right icon" aria-hidden="true" />
                    <span className="fontsize14">
                      Shritika Bagchi (Scholar)
                      <br />
                      College-Jaipuria Institute of Management, Jaipur.
                      <br /> PGDM-SM
                    </span>
                  </p>
                  <span className="border" />
                </div>
                <div className="box1">
                  <img src={`${imgBaseUrl}reemaAswani.png`} alt="student-pic" />
                  <p>
                    <i className="fa fa-quote-left icon" aria-hidden="true" />
                    I belong to a middle-class family. Owing to my father’s
                    limited income,
                    <br />
                    it was really difficult for me and my brother to pursue
                    higher education. <br />
                    Capital First Scholarship brought me much-needed <br />{" "}
                    financial help to continue my education.
                    <i className="fa fa-quote-right icon" aria-hidden="true" />
                    <span className="fontsize14">
                      Reema Aswani (Scholar)
                      <br />
                      MBA 2018-20 <br />
                      IIM Ranchi
                    </span>
                  </p>
                  <span className="border" />
                </div>
                <div className="box1">
                  <img src={`${imgBaseUrl}Madhulika.png`} alt="student-pic" />
                  <p>
                    <i className="fa fa-quote-left icon" aria-hidden="true" />
                    &nbsp; I got to know about L’Oréal scholarship from
                    Buddy4Study,
                    <br />
                    and it is a great platform. The process is simple. Every
                    <br />
                    student should connect to Buddy4Study and get the
                    <br />
                    scholarship benefits.&nbsp;
                    <i className="fa fa-quote-right icon" aria-hidden="true" />
                    <span className="fontsize14">
                      Madhulika (Scholar)
                      <br />
                      Telecommunication Engineering
                      <br /> R.V.College of Engineering, Bangalore
                    </span>
                  </p>
                  <span className="border" />
                </div>
                <div className="box1">
                  <img
                    src={`${imgBaseUrl}Sravya-Borra.png`}
                    alt="student-pic"
                  />
                  <p>
                    <i className="fa fa-quote-left icon" aria-hidden="true" />
                    &nbsp; Buddy4Study’s SMS and Email alerts helped me
                    understand the
                    <br />
                    various phases of scholarship process. Thank you,
                    <br />
                    Buddy4Study for supporting me in my tough times.&nbsp;
                    <i className="fa fa-quote-right icon" aria-hidden="true" />
                    <span className="fontsize14">
                      Sravya Borra (Scholar)
                      <br /> BE in Electronics and Communication Engineering{" "}
                      <br />
                      SRKR Engineering College, Andhra Pradesh
                    </span>
                  </p>
                  <span className="border" />
                </div>
                <div className="box1">
                  <img
                    src={`${imgBaseUrl}AnchalJaiswal.png`}
                    alt="student-pic"
                  />
                  <p>
                    <i className="fa fa-quote-left icon" aria-hidden="true" />
                    &nbsp; Buddy4Study is efficiently managing the scholarship
                    alerts
                    <br />
                    service. Email updates were the best. I came to know about
                    <br />
                    each and every step of the scholarship scheme on time.&nbsp;
                    <i className="fa fa-quote-right icon" aria-hidden="true" />
                    <span className="fontsize14">
                      Anchal Jaiswal (Scholar) <br />
                      Electronics Engineering
                      <br />
                      Sardar Patel Institute of Technology, Andheri, Mumbai
                    </span>
                  </p>
                  <span className="border" />
                </div>
                <div className="box1">
                  <img
                    src={`${imgBaseUrl}NandhiniBharthi.png`}
                    alt="student-pic"
                  />

                  <p>
                    <i className="fa fa-quote-left icon" aria-hidden="true" />
                    &nbsp; I lost my father during my school days. Buddy4Study
                    helped
                    <br />
                    me a lot. I pursued Engineering and Masters with
                    <br />
                    scholarships. I studied Aerospace engineering from NTU,
                    <br />
                    Singapore. Now I got an opportunity to do my research from
                    <br />
                    the University of Central Florida, USA.&nbsp;
                    <i className="fa fa-quote-right icon" aria-hidden="true" />
                    <span className="fontsize14">
                      Nandhini Bharthi <br />
                      (Research Fellow)
                      <br />
                      University of Central Florida, USA
                    </span>
                  </p>
                  <span className="border" />
                </div>
              </Slider>
            </article>
          </article>
        </section>
      </section>
    );
  }
}
export default TestimonialSlider;
