import {
  FETCH_DASHDATA_SUCCEEDED,
  FETCH_DASHDATA_REQUESTED,
  FETCH_DASHDATA_FAILED,
  FETCH_PROMOTIONAL_BANNER_REQUESTED,
  FETCH_PROMOTIONAL_BANNER_SUCCEEDED,
  FETCH_PROMOTIONAL_BANNER_FAILED
} from "./actions";
import {
  OPEN_LOGIN_PAGE_POPUP,
  CLOSE_LOGIN_PAGE_POPUP,
  LOG_USER_OUT,
  LOG_USER_OUT_SUCCEEDED,
  LOG_USER_OUT_FAILED
} from "../login/actions";

const initialState = {
  showLoader: false,
  isError: false,
  promotionalBanner: [],
  errorMessage: ""
};

const homeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_DASHDATA_REQUESTED:
      return Object.assign({}, state, {
        dashData: {},
        showLoader: true,
        type
      });

    case FETCH_DASHDATA_SUCCEEDED:
      return Object.assign({}, state, {
        dashData: payload,
        showLoader: false,
        type
      });

    case FETCH_DASHDATA_FAILED:
      return Object.assign({}, state, {
        dashData: null,
        showLoader: false,
        type
      });

    case FETCH_PROMOTIONAL_BANNER_REQUESTED:
      return {
        ...state,
        payload: null,
        type,
        showLoader: true,
        isError: false,
        errorMessage: ""
      };
    case FETCH_PROMOTIONAL_BANNER_SUCCEEDED:
      return {
        ...state,
        promotionalBanner: payload,
        type,
        showLoader: false,
        isError: false,
        errorMessage: ""
      };

    case FETCH_PROMOTIONAL_BANNER_FAILED:
      return {
        ...state,
        payload: payload,
        showLoader: false,
        type,
        isError: true,
        errorMessage: payload
      };

    case LOG_USER_OUT:
      return Object.assign({}, state, { type });

    case LOG_USER_OUT_SUCCEEDED:
      return Object.assign({}, state, {
        type,
        showLoader: false,
        isError: false
      });

    case LOG_USER_OUT_FAILED:
      return Object.assign({}, state, {
        type,
        showLoader: false,
        isError: true
      });

    case OPEN_LOGIN_PAGE_POPUP:
      return Object.assign({}, state, { type });

    case CLOSE_LOGIN_PAGE_POPUP:
      return Object.assign({}, state, { type });

    default:
      return state;
  }
};

export default homeReducer;
