import React, { Component } from "react";
class FAL9 extends Component {
  render() {
    return (
      <section className="container-fluid  details-page-wrapper-myanmar">
        <section className="container">
          <section className="row">
            <section className="col-md-12 padding0">
              <ul className="breadcrumb">
                <li>
                  <a href="/">Home</a>
                </li>
                <li>
                  <a href="/scholarships">Scholarship</a>
                </li>
                <li className="marginleft0">
                  <a property="url">
                    Fair and Lovely Career Foundation Nepal Scholarship 2019
                  </a>
                </li>
              </ul>
            </section>
          </section>
          <section className="row">
            <section className="col-md-12">
              <section className="row">
                <section className="moboCellOrder">
                  <article className="row flex-item">
                    <section className="col-md-9 col-sm-9 heading-static">
                      <h1 className="titleText">
                        Fair and Lovely Career Foundation Nepal Scholarship 2019
                      </h1>
                    </section>
                  </article>
                  <article className="row flex-item">
                    <article className="moboCellOrderIn">
                      <section className="col-md-9 col-sm-9 flex-item padding0">
                        <article className="panel boxPadding posR">
                          <article className="data-row">
                            <h2>
                              What is Fair and Lovely Career Foundation Nepal
                              Scholarship 2019?
                            </h2>
                            <div>
                              <p>
                                The Fair and Lovely Career Foundation Nepal
                                Scholarship 2019&nbsp;invites applications from
                                ambitious young&nbsp;women from Nepal who aim to
                                fulfil&nbsp;their dreams through education. The
                                Fair and Lovely Foundation believes that every
                                woman can achieve her dreams through education
                                and proper skills. So, this&nbsp;scholarship
                                supports the higher education of girls by
                                covering their tuition expenses.&nbsp;
                              </p>
                            </div>
                          </article>
                          <article className="data-row">
                            <h2>Who is offering this scholarship ?</h2>
                            <div>
                              <p>
                                This scholarship is being offered by&nbsp;Fair
                                and Lovely Career Foundation (Nepal).
                              </p>
                            </div>
                          </article>
                          <article className="data-row">
                            <h2>Who can apply for this scholarship ?</h2>
                            <div>
                              <p>
                                To be eligible for this scholarship, an
                                applicant must:&nbsp;
                              </p>

                              <ul>
                                <li>
                                  Be a citizen of Nepal, studying in the country
                                </li>
                                <li>Be in the age group of 16–30 years</li>
                                <li>
                                  Be studying in class 11 or class 12 at a
                                  recognized school, or pursuing graduation or
                                  postgraduation at a recognized
                                  college/university
                                </li>
                                <li>
                                  Have passed class 10 with a minimum of 60%
                                  marks
                                </li>
                                <li>
                                  Have a family income not exceeding NPR 5 lakhs
                                  per annum (Nepali currency)
                                </li>
                              </ul>
                            </div>
                          </article>
                          <article className="data-row">
                            <h2>What are the benefits ?</h2>
                            <div>
                              <p>
                                The selected girl students will be given a
                                scholarship of either NPR 50,000 or tuition fee,
                                whichever is less.
                              </p>
                            </div>
                          </article>
                          <article className="data-row">
                            <h2>How can you apply ?</h2>
                            <div>
                              <p>
                                The applications can be submitted&nbsp;through
                                the following steps:
                              </p>

                              <p>
                                <strong>Step 1:</strong>&nbsp;Click on "Apply
                                Now" and register by Gmail, Facebook or any
                                other email id.<br />
                                <strong>Step 2:&nbsp;</strong>Fill the 6-step
                                form.<br />
                                <strong>Step 3:&nbsp;</strong>Provide personal
                                details, academic details, reference details
                                etc.<br />
                                <strong>Step 4:&nbsp;</strong>The applicants
                                have to write their responses to
                                three&nbsp;subjective type questions&nbsp;in
                                about 300 words each, describing themselves, why
                                they have decided to pursue the chosen course of
                                study and how this scholarship will help them.<br />
                                <strong>Step 5:&nbsp;</strong>Submit the
                                completed application form.
                              </p>
                            </div>
                          </article>
                          <article className="data-row">
                            <h2>
                              What are the documents required for the Fair and
                              Lovely Career Foundation Nepal Scholarship 2019?
                            </h2>
                            <div>
                              <p>
                                The following documents are&nbsp;required while
                                applying for the scholarship:
                              </p>

                              <ul>
                                <li>Passport Size Photo</li>
                                <li>Citizenship&nbsp;Card</li>
                                <li>Class 10 marks sheet</li>
                                <li>
                                  Marks sheet of the last qualified examination
                                </li>
                                <li>Latest college fee receipt</li>
                                <li>Admission&nbsp;letter</li>
                              </ul>

                              <p>
                                <strong>Note:</strong>&nbsp;If the girl student
                                is studying in post graduation courses, the
                                marks of graduation will also be considered.
                              </p>
                            </div>
                          </article>
                          <article className="data-row">
                            <h2>What are the selection criteria ?</h2>
                            <div>
                              <ul>
                                <li>
                                  Application: Girl candidates need to fill an
                                  online application form, where they:
                                  <ul>
                                    <li>
                                      Need to provide personal details, academic
                                      details, etc
                                    </li>
                                    <li>
                                      Have to answer three subjective questions
                                      related to&nbsp;themselves only
                                    </li>
                                  </ul>
                                </li>
                                <li>
                                  Shortlist: The selected applications will get
                                  a percentile ranking based on merit
                                  and&nbsp;family income
                                </li>
                                <li>
                                  Interview: Based on the shortlisting,
                                  candidates will be selected for the final
                                  telephonic interview round
                                </li>
                              </ul>
                            </div>
                          </article>
                          <article className="data-row">
                            <h2>What are the important dates ?</h2>

                            <div>
                              <p>
                                <strong>Tentative Dates are as follows:</strong>
                              </p>

                              <ul>
                                <li>Scholarship Application opening date: May 16, 2019</li>
                                <li>Scholarship Application closing day (Online): December 31, 2019</li>
                                <li>Shortlisting and finalist declaration: January 8, 2019</li>
                                <li> Finalist Telephonic Round interview Slot booking: January 12, 2019 to January 19, 2019</li>
                                <li> Telephonic Interview round: January 18, 2019 to January 28, 2019</li>
                                <li> Result declaration: First week of February 2019</li>
                                <li> Fund disbursement: End of March 2020</li>
                              </ul>

                              <p>
                                <strong>
                                  Note: The timelines are tentative and are
                                  subject to change as per the situation.
                                </strong>
                              </p>

                              <p>&nbsp;</p>
                            </div>
                          </article>

                          <article className="data-row">
                            <section className="faqs">
                              <h3>Frequently Asked Questions </h3>
                              <section className="accordion">
                                <h2>
                                  What level of education does the scholarship
                                  support?
                                </h2>
                                <p>
                                  Scholarships are available to women between 16
                                  to 30 years who wish to pursue high school
                                  (classes 11 to 12), undergraduate or
                                  post-graduate education from a recognized
                                  institution.
                                </p>
                              </section>
                              <section className="accordion">
                                <h2>
                                  Can I apply for the scholarship if I am
                                  currently in my 2nd year of Graduation?
                                </h2>
                                <p>
                                  Yes, as long as you fulfil the other
                                  eligibility criteria, you may apply for the
                                  scholarship in any year of your undergraduate
                                  or post-graduate studies.
                                </p>
                              </section>
                              <section className="accordion">
                                <h2>
                                  What are the application dates and timelines?
                                </h2>
                                <p>
                                  Online applications open on May 16, 2019 and
                                  close on October 31, 2019.
                                </p>
                              </section>
                              <section className="accordion">
                                <h2>
                                  How are the scholars selected for the
                                  scholarship?
                                </h2>
                                <p>
                                  Candidates are shortlisted based on their
                                  overall merit and need before being selected
                                  on the basis of telephonic interviews.
                                </p>
                              </section>
                              <section className="accordion">
                                <h2>
                                  When can I expect to hear back about the
                                  Scholarship?
                                </h2>
                                <p>
                                  All shortlisted candidates will be informed by
                                  November 8, 2019 and telephonic interviews
                                  will be concluded by the end of November. The
                                  final list of scholarship winners will be
                                  announced in the first week of December, 2019.
                                </p>
                              </section>
                            </section>
                          </article>
                          <article className="data-row">
                            <article className="col-md-6 nopadding">
                              <h2>Important Links</h2>
                              <ul className="optional-links-text">
                                <li>
                                  <a
                                    href="https://buddy4study.com/application/FAL9/instruction"
                                    target="_blank"
                                  >
                                    <strong>Apply online link</strong>
                                  </a>
                                </li>
                              </ul>
                            </article>
                            <article className="col-md-6 nopadding">
                              <h2>Contact details</h2>
                              <article>
                                <p>
                                  Email:
                                  <a href="mailto:fal_nepal@buddy4study.com">
                                    fal_nepal@buddy4study.com
                                  </a>
                                </p>
                              </article>
                            </article>
                          </article>
                        </article>
                        <article className="panel boxPadding disclaimer">
                          <article className="data-row">
                            <h2>Disclaimer</h2>
                            <p>
                              All the information provided here is for reference
                              purpose only. While we strive to list all
                              scholarships for benefit of students, Buddy4Study
                              does not guarantee the accuracy of the data
                              published here. For official information, please
                              refer to the official website.{" "}
                              <a href="/disclaimer">read more</a>
                            </p>
                          </article>
                        </article>
                      </section>
                      <section className="col-md-3 col-sm-3 flex-item padding0">
                        <section className="panel ">
                          <section className="left-cell-1">
                            <img
                              property="image"
                              src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/scholarship_logo/logo_10302_1556793420.png"
                              alt="IKIGAI Scholarship Program 2018-19"
                              title=""
                              className="school-logo"
                            />

                            <article className="deadlineBox">
                              <dd>
                                <span>Deadline:</span> 31-December-2019
                              </dd>
                              <a
                                href="https://buddy4study.com/application/FAL9/instruction"
                                target="_blank"
                              >
                                <button>Apply Now</button>
                              </a>
                            </article>
                          </section>
                          <section className="left-cell-2">
                            <article className="cellBox">
                              <article>
                                <dd>
                                  <i>Award</i>Up to NPR 50,000
                                </dd>
                                <dd>
                                  <i>Eligibility</i>Class 10,12 passed,
                                  graduation and post-graduation
                                </dd>
                                <dd>
                                  <i>Region</i>
                                  Nepal
                                </dd>
                              </article>
                            </article>
                          </section>
                        </section>
                      </section>
                    </article>
                  </article>
                </section>
              </section>
            </section>
          </section>
        </section>
      </section >
    );
  }
}

export default FAL9;
