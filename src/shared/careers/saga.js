import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
	FETCH_OPP_REQUESTED,
	FETCH_OPP_SUCCEEDED,
	FETCH_OPP_FAILED,
	FETCH_CAREER_REQUESTED,
	FETCH_CAREER_SUCCEEDED,
	FETCH_CAREER_FAILED,
	FETCH_CULTURE_DETAILS_REQUESTED,
	FETCH_CULTURE_DETAILS_SUCCEEDED,
	FETCH_CULTURE_DETAILS_FAILED,
	SUBMIT_CV_DETAILS_REQUESTED,
	SUBMIT_CV_DETAILS_SUCCEEDED,
	SUBMIT_CV_DETAILS_FAILED,
} from "./actions";

const fetchUrl = () =>
	fetchClient.get(apiUrl.careerOpportunities).then(res => {
		return res.data;
	});

function* fetchOpp(input) {
	try {
		const opp = yield call(fetchUrl);
		yield put({
			type: FETCH_OPP_SUCCEEDED,
			payload: opp
		});
	} catch (error) {
		yield put({
			type: FETCH_OPP_FAILED,
			payload: error.errorMessage
		});
	}
}

const fetchCultureUrl = input =>
	fetchClient.get(apiUrl.cultureDetails)
		.then(res => {
			return res.data;
		});

function* fetchCultureDetails(input) {
	try {
		const opp = yield call(fetchCultureUrl);
		yield put({
			type: FETCH_CULTURE_DETAILS_SUCCEEDED,
			payload: opp
		});
	} catch (error) {
		yield put({
			type: FETCH_CULTURE_DETAILS_FAILED,
			payload: error.errorMessage
		});
	}
}

const fetchDetailsUrl = input =>
	fetchClient.get(`${apiUrl.careerDetails}/${input}`, {}).then(res => {
		return res.data;
	});

function* fetchCareerDetails(input) {
	try {
		const teamDetails = yield call(fetchDetailsUrl, input.payload["id"]);

		yield put({
			type: FETCH_CAREER_SUCCEEDED,
			payload: teamDetails
		});
	} catch (error) {
		yield put({
			type: FETCH_CAREER_FAILED,
			payload: error.errorMessage
		});
	}
}

const submitCvUrl = input => {
	let dataParams = new FormData();
	dataParams.append("docFile", input.docFile);
	//dataParams.append("userDocRequest", payload.userDocRequest);
	const userId = localStorage.getItem("userId");
	const url = `${apiUrl.submitCvPopup}?candidateName=${input.name}&mobileNo=${input.mobileNumber}&emailId=${input.emailId}&experience=${input.experience}&applyFor=${input.roleApplied}`;
	return fetchClient
		.post(url, dataParams, {
			headers: { "Content-Type": "multipart/form-data" }
		})
		.then(res => {
			return res.data;
		});
}

function* submitCvDetails(input) {
	try {
		const teamDetails = yield call(submitCvUrl, input.payload["id"]);

		yield put({
			type: SUBMIT_CV_DETAILS_SUCCEEDED,
			payload: teamDetails
		});
	} catch (error) {
		yield put({
			type: SUBMIT_CV_DETAILS_FAILED,
			payload: error.errorMessage
		});
	}
}

export default function* fetchCareerSaga() {
	yield takeEvery(FETCH_OPP_REQUESTED, fetchOpp);
	yield takeEvery(FETCH_CAREER_REQUESTED, fetchCareerDetails);
	yield takeEvery(FETCH_CULTURE_DETAILS_REQUESTED, fetchCultureDetails);
	yield takeEvery(SUBMIT_CV_DETAILS_REQUESTED, submitCvDetails);
}
