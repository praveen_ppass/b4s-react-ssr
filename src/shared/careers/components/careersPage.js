import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import { imgBaseUrl } from "../../../constants/constants";
import CultureBannerSlider from "./cultureBannerSlider";
import ServerError from "../../common/components/serverError";
import Loader from "../../common/components/loader";
import AlertMessage from "../../common/components/alertMsg";
import CulturehomeSlider from "./culturehomeSlider";
import CareersPopup from "./careersPagePopup";
import { ruleRunner } from "../../../../src/validation/ruleRunner";
import { allowedDocumentExtensions } from "../../../../src/constants/constants";
//import { ruleRunner } from "../../../validation/ruleRunner";
//import { required, isEmail, minLength, isMobileNumber } from "../../../../src/validation/rules";
import {
	required,
	maxFileSize,
	hasValidExtension,
	isEmail,
	minLength,
	isMobileNumber,
	isNumeric,
	isNumericAndFloat
} from "../../../../src/validation/rules";

class Careers extends Component {
	constructor(props) {
		super(props);
		this.state = {
			opportunities: [],
			careerDetails: [],
			cultureDetails: [],
			showDetailsSection: false,
			showOpeningTab: true,
			showCultureTab: false,
			showSubmitCvPopup: false,
showPopup:false,
			validations: {
				name: "",
				emailId: "",
				mobileNo: "",
				experience: "",
				roleAppliedFor: "",
				docFile: "",
			}
		};
		this.loadData = this.loadData.bind(this);
		this.showCardDetails = this.showCardDetails.bind(this);
		this.showSubmitCvPopup = this.showSubmitCvPopup.bind(this);
		this.onCloseCvPopup = this.onCloseCvPopup.bind(this);
		//this.onCvUploaded = this.onCvUploaded.bind(this);
		this.onFormFieldChange = this.onFormFieldChange.bind(this);
		this.onSubmitCvUploadForm = this.onSubmitCvUploadForm.bind(this);
		this.tabChangeHandler = this.tabChangeHandler.bind(this);
		this.checkFormValidations = this.checkFormValidations.bind(this);
		this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
		this.close = this.close.bind(this);
	}

	componentDidMount() {
		this.props.loadOpportunities();
		this.props.loadCultureDetails();
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.oppList && nextProps.oppList.length > 0) {
			this.setState({
				opportunities: nextProps.oppList,
				id: !!nextProps && !!nextProps.oppList && nextProps.oppList
			});
		}

		if (nextProps.type === "FETCH_CAREER_SUCCEEDED") {
			this.setState({
				careerDetails:
					!!nextProps && !!nextProps.careerDetails && nextProps.careerDetails
			});
		}
		if (nextProps.type === "FETCH_CULTURE_DETAILS_SUCCEEDED") {
			this.setState({
				cultureDetails:
					!!nextProps &&
					!!nextProps.cultureDetails &&
					nextProps.cultureDetails.careerEventDto
			});
		}
		if (nextProps.type === "SUBMIT_CV_DETAILS_REQUESTED") {
			this.setState({
				showLoader: true,
			})
		}
		if (nextProps.type === "SUBMIT_CV_DETAILS_SUCCEEDED") {
			this.setState({
				status: true,
				msg: "Successfully Submitted",
				showPopup: true,
				showLoader: false,
				showSubmitCvPopup: false,
				name: "",
				mobileNo: "",
				emailId: "",
				experience: "",
				roleAppliedFor: "",
				docFile: "",
				docFileName: "",
				validations: {
					name: "",
					mobileNo: "",
					emailId: "",
					experience: "",
					roleAppliedFor: "",
					docFile: "",
				}
			})
		}
		if (nextProps.type === "SUBMIT_CV_DETAILS_FAILED") {
			this.setState({
				status: false,
				msg: "Cv could not be submitted at this moment. Please try after some time",
				showPopup: true,
				showLoader: false,
				showSubmitCvPopup: false
			})
		}
	}

	getValidationRulesObject(fieldID) {
		let validationObject = {};
		switch (fieldID) {
			case "name":
				validationObject.name = "*Name";
				validationObject.validationFunctions = [required, minLength(1)];
				return validationObject;
			case "mobileNo":
				validationObject.name = "*Mobile Number";
				validationObject.validationFunctions = [required, isMobileNumber, minLength(10)];
				return validationObject;
			case "experience":
				validationObject.name = "*Experience";
				validationObject.validationFunctions = [required, isNumericAndFloat];
				return validationObject;
			case "emailId":
				validationObject.name = "*Email address";
				validationObject.validationFunctions = [required, isEmail];
				return validationObject;

			case "roleAppliedFor":
				validationObject.name = "*Role Applied";
				validationObject.validationFunctions = [required, minLength(2)];
				return validationObject;

			case "docFile":
				validationObject.name = "*Upload Cv";
				validationObject.validationFunctions = [required];
				return validationObject;
		}
	}

	//   handleChange(event) {
	// 	const { id, value } = event.target;

	// 	const { name, validationFunctions } = this.getValidationRulesObject(id);
	// 	const validationResult = ruleRunner(
	// 	  value,
	// 	  id,
	// 	  name,
	// 	  ...validationFunctions
	// 	);

	// 	let validations = { ...this.state.validations };
	// 	validations[id] = validationResult[id];

	// 	this.setState({
	// 	  [id]: value,
	// 	  validations
	// 	});
	//   }
	close() {
		this.setState(
			{
				showPopup: false,
				msg: "",
				status: false
			}/* ,
      () => {
        if (this.state.eduStatus == "completed") {
          this.goToNextStep();
        }
      } */
		);
this.props.submitCvReset();
	}

	checkFormValidations() {
		let validations = { ...this.state.validations };
		let isFormValid = true;
		for (let key in validations) {
			let { name, validationFunctions } = this.getValidationRulesObject(key);
			let validationResult = ruleRunner(
				this.state[key],
				key,
				name,
				...validationFunctions
			);

			validations[key] = validationResult[key];

			if (validationResult[key] !== null) {
				isFormValid = false;
			}
		}

		this.setState({
			validations,
			isFormValid
		});

		return isFormValid;
	}

	tabChangeHandler(tabName) {
		switch (tabName) {
			case "Current Opening":
				this.setState({
					showOpeningTab: true,
					showCultureTab: false
				})
				break;
			case "Culture":
				this.setState({
					showOpeningTab: false,
					showCultureTab: true
				})
				break;
		}
	}

	onFormFieldChange(e) {
		const { id, value } = e.target;

		const { name, validationFunctions } = this.getValidationRulesObject(id);
		const validationResult = ruleRunner(
			value,
			id,
			name,
			...validationFunctions
		);

		let validations = { ...this.state.validations };
		validations[id] = validationResult[id];

		this.setState({
			[id]: value,
			validations
		});

		if (id == 'docFile') {
			const uploadedFile = e.target.files[0];
			//const id = event.target.id;
			// let formData = new FormData();
			// formData.append("data", uploadedFile);
			//Convert to MB.

			const sizeInMb = Math.ceil(uploadedFile.size / (1024 * 1024));
			const fileExtension = uploadedFile.name.slice(
				uploadedFile.name.lastIndexOf(".")
			);

			//const { name, validationFunctions } = this.getValidationRulesObject(id);
			let validationResult = ruleRunner(
				fileExtension,
				id,
				"File",
				hasValidExtension(allowedDocumentExtensions)
			);

			if (validationResult[id] == null) {
				validationResult = ruleRunner(
					sizeInMb,
					id,
					"Uploaded file",
					maxFileSize(1)
				);
			}

			validations = { ...this.state.validations };
			validations[id] = validationResult[id];

			this.setState({
				docFile: uploadedFile,
				validations,
				docFileName: uploadedFile.name
			});

		}


		//case "name":
		// this.setState({
		// 	[name]: value
		// })
	}

	// onCvUploaded(event) {
	// 	debugger
	// 	const uploadedFile = event.target.files[0];
	// 	const id = event.target.id;
	// 	// let formData = new FormData();
	// 	// formData.append("data", uploadedFile);
	// 	//Convert to MB.

	// 	const sizeInMb = Math.ceil(uploadedFile.size / (1024 * 1024));
	// 	const fileExtension = uploadedFile.name.slice(
	// 		uploadedFile.name.lastIndexOf(".")
	// 	);

	// 	//const { name, validationFunctions } = this.getValidationRulesObject(id);
	// 	let validationResult = ruleRunner(
	// 		fileExtension,
	// 		id,
	// 		"File",
	// 		hasValidExtension(allowedDocumentExtensions)
	// 	);

	// 	if (validationResult[id] == null) {
	// 		validationResult = ruleRunner(
	// 			sizeInMb,
	// 			id,
	// 			"Uploaded file",
	// 			maxFileSize(1)
	// 		);
	// 	}

	// 	validations = { ...this.state.validations };
	// 	validations[id] = validationResult[id];

	// 	this.setState({
	// 		[id]: uploadedFile,
	// 		validations,
	// 		docFileName: uploadedFile.name
	// 	});
	// }

	onCloseCvPopup() {
		this.setState({
			showSubmitCvPopup: false,
			name: "",
			mobileNo: "",
			emailId: "",
			experience: "",
			roleAppliedFor: "",
			docFile: "",
			docFileName: "",
			validations: {
				name: "",
				mobileNo: "",
				emailId: "",
				experience: "",
				roleAppliedFor: "",
				docFile: "",
			}
		})

	}

	onSubmitCvUploadForm(e) {
		e.preventDefault();

		if (this.checkFormValidations()) {
			let obj = {
				name: this.state.name,
				mobileNumber: this.state.mobileNo,
				experience: this.state.experience,
				roleApplied: this.state.roleAppliedFor,
				emailId: this.state.emailId,
				docFile: this.state.docFile
			}
			this.props.submitCvDetails(obj)
		}
	}
	// 	this.props.submitCvDetails(obj)
	// 	console.log('cv uploaded')
	// }else{
	// 	console.log('not uploaded')
	// 	let obj = {
	// 		name: this.state.name,
	// 		mobile: this.state.mobileNo,
	// 		experience: this.state.experience,
	// 		roleApplied: this.state.roleAppliedFor,
	// 		email: this.state.emailId,
	// 	}
	// 	this.props.submitCvDetails(obj)
	// 	//debugger
	// }

	showSubmitCvPopup(roleApplied) {
		if (!!roleApplied) {
			this.setState({
				showSubmitCvPopup: true,
				roleAppliedFor: roleApplied,
			});
		} else {
			this.setState({
				showSubmitCvPopup: true,
				roleAppliedFor: ""
			})
		}

	}

	loadData() {
		if (this.state.opportunities.length > 0) {
			return this.state.opportunities.map((item, index) => {
				return (
					<article className="col-md-12 col-sm-12" onClick={this.showCardDetails}>
						<article className="careerCart">
							<h3> {item.jobTitle}</h3>
							<ul>
								<li>
									Location <span>{item.location}</span>
								</li>
								<li>
									Experience
									<span>{item.experience}</span>
								</li>
								<li>
									Qualification
									<span className="qualification">{item.qualification}</span>
								</li>
								<li>
									No. of Position <span>{item.totalPosition}</span>
								</li>
								<li>
									<a
										//href="/career-details/14"
										onClick={() =>
											this.setState(
												{
													showDetailsSection: true
												},
												() => {
													this.props.loadCareerDetails(item.id);
												}
											)
										}
									>
										View Details
                  </a>
								</li>
							</ul>
						</article>
					</article>
				);
			});
		}
	}

	showCardDetails() {
		const careerDetails = this.state.careerDetails && this.state.careerDetails;

		if (!!careerDetails) {
			return (
				<article className="col-md-12 col-sm-12">
					<article className="carrerCartDetails">
						<h3>
							{careerDetails.designation}
							<article className="spacer"></article>
							<button onClick={(e) => this.showSubmitCvPopup(careerDetails.designation)}> Apply</button>
						</h3>
						<article className="list">
							<ul>
								<li>
									Location
									<span>{careerDetails.location}</span>
								</li>
								<li>
									Experience
									<span>{careerDetails.experience}</span>
								</li>
								<li>
									No. of Position
									<span>{careerDetails.totalPosition}</span>
								</li>
								<li>
									Qualification
									<span>{careerDetails.qualification}</span>
								</li>
							</ul>
						</article>
						<article className="htmlData">
							<p
								dangerouslySetInnerHTML={{
									__html: careerDetails.description
								}}
							></p>
							<a
								href="javascript:void(0)"
								onClick={() =>
									this.setState({
										showDetailsSection: false
									})
								}
							>
								View Less
            </a>
						</article>
					</article>
				</article>
			);
		}
	}

	render() {
		const { name, mobileNo, emailId, experience, roleAppliedFor, showSubmitCvPopup, showOpeningTab, showCultureTab, validations, docFileName } = this.state;
		return (
			<Fragment>
				<Loader isLoader={this.state.showLoader} />
				<AlertMessage
					close={this.close.bind(this)}
					isShow={this.state.showPopup}
					status={this.state.status}
					msg={this.state.msg}
				/>
				{showSubmitCvPopup &&
					<CareersPopup
						name={name}
						mobileNo={mobileNo}
						emailId={emailId}
						experience={experience}
						roleAppliedFor={roleAppliedFor}
						onCloseCvPopup={this.onCloseCvPopup}
						onFormFieldChange={this.onFormFieldChange}
						onCvUploaded={this.onCvUploaded}
						onSubmitCvUploadForm={this.onSubmitCvUploadForm}
						validations={validations}
						docFileName={docFileName}
					/>}
				<section className="careers">
					<CulturehomeSlider />
					<section className="container-fluid tabCarrer">
						<section className="container">
							<section className="row">
								<article className="col-md-12 col-sm-12">
									<article className="button-wrapper">
										<span
											className={`button ${showOpeningTab ? "active" : ""}`}
											onClick={() => this.tabChangeHandler("Current Opening")}
										>
											Current Opening
                  </span>
										<span
											className={`button ${showCultureTab ? "active" : ""}`}
											onClick={() => this.tabChangeHandler("Culture")}
										>
											Culture
                  </span>
									</article>
								</article>
							</section>
						</section>
					</section>

					{showOpeningTab && (
						<Openings
							{...this.state}
							loadData={this.loadData()}
							showCardDetails={this.showCardDetails()}
						/>
					)}

					{showCultureTab && <Culture {...this.state} />}
					<section className="container-fluid msg-button-wrapper">
						<section className="container">
							<section className="row">
								<article className="col-md-12">
									<article className="emailgp">
										<p>
											You can send your resume with the job title & attached
                    resume on:{" "}
											<a href="mailto:careers@buddy4study.com">
												careers@buddy4study.com
                    </a>
										</p>
										<button onClick={() => this.showSubmitCvPopup()}>
											Submit CV for Future opening
                  </button>
									</article>
								</article>
							</section>
						</section>
					</section>
				</section>
			</Fragment>
		);
	}
}

export default Careers;

/* Start Current Opening Component */
const Openings = props => {
	return (
		<section>
			<section className="container-fluid listwork">
				<section className="container">
					<section className="row">
						<article className="col-md-12">
							<h2>why work with us?</h2>
						</article>
					</section>
					<article className="row">
						<article className="col-md-3">
							<article className="box">
								<i className="fa fa-globe" />
								<h5>We lead in the world of scholarships</h5>
								<p>Buddy4Study is India’s largest scholarship match-making platform with a vision to make quality education accessible to all.</p>
							</article>
						</article>
						<article className="col-md-3">
							<article className="box">
								<i className="fa fa-handshake-o" />
								<h5>We will help you flourish and offer unparalleled rewards</h5>
								<p>Our employees enjoy quality pay and perks, and also value the opportunity to learn from the most passionate and driven people in the industry.</p>
							</article>
						</article>
						<article className="col-md-3">
							<article className="box">
								<i className="fa fa-file-text-o" />
								<h5>We've established an open door policy</h5>
								<p>We have built a culture of trust, collaboration, communication and respect, regardless of an individual's position in the hierarchy.</p>
							</article>
						</article>
						<article className="col-md-3">
							<article className="box">
								<i className="fa fa-user" />
								<h5>We frequently organise corporate trainings and workshops</h5>
								<p>We help in updating your skills since our leaders believe that the human capital is the most valuable asset for a sustainable business journey.</p>
							</article>
						</article>
					</article>
				</section>
			</section>

			<section className="container-fluid listCareers">
				<section className="container">
					<section className="row">
						<article className="col-md-12">
							<h2>pick your passions </h2>
							<h4>current openings </h4>
						</article>
					</section>
					{!props.showDetailsSection ? (
						<section className="row">{props.loadData}</section>
					) : (
							<section className="row">{props.showCardDetails}</section>
						)}
				</section>
			</section>
		</section>
	);
};

/* Start Culture Component */
const Culture = ({ cultureDetails }) => {
	return (
		<section className="container-fluid culture">
			<section className="container">
				<section className="culturecard">
					{cultureDetails &&
						cultureDetails.length > 0 &&
						cultureDetails.map(item => {
							return (
								<article className="col-md-6 bottom-margin">
									<article className="activity">
										<h5>{item.title}</h5>
										<article className="img-wrapper">
											<CultureBannerSlider bannerList={item.careerEventFiles} />
										</article>
										<p>{item.description}</p>
									</article>
								</article>
							);
						})}
					{/* <article className="col-md-6 bottom-margin">
						<article className="activity">
							<h5>Fun & Culture</h5>
							<article className="img-wrapper"><img src={`${imgBaseUrl}b4s-group.jpg`} alt="Buddy4study" /></article>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed bibendum, nunc id pharetra ullamcorper, lorem ante aliquam purus, non rhoncus nunc lacus scelerisque lacus. Integer pellentesque congue metus, ut vestibulum tortor consequat a.</p>
						</article>
					</article>
					<article className="col-md-6 bottom-margin">
						<article className="activity">
							<h5>Parties at BUDDY4STUDY</h5>
							<article className="img-wrapper"><img src={`${imgBaseUrl}b4s-group.jpg`} alt="Buddy4study" /></article>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed bibendum, nunc id pharetra ullamcorper, lorem ante aliquam purus, non rhoncus nunc lacus scelerisque lacus. Integer pellentesque congue metus, ut vestibulum tortor consequat a.</p>
						</article>
					</article>
					<article className="col-md-6 bottom-margin">
						<article className="activity">
							<h5>Parties at BUDDY4STUDY</h5>
							<article className="img-wrapper"><img src={`${imgBaseUrl}b4s-group.jpg`} alt="Buddy4study" /></article>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed bibendum, nunc id pharetra ullamcorper, lorem ante aliquam purus, non rhoncus nunc lacus scelerisque lacus. Integer pellentesque congue metus, ut vestibulum tortor consequat a.</p>
						</article>
					</article>
				 */}
				</section>
			</section>
		</section>
	);
};

const SubmitCvPopup = props => {
	return (
		<section style={{ marginLeft: "25px" }}>
			<form>
				<article className="row">
					<input type="text" placeholder="Name of the candidate"></input>
				</article>

				<article className="row">
					<input type="number" placeholder="Mobile No."></input>
				</article>

				<article className="row">
					<input type="email" placeholder="Email Id"></input>
				</article>

				<article className="row">
					<input type="number" placeholder="Experience"></input>
				</article>

				<article className="row">
					<select placeholder="Senior Software Developer(Java)">
						<option>Senior Software Developer</option>
					</select>
				</article>

				<button type="submit">Submit</button>
			</form>
		</section>
	);
};
