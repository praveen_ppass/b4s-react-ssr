import React, { Component } from "react";
import Slider from "react-slick";
import { imgBaseUrl } from "../../../constants/constants";

export default class CulturehomeSlider extends Component {
  render() {
    var settings = {
      dots: true,
      className: "center",
      infinite: true,
      autoplay: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      cssEase: "linear",
      speed: 200,
      pauseOnHover: true
    };
    return (
      <article className="banner">
        <Slider {...settings}>
          <article className="contentBody">
            <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/banner-tech.jpg" alt="Technical Team" />
          </article>
          <article className="contentBody">
            <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/banner-content.jpg" alt="Content Team" />
          </article>
          <article className="contentBody">
            <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/banner-bd.jpg" alt="Business Development" />
          </article>

          <article className="contentBody">
            <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/banner-operations.jpg" alt="Operations Team" />
            {/* <img src={`${imgBaseUrl}career.jpg`} alt="" /> */}
          </article>
          <article className="contentBody">
            <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/banner-admin-hr.jpg" alt="Admin and HR Team" />
          </article>
          <article className="contentBody">
            <img src="https://buddy4study.s3.ap-southeast-1.amazonaws.com/static/images/banner-study-abroad.jpg" alt="Buddy4study" />
          </article>

        </Slider>
      </article>
    );
  }
}
