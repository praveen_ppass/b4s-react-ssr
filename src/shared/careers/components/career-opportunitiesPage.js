import React, { Component } from "react";
import { Link } from "react-router-dom";
import ServerError from "../../common/components/serverError";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";

class CareersOpportunities extends Component {
  constructor(props) {
    super(props);
    this.state = {
      opportunities: []
    };
  }

  componentDidMount() {
    this.props.loadOpportunities();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.oppList.length > 0) {
      this.setState({
        opportunities: nextProps.oppList
      });
    }
  }

  loadData() {
    if (this.state.opportunities.length > 0) {
      return this.state.opportunities.map((items, index) => {
        return (
          <tr key={index}>
            <td>
              <Link to={`/career-details/${items.id}`}>{items.jobTitle}</Link>
            </td>
            <td>{items.location}</td>
            <td>{items.createdAt}</td>
            <td>{items.status == 1 ? "Active" : "Inactive"}</td>
          </tr>
        );
      });
    }
  }

  render() {
    if (this.props.isError) {
      return (
        <section className="careers">
          <section className="row">
            <article className="col-md-12">
              <article className="middleComponent">
                <article className="bgcareers">
                  <h1>CAREERS</h1>
                  <p className="hidden-xs">Choose your best career path</p>
                </article>
              </article>
              <ServerError errorMessage={this.props.errorMessage} />
            </article>
          </section>
        </section>
      );
    }

    return (
      <section className="careers">
        {/* breacrum trail */}
        <section className="row">
          <article className="col-md-12">
            <article className="middleComponent">
              <article className="bgcareers">
                <h1>CAREERS</h1>
                <p className="hidden-xs">Choose your best career path</p>
              </article>
            </article>
          </article>
        </section>
        <section className="bggreay">
          <article className="container">
            <article className="row">
              <ul className="col-md-12">
                <li>
                  <Link to="/">Home</Link>
                </li>
                <li>
                  <Link to="/careers">career</Link>
                </li>
                <li>
                  <Link to="/career-opportunities">career-opportunities</Link>
                </li>
              </ul>
            </article>
          </article>
        </section>
        <section className="container-fluid career">
          <section className="container">
            <section className="row">
              <article className="col-md-12 text-center career-heading">
                <h1>Careers </h1>
                <p>
                  You can send your resume with the job title & attached resume
                  on: <a href="mailto:careers@buddy4study.com">
                    careers@buddy4study.com
                  </a>
                </p>
              </article>
            </section>
            <section className="row">
              <section className="table-responsive">
                <table className="table table-striped table-bordered table-hover">
                  <thead>
                    <tr className="active">
                      <th>
                        <b>Job Title</b>
                      </th>
                      <th>
                        <b>Location</b>
                      </th>
                      <th>
                        <b>Date</b>
                      </th>
                      <th>
                        <b>Status</b>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.loadData()}

                    {/*  <tr>
                      <td>
                        <a href="/career-details">Senior Java Developer</a>
                      </td>
                      <td>Noida</td>
                      <td>05/01/2018</td>
                      <td>Active</td>
                    </tr>
                    <tr>
                      <td>
                        <a href="/career-details">Senior Java Developer</a>
                      </td>
                      <td>Noida</td>
                      <td>05/01/2018</td>
                      <td>Active</td>
                    </tr>
                    <tr>
                      <td>
                        <a href="/career-details">Senior Java Developer</a>
                      </td>
                      <td>Noida</td>
                      <td>05/01/2018</td>
                      <td>Inactive</td>
                    </tr> */}
                  </tbody>
                </table>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default CareersOpportunities;
