import React, { Component } from "react";

class CareersPopup extends Component {
	constructor(props) {
		super(props);
		this.state = {
			listdetails: {},
			name: ""
		};
	}

	render() {
		const { name, mobileNo, emailId, experience, onCloseCvPopup, onFormFieldChange, onCvUploaded, onSubmitCvUploadForm, roleAppliedFor, validations, docFileName } = this.props;
		return (
			<article className="popup careerPopupNew">
				<article className="popupsms">
					<article className="subscribe_inner">
						<section className="modal fade modelAuthPopup1">
							<section className="modal-dialog">
								<section className="modal-content modelBg">
									<article className="modal-header">
										<button
											type="button"
											className="close btnPos"
											onClick={onCloseCvPopup}
										>
											<i>&times;</i>
										</button>
									</article>

									<article className="modal-body">
										<article className="row">
											<article className="col-md-12">
												<h3>Please fill your details</h3>
												<form
													//autoCapitalize="off"
													className="formelemt"
													onSubmit={(e) => onSubmitCvUploadForm(e)}
												>
													<section className="ctrl-wrapper">
														<article className="form-group">
															<input
																type="text"
																className="form-control"
																id="name"
																name="name"
																placeholder="Name of the candidate"
																value={name}
																onChange={(e) => onFormFieldChange(e)}
															//required
															/>
															{validations.name &&
																<dd className="error">{validations.name}</dd>}
															{/* <dd className="error">required</dd> */}
														</article>
														<article className="form-group">
															<input
																type="tel"
																className="form-control"
																maxLength="10"
																minLength="10"
																id="mobileNo"
																name="mobileNo"
																placeholder="Mobile No."
																value={mobileNo}
																onChange={(e) => onFormFieldChange(e)}
															//required
															/>
															{validations.mobileNo &&
																<dd className="error">{validations.mobileNo}</dd>
															}
															{/* 															<dd className="error">required</dd> */}
														</article>
														<article className="form-group">
															<input
																type="text"
																className="form-control"
																id="emailId"
																name="emailId"
																placeholder="Email ID"
																value={emailId}
																onChange={(e) => onFormFieldChange(e)}
															//required
															/>
															{validations.emailId &&
																<dd className="error">{validations.emailId}</dd>
															}
														</article>
														<article className="form-group">
															<input
																type="text"
																className="form-control"
																id="experience"
																name="experience"
																placeholder="Experience"
																value={experience}
																onChange={(e) => onFormFieldChange(e)}
															//required
															/>
															{validations.experience &&
																<dd className="error">{validations.experience}</dd>
															}
														</article>
														<article className="form-group">
															<input
																type="text"
																className="form-control"
																id="roleAppliedFor"
																name="role-applied-for"
																placeholder="Role Applied for"
																value={roleAppliedFor}
																onChange={(e) => onFormFieldChange(e)}
															//required
															/>
															{validations.roleAppliedFor &&
																<dd className="error">{validations.roleAppliedFor}</dd>
															}
														</article>
														<article className="form-group">
															{/* <button
																onClick={(e) => this.props.onUploadClick(e)}>
																Upload your CV
															</button> */}				<label htmlFor="docFile">
																<span className="browse-btn">Upload Resume</span>
																<input
																	type="file"
																	id="docFile"
																	onChange={(e) => onFormFieldChange(e)}
																	className="form-control"
																	name="docFile"
																	accept="file_extension|image/*"
																//required
																/>
															</label>
															{!!docFileName && !validations.docFile &&
																<p>{docFileName}{/*  <i>uploaded</i> */}</p>}
															{validations.docFile &&
																<dd className="error">{validations.docFile}</dd>
															}
														</article>
													</section>

													<article className="btn-wrapper">
														<button
															type="submit"
														>Submit</button>
													</article>
												</form>
											</article>
										</article>
									</article>
								</section>
							</section>
						</section>
					</article>
				</article>
			</article>
		);
	}
}

export default CareersPopup;
