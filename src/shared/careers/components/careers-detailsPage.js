import React, { Component } from "react";
import { Link } from "react-router-dom";
import ServerError from "../../common/components/serverError";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";

class careersDetails extends Component {
  constructor() {
    super();
    this.state = {
      details: {}
    };
  }

  componentDidMount() {
    if (this.props.match.params && this.props.match.params.id) {
      this.props.loadCareerDetails(this.props.match.params.id);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.careerDetails != null) {
      // update props and set state
      this.setState({
        details: nextProps.careerDetails
      });
    }
  }

  render() {
    if (this.props.isError) {
      return (
        <section className="careers">
          <section className="row">
            <article className="col-md-12">
              <article className="middleComponent">
                <article className="bgcareers">
                  <h1>CAREERS</h1>
                  <p className="hidden-xs">Choose your best career path</p>
                </article>
              </article>
              <ServerError errorMessage={this.props.errorMessage} />
            </article>
          </section>
        </section>
      );
    }

    return (
      <section className="careers">
        <section className="row">
          <article className="col-md-12">
            <article className="middleComponent">
              <article className="bgcareers">
                <h1>CAREERS</h1>
                <p className="hidden-xs">Choose your best career path</p>
              </article>
            </article>
          </article>
        </section>
        <section className="bggreay">
          <article className="container">
            <article className="row">
              <ul className="col-md-12">
                <li>
                  <Link to="/">Home</Link>
                </li>
                <li>
                  <Link to="/careers">career</Link>
                </li>
                <li>
                  <Link to="/career-opportunities">career-opportunities</Link>
                </li>
                <li>
                  <Link to="/career-details">career-details</Link>
                </li>
              </ul>
            </article>
          </article>
        </section>
        <section className="container-fluid career">
          <section className="container">
            <section className="row">
              <article className="col-md-12 text-center career-heading">
                <h1>Current Openings</h1>
                <p>
                  You can send your resume with the job title & attached resume
                  on:{" "}
                  <a href="mailto:careers@buddy4study.com">
                    careers@buddy4study.com
                  </a>{" "}
                </p>
              </article>
            </section>
            <section className="row">
              {
                <section className="col-md-12 col-sm-12 careerdetails">
                  <h1>Job Summary</h1>
                  <article className="key">
                    <span className="col-md-4 col-sm-6">
                      <strong>Designation:</strong>{" "}
                      {this.state.details.designation}
                    </span>{" "}
                    <span className="col-md-3 col-sm-6">
                      <strong>Location:</strong> {this.state.details.location}
                    </span>{" "}
                    <span className="col-md-4 col-sm-6">
                      <strong> Qualification:</strong>{" "}
                      {this.state.details.qualification}
                    </span>
                    <span className="col-md-4 col-sm-6">
                      <strong>Experience:</strong>{" "}
                      {this.state.details.experience}
                    </span>
                    <span className="col-md-3 col-sm-6">
                      <strong> No. of Positions:</strong>{" "}
                      {this.state.details.totalPosition}
                    </span>
                  </article>
                  {/* <h2>Primary Rolls &amp; Responsibility</h2> */}
                  <p
                    dangerouslySetInnerHTML={{
                      __html: this.state.details.description
                    }}
                  />
                </section>
              }
            </section>
          </section>
        </section>
      </section>
    );
  }
}
export default careersDetails;
