import React from "react";
import { connect } from "react-redux";
//import { fetchAboutConditions as fetchAboutAction } from "../actions";
import {
	fetchOpportunities as fetchOpportunitiesAction,
	fetchCareerDetails as fetchCareerDetailsAction,
	fetchCultureDetails as fetchCultureDetailsAction,
	submitCvDetails as submitCvDetailsAction,
submitCvReset as submitCvReset
} from "../actions";
import Careers from "../components/careersPage";

const mapStateToProps = ({ oppReducer }) => ({
	type: oppReducer.type,
	//aboutList: careers,
	oppList: oppReducer.oppList,
	isError: oppReducer.isError,
	errorMessage: oppReducer.errorMessage,
	careerDetails: oppReducer.careerDetails,
	cultureDetails: oppReducer.cultureDetails,
	showLoader: oppReducer.showLoader,
});
const mapDispatchToProps = dispatch => ({
	loadAboutConditions: () => dispatch(fetchAboutAction("pageData")),
	loadOpportunities: () => dispatch(fetchOpportunitiesAction("pageData")),
	loadCareerDetails: id => dispatch(fetchCareerDetailsAction(id)),
	loadCultureDetails: () => dispatch(fetchCultureDetailsAction("pageData")),
	submitCvDetails: (data) => dispatch(submitCvDetailsAction(data)),
	submitCvReset: (data) => dispatch(submitCvReset(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Careers);
