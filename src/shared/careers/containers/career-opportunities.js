import React from "react";
import { connect } from "react-redux";
import { fetchOpportunities as fetchOpportunitiesAction } from "../actions";
import CareerOpportunities from "../components/career-opportunitiesPage";

const mapStateToProps = ({ careers }) => ({
  oppList: careers.oppList,
  isError: careers.isError,
  errorMessage: careers.errorMessage
});
const mapDispatchToProps = dispatch => ({
  loadOpportunities: () => dispatch(fetchOpportunitiesAction("pageData"))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  CareerOpportunities
);
