import React, { Component } from "react";
import { Link } from "react-router-dom";

class kindLanding extends Component {
  render() {
    return (
      <section className="kindLanding">
        <section className="banner">
          <section className="container">
            <section className="row ">
              <div className="col-md-6">
                <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/white-logo.png" />
              </div>
              <div className="col-md-6">
                <img
                  src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/b4s-foundation.png"
                  className=" pull-right"
                />
              </div>
            </section>
          </section>
        </section>

        <section className="container">
          <section className="row">
            <article>
              <h1>Buddy2Buddy Kind Scholar Program</h1>
              <h2>
                Everyone deserves an opportunity for a higher education,
                regardless of financial status.
              </h2>
            </article>
          </section>
        </section>
        <section className="options">
          <section className="container">
            <section className="row">
              <div className="col-md-4">
                <div className="box">
                  A unique program designed for individuals & professionals to
                  start their own scholarship as their Individual Social
                  Responsibility (ISR) or as kindness to give back to society.
                </div>
              </div>
              <div className="col-md-4">
                <div className="box">
                  Contributors can choose their own scholarship name,
                  eligibility and selection criteria. They can also be involved
                  in either interview or final scholar selection process.
                </div>
              </div>
              <div className="col-md-4">
                <div className="box">
                  All contributed money will go into creation of a large
                  Buddy2Buddy scholarship corpus, which will fund scholars
                </div>
              </div>
            </section>
          </section>
          <div className="btn btn-yellow">
            <a
              href="https://goo.gl/forms/1ICP6SXLgvvoPAmy2"
              target="_blank"
              className="text-center "
            >
              Fill Interest Form
            </a>
          </div>
        </section>

        <section className="typescholar">
          <section className="container">
            <section className="row">
              <h3>Type of Scholarships you can launch</h3>
              <div className="col-md-4">
                <div className="box">
                  <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/icon3.png" />
                  <div className="heading">Memorial Scholarship</div>
                  <p>
                    A Memorial Scholarship can be started for someone who are
                    very special to you, makes a big impact in your life or
                    changes the community for good. Education is the gift that
                    lasts a lifetime. A memorial scholarship program provides a
                    meaningful way to honor that special person while carrying
                    his or her legacy forward to future generations of students.
                  </p>
                </div>
              </div>
              <div className="col-md-4">
                <div className="box">
                  <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/icon2.png" />
                  <div className="heading">Alumni (or Group) Scholarship</div>
                  <p>
                    An Alumni (or Group) Scholarship can be started by any or
                    many alumni members of a school or college to stay involved
                    and remain connected with their alma-mater. Batchmates,
                    friends can come together to create their own scholarships,
                    funded entirely by alumni donations.
                  </p>
                </div>
              </div>
              <div className="col-md-4">
                <div className="box">
                  <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/icon1.png" />
                  <div className="heading">Kind Scholarship</div>
                  <p>
                    A Kind Scholarship can be started by any professional or
                    influencer who want to give back to the society he/she
                    belongs to and support meritorious students to complete
                    their education. It can be customized as per location,
                    cause, or simply kindness.
                  </p>
                </div>
              </div>
            </section>
          </section>
        </section>
        <section className="faqs">
          <section className="container">
            <section className="row">
              <div className="col-md-12">
                <h3>Frequently Ask Question</h3>
                <div>
                  <div className="btn pull-right">
                    <a
                      href="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/Buddy2Buddy Kind Schoalrship FAQs.pdf"
                      target="_blank"
                      className="text-center "
                    >
                      Download FAQs & Answers
                    </a>
                  </div>
                </div>
                <div className="col-md-12">
                  <div className="heading">About Scholarship Process</div>

                  <p>
                    <i className="fa fa-check" /> Who can launch scholarship,
                    what are types of scholarships that can be launched?
                  </p>
                  <p>
                    <i className="fa fa-check" /> What details are required from
                    the donors?{" "}
                  </p>
                  <p>
                    <i className="fa fa-check" /> Who is an ideal recipient for
                    your scholarship?
                  </p>
                  <p>
                    <i className="fa fa-check" /> How students will be
                    shortlisted?{" "}
                  </p>
                  <p>
                    <i className="fa fa-check" /> How much amount to be given as
                    scholarship amount?{" "}
                  </p>
                  <p>
                    <i className="fa fa-check" /> How much money should you
                    allocate?{" "}
                  </p>
                  <p>
                    <i className="fa fa-check" /> How students will receive
                    scholarship fund?{" "}
                  </p>
                  <p>
                    <i className="fa fa-check" /> What is the management &
                    overall cost for administering the complete process?
                  </p>

                  <div className="heading">
                    Scholarship name, Eligibility & Selection criteria
                  </div>

                  <p>
                    <i className="fa fa-check" /> What name you can give your
                    scholarship to?{" "}
                  </p>
                  <p>
                    <i className="fa fa-check" /> How Buddy4Study verifies
                    income or socio-economic status of the potential scholars?{" "}
                  </p>
                  <p>
                    <i className="fa fa-check" /> What eligibility and selection
                    criteria you should put for your scholarship at the time of
                    scholarship establishment?{" "}
                  </p>

                  <div className="heading">
                    Donation Receipt & Tax Exemption Certificate
                  </div>

                  <p>
                    <i className="fa fa-check" /> How Income Tax Exemption works
                    for contributions towards scholarships?{" "}
                  </p>
                  <p>
                    <i className="fa fa-check" /> What is Section 80G of income
                    tax act and how does it apply to contributions towards this
                    scholarship fund?{" "}
                  </p>
                  <p>
                    <i className="fa fa-check" /> What Tax exemption benefit do
                    I get?{" "}
                  </p>
                  <p>
                    <i className="fa fa-check" /> What is the minimum amount
                    that needs to be donated to get a tax exemption?{" "}
                  </p>
                  <p>
                    <i className="fa fa-check" /> When will you get a tax
                    exemption certificate?
                  </p>
                </div>
              </div>
            </section>
          </section>
        </section>
        <section className="SupportRow">
          <section className="container">
            <section className="row">
              <article className="content">
                <article className="row">
                  <div className="pull-left">
                    <p>In case of queries, please reach out to us:</p>
                    <ul className="supportList">
                      <li>
                        <a href="mailto: ashutosh@buddy4study.com ">
                          ashutosh@buddy4study.com
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="pull-right">
                    <div className="btn btn-yellow">
                      <a
                        href="https://goo.gl/forms/1ICP6SXLgvvoPAmy2"
                        target="_blank"
                        className="text-center "
                      >
                        Fill Interest Form
                      </a>
                    </div>
                  </div>
                </article>
              </article>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default kindLanding;
