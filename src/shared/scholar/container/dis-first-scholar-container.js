import { connect } from "react-redux";
import DisFirstScholar from "../components/dis-first-scholar";
import {
  fetchBankDetail as fetchBankDetailAction,
  fetchBankSaveDetail as fetchBankSaveDetailAction,
  sendBankDetail as sendBankDetailAction,
  congsDetail as congsDetailAction,
  sendDocDetail as sendDocDetailAction,
  sendEduDetail as sendEduDetailAction,
  getEduDetail as getEduDetailAction,
  fetchRule as fetchRuleAction,
  getDocDetail as getDocDetailAction,
  sendTestimonial as sendTestimonialAction,
  sendAck as sendAckAction,
  getStepDetail as getStepDetailAction,
  completeStep as completeStepAction,
  getEduCourses as getEduCoursesAction,
  fetchDistrict as fetchDistrictAction
} from "../scholarAction";

const mapStateToProps = ({ scholarReducers, loginOrRegister }) => ({
  ...scholarReducers,
  isAuthenticated: loginOrRegister.isAuthenticated,
});
const mapDispatchToProps = dispatch => ({
  fetchBankDetail: data => dispatch(fetchBankDetailAction(data)),
  fetchBankSaveDetail: data => dispatch(fetchBankSaveDetailAction(data)),
  sendBankDetail: data => dispatch(sendBankDetailAction(data)),
  fetchCongsDetail: data => dispatch(congsDetailAction(data)),
  sendDocDetail: data => dispatch(sendDocDetailAction(data)),
  fetchDocDetail: data => dispatch(getDocDetailAction(data)),
  sendEduDetail: data => dispatch(sendEduDetailAction(data)),
  fetchEduDetail: data => dispatch(getEduDetailAction(data)),
  fetchRule: data => dispatch(fetchRuleAction(data)),
  fetchTestimonial: data => dispatch(sendTestimonialAction(data)),
  sendAckDetail: data => dispatch(sendAckAction(data)),
  fetchStepDetail: data => dispatch(getStepDetailAction(data)),
  sendCompleteStep: data => dispatch(completeStepAction(data)),
  fetchEduCourses: data => dispatch(getEduCoursesAction(data)),
  fetchDistrict: data => dispatch(fetchDistrictAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(DisFirstScholar);
