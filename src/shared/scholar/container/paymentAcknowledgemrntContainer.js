import {connect} from "react-redux"
import PaymentAcknowledgement from "../components/paymentAcknowledgement";
import {paymentAckt as paymentAcktAction} from "../scholarAction"

const mapStateToProps=({scholarReducers})=>({
    ...scholarReducers
})

const mapReceiveToProps=(dispatch)=>({
fetchPaymentAckt : data => dispatch(paymentAcktAction(data))
})

export default connect(mapStateToProps,mapReceiveToProps)(PaymentAcknowledgement)