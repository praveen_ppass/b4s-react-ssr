import React from "react";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
const tesValidationSchema = Yup.object().shape({
  desc: Yup.string()
    .nullable()
    .required("Required")
    .test("match", "Minimum 50 and Maximum 100 words required", function(desc) {
      if (
        desc &&
        desc.match(/\b\S+\b/g).length >= 50 &&
        desc.match(/\b\S+\b/g).length <= 100
      ) {
        return true;
      }
    })
});
export class Testimonial extends React.Component {
  constructor() {
    super();
    this.state = { desc: "" };
    this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount() {
    this.props.hideBackBtn("TESTIMONIAL");
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.tesimonialSuccess) {
      this.setState({ desc: "" });
    }
  }
  handleChange({ target }, cb) {
    cb(target.name, target.value);
    this.setState({ [target.name]: target.value });
  }

  render() {
    const { disbursalId, scholarshipId, isBackHide } = this.props;
    return (
      <article>
        <Formik
          initialValues={{ desc: this.state.desc }}
          onSubmit={values => {
            let finalData = {
              disbursalId,
              scholarshipId,
              data: { body: values.desc }
            };
            this.props.fetchTestimonial(finalData);
          }}
          enableReinitialize
          validationSchema={tesValidationSchema}
        >
          {({ errors, touched, values, setFieldValue }) => {
            return (
              <Form name="TestimonialForm">
                <article className="bankDeatils">
                  <h3>
                    Please answer the question given below to help us improve
                    our program and better serve society.(Minimum 50 and Maximum
                    100 words required)
                  </h3>
                  <article className="ctrl-wrapper">
                    <article className="form-group textAreaCrtl width100">
                      <label for="description">
                        How did this scholarship help you in your education?
                      </label>
                      <textarea
                        name="desc"
                        className="form-control testimonial-text-area"
                        id="desc"
                        value={this.state.desc}
                        placeholder="Enter testimonial"
                        onChange={e => this.handleChange(e, setFieldValue)}
                      />
                      {errors.desc &&
                        touched.desc && (
                          <span className="error" style={{ bottom: "107px" }}>
                            {errors.desc}
                          </span>
                        )}
                    </article>
                  </article>
                </article>
                <article className="btnWrapper">
                  {!isBackHide ? (
                    <button
                      className="btn pull-left"
                      onClick={() => this.props.prevStep("TESTIMONIAL")}
                    >
                      Back
                    </button>
                  ) : (
                    ""
                  )}
                  <input
                    type="submit"
                    className="btn pull-right"
                    value="Continue"
                  />
                </article>
              </Form>
            );
          }}
        </Formik>
      </article>
    );
  }
}
