import React, { Component } from "react";
import { imgBaseUrlDev } from "../../../constants/constants";
import gblFunc from "../../../globals/globalFunctions";
class CongMsg extends Component {
  render() {
    const {
      isCongActive,
      isThanks,
      scholarshipName,
      message,
      disbursalId,
      scholarshipId
    } = this.props;
    return (
      <section className="container-fluid scholars-new">
        <section className="container">
          <section className="row">
            <article className="col-md-12">
              <article class="msgWrapper">
                <article class="item">
                  {isCongActive == 1 && (
                    <article className="content">
                      <img
                        src={`${imgBaseUrlDev}cong-icon.png`}
                        alt="scholarship portal"
                      />
                      <h1>Congratulations!</h1>
                      {message ?
                        <article dangerouslySetInnerHTML={{
                          __html: gblFunc.replaceWithLoreal(message, true)
                        }}></article>
                        : ""}
                      <button
                        className="btn"
                        onClick={() =>
                          this.props.fetchStepDetail({
                            disbursalId,
                            scholarshipId
                          })
                        }
                      >
                        Continue
                      </button>
                    </article>
                  )}

                  {/* Congratulations!2 */}
                  {isCongActive >= 2 && (
                    <article className="content">
                      <img
                        src={`${imgBaseUrlDev}cong-icon.png`}
                        alt="scholarship portal"
                      />
                      <h1>Congratulations!</h1>
                      {message ?
                        <article dangerouslySetInnerHTML={{
                          __html: gblFunc.replaceWithLoreal(message, true)
                        }}></article>
                        : ""}
                      <button
                        className="btn"
                        onClick={() =>
                          this.props.fetchStepDetail({
                            disbursalId,
                            scholarshipId
                          })
                        }
                      >
                        Continue
                      </button>
                    </article>
                  )}
                </article>
              </article>
            </article>
          </section>
        </section>
      </section>
    );
  }
}

export default CongMsg;
