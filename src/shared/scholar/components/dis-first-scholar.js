import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import AlertMessagePopup from "../../common/components/alertMsg";
import Loader from "../../common/components/loader";
import gblFunc from "../../../globals/globalFunctions";
import CongMsg from "./cong-msg";
import { BankDetails } from "./bankDetail";
import { DocumentDetail } from "./documentDetail";
import { EducationDetail } from "./educationDetail";
import { Testimonial } from "./testimonial";
import { Acknowledgement } from "./acknowledgement";
import { ThankYou } from "./thankYou";
import {
  FETCH_BANK_DETAIL_SUCCEDED,
  FETCH_BANK_DETAIL_FAILED,
  FETCH_BANK_SAVE_DETAIL_SUCCEDED,
  FETCH_BANK_SAVE_DETAIL_FAILED,
  POST_BANK_DETAIL_SUCCEDED,
  POST_BANK_DETAIL_FAILED,
  GET_EDUCATION_COURSES_SUCCEDED,
  GET_EDUCATION_COURSES_FAILED,
  FETCH_RULE_SUCCEDED,
  FETCH_RULE_FAILED,
  POST_EDUCATION_DETAIL_SUCCEDED,
  POST_EDUCATION_DETAIL_FAILED,
  GET_EDUCATION_DETAIL_SUCCEDED,
  GET_EDUCATION_DETAIL_FAILED,
  GET_DOCUMENT_DETAIL_SUCCEDED,
  GET_DOCUMENT_DETAIL_FAILED,
  POST_DOCUMENT_DETAIL_SUCCEDED,
  POST_DOCUMENT_DETAIL_FAILED,
  POST_TESTIMONIAL_SUCCEDED,
  POST_TESTIMONIAL_FAILED,
  POST_ACKNOWLEDGEMENT_SUCCEDED,
  POST_ACKNOWLEDGEMENT_FAILED,
  GET_TAB_STEP_SUCCEDED,
  GET_TAB_STEP_FAILED,
  POST_STEP_COMPLETED_SUCCEDED,
  POST_STEP_COMPLETED_FAILED,
  FETCH_DISTRICT_SUCCEDED,
  FETCH_DISTRICT_FAILED
} from "../scholarAction";

class DisFirstScholar extends Component {
  constructor() {
    super();
    this.state = {
      bankDetail: {
        bank: "",
        district: "",
        state: "",
        ifscCode: "",
        branchName: ""
      },
      districtData: [],
      isEduEditing: true,
      courses: "",
      prevCourses: "",
      courseType: "",
      accountHolderName: "",
      accountNumber: "",
      showAlertPopup: false,
      alertMsg: "",
      isError: "",
      isCongActive: "",
      isThanks: "",
      selectedTab: "",
      isCongsActive: false,
      ruleFilter: "",
      scholarshipName: "",
      message: "",
      providerName: "",
      scholarshipId: "",
      educationData: "",
      disbursalId: "",
      docData: "",
      isBackHide: false,
      tesimonialSuccess: false,
      tabList: [],
      allTabLists: [
        "BANK",
        "EDUCATION",
        "DOCUMENTS",
        "TESTIMONIAL",
        "ACKNOWLEDGEMENT"
      ]
    };
    this.closeAlertPopup = this.closeAlertPopup.bind(this);
    this.prevStep = this.prevStep.bind(this);
    this.nextStep = this.nextStep.bind(this);
    this.goThanks = this.goThanks.bind(this);
    this.hideBackBtn = this.hideBackBtn.bind(this);
    this.fetchCourses = this.fetchCourses.bind(this);
    this.editEducation = this.editEducation.bind(this);
    this.stepCount = 1;
  }

  componentDidMount() {
    const { params } = this.props.match;
    let filterTab = [];
    let disNumber = getNestedObjKey(params, ["disNumber"]);
    let disbursalId = getNestedObjKey(params, ["disbursalId"]);
    let scholarshipName = getNestedObjKey(params, ["schName"]);
    let scholarshipId = getNestedObjKey(params, ["schId"]);
    let providerName = getNestedObjKey(params, ["provName"]);
    this.setState({
      isCongActive: disNumber,
      isThanks: disNumber,
      scholarshipName: scholarshipName,
      providerName: providerName,
      scholarshipId: scholarshipId,
      disbursalId: disbursalId,
      message: getNestedObjKey(this.props, ["location", "state", "message"])
    });
    // this.props.fetchCongsDetail();
  }
  componentWillReceiveProps(nextProps) {
    const {
      bankData,
      bankIfscData,
      ruleData,
      eduData,
      docData,
      ackData,
      stepData,
      courseData
    } = nextProps;
    switch (nextProps.type) {
      case FETCH_BANK_DETAIL_SUCCEDED:
       let bankDetailObj = {
          type:"FETCH_BANK_DETAIL_SUCCEDED",
          bankName: "",
          district: "",
          state: "",
          ifscCode: bankIfscData.ifsc,
          branchName: "",
          accountHolderName: getNestedObjKey(bankData, [
            0,
            "accountHolderName"
          ]),
          accountNumber: getNestedObjKey(bankData, [0, "accountNumber"]),
          id: getNestedObjKey(bankData, [0, "id"])
        };
        if(!!bankIfscData.bank){
           bankDetailObj = {
            type:"FETCH_BANK_DETAIL_SUCCEDED",
            bankName: bankIfscData.bank,
            district: bankIfscData.district,
            state: bankIfscData.state,
            ifscCode: bankIfscData.ifsc,
            branchName: bankIfscData.branch,
            accountHolderName: getNestedObjKey(bankData, [
              0,
              "accountHolderName"
            ]),
            accountNumber: getNestedObjKey(bankData, [0, "accountNumber"]),
            id: getNestedObjKey(bankData, [0, "id"])
          };

        }
        
        
        this.setState({
          bankDetail: bankDetailObj
        });
        break;
      case FETCH_BANK_DETAIL_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case FETCH_BANK_SAVE_DETAIL_SUCCEDED:
        let savedBankDetail = {
          bankName: getNestedObjKey(bankData, [0, "bankName"]),
          district: getNestedObjKey(bankData, [0, "district"]),
          state: getNestedObjKey(bankData, [0, "state"]),
          ifscCode: getNestedObjKey(bankData, [0, "ifscCode"]),
          id: getNestedObjKey(bankData, [0, "id"]),
          branchName: getNestedObjKey(bankData, [0, "branchName"]),
          accountHolderName: getNestedObjKey(bankData, [
            0,
            "accountHolderName"
          ]),
          accountNumber: getNestedObjKey(bankData, [0, "accountNumber"])
        };
        this.setState({
          bankDetail: savedBankDetail
        });
        break;
      case FETCH_BANK_SAVE_DETAIL_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case POST_BANK_DETAIL_SUCCEDED:
        this.nextStep();
        this.goThanks();
        break;
      case POST_BANK_DETAIL_FAILED:
        const errCode = getNestedObjKey(bankData, [
          "response",
          "data",
          "errorCode"
        ]);
        const errMsg = getNestedObjKey(bankData, [
          "response",
          "data",
          "message"
        ]);
        if (errCode == 701) {
          this.setState({
            showAlertPopup: true,
            alertMsg: errMsg,
            isError: true
          });
        } else {
          this.setState({
            showAlertPopup: true,
            alertMsg: "Server error",
            isError: true
          });
        }
        break;
      case FETCH_RULE_SUCCEDED:
        this.setState({ ruleFilter: ruleData });
        break;
      case FETCH_RULE_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case POST_EDUCATION_DETAIL_SUCCEDED:
        // this.props.fetchEduDetail();
        this.setState(
          {
            showAlertPopup: true,
            alertMsg: "Submitted successfully",
            isError: false,
            isEduEditing: false
          },
          () => {
            this.props.fetchEduDetail({
              scholarshipId: this.state.scholarshipId
            });
          }
        );
        break;
      case POST_EDUCATION_DETAIL_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case GET_EDUCATION_DETAIL_SUCCEDED:
        this.setState({
          educationData: eduData
        });

        break;
      case GET_EDUCATION_DETAIL_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case GET_DOCUMENT_DETAIL_SUCCEDED:
        this.setState({
          docData: docData
        });
        break;
      case GET_DOCUMENT_DETAIL_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case POST_DOCUMENT_DETAIL_SUCCEDED:
        this.setState(
          {
            showAlertPopup: true,
            alertMsg: "Document uploaded successfully",
            isError: false
          },
          () => {
            this.props.fetchDocDetail({
              disbursalId: this.state.disbursalId
            });
          }
        );
        break;
      case POST_DOCUMENT_DETAIL_FAILED:
        const decErrCode = getNestedObjKey(docData, [
          "response",
          "data",
          "errorCode"
        ]);
        const docErrMsg = getNestedObjKey(docData, [
          "response",
          "data",
          "message"
        ]);
        if (decErrCode == 801) {
          this.setState({
            showAlertPopup: true,
            alertMsg: docErrMsg,
            isError: true
          });
        } else {
          this.setState({
            showAlertPopup: true,
            alertMsg: "Server error",
            isError: true
          });
        }
        break;
      case POST_TESTIMONIAL_SUCCEDED:
        this.nextStep();
        this.goThanks();
        break;
      case POST_TESTIMONIAL_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case POST_ACKNOWLEDGEMENT_SUCCEDED:
        this.nextStep();
        this.goThanks();
        break;
      case POST_ACKNOWLEDGEMENT_FAILED:
        const ackErrCode = getNestedObjKey(ackData, [
          "response",
          "data",
          "errorCode"
        ]);
        const ackErrMsg = getNestedObjKey(ackData, [
          "response",
          "data",
          "message"
        ]);
        if (ackErrCode == 701) {
          this.setState({
            showAlertPopup: true,
            alertMsg: ackErrMsg,
            isError: true
          });
        } else {
          this.setState({
            showAlertPopup: true,
            alertMsg: "Server error",
            isError: true
          });
        }
        break;
      case GET_TAB_STEP_SUCCEDED:
        const { allTabLists } = this.state;
        let finalStep = [];
        let selectedStep = [];
        let steps = getNestedObjKey(stepData, ["step"]);
        for (let j = 0; j < allTabLists.length; j++) {
          if (steps.includes(allTabLists[j])) {
            selectedStep.push(allTabLists[j]);
          }
        }
        for (let i = 0; i < selectedStep.length; i++) {
          finalStep.push({ name: selectedStep[i] });
        }
        this.setState({
          isCongsActive: true,
          tabList: finalStep,
          selectedTab: selectedStep[0]
        });
        break;
      case GET_TAB_STEP_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case POST_STEP_COMPLETED_SUCCEDED:
        this.props.history.push("/myProfile/PersonalInfo");
        break;
      case POST_STEP_COMPLETED_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case GET_EDUCATION_COURSES_SUCCEDED:
        if (this.state.courseType == "degree") {
          this.setState({ courses: courseData });
        } else {
          this.setState({ prevCourses: courseData });
        }
        break;
      case GET_EDUCATION_COURSES_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
      case FETCH_DISTRICT_SUCCEDED:
        this.setState({ districtData: nextProps.districtData });
        break;
      case FETCH_DISTRICT_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: "Server error",
          isError: true
        });
        break;
    }
  }
  componentWillUnmount() {
    clearTimeout(this.timer);
  }
  goThanks() {
    const { selectedTab, tabList } = this.state;
    if (getNestedObjKey(tabList, [tabList.length - 1, "name"]) == selectedTab) {
      this.setState({ selectedTab: "THANKYOU" });
    }
  }
  closeAlertPopup() {
    this.setState({ showAlertPopup: false, alertMsg: "" });
  }
  editEducation() {
    this.setState({ isEduEditing: true });
  }
  nextStep() {
    this.setState({
      selectedTab: getNestedObjKey(this.state.tabList, [this.stepCount, "name"])
    });
    this.stepCount++;
  }
  prevStep(tabName) {
    const { tabList } = this.state;
    let index;
    tabList.map((x, i) => {
      if (x.name == tabName) {
        index = i;
      }
      return x;
    });
    this.stepCount = index;
    this.setState({
      selectedTab: getNestedObjKey(this.state.tabList, [
        this.stepCount - 1,
        "name"
      ])
    });
  }
  hideBackBtn(tabName) {
    const { tabList } = this.state;
    if (tabName == getNestedObjKey(tabList, [0, "name"])) {
      this.setState({ isBackHide: true });
    } else {
      this.setState({ isBackHide: false });
    }
  }
  fetchCourses(type, id) {
    this.setState({ courseType: type });
    this.props.fetchEduCourses({ courseId: parseInt(id) });
  }

  render() {
    const {
      selectedTab,
      isCongsActive,
      ruleFilter,
      scholarshipName,
      message,
      providerName,
      scholarshipId,
      educationData,
      disbursalId,
      docData,
      tesimonialSuccess,
      prevCourses,
      courses,
      isBackHide,
      tabList,
      isThanks
    } = this.state;
    const isAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    if (!isAuthenticated) {
      return <Redirect to="/" />;
    }
    return (
      <section className="container-fluid scholars-new">
        <section className="container equial-padding">
          <section className="row">
            <Loader isLoader={this.props.showLoader} />
            {this.state.showAlertPopup && (
              <AlertMessagePopup
                msg={this.state.alertMsg}
                isShow={this.state.showAlertPopup}
                status={!this.state.isError}
                close={this.closeAlertPopup}
              />
            )}
            {!isCongsActive && (
              <CongMsg
                isCongActive={this.state.isCongActive}
                isThanks={this.state.isThanks}
                fetchStepDetail={this.props.fetchStepDetail}
                scholarshipName={scholarshipName}
                disbursalId={disbursalId}
                scholarshipId={scholarshipId}
                message={message}
              />
            )}
            {isCongsActive && (
              <article>
                {selectedTab != "THANKYOU" ? (
                  <article className="col-md-12">
                    <article className="step">
                      <ul>
                        {tabList &&
                          tabList.length > 1 &&
                          tabList.map((t, index) => (
                            <li
                              className={selectedTab == t.name ? "active" : ""}
                            >
                              <label>{t.name}</label>
                            </li>
                          ))}
                      </ul>
                    </article>
                  </article>
                ) : (
                    ""
                  )}
                <article className="col-md-12">
                  {selectedTab == "BANK" && (
                    <BankDetails
                      bankDetail={this.state.bankDetail}
                      disbursalId={disbursalId}
                      scholarshipId={scholarshipId}
                      nextStep={this.nextStep}
                      goThanks={this.goThanks}
                      isBackHide={isBackHide}
                      hideBackBtn={this.hideBackBtn}
                      prevStep={this.prevStep}
                      fetchBankSaveDetail={this.props.fetchBankSaveDetail}
                      sendBankDetail={this.props.sendBankDetail}
                      fetchBankDetail={this.props.fetchBankDetail}
                    />
                  )}
                  {selectedTab == "DOCUMENTS" && (
                    <DocumentDetail
                      disbursalId={disbursalId}
                      scholarshipId={scholarshipId}
                      docData={docData}
                      sendDocDetail={this.props.sendDocDetail}
                      fetchDocDetail={this.props.fetchDocDetail}
                      nextStep={this.nextStep}
                      goThanks={this.goThanks}
                      isBackHide={isBackHide}
                      hideBackBtn={this.hideBackBtn}
                      prevStep={this.prevStep}
                    />
                  )}
                  {selectedTab == "EDUCATION" && (
                    <EducationDetail
                      fetchRule={this.props.fetchRule}
                      sendEduDetail={this.props.sendEduDetail}
                      fetchEduDetail={this.props.fetchEduDetail}
                      ruleFilter={ruleFilter}
                      scholarshipId={scholarshipId}
                      disbursalId={disbursalId}
                      educationData={educationData}
                      nextStep={this.nextStep}
                      isBackHide={isBackHide}
                      hideBackBtn={this.hideBackBtn}
                      prevStep={this.prevStep}
                      fetchCourses={this.fetchCourses}
                      prevCourses={prevCourses}
                      courses={courses}
                      isEduEditing={this.state.isEduEditing}
                      editEdu={this.editEducation}
                      fetchDistrict={this.props.fetchDistrict}
                      districtData={this.state.districtData}
                      goThanks={this.goThanks}
                    />
                  )}
                  {selectedTab == "TESTIMONIAL" && (
                    <Testimonial
                      fetchTestimonial={this.props.fetchTestimonial}
                      disbursalId={disbursalId}
                      scholarshipId={scholarshipId}
                      tesimonialSuccess={tesimonialSuccess}
                      nextStep={this.nextStep}
                      isBackHide={isBackHide}
                      hideBackBtn={this.hideBackBtn}
                      prevStep={this.prevStep}
                    />
                  )}
                  {selectedTab == "ACKNOWLEDGEMENT" && (
                    <Acknowledgement
                      sendAckDetail={this.props.sendAckDetail}
                      disbursalId={disbursalId}
                      scholarshipId={scholarshipId}
                      scholarshipName={scholarshipName}
                      providerName={providerName}
                      nextStep={this.nextStep}
                      isBackHide={isBackHide}
                      hideBackBtn={this.hideBackBtn}
                      prevStep={this.prevStep}
                    />
                  )}
                  {selectedTab == "THANKYOU" && (
                    <ThankYou
                      isThanks={isThanks}
                      history={this.props.history}
                      sendCompleteStep={this.props.sendCompleteStep}
                      disbursalId={disbursalId}
                      scholarshipId={scholarshipId}
                    />
                  )}
                </article>
              </article>
            )}
          </section>
        </section>
      </section>
    );
  }
}

export default DisFirstScholar;
