import React, { Component } from "react";
import { Formik, Field, Form } from "formik";
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import AlertMessage from "../../common/components/alertMsg";
import * as Yup from "yup";

const bankValidationSchema = Yup.object().shape({
  ifscCode: Yup.string()
    .nullable()
    .required("Required.")
    .matches(/^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/, "Enter valid IFSC code"),
  accountHolderName: Yup.string()
    .nullable()
    .required("Required.")
    .matches(/^[_A-z]*((-|\s)*[_A-z])*$/, "Name is not valid."),
  accountNumber: Yup.string()
    .nullable()
    .required("Required.")
    .max(16, "A/c number can't be more than 16 digits")
});
export class BankDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAlert:false,
      status: "",
      msg: "",
      bankDetail: {
        bank: "",
        district: "",
        state: "",
        ifscCode: "",
        branchName: ""
      },
      beneficiaryType: "Scholar"
    };
    this.provideFormInitialValues = this.provideFormInitialValues.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.close2=this.close2.bind(this);

  }

  componentDidMount() {
    this.props.fetchBankSaveDetail();
    this.props.hideBackBtn("BANK");
  }

  provideFormInitialValues() {
    const { bankDetail } = this.props;
    var initialValues = {
      bankName: getNestedObjKey(bankDetail, ["bankName"]),
      branchName: getNestedObjKey(bankDetail, ["branchName"]),
      state: getNestedObjKey(bankDetail, ["state"]),
      district: getNestedObjKey(bankDetail, ["district"]),
      ifscCode: getNestedObjKey(bankDetail, ["ifscCode"]),
      accountHolderName: getNestedObjKey(bankDetail, ["accountHolderName"]),
      accountNumber: getNestedObjKey(bankDetail, ["accountNumber"]),
      beneficiaryType: this.state.beneficiaryType
    };
    // if(bankDetail.type==="FETCH_BANK_DETAIL_SUCCEDED" && bankDetail.bankName===""){
    //   this.setState({
    //     showAlert: true,
    //     status:false,
    //     msg:
    //       "Your IFSC code is invalid. Please provide correct IFSC code.",
      
    //   });

    // }
    return initialValues;
  }
  componentWillReceiveProps(nextProps){
    const {bankDetail}=nextProps;
    if(this.props.bankDetail.bankName != bankDetail.bankName && bankDetail.type==="FETCH_BANK_DETAIL_SUCCEDED" && bankDetail.bankName===""){
    
      this.setState({
        showAlert: true,
        status:false,
        msg:
          "Your IFSC code is invalid. Please provide correct IFSC code.",
      
      });

    }

  }
  handleChange({ target }, cb) {
    let ifscCode = /^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/;
    if (target.name == "ifscCode" && target.value.length == 11) {
      let isValid = ifscCode.test(target.value);
      if (isValid){
        this.props.fetchBankDetail({ ifscCode: target.value });
        cb(target.name, target.value);
        this.setState({ [target.name]: target.value });
      } 
      else{
        this.setState({
          showAlert: true,
          status:false,
          msg:
            "Your IFSC code is invalid. Please provide correct IFSC code.",
         [target.name]: target.value,
         ["bankName"]:"",
         ["branchName"]:"",
         ["state"]:"",
         ["district"]:""
        });
        cb(target.name, target.value);
        cb("bankName","");
        cb("branchName","");
        cb("state","");
        cb("district","");

        
    // this.setState({ [target.name]: "" });

      }
    }
    else{
      cb(target.name, target.value);
    this.setState({ [target.name]: target.value });

    }
    
  }

  close2() {
    this.setState({
      showAlert: false,
      msg: "",
      status: false
    });
  }

  render() {
    const { isBackHide } = this.props;
    return (
      <article>
         <AlertMessage
          close={this.close2.bind(this)}
          isShow={this.state.showAlert}
          status={this.state.status}
          msg={this.state.msg}
        />
        <Formik
          initialValues={this.provideFormInitialValues()}
          onSubmit={values => {
            let finalData = {
              ...values,
              beneficiaryType: 1
            };
            if (this.props.bankDetail && this.props.bankDetail.id) {
              finalData["id"] = this.props.bankDetail.id;
            }
            this.props.sendBankDetail(finalData);
          }}
          enableReinitialize
          validationSchema={bankValidationSchema}
        >
          {({ errors, touched, values, setFieldValue }) => {
            return (
              <Form name="BankForm">
                <article className="bankDeatils">
                  <h3>Bank Details</h3>
                  <article className="ctrl-wrapper">
                    <article className="form-group">
                      <label for="scholar">Beneficiary Type</label>
                      <Field
                        name="beneficiaryType"
                        className="form-control"
                        placeholder="Enter Beneficiary Type"
                        value="Scholar"
                        disabled
                      />
                    </article>

                    <article className="form-group">
                      <label for="IFSC">IFSC Code *</label>
                      <Field
                        maxLength="11"
                        className="form-control"
                        placeholder="Enter IFSC Code"
                        name="ifscCode"
                        onChange={e => this.handleChange(e, setFieldValue)}
                      />
                      {errors.ifscCode && touched.ifscCode && (
                        <span className="error">{errors.ifscCode}</span>
                      )}
                      <a
                        className="ifsc-link"
                        rel="nofollow"
                        href="https://www.rbi.org.in/Scripts/IFSCMICRDetails.aspx"
                        target="_blank"
                      >
                        Get IFSC Code Here
                      </a>
                    </article>
                    <article className="form-group">
                      <label for="account">Account No. *</label>
                      <Field
                        type="number"
                        className="form-control"
                        placeholder="Enter Account No."
                        name="accountNumber"
                        onChange={e => this.handleChange(e, setFieldValue)}
                      />
                      {errors.accountNumber && touched.accountNumber && (
                        <span className="error">{errors.accountNumber}</span>
                      )}
                    </article>
                    <article className="form-group">
                      <label for="ahn">Account Holder Name *</label>
                      <Field
                        className="form-control"
                        placeholder="Enter Holder Account Name"
                        name="accountHolderName"
                        onChange={e => this.handleChange(e, setFieldValue)}
                      />
                      {errors.accountHolderName &&
                        touched.accountHolderName && (
                          <span className="error">
                            {errors.accountHolderName}
                          </span>
                        )}
                    </article>
                    <article className="form-group">
                      <label for="bank">Bank Name *</label>
                      <Field
                        name="bankName"
                        className="form-control"
                        placeholder="Enter Bank Name"
                        disabled
                      />
                    </article>
                    <article className="form-group">
                      <label for="email">Branch Name *</label>
                      <Field
                        name="branchName"
                        className="form-control"
                        placeholder="Enter Branch Name"
                        disabled
                      />
                    </article>
                    <article className="form-group">
                      <label for="state">State Name *</label>
                      <Field
                        className="form-control"
                        name="state"
                        placeholder="Enter State Name"
                        disabled
                      />
                    </article>

                    <article className="form-group">
                      <label for="District">District Name *</label>
                      <Field
                        className="form-control"
                        name="district"
                        placeholder="Enter District Name"
                        disabled
                      />
                    </article>
                  </article>
                </article>
                <article className="btnWrapper">
                  {!isBackHide ? (
                    <button
                      className="btn pull-left"
                      onClick={() => this.prevStep("BANK")}
                    >
                      Back
                    </button>
                  ) : (
                    ""
                  )}
                  <input
                    type="submit"
                    className="btn pull-right"
                    value="Continue"
                  />
                </article>
              </Form>
            );
          }}
        </Formik>
      </article>
    );
  }
}
