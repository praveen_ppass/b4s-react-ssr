import React from "react";
import AlertMessagePopup from "../../common/components/alertMsg";
import { getDocStatus } from "../../../constants/constants";

export class DocumentDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      fileName: "",
      id: "",
      showAlertPopup: false,
      alertMsg: "",
      isError: ""
    };
    this.closeAlertPopup = this.closeAlertPopup.bind(this);
    this.fileHandler = this.fileHandler.bind(this);
    this.submit = this.submit.bind(this);
  }
  componentDidMount() {
    this.props.hideBackBtn("DOCUMENTS");
    this.props.fetchDocDetail({
      disbursalId: this.props.disbursalId
    });
  }
  closeAlertPopup() {
    this.setState({ showAlertPopup: false, alertMsg: "" });
  }
  fileHandler(e, id) {
    const nullIfFalsy = value => value || null;
    const stringifiedData = JSON.stringify({
      documentType: id,
      documentTypeCategory: nullIfFalsy(null), // pass type Year or Semester
      documentTypeCategoryOption: nullIfFalsy(null) // pass year Value or Semester Value
    });
    const file = e.target.files[0];
    this.props.sendDocDetail({
      docFile: file,
      scholarshipId: this.props.scholarshipId,
      disbursalId: this.props.disbursalId,
      userDocRequest: stringifiedData
    });
    this.setState({ fileName: file.name, id: id });
  }
  submit(e) {
    e.preventDefault();
    let fDocs = this.props.docData.filter(x => x.location == "");
    if (fDocs.length != 0) {
      this.setState({
        showAlertPopup: true,
        alertMsg: "Please upload all documents",
        isError: true
      });
    } else {
      this.props.nextStep();
      this.props.goThanks();
    }
  }
  render() {
    const { fileName, id } = this.state;
    const { docData, isBackHide } = this.props;
    return (
      <form autocapitalize="off" onSubmit={this.submit}>
        {this.state.showAlertPopup && (
          <AlertMessagePopup
            msg={this.state.alertMsg}
            isShow={this.state.showAlertPopup}
            status={!this.state.isError}
            close={this.closeAlertPopup}
          />
        )}
        <article className="bankDeatils">
          <h3>Documents Details</h3>
          <article className="tableResponsive">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Document Name</th>
                  <th className="left40">Action</th>
                </tr>
              </thead>
              <tbody>
                {docData.length
                  ? docData.map(d => (
                      <tr>
                        <td>{d.docNameLabel}</td>
                        <td className="actionBtn">
                          {d.location ? (
                            <article className="btn-green pull-left">
                              <a
                                style={{
                                  cursor: "default",
                                  textDecoration: "none"
                                }}
                                href={d.location}
                                target="_blank"
                              >
                                <span>Download</span>
                                <i
                                  className="fa fa-download file-lbl dis-inline"
                                  aria-hidden="true"
                                />
                              </a>
                            </article>
                          ) : (
                            ""
                          )}
                          <article className="btn-green pull-left">
                            {d.location ? (
                              <article className="btn-red">
                                {" "}
                                <span> Update </span>
                              </article>
                            ) : (
                              <article className="btn-red">
                                {" "}
                                <span> Upload </span>
                              </article>
                            )}
                            <label className="pos-rel btn-red">
                              <i
                                className="fa fa-upload file-lbl dis-inline"
                                aria-hidden="true"
                              />
                              <input
                                type="file"
                                className="file-inpt"
                                onChange={e =>
                                  this.fileHandler(e, d.documentType)
                                }
                              />
                            </label>
                            {id == d.documentType ? (
                              <span
                                style={{
                                  width: "100%",
                                  textAlign: "center",
                                  display: "block"
                                }}
                              >
                                {fileName}
                              </span>
                            ) : (
                              ""
                            )}
                          </article>
                        </td>
                      </tr>
                    ))
                  : ""}
              </tbody>
            </table>
          </article>
        </article>
        <article className="btnWrapper">
          {!isBackHide ? (
            <button
              className="btn pull-left"
              onClick={() => this.props.prevStep("DOCUMENTS")}
            >
              Back
            </button>
          ) : (
            ""
          )}
          <button className="btn pull-right" type="submit">
            Continue
          </button>
        </article>
      </form>
    );
  }
}
