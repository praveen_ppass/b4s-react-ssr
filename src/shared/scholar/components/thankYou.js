import React from "react";
import { imgBaseUrlDev } from "../../../constants/constants";

export class ThankYou extends React.Component {
  constructor() {
    super();
    this.redirect = this.redirect.bind(this);
  }
  redirect() {
    const { disbursalId, scholarshipId } = this.props;
    this.props.sendCompleteStep({
      disbursalId,
      scholarshipId
    });
  }
  render() {
    const { isThanks } = this.props;
    return (
      <article>
        {isThanks == 1 && (
          <article className="content">
            <img
              src={`${imgBaseUrlDev}thankyou-scholar.png`}
              alt="scholarship portal"
            />
            <h1>Thank you</h1>
            <p>You have successfully updated your details.</p>
            <button onClick={this.redirect} className="btn">
              Go to my profile
            </button>
          </article>
        )}

        {isThanks >= 2 && (
          <article className="content">
            <img
              src={`${imgBaseUrlDev}thankyou-scholar.png`}
              alt="scholarship portal"
            />
            <h1>Thank you</h1>
            <p>You have successfully updated your details.</p>
            <button onClick={this.redirect} className="btn">
              Go to my profile
            </button>
          </article>
        )}
      </article>
    );
  }
}
