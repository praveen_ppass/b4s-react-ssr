import React from "react";
import AlertMessagePopup from "../../common/components/alertMsg";
import gblFunc from "../../../globals/globalFunctions";
export class Acknowledgement extends React.Component {
  constructor() {
    super();
    this.state = {
      ack: "",
      showAlertPopup: false,
      alertMsg: "",
      isError: "",
      ackContent: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.submit = this.submit.bind(this);
    this.closeAlertPopup = this.closeAlertPopup.bind(this);
  }
  componentDidMount() {
    const { firstName, lastName } = gblFunc.getStoreUserDetails();
    this.props.hideBackBtn("ACKNOWLEDGEMENT");
    this.setState({
      ackContent: ` <h5>
      I,
      <b>
        ${firstName}${" "}
        ${lastName}
      </b>
      hereby declare that the information provided by me in my
      <b>${
        this.props.scholarshipName
          ? gblFunc.replaceWithLoreal(this.props.scholarshipName, true)
          : ""
      }</b> application form as well
      as information to be provided in future course will be
      correct. I further agree to fully indemnify, defend and hold
      harmless the <b>${
        this.props.providerName
          ? gblFunc.replaceWithLoreal(this.props.providerName, true)
          : ""
      }</b> and its
      officers, employees, directors, shareholders, subsidiaries,
      affiliates, and/or agents from and against any and all
      liability, claims, demands, actions, damages, costs, expenses,
      proceedings or investigations, whether judicial or
      administrative in nature, and/or other losses of any kind
      whatsoever (including, without limitation, reasonable
      attorneys' fees) with respect to
      <b>${
        this.props.scholarshipName
          ? gblFunc.replaceWithLoreal(this.props.scholarshipName, true)
          : ""
      }</b>, arising or resulting from
      discontinuation/withdrawal/non-payment of the scholarship
      amount to me by <b> ${
        this.props.providerName
          ? gblFunc.replaceWithLoreal(this.props.providerName, true)
          : ""
      } </b>
    </h5>
    <h5>
      I agree to submit attested copies of my fee receipts and mark
      sheets to avail the scholarship.
    </h5>
    <h5>
      I understand that if the information provided by me is proven
      to be incorrect or if I discontinue/change the course for
      which scholarship is awarded, I shall not be eligible to
      receive any further payments as part of the scholarship
      program. I promise to maintain the highest standards of
      responsibility, integrity, honesty and transparency and to not
      indulge in any form of corruption or illegal activities. I
      acknowledge that <b>${this.props.providerName}</b> reserves the
      right to discontinue this scholarship at any time without any
      prior notice to me and nobody shall be held liable for any
      consequences thereof.
    </h5>`
    });
  }
  closeAlertPopup() {
    this.setState({ showAlertPopup: false, alertMsg: "" });
  }
  handleChange({ target }) {
    this.setState({ [target.name]: target.checked });
  }
  submit(e) {
    e.preventDefault();
    const { disbursalId, scholarshipId } = this.props;
    if (this.state.ack) {
      let final = {
        disbursalId,
        scholarshipId,
        data: { body: this.state.ackContent }
      };
      this.props.sendAckDetail(final);
    } else {
      this.setState({
        showAlertPopup: true,
        alertMsg: "Please accept the acknowledgement",
        isError: true
      });
    }
  }
  render() {
    return (
      <article className="bankDeatils">
        {this.state.showAlertPopup && (
          <AlertMessagePopup
            msg={this.state.alertMsg}
            isShow={this.state.showAlertPopup}
            status={!this.state.isError}
            close={this.closeAlertPopup}
          />
        )}
        <h3>Acknowledgement</h3>
        <article className="ctrl-wrapper boxBorder">
          <article className="form-group interest">
            <article
              dangerouslySetInnerHTML={{ __html: this.state.ackContent }}
            />
            <label>
              <input
                type="checkbox"
                name="ack"
                checked={this.state.ack ? true : false}
                onChange={this.handleChange}
              />
              <dd className="chkbox" name="" />
              <span className="red"> I accept the above declaration.</span>
            </label>
          </article>
        </article>
        <article className="btnWrapper">
          {!this.props.isBackHide ? (
            <button
              className="btn pull-left"
              onClick={() => this.props.prevStep("ACKNOWLEDGEMENT")}
            >
              Back
            </button>
          ) : (
            ""
          )}
          <button onClick={this.submit} className="btn pull-right">
            Continue
          </button>
        </article>
      </article>
    );
  }
}
