import React, { Component } from 'react';
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import AlertMessagePopup from "../../../shared/common/components/alertMsg";
import Loader from "../../common/components/loader";
import {
    PAYMENT_ACKNOWLEDGEMENT_SUCCEDED,
    PAYMENT_ACKNOWLEDGEMENT_FAILED
} from "../scholarAction"

class paymentAcknowledgement extends Component {
    constructor(){
        super()
        this.state={
            isAlertShow: false,
            alertStatus: "",
            alertMsg: "",
            isAckt:false,
            scholarshipSupEmail:""
        }
        this.closePopup = this.closePopup.bind(this)
    }

    componentDidMount() {
      if(typeof window !== undefined){
        const urlParams = new URLSearchParams(window.location.search);
        const qrParam = urlParams.get('token');
        if(qrParam){
            this.props.fetchPaymentAckt({
                token:qrParam
            })
        }
      }
    }

    componentWillReceiveProps(nextProps) {
        const {type,paymentAckt} = nextProps
        switch(type){
            case PAYMENT_ACKNOWLEDGEMENT_SUCCEDED:
                this.setState({
                    isAckt:true,
                    scholarshipSupEmail:paymentAckt.scholarshipSupportEmail
                })
                break
            case PAYMENT_ACKNOWLEDGEMENT_FAILED:
                const errCode = getNestedObjKey(paymentAckt,["response","data","errorCode"])
                if(errCode == 805){
                    this.setState({
                        isAlertShow: true,
                        alertStatus: false,
                        alertMsg: "Invalid Token"
                    })
                } else {
                    this.setState({
                        isAlertShow: true,
                        alertStatus: false,
                        alertMsg: "Server error"
                    })
                }
                break
        }
    }
    closePopup(){
        this.setState({
            isAlertShow: false,
            alertStatus: "",
            alertMsg: ""
        })
    }

    render() {
        const {isAlertShow,alertStatus,alertMsg,isAckt,scholarshipSupEmail} = this.state
        return (
            <div>
            <Loader isLoader={this.props.showLoader} />
            <AlertMessagePopup
            isShow = {isAlertShow}
            status = {alertStatus}
            msg = {alertMsg}
            close={this.closePopup}
          />

<article className="paymentAck">
<article className="content">
<i className="fa fa-check" aria-hidden="true"></i>
         <h1>Payment Acknowledgement</h1>
            {isAckt ?
                  <article>
                <p>Thanks for confirming credit of scholarship amount into your bank. </p>
                <p> For any query please reach out to <span style={{color:"blue",textDecoration:"underline"}}>{scholarshipSupEmail}</span></p>
                </article>
            : ""
                }
         
       </article>
</article>


               
              
            </div>
        );
    }
}

export default paymentAcknowledgement;