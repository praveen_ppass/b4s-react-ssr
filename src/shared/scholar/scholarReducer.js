import {
  FETCH_BANK_DETAIL_REQUESTED,
  FETCH_BANK_DETAIL_SUCCEDED,
  FETCH_BANK_DETAIL_FAILED,
  POST_BANK_DETAIL_REQUESTED,
  POST_BANK_DETAIL_SUCCEDED,
  POST_BANK_DETAIL_FAILED,
  CONGRATULATION_DETAIL_REQUESTED,
  CONGRATULATION_DETAIL_SUCCEDED,
  CONGRATULATION_DETAIL_FAILED,
  POST_DOCUMENT_DETAIL_REQUESTED,
  POST_DOCUMENT_DETAIL_SUCCEDED,
  POST_DOCUMENT_DETAIL_FAILED,
  GET_DOCUMENT_DETAIL_REQUESTED,
  GET_DOCUMENT_DETAIL_SUCCEDED,
  GET_DOCUMENT_DETAIL_FAILED,
  POST_EDUCATION_DETAIL_REQUESTED,
  POST_EDUCATION_DETAIL_SUCCEDED,
  POST_EDUCATION_DETAIL_FAILED,
  GET_EDUCATION_DETAIL_REQUESTED,
  GET_EDUCATION_DETAIL_SUCCEDED,
  GET_EDUCATION_DETAIL_FAILED,
  GET_EDUCATION_COURSES_REQUESTED,
  GET_EDUCATION_COURSES_SUCCEDED,
  GET_EDUCATION_COURSES_FAILED,
  FETCH_RULE_REQUESTED,
  FETCH_RULE_SUCCEDED,
  FETCH_RULE_FAILED,
  POST_TESTIMONIAL_REQUESTED,
  POST_TESTIMONIAL_SUCCEDED,
  POST_TESTIMONIAL_FAILED,
  POST_ACKNOWLEDGEMENT_REQUESTED,
  POST_ACKNOWLEDGEMENT_SUCCEDED,
  POST_ACKNOWLEDGEMENT_FAILED,
  GET_TAB_STEP_REQUESTED,
  GET_TAB_STEP_SUCCEDED,
  GET_TAB_STEP_FAILED,
  FETCH_BANK_SAVE_DETAIL_REQUESTED,
  FETCH_BANK_SAVE_DETAIL_SUCCEDED,
  FETCH_BANK_SAVE_DETAIL_FAILED,
  POST_STEP_COMPLETED_REQUESTED,
  POST_STEP_COMPLETED_SUCCEDED,
  POST_STEP_COMPLETED_FAILED,
  FETCH_DISTRICT_REQUESTED,
  FETCH_DISTRICT_SUCCEDED,
  FETCH_DISTRICT_FAILED,
  PAYMENT_ACKNOWLEDGEMENT_REQUESTED,
  PAYMENT_ACKNOWLEDGEMENT_SUCCEDED,
  PAYMENT_ACKNOWLEDGEMENT_FAILED
} from "./scholarAction";

const defaultState = {
  type: null,
  showLoader: false,
  bankData: "",
  congsData: "",
  docData: "",
  eduData: "",
  ruleData: "",
  testimonialData: "",
  ackData: "",
  stepData: "",
  stepComp: "",
  bankIfscData: "",
  courseData: "",
  districtData: "",
  paymentAckt:""
};

const scholarReducers = (state = defaultState, { type, payload }) => {
  switch (type) {
    case FETCH_BANK_DETAIL_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        bankIfscData: ""
      };
    case FETCH_BANK_DETAIL_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        bankIfscData: payload
      };
    case FETCH_BANK_DETAIL_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        bankIfscData: payload
      };
    case FETCH_BANK_SAVE_DETAIL_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        bankData: ""
      };
    case FETCH_BANK_SAVE_DETAIL_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        bankData: payload
      };
    case FETCH_BANK_SAVE_DETAIL_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        bankData: payload
      };
    case POST_BANK_DETAIL_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        bankData: ""
      };
    case POST_BANK_DETAIL_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        bankData: payload
      };
    case POST_BANK_DETAIL_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        bankData: payload
      };
    case CONGRATULATION_DETAIL_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        congsData: ""
      };
    case CONGRATULATION_DETAIL_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        congsData: payload
      };
    case CONGRATULATION_DETAIL_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        congsData: payload
      };
    case POST_DOCUMENT_DETAIL_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        docData: ""
      };
    case POST_DOCUMENT_DETAIL_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        docData: payload
      };
    case POST_DOCUMENT_DETAIL_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        docData: payload
      };
    case GET_DOCUMENT_DETAIL_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        docData: ""
      };
    case GET_DOCUMENT_DETAIL_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        docData: payload
      };
    case GET_DOCUMENT_DETAIL_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        docData: payload
      };
    case GET_EDUCATION_DETAIL_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        eduData: ""
      };
    case GET_EDUCATION_DETAIL_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        eduData: payload
      };
    case GET_EDUCATION_DETAIL_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        eduData: payload
      };
    case POST_EDUCATION_DETAIL_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        eduData: ""
      };
    case POST_EDUCATION_DETAIL_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        eduData: payload
      };
    case POST_EDUCATION_DETAIL_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        eduData: payload
      };
    case FETCH_RULE_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        ruleData: ""
      };
    case FETCH_RULE_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        ruleData: payload
      };
    case FETCH_RULE_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        ruleData: payload
      };
    case POST_TESTIMONIAL_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        testimonialData: ""
      };
    case POST_TESTIMONIAL_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        testimonialData: payload
      };
    case POST_TESTIMONIAL_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        testimonialData: payload
      };
    case POST_ACKNOWLEDGEMENT_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        ackData: ""
      };
    case POST_ACKNOWLEDGEMENT_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        ackData: payload
      };
    case POST_ACKNOWLEDGEMENT_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        ackData: payload
      };
    case GET_TAB_STEP_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        stepData: ""
      };
    case GET_TAB_STEP_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        stepData: payload
      };
    case GET_TAB_STEP_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        stepData: payload
      };
    case POST_STEP_COMPLETED_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        stepComp: ""
      };
    case POST_STEP_COMPLETED_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        stepComp: payload
      };
    case POST_STEP_COMPLETED_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        stepComp: payload
      };
    case GET_EDUCATION_COURSES_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        courseData: ""
      };
    case GET_EDUCATION_COURSES_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        courseData: payload
      };
    case GET_EDUCATION_COURSES_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        courseData: payload
      };
    case FETCH_DISTRICT_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        districtData: ""
      };
    case FETCH_DISTRICT_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        districtData: payload
      };
    case FETCH_DISTRICT_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        districtData: payload
      };
    case PAYMENT_ACKNOWLEDGEMENT_REQUESTED:
      return {
        ...state,
        type,
        showLoader: true,
        paymentAckt: ""
      };
    case PAYMENT_ACKNOWLEDGEMENT_SUCCEDED:
      return {
        ...state,
        type,
        showLoader: false,
        paymentAckt: payload
      };
    case PAYMENT_ACKNOWLEDGEMENT_FAILED:
      return {
        ...state,
        type,
        showLoader: false,
        paymentAckt: payload
      };
    default:
      return state;
  }
};

export default scholarReducers;
