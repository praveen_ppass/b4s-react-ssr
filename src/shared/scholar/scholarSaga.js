import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl, dependantApiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  FETCH_BANK_DETAIL_REQUESTED,
  FETCH_BANK_DETAIL_SUCCEDED,
  FETCH_BANK_DETAIL_FAILED,
  FETCH_BANK_SAVE_DETAIL_REQUESTED,
  FETCH_BANK_SAVE_DETAIL_SUCCEDED,
  FETCH_BANK_SAVE_DETAIL_FAILED,
  POST_BANK_DETAIL_REQUESTED,
  POST_BANK_DETAIL_SUCCEDED,
  POST_BANK_DETAIL_FAILED,
  CONGRATULATION_DETAIL_REQUESTED,
  CONGRATULATION_DETAIL_SUCCEDED,
  CONGRATULATION_DETAIL_FAILED,
  GET_DOCUMENT_DETAIL_REQUESTED,
  GET_DOCUMENT_DETAIL_SUCCEDED,
  GET_DOCUMENT_DETAIL_FAILED,
  POST_DOCUMENT_DETAIL_REQUESTED,
  POST_DOCUMENT_DETAIL_SUCCEDED,
  POST_DOCUMENT_DETAIL_FAILED,
  GET_EDUCATION_DETAIL_REQUESTED,
  GET_EDUCATION_DETAIL_SUCCEDED,
  GET_EDUCATION_DETAIL_FAILED,
  POST_EDUCATION_DETAIL_REQUESTED,
  POST_EDUCATION_DETAIL_SUCCEDED,
  POST_EDUCATION_DETAIL_FAILED,
  GET_EDUCATION_COURSES_REQUESTED,
  GET_EDUCATION_COURSES_SUCCEDED,
  GET_EDUCATION_COURSES_FAILED,
  FETCH_RULE_REQUESTED,
  FETCH_RULE_SUCCEDED,
  FETCH_RULE_FAILED,
  POST_TESTIMONIAL_REQUESTED,
  POST_TESTIMONIAL_SUCCEDED,
  POST_TESTIMONIAL_FAILED,
  POST_ACKNOWLEDGEMENT_REQUESTED,
  POST_ACKNOWLEDGEMENT_SUCCEDED,
  POST_ACKNOWLEDGEMENT_FAILED,
  GET_TAB_STEP_REQUESTED,
  GET_TAB_STEP_SUCCEDED,
  GET_TAB_STEP_FAILED,
  POST_STEP_COMPLETED_REQUESTED,
  POST_STEP_COMPLETED_SUCCEDED,
  POST_STEP_COMPLETED_FAILED,
  FETCH_DISTRICT_REQUESTED,
  FETCH_DISTRICT_SUCCEDED,
  FETCH_DISTRICT_FAILED,
  PAYMENT_ACKNOWLEDGEMENT_REQUESTED,
  PAYMENT_ACKNOWLEDGEMENT_SUCCEDED,
  PAYMENT_ACKNOWLEDGEMENT_FAILED
} from "./scholarAction";

// const steps = { step: ["ACKNOWLEDGEMENT", "DOCUMENTS", "EDUCATION"] };

const fetchBankDetailApi = payload => {
  return fetchClient
    .get(`${apiUrl.bankDetail}/${payload.ifscCode}`)
    .then(res => res.data);
};

function* fetchBankDetail({ payload }) {
  try {
    const response = yield call(fetchBankDetailApi, payload);
    yield put({
      type: FETCH_BANK_DETAIL_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: FETCH_BANK_DETAIL_FAILED,
      payload: err
    });
  }
}

const fetchBankSaveDetailApi = payload => {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .get(`${apiUrl.dmdcApi}/${userId}/bankDetail`)
    .then(res => res.data);
};

function* fetchBankSaveDetail({ payload }) {
  try {
    const response = yield call(fetchBankSaveDetailApi, payload);
    yield put({
      type: FETCH_BANK_SAVE_DETAIL_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: FETCH_BANK_SAVE_DETAIL_FAILED,
      payload: err
    });
  }
}

const sendBankDetailApi = payload => {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .post(`${apiUrl.dmdcApi}/${userId}/bankDetail`, payload)
    .then(res => res.data);
};

function* sendBankDetail({ payload }) {
  try {
    const response = yield call(sendBankDetailApi, payload);
    yield put({
      type: POST_BANK_DETAIL_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: POST_BANK_DETAIL_FAILED,
      payload: err
    });
  }
}

const getCongsDetailApi = payload => {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .get(`${apiUrl.dmdcApi}/${userId}/scholarship/disbursal`)
    .then(res => res.data);
};

function* getCongsDetail({ payload }) {
  try {
    const response = yield call(getCongsDetailApi, payload);
    yield put({
      type: CONGRATULATION_DETAIL_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: CONGRATULATION_DETAIL_FAILED,
      payload: err
    });
  }
}
const getDocumentDetailApi = payload => {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .get(
      `${apiUrl.educationInfo}/${userId}/disburse-cycle/${
        payload.disbursalId
      }/document`
    )
    .then(res => res.data);
};

function* getDocumentDetail({ payload }) {
  try {
    const response = yield call(getDocumentDetailApi, payload);
    yield put({
      type: GET_DOCUMENT_DETAIL_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: GET_DOCUMENT_DETAIL_FAILED,
      payload: err
    });
  }
}

const sendDocumentDetailApi = payload => {
  let dataParams = new FormData();
  dataParams.append("docFile", payload.docFile);
  dataParams.append("userDocRequest", payload.userDocRequest);
  const userId = localStorage.getItem("userId");
  return fetchClient
    .post(
      `${apiUrl.educationInfo}/${userId}/scholarship/${
        payload.scholarshipId
      }/disbursal/${payload.disbursalId}/document`,
      dataParams,
      {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }
    )
    .then(res => res.data);
};

function* sendDocumentDetail({ payload }) {
  try {
    const response = yield call(sendDocumentDetailApi, payload);
    yield put({
      type: POST_DOCUMENT_DETAIL_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: POST_DOCUMENT_DETAIL_FAILED,
      payload: err
    });
  }
}

const sendEducationDetailApi = payload => {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .post(
      `${apiUrl.educationInfo}/${userId}/scholarship/${
        payload.scholarshipId
      }/disbursal/${payload.disbursalId}/education`,
      payload.data
    )
    .then(res => res.data);
  // return true;
};

function* sendEducationDetail({ payload }) {
  try {
    const response = yield call(sendEducationDetailApi, payload);
    yield put({
      type: POST_EDUCATION_DETAIL_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: POST_EDUCATION_DETAIL_FAILED,
      payload: err
    });
  }
}

const fetchRulesApi = payload => {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .get(`${apiUrl.getRules}`, {
      params: {
        filter: JSON.stringify(payload.filter)
      }
    })
    .then(res => res.data);
};

function* fetchRules({ payload }) {
  try {
    const response = yield call(fetchRulesApi, payload);
    yield put({
      type: FETCH_RULE_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: FETCH_RULE_FAILED,
      payload: err
    });
  }
}

const fetchEduDetailApi = ({ scholarshipId }) => {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .get(
      `${apiUrl.educationInfo}/${userId}/scholarship/${scholarshipId}/education`
    )
    .then(res => res.data);
  // return eduGetData;
};

function* fetchEduDetail({ payload }) {
  try {
    const response = yield call(fetchEduDetailApi, payload);
    yield put({
      type: GET_EDUCATION_DETAIL_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: GET_EDUCATION_DETAIL_FAILED,
      payload: err
    });
  }
}

const sendTestimonialApi = payload => {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .post(
      `${apiUrl.educationInfo}/${userId}/scholarship/${
        payload.scholarshipId
      }/disbursal/${payload.disbursalId}/testimonial`,
      payload.data
    )
    .then(res => res.data);
};

function* sendTestimonial({ payload }) {
  try {
    const response = yield call(sendTestimonialApi, payload);
    yield put({
      type: POST_TESTIMONIAL_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: POST_TESTIMONIAL_FAILED,
      payload: err
    });
  }
}

const sendAckApi = payload => {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .post(
      `${apiUrl.educationInfo}/${userId}/scholarship/${
        payload.scholarshipId
      }/disbursal/${payload.disbursalId}/acknowledgment`,
      payload.data
    )
    .then(res => res.data);
};

function* sendAck({ payload }) {
  try {
    const response = yield call(sendAckApi, payload);
    yield put({
      type: POST_ACKNOWLEDGEMENT_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: POST_ACKNOWLEDGEMENT_FAILED,
      payload: err
    });
  }
}

const getTabStepApi = payload => {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .get(
      `${apiUrl.educationInfo}/${userId}/scholarship/${
        payload.scholarshipId
      }/disbursal/${payload.disbursalId}/steps`
    )
    .then(res => res.data);
};

function* getTabStep({ payload }) {
  try {
    const response = yield call(getTabStepApi, payload);
    yield put({
      type: GET_TAB_STEP_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: GET_TAB_STEP_FAILED,
      payload: err
    });
  }
}

const completedStepApi = payload => {
  const userId = localStorage.getItem("userId");
  return fetchClient
    .post(
      `${apiUrl.educationInfo}/${userId}/scholarship/${
        payload.scholarshipId
      }/disbursal/${payload.disbursalId}/stepCompleted?stepCompleted=true`,
      payload.data
    )
    .then(res => res.data);
};

function* completedStep({ payload }) {
  try {
    const response = yield call(completedStepApi, payload);
    yield put({
      type: POST_STEP_COMPLETED_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: POST_STEP_COMPLETED_FAILED,
      payload: err
    });
  }
}

const getCoursesApi = payload => {
  return fetchClient
    .get(`${apiUrl.ruleFilter}/${payload.courseId}`)
    .then(res => res.data);
};

function* getCourses({ payload }) {
  try {
    const response = yield call(getCoursesApi, payload);
    yield put({
      type: GET_EDUCATION_COURSES_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: GET_EDUCATION_COURSES_FAILED,
      payload: err
    });
  }
}

const getDistrictApi = payload => {
  return fetchClient
    .get(`${dependantApiUrl.state}/${payload.stateId}`)
    .then(res => res.data);
};

function* getDistrict({ payload }) {
  try {
    const response = yield call(getDistrictApi, payload);
    yield put({
      type: FETCH_DISTRICT_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: FETCH_DISTRICT_FAILED,
      payload: err
    });
  }
}

const getPaymentAcktApi = payload => {
  return fetchClient
    .post(`${apiUrl.paymentAckt}?token=${payload.token}`)
    .then(res => res.data);
};

function* getPaymentAckt({ payload }) {
  try {
    const response = yield call(getPaymentAcktApi, payload);
    yield put({
      type: PAYMENT_ACKNOWLEDGEMENT_SUCCEDED,
      payload: response
    });
  } catch (err) {
    yield put({
      type: PAYMENT_ACKNOWLEDGEMENT_FAILED,
      payload: err
    });
  }
}

export default function* scholarSaga() {
  yield takeEvery(FETCH_BANK_SAVE_DETAIL_REQUESTED, fetchBankSaveDetail);
  yield takeEvery(FETCH_BANK_DETAIL_REQUESTED, fetchBankDetail);
  yield takeEvery(POST_BANK_DETAIL_REQUESTED, sendBankDetail);
  yield takeEvery(CONGRATULATION_DETAIL_REQUESTED, getCongsDetail);
  yield takeEvery(POST_DOCUMENT_DETAIL_REQUESTED, sendDocumentDetail);
  yield takeEvery(GET_DOCUMENT_DETAIL_REQUESTED, getDocumentDetail);
  yield takeEvery(GET_EDUCATION_COURSES_REQUESTED, getCourses);
  yield takeEvery(POST_EDUCATION_DETAIL_REQUESTED, sendEducationDetail);
  yield takeEvery(FETCH_RULE_REQUESTED, fetchRules);
  yield takeEvery(GET_EDUCATION_DETAIL_REQUESTED, fetchEduDetail);
  yield takeEvery(POST_TESTIMONIAL_REQUESTED, sendTestimonial);
  yield takeEvery(POST_ACKNOWLEDGEMENT_REQUESTED, sendAck);
  yield takeEvery(GET_TAB_STEP_REQUESTED, getTabStep);
  yield takeEvery(POST_STEP_COMPLETED_REQUESTED, completedStep);
  yield takeEvery(FETCH_DISTRICT_REQUESTED, getDistrict);
  yield takeEvery(PAYMENT_ACKNOWLEDGEMENT_REQUESTED, getPaymentAckt);
}
