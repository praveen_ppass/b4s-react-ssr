import {
  REGISTER_USER_REQUESTED,
  REGISTER_USER_SUCCEEDED,
  REGISTER_USER_FAILED,
  POST_QUALIFICATION_REQUESTED,
  POST_QUALIFICATION_SUCCEEDED,
  POST_QUALIFICATION_FAILED,
  POST_STUDY_PLANS_REQUESTED,
  POST_STUDY_PLANS_SUCCEEDED,
  POST_STUDY_PLANS_FAILED,
  POST_BUDGET_PLANS_REQUESTED,
  POST_BUDGET_PLANS_SUCCEEDED,
  POST_BUDGET_PLANS_FAILED,
  POST_COMPETITION_SCORES_REQUESTED,
  POST_COMPETITION_SCORES_SUCCEEDED,
  POST_COMPETITION_SCORES_FAILED,
  POST_VERIFY_OTP_REQUESTED,
  POST_VERIFY_OTP_SUCCEEDED,
  POST_VERIFY_OTP_FAILED,
  REQUEST_OTP_REQUESTED,
  REQUEST_OTP_SUCCEEDED,
  REQUEST_OTP_FAILED,
  REQUEST_OTP_COUNTING_REQUESTED,
  REQUEST_OTP_COUNTING_SUCCEEDED,
  REQUEST_OTP_COUNTING_FAILED
} from "./actions";
import {
  FETCH_USER_RULES_REQUEST,
  FETCH_USER_RULES_SUCCESS,
  FETCH_USER_RULES_FAILURE
} from "../../constants/commonActions";
import { LOGIN_USER_SUCCEEDED,  GET_EXISTING_MOBILE_FAILURE,
  GET_EXISTING_MOBILE_SUCCESS,
  GET_EXISTING_MOBILE_REQUEST } from "../login/actions";
const initialState = {
  showLoader: false,
  isError: false,
  errorMessage: ""
};
const scholarshipConclaveReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    /********************** USer Registration *************************** */
    case REGISTER_USER_REQUESTED:
      return { ...state, showLoader: true, type };
    case REGISTER_USER_SUCCEEDED:
      return { ...state, type, registerResponse: payload, showLoader: false };
    case REGISTER_USER_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        errorMessage: payload,
        type,
        registerResponse: null
      };
    /********************** USer Registration *************************** */
    case FETCH_USER_RULES_REQUEST:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };
    case FETCH_USER_RULES_SUCCESS:
      return {
        ...state,
        showLoader: false,
        userRulesData: payload,
        isError: false,
        type
      };
    case FETCH_USER_RULES_FAILURE:
      return {
        ...state,
        showLoader: false,
        isError: true,
        type
      };
    /********************** USer Qualification *************************** */

    case POST_QUALIFICATION_REQUESTED:
      return { ...state, showLoader: true, type };
    case POST_QUALIFICATION_SUCCEEDED:
      return {
        ...state,
        type,
        qualificationPostResp: payload,
        showLoader: false
      };
    case POST_QUALIFICATION_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        errorMessage: payload,
        type,
        qualificationPostResp: null
      };
    /********************** USer Qualification *************************** */

    /********************** USer Future Study Plans *************************** */

    case POST_STUDY_PLANS_REQUESTED:
      return { ...state, showLoader: true, type };
    case POST_STUDY_PLANS_SUCCEEDED:
      return {
        ...state,
        type,
        studyPlansResp: payload,
        showLoader: false
      };
    case POST_STUDY_PLANS_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        errorMessage: payload,
        type,
        studyPlansResp: null
      };
    /********************** USer Future Study Plans *************************** */

    /********************** USer Study Budget Plans *************************** */

    case POST_BUDGET_PLANS_REQUESTED:
      return { ...state, showLoader: true, type };
    case POST_BUDGET_PLANS_SUCCEEDED:
      return {
        ...state,
        type,
        budgetPlansResp: payload,
        showLoader: false
      };
    case POST_BUDGET_PLANS_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        errorMessage: payload,
        type,
        budgetPlansResp: null
      };
    /********************** USer  Study Budget Plans *************************** */

    /********************** User Competition Scores *************************** */

    case POST_COMPETITION_SCORES_REQUESTED:
      return { ...state, showLoader: true, type };
    case POST_COMPETITION_SCORES_SUCCEEDED:
      return {
        ...state,
        type,
        competitionScoresResp: payload,
        showLoader: false
      };
    case POST_COMPETITION_SCORES_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        errorMessage: payload,
        type,
        competitionScoresResp: null
      };
    /********************** User Competition Scores *************************** */

    /********************** User Competition Scores *************************** */

    case POST_VERIFY_OTP_REQUESTED:
      return { ...state, showLoader: true, type };
    case POST_VERIFY_OTP_SUCCEEDED:
      return {
        ...state,
        type,
        verifyOTPResp: payload,
        showLoader: false
      };
    case LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload
      };
    case POST_VERIFY_OTP_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        errorMessage: payload,
        type,
        verifyOTPResp: null
      };
    /********************** User Competition Scores *************************** */

    /********************** User OTP Verification *************************** */

    case POST_VERIFY_OTP_REQUESTED:
      return { ...state, showLoader: true, type };
    case POST_VERIFY_OTP_SUCCEEDED:
      return {
        ...state,
        type,
        verifyOTPResp: payload,
        showLoader: false
      };
    case POST_VERIFY_OTP_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        errorMessage: payload,
        type,
        verifyOTPResp: null
      };
    case "VERIFY_TOKEN":
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload,
        isAuthenticated: true,
        userRulesPresent: false
      };
    /********************** User OTP Verification *************************** */

    /********************** User OTP Requested *************************** */

    case REQUEST_OTP_REQUESTED:
      return { ...state, showLoader: true, type };
    case REQUEST_OTP_SUCCEEDED:
      return {
        ...state,
        type,
        requestOTPResp: payload,
        showLoader: false
      };
    case REQUEST_OTP_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        errorMessage: payload,
        type,
        requestOTPResp: null
      };
    /********************** User OTP Requested *************************** */
    /********************** User OTP Counter *************************** */
    case REQUEST_OTP_COUNTING_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type
      };
    case REQUEST_OTP_COUNTING_SUCCEEDED:
      return {
        ...state,
        type,
        otpCounter: payload,
        showLoader: false
      };
    case REQUEST_OTP_COUNTING_FAILED:
      return {
        ...state,
        showLoader: false,
        otpCounter: payload,
        isError: true,
        type
      };
         // Get existing mobile number
    case GET_EXISTING_MOBILE_REQUEST:
        return { ...state, type, mobileStatus:"", showLoader: true };
  
      case GET_EXISTING_MOBILE_SUCCESS:
        return { ...state, type, mobileStatus: payload, showLoader: false };
  
      case GET_EXISTING_MOBILE_FAILURE:
        return { ...state, type, mobileStatus: payload, showLoader: false };
  
    /********************** User OTP Counter End *************************** */
    default:
      return state;
  }
};
export default scholarshipConclaveReducer;
