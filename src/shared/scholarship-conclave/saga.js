import { takeLatest, call, put } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import axios from "axios";
import {
  REGISTER_USER_REQUESTED,
  REGISTER_USER_SUCCEEDED,
  REGISTER_USER_FAILED,
  POST_QUALIFICATION_REQUESTED,
  POST_QUALIFICATION_SUCCEEDED,
  POST_QUALIFICATION_FAILED,
  POST_STUDY_PLANS_REQUESTED,
  POST_STUDY_PLANS_SUCCEEDED,
  POST_STUDY_PLANS_FAILED,
  POST_BUDGET_PLANS_REQUESTED,
  POST_BUDGET_PLANS_SUCCEEDED,
  POST_BUDGET_PLANS_FAILED,
  POST_COMPETITION_SCORES_REQUESTED,
  POST_COMPETITION_SCORES_SUCCEEDED,
  POST_COMPETITION_SCORES_FAILED,
  POST_VERIFY_OTP_REQUESTED,
  POST_VERIFY_OTP_SUCCEEDED,
  POST_VERIFY_OTP_FAILED,
  REQUEST_OTP_REQUESTED,
  REQUEST_OTP_SUCCEEDED,
  REQUEST_OTP_FAILED,
  REQUEST_OTP_COUNTING_REQUESTED,
  REQUEST_OTP_COUNTING_SUCCEEDED,
  REQUEST_OTP_COUNTING_FAILED
} from "./actions";
import { LOGIN_USER_SUCCEEDED } from "../login/actions";
import fetchClient from "../../api/fetchClient";
import gblFunc from "../../globals/globalFunctions";

/****************************** User Registration *********************** */

const regiterApi = input => {
  return fetchClient.post(apiUrl.registerAdmissionUser, input).then(res => {
    return res.data;
  });
};

function* registerUser({ payload }) {
  try {
    const registerResponse = yield call(regiterApi, payload.data);
    yield put({
      type: REGISTER_USER_SUCCEEDED,
      payload: registerResponse
    });
  } catch (error) {
    yield put({
      type: REGISTER_USER_FAILED,
      payload: {
        errorCode: error.response
          ? error.response.data
            ? error.response.data.errorCode
            : error.response.status
          : "",
        message: error.errorMessage
      }
    });
  }
}

/****************************** User Registration *********************** */

/****************************** User Qualification *********************** */

const submitQualificationApi = input => {
  const userId = gblFunc.getStoreUserDetails()["admissionUserId"];
  return fetchClient
    .post(
      `${apiUrl.qualificationAdmissionUser}/${userId}/current/education`,
      input
    )
    .then(res => {
      return res.data;
    });
};

function* submitQualification({ payload }) {
  try {
    const qualificationResp = yield call(submitQualificationApi, payload.data);
    yield put({
      type: POST_QUALIFICATION_SUCCEEDED,
      payload: qualificationResp
    });
  } catch (error) {
    yield put({ type: POST_QUALIFICATION_FAILED, payload: error.errorMessage });
  }
}
/****************************** User Qualification *********************** */

/****************************** Future Study Plans *********************** */
const submitstudyPlansApi = input => {
  const userId = gblFunc.getStoreUserDetails()["admissionUserId"];
  return fetchClient
    .post(
      `${apiUrl.qualificationAdmissionUser}/${userId}/future/education`,
      input
    )
    .then(res => {
      return res.data;
    });
};

function* submitStudyPlans({ payload }) {
  try {
    const studyPlansResp = yield call(submitstudyPlansApi, payload.data);
    yield put({
      type: POST_STUDY_PLANS_SUCCEEDED,
      payload: studyPlansResp
    });
  } catch (error) {
    yield put({ type: POST_STUDY_PLANS_FAILED, payload: error.errorMessage });
  }
}
/****************************** Future Study Plans *********************** */

/******************************  Study Budget  Plans *********************** */
const submitBudgetPlansApi = input => {
  const userId = gblFunc.getStoreUserDetails()["admissionUserId"];
  return fetchClient
    .post(`${apiUrl.qualificationAdmissionUser}/${userId}/budget`, input)
    .then(res => {
      return res.data;
    });
};

function* submitBudgetPlans({ payload }) {
  try {
    const budgetPlansResp = yield call(submitBudgetPlansApi, payload.data);
    yield put({
      type: POST_BUDGET_PLANS_SUCCEEDED,
      payload: budgetPlansResp
    });
  } catch (error) {
    yield put({ type: POST_BUDGET_PLANS_FAILED, payload: error.errorMessage });
  }
}
/******************************  Study Budget Plans *********************** */

/******************************  Competition Scores *********************** */
const submitCompetitionScoresApi = input => {
  const userId = gblFunc.getStoreUserDetails()["admissionUserId"];
  return fetchClient
    .post(`${apiUrl.qualificationAdmissionUser}/${userId}/exam`, input)
    .then(res => {
      return res.data;
    });
};

function* submitCompetitionScores({ payload }) {
  try {
    const budgetPlansResp = yield call(
      submitCompetitionScoresApi,
      payload.data
    );
    yield put({
      type: POST_COMPETITION_SCORES_SUCCEEDED,
      payload: budgetPlansResp
    });
  } catch (error) {
    yield put({
      type: POST_COMPETITION_SCORES_FAILED,
      payload: error.errorMessage
    });
  }
}
/******************************  Competition Scores  *********************** */

/******************************  OTP Veirification *********************** */
const submitVerifyOTPApi = input =>
  fetchClient.post(apiUrl.verifyApi, input).then(res => {
    console.log(res, 'response api')
    return res.data;
  });

function* submitVerifyOTP({ payload }) {
  try {
    const verifyOTPResp = yield call(submitVerifyOTPApi, payload.data);
    gblFunc.storeRegAuthDetails(verifyOTPResp);
    yield put({
      type: LOGIN_USER_SUCCEEDED,
      payload: verifyOTPResp
    });
  } catch (error) {
    yield put({
      type: POST_VERIFY_OTP_FAILED,
      payload: { errorCode: null, message: error.errorMessage, errors: error }
    });
  }
}

/******************************  OTP Verification *********************** */

/******************************  OTP Request *********************** */
const submitRequestOTPApi = input => {
  if (input.countryCode != "+91") {
    return fetchClient
      .post(`${apiUrl.UpdateMobile}/contact-details`, input)
      .then(res => {
        return res.data;
      });
  }
  return fetchClient.post(apiUrl.sendApi, input).then(res => {
    return res.data;
  });
};

function* submitRequestOTP({ payload }) {
  try {
    const verifyOTPResp = yield call(submitRequestOTPApi, payload.data);
    yield put({
      type: REQUEST_OTP_SUCCEEDED,
      payload: verifyOTPResp
    });
  } catch (error) {
    yield put({
      type: REQUEST_OTP_FAILED,
      payload: { errorCode: error.response.status, message: error.errorMessage, errors: error }
    });
  }
}

/******************************  OTP Request *********************** */
/********************** User OTP Counter *************************** */
const getOTPCounterApi = input => {
  return fetchClient
    .post(`${apiUrl.countingOTP}?retryOTP=true`, input)
    .then(res => {
      return res.data;
    });
};

function* getOTPCounter({ payload }) {
  try {
    const response = yield call(getOTPCounterApi, payload.data);
    yield put({
      type: REQUEST_OTP_COUNTING_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    yield put({
      type: REQUEST_OTP_COUNTING_FAILED,
      payload: error
    });
  }
}
/********************** User OTP Counter End *************************** */

export default function* scholarConclaveSaga() {
  yield takeLatest(REGISTER_USER_REQUESTED, registerUser);
  yield takeLatest(POST_QUALIFICATION_REQUESTED, submitQualification);
  yield takeLatest(POST_STUDY_PLANS_REQUESTED, submitStudyPlans);
  yield takeLatest(POST_BUDGET_PLANS_REQUESTED, submitBudgetPlans);
  yield takeLatest(POST_COMPETITION_SCORES_REQUESTED, submitCompetitionScores);
  yield takeLatest(POST_VERIFY_OTP_REQUESTED, submitVerifyOTP);
  yield takeLatest(REQUEST_OTP_REQUESTED, submitRequestOTP);
  yield takeLatest(REQUEST_OTP_COUNTING_REQUESTED, getOTPCounter);
}
