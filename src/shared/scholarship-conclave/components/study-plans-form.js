import React, { Component } from "react";
import { Formik, Field, Form } from "formik";
import Loader from "../../common/components/loader";
import AlertMessagePopup from "../../common/components/alertMsg";
import { messages } from "../../../constants/constants";
import globalFunc from "../../../globals/globalFunctions";
import {
  POST_STUDY_PLANS_SUCCEEDED,
  POST_STUDY_PLANS_FAILED
} from "../actions";
import * as Yup from "yup";
const studyPlansValidationSchema = Yup.object().shape({
  interestedYear: Yup.string().required("Required."),
  applyCourseTypeId: Yup.string().required("Required."),
  applyEducationLevelId: Yup.string().required("Required."),
  interestedCategoryId: Yup.string().required("Required.")
});
class FutureStudyPlans extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initialValues: {
        applyEducationLevelId: "",
        interestedYear: "",
        applyCourseTypeId: "1",
        interestedCategoryId: ""
      }
    };
    this.closeAlertPopup = this.closeAlertPopup.bind(this);
    this.updateDimensionsScrollForm = this.updateDimensionsScrollForm.bind(
      this
    );
  }
  componentDidMount() {
    this.updateDimensionsScrollForm();
    window.addEventListener(
      "resize",
      this.updateDimensionsScrollForm.bind(this)
    );
  }
  componentWillUnmount() {
    this.updateDimensionsScrollForm();
    window.addEventListener(
      "resize",
      this.updateDimensionsScrollForm.bind(this)
    );
  }
  updateDimensionsScrollForm() {
    if (window && window.screen.width <= 600) {
      this.setState({
        isScrollForm: true
      });
    } else {
      this.setState({
        isScrollForm: false
      });
    }
  }
  componentWillReceiveProps(nextProps, prevState) {
    switch (nextProps.type) {
      case POST_STUDY_PLANS_SUCCEEDED:
        this.setState(
          {
            showAlertPopup: true,
            alertMsg: messages.admission.recordSaved
          },
          () => {
            this.props.getNextForm();
          }
        );
        globalFunc.gaTrack.trackEvent([
          "Future Study Plans",
          "Submit",
          "Succeeded"
        ]);

        break;
      case POST_STUDY_PLANS_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: messages.generic.error
        });
        globalFunc.gaTrack.trackEvent([
          "Future Study Plans",
          "Submit",
          "Failed"
        ]);
        break;
      default:
    }
  }
  closeAlertPopup() {
    this.setState({ showAlertPopup: false, alertMsg: "" });
  }
  render() {
    return (
      <article className="formWrapper">
        <div className="timing">
          You are just <span className="span">3</span> minutes away.
        </div>
        <Loader isLoader={this.props.showLoader} />
        {this.state.showAlertPopup && (
          <AlertMessagePopup
            msg={this.state.alertMsg}
            isShow={this.state.showAlertPopup}
            status={!this.props.isError}
            close={this.closeAlertPopup}
          />
        )}
        <Formik
          initialValues={this.state.initialValues}
          onSubmit={values => {
            this.props.submitStudyPlansData(values);
            globalFunc.gaTrack.trackEvent([
              "Future Study Plans",
              "Submit",
              "Click"
            ]);
          }}
          validationSchema={studyPlansValidationSchema}
        >
          {({ errors, touched, values, setFieldValue }) => {
            return (
              <Form name="studyPlans">
                <article
                  className={`overFlowData ${
                    this.state.isScrollForm ? "scroll" : ""
                  }`}
                >
                  <article className="ctrl-wrapper">
                    <article className="commonFlowData">
                      <article className="form-group">
                        <label>
                          Select level of Studies do you wish to apply for
                        </label>
                        <Field
                          name="applyEducationLevelId"
                          component="select"
                          className="form-control selectCtrl"
                        >
                          <option value="">Select</option>
                          {this.props.dropDownJson.courseLevel.map(course => (
                            <option value={course.id}>{course.title}</option>
                          ))}
                        </Field>
                        {errors.applyEducationLevelId &&
                          touched.applyEducationLevelId && (
                            <span className="error">
                              {errors.applyEducationLevelId}
                            </span>
                          )}
                      </article>
                      <article className="form-group ">
                        <label className="height">
                          Type of Course you are looking for
                        </label>
                        <Field
                          name="applyCourseTypeId"
                          component="select"
                          className="form-control selectCtrl"
                        >
                          <option value="">Select</option>
                          {this.props.dropDownJson.courseType.map(course => (
                            <option value={course.id}>{course.name}</option>
                          ))}
                        </Field>
                        {errors.applyCourseTypeId &&
                          touched.applyCourseTypeId && (
                            <span className="error">
                              {errors.applyCourseTypeId}
                            </span>
                          )}
                      </article>
                    </article>
                  </article>

                  <article className="ctrl-wrapper">
                    <article className="form-group">
                      <label className="height">
                        In which year would you start this course
                      </label>
                      <Field
                        name="interestedYear"
                        component="select"
                        className="form-control selectCtrl"
                      >
                        <option value="">Select</option>
                        {this.props.dropDownJson.admissionYears.map(year => (
                          <option value={year}>{year}</option>
                        ))}
                      </Field>
                      {errors.interestedYear &&
                        touched.interestedYear && (
                          <span className="error">{errors.interestedYear}</span>
                        )}
                    </article>
                  </article>
                  <article className="ctrl-wrapper">
                    <article className="form-group">
                      <label>
                        Enter the field of study that you are intersted in
                      </label>
                      <Field
                        name="interestedCategoryId"
                        component="select"
                        className="form-control selectCtrl"
                      >
                        <option value="">Select</option>
                        {this.props.dropDownJson.courseSubCategory.map(
                          field => (
                            <option value={field.id}>{field.name}</option>
                          )
                        )}
                      </Field>
                      {errors.interestedCategoryId &&
                        touched.interestedCategoryId && (
                          <span className="error">
                            {errors.interestedCategoryId}
                          </span>
                        )}
                    </article>
                  </article>
                </article>
                <article className="ctrl-wrapper width">
                  <article className="form-group">
                    <button type="submit" className="btnuni">
                      Next
                    </button>
                  </article>
                </article>
              </Form>
            );
          }}
        </Formik>
      </article>
    );
  }
}
export default FutureStudyPlans;
