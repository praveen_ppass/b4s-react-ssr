import React, { Component } from "react";
import { ruleRunner } from "../../../validation/ruleRunner";
import { required } from "../../../validation/rules";
import globalFunc from "../../../globals/globalFunctions";
import { POST_QUALIFICATION_SUCCEEDED } from "../actions";
class QualificationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        educationStatus: "true",
        lastEducationCountryId: "102"
      },
      validations: {
        higherEducationName: null,
        highestEducationLevelId: null,
        gradingSystemId: null,
        lastEducationCountryId: null,
        subGradingSystemId: null
      },
      isFormValid: false,
      levelOfEducation: "Select your highest level of education"
    };
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.submitFormData = this.submitFormData.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
  }
  componentDidMount() {
    //console.log(this.props.dropDownJson);
  }
  componentWillReceiveProps(nextProps, prevState) {
    switch (nextProps.type) {
      case POST_QUALIFICATION_SUCCEEDED:
        globalFunc.gaTrack.trackEvent(["Qualification", "Submit", "Succeeded"]);
        this.props.getNextForm();
        break;

      default:
    }
  }
  submitFormData() {
    if (this.checkFormValidations()) {
      globalFunc.gaTrack.trackEvent(["Qualification", "Submit", "Click"]);
      this.props.submitQualificationData(this.state.formData);
    }
  }
  /************ start - validation part - check form submit validation******************* */
  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      if (validations.hasOwnProperty(key)) {
        let { name, validationFunctions } = this.getValidationRulesObject(key);
        let validationResult = ruleRunner(
          this.state.formData[key],
          key,
          name,
          ...validationFunctions
        );
        validations[key] = validationResult[key];
        if (validationResult[key] !== null) {
          isFormValid = false;
        }
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  /************ start - validation part - check form submit validation******************* */
  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldName) {
    let validationObject = {};
    switch (fieldName) {
      case "higherEducationName":
        validationObject.name = "";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "highestEducationLevelId":
        validationObject.name = "";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "gradingSystemId":
        validationObject.name = "";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "subGradingSystemId":
        validationObject.name = "";
        validationObject.validationFunctions = [required];
        return validationObject;
      case "lastEducationCountryId":
        validationObject.name = "";
        validationObject.validationFunctions = [required];
        return validationObject;
      default:
    }
  }
  /************ end - validation part - get message and required validation******************* */
  handleFieldChange(ev) {
    const { name, value } = ev.target;
    if (value == "false" && name == "educationStatus") {
      this.setState({
        levelOfEducation: "Select the level of the ongoing education"
      });
    } else {
      this.setState({
        levelOfEducation: "Select your highest level of education"
      });
    }
    /********************************************** */
    // let validations = { ...this.state.validations };
    // if (validations.hasOwnProperty(id)) {
    //   const { name, validationFunctions } = this.getValidationRulesObject(id);
    //   const validationResult = ruleRunner(
    //     value,
    //     id,
    //     name,
    //     ...validationFunctions
    //   );
    //   validations[id] = validationResult[id];
    // }
    /********************************************** */
    const formData = { ...this.state.formData };
    formData[name] = value;
    this.setState({ formData });
  }

  render() {
    return (
      <article className="formWrapper">
        <div className="timing">
          You are just <span className="span">4</span> minutes away.
        </div>
        <form name="formStepOne">
          <article className="overFlowData scroll">
            <article className="ctrl-wrapper width">
              <article className="form-group radiobtnwidth">
                <label className="align320">Highest Level of Education:</label>

                <article
                  className="radioBoxWrapper"
                  onChange={this.handleFieldChange}
                >
                  <p>
                    <input
                      type="radio"
                      id="completed"
                      name="educationStatus"
                      defaultChecked
                      value="true"
                    />
                    <label htmlFor="completed">Completed</label>
                  </p>
                  <p>
                    <input
                      type="radio"
                      id="ongoing"
                      name="educationStatus"
                      value="false"
                    />
                    <label htmlFor="ongoing">Ongoing</label>
                  </p>
                </article>
              </article>
            </article>
            <article className="ctrl-wrapper">
              <article className="form-group">
                <label className="height">Course Name</label>
                <input
                  type="text"
                  placeholder="Course Name"
                  className="form-control"
                  name="higherEducationName"
                  value={this.state.formData.higherEducationName}
                  onChange={this.handleFieldChange}
                />
                {this.state.validations.higherEducationName ? (
                  <span className="error animated bounce">{"Required"}</span>
                ) : null}
              </article>
            </article>
            <article className="ctrl-wrapper">
              <article className="form-group">
                <label>{this.state.levelOfEducation}</label>
                <select
                  name="highestEducationLevelId"
                  className="form-control selectCtrl"
                  onChange={this.handleFieldChange}
                >
                  <option value="">Select</option>
                  {this.props.dropDownJson.courseLevel.map(course => (
                    <option value={course.id}>{course.title}</option>
                  ))}
                </select>
                {this.state.validations.highestEducationLevelId ? (
                  <span className="error animated bounce bottom90">
                    {"Required"}
                  </span>
                ) : null}
              </article>
            </article>
            <article className="ctrl-wrapper">
              <article className="form-group" onChange={this.handleFieldChange}>
                <label>
                  Your last passing marks, choose your grading system
                </label>
                {this.props.dropDownJson.nestedGradingSystem.map(
                  (grad, index) => (
                    <article className="radioBoxWrapper clearleft">
                      <p>
                        <input
                          type="radio"
                          name="gradingSystemId"
                          value={grad.id}
                          id={`Grade-${index}`}
                        />
                        <label htmlFor={`Grade-${index}`}>{grad.title}</label>
                      </p>
                      {this.state.formData.gradingSystemId == grad.id &&
                        grad.subGrading.map((subGrad, index) => (
                          <article className="radioBoxWrapper inline floatnone">
                            <p>
                              <input
                                type="radio"
                                name="subGradingSystemId"
                                value={subGrad.id}
                                id={`subGrad-${index}`}
                              />
                              <label htmlFor={`subGrad-${index}`}>
                                {subGrad.title}
                              </label>
                            </p>
                          </article>
                        ))}
                    </article>
                  )
                )}
                {this.state.validations.subGradingSystemId ? (
                  <span className="error animated bounce bottom90">
                    {"Required"}
                  </span>
                ) : null}
              </article>
              <article className="form-group">
                <label>Select Country of your last level of education</label>
                <select
                  name="lastEducationCountryId"
                  className="form-control selectCtrl"
                  onChange={this.handleFieldChange}
                  defaultValue="102"
                >
                  <option value="">Select</option>
                  {this.props.dropDownJson.country.map(country => (
                    <option value={country.id}>{country.shortName}</option>
                  ))}
                </select>
                {this.state.validations.lastEducationCountryId ? (
                  <span className="error animated bounce">{"Required"}</span>
                ) : null}
              </article>
            </article>
          </article>

          <article className="ctrl-wrapper width">
            <article className="form-group">
              {/*  <button type="button" className="btn width50">
                Skip
              </button> */}
              <button
                type="button"
                className="btnuni"
                //onClick={this.props.getNextForm}
                onClick={this.submitFormData}
              >
                Next
              </button>
            </article>
          </article>
        </form>
      </article>
    );
  }
}
export default QualificationForm;
