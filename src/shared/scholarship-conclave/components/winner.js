import React, { Component } from "react";
import Slider from "react-slick";
class Winners extends Component {
  render() {
    var settings = {
      dots: true,
      infinite: true,
      autoplay: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 3,
            autoplay: true,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 853,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            dots: true
          }
        },
        {
          breakpoint: 603,
          settings: {
            slidesToShow: 1,
            autoplay: true,
            slidesToScroll: 1,
            dots: true
          }
        }
      ]
    };

    return (
      <section>
        <Slider {...settings}>
          <article className="col-md-12 col-sm-12">
            <div className="topwinner">
              <img src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/images/uni/1.jpg" />
              <p> Tarun Luthra</p>
              <span>Maharaja Agarsain Public School</span>
            </div>
          </article>
          <article className="col-md-12 col-sm-12">
            <div className="topwinner">
              <img src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/images/uni/2.jpg" />
              <p> Bhhavya R Sureka</p>
              <span>Somerville School</span>
            </div>
          </article>
          <article className="col-md-12 col-sm-12">
            <div className="topwinner">
              <img src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/images/uni/6.jpg" />
              <p>Chetna Bhagat</p>
              <span>Modern School, Barakhamba Road</span>
            </div>
          </article>
          <article className="col-md-12 col-sm-12">
            <div className="topwinner">
              <img src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/images/uni/7.jpg" />
              <p> Tripti Jain</p>
              <span>Salwan Public School</span>
            </div>
          </article>
          <article className="col-md-12 col-sm-12">
            <div className="topwinner">
              <img src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/images/uni/3.jpg" />
              <p> Anmol Gupta</p>
              <span>Bal Bharati Public School</span>
            </div>
          </article>
          <article className="col-md-12 col-sm-12">
            <div className="topwinner">
              <img src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/images/uni/4.jpg" />
              <p> Devesh Chainani</p>
              <span>Jayshree Periwal High School</span>
            </div>
          </article>
          <article className="col-md-12 col-sm-12">
            <div className="topwinner">
              <img src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/images/uni/5.jpg" />
              <p>Piyush Mishra</p>
              <span>DAV Public School</span>
            </div>
          </article>
        </Slider>
      </section>
    );
  }
}

export default Winners;
