import React, { Component } from "react";
import { Link } from "react-router-dom";
import Testimonials from "../components/testimonials";
import Winners from "../components/winner";
import { imgBaseUrlDev } from "../../../constants/constants";

class ScholarshipConclave extends Component {
  render() {
    return (
      <section className="uniAgent">
        <section className="container-fluid top">
          <section className="container">
            <section className="row">
              <section className="col-md-6 col-sm-6 col-xs-6">
                <Link to="/">
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/logo-b4s.png"
                    alt="Uni Agent"
                    className="logo img-responsive"
                  />
                </Link>
              </section>

              <section className="col-md-6 col-sm-6 col-xs-6">
                <article className="contactWrapper">
                  <span />
                  <span>
                    <a href="mailto:institute@buddy4study.com">
                      institute@buddy4study.com
                    </a>
                    <a href="tel:+91 8929016460">Ph.+91 8929016460</a>
                  </span>
                </article>
              </section>
            </section>
          </section>
        </section>

        <section className="container-fluid bannerBg">
          <section className="container">
            <section className="row">
              <section className="col-md-12 text-left">
                <section className="row">
                  <section className="col-md-7 msgbox">
                    <h1>4th May, 2019</h1>
                    <h2>
                      <i>India's Largest Study Abroad</i>
                      <i>Scholarship Exam &amp; Fair</i>
                    </h2>
                    <Link
                      className="buttonregister margin0 blink"
                      to="/scholarship-conclave/profile"
                    >
                      Register Now
                    </Link>
                    {/* Sticky Register Button */}
                    <Link
                      className="stickyRegisterBtn"
                      to="/scholarship-conclave/profile"
                    >
                      Register Now{" "}
                      <i class="fa fa-hand-o-left" aria-hidden="true" />
                    </Link>
                    <p>
                      <span>Venue:</span> The Leela Ambience Convention Hotel,
                      New Delhi*
                      <br />
                      <span>Time:</span> 11:00 AM to 7:30 PM
                    </p>
                    <p className="marginElement">
                      <span className="tnc">
                        *Online Exam and Workshops for students who cannot
                        travel to Delhi.
                      </span>
                    </p>
                    <p>
                      <span className="textHighlighter">
                        Who can participate?
                      </span>

                      <ul className="listing">
                        <li>
                          Class 12 appearing/passed students looking for Study
                          Abroad options
                        </li>
                        <li>
                          {" "}
                          Students looking for Post-Graduate level programs
                          abroad
                        </li>
                        <li> Parents of Study Abroad aspirants</li>
                      </ul>
                    </p>
                    {/* <Link
                      className="buttonregister margin0"
                      to="/scholarship-conclave/profile"
                    >
                      Register Now
                    </Link> */}
                    {/* <a href="" className="buttonregister margin0">
                      Register Now
                    </a> */}
                    {/* popup registration */}
                    {/* <article className="formPanel landingpage">
                      <article className="formWrapper">
                        <article className="ctrl-wrapper">
                          <article className="form-group">
                            <h1>sdsdfds</h1>
                            <article className="radioBoxWrapper">
                              <p>
                                <input
                                  type="radio"
                                  id="completed"
                                  name="educationStatus"
                                  value="true"
                                />
                                <label htmlFor="completed">Completed</label>
                              </p>
                              <p>
                                <input
                                  type="radio"
                                  id="ongoing"
                                  name="educationStatus"
                                  value="false"
                                />
                                <label htmlFor="ongoing">Ongoing</label>
                              </p>
                            </article>
                          </article>
                        </article>
                      </article>
                    </article> */}
                  </section>
                  <section className="col-md-5 col-xs-12 msgbox">
                    <article class="rightside">
                      <article className="row speech">
                        <h1>Key Highlights</h1>
                        <article className="key">
                          <ul>
                            <li>300+ foreign universities to choose from</li>
                            <li>
                              600+ experts/counsellors available for one-on-one
                              admission guidance
                            </li>
                            <li>
                              Assured scholarships in the range of INR 30,000 to
                              INR 20,00,000
                            </li>
                          </ul>
                        </article>
                        <h1>Top Winners of 2018</h1>
                        <Winners />
                      </article>
                    </article>
                  </section>
                </section>
              </section>
            </section>
          </section>
        </section>
        <section className="container-fluid whiteBgColor">
          <section className="container">
            <section className="row">
              <section className="col-md-6">
                <h2>Highlights of 2018 Conclave</h2>
                <iframe
                  width="100%"
                  height="315"
                  src="https://www.youtube.com/embed/n2GzNOKsdng"
                  frameborder="0"
                  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen
                />
              </section>
              <section className="col-md-6">
                <h2>Scholarship Winners of 2018</h2>
                <article className="row winner">
                  <section className="speech">
                    <Testimonials />
                  </section>
                </article>
              </section>
            </section>
          </section>
        </section>
        <section className="container-fluid pinkBgColor">
          <section className="container">
            <section className="row">
              <section className="col-md-6">
                <h2>Programme Schedule</h2>
                <span className="time">
                  <i className="fa fa-clock-o" />
                  11:30 am &ndash; 4:00 pm
                </span>
                <p>
                  <b>Aptitude-based Scholarship Exam:</b> Earn scholarships from
                  participating institutions based on your prformance
                </p>
                <span className="time">
                  <i className="fa fa-clock-o" />
                  12:00 pm &ndash; 6:00 pm
                </span>
                <p>
                  <b>India's Largest Student Fair:</b> Meet 300+ delegates from
                  leading international universities &amp; government bodies and
                  get answers to all your study abroad queries on scholarships
                  and admissions
                </p>
                <span className="time">
                  <i className="fa fa-clock-o" />
                  5:30 pm &ndash; 7:30 pm
                </span>
                <p>
                  <b>REAL Virtual Student Fair:</b> Unable to meet Institutions
                  in person? Join the virtual fair and grab your place
                </p>
              </section>
              <section className="col-md-6">
                <p className="textmock">
                  Get Sample Papers and take Mock Tests to improve your chances
                  of getting a good scholarship
                </p>

                <img
                  src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/images/graph.png"
                  className="img-responsive"
                />
                <Link
                  className="buttonregister"
                  to="/scholarship-conclave/profile"
                >
                  Register Now
                </Link>
              </section>
            </section>
          </section>
        </section>

        {/* graph */}
        <section className="container-fluid grayBgColor">
          <section className="container">
            <section className="row">
              <section className="col-md-12">
                <h2>Top Reasons to Register</h2>
                <ul>
                  <li>
                    Attempt India's only Aptitude-based Scholarship Exam for
                    International Scholarships
                  </li>
                  <li>
                    Interact with representatives from leading international
                    universities and government bodies to get clearer insight
                    into the admission process
                  </li>
                  <li>
                    Finance Advisory/Funding Solutions for studying abroad,
                    along with on-the-spot assessment
                  </li>
                </ul>
                <Link
                  className="buttonregister"
                  to="/scholarship-conclave/profile"
                >
                  Register Now
                </Link>
              </section>
            </section>
          </section>
        </section>
        {/* logos */}
        <section className="container-fluid">
          <section className="container">
            <section className="row">
              <section className="col-md-12 text-center">
                <section className="msgImgWrapper">
                  <h4>
                    Largest Congregation of World's Leading Universities &amp;
                    Education Specialists
                  </h4>
                  <p>
                    From world-leading universities to eminent speakers &amp;
                    panelists, the summit will see international education
                    specialists from world over assembled all under one roof
                  </p>
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/participants-Guests-Logo1.jpg"
                    className="img-responsive"
                  />
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/participants-Guests-Logo2.jpg"
                    className="img-responsive"
                  />
                </section>
              </section>
            </section>
          </section>
        </section>
        <section className="container-fluid blueBgColor">
          <section className="container">
            <section className="row">
              <section className="col-md-12 text-center">
                <section className="footerWrapper">
                  <p>
                    Contact Information
                    <span>
                      <a href="mailto:institute@buddy4study.com.com">
                        institute@buddy4study.com
                      </a>
                      <a href="tel:+91 8929016460">Ph.+91 8929016460</a>
                    </span>
                  </p>
                </section>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default ScholarshipConclave;
