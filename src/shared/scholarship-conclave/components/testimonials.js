import React, { Component } from "react";
import Slider from "react-slick";
class Testimonials extends Component {
  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 2,
      autoplay: true,
      slidesToScroll: 2,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 853,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            dots: true
          }
        },
        {
          breakpoint: 603,
          settings: {
            slidesToShow: 1,
            autoplay: true,
            slidesToScroll: 1,
            dots: true
          }
        }
      ]
    };

    return (
      <section>
        <Slider {...settings}>
          <article className="col-md-12 col-sm-12">
            <div className="box">
              <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/NandhiniBharthi.png" />

              <p>
                <i className="fa fa-quote-left icon" aria-hidden="true" />
                &nbsp; I lost my father during my school days. Buddy4Study
                helped me a lot. I pursued Engineering and Masters with
                scholarships. I studied Aerospace engineering from NTU,
                Singapore. Now I got an opportunity to do my research from the
                University of Central Florida, USA.&nbsp;
                <i className="fa fa-quote-right icon" aria-hidden="true" />
              </p>
              <span className="fontsize14">Nandhini Bharthi</span>
            </div>
          </article>
          <article className="col-md-12 col-sm-12">
            <div className="box">
              <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/malvika.jpg" />

              <p>
                <i className="fa fa-quote-left icon" aria-hidden="true" />
                &nbsp;This is to express my gratitude to Buddy4Study for guiding
                me to get scholarships for my program in the UK. I have received
                a scholarship of <i className="fa fa-gbp" aria-hidden="true" />
                17,000. I highly recommend Buddy4Study because they actually
                guide you step by step and help in listing all eligible
                scholarships for you.&nbsp;
                <i className="fa fa-quote-right icon" aria-hidden="true" />
              </p>
              <span className="fontsize14">Malvika Mehta</span>
            </div>
          </article>
          <article className="col-md-12 col-sm-12">
            <div className="box">
              <img src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/naina.jpg" />

              <p>
                <i className="fa fa-quote-left icon" aria-hidden="true" />
                &nbsp;Buddy4Study has helped me tremendously in achieving my
                dream of studying in Ireland. They graciously guided me
                throughout the whole process. Thanks to their constant help and
                support I was able to enroll in one of the top universities in
                the world, University College Cork, with a good
                scholarship.&nbsp;
                <i className="fa fa-quote-right icon" aria-hidden="true" />
              </p>
              <span className="fontsize14">Naina Nair</span>
            </div>
          </article>
        </Slider>
      </section>
    );
  }
}

export default Testimonials;
