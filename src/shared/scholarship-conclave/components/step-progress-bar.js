import React from "react";
import { formsWithProgressBar } from "../../../constants/constants";
const StepsProgressBar = props => {
  const currentFormIndex = formsWithProgressBar.indexOf(props.currentForm);
  return currentFormIndex > -1 ? (
    <ul className="stepNav">
      {formsWithProgressBar.map((item, index) => (
        <li className={index <= currentFormIndex ? "active" : ""} />
      ))}
    </ul>
  ) : (
    ""
  );
};
export default StepsProgressBar;
