import React, { Component } from "react";
import Select from "react-select";
import { Formik, Field, Form } from "formik";
import Loader from "../../common/components/loader";
import AlertMessagePopup from "../../common/components/alertMsg";
import { messages } from "../../../constants/constants";
import globalFunc from "../../../globals/globalFunctions";
import {
  POST_BUDGET_PLANS_SUCCEEDED,
  POST_BUDGET_PLANS_FAILED
} from "../actions";
import * as Yup from "yup";
const budgetPlansValidationSchema = Yup.object().shape({
  interestedCountry: Yup.string().required("Required."),
  budgetId: Yup.string().required("Required.")
});

class StudyBudget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initialValues: {
        interestedCountry: [],
        budgetId: ""
      }
    };
    this.closeAlertPopup = this.closeAlertPopup.bind(this);
  }
  closeAlertPopup() {
    this.setState({ showAlertPopup: false, alertMsg: "" });
  }
  componentWillReceiveProps(nextProps, prevState) {
    switch (nextProps.type) {
      case POST_BUDGET_PLANS_SUCCEEDED:
        this.setState(
          {
            showAlertPopup: true,
            alertMsg: messages.admission.recordSaved
          },
          () => {
            this.props.getNextForm();
          }
        );
        globalFunc.gaTrack.trackEvent(["Study Budget", "Submit", "Succeeded"]);
        break;
      case POST_BUDGET_PLANS_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: messages.generic.error
        });
        globalFunc.gaTrack.trackEvent(["Study Budget", "Submit", "Failed"]);
        break;
      default:
    }
  }
  render() {
    return (
      <article className="formWrapper">
        <div className="timing">
          You are just <span className="span">1</span> minutes away.
        </div>
        <Loader isLoader={this.props.showLoader} />
        {this.state.showAlertPopup && (
          <AlertMessagePopup
            msg={this.state.alertMsg}
            isShow={this.state.showAlertPopup}
            status={!this.props.isError}
            close={this.closeAlertPopup}
          />
        )}
        <Formik
          initialValues={this.state.initialValues}
          onSubmit={values => {
            this.props.submitBudgetPlans(values);
            globalFunc.gaTrack.trackEvent(["Study Budget", "Submit", "Click"]);
          }}
          validationSchema={budgetPlansValidationSchema}
        >
          {({ errors, touched, values, setFieldValue }) => {
            return (
              <Form name="formStepOne">
                <article className="overFlowData scroll">
                  <article className="ctrl-wrapper">
                    <article className="form-group">
                      <label>Select countries of your interest (Max:6)</label>
                      <Select
                        value={values.interestedCountry}
                        valueKey="id"
                        placeholder=""
                        labelKey="shortName"
                        multi={true}
                        name="interestedCountry"
                        options={
                          values.interestedCountry.length > 5
                            ? values.interestedCountry
                            : this.props.dropDownJson.country
                        }
                        className="form-control selectCtrl"
                        onChange={selectedOption =>
                          setFieldValue("interestedCountry", selectedOption)
                        }
                      />

                      {errors.interestedCountry &&
                        touched.interestedCountry && (
                          <span className="error">
                            {errors.interestedCountry}
                          </span>
                        )}
                    </article>
                  </article>
                  <article className="ctrl-wrapper">
                    <article className="form-group">
                      <label>Your total budget preference for education</label>
                      <article className="radioBoxWrapper budget">
                        {this.props.dropDownJson.budget.map((budget, index) => (
                          <article>
                            <p>
                              <Field
                                name="budgetId"
                                type="radio"
                                className="form-control"
                                value={budget.id}
                                id={`budget-${index}`}
                              />

                              <label htmlFor={`budget-${index}`}>
                                {budget.titleBudget}
                              </label>
                              {parseInt(budget.upperAmount) ? (
                                <dd>{`Upto ${budget.budgetInInr} INR`}</dd>
                              ) : (
                                ""
                              )}
                            </p>
                          </article>
                        ))}

                        {errors.budgetId &&
                          touched.budgetId && (
                            <span className="error bottom70">
                              {errors.budgetId}
                            </span>
                          )}
                      </article>
                    </article>
                  </article>
                </article>
                <article className="ctrl-wrapper width">
                  <article className="form-group">
                    <button type="submit" className="btnuni">
                      Finish
                    </button>
                  </article>
                </article>
              </Form>
            );
          }}
        </Formik>
      </article>
    );
  }
}
export default StudyBudget;
