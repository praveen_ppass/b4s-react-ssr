import React from "react";
import { Link } from "react-router-dom";
const thanks = () => {
  return (
    <article className="formWrapper tableWrapper">
      <article className="overFlowData">
        <article className="ctrl-wrapper width">
          <article className="thanksreen">
            <h4>
              <i className="fa fa-check-circle-o" />
              Thank you
            </h4>

            <p>
              Thank you for registration with us, <br />
              Check your email to download your entry ticket.
            </p>
          </article>
        </article>
      </article>
      <article className="ctrl-wrapper width">
        <article className="form-group">
          <a href="https://admission.buddy4study.com" target="_blank">
            <button type="button" className="btnuni">
              Browse courses in international institutions.
            </button>
          </a>
        </article>
      </article>
    </article>
  );
};
export default thanks;
