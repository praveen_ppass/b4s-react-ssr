import React, { Component } from "react";
import { Link } from "react-router-dom";
import Register from "../containers/register-form-container";
import Qualification from "../containers/qualification-form-container";
import StudyPlans from "../containers/study-plans-form-container";
import CompetitionScores from "../containers/competition-score-from-container";
import StudyBudget from "../containers/study-budget-form-container";
import Thanks from "../containers/thanks-container";
import Otp from "../containers/otp-container";
import jsonFile from "../../../constants/globalJSON.json";
import Winners from "../components/winner";

import {
  imgBaseUrlDev,
  scholarshipConclaveConfigservice
} from "../../../constants/constants";
import StepsProgressBar from "./step-progress-bar";
const marginOnForms = ["register", "thanks", "otp", "futureStudyPlans"];
class ScholarshipConclave extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentForm: "register"
    };
    this.getNextForm = this.getNextForm.bind(this);
    this.renderForm = this.renderForm.bind(this);
  }

  getNextForm() {
    const nextForm = scholarshipConclaveConfigservice.getNextForm(
      this.state.currentForm
    );
    this.setState({ currentForm: nextForm });
  }
  renderForm() {
    const {
      courseLevel,
      country,
      courseType,
      courseSubCategory,
      admissionYears,
      nestedGradingSystem,
      budget,
      examScoreTable,
      exam,
      examCategory
    } = jsonFile;
    switch (this.state.currentForm) {
      case "register":
        return (
          <Register
            getNextForm={this.getNextForm}
            location={this.props.location}
          />
        );
      case "otp":
        return (
          <Otp getNextForm={this.getNextForm} location={this.props.location} />
        );

      case "qualification":
        return (
          <Qualification
            getNextForm={this.getNextForm}
            dropDownJson={{
              courseLevel,
              country,
              nestedGradingSystem
            }}
          />
        );
      case "futureStudyPlans":
        return (
          <StudyPlans
            getNextForm={this.getNextForm}
            dropDownJson={{
              courseLevel,
              courseType,
              courseSubCategory,
              admissionYears
            }}
          />
        );
      case "competitionScore":
        return (
          <CompetitionScores
            getNextForm={this.getNextForm}
            dropDownJson={{ examScoreTable, exam, examCategory }}
          />
        );
      case "budget":
        return (
          <StudyBudget
            getNextForm={this.getNextForm}
            dropDownJson={{
              country,
              budget
            }}
          />
        );
      case "thanks":
        return <Thanks />;
      default:
        return <Register />;
    }
  }
  render() {
    return (
      <section className="uniAgent">
        <section className="container-fluid top">
          <section className="container">
            <section className="row">
              <section className="col-md-6 col-sm-6 col-xs-6">
                <Link to="/">
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/logo-b4s.png"
                    alt="Uni Agent"
                    className="logo img-responsive"
                  />
                </Link>
              </section>

              <section className="col-md-6 col-sm-6 col-xs-6">
                <article className="contactWrapper">
                  <span />
                  <span>
                    <a href="mailto:institute@buddy4study.com.com">
                      institute@buddy4study.com
                    </a>
                    <a href="tel:+91 8929016460">Ph.+91 8929016460</a>
                  </span>
                </article>
              </section>
            </section>
          </section>
        </section>

        <section className="container-fluid bannerBg heightBanner">
          <section className="container">
            <section className="row">
              <section className="col-md-12 text-left">
                {/* <section
                  className={
                    marginOnForms.indexOf(this.state.currentForm) > -1
                      ? "mtRegistration row"
                      : "row"
                  }
                > */}
                <section className="mtRegistration">
                  {/* mtRegistration */}
                  <section className="col-md-5 col-xs-12 msgbox">
                    <article class="rightside instraution">
                      <article className="row">
                        {/* <h1 className="h1">
                          REGISTER WITH VERIFIABLE CONTACT DETAILS TO RECEIVE:
                        </h1>
                        <div className="key">
                          <ul>
                            <li>
                              QR CODE to participate in the Fair and take the
                              Exam
                            </li>
                            <li>Scholarship Exam Results</li>
                            <li>Scholarship Disbursement Information</li>
                            <li>Event Related Messages/Alerts</li>
                          </ul>                        
                        </div> */}
                        <section className="msgbox">
                          <h1>4th May, 2019</h1>
                          <h2>
                            <i>India's Largest Study Abroad</i>
                            <i>Scholarship Exam &amp; Fair</i>
                          </h2>
                          {/* Sticky Register Button */}
                          {/* <Link
                            className="stickyRegisterBtn"
                            to="/scholarship-conclave"
                          >
                            Register Now{" "}
                            <i class="fa fa-hand-o-left" aria-hidden="true" />
                          </Link> */}
                          <p>
                            <span>Venue:</span> The Leela Ambience Convention
                            Hotel, New Delhi*
                            <br />
                            <span>Time:</span> 11:00 AM to 7:30 PM
                          </p>
                          <p className="marginElement">
                            <span className="tnc">
                              *Online Exam and Workshops for students who cannot
                              travel to Delhi.
                            </span>
                          </p>
                          <p>
                            <span className="textHighlighter">
                              Who can participate?
                            </span>

                            <ul className="listing">
                              <li>
                                Class 12 appearing/passed students looking for
                                Study Abroad options
                              </li>
                              <li>
                                {" "}
                                Students looking for Post-Graduate level
                                programs abroad
                              </li>
                              <li> Parents of Study Abroad aspirants</li>
                            </ul>
                          </p>
                        </section>
                      </article>
                    </article>
                  </section>
                  <section className="col-md-7 pull-right smallDevice">
                    <article className="formPanel">
                      <StepsProgressBar currentForm={this.state.currentForm} />
                      {this.renderForm()}
                    </article>
                  </section>
                </section>
              </section>
            </section>
          </section>
        </section>
        <section className="container-fluid whiteBgColor">
          <section className="container">
            <section className="row">
              <section className="col-md-6">
                <h2>Highlights of 2018 Conclave</h2>
                <iframe
                  width="100%"
                  height="315"
                  src="https://www.youtube.com/embed/n2GzNOKsdng"
                  frameborder="0"
                  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen
                />
              </section>
              <section className="col-md-6">
                <h2>Key Highlights</h2>
                <article class="rightside">
                  <article className="row speech">
                    <article className="key">
                      <ul>
                        <li>300+ foreign universities to choose from</li>
                        <li>
                          600+ experts/counsellors available for one-on-one
                          admission guidance
                        </li>
                        <li>
                          Assured scholarships in the range of INR 30,000 to INR
                          20,00,000
                        </li>
                      </ul>
                    </article>
                    <h2 className="marginleft20">Top Winners of 2018</h2>
                    <Winners />
                  </article>
                </article>
              </section>
            </section>
          </section>
        </section>
        <section className="container-fluid pinkBgColor">
          <section className="container">
            <section className="row">
              <section className="col-md-6">
                <h2>Programme Schedule</h2>
                <span className="time">
                  <i className="fa fa-clock-o" />
                  11:30 am &ndash; 4:00 pm
                </span>
                <p>
                  <b>Aptitude-based Scholarship Exam:</b> Earn scholarships from
                  participating institutions based on your prformance
                </p>
                <span className="time">
                  <i className="fa fa-clock-o" />
                  12:00 pm &ndash; 6:00 pm
                </span>
                <p>
                  <b>India's Largest Student Fair:</b> Meet 300+ delegates from
                  leading international universities &amp; government bodies and
                  get answers to all your study abroad queries on scholarships
                  and admissions
                </p>
                <span className="time">
                  <i className="fa fa-clock-o" />
                  5:30 pm &ndash; 7:30 pm
                </span>
                <p>
                  <b>REAL Virtual Student Fair:</b> Unable to meet Institutions
                  in person? Join the virtual fair and grab your place
                </p>
              </section>
              <section className="col-md-6">
                <p className="textmock">
                  Get Sample Papers and take Mock Tests to improve your chances
                  of getting a good scholarship
                </p>

                <img
                  src="https://s3-ap-southeast-1.amazonaws.com/cdn.buddy4study.com/static/images/graph.png"
                  className="img-responsive"
                />
                <Link className="buttonregister" to="/scholarship-conclave">
                  Register Now
                </Link>
              </section>
            </section>
          </section>
        </section>

        {/* graph */}
        <section className="container-fluid grayBgColor">
          <section className="container">
            <section className="row">
              <section className="col-md-12">
                <h2>Top Reasons to Register</h2>
                <ul>
                  <li>
                    Attempt India's only Aptitude-based Scholarship Exam for
                    International Scholarships
                  </li>
                  <li>
                    Interact with representatives from leading international
                    universities and government bodies to get clearer insight
                    into the admission process
                  </li>
                  <li>
                    Finance Advisory/Funding Solutions for studying abroad,
                    along with on-the-spot assessment
                  </li>
                </ul>
                <Link className="buttonregister" to="/scholarship-conclave">
                  Register Now
                </Link>
              </section>
            </section>
          </section>
        </section>
        {/* logos */}
        <section className="container-fluid">
          <section className="container">
            <section className="row">
              <section className="col-md-12 text-center">
                <section className="msgImgWrapper">
                  <h4>
                    Largest Congregation of World's Leading Universities &amp;
                    Education Specialists
                  </h4>
                  <p>
                    From world-leading universities to eminent speakers &amp;
                    panelists, the summit will see international education
                    specialists from world over assembled all under one roof
                  </p>
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/participants-Guests-Logo1.jpg"
                    className="img-responsive"
                  />
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/participants-Guests-Logo2.jpg"
                    className="img-responsive"
                  />
                </section>
              </section>
            </section>
          </section>
        </section>
        <section className="container-fluid blueBgColor">
          <section className="container">
            <section className="row">
              <section className="col-md-12 text-center">
                <section className="footerWrapper">
                  <p>
                    Contact Information
                    <span>
                      <a href="mailto:institute@buddy4study.com.com">
                        institute@buddy4study.com
                      </a>
                      <a href="tel:+91 8929016460">Ph.+91 8929016460</a>
                    </span>
                  </p>
                </section>
              </section>
            </section>
          </section>
        </section>
      </section>
    );
  }
}

export default ScholarshipConclave;
