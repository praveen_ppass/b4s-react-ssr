import React, { Component } from "react";
import { Formik, Field, Form } from "formik";
import DatePicker from "react-datepicker";
import moment from "moment";
import {
  SATerrors,
  GMATerrors,
  GREerrors
} from "../../../validation/errorMessages";

import {
  POST_COMPETITION_SCORES_SUCCEEDED,
  POST_COMPETITION_SCORES_FAILED
} from "../actions";
import Loader from "../../common/components/loader";
import AlertMessagePopup from "../../common/components/alertMsg";
import { messages } from "../../../constants/constants";
import globalFunc from "../../../globals/globalFunctions";
import * as Yup from "yup";
import { isError } from "util";
const scoreButtonsValidationSchema = Yup.object().shape({
  interestedYear: Yup.string().required("Required."),
  applyCourseTypeId: Yup.string().required("Required."),
  applyEducationLevelId: Yup.string().required("Required."),
  interestedCategoryId: Yup.string().required("Required.")
});

const satFormValidationSchema = Yup.object().shape({
  satRawScore: Yup.number()
    .required("Required.")
    .moreThan(0, SATerrors.satRawScore)
    .lessThan(10000000, SATerrors.satRawScore),
  satMathScore: Yup.number()
    .required("Required.")
    .moreThan(0, SATerrors.satMathScore)
    .lessThan(10000000, SATerrors.satMathScore),
  satReadingScore: Yup.number()
    .required("Required.")
    .moreThan(0, SATerrors.satReadingScore)
    .lessThan(10000000, SATerrors.satReadingScore),
  satWritingScore: Yup.number()
    .required("Required.")
    .moreThan(0, SATerrors.satWritingScore)
    .lessThan(10000000, SATerrors.satWritingScore),
  satLanguageScore: Yup.number()
    .required("Required.")
    .moreThan(0, SATerrors.satLanguageScore)
    .lessThan(10000000, SATerrors.satLanguageScore),
  satExamDate: Yup.string().required("Required.")
});

const gmatFormValidationSchema = Yup.object().shape({
  gmatVerbalScore: Yup.number()
    .required("Required.")
    .lessThan(171, GMATerrors.gmatVerbalScore)
    .moreThan(129, GMATerrors.gmatVerbalScore),
  gmatVerbal: Yup.number()
    .required("Required.")
    .lessThan(101, GMATerrors.gmatVerbal)
    .moreThan(0, GMATerrors.gmatVerbal),
  gmatQuantitativeScore: Yup.number()
    .required("Required.")
    .lessThan(171, GMATerrors.gmatQuantitativeScore)
    .moreThan(129, GMATerrors.gmatQuantitativeScore),
  gmatQuantitative: Yup.number()
    .required("Required.")
    .lessThan(101, GMATerrors.gmatQuantitative)
    .moreThan(0, GMATerrors.gmatQuantitative),
  gmatAnalyticalWritingScore: Yup.number()
    .required("Required.")
    .lessThan(7, GMATerrors.gmatAnalyticalWritingScore)
    .moreThan(-1, GMATerrors.gmatAnalyticalWritingScore),
  gmatAnalyticalWriting: Yup.number()
    .required("Required.")
    .lessThan(101, GMATerrors.gmatAnalyticalWriting)
    .moreThan(0, GMATerrors.gmatAnalyticalWriting),
  gmatTotalScore: Yup.number()
    .required("Required.")
    .lessThan(1000, GMATerrors.gmatTotalScore)
    .moreThan(0, GMATerrors.gmatTotalScore),
  gmatTotalPercentage: Yup.number()
    .required("Required.")
    .lessThan(101, GMATerrors.gmatTotalPercentage)
    .moreThan(0, GMATerrors.gmatTotalPercentage),
  gmatExamData: Yup.string().required("Required.")
});

const greFormValidationSchema = Yup.object().shape({
  greVerbalScore: Yup.number()
    .required("Required.")
    .lessThan(171, GREerrors.greVerbalScore)
    .moreThan(129, GREerrors.greVerbalScore),
  greVerbal: Yup.number()
    .required("Required.")
    .lessThan(101, GREerrors.greVerbal)
    .moreThan(0, GREerrors.greVerbal),
  greQuantitativeScore: Yup.number()
    .required("Required.")
    .lessThan(171, GREerrors.greQuantitativeScore)
    .moreThan(129, GREerrors.greQuantitativeScore),
  greQuantitative: Yup.number()
    .required("Required.")
    .lessThan(101, GREerrors.greQuantitative)
    .moreThan(0, GREerrors.greQuantitative),
  greAnalyticalWriting: Yup.number()
    .required("Required.")
    .lessThan(7, GREerrors.greAnalyticalWriting)
    .moreThan(-1, GREerrors.greAnalyticalWriting),
  greAnalyticalWritingScore: Yup.number()
    .required("Required.")
    .lessThan(101, GREerrors.greAnalyticalWritingScore)
    .moreThan(0, GREerrors.greAnalyticalWritingScore),
  greExamData: Yup.string().required("Required.")
});
class CompetitionScore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        validScores: "false",
        noExamScore: { englishExamLevelIelts: 1, englishExamLevelToeflIbt: 12 }, //Defualt row selected
        validExamScore: { qualifiedExams: [] }
      }
    };
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.closeAlertPopup = this.closeAlertPopup.bind(this);
    this.submitFormData = this.submitFormData.bind(this);
    this.getChildDataAndFormat = this.getChildDataAndFormat.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    switch (nextProps.type) {
      case POST_COMPETITION_SCORES_SUCCEEDED:
        this.setState(
          {
            showAlertPopup: true,
            alertMsg: messages.admission.recordSaved
          },
          () => {
            this.props.getNextForm();
          }
        );
        globalFunc.gaTrack.trackEvent([
          "Competition Score",
          "Submit",
          "Succeeded"
        ]);
        break;
      case POST_COMPETITION_SCORES_FAILED:
        this.setState({
          showAlertPopup: true,
          alertMsg: messages.generic.error,
          isError: true
        });
        globalFunc.gaTrack.trackEvent([
          "Competition Score",
          "Submit",
          "Failed"
        ]);
        break;
    }
  }
  handleFieldChange(ev) {
    const { value, name } = ev.target;
    let formData = { ...this.state.formData };
    formData[name] = value;
    this.setState({ formData });
  }
  getChildDataAndFormat(formsFlag, data) {
    if (formsFlag == 1) {
      this.setState({
        showAlertPopup: true,
        alertMsg: "Data Saved. Please press Next to complete form.",
        isError: false
      });

      this.setState({
        selectedExam: data.examId,
        formData: {
          ...this.state.formData,
          validExamScore: {
            ...this.state.formData.validExamScore,
            ...data
          }
        }
      });
    } else if (formsFlag == 2) {
      return this.refs.scoreButtonsChild.sendButtonsData();
    }
  }
  submitFormData() {
    let finalData;
    if (this.state.formData.validScores == "true") {
      finalData = {
        ...this.state.formData,
        validExamScore: {
          ...this.state.formData.validExamScore,
          ...this.getChildDataAndFormat(2)
        }
      };
      if (
        !finalData.validExamScore.qualifiedExams.length &&
        !finalData.validExamScore.satExamDate &&
        !finalData.validExamScore.greExamDate &&
        !finalData.validExamScore.gmatExamDate &&
        !this.state.selectedExam
      ) {
        this.setState({
          alertMsg: "Please select one exam and fill it's details",
          showAlertPopup: true,
          isError: true
        });
        return;
      }
    } else {
      finalData = { ...this.state.formData };
    }

    this.props.submitcompetitionScore(finalData);
    globalFunc.gaTrack.trackEvent(["Competition Score", "Submit", "Click"]);
  }
  closeAlertPopup() {
    this.setState({ showAlertPopup: false, alertMsg: "" });
  }
  render() {
    return (
      <article className="formWrapper">
        <div className="timing">
          You are just <span className="span">2</span> minutes away.
        </div>
        <Loader isLoader={this.props.showLoader} />
        {this.state.showAlertPopup && (
          <AlertMessagePopup
            msg={this.state.alertMsg}
            isShow={this.state.showAlertPopup}
            status={!this.state.isError}
            close={this.closeAlertPopup}
          />
        )}
        <form name="formStepThree">
          <article className="overFlowData scroll">
            <article className="ctrl-wrapper width">
              <ExamScoreRadio handleFieldChange={this.handleFieldChange} />
            </article>
            {this.state.formData.validScores == "false" ? (
              <article className="ctrl-wrapper width">
                <article className="form-group contentAlign">
                  <label>
                    How do you rate your english proficiency? Predict your
                    level.
                  </label>
                </article>
                <article className="form-group">
                  <NotValidScoreTable
                    handleFieldChange={this.handleFieldChange}
                    examScoreTable={this.props.dropDownJson.examScoreTable}
                  />
                </article>
              </article>
            ) : (
              <article className="ctrl-wrapper width">
                <article className="form-group">
                  <ScoreButtons
                    dropDownJson={this.props.dropDownJson}
                    ref="scoreButtonsChild"
                    getChildDataAndFormat={this.getChildDataAndFormat}
                  />
                </article>
              </article>
            )}
          </article>
          <article className="ctrl-wrapper width">
            <article className="form-group">
              <button
                type="button"
                className="btnuni"
                onClick={this.submitFormData}
              >
                Next
              </button>
            </article>
          </article>
        </form>
      </article>
    );
  }
}

class NotValidScoreTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTableItemId: 1 //default selection
    };
  }
  render() {
    return (
      <article className="tableList">
        <ul className="titleText">
          <li>
            Level <i>Description</i>
          </li>
          <li>
            IELTS <i>(1.0 - 9.0)</i>
          </li>
          <li>
            TOEFL IBT <i>(0 - 120)</i>
          </li>
        </ul>
        {this.props.examScoreTable.map(tableItem => (
          <ul
            className={
              this.state.activeTableItemId == tableItem.id
                ? "tableRow colorGreen active"
                : "tableRow colorGreen"
            }
            onClick={() =>
              this.props.handleFieldChange(
                {
                  target: {
                    name: "noExamScore",
                    value: {
                      englishExamLevelIelts: tableItem.ieltsId,
                      englishExamLevelToeflIbt: tableItem.tofelId
                    }
                  }
                },
                this.setState({ activeTableItemId: tableItem.id })
              )
            }
          >
            {/* active class*/}
            <li>{tableItem.examTitle}</li>
            <li>{tableItem.ieltsValue}</li>
            <li>{tableItem.tofelValue}</li>
          </ul>
        ))}
      </article>
    );
  }
}

const ExamScoreRadio = props => (
  <article className="form-group">
    <label>
      Do you have IELTS, TOEFL, IBT, CBT, PTE, GRE, SAT or GMAT scores
    </label>
    <article className="radioBoxWrapper inline">
      <p>
        <input
          type="radio"
          id="notValid"
          value="false"
          name="validScores"
          defaultChecked
          onClick={props.handleFieldChange}
        />
        <label htmlFor="notValid">I don't have valid score</label>
      </p>
      <p>
        <input
          type="radio"
          id="valid"
          value="true"
          name="validScores"
          onClick={props.handleFieldChange}
        />
        <label htmlFor="valid">I have valid score</label>
      </p>
    </article>
  </article>
);

class ScoreButtons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCheckBoxes: {},
      scoreData: {},
      satStartDate: "",
      greStartDate: "",
      gmatStartDate: ""
    };
    this.examScoreChange = this.examScoreChange.bind(this);
    this.sendButtonsData = this.sendButtonsData.bind(this);
    this.provideSatFormInitialValues = this.provideSatFormInitialValues.bind(
      this
    );
    this.provideGmatFormInitialValues = this.provideGmatFormInitialValues.bind(
      this
    );
    this.provideGreFormInitialValues = this.provideGreFormInitialValues.bind(
      this
    );
    this.handleChange = this.handleChange.bind(this);
  }
  provideSatFormInitialValues(examId) {
    var initialValues = {
      satRawScore: "",
      satMathScore: "",
      satReadingScore: "",
      satWritingScore: "",
      satLanguageScore: "",
      satExamDate: "",
      examId
    };
    return initialValues;
  }

  provideGmatFormInitialValues(examId) {
    var initialValues = {
      gmatVerbalScore: "",
      gmatVerbal: "",
      gmatQuantitativeScore: "",
      gmatQuantitative: "",
      gmatAnalyticalWritingScore: "",
      gmatAnalyticalWriting: "",
      gmatTotalScore: "",
      gmatTotalPercentage: "",
      gmatExamData: "",
      examId
    };
    return initialValues;
  }

  provideGreFormInitialValues(examId) {
    var initialValues = {
      greVerbalScore: "",
      greVerbal: "",
      greQuantitativeScore: "",
      greQuantitative: "",
      greAnalyticalWriting: "",
      greAnalyticalWritingScore: "",
      examId
    };
    return initialValues;
  }
  handleChange(formName, date) {
    console.log(formName, date);
    if (formName == "gre") {
      this.setState({
        greStartDate: date
      });
    }
    if (formName == "gmat") {
      this.setState({
        gmatStartDate: date
      });
    }
    if (formName == "sat") {
      this.setState({
        satStartDate: date
      });
    }
  }
  sendButtonsData() {
    const qualifiedExams = Object.keys(this.state.scoreData);
    const qualifiedExamScores = Object.values(this.state.scoreData);
    return { qualifiedExams, qualifiedExamScores };
  }
  examScoreChange(ev) {
    const { value, name, checked } = ev.target;
    //console.log("value, name.....", value, name, ev.target.checked);
    if (name == "exam") {
      let checkObj = { ...this.state.selectedCheckBoxes };
      if (checked) {
        checkObj[value] = true;
      } else {
        delete checkObj[value];
        /** Update score data also */
        let scoreDataUpdated = { ...this.state.scoreData };
        delete scoreDataUpdated[value];
        this.setState({ scoreData: scoreDataUpdated });
      }
      /** Update score data also */
      this.setState({ selectedCheckBoxes: checkObj });
    } else if (name == "examCategory") {
      const valuesData = value.split(":");
      let scoreDataUpdated = { ...this.state.scoreData };
      scoreDataUpdated[valuesData[0]] = valuesData[1];
      this.setState({ scoreData: scoreDataUpdated });
    }
  }
  sectionProvider(exam) {
    switch (exam.id) {
      case 5:
        return (
          <GREForm
            getChildDataAndFormat={this.props.getChildDataAndFormat}
            provideGreFormInitialValues={this.provideGreFormInitialValues}
            examId={exam.id}
            handleChange={this.handleChange}
            greStartDate={this.state.greStartDate}
          />
        );

      case 6:
        return (
          <GMATForm
            getChildDataAndFormat={this.props.getChildDataAndFormat}
            examId={exam.id}
            provideGmatFormInitialValues={this.provideGmatFormInitialValues}
            handleChange={this.handleChange}
            gmatStartDate={this.state.gmatStartDate}
          />
        );

      case 7:
        return (
          <SATForm
            getChildDataAndFormat={this.props.getChildDataAndFormat}
            examId={exam.id}
            provideSatFormInitialValues={this.provideSatFormInitialValues}
            handleChange={this.handleChange}
            satStartDate={this.state.satStartDate}
          />
        );

      default:
        return this.props.dropDownJson.examCategory
          .filter(ex => ex.examId == exam.id)
          .map(examCat => (
            <article className="radioBoxWrapper inline pl">
              <p>
                <input
                  type="radio"
                  value={`${exam.id}:${examCat.id}`}
                  id={examCat.id}
                  name="examCategory"
                />
                <label htmlFor={examCat.id}>{`${examCat.title}-${
                  examCat.value
                }`}</label>
              </p>
            </article>
          ));
    }
  }
  render() {
    return (
      <article>
        <form>
          {this.props.dropDownJson.exam.map((exam, index) => (
            <article onChange={this.examScoreChange}>
              <article className="chkBoxWrapper inline mtpl">
                <label htmlFor={`exam-${index}`}>
                  <input
                    type="checkbox"
                    id={`exam-${index}`}
                    name="exam"
                    value={exam.id}
                  />
                  <dd className="chkbox" />
                  {exam.title}
                </label>
              </article>
              {this.state.selectedCheckBoxes[exam.id] &&
                this.sectionProvider(exam)}
            </article>
          ))}
        </form>
      </article>
    );
  }
}
const SATForm = props => (
  <article className="ctrl-wrapper">
    <label className="mt15">Please fill your SAT scores</label>
    <Formik
      initialValues={props.provideSatFormInitialValues(props.examId)}
      onSubmit={values => {
        props.getChildDataAndFormat(1, values);
      }}
      validationSchema={satFormValidationSchema}
    >
      {({ errors, touched, values, setFieldValue }) => {
        //console.log("values....", values);
        return (
          <Form name="SATForm">
            <article className="form-group langDob radiobtnwidth">
              <DatePicker
                selected={props.satStartDate}
                showYearDropdown
                scrollableYearDropdown
                readOnly
                yearDropdownItemNumber={40}
                minDate={moment().subtract(100, "years")}
                maxDate={moment()}
                name="satExamDate"
                placeholderText="Date of Exam"
                className="icon-date form-control"
                onChange={date => {
                  props.handleChange("sat", date);
                  setFieldValue(
                    "satExamDate",
                    moment(date).format("YYYY-MM-DD")
                  );
                }}
                value={values.satExamDate}
                dateFormat="DD-MM-YYYY"
              />
              {errors.satExamDate && (
                <span className="error">{errors.satExamDate}</span>
              )}
            </article>

            <article class="form-group radiobtnwidth">
              <Field
                type="number"
                placeholder="Raw Score"
                class="form-control"
                name="satRawScore"
              />
              {errors.satRawScore &&
                touched.satRawScore && (
                  <span className="error">{errors.satRawScore}</span>
                )}
            </article>
            <article class="form-group radiobtnwidth">
              <Field
                type="number"
                placeholder="Math Score"
                class="form-control"
                name="satMathScore"
              />
              {errors.satMathScore &&
                touched.satMathScore && (
                  <span className="error">{errors.satMathScore}</span>
                )}
            </article>
            <article class="form-group radiobtnwidth">
              <Field
                type="number"
                placeholder="Reading Score"
                class="form-control"
                name="satReadingScore"
              />
              {errors.satReadingScore &&
                touched.satReadingScore && (
                  <span className="error">{errors.satReadingScore}</span>
                )}
            </article>
            <article class="form-group radiobtnwidth">
              <Field
                type="number"
                placeholder="Writing Score"
                class="form-control"
                name="satWritingScore"
              />
              {errors.satWritingScore &&
                touched.satWritingScore && (
                  <span className="error">{errors.satWritingScore}</span>
                )}
            </article>
            <article class="form-group radiobtnwidth">
              <Field
                type="number"
                placeholder="Language Score"
                class="form-control"
                name="satLanguageScore"
              />
              {errors.satLanguageScore &&
                touched.satLanguageScore && (
                  <span className="error">{errors.satLanguageScore}</span>
                )}
            </article>
            <article>
              <button type="submit" className="btnuni savebtn">
                Save
              </button>
            </article>
          </Form>
        );
      }}
    </Formik>
  </article>
);

const GMATForm = props => (
  <article className="ctrl-wrapper">
    <label className="mt15">Please fill your GMAT scores</label>
    <Formik
      initialValues={props.provideGmatFormInitialValues(props.examId)}
      onSubmit={values => {
        props.getChildDataAndFormat(1, values);
      }}
      validationSchema={gmatFormValidationSchema}
    >
      {({ errors, touched, values, setFieldValue }) => {
        //console.log("values....", values);
        return (
          <Form name="GMATForm">
            <article className="form-group langDob radiobtnwidth">
              <DatePicker
                selected={props.gmatStartDate}
                showYearDropdown
                scrollableYearDropdown
                readOnly
                yearDropdownItemNumber={40}
                minDate={moment().subtract(100, "years")}
                maxDate={moment()}
                name="gmatExamData"
                placeholderText="Date of Exam"
                className="icon-date form-control"
                onChange={date => {
                  props.handleChange("gmat", date);
                  setFieldValue(
                    "gmatExamData",
                    moment(date).format("YYYY-MM-DD")
                  );
                }}
                value={values.gmatExamData}
                dateFormat="DD-MM-YYYY"
              />
              {errors.gmatExamData && (
                <span className="error">{errors.gmatExamData}</span>
              )}
            </article>
            <article class="form-group  radiobtnwidth">
              <Field
                type="number"
                placeholder="verbal(130-170)"
                class="form-control"
                name="gmatVerbalScore"
              />
              {errors.gmatVerbalScore &&
                touched.gmatVerbalScore && (
                  <span className="error">{errors.gmatVerbalScore}</span>
                )}
            </article>
            <article class="form-group radiobtnwidth">
              <Field
                type="number"
                placeholder="Verbal%"
                class="form-control"
                name="gmatVerbal"
              />
              {errors.gmatVerbal &&
                touched.gmatVerbal && (
                  <span className="error">{errors.gmatVerbal}</span>
                )}
            </article>
            <article class="form-group radiobtnwidth">
              <Field
                type="number"
                placeholder="Quantitative(130-170)"
                class="form-control"
                name="gmatQuantitativeScore"
              />
              {errors.gmatQuantitativeScore &&
                touched.gmatQuantitativeScore && (
                  <span className="error">{errors.gmatQuantitativeScore}</span>
                )}
            </article>
            <article class="form-group radiobtnwidth">
              <Field
                type="number"
                placeholder="Quantitative%"
                class="form-control"
                name="gmatQuantitative"
              />
              {errors.gmatQuantitative &&
                touched.gmatQuantitative && (
                  <span className="error">{errors.gmatQuantitative}</span>
                )}
            </article>
            <article class="form-group radiobtnwidth">
              <Field
                type="number"
                placeholder="Analytical Writing(0-6)"
                class="form-control"
                name="gmatAnalyticalWritingScore"
              />
              {errors.gmatAnalyticalWritingScore &&
                touched.gmatAnalyticalWritingScore && (
                  <span className="error">
                    {errors.gmatAnalyticalWritingScore}
                  </span>
                )}
            </article>
            <article class="form-group radiobtnwidth">
              <Field
                type="number"
                placeholder="Analytical Writing%"
                class="form-control"
                name="gmatAnalyticalWriting"
              />
              {errors.gmatAnalyticalWriting &&
                touched.gmatAnalyticalWriting && (
                  <span className="error">{errors.gmatAnalyticalWriting}</span>
                )}
            </article>
            <article class="form-group radiobtnwidth">
              <Field
                type="number"
                placeholder="Total Score"
                class="form-control"
                name="gmatTotalScore"
              />
              {errors.gmatTotalScore &&
                touched.gmatTotalScore && (
                  <span className="error">{errors.gmatTotalScore}</span>
                )}
            </article>
            <article class="form-group radiobtnwidth">
              <Field
                type="number"
                placeholder="Total Score%"
                class="form-control"
                name="gmatTotalPercentage"
              />
              {errors.gmatTotalPercentage &&
                touched.gmatTotalPercentage && (
                  <span className="error">{errors.gmatTotalPercentage}</span>
                )}
            </article>
            <article>
              <button type="submit" className="btnuni savebtn">
                Save
              </button>
            </article>
          </Form>
        );
      }}
    </Formik>
  </article>
);

export class GREForm extends Component {
  constructor(props) {
    super(props);
    this.state = { greStartDate: "" };
  }

  render() {
    return (
      <article className="ctrl-wrapper">
        <label className="mt15">Please fill your GRE scores</label>
        <Formik
          initialValues={this.props.provideGreFormInitialValues(
            this.props.examId
          )}
          onSubmit={values => {
            this.props.getChildDataAndFormat(1, values);
          }}
          validationSchema={greFormValidationSchema}
        >
          {({ errors, touched, values, setFieldValue }) => {
            //console.log("values....", values);
            return (
              <Form name="GreForm">
                <article className="form-group langDob radiobtnwidth">
                  <DatePicker
                    selected={this.props.greStartDate}
                    showYearDropdown
                    scrollableYearDropdown
                    readOnly
                    yearDropdownItemNumber={40}
                    minDate={moment().subtract(100, "years")}
                    maxDate={moment()}
                    name="greExamData"
                    placeholderText="Date of Exam"
                    className="icon-date form-control"
                    onChange={date => {
                      this.props.handleChange("gre", date);
                      setFieldValue(
                        "greExamData",
                        moment(date).format("YYYY-MM-DD")
                      );
                    }}
                    value={values.greExamData}
                    dateFormat="DD-MM-YYYY"
                  />
                  {errors.greExamData && (
                    <span className="error">{errors.greExamData}</span>
                  )}
                </article>
                <article class="form-group  radiobtnwidth">
                  <Field
                    type="number"
                    placeholder="verbal(130-170)"
                    class="form-control"
                    name="greVerbalScore"
                  />
                  {errors.greVerbalScore &&
                    touched.greVerbalScore && (
                      <span className="error">{errors.greVerbalScore}</span>
                    )}
                </article>
                <article class="form-group radiobtnwidth">
                  <Field
                    type="number"
                    placeholder="Verbal%"
                    class="form-control"
                    name="greVerbal"
                  />
                  {errors.greVerbal &&
                    touched.greVerbal && (
                      <span className="error">{errors.greVerbal}</span>
                    )}
                </article>
                <article class="form-group radiobtnwidth">
                  <Field
                    type="number"
                    placeholder="Quantitative(130-170)"
                    class="form-control"
                    name="greQuantitativeScore"
                  />
                  {errors.greQuantitativeScore &&
                    touched.greQuantitativeScore && (
                      <span className="error">
                        {errors.greQuantitativeScore}
                      </span>
                    )}
                </article>
                <article class="form-group radiobtnwidth">
                  <Field
                    type="number"
                    placeholder="Quantitative%"
                    class="form-control"
                    name="greQuantitative"
                  />
                  {errors.greQuantitative &&
                    touched.greQuantitative && (
                      <span className="error">{errors.greQuantitative}</span>
                    )}
                </article>
                <article class="form-group radiobtnwidth">
                  <Field
                    type="number"
                    placeholder="Analytical Writing(0-6)"
                    class="form-control"
                    name="greAnalyticalWriting"
                  />
                  {errors.greAnalyticalWriting &&
                    touched.greAnalyticalWriting && (
                      <span className="error">
                        {errors.greAnalyticalWriting}
                      </span>
                    )}
                </article>
                <article class="form-group radiobtnwidth">
                  <Field
                    type="number"
                    placeholder="Analytical Writing%"
                    class="form-control"
                    name="greAnalyticalWritingScore"
                  />
                  {errors.greAnalyticalWritingScore &&
                    touched.greAnalyticalWritingScore && (
                      <span className="error">
                        {errors.greAnalyticalWritingScore}
                      </span>
                    )}
                </article>
                <article>
                  <button type="submit" className="btnuni savebtn">
                    Save
                  </button>
                </article>
              </Form>
            );
          }}
        </Formik>
      </article>
    );
  }
}

export default CompetitionScore;
