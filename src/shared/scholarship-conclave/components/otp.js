import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import OtpInput from "react-otp-input";
import globalFunc from "../../../globals/globalFunctions";
import gblFunc from "../../../globals/globalFunctions";
import { FETCH_USER_RULES_SUCCESS } from "../../../constants/commonActions";
import {
  REQUEST_OTP_SUCCEEDED,
  REQUEST_OTP_FAILED,
  POST_VERIFY_OTP_SUCCEEDED,
  POST_VERIFY_OTP_FAILED,
  REQUEST_OTP_COUNTING_SUCCEEDED,
  REQUEST_OTP_COUNTING_FAILED
} from "../actions";
import { LOGIN_USER_SUCCEEDED } from "../../login/actions";
import { isMobileNumber } from "../../../validation/rules";
import Loader from "../../common/components/loader";
import AlertMessagePopup from "../../common/components/alertMsg";
import { messages } from "../../../constants/constants";
const otpTimeOut = 15000; //ms
var otpTimerId;
class OTPForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCallFire: false,
      showAlertPopup: false,
      isIndiaCode: true,
      isError: true,
      otpEntered: null,
      otpTimedOut: false,
      editInit: false,
      disableOTP: false,
      alertStatus: false,
      isActiveVarifyBtn: false,
      userRulesData: "",
      userInfo: "",
      isRedirectUserInfo: false,
      redirectUrlState:
        this.props.location &&
        this.props.location.state &&
        this.props.location.state.location
          ? this.props.location.state.location
          : "",
      mobileNumber:
        this.props.location &&
        this.props.location.state &&
        this.props.location.state.mobile
          ? this.props.location.state.mobile
          : "",
      updateMobile: false,
      countryCode: "+91"
    };
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.verifyOTP = this.verifyOTP.bind(this);
    this.requestOTP = this.requestOTP.bind(this);
    this.requestOTPCount = this.requestOTPCount.bind(this);
    this.closeAlertPopup = this.closeAlertPopup.bind(this);
    this.stopTimer = this.stopTimer.bind(this);
  }
  setComponentState(obj) {
    this.setState(obj);
  }
  componentDidMount() {
    if (!this.props.location || !this.props.location.state) {
      return this.props.history.push("/");
    }
    if (getNestedObjKey(this.props.history, ["location", "state", "userId"])) {
      this.setState({
        userId:
          getNestedObjKey(this.props.history, ["location", "userId"]) ||
          getNestedObjKey(this.props.history, ["location", "state", "userId"])
      });
    }
    if (
      getNestedObjKey(this.props.history, ["location", "state", "temp_Token"])
    ) {
      this.setState({
        temp_Token: getNestedObjKey(this.props.history, [
          "location",
          "state",
          "temp_Token"
        ])
      });
    }
    globalFunc.gaTrack.trackEvent(["OTP", "Request", "Click"]);
    otpTimerId = setTimeout(() => {
      this.setComponentState({ otpTimedOut: true });
    }, otpTimeOut);

    if (
      !this.props.location.state.mobile ||
      this.props.location.state.mobile == "" ||
      !this.props.location.state.countryCode ||
      this.props.location.state.countryCode == ""
    ) {
      this.setState({ updateMobile: true }, () => {
        clearTimeout(otpTimerId);
        clearTimeout(this.otpTimerCall);
        clearTimeout(this.otpMsgTimerStart);
        clearTimeout(this.otpMsgTimerEnd);
      });
    }
    if (!this.state.updateMobile) {
      this.otpTimerCall = setTimeout(() => {
        this.requestOTPCount();
      }, 60000);
    }
    this.otpMsgTimerStart = setTimeout(() => {
      this.setComponentState({ isCallFire: true });
    }, 50000);
    this.otpMsgTimerEnd = setTimeout(() => {
      this.setComponentState({ isCallFire: false });
    }, 90000);
  }
  componentWillReceiveProps(nextProps) {
    const { redirectUrlState } = this.state;
    switch (nextProps.type) {
      case "GET_EXISTING_MOBILE_SUCCESS":
        this.setState({
          mobileError: false,
          errorMobile: ""
        });
        break;
      case "GET_EXISTING_MOBILE_FAILURE":
        this.setState({
          mobileError: true,
          errorMobile: getNestedObjKey(nextProps, [
            "mobileStatus",
            "data",
            "message"
          ])
        });
        break;
      case FETCH_USER_RULES_SUCCESS:
        const ruleData = getNestedObjKey(nextProps, [
          "commonReducer",
          "userRulesData"
        ]);
        const { userRules, id } = ruleData;
        gblFunc.storeUserDetails(ruleData);
        gblFunc.storeRegAuthDetails(this.props.userLoginData);
        let isUserRuleActive = 0;
        let userInfo = {};
        userInfo["userId"] = id;
        if (userRules && userRules.length) {
          userRules.map(x => {
            if (x.ruleTypeId == 1 && x.ruleId) {
              isUserRuleActive++;
              userInfo["class"] = x.ruleId;
            }
            if (x.ruleTypeId == 5 && x.ruleId) {
              userInfo["gender"] = x.ruleId;
              isUserRuleActive++;
            }
            if (x.ruleTypeId == 21 && x.ruleId) {
              userInfo["state"] = x.ruleId;
              isUserRuleActive++;
            }
            return x;
          });
        } else {
          this.setState({ isRedirectUserInfo: true }, () => {
            localStorage.setItem("isAuth", 0);
            this.props.history.push({
              pathname: "/user-info",
              state: {
                userRulesData: nextProps.userRulesData,
                location: getNestedObjKey(this.props, [
                  "location",
                  "state",
                  "location"
                ])
              }
            });
            return;
          });
        }
        if (isUserRuleActive == 3) {
          localStorage.setItem("isAuth", 1);
          localStorage.setItem("userId", id);
          if (nextProps.userRulesData && nextProps.userRulesData.vleUser) {
            this.setState({ userRulesData: nextProps.userRulesData });
            if (
              nextProps.userRulesData.cscId &&
              nextProps.userRulesData.cscId != null
            ) {
              return this.props.history.push("/subscribers");
            }
            return this.props.history.push("/vle/add-student");
          } else if (localStorage.getItem("currentPath")) {
            return this.props.history.push(localStorage.getItem("currentPath"));
          } else {
            return this.props.history.push(redirectUrlState);
          }
        } else {
          this.setState({ isRedirectUserInfo: true, userInfo: userInfo });
          localStorage.setItem("isAuth", 0);
        }
        break;
      case REQUEST_OTP_SUCCEEDED:
        if (this.state.countryCode != "+91") {
          this.setState({
            showAlertPopup: true,
            updateMobile: false,
            alertMsg:
              "Your Mobile Number has been Updated. Please Login again to continue",
            redirectingToLogin: true,
            alertStatus: true
          });
        } else if (this.state.countryCode == "+91" && this.state.isIndiaCode) {
          this.setState(
            {
              showAlertPopup: true,
              alertMsg: messages.otp.success,
              alertStatus: true,
              updateMobile: false,
              isIndiaCode: false
            },
            () => {
              let timeOut1 = false;
              otpTimerId = setTimeout(() => {
                timeOut1 = true;
              }, otpTimeOut);
              if (timeOut1) {
                this.setState({ otpTimedOut: true });
              }
            }
          );
        }
        globalFunc.gaTrack.trackEvent(["OTP", "Request", "Succeeded"]);
        break;

      case REQUEST_OTP_FAILED:
        let errCode = getNestedObjKey(nextProps.errorMessage, [
          "errors",
          "response",
          "data",
          "errorCode"
        ]);
        if (errCode == 1108 && this.state.isError) {
          this.setState({
            showAlertPopup: true,
            isError: false,
            isAlertShow: true,
            alertStatus: false,
            updateMobile: true,
            alertMsg:
              "Your mobile number is already registered with us. If you remember your registered email id, please use login to sign in. In case you have forgotten your password, please use 'forgot password' to retrieve your account details."
          });
        } else if (this.state.isError) {
          this.setState({
            showAlertPopup: true,
            updateMobile: true,
            isError: false,
            alertMsg: messages.generic.error,
            alertStatus: false
          });
        }
        globalFunc.gaTrack.trackEvent(["OTP", "Request", "Failed"]);
        break;
      case LOGIN_USER_SUCCEEDED:
        globalFunc.gaTrack.trackEvent(["OTP", "Verification", "Succeeded"]);
        gblFunc.storeAuthToken(nextProps.userLoginData.access_token);
        this.props.fetchUserRules({
          userid: nextProps.userLoginData.userId
        });
        // ...................................................................
        // if (
        //   window &&
        //   (redirectUrlState.includes("https://") ||
        //     redirectUrlState.includes("http://"))
        // ) {
        //   window.location.assign(redirectUrlState);
        // }
        // this.props.history.push(redirectUrlState);
        break;

      case POST_VERIFY_OTP_FAILED:
        let errorCode = getNestedObjKey(nextProps.errorMessage, [
          "errors",
          "response",
          "data",
          "errorCode"
        ]);
        if (errorCode == 701) {
          this.setState({
            alertStatus: false,
            showAlertPopup: true,
            alertMsg: "Invalid OTP"
          });
        } else {
          this.setState({
            alertStatus: false,
            showAlertPopup: true,
            alertMsg:
              nextProps.errorMessage.errorCode == 404
                ? messages.otp.verifyError
                : messages.generic.error
          });
        }
        globalFunc.gaTrack.trackEvent(["OTP", "Verification", "Failed"]);
        break;
      case REQUEST_OTP_COUNTING_SUCCEEDED:
        this.setState(
          {
            otpCounter: nextProps.otpCounter && nextProps.otpCounter.retryCount
          },
          () => {
            clearTimeout(otpTimerId);
            clearTimeout(this.otpTimerCall);
            clearTimeout(this.otpMsgTimerStart);
            clearTimeout(this.otpMsgTimerEnd);
          }
        );
        break;
      case REQUEST_OTP_COUNTING_FAILED:
        const errorKey = getNestedObjKey(nextProps.otpCounter, [
          "response",
          "data",
          "errorCode"
        ]);
        clearTimeout(otpTimerId);
        clearTimeout(this.otpTimerCall);
        clearTimeout(this.otpMsgTimerStart);
        clearTimeout(this.otpMsgTimerEnd);
        if (errorKey == 1105) {
          this.setState({
            alertStatus: false,
            showAlertPopup: true,
            redirectingToLogin: true,
            alertMsg: "Maximum attempt reached"
          });
        } else {
          this.setState({
            alertStatus: false,
            showAlertPopup: true,
            alertMsg: "Server error"
          });
          break;
        }
    }
  }
  componentWillUnmount() {
    clearTimeout(otpTimerId);
    clearTimeout(this.otpTimerCall);
    clearTimeout(this.otpMsgTimerStart);
    clearTimeout(this.otpMsgTimerEnd);
  }
  handleFieldChange(otp) {
    if (otp.length == 6) {
      this.setState({ otpEntered: otp, isActiveVarifyBtn: true });
    } else {
      this.setState({ otpEntered: otp, isActiveVarifyBtn: false });
    }
  }
  verifyOTP() {
    let mobile = "";
    if (this.state.mobileNumber) {
      mobile = this.state.mobileNumber;
    } else if (
      this.props.location &&
      this.props.location.state &&
      this.props.location.state.mobile
    ) {
      mobile = this.props.location.state.mobile;
    }
    globalFunc.gaTrack.trackEvent(["OTP", "Verification", "Click"]);
    this.props.verifyOTP({
      mobile: mobile,
      otp: this.state.otpEntered,
      userId:
        this.state.userId ||
        getNestedObjKey(this.props.history, ["location", "state", "userId"]) ||
        this.props.registerResponse.id
    });
  }
  requestOTPCount() {
    if (this.state.mobileNumber) {
      clearTimeout(this.otpTimerCall);
      globalFunc.gaTrack.trackEvent([
        "Change Mobile Number",
        "New OTP Request",
        "Click"
      ]);
      this.props.fetchOTPCounting({
        mobile: this.state.mobileNumber,
        userId:
          this.state.userId ||
          getNestedObjKey(this.props.history, [
            "location",
            "state",
            "userId"
          ]) ||
          this.props.registerResponse.id
      });
      if (this.state.otpCounter > 2) {
        this.setComponentState({ disableOTP: true });
        return;
      }
    }
  }
  requestOTP() {
    this.setComponentState({ isError: true });
    globalFunc.gaTrack.trackEvent(["OTP", "Resend Request", "Click"]);
    if (this.state.mobileNumber) {
      this.props.requestOTP({
        mobile: this.state.mobileNumber,
        userId:
          this.state.userId ||
          getNestedObjKey(this.props.history, [
            "location",
            "state",
            "userId"
          ]) ||
          getNestedObjKey(this.props, ["registerResponse", "id"]),
        countryCode: this.state.countryCode,
        token:
          this.state.temp_Token ||
          getNestedObjKey(this.props, ["location", "state", "token"])
      });
    } else {
      this.props.requestOTP({
        mobile: this.props.history.location.state.mobile,
        userId:
          this.state.userId ||
          getNestedObjKey(this.props.history, ["location", "state", "userId"]),
        countryCode: this.props.history.location.countryCode,
        token: this.state.temp_Token
      });
    }
  }

  stopTimer() {
    this.setState({ updateMobile: true, isIndiaCode: true });
    clearTimeout(this.otpTimerCall);
  }
  clearTimeout() {
    this.setState({ clearTime: false });
  }
  closeAlertPopup() {
    if (this.state.redirectingToLogin) {
      this.setState(
        {
          showAlertPopup: false,
          alertMsg: "",
          alertStatus: false,
          redirectingToLogin: false
        },
        () => {
          if (this.state.redirectUrlState)
            this.props.history.push({ pathname: this.state.redirectUrlState });
          else this.props.history.push("/");
        }
      );
    }
    this.setState({ showAlertPopup: false, alertMsg: "", alertStatus: false });
  }
  render() {
    const { disableOTP, isRedirectUserInfo, userInfo, isCallFire } = this.state;
    if (typeof window !== "undefined") {
      history.pushState(null, null, location.href);
      window.onpopstate = function(event) {
        history.go(1);
      };
      history.pushState(null, null, location.href);
      window.onpopstate = function(event) {
        history.go(1);
      };
    }
    if (isRedirectUserInfo) {
      return (
        <Redirect
          to={{
            pathname: "/user-info",
            state: {
              userInfo: userInfo,
              userRulesData: this.state.userRulesData,
              location: getNestedObjKey(this.props, [
                "location",
                "state",
                "location"
              ])
            }
          }}
        />
      );
    }
    if (!this.state.updateMobile && this.state.countryCode == "+91") {
    }
    return (
      <article className="formWrapper tableWrapper">
        <Loader isLoader={this.props.showLoader} />
        {this.state.showAlertPopup && (
          <AlertMessagePopup
            msg={this.state.alertMsg}
            isShow={this.state.showAlertPopup}
            status={this.state.alertStatus}
            close={this.closeAlertPopup}
          />
        )}

        <section className="container-fluid equial-padding otpbg">
          <section className="container">
            <section className="row">
              <article className="otpWrapper otpmobile">
                {this.state.updateMobile ? (
                  <article
                    style={{
                      textAlign: "center",
                      display: "flex",
                      justifyContent: "center",
                      flexWrap: "wrap",
                      width: "100%"
                    }}
                  >
                    <h1>Please Update Your Mobile Number</h1>
                    <br />
                    <br />
                    <span>*Please Enter 10 Digits of your Mobile Number.</span>
                  </article>
                ) : (
                  <h1>Thanks for Registration</h1>
                )}

                {!this.state.updateMobile && (
                  <article className="formWrapper">
                    <p>Enter the OTP sent to your mobile number.</p>
                    <p>
                      {this.state.mobileNumber
                        ? this.state.mobileNumber
                        : this.props.history.location &&
                          this.props.history.location.state &&
                          this.props.history.location.state.mobile
                        ? this.props.history.location.state.mobile
                        : ""}
                    </p>
                    <button
                      className="resendbut linkButton "
                      style={{ color: "blue", textDecorationLine: "underline" }}
                      onClick={this.stopTimer}
                    >
                      Change Your Mobile Number
                    </button>
                    <p style={{ marginBottom: "-10px" }}>
                      {" "}
                      Enter the correct OTP
                    </p>
                    <article className="cellWrapper">
                      <article className="width100">
                        <article className="form-group registerOtp otp">
                          <OtpInput
                            onChange={otp => this.handleFieldChange(otp)}
                            numInputs={6}
                            separator={<span>-</span>}
                          />
                        </article>
                      </article>
                    </article>
                    {!disableOTP && !isCallFire ? (
                      <React.Fragment>
                        {this.state.otpTimedOut && (
                          <button
                            className="resendbut linkButton "
                            style={{
                              color: "blue",
                              textDecorationLine: "underline"
                            }}
                            onClick={() => {
                              this.requestOTPCount();
                            }}
                          >
                            Request OTP on call
                          </button>
                        )}
                      </React.Fragment>
                    ) : (
                      <React.Fragment>
                        {isCallFire ? (
                          <p>We are trying to call you with OTP</p>
                        ) : (
                          <p>
                            Maximum attempts reached. Please try again after
                            some time.
                          </p>
                        )}
                      </React.Fragment>
                    )}
                    <button
                      type="button"
                      className={`btn gradientBtn ${
                        this.state.isActiveVarifyBtn
                          ? "act-pointer"
                          : "dact-pointer"
                      }`}
                      style={{ marginTop: "10px" }}
                      onClick={this.verifyOTP}
                      disabled={!this.state.isActiveVarifyBtn}
                    >
                      Verify OTP
                    </button>
                  </article>
                )}
                {this.state.updateMobile && (
                  <article className="otpnumber">
                    {this.state.showAlertPopup && (
                      <AlertMessagePopup
                        msg={this.state.alertMsg}
                        isShow={this.state.showAlertPopup}
                        status={this.state.alertStatus}
                        close={this.closeAlertPopup}
                      />
                    )}
                    <select
                      className="form-control"
                      value={
                        this.props.history.location &&
                        this.props.history.location.countryCode
                          ? this.props.history.location.countryCode
                          : this.state.countryCode
                      }
                      onChange={e =>
                        this.setState({ countryCode: e.target.value })
                      }
                    >
                      <option value="+91">India(+91)</option>
                      <option value="+977">Nepal(+977)</option>
                      <option value="+95">Myanmar(+95)</option>
                    </select>

                    <article className="gpnum">
                      <input
                        placeholder="Enter Your Mobile Number"
                        type="text"
                        value={this.state.mobileNumber}
                        maxLength="10"
                        onChange={e => {
                          if (!e.target.value.match(/^(0|[6789][0-9]*)$/)) {
                            this.setState({
                              mobileError: true,
                              errorMobile:
                                "Should be Numeric or Start with 9,8,7,6 *.",
                              mobileNumber: e.target.value
                            });
                          } else if (10 > e.target.value.length) {
                            this.setState({
                              mobileError: true,
                              errorMobile:
                                "Mobile number should be 10 digit only*.",
                              mobileNumber: e.target.value
                            });
                          } else {
                            const mobileNo = e.target.value;
                            this.setState(
                              {
                                mobileError: false,
                                errorMobile: "",
                                mobileNumber: e.target.value
                              },
                              () => {
                                this.props.existingMobileStatus({
                                  mobile: mobileNo,
                                  userId: getNestedObjKey(this.props, [
                                    "userLoginData",
                                    "data",
                                    "data",
                                    "userId"
                                  ])
                                });
                              }
                            );
                          }
                        }}
                      />
                      {this.state.mobileError ? (
                        <span className="error animated bounce">
                          {this.state.errorMobile}
                        </span>
                      ) : (
                        ""
                      )}

                      <button
                        className="btn"
                        onClick={() => {
                          let { mobileError } = this.state;
                          if (mobileError) {
                            return false;
                          } else {
                            this.requestOTP();
                          }
                        }}
                      >
                        Submit
                      </button>
                    </article>
                  </article>
                )}
              </article>
            </section>
          </section>
        </section>
      </article>
    );
  }
}
export default OTPForm;
