import React, { Component } from "react";
import { Formik, Field, Form } from "formik";
import { Link } from "react-router-dom";
import DatePicker from "react-datepicker";
import moment from "moment";
import * as Yup from "yup";
import { REGISTER_USER_SUCCEEDED, REGISTER_USER_FAILED } from "../actions";
import {
  FETCH_USER_RULES_SUCCESS,
  FETCH_USER_RULES_REQUEST
} from "../../../constants/commonActions";
import Loader from "../../common/components/loader";
import AlertMessagePopup from "../../common/components/alertMsg";
import { messages } from "../../../constants/constants";
import globalFunc from "../../../globals/globalFunctions";
if (typeof window !== "undefined") {
  require("react-datepicker/dist/react-datepicker.css");
}
const registerValidationSchema = Yup.object().shape({
  name: Yup.string().required("Required."),
  gender: Yup.string().required("Required."),
  email: Yup.string()
    .required("Required.")
    .email("Invalid Email."),
  dob: Yup.string().required("Required."),
  city: Yup.string().required("Required."),
  mobile: Yup.string()
    .required("Required.")
    .matches(/^(0|[6789][0-9]*)$/, "Phone number is not valid.")
});
class FormRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newUser: true,
      isError: false,
      isScrollForm: false,
      startDate: ""
    };
    this.closeAlertPopup = this.closeAlertPopup.bind(this);
    this.provideInitialValues = this.provideInitialValues.bind(this);
    this.updateDimensionsScrollForm = this.updateDimensionsScrollForm.bind(
      this
    );
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.updateDimensionsScrollForm();
    window.addEventListener(
      "resize",
      this.updateDimensionsScrollForm.bind(this)
    );
  }
  componentWillUnmount() {
    this.updateDimensionsScrollForm();
    window.addEventListener(
      "resize",
      this.updateDimensionsScrollForm.bind(this)
    );
  }

  closeAlertPopup() {
    this.setState({ showAlertPopup: false, alertMsg: "" });
  }
  provideInitialValues() {
    var initialValues = {
      name: "",
      email: "",
      mobile: "",
      city: "",
      dob: "",
      gender: ""
    };
    if (globalFunc.isUserAuthenticated()) {
      const { firstName, mobile, email } = globalFunc.getStoreUserDetails();
      initialValues.name = firstName;
      initialValues.email = email == "undefined" ? "" : email;
      initialValues.mobile = mobile;
    }
    return initialValues;
  }
  componentDidCatch(err, info) {
    this.setState({ isError: true });
  }
  componentWillReceiveProps(nextProps, prevState) {
    // switch (nextProps.commonType) {
    //   case FETCH_USER_RULES_SUCCESS:
    //     globalFunc.storeUserDetails(nextProps.userRulesData);
    //     this.setState(
    //       {
    //         showAlertPopup: true,
    //         alertMsg: messages.admission.registerSuccess,
    //         newUser: nextProps.registerResponse.newUser
    //       },
    //       () => {
    //         this.props.getNextForm();
    //       }
    //     );
    //     return;
    //   case FETCH_USER_RULES_REQUEST:
    //     return;
    //   default:
    // }

    switch (nextProps.type) {
      case REGISTER_USER_SUCCEEDED:
        globalFunc.gaTrack.trackEvent([
          "Registration",
          "Book a seat",
          "Succeeded"
        ]);
        localStorage.setItem(
          "admissionUserId",
          nextProps.registerResponse.userId
        );
        this.setState(
          {
            showAlertPopup: true,
            alertMsg: messages.admission.registerSuccess,
            newUser: nextProps.registerResponse.newUser
          },
          () => {
            this.props.getNextForm();
          }
        );

        //  localStorage.clear();
        // globalFunc.storeAuthDetails(nextProps.registerResponse);
        //this.props.getUserData({ userid: nextProps.registerResponse.userId });

        // if (!globalFunc.isUserAuthenticated()) {
        //   if (nextProps.registerResponse.newUser) {
        //     globalFunc.storeAuthDetails(nextProps.registerResponse);
        //     this.setState(
        //       {
        //         showAlertPopup: true,
        //         alertMsg: messages.admission.registerSuccess,
        //         newUser: nextProps.registerResponse.newUser
        //       },
        //       () => {
        //         this.props.getNextForm();
        //       }
        //     );
        //   }
        // } else {
        //   localStorage.clear();
        //   globalFunc.storeAuthDetails(nextProps.registerResponse);
        //   this.setState({ newUser: nextProps.registerResponse.newUser });
        //   this.props.getNextForm();
        // }
        break;
      case REGISTER_USER_FAILED:
        globalFunc.gaTrack.trackEvent([
          "Registration",
          "Book a seat",
          "Failed"
        ]);
        if (nextProps.errorMessage.errorCode == 403) {
          //localStorage.clear();
          this.setState({ newUser: false });
        } else if (nextProps.errorMessage.errorCode == 702) {
          this.setState({
            showAlertPopup: true,
            alertMsg: messages.otp.alreadyVerifiedMobile
          });
        } else if (nextProps.errorMessage.errorCode == 701) {
          this.setState({ newUser: false });
        } else {
          this.setState({
            showAlertPopup: true,
            alertMsg: messages.generic.error
          });
        }
        break;
      default:
    }
  }
  updateDimensionsScrollForm() {
    if (window && window.screen.width <= 600) {
      this.setState({
        isScrollForm: true
      });
    } else {
      this.setState({
        isScrollForm: false
      });
    }
  }
  handleChange(date) {
    this.setState({
      startDate: date
    });
  }

  render() {
    return (
      <article className="formWrapper">
        <Loader isLoader={this.props.showLoader} />
        {this.state.showAlertPopup && (
          <AlertMessagePopup
            msg={this.state.alertMsg}
            isShow={this.state.showAlertPopup}
            status={!this.props.isError}
            close={this.closeAlertPopup}
          />
        )}
        <h5>Register Here</h5>
        <Formik
          initialValues={this.provideInitialValues()}
          onSubmit={values => {
            const data = {
              ...values,
              portalId: 1,
              userAddress: { type: 1, city: values.city }
            };
            data.firstName = data.name;
            data.gender = parseInt(data.gender, 10);
            delete data.city;
            delete data.name;
            this.props.registerUser(data);
            globalFunc.gaTrack.trackEvent(["Registration", "Book a seat"]);
            // this.props.getNextForm();
          }}
          validationSchema={registerValidationSchema}
        >
          {({ errors, touched, values, setFieldValue }) => {
            return (
              <Form name="register">
                <article
                  className={`overFlowData ${
                    this.state.isScrollForm ? "scroll" : ""
                  }`}
                >
                  <article className="ctrl-wrapper">
                    <article className="form-group">
                      <label>Full name</label>
                      <Field
                        placeholder="Enter Your Full Name"
                        className="form-control"
                        name="name"
                      />
                      {errors.name && touched.name ? (
                        <span className="error">{errors.name}</span>
                      ) : null}
                    </article>
                  </article>
                  <article className="ctrl-wrapper">
                    <article className="form-group">
                      <label>Email</label>
                      <Field
                        type="email"
                        placeholder="Enter Your Email Address"
                        className="form-control"
                        name="email"
                        disabled={
                          globalFunc.getStoreUserDetails()["email"] &&
                          globalFunc.isUserAuthenticated() &&
                          globalFunc.getStoreUserDetails()["email"] !==
                            "undefined"
                        }
                      />
                      {errors.email &&
                        touched.email && (
                          <span className="error">{errors.email}</span>
                        )}
                    </article>
                  </article>
                  <article className="ctrl-wrapper">
                    <article className="form-group">
                      <label>Mobile Number</label>
                      <Field
                        type="mobile"
                        placeholder="Enter Your Mobile Number"
                        maxlength="10"
                        minlength="10"
                        className="form-control"
                        name="mobile"
                      />
                      {errors.mobile &&
                        touched.mobile && (
                          <span className="error">{errors.mobile}</span>
                        )}
                    </article>
                  </article>
                  <article className="ctrl-wrapper">
                    <article className="form-group">
                      <label>City</label>
                      <Field
                        placeholder="Enter Your City"
                        className="form-control"
                        name="city"
                      />
                      {errors.city &&
                        touched.city && (
                          <span className="error">{errors.city}</span>
                        )}
                    </article>
                  </article>
                  <article className="ctrl-wrapper">
                    <article className="form-group ">
                      <label>Gender</label>
                      <Field
                        name="gender"
                        component="select"
                        className="form-control"
                      >
                        <option value="">-Select Gender-</option>
                        <option value={110}>Female</option>
                        <option value={111}>Male</option>
                      </Field>
                      {errors.gender &&
                        touched.gender && (
                          <span className="error">{errors.gender}</span>
                        )}
                    </article>
                  </article>

                  <article className="ctrl-wrapper">
                    <article className="form-group dob">
                      <label>Date Of Birth</label>
                      <DatePicker
                        selected={this.state.startDate}
                        showYearDropdown
                        scrollableYearDropdown
                        readOnly
                        yearDropdownItemNumber={40}
                        minDate={moment().subtract(100, "years")}
                        maxDate={moment().subtract(10, "years")}
                        name="dob"
                        value={values.dob}
                        placeholderText="DOB"
                        className="icon-date"
                        onChange={date => {
                          this.handleChange(date);
                          setFieldValue(
                            "dob",
                            moment(date).format("YYYY-MM-DD")
                          );
                        }}
                        dateFormat="YYYY-MM-DD"
                      />
                      {errors.dob &&
                        touched.dob && (
                          <span className="error">{errors.dob}</span>
                        )}
                    </article>

                    {/* <article
                    className="form-group"
                    onChange={this.handleFieldChange}
                  >
                    <label>Grade System</label>

                    <article className="radioBoxWrapper clearleft">
                      <article className="radioBoxWrapper inline floatnone">
                        <p>
                          <input
                            type="radio"
                            name="subGradingSystemId"
                            value=""
                            id="male"
                          />
                          <label htmlFor="male">dsfdfds</label>
                        </p>
                      </article>

                      <article className="radioBoxWrapper inline floatnone">
                        <p>
                          <input
                            type="radio"
                            name="subGradingSystemId"
                            value=""
                            id="male"
                          />
                          <label htmlFor="male">dsfdfds</label>
                        </p>
                      </article>

                      <span className="error animated bounce bottom90">
                        kjklj
                      </span>
                    </article>
                  </article> */}
                  </article>
                </article>
                <article className="ctrl-wrapper width">
                  <article className="form-group">
                    <button type="submit" class="btnuni">
                      Confirm Your Seat
                    </button>
                  </article>
                </article>
              </Form>
            );
          }}
        </Formik>
        {!this.state.newUser ||
          (this.state.isError && (
            <article>
              {" "}
              <span className="errortext">
                You are already registered with us. Please login to
                continue:&nbsp;
                <Link
                  to={{
                    pathname: "/user/login",
                    state: {
                      isRedirecting: true,
                      openLoginPopup: true,
                      from: this.props.location
                    }
                  }}
                >
                  <span>Login here</span>
                </Link>
              </span>
            </article>
          ))}
      </article>
    );
  }
}
export default FormRegister;
