export const REGISTER_USER_REQUESTED = "REGISTER_USER_REQUESTED";
export const REGISTER_USER_SUCCEEDED = "REGISTER_USER_SUCCEEDED";
export const REGISTER_USER_FAILED = "REGISTER_USER_FAILED";
export const POST_QUALIFICATION_REQUESTED = "POST_QUALIFICATION_REQUESTED";
export const POST_QUALIFICATION_SUCCEEDED = "POST_QUALIFICATION_SUCCEEDED";
export const POST_QUALIFICATION_FAILED = "POST_QUALIFICATION_FAILED";
export const POST_STUDY_PLANS_REQUESTED = "POST_STUDY_PLANS_REQUESTED";
export const POST_STUDY_PLANS_SUCCEEDED = "POST_STUDY_PLANS_SUCCEEDED";
export const POST_STUDY_PLANS_FAILED = "POST_STUDY_PLANS_FAILED";
export const POST_BUDGET_PLANS_REQUESTED = "POST_BUDGET_PLANS_REQUESTED";
export const POST_BUDGET_PLANS_SUCCEEDED = "POST_BUDGET_PLANS_SUCCEEDED";
export const POST_BUDGET_PLANS_FAILED = "POST_BUDGET_PLANS_FAILED";
export const POST_VERIFY_OTP_REQUESTED = "POST_VERIFY_OTP_REQUESTED";
export const POST_VERIFY_OTP_SUCCEEDED = "POST_VERIFY_OTP_SUCCEEDED";
export const POST_VERIFY_OTP_FAILED = "POST_VERIFY_OTP_FAILED";
export const REQUEST_OTP_REQUESTED = "REQUEST_OTP_REQUESTED";
export const REQUEST_OTP_SUCCEEDED = "REQUEST_OTP_SUCCEEDED";
export const REQUEST_OTP_FAILED = "REQUEST_OTP_FAILED";
export const POST_COMPETITION_SCORES_REQUESTED =
  "POST_COMPETITION_SCORES_REQUESTED";
export const POST_COMPETITION_SCORES_SUCCEEDED =
  "POST_COMPETITION_SCORES_SUCCEEDED";
export const POST_COMPETITION_SCORES_FAILED = "POST_COMPETITION_SCORES_FAILED";

/************************ Actions Creator **************** */
export const registerUserAction = data => ({
  type: REGISTER_USER_REQUESTED,
  payload: { data: data }
});
export const postUserQualificationAction = data => ({
  type: POST_QUALIFICATION_REQUESTED,
  payload: { data: data }
});
export const postUserStudyPlansAction = data => ({
  type: POST_STUDY_PLANS_REQUESTED,
  payload: { data: data }
});
export const postUserBudgetPlansAction = data => ({
  type: POST_BUDGET_PLANS_REQUESTED,
  payload: { data: data }
});
export const postUserCompetitionScoresAction = data => ({
  type: POST_COMPETITION_SCORES_REQUESTED,
  payload: { data: data }
});
export const verifyUserOTPAction = data => ({
  type: POST_VERIFY_OTP_REQUESTED,
  payload: { data: data }
});
export const requestUserOTPAction = data => ({
  type: REQUEST_OTP_REQUESTED,
  payload: { data: data }
});

export const REQUEST_OTP_COUNTING_REQUESTED = "REQUEST_OTP_COUNTING_REQUESTED";
export const REQUEST_OTP_COUNTING_SUCCEEDED = "REQUEST_OTP_COUNTING_SUCCEEDED";
export const REQUEST_OTP_COUNTING_FAILED = "REQUEST_OTP_COUNTING_FAILED";

export const getOTPCountingAction = data => ({
  type: REQUEST_OTP_COUNTING_REQUESTED,
  payload: { data: data }
});
/************************ Actions Creator **************** */
