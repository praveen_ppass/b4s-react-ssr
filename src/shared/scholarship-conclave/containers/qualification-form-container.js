import React from "react";
import { connect } from "react-redux";
import qualificationFrom from "../components/qualification-form";
import { postUserQualificationAction } from "../actions";

const mapStateToProps = ({ scholarshipConclave }) => ({
  ...scholarshipConclave
});

const mapDispatchToProps = dispatch => ({
  submitQualificationData: data => dispatch(postUserQualificationAction(data))
});
export default connect(mapStateToProps, mapDispatchToProps)(qualificationFrom);
