import { connect } from "react-redux";
import thanksPage from "../components/thanks";
const mapStateToProps = ({ scholarshipConclave }) => ({
  ...scholarshipConclave
});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(thanksPage);
