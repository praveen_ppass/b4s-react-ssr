import { connect } from "react-redux";
import otpPage from "../components/otp";
import { fetchUserRules as fetchUserRulesAction } from "../../../constants/commonActions";
import {
  verifyUserOTPAction,
  requestUserOTPAction,
  getOTPCountingAction
} from "../actions";
import { getExistingMobile as getExistingMobileAction } from "../../login/actions";
const mapStateToProps = ({ scholarshipConclave, common }) => ({
  ...scholarshipConclave,
  commonReducer: common
});
const mapDispatchToProps = dispatch => ({
  verifyOTP: data => dispatch(verifyUserOTPAction(data)),
  requestOTP: data => dispatch(requestUserOTPAction(data)),
  fetchUserRules: inputData => dispatch(fetchUserRulesAction(inputData)),
  fetchOTPCounting: inputData => dispatch(getOTPCountingAction(inputData)),
  existingMobileStatus: inputData => dispatch(getExistingMobileAction(inputData))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(otpPage);
