import { connect } from "react-redux";
import FutureStudyPlans from "../components/study-plans-form";
import { postUserStudyPlansAction } from "../actions";
const mapStateToProps = ({ scholarshipConclave }) => ({
  ...scholarshipConclave
});
const mapDispatchToProps = dispatch => ({
  submitStudyPlansData: data => dispatch(postUserStudyPlansAction(data))
});
export default connect(mapStateToProps, mapDispatchToProps)(FutureStudyPlans);
