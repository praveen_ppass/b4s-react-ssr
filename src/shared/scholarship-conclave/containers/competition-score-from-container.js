import { connect } from "react-redux";
import competitionScoreForm from "../components/competition-score-form";
import { postUserCompetitionScoresAction } from "../actions";
const mapStateToProps = ({ scholarshipConclave }) => ({
  ...scholarshipConclave
});

const mapDispatchToProps = dispatch => ({
  submitcompetitionScore: data =>
    dispatch(postUserCompetitionScoresAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  competitionScoreForm
);
