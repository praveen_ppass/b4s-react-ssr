import { connect } from "react-redux";
import studyBudgetForm from "../components/study-budget-form";
import { postUserBudgetPlansAction } from "../actions";

const mapStateToProps = ({ scholarshipConclave }) => ({
  ...scholarshipConclave
});

const mapDispatchToProps = dispatch => ({
  submitBudgetPlans: data => dispatch(postUserBudgetPlansAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(studyBudgetForm);
