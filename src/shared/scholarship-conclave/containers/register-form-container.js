import { connect } from "react-redux";
import registerForm from "../components/register-form";
import { registerUserAction } from "../actions";
import { fetchUserRules } from "../../../constants/commonActions";

const mapStateToProps = ({ scholarshipConclave, common }) => ({
  ...scholarshipConclave,
  userRulesData: common.userRulesData,
  commonType: common.type
});

const mapDispatchToProps = dispatch => ({
  registerUser: data => dispatch(registerUserAction(data)),
  getUserData: data => dispatch(fetchUserRules(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(registerForm);
