import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../../constants/constants";
import fetchClient from "../../../api/fetchClient";

import {
  GET_ALL_DOCUMENT_REQUEST,
  GET_ALL_DOCUMENT_SUCCESS,
  GET_ALL_DOCUMENT_FAILURE,
  GET_ALL_DOCUMENT_UPLOAD_REQUEST,
  GET_ALL_DOCUMENT_UPLOAD_SUCCESS,
  GET_ALL_DOCUMENT_UPLOAD_FAILURE,
  GET_ALL_DOCUMENT_UPLOAD_DELETE_REQUEST,
  GET_ALL_DOCUMENT_UPLOAD_DELETE_SUCCESS,
  GET_ALL_DOCUMENT_UPLOAD_DELETE_FAILURE,
  GET_ALL_DOCUMENT_UPLOAD_COMPLETE_REQUEST,
  GET_ALL_DOCUMENT_UPLOAD_COMPLETE_SUCCESS,
  GET_ALL_DOCUMENT_UPLOAD_COMPLETE_FAILURE,
  FETCH_APPLICATION_DOCS_STEP_REQUEST,
  FETCH_APPLICATION_DOCS_STEP_SUCCESS,
  FETCH_APPLICATION_DOCS_STEP_FAILURE
} from "../actions/applicationDocumentAction";
import { isNull } from "util";
import gblFunc from "../../../globals/globalFunctions";

const getDocumentsAPi = ({ payload }) => {
  let url = `${apiUrl.marchentCall}${payload.userId}/applicationDoc/${payload.scholarshipId}?classId=${payload.presentClass}`;

  // if (payload.presentClass) {
  //   url = url + "&includeGlobal=true";
  // }
  if(localStorage.getItem("profileLocked")){
    if(localStorage.getItem("profileLocked")==0){
      url = url + "&includeGlobal=false" ;
    }else{
      url = url + "&includeGlobal=true" ;
  
    }
  }
  else{
    url = url + "&includeGlobal=true" ;
  }
  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

const completeDocumentAPi = ({ payload }) => {
  return fetchClient
    .post(
      `${apiUrl.marchentCall}${payload.userId}/application/${payload.scholarshipId}/step/${payload.steps}?presentClassId=${payload.presentClassId}`
    )
    .then(res => {
      return res.data;
    });
};

const documentCompletePostApi = ({ payload }) => {
  let scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
  if (payload.isPosted) {
    return fetchClient
      .post(
        `${apiUrl.marchentCall}${payload.userId}/application/${scholarshipId}/step/DOCUMENTS`
      )
      .then(res => {
        return res.data;
      });
  }
};

const documentCompleteDeleteApi = ({ payload }) => {
  if (payload.isMandatory) {
    let scholarshipId = null;

    if (payload.scholarshipId) {
      scholarshipId = payload.scholarshipId;
    } else
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    if (payload.isDeleted) {
      return fetchClient
        .delete(
          `${apiUrl.marchentCall}${payload.userId}/application/${scholarshipId}/step/DOCUMENTS`
        )
        .then(res => {
          return res.data;
        });
    }
  } else {
    return {};
  }
};

const uploadsDocumentsAPi = ({ payload }) => {
  let dataParams = new FormData();
  dataParams.append("docFile", payload.docFile);
  dataParams.append("userDocRequest", payload.userDocRequest);
  let scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
  return fetchClient
    .post(`${apiUrl.marchentCall}${payload.userId}/applicationDoc/${scholarshipId}`, dataParams, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    })
    .then(res => {
      return res.data;
    });
};

const deleteDocumentsAPi = ({ payload }) => {
  return fetchClient
    .delete(
      `${apiUrl.marchentCall}${payload.userId}/application/${payload.scholarshipId}/document/${payload.docId}`
    )
    .then(res => {
      return res.data;
    });
};

function* fetchDocuments(input) {
  try {
    const data = yield call(getDocumentsAPi, input);
    yield put({
      type: GET_ALL_DOCUMENT_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: GET_ALL_DOCUMENT_FAILURE,
      payload: error
    });
  }
}

function* uploadDocuments(input) {
  try {
    const { upoadPost, uploadCompletePost } = yield all({
      upoadPost: call(uploadsDocumentsAPi, input)
      //uploadCompletePost: call(documentCompletePostApi, input)
    });
    let data = { upoadPost, uploadCompletePost };

    yield put({
      type: GET_ALL_DOCUMENT_UPLOAD_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: GET_ALL_DOCUMENT_UPLOAD_FAILURE,
      payload: error
    });
  }
}

function* deleteDocuments(input) {
  try {
    const { deleteDocument, documentCompleteDelete } = yield all({
      deleteDocument: call(deleteDocumentsAPi, input),
      documentCompleteDelete: call(documentCompleteDeleteApi, input)
    });
    let data = { deleteDocument, documentCompleteDelete };
    yield put({
      type: GET_ALL_DOCUMENT_UPLOAD_DELETE_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: GET_ALL_DOCUMENT_UPLOAD_DELETE_FAILURE,
      payload: error
    });
  }
}

function* completeDocuments(input) {
  try {
    const data = yield call(completeDocumentAPi, input);
    yield put({
      type: GET_ALL_DOCUMENT_UPLOAD_COMPLETE_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: GET_ALL_DOCUMENT_UPLOAD_COMPLETE_FAILURE,
      payload: error
    });
  }
}

const fetchInstructionStepApi = ({ scholarshipId, userId }) => {
  // get completed and not completed steps

  if (userId == undefined) {
    if (window != undefined) {
      userId = gblFunc.getStoreUserDetails()["userId"];
    }
  }

  return fetchClient
    .get(`${apiUrl.marchentCall}${userId}/application/${scholarshipId}/step`)
    .then(res => {
      return res.data;
    });
};

function* fetchInstructionStepRequest(input) {
  try {
    const applInstrStep = yield call(fetchInstructionStepApi, input.payload);
    yield put({
      type: FETCH_APPLICATION_DOCS_STEP_SUCCESS,
      payload: applInstrStep
    });
  } catch (error) {
    yield put({
      type: FETCH_APPLICATION_DOCS_STEP_FAILURE,
      payload: error
    });
  }
}

export default function* applicationFormSaga() {
  yield takeEvery(GET_ALL_DOCUMENT_REQUEST, fetchDocuments);
  yield takeEvery(GET_ALL_DOCUMENT_UPLOAD_REQUEST, uploadDocuments);
  yield takeEvery(GET_ALL_DOCUMENT_UPLOAD_DELETE_REQUEST, deleteDocuments);
  yield takeEvery(GET_ALL_DOCUMENT_UPLOAD_COMPLETE_REQUEST, completeDocuments);
  yield takeEvery(
    FETCH_APPLICATION_DOCS_STEP_REQUEST,
    fetchInstructionStepRequest
  );
}
