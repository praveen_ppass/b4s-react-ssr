import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import {
  apiUrl,
  apiKeys,
  apiEndPoints,
  apiRequestData
} from "../../../constants/constants";
import fetchClient from "../../../api/fetchClient";

import {
  FETCH_APPLICATION_FORMS_EXAM_REQUEST,
  FETCH_APPLICATION_FORMS_EXAM_SUCCESS,
  FETCH_APPLICATION_FORMS_EXAM_FAILURE,
  UPDATE_APPLICATION_FORMS_EXAM_REQUEST,
  UPDATE_APPLICATION_FORMS_EXAM_SUCCESS,
  UPDATE_APPLICATION_FORMS_EXAM_FAILURE,
  DELETE_APPLICATION_FORMS_EXAM_REQUEST,
  DELETE_APPLICATION_FORMS_EXAM_SUCCESS,
  DELETE_APPLICATION_FORMS_EXAM_FAILURE
} from "../actions/applicationEntranceExamAction";
import gblFunc from "../../../globals/globalFunctions";

const examConfigApi = ({ payload }) => {
  return fetchClient
    .get(
      `${apiUrl.getApplicationConfig}/${payload.scholarshipId}/form/${
        payload.step
      }/config`
    )
    .then(res => {
      return res.data;
    });
};

const examGetApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .get(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/entranceExam`
    )
    .then(res => {
      return res.data;
    });
};

function* fetchExamConfig(input) {
  try {
    const { examConfig, examGet } = yield all({
      examConfig: call(examConfigApi, input),
      examGet: call(examGetApi, input)
    });
    let data = { examConfig, examGet };
    // let data = yield call(examConfigApi, input);
    yield put({
      type: FETCH_APPLICATION_FORMS_EXAM_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: FETCH_APPLICATION_FORMS_EXAM_FAILURE,
      payload: error
    });
  }
}

const examPostApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .post(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/entranceExam`,
      payload.data
    )
    .then(res => {
      return res.data;
    });
};

const completePostApi = ({ payload }) => {
  if (payload.isPosted) {
    return fetchClient
      .post(
        `${apiUrl.marchentCall}${payload.userId}/application/${
          payload.scholarshipId
        }/step/ENTRANCE_EXAM`
      )
      .then(res => {
        return res.data;
      });
  }
};

function* fetchExamSave(input) {
  try {
    const { examPost, completePost } = yield all({
      examPost: call(examPostApi, input),
      completePost: call(completePostApi, input)
    });
    let data = { examPost, completePost };

    yield put({
      type: UPDATE_APPLICATION_FORMS_EXAM_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: UPDATE_APPLICATION_FORMS_EXAM_FAILURE,
      payload: error
    });
  }
}

const examDeleteApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .delete(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/scholarshipHistory/${payload.wonId}`
    )
    .then(res => {
      return res.data;
    });
};

function* deleteExamCall(input) {
  try {
    const data = yield call(EXAMDeleteApi, input);
    yield put({
      type: DELETE_APPLICATION_FORMS_EXAM_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: DELETE_APPLICATION_FORMS_EXAM_FAILURE,
      payload: error
    });
  }
}

export default function* applicationExamSaga() {
  yield takeEvery(FETCH_APPLICATION_FORMS_EXAM_REQUEST, fetchExamConfig);
  yield takeEvery(UPDATE_APPLICATION_FORMS_EXAM_REQUEST, fetchExamSave);
  /*   yield takeEvery(FETCH_APPLICATION_FORMS_EXAM_REQUEST, fetchExamList); */
}
