import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import {
  apiUrl,
  apiKeys,
  apiEndPoints,
  apiRequestData
} from "../../../constants/constants";
import fetchClient from "../../../api/fetchClient";

import {
  FETCH_APPLICATION_FORMS_FAMILYEARNING_REQUEST,
  FETCH_APPLICATION_FORMS_FAMILYEARNING_SUCCESS,
  FETCH_APPLICATION_FORMS_FAMILYEARNING_FAILURE,
  UPDATE_APPLICATION_FORMS_FAMILYEARNING_REQUEST,
  UPDATE_APPLICATION_FORMS_FAMILYEARNING_SUCCESS,
  UPDATE_APPLICATION_FORMS_FAMILYEARNING_FAILURE,
  DELETE_APPLICATION_FORMS_FAMILYEARNING_REQUEST,
  DELETE_APPLICATION_FORMS_FAMILYEARNING_SUCCESS,
  DELETE_APPLICATION_FORMS_FAMILYEARNING_FAILURE
} from "../actions/familyEarningsActions";
import gblFunc from "../../../globals/globalFunctions";

const familyEarningConfigApi = ({ payload }) => {
  return fetchClient
    .get(
      `${apiUrl.getApplicationConfig}/${payload.scholarshipId}/form/${
        payload.step
      }/config`
    )
    .then(res => {
      return res.data;
    });
};

const familyCompletePostApi = ({ payload }) => {
  if (payload.isPosted) {
    return fetchClient
      .post(
        `${apiUrl.marchentCall}${payload.userId}/application/${
          payload.scholarshipId
        }/step/FAMILY_INFO`
      )
      .then(res => {
        return res.data;
      });
  }
};

const familyCompleteDeleteApi = ({ payload }) => {
  if (payload.isDeleted) {
    return fetchClient
      .delete(
        `${apiUrl.marchentCall}${payload.userId}/application/${
          payload.scholarshipId
        }/step/FAMILY_INFO/`
      )
      .then(res => {
        return res.data;
      });
  }
};

const familyEarningGetApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  let url ="";
  url=`${apiUrl.marchentCall}${userId}/application/${
    payload.scholarshipId
  }/familyInfo`;
  if(localStorage.getItem("profileLocked")){
    if(localStorage.getItem("profileLocked")==0){
      url = url + "?includeGlobal=false" ;
    }else{
      url = url + "?includeGlobal=true" ;
  
    }
  }
  else{
    url = url + "?includeGlobal=true" ;
  }
  return fetchClient
    .get(url)
    .then(res => {
      return res.data;
    });
};

const completedStepApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .get(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/step`
    )
    .then(res => {
      return res.data;
    });
};

const familyEarningPostApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .post(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/familyInfo`,
      payload.data
    )
    .then(res => {
      return res.data;
    });
};

const familyEarningDeleteApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }

  return fetchClient
    .delete(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/familyInfo/${payload.relationId}`
    )
    .then(res => {
      return res.data;
    });
};

const getOccupationAPi = () => {
  return fetchClient.get(`${apiUrl.getOccupation}`).then(res => {
    return res.data;
  });
};

const getRelationAPi = () => {
  const relationArray = [apiRequestData.RULETYPES[14]];
  return fetchClient
    .get(`${apiUrl.rulesList}?filter=${JSON.stringify(relationArray)}`)
    .then(res => {
      return res.data;
    });
};

function* fetchfamilyEarningConfig(input) {
  try {
    const {
      familyEarningDataConfig,
      occupationList,
      familyEarningGetListByUser
    } = yield all({
      familyEarningDataConfig: call(familyEarningConfigApi, input),
      occupationList: call(getOccupationAPi),
      familyEarningGetListByUser: call(familyEarningGetApi, input)
    });

    let data = {
      familyEarningDataConfig,
      occupationList,
      familyEarningGetListByUser
    };

    yield put({
      type: FETCH_APPLICATION_FORMS_FAMILYEARNING_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: FETCH_APPLICATION_FORMS_FAMILYEARNING_FAILURE,
      payload: error
    });
  }
}

function* fetchfamilyEarningSave(input) {
  try {
    const { familyPost, familyCompletePost } = yield all({
      familyPost: call(familyEarningPostApi, input)
      // ,familyCompletePost: call(familyCompletePostApi, input)
    });
    let data = { familyPost, familyCompletePost };
    yield put({
      type: UPDATE_APPLICATION_FORMS_FAMILYEARNING_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: UPDATE_APPLICATION_FORMS_FAMILYEARNING_FAILURE,
      payload: error
    });
  }
}

function* deletefamilyEarningCall(input) {
  try {
    const { familyDelete, familyCompleteDelete } = yield all({
      familyDelete: call(familyEarningDeleteApi, input),
      familyCompleteDelete: call(familyCompleteDeleteApi, input)
    });
    let data = { familyDelete, familyCompleteDelete };
    yield put({
      type: DELETE_APPLICATION_FORMS_FAMILYEARNING_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: DELETE_APPLICATION_FORMS_FAMILYEARNING_FAILURE,
      payload: error
    });
  }
}

export default function* applicationFamilyEarningsSaga() {
  yield takeEvery(
    FETCH_APPLICATION_FORMS_FAMILYEARNING_REQUEST,
    fetchfamilyEarningConfig
  );
  yield takeEvery(
    UPDATE_APPLICATION_FORMS_FAMILYEARNING_REQUEST,
    fetchfamilyEarningSave
  );
  yield takeEvery(
    DELETE_APPLICATION_FORMS_FAMILYEARNING_REQUEST,
    deletefamilyEarningCall
  );
}
