import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../../constants/constants";
import fetchClient from "../../../api/fetchClient";

import {
  GET_ALL_RULES_REQUEST,
  GET_ALL_RULES_SUCCESS,
  GET_ALL_RULES_FAILURE,
  FETCH_APPLICATION_STEP_REQUEST,
  FETCH_APPLICATION_STEP_FAILURE,
  FETCH_APPLICATION_STEP_SUCCESS,
  FETCH_APPLICATION_FORM_INSTRUCTION_REQUEST,
  FETCH_APPLICATION_FORM_INSTRUCTION_SUCCESS,
  FETCH_APPLICATION_FORM_INSTRUCTION_FAILURE,
  FETCH_SCHOLARSHIP_APPLICATION_STEP_FAILURE,
  FETCH_SCHOLARSHIP_APPLICATION_STEP_REQUEST,
  FETCH_SCHOLARSHIP_APPLICATION_STEP_SUCCESS
} from "../actions/applicationFormAction";
import gblFunc from "../../../globals/globalFunctions";

const getRulesAPi = () => {
  const relationArray = [
    "class",
    "subject",
    "course",
    "state",
    "board",
    "orphan",
    "religion",
    "special",
    "gender",
    "quota",
    "family_income",
    "foreign",
    "orphan",
    "subscriber_role",
    "subscriber_relation",
    "entrance_exams",
    "level",
    "scholarship_provider",
    "disabled",
    "earning_member"
  ];
  return fetchClient
    .get(`${apiUrl.rulesList}?filter=${JSON.stringify(relationArray)}`)
    .then(res => {
      return res.data;
    });
};

function* fetchRules() {
  try {
    const data = yield call(getRulesAPi);
    yield put({
      type: GET_ALL_RULES_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: GET_ALL_RULES_FAILURE,
      payload: error
    });
  }
}

const fetchInstructionListApi = slug => {
  let url = `/${slug}/instruction`;

  if (
    typeof window !== undefined &&
    (localStorage.getItem("applicationFormBlock") == "1" || slug == "SGA3")
  ) {
    url = url + "?block=1";
  }

  return fetchClient.get(apiUrl.scholarshipSlugByBsid + url).then(res => {
    return res.data;
  });
};

function* fetchInstructionList(input) {
  try {
    const applInstr = yield call(fetchInstructionListApi, input.payload);
    yield put({
      type: FETCH_APPLICATION_FORM_INSTRUCTION_SUCCESS,
      payload: applInstr
    });
  } catch (error) {
    yield put({
      type: FETCH_APPLICATION_FORM_INSTRUCTION_FAILURE,
      payload: error
    });
  }
}

const fetchInstructionStepApi = ({ scholarshipId, userId }) => {
  // get completed and not completed steps

  if (userId == undefined) {
    if (window != undefined) {
      userId = gblFunc.getStoreUserDetails()["userId"];
    }
  }

  return fetchClient
    .get(`${apiUrl.marchentCall}${userId}/application/${scholarshipId}/step`)
    .then(res => {
      return res.data;
    });
};

function* fetchInstructionStepRequest(input) {
  try {
    const applInstrStep = yield call(fetchInstructionStepApi, input.payload);
    yield put({
      type: FETCH_APPLICATION_STEP_SUCCESS,
      payload: applInstrStep
    });
  } catch (error) {
    yield put({
      type: FETCH_APPLICATION_STEP_FAILURE,
      payload: error
    });
  }
}

const fetchSchAppStepApi = ({ scholarshipId }) => {
  return fetchClient
    .get(`${apiUrl.scholarshipSlugByBsid}/${scholarshipId}/application/step`)
    .then(res => {
      return res.data;
    });
};

function* fetcSchApplicableStepRequest(input) {
  try {
    const appSchStep = yield call(fetchSchAppStepApi, input.payload);
    yield put({
      type: FETCH_SCHOLARSHIP_APPLICATION_STEP_SUCCESS,
      payload: appSchStep
    });
  } catch (error) {
    yield put({
      type: FETCH_SCHOLARSHIP_APPLICATION_STEP_FAILURE,
      payload: error
    });
  }
}

export default function* applicationFormSaga() {
  yield takeEvery(GET_ALL_RULES_REQUEST, fetchRules);
  yield takeEvery(
    FETCH_APPLICATION_FORM_INSTRUCTION_REQUEST,
    fetchInstructionList
  );
  yield takeEvery(FETCH_APPLICATION_STEP_REQUEST, fetchInstructionStepRequest);
  yield takeEvery(
    FETCH_SCHOLARSHIP_APPLICATION_STEP_REQUEST,
    fetcSchApplicableStepRequest
  );
}
