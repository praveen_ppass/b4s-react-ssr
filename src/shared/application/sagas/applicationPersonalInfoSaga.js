import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import { apiUrl } from "../../../constants/constants";
import fetchClient from "../../../api/fetchClient";

import {
  APPLICATION_FETCH_PERSONAL_INFO_REQUESTED,
  APPLICATION_FETCH_PERSONAL_INFO_SUCCEEDED,
  APPLICATION_FETCH_PERSONAL_INFO_FAILED,
  APPLICATION_UPDATE_PERSONAL_INFO_REQUESTED,
  APPLICATION_UPDATE_PERSONAL_INFO_SUCCEEDED,
  APPLICATION_UPDATE_PERSONAL_INFO_FAILED,
  FETCH_PERSONAL_INFO_CONFIG_REQUESTED,
  FETCH_PERSONAL_INFO_CONFIG_SUCCEEDED,
  FETCH_PERSONAL_INFO_CONFIG_FAILED
} from "../actions/personalInfoActions";

const userDataCall = ({ payload }) => {
  const url = `${apiUrl.applicationPersonalInfo}/${
    payload.userId
  }/application/${payload.scholarshipId}/personalInfo?includeGlobal=true`;

  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchUserData(input) {
  try {
    const userData = yield call(userDataCall, input);
    yield put({
      type: APPLICATION_FETCH_PERSONAL_INFO_SUCCEEDED,
      payload: userData
    });
  } catch (error) {
    yield put({
      type: FETCH_PERSONAL_INFO_CONFIG_FAILED,
      payload: error
    });
  }
}

/************ START APPLICATION CONFIG************** */
const fetchApplicationConfigCall = ({ inputData }) =>
  fetchClient
    .get(
      `${apiUrl.getApplicationConfig}/${inputData.scholarshipId}/form/${
        inputData.step
      }/config`
    )
    .then(res => {
      return res.data;
    });

function* fetchApplicationConfig(input) {
  try {
    const { payload } = input;
    const configData = yield call(fetchApplicationConfigCall, payload);

    yield put({
      type: FETCH_PERSONAL_INFO_CONFIG_SUCCEEDED,
      payload: configData
    });
  } catch (error) {
    yield put({
      type: FETCH_PERSONAL_INFO_CONFIG_FAILED,
      payload: error
    });
  }
}
/************ END APPLICATION CONFIG************** */

/************ START UPDATE PERSONAL INFO************** */
const updatePersonalInfoCall = inputData =>
  fetchClient
    .post(
      `${apiUrl.applicationPersonalInfo}/${inputData.userId}/application/${
        inputData.scholarshipId
      }/${inputData.step}`,
      inputData.formData
    )
    .then(res => {
      return res.data;
    });

function* updatePersonalInfo(input) {
  try {
    const { payload } = input;
    const personalInfoData = yield call(updatePersonalInfoCall, payload);

    yield put({
      type: APPLICATION_UPDATE_PERSONAL_INFO_SUCCEEDED,
      payload: personalInfoData
    });
  } catch (error) {
    yield put({
      type: APPLICATION_UPDATE_PERSONAL_INFO_FAILED,
      payload: error
    });
  }
}
/************ END UPDATE PERSONAL INFO************** */
export default function* applicationPersonalInfoSaga() {
  yield takeEvery(APPLICATION_FETCH_PERSONAL_INFO_REQUESTED, fetchUserData);
  yield takeEvery(FETCH_PERSONAL_INFO_CONFIG_REQUESTED, fetchApplicationConfig);
  yield takeEvery(
    APPLICATION_UPDATE_PERSONAL_INFO_REQUESTED,
    updatePersonalInfo
  );
}
