import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import {
  apiUrl,
  apiKeys,
  apiEndPoints,
  apiRequestData
} from "../../../constants/constants";
import fetchClient from "../../../api/fetchClient";

import {
  FETCH_APPLICATION_FORMS_QUESTION_REQUEST,
  FETCH_APPLICATION_FORMS_QUESTION_SUCCESS,
  FETCH_APPLICATION_FORMS_QUESTION_FAILURE,
  UPDATE_APPLICATION_FORMS_QUESTION_REQUEST,
  UPDATE_APPLICATION_FORMS_QUESTION_SUCCESS,
  UPDATE_APPLICATION_FORMS_QUESTION_FAILURE
} from "../actions/questionAction";
import gblFunc from "../../../globals/globalFunctions";

const QuestionApi = ({ payload }) => {

  let url ="";
  url=`${apiUrl.marchentCall}${
    gblFunc.getStoreUserDetails()["userId"]
  }/scholarshipQuestion/${payload.scholarshipId}`;
  if(localStorage.getItem("profileLocked")){
    if(localStorage.getItem("profileLocked")==0){
      url = url + "?includeGlobal=false" ;
    }else{
      url = url + "?includeGlobal=true" ;
  
    }
  }
  else{
    url = url + "?includeGlobal=true" ;
  }
  return fetchClient
    .get(url)
    .then(res => {
      return res.data;
    });
};

const saveQuestionApi = ({ payload }) => {
  return fetchClient
    .post(
      `${apiUrl.marchentCall}${
        gblFunc.getStoreUserDetails()["userId"]
      }/scholarshipQuestion/${payload.scholarshipId}`,
      payload.question
    )
    .then(res => {
      return res.data;
    });
};

function* fetchQuestionConfig(input) {
  try {
    const data = yield call(QuestionApi, input);

    yield put({
      type: FETCH_APPLICATION_FORMS_QUESTION_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: FETCH_APPLICATION_FORMS_QUESTION_FAILURE,
      payload: error
    });
  }
}

function* questionSave(input) {
  try {
    const data = yield call(saveQuestionApi, input);

    yield put({
      type: UPDATE_APPLICATION_FORMS_QUESTION_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: UPDATE_APPLICATION_FORMS_QUESTION_FAILURE,
      payload: error
    });
  }
}

export default function* applicationQuestionSaga() {
  yield takeEvery(
    FETCH_APPLICATION_FORMS_QUESTION_REQUEST,
    fetchQuestionConfig
  );
  yield takeEvery(UPDATE_APPLICATION_FORMS_QUESTION_REQUEST, questionSave);
}
