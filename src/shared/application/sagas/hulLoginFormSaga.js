import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../../constants/constants";
import fetchClient from "../../../api/fetchClient";

import {
  FETCH_USER_CODE_REQUEST,
  FETCH_USER_CODE_SUCCESS,
  FETCH_USER_CODE_FAIL
} from "../actions/hulLoginFormAction";

const fetchUserCodeApi = ({ payload }) => {
  return fetchClient
    .get(`${apiUrl.hulUserCode}/${payload.userCode}/validate`)
    .then(res => {
      return res.data;
    });
};

function* fetchUserCode(input) {
  try {
    const userCode = yield call(fetchUserCodeApi, input);

    yield put({
      type: FETCH_USER_CODE_SUCCESS,
      payload: userCode
    });
  } catch (error) {
    yield put({
      type: FETCH_USER_CODE_FAIL,
      payload: error.response
    });
  }
}

export default function* hulLoginFormSaga() {
  yield takeEvery(FETCH_USER_CODE_REQUEST, fetchUserCode);
}
