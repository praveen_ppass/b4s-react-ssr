import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../../constants/constants";
import fetchClient from "../../../api/fetchClient";

import {
  GET_ALL_SUMMERY_RULES_REQUEST,
  GET_ALL_SUMMERY_RULES_SUCCESS,
  GET_ALL_SUMMERY_RULES_FAILURE,
  FETCH_SCHOLARSHIP_APPLY_REQUEST_SUMMARY,
  FETCH_SCHOLARSHIP_APPLY_SUCCESS_SUMMARY,
  FETCH_SCHOLARSHIP_APPLY_FAILURE_SUMMARY
} from "../actions/applicationSummeryAction";
import gblFunc from "../../../globals/globalFunctions";

const personalsConfigGetApi = ({ payload }) =>
  fetchClient
    .get(
      `${apiUrl.getApplicationConfig}/${
        payload.scholarshipId
      }/form/PERSONAL_INFO/config`
    )
    .then(res => {
      return res.data;
    });

const educationConfigGetApi = ({ payload }) => {
  return fetchClient
    .get(
      `${apiUrl.getApplicationConfig}/${
        payload.scholarshipId
      }/form/EDUCATION_INFO/config`
    )
    .then(res => {
      return res.data;
    });
};

const familyInfoConfigGetApi = ({ payload }) => {
  return fetchClient
    .get(
      `${apiUrl.getApplicationConfig}/${
        payload.scholarshipId
      }/form/FAMILY_INFO/config`
    )
    .then(res => {
      return res.data;
    });
};

const referenceConfigGetApi = ({ payload }) => {
  return fetchClient
    .get(
      `${apiUrl.getApplicationConfig}/${
        payload.scholarshipId
      }/form/REFERENCES/config`
    )
    .then(res => {
      return res.data;
    });
};

const fetchSchApplyApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .put(
      `${apiUrl.scholarshipSlugByBsid}/${
        payload.scholarshipId
      }/user/scholarshipapply`,
      {
        step: 2,
        userIds: [userId]
      }
    )
    .then(res => {
      return res.data;
    });
};

function* fetchSchApply(input) {
  try {
    const schApplyList = yield call(fetchSchApplyApi, input);

    yield put({
      type: FETCH_SCHOLARSHIP_APPLY_SUCCESS_SUMMARY,
      payload: schApplyList
    });
  } catch (error) {
    yield put({
      type: FETCH_SCHOLARSHIP_APPLY_FAILURE_SUMMARY,
      payload: error
    });
  }
}

const getSummeryAPi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  let url = `${apiUrl.marchentCall}${userId}/application/${
    payload.scholarshipId
  }/summary`;

  if (payload.presentClass) {
    url = url + "?presentClassId=" + payload.presentClass;
  }
  if (payload.hul3PUser) {
    url = url + "&hul3PUser=" + payload.hul3PUser;
  }
  return fetchClient.get(url).then(res => {
    return res.data;
  }); 
};

function* fetchSummery(input) {
  try {
    const {
      summary /* ,
      personalConfig,
      educationConfig,
      familyConfig,
      referenceConfig */
    } = yield all({
      summary: call(getSummeryAPi, input)
      /*  personalConfig: call(personalsConfigGetApi, input),
      educationConfig: call(educationConfigGetApi, input),
      familyConfig: call(familyInfoConfigGetApi, input),
      referenceConfig: call(referenceConfigGetApi, input) */
    });

    let data = {
      summary /* ,
      personalConfig,
      educationConfig,
      familyConfig,
      referenceConfig */
    };

    yield put({
      type: GET_ALL_SUMMERY_RULES_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: GET_ALL_SUMMERY_RULES_FAILURE,
      payload: error
    });
  }
}

export default function* applicationSummerySaga() {
  yield takeEvery(GET_ALL_SUMMERY_RULES_REQUEST, fetchSummery);
  yield takeEvery(FETCH_SCHOLARSHIP_APPLY_REQUEST_SUMMARY, fetchSchApply);
}
