import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import {
  apiUrl,
  apiKeys,
  apiEndPoints,
  apiRequestData
} from "../../../constants/constants";
import fetchClient from "../../../api/fetchClient";

import {
  FETCH_APPLICATION_FORMS_REFERENCE_REQUEST,
  FETCH_APPLICATION_FORMS_REFERENCE_SUCCESS,
  FETCH_APPLICATION_FORMS_REFERENCE_FAILURE,
  UPDATE_APPLICATION_FORMS_REFERENCE_REQUEST,
  UPDATE_APPLICATION_FORMS_REFERENCE_SUCCESS,
  UPDATE_APPLICATION_FORMS_REFERENCE_FAILURE,
  DELETE_APPLICATION_FORMS_REFERENCE_REQUEST,
  DELETE_APPLICATION_FORMS_REFERENCE_SUCCESS,
  DELETE_APPLICATION_FORMS_REFERENCE_FAILURE,
  UPDATE_STEP_REFERENCE_SUCCESS,
  UPDATE_STEP_REFERENCE_FAILURE,
  UPDATE_STEP_REFERENCE_REQUEST
} from "../actions/referenceAction";
import gblFunc from "../../../globals/globalFunctions";

const referenceConfigApi = ({ payload }) => {
  return fetchClient
    .get(
      `${apiUrl.getApplicationConfig}/${payload.scholarshipId}/form/${
        payload.step
      }/config`
    )
    .then(res => {
      return res.data;
    });
};

const referenceGetApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  let url ="";
  url=`${apiUrl.marchentCall}${userId}/application/${
    payload.scholarshipId
  }/reference`;
  if(localStorage.getItem("profileLocked")){
    if(localStorage.getItem("profileLocked")==0){
      url = url + "?includeGlobal=false" ;
    }else{
      url = url + "?includeGlobal=true" ;
  
    }
  }
  else{
    url = url + "?includeGlobal=true" ;
  }
  return fetchClient
    .get(url)
    .then(res => {
      return res.data;
    });
};

const referencesCompletePostApi = ({ payload }) => {
  if (payload.isPosted) {
    return fetchClient
      .post(
        `${apiUrl.marchentCall}${payload.userId}/application/${
          payload.scholarshipId
        }/step/REFERENCES`
      )
      .then(res => {
        return res.data;
      });
  }
};

const updateStepsPostApi = ({ payload }) => {
  if (payload.isPosted) {
    return fetchClient
      .post(
        `${apiUrl.marchentCall}${payload.userId}/application/${
          payload.scholarshipId
        }/step/REFERENCES`
      )
      .then(res => {
        return res.data;
      });
  }
};

const referencesCompleteDeleteApi = ({ payload }) => {
  if (payload.isDeleted) {
    return fetchClient
      .delete(
        `${apiUrl.marchentCall}${payload.userId}/application/${
          payload.scholarshipId
        }/step/REFERENCES/`
      )
      .then(res => {
        return res.data;
      });
  }
};

const referencePostApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .post(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/reference`,
      payload.data
    )
    .then(res => {
      return res.data;
    });
};

const referenceDeleteApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .delete(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/reference/${payload.referenceId}`
    )
    .then(res => {
      return res.data;
    });
};

const getOccupationAPi = () => {
  return fetchClient.get(`${apiUrl.getOccupation}`).then(res => {
    return res.data;
  });
};

const getRelationAPi = () => {
  const relationArray = [apiRequestData.RULETYPES[14]];
  return fetchClient
    .get(`${apiUrl.rulesList}?filter=${JSON.stringify(relationArray)}`)
    .then(res => {
      return res.data;
    });
};

function* fetchReferenceConfig(input) {
  try {
    const {
      referenceDataConfig,
      occupationList,
      relationList,
      referenceGetListByUser
    } = yield all({
      referenceDataConfig: call(referenceConfigApi, input),
      occupationList: call(getOccupationAPi),
      relationList: call(getRelationAPi),
      referenceGetListByUser: call(referenceGetApi, input)
    });
    const { subscriber_relation } = relationList;
    let data = {
      referenceDataConfig,
      occupationList,
      subscriber_relation,
      referenceGetListByUser
    };

    yield put({
      type: FETCH_APPLICATION_FORMS_REFERENCE_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: FETCH_APPLICATION_FORMS_REFERENCE_FAILURE,
      payload: error
    });
  }
}

function* fetchReferenceSave(input) {
  try {
    const { refrencesPost } = yield all({
      refrencesPost: call(referencePostApi, input)
    });
    let data = { refrencesPost };
    yield put({
      type: UPDATE_APPLICATION_FORMS_REFERENCE_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: UPDATE_APPLICATION_FORMS_REFERENCE_FAILURE,
      payload: error
    });
  }
}
function* updateStepCall(input) {
  try {
    const { updateStepsPost } = yield all({
      updateStepsPost: call(updateStepsPostApi, input)
    });
    let data = { updateStepsPost };
    yield put({
      type: UPDATE_STEP_REFERENCE_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: UPDATE_STEP_REFERENCE_FAILURE,
      payload: error
    });
  }
}

function* deleteReferenceCall(input) {
  try {
    const { referencesDelete, referencesCompleteDelete } = yield all({
      referencesDelete: call(referenceDeleteApi, input),
      referencesCompleteDelete: call(referencesCompleteDeleteApi, input)
    });
    let data = { referencesDelete, referencesCompleteDelete };
    yield put({
      type: DELETE_APPLICATION_FORMS_REFERENCE_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: DELETE_APPLICATION_FORMS_REFERENCE_FAILURE,
      payload: error
    });
  }
}

export default function* applicationReferenceSaga() {
  yield takeEvery(
    FETCH_APPLICATION_FORMS_REFERENCE_REQUEST,
    fetchReferenceConfig
  );
  yield takeEvery(
    UPDATE_APPLICATION_FORMS_REFERENCE_REQUEST,
    fetchReferenceSave
  );
  yield takeEvery(
    DELETE_APPLICATION_FORMS_REFERENCE_REQUEST,
    deleteReferenceCall
  );
  yield takeEvery(UPDATE_STEP_REFERENCE_REQUEST, updateStepCall);
}
