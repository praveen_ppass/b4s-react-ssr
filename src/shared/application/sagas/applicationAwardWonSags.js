import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import {
  apiUrl,
  apiKeys,
  apiEndPoints,
  apiRequestData
} from "../../../constants/constants";
import fetchClient from "../../../api/fetchClient";

import {
  FETCH_APPLICATION_FORMS_AWARDWON_REQUEST,
  FETCH_APPLICATION_FORMS_AWARDWON_SUCCESS,
  FETCH_APPLICATION_FORMS_AWARDWON_FAILURE,
  UPDATE_APPLICATION_FORMS_AWARDWON_REQUEST,
  UPDATE_APPLICATION_FORMS_AWARDWON_SUCCESS,
  UPDATE_APPLICATION_FORMS_AWARDWON_FAILURE,
  DELETE_APPLICATION_FORMS_AWARDWON_REQUEST,
  DELETE_APPLICATION_FORMS_AWARDWON_SUCCESS,
  DELETE_APPLICATION_FORMS_AWARDWON_FAILURE
} from "../actions/applicationAwardWonAction";
import gblFunc from "../../../globals/globalFunctions";

const AwardWonConfigApi = ({ payload }) => {
  return fetchClient
    .get(
      `${apiUrl.getApplicationConfig}/${payload.scholarshipId}/form/${
        payload.step
      }/config`
    )
    .then(res => {
      return res.data;
    });
};

const AwardWonGetApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .get(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/scholarshipHistory`
    )
    .then(res => {
      return res.data;
    });
};

const AwardWonPostApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .post(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/scholarshipHistory`,
      payload.data
    )
    .then(res => {
      return res.data;
    });
};

const AwardWonDeleteApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .delete(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/scholarshipHistory/${payload.wonId}`
    )
    .then(res => {
      return res.data;
    });
};

function* fetchAwardWonConfig(input) {
  try {
    const { AwardWonDataConfig, AwardWonGetListByUser } = yield all({
      AwardWonDataConfig: call(AwardWonConfigApi, input),
      AwardWonGetListByUser: call(AwardWonGetApi, input)
    });
    let data = {
      AwardWonDataConfig,
      AwardWonGetListByUser
    };

    yield put({
      type: FETCH_APPLICATION_FORMS_AWARDWON_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: FETCH_APPLICATION_FORMS_AWARDWON_FAILURE,
      payload: error
    });
  }
}

function* fetchAwardWonSave(input) {
  try {
    const data = yield call(AwardWonPostApi, input);
    yield put({
      type: UPDATE_APPLICATION_FORMS_AWARDWON_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: UPDATE_APPLICATION_FORMS_AWARDWON_FAILURE,
      payload: error
    });
  }
}

function* deleteAwardWonCall(input) {
  try {
    const data = yield call(AwardWonDeleteApi, input);
    yield put({
      type: DELETE_APPLICATION_FORMS_AWARDWON_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: DELETE_APPLICATION_FORMS_AWARDWON_FAILURE,
      payload: error
    });
  }
}

export default function* applicationAwardWonSaga() {
  yield takeEvery(
    FETCH_APPLICATION_FORMS_AWARDWON_REQUEST,
    fetchAwardWonConfig
  );
  yield takeEvery(UPDATE_APPLICATION_FORMS_AWARDWON_REQUEST, fetchAwardWonSave);
  yield takeEvery(
    DELETE_APPLICATION_FORMS_AWARDWON_REQUEST,
    deleteAwardWonCall
  );
}
