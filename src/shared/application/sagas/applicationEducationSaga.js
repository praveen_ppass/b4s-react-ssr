import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl, dependantApiUrl } from "../../../constants/constants";
import fetchClient from "../../../api/fetchClient";

import {
  FETCH_EDUCATION_INFO_FORM_REQUEST,
  FETCH_EDUCATION_INFO_FORM_SUCCESS,
  FETCH_EDUCATION_INFO_FORM_FAILURE,
  FETCH_DISTRICT_LIST_REQUEST,
  FETCH_DISTRICT_LIST_SUCCESS,
  FETCH_DISTRICT_LIST_FAILURE,
  UPDATE_EDUCATION_INFO_FORM_REQUEST,
  UPDATE_EDUCATION_INFO_FORM_SUCCESS,
  UPDATE_EDUCATION_INFO_FORM_FAILURE,
  FETCH_SUBJECT_REQUEST,
  FETCH_SUBJECT_SUCCESS,
  FETCH_SUBJECT_FAILURE,
  FETCH_EDUCATION_INFO_CONFIG_REQUEST,
  FETCH_EDUCATION_INFO_CONFIG_SUCCESS,
  FETCH_EDUCATION_INFO_CONFIG_FAILURE,
  UPDATE_EDUCATION_INFO_STEP_REQUEST,
  UPDATE_EDUCATION_INFO_STEP_SUCCESS,
  UPDATE_EDUCATION_INFO_STEP_FAILURE,
  FETCH_EDUCATION_CLASS_CONFIG_REQUEST,
  FETCH_EDUCATION_CLASS_CONFIG_SUCCESS,
  FETCH_EDUCATION_CLASS_CONFIG_FAILURE,
  DELETE_EDUCATION_INFO_REQUEST,
  DELETE_EDUCATION_INFO_SUCCESS,
  DELETE_EDUCATION_INFO_FAILURE,
  SAVE_LIF9_PRESENT_CLASS_REQUEST,
  SAVE_LIF9_PRESENT_CLASS_SUCCESS,
  SAVE_LIF9_PRESENT_CLASS_FAILURE,
  PRESENT_CLASS_INFO_REQUEST,
  PRESENT_CLASS_INFO_SUCCESS,
  PRESENT_CLASS_INFO_FAILURE
} from "../actions/educationInfoAction";
import gblFunc from "../../../globals/globalFunctions";

const getBoardListApi = () => {
  return fetchClient.get(`${apiUrl.getBoardList}`).then(res => {
    return res.data;
  });
};

const saveLif9Present = ({ payload }) => {
  const userId = gblFunc.getStoreUserDetails()["userId"];
  return fetchClient
    .post(`${apiUrl.marchentCall}${userId}/rule`, payload.userRules)
    .then(res => {
      return res.data;
    });
};

function* fetchLif9Save(input) {
  try {
    const data = yield call(saveLif9Present, input);
    yield put({
      type: SAVE_LIF9_PRESENT_CLASS_REQUEST,
      payload: data
    });
  } catch (error) {
    yield put({
      type: SAVE_LIF9_PRESENT_CLASS_FAILURE,
      payload: error
    });
  }
}

const educationGetApi = ({ payload }) => {
 let userId = null;
 if (typeof window !== "undefined") {
    userId = gblFunc.getStoreUserDetails()["userId"];
 }
  let url = `${apiUrl.marchentCall}${userId}/application/${
    payload.scholarshipId
  }/educationInfo`;

  if (payload.presentClassId) {
    url = url + "?presentClassId=" + payload.presentClassId;
  }
  if(localStorage.getItem("profileLocked")){
    if(localStorage.getItem("profileLocked")==0){
      url = url + "&includeGlobal=false" ;
    }else{
      url = url + "&includeGlobal=true" ;
  
    }
  }
  else{
    url = url + "&includeGlobal=true" ;
  }
 


  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

const getRelationAPi = () => {
  const relationArray = ["class", "subject", "course", "state", "board"];
  return fetchClient
    .get(`${apiUrl.rulesList}?filter=${JSON.stringify(relationArray)}`)
    .then(res => {
      return res.data;
    });
};

function* fetchEducation(input) {
  try {
    const {
      // educationDataApi,
      //   occupationList,
      boardList,
      userEducationList
    } = yield all({
      // educationDataApi: call(educationConfigApi, input),
      // relationList: call(getRelationAPi),
      boardList: call(getBoardListApi),
      userEducationList: call(educationGetApi, input)
    });

    let data = {
      // educationDataApi,
      boardList,
      userEducationList
    };

    yield put({
      type: FETCH_EDUCATION_INFO_FORM_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: FETCH_EDUCATION_INFO_FORM_FAILURE,
      payload: error
    });
  }
}

const saveEducationInfoApi = ({ payload }) => {
  let userId = null;
  if (typeof window !== undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .post(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/educationInfo?presentClassId=${payload.presentClassId ? payload.presentClassId : null}`,
      payload.education
    )
    .then(res => {
      return res.data;
    });
};

function* fetchEducationSave(input) {
  try {
    const data = yield call(saveEducationInfoApi, input);
    yield put({
      type: UPDATE_EDUCATION_INFO_FORM_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: UPDATE_EDUCATION_INFO_FORM_FAILURE,
      payload: error
    });
  }
}

function districtApi({ payload }) {
  return fetchClient
    .get(`${dependantApiUrl["state"]}/${payload.id}`)
    .then(res => {
      return res.data;
    });
}

function* fetchDistrictConfig(input) {
  try {
    const data = yield call(districtApi, input);
    yield put({
      type: FETCH_DISTRICT_LIST_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: FETCH_DISTRICT_LIST_FAILURE,
      payload: error
    });
  }
}

const subjectApi = ({ payload }) => {
  return fetchClient.get(`${apiUrl.ruleFilter}/${payload.id}`).then(res => {
    return res.data;
  });
};

function* fetchSubject(input) {
  try {
    const data = yield call(subjectApi, input);
    yield put({
      type: FETCH_SUBJECT_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: FETCH_SUBJECT_FAILURE,
      payload: error
    });
  }
}

const educationConfigApi = ({ payload }) => {
  let url = `${apiUrl.getApplicationConfig}/${payload.scholarshipId}/form/${
    payload.step
  }/config`;
  if (payload.scholarshipType) {
    url = `${url}?applicationCategoryId=${payload.scholarshipType}`;
  }
  return fetchClient.get(url).then(res => {
    return res.data;
  });
};

function* fetchEducationConfig(input) {
  try {
    const eduConfigdata = yield call(educationConfigApi, input);
    yield put({
      type: FETCH_EDUCATION_INFO_CONFIG_SUCCESS,
      payload: eduConfigdata
    });
  } catch (error) {
    yield put({
      type: FETCH_EDUCATION_INFO_CONFIG_FAILURE,
      payload: error
    });
  }
}
/************ Update step Call************ */
const updateStepApi = ({ payload }) => {
  const query = `?presentClassId=${payload.presentClassId ? payload.presentClassId : null}`
  return fetchClient.post(
    `${apiUrl.applicationStepCall}${payload.userId}/application/${
      payload.scholarshipId
    }/step/${payload.step}${payload.step == "EDUCATION_INFO" ? query : ""}`
  );
};
function* updateFormStep(input) {
  try {
    const updateStepInfo = yield call(updateStepApi, input);
    yield put({
      type: UPDATE_EDUCATION_INFO_STEP_SUCCESS,
      payload: updateStepInfo.data
    });
  } catch (error) {
    yield put({
      type: UPDATE_EDUCATION_INFO_STEP_FAILURE,
      payload: error
    });
  }
}

/************ Update step Call************ */

/************ CLASS CONFIG CALL ************ */
const fetchClassConfigApi = ({ payload }) => {
  let url = `${apiUrl.getApplicationConfig}/${
    payload.scholarshipId
  }/educationclass/${payload.classID}/config`;
  if (payload.query) {
    url = `${url}?configRequiredForClassId=${payload.query}`;
  }

  return fetchClient.get(url).then(res => {
    return res.data;
  });
};
function* fetchClassConfig(input) {
  try {
    const data = yield call(fetchClassConfigApi, input);
    yield put({
      type: FETCH_EDUCATION_CLASS_CONFIG_SUCCESS,
      payload: data
    });
  } catch (error) {
    yield put({
      type: FETCH_EDUCATION_CLASS_CONFIG_FAILURE,
      payload: error
    });
  }
}

/************ CLASS CONFIG CALL ************ */

/************ DELETE ACADEMIC ************ */
const deleteEducationInfoApi = ({ payload }) => {
  let url = `${apiUrl.marchentCall}${payload.userId}/application/${
    payload.scholarshipId
  }/educationInfo/${payload.classId}`;

  return fetchClient.delete(url).then(res => {
    return res.data;
  });
};
function* deleteEducationInfo(input) {
  try {
    const deleteAcademic = yield call(deleteEducationInfoApi, input);
    yield put({
      type: DELETE_EDUCATION_INFO_SUCCESS,
      payload: deleteAcademic
    });
  } catch (error) {
    yield put({
      type: DELETE_EDUCATION_INFO_FAILURE,
      payload: error
    });
  }
}

const fetchPresentClassApi = (payload) => {
let userId = null;
 if (typeof window !== "undefined") {
    userId = gblFunc.getStoreUserDetails()["userId"];
 }
  let url = `${apiUrl.presentClassInfo}/${userId}/application/${payload.payload}/educationInfo/present-class`;
  return fetchClient.get(url).then(res => {
    return res.data;
  });
};
function* fetchPresentClass(input) {
  try {
    const presntClassData = yield call(fetchPresentClassApi, input);
    yield put({
      type: PRESENT_CLASS_INFO_SUCCESS,
      payload: presntClassData
    });
  } catch (error) {
    yield put({
      type: PRESENT_CLASS_INFO_FAILURE,
      payload: error
    });
  }
}

export default function* applicationEducationSaga() {
  yield takeEvery(FETCH_EDUCATION_INFO_FORM_REQUEST, fetchEducation);
  yield takeEvery(FETCH_DISTRICT_LIST_REQUEST, fetchDistrictConfig);
  yield takeEvery(FETCH_SUBJECT_REQUEST, fetchSubject);
  yield takeEvery(UPDATE_EDUCATION_INFO_FORM_REQUEST, fetchEducationSave);
  yield takeEvery(FETCH_EDUCATION_INFO_CONFIG_REQUEST, fetchEducationConfig);
  yield takeEvery(UPDATE_EDUCATION_INFO_STEP_REQUEST, updateFormStep);
  yield takeEvery(FETCH_EDUCATION_CLASS_CONFIG_REQUEST, fetchClassConfig);
  yield takeEvery(DELETE_EDUCATION_INFO_REQUEST, deleteEducationInfo);
  yield takeEvery(SAVE_LIF9_PRESENT_CLASS_REQUEST, fetchLif9Save);
  yield takeEvery(PRESENT_CLASS_INFO_REQUEST, fetchPresentClass);
}
