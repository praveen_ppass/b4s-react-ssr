import { call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../../constants/constants";
import fetchClient from "../../../api/fetchClient";

import {
  FETCH_APPLICATION_INSTRUCTION_REQUEST,
  FETCH_APPLICATION_INSTRUCTION_SUCCESS,
  FETCH_APPLICATION_INSTRUCTION_FAILURE,
  FETCH_SCHOLARSHIP_CATEGORY_SUCCESS,
  FETCH_SCHOLARSHIP_CATEGORY_FAILURE,
  FETCH_SCHOLARSHIP_CATEGORY_REQUEST,
  SAVE_SCHOLARSHIP_TYPE_REQUEST,
  SAVE_SCHOLARSHIP_TYPE_SUCCESS,
  SAVE_SCHOLARSHIP_TYPE_FAILURE,
  CHECK_DISBURSAL_STATUS_REQUEST,
  CHECK_DISBURSAL_STATUS_SUCCESS,
  CHECK_DISBURSAL_STATUS_FAILURE
} from "../actions/applicationInstructionActions";

const applicationInstructionsApi = slug => {
  let url = `/${slug}/instruction`;

  if (
    typeof window !== undefined &&
    (location.href.includes("?block=1") || slug == "SGA3")
  ) {
    localStorage.setItem("applicationFormBlock", 1);
    url = url + "?block=1";
  }
  // const url = `/${slug}?extraInfo?infoType=consent`;
  return fetchClient.get(apiUrl.scholarshipSlugByBsid + url).then(res => {
    return res.data;
  });
};

function* fetchApplicationInstructions(input) {
  try {
    const applInstr = yield call(applicationInstructionsApi, input.payload);
    yield put({
      type: FETCH_APPLICATION_INSTRUCTION_SUCCESS,
      payload: applInstr
    });
  } catch (error) {
    yield put({
      type: FETCH_APPLICATION_INSTRUCTION_FAILURE,
      payload: error
    });
  }
}

const scholarshipCategoryAPI = schid => {
  const url = `/${schid}/applicationcategorytype`;

  return fetchClient.get(apiUrl.scholarshipSlugByBsid + url).then(res => {
    return res.data;
  });
};

function* fetchSchCategory(input) {
  try {
    const schCategory = yield call(scholarshipCategoryAPI, input.payload);
    yield put({
      type: FETCH_SCHOLARSHIP_CATEGORY_SUCCESS,
      payload: schCategory
    });
  } catch (error) {
    yield put({
      type: FETCH_SCHOLARSHIP_CATEGORY_FAILURE,
      payload: error
    });
  }
}

const schTypeAPI = ({ schType, schID, userId }) => {
  const url = `/${schID}/user/${userId}/scholarshipapply`;
let applyMode = "online";
if (typeof window !== undefined && location.href.includes("applyMode")) {
let splitingUrl = location.href.split("?");
let queryArray = splitingUrl[1];

if (queryArray.includes("&")) {
let splitByAnd = queryArray.split("&");
applyMode = splitByAnd
  .filter(query => query.includes("applyMode"))
  .join("")
  .split("=")[1];
}

if (!queryArray.includes("&")) {
applyMode = queryArray.split("=")[1];
}
}

return fetchClient
.post(apiUrl.scholarshipSlugByBsid + url, {
applyMode,
scholarshipType: schType
})
.then(res => {
return res.data;
});
};

function* saveSchType(input) {
  try {
    const schType = yield call(schTypeAPI, input.payload);
    yield put({
      type: SAVE_SCHOLARSHIP_TYPE_SUCCESS,
      payload: schType
    });
  } catch (error) {
    yield put({
      type: SAVE_SCHOLARSHIP_TYPE_FAILURE,
      payload:
        error.response && error.response.data && error.response.data.message
          ? error.response.data.message
          : null
    });
  }
}

const checkDisbursalAPI = () => {
  return fetchClient.get(`${apiUrl.checkStatus}/${localStorage && localStorage.getItem('userId') && localStorage.getItem('userId')}/scholarship/disbursal`).then(res => {
    return (!res.data || !res.data.disbursalId) ? true : ({message: "You are an existing scholar. As per our records, some information from your end are pending for disbursal. Please provide the same before applying to new scholarships."});
  });
};

function* checkDisbursal(input=null) {
  try {
    const disbursalStatus = yield call(checkDisbursalAPI, input);
    yield put({
      type: CHECK_DISBURSAL_STATUS_SUCCESS,
      payload: disbursalStatus
    });
  } catch (error) {
    yield put({
      type: CHECK_DISBURSAL_STATUS_FAILURE,
      payload: error
    });
  }
}

export default function* applicationInstructionsSaga() {
  yield takeEvery(
    FETCH_APPLICATION_INSTRUCTION_REQUEST,
    fetchApplicationInstructions
  );
  yield takeEvery(FETCH_SCHOLARSHIP_CATEGORY_REQUEST, fetchSchCategory);
  yield takeEvery(SAVE_SCHOLARSHIP_TYPE_REQUEST, saveSchType);
  yield takeEvery(CHECK_DISBURSAL_STATUS_REQUEST, checkDisbursal);
}
