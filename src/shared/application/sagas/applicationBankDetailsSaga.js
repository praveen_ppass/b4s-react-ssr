import { all, call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../../constants/constants";
import fetchClient from "../../../api/fetchClient";

import {
  FETCH_APPLICATION_BANK_DETAILS_SUCCESS,
  FETCH_APPLICATION_BANK_DETAILS_REQUEST,
  FETCH_APPLICATION_BANK_DETAILS_FAILURE,
  SAVE_APPLICATION_BANK_DETAILS_REQUEST,
  SAVE_APPLICATION_BANK_DETAILS_SUCCESS,
  SAVE_APPLICATION_BANK_DETAILS_FAILURE,
  FETCH_BANK_DTS_CONFIG_REQUEST,
  FETCH_BANK_DTS_CONFIG_SUCCESS,
  FETCH_BANK_DTS_CONFIG_FAILURE,
  UPDATE_BANK_DT_STEP_REQUEST,
  UPDATE_BANK_DT_STEP_SUCCESS,
  UPDATE_BANK_DT_STEP_FAILURE
} from "../actions/applicationBankDetailsAction";
import gblFunc from "../../../globals/globalFunctions";

const fetchBankDetailsApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .get(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/bankDetail`
    )
    .then(res => {
      return res.data;
    });
};

function* fetchBankDetails(input) {
  try {
    const fetchedBankDts = yield call(fetchBankDetailsApi, input);

    yield put({
      type: FETCH_APPLICATION_BANK_DETAILS_SUCCESS,
      payload: fetchedBankDts
    });
  } catch (error) {
    yield put({
      type: FETCH_APPLICATION_BANK_DETAILS_FAILURE,
      payload: error
    });
  }
}

const saveAppBankDtsApi = ({ payload }) => {
  let userId = "";
  if (window != undefined) {
    userId = gblFunc.getStoreUserDetails()["userId"];
  }
  return fetchClient
    .post(
      `${apiUrl.marchentCall}${userId}/application/${
        payload.scholarshipId
      }/bankDetail`,
      payload.data
    )
    .then(res => {
      return res.data;
    });
};

function* saveAppBankDts(input) {
  try {
    const fetchBankData = yield call(saveAppBankDtsApi, input);
    yield put({
      type: SAVE_APPLICATION_BANK_DETAILS_SUCCESS,
      payload: fetchBankData
    });
  } catch (error) {
    yield put({
      type: SAVE_APPLICATION_BANK_DETAILS_FAILURE,
      payload: error
    });
  }
}

const bankDtsConfigApi = ({ payload }) => {
  return fetchClient
    .get(
      `${apiUrl.getApplicationConfig}/${
        payload.scholarshipId
      }/form/BANK_DETAIL/config`
    )
    .then(res => {
      return res.data;
    });
};

function* bankDtsConfig(input) {
  try {
    const bnkConfig = yield call(bankDtsConfigApi, input);

    yield put({
      type: FETCH_BANK_DTS_CONFIG_SUCCESS,
      payload: bnkConfig
    });
  } catch (err) {
    yield put({
      type: FETCH_BANK_DTS_CONFIG_FAILURE,
      payload: err
    });
  }
}

const bankStepCallApi = ({ payload }) => {
  return fetchClient.post(
    `${apiUrl.applicationStepCall}${payload.userId}/application/${
      payload.scholarshipId
    }/step/${payload.step}`
  );
};

function* bankStepCall(input) {
  try {
    const bankStep = yield call(bankStepCallApi, input);

    yield put({
      type: UPDATE_BANK_DT_STEP_SUCCESS,
      payload: bankStep
    });
  } catch (err) {
    yield put({
      type: UPDATE_BANK_DT_STEP_FAILURE,
      payload: err
    });
  }
}

export default function* applicationBankDtsSaga() {
  yield takeEvery(FETCH_APPLICATION_BANK_DETAILS_REQUEST, fetchBankDetails);
  yield takeEvery(SAVE_APPLICATION_BANK_DETAILS_REQUEST, saveAppBankDts);
  yield takeEvery(FETCH_BANK_DTS_CONFIG_REQUEST, bankDtsConfig);
  yield takeEvery(UPDATE_BANK_DT_STEP_REQUEST, bankStepCall);
}
