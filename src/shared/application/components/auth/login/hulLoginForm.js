import React, { Component } from "react";

import { withRouter } from "react-router-dom";
import { ruleRunner } from "../../../../../validation/ruleRunner";
import {
  required,
  isEmail,
  minLength,
  lengthRange
} from "../../../../../validation/rules";
import {
  LOGIN_USER_FAILED,
  LOGIN_USER_SUCCEEDED,
  LOG_USER_OUT_SUCCEEDED
} from "../../../../login/actions";

import {
  FETCH_USER_CODE_SUCCESS,
  FETCH_USER_CODE_FAIL
} from "../../../actions/hulLoginFormAction";
import { childEduBSID, formattedEmail } from "../../../formconfig";
import gblFunc from "../../../../../globals/globalFunctions";

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.loginUser = this.loginUser.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);

    this.state = {
      username: "",
      password: "",
      socialLogin: true,
      isFormValid: false,
      validations: {
        username: null,
        password: null
      },
      disableFormEle: true
    };

    this.handleChange = this.handleChange.bind(this);
  }

  loginUser(event) {
    event.preventDefault();
    if (this.checkFormValidations()) {
      let getFormatEmail = null;
      if (childEduBSID[this.props.slug]) {
        getFormatEmail = formattedEmail(
          childEduBSID[this.props.slug],
          this.state.username
        );
      }
      this.props.loginUser({
        username: getFormatEmail,
        password: this.state.password
      });
    }
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "username":
        validationObject.name = "*User Code";
        validationObject.validationFunctions = [required, lengthRange(3, 9)];
        return validationObject;

      case "password":
        validationObject.name = "*Password ";
        validationObject.validationFunctions = [required, minLength(6)];
        return validationObject;
    }
  }

  handleChange(event) {
    const { id, value } = event.target;
    if (
      id === "username" &&
      (this.props.slug == "SKKS1" || value.length >= 5)
    ) {
      this.props.validateUserCode({
        userCode: value
      });
    }

    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );

    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    this.setState({
      [id]: value,
      validations
    });
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );

      validations[key] = validationResult[key];

      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }

    this.setState({
      validations,
      isFormValid
    });

    return isFormValid;
  }

  componentWillReceiveProps(nextProps) {
    const { type, loginType } = nextProps;
    let validations = { ...this.state.validations };

    switch (type) {
      case FETCH_USER_CODE_FAIL:
        validations.username = nextProps.serverUserCodeErrors;
        this.setState({
          validations
        });
        break;

      case FETCH_USER_CODE_SUCCESS:
        const { validate, rsType } = nextProps.userCode;
        if (!validate) {
          validations.username =
            "Employee Code is not eligible for this scheme";
        }
        this.setState(
          {
            disableFormEle: validate ? false : true,
            validations
          },
          () => gblFunc.storeHulAuthType(rsType)
        );
        break;
      case LOG_USER_OUT_SUCCEEDED:
        localStorage.removeItem("hulType");
        break;
    }

    if (loginType === "LOGIN_USER_FAILED") {
      let validations = { ...this.state.validations };

      validations.username = nextProps.serverErrorLogin.includes(
        "Invalid email address"
      )
        ? nextProps.serverErrorLogin.replace(
            "Invalid email address",
            "Invalid employee code"
          )
        : nextProps.serverErrorLogin;
      this.setState({
        validations
      });
    }
  }

  render() {
    return (
      <article className="bglogin pull-left paddingBoth">
        <article className="applicationform register">
          <article>
            <h2>Please Log in to start application</h2>

            <form name="regForm" onSubmit={this.loginUser}>
              <section className="ctrl-wrapper">
                <article className="form-group">
                  <input
                    type="username"
                    className="form-control"
                    minLength="3"
                    name="username"
                    placeholder={
                      this.props.slug == "SKKS1"
                        ? "Enter DE ID"
                        : "Enter employee code"
                    }
                    id="username"
                    value={this.state.username}
                    onChange={this.handleChange}
                    autoComplete="off"
                    tabIndex="1"
                  />
                  {this.state.validations.username ? (
                    <span className="error animated bounce">
                      {this.state.validations.username}
                    </span>
                  ) : null}
                </article>
              </section>
              <section className="ctrl-wrapper">
                <article className="form-group">
                  <input
                    type="password"
                    className="form-control"
                    maxLength="20"
                    minLength="6"
                    name="password"
                    placeholder={
                      this.props.slug == "SKKS1" ? "Phone Number" : "password"
                    }
                    onChange={this.handleChange}
                    value={this.state.password}
                    id="password"
                    autoComplete="off"
                    tabIndex="2"
                    disabled={this.state.disableFormEle}
                  />
                  {this.state.validations.password ? (
                    <span className="error animated bounce">
                      {this.state.validations.password}
                    </span>
                  ) : null}
                </article>
              </section>

              <section className="ctrl-wrapper">
                <article className="form-group">
                  <button
                    type="submit"
                    className="btn-block greenBtn margintop"
                    tabIndex="3"
                    disabled={this.state.disableFormEle}
                  >
                    Sign In!
                  </button>
                </article>
              </section>
            </form>
            {/* <article className="textPara">
              <dd>
                By clicking on any of the buttons, you agree to Buddy4Study's
                &nbsp;
                <Link to="/terms-and-conditions">terms &amp; conditions</Link>
              </dd>
              <br />
            </article> */}
          </article>
        </article>
      </article>
    );
  }
}

export default withRouter(LoginForm);
