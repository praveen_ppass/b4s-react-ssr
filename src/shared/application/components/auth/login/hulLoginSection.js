import React, { Component } from "react";

import LoginForm from "../../../containers/hulLoginFormContainer";

class HulLoginSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentForm: "LOGIN_FORM"
    };
    this.renderCurrentForm = this.renderCurrentForm.bind(this);
    this.showLoginForm = this.showLoginForm.bind(this);
  }

  showLoginForm() {
    this.setState({
      currentForm: "LOGIN_FORM"
    });
  }

  renderCurrentForm(currentForm) {
    switch (currentForm) {
      case "LOGIN_FORM":
        return <LoginForm slug={this.props.slug} />;
    }
  }

  render() {
    return this.renderCurrentForm(this.state.currentForm);
  }
}

export default HulLoginSection;
