import React, { Component } from "react";
import ApplicationStatusInfo from "../applicationInstructions/applicationInstructionStatus";
import LoginSection from "../auth/login/loginSection";
import RegisterSection from "../auth/register/registerSection";
import HulLoginSection from "./login/hulLoginSection";
import { childEduBSID } from "../../formconfig";

class LoginRegisterSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSection: "Register"
    };
    this.renderCurrentForm = this.renderCurrentForm.bind(this);
    this.showRegisterSection = this.showRegisterSection.bind(this);
    this.showLoginSection = this.showLoginSection.bind(this);
    this.onTabChange = this.onTabChange.bind(this);
  }

  onTabChange(event) {
    event.stopPropagation();
    const { id } = event.target;
    const parentId = event.target.parentNode.id;

    const isLoginOrRegister = value =>
      value === "Login" || value === "Register";

    if (isLoginOrRegister(id) || isLoginOrRegister(parentId)) {
      this.setState({
        currentSection: id || parentId
      });
    }
  }

  showRegisterSection() {
    this.setState({
      currentSection: "Register"
    });
  }

  showLoginSection() {
    this.setState({
      currentSection: "Login"
    });
  }

  componentDidMount() {
    if (childEduBSID[this.props.slug]) {
      this.setState({
        currentSection: "HulLogin"
      });
    }
  }

  renderCurrentForm() {
    const { currentSection } = this.state;

    switch (currentSection) {
      case "Login":
        return (
          <LoginSection
            showRegisterSection={this.showRegisterSection}
            showLoginSection={this.showLoginSection}
          />
        );

      case "Register":
        return (
          <RegisterSection
            showLoginSection={this.showLoginSection}
            showRegisterSection={this.showRegisterSection}
          />
        );

      case "ForgotPassword":
        return <ForgotPasswordForm />;
      case "HulLogin":
        return <HulLoginSection slug={this.props.slug} />;
    }
  }

  render() {
    let renderedLogin = null;

    if (this.props && childEduBSID[this.props.slug]) {
      renderedLogin = (
        <article>
          <ul onClick={this.onTabChange} className="optional-links">
            <li
              id="hulLogin"
              className={
                this.state.currentSection === "HulLogin" ? "active" : ""
              }
            >
              <span>Login</span>
            </li>
          </ul>
          {this.renderCurrentForm()}
        </article>
      );
    } else {
      renderedLogin = (
        <article>
          <ul onClick={this.onTabChange} className="optional-links">
            <li
              id="Login"
              className={this.state.currentSection === "Login" ? "active" : ""}
            >
              <span>Login</span>
            </li>
            <li
              id="Register"
              className={
                this.state.currentSection === "Register" ? "active" : ""
              }
            >
              <span>Register</span>
            </li>
          </ul>
          {this.renderCurrentForm()}
        </article>
      );
    }
    let ApplicationStatus = this.props.isAuth ? (
      <ApplicationStatusInfo
        logUserOut={this.props.logUserOut}
        schCate={this.props.schCate}
        schCategory={this.props.schCategory}
        onSchCateHandler={this.props.onSchCateHandler}
        showDeadlineEnd={this.props.isDeadlineNotExceed}
        startApplication={this.props.startApplication}
        schApply={this.props.schApply}
        slug={this.props.slug}
        withCode={this.props.withCode}
      />
    ) : (
      renderedLogin
    );

    return ApplicationStatus;
  }
}

export default LoginRegisterSection;
