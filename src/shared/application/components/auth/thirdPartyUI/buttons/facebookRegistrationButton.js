import React from "react";
import SocialLogin from "react-social-login";

const FacebookRegisterButton = ({ children, triggerLogin, ...props }) => (
  <span onClick={triggerLogin} className="btn btn-block facebook" {...props}>
    <i className="fa fa-facebook" aria-hidden="true" /> &nbsp;Register with
    Facebook
    {children}
  </span>
);

export default SocialLogin(FacebookRegisterButton);
