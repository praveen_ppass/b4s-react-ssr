import React from "react";
import ValidationError from "../../components/validationError";
const OtherTextField = props => {
  return (
    <article className="col-md-4">
      <article className="form-group">
        <input
          type="text"
          required
          data-custom={props.custom}
          value={props.fieldValue}
          id={props.fieldId}
          placeholder={props.placeHolder}
          onChange={props.clickHandler}
        />
        <label>{props.fieldLabel}</label>
        <ValidationError fieldValidation={props.fieldValidation} />
      </article>
    </article>
  );
};
export default OtherTextField;
