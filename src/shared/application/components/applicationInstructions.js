import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";

import LoginRegisterSection from "./loginRegisterSection";
import AlertMessagePopup from "../../common/components/alertMsg";
import Needapphelp from "../components/needHelpApp";
import {
  FETCH_USER_RULES_SUCCESS,
  FETCH_SCHOLARSHIP_APPLY_SUCCESS,
  FETCH_SCHOLARSHIP_APPLY_FAILURE
} from "../../../constants/commonActions";
import gblFunc from "../../../globals/globalFunctions";
import moment from "moment";
import {
  messages,
  dateProcess,
  getCurrentDate,
  encode
} from "../../../constants/constants";
import {
  SOCIAL_LOGIN_USER_SUCCEEDED,
  REGISTER_USER_SUCCEEDED,
  LOGIN_USER_SUCCEEDED,
  LOG_USER_OUT_SUCCEEDED,
  FORGOT_USER_PASSWORD_SUCCESS
} from "../../login/actions";
import Loader from "../../common/components/loader";
import {
  FETCH_SCHOLARSHIP_CATEGORY_SUCCESS,
  FETCH_APPLICATION_INSTRUCTION_SUCCESS,
  SAVE_SCHOLARSHIP_TYPE_SUCCESS,
  FETCH_APPLICATION_INSTRUCTION_FAILURE
} from "../actions/applicationInstructionActions";
import { needHelpConfig, authorizeByCode } from "../formconfig";

class ApplicationInstructions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      needHelpName: "info@buddy4study.com",
      needHelpPhone: "+91-8929016461",
      tabs: ["Eligibility", "Important info"],
      isActive: true,
      load: true,
      showAlertMessagePopup: false,
      alertMessage: "",
      isDeadlineNotExceed: false,
      schCategory: null,
      schApply: null,
      isInstructionExist: 0,
      myProfileRedirect: false
    };

    this.toggleTabs = this.toggleTabs.bind(this);
    this.rawMarkUp = this.rawMarkUp.bind(this);
    this.showAlertMessagePopup = this.showAlertMessagePopup.bind(this);
    this.hideAlertMessagePopup = this.hideAlertMessagePopup.bind(this);
    this.checkDeadlineExceedHandler = this.checkDeadlineExceedHandler.bind(
      this
    );
    this.logUserOut = this.logUserOut.bind(this);
    this.onSchCateHandler = this.onSchCateHandler.bind(this);
    this.startApplication = this.startApplication.bind(this);
  }

  componentDidMount() {
    gblFunc.getAuthToken()
      ? this.props.loadScholarshipDetail(this.props.match.params.slug)
      : null;
  }

  showAlertMessagePopup(message) {
    this.setState({
      showAlertMessagePopup: true,
      alertMessage: message
    });
  }

  startApplication(schType) {
    const user_list = gblFunc.getStoreUserDetails();

    if (!this.state.schApply) {
      this.props.saveSchType({
        schType: schType,
        userId: user_list.userId,
        schID: this.props.schDtdata.scholarshipId
      });
    } else {
      this.props.history.push(
        `/application/${this.props.match.params.slug.toUpperCase()}/form/personalInfo`
      );
    }
  }

  logUserOut() {
    this.props.logUserOut();
  }

  onSchCateHandler(e, f = null) {
    if (!f && !e.target) {
      this.setState({
        schCategory: e
      });
    } else {
      if (f) {
        this.setState({
          schCategory: f
        });
      } else {
        this.setState({
          schCategory: e.target.value
        });
      }
    }
  }

  hideAlertMessagePopup() {
    this.setState({
      showAlertMessagePopup: false,
      alertMessage: ""
    });
  }

  checkDeadlineExceedHandler(date) {
    let isDateExceed = moment().isSameOrBefore(date, "day");

    return isDateExceed;
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    let user_list = gblFunc.getStoreUserDetails();
    switch (type) {
      case "VERIFY_TOKEN":
        if (
          nextProps.userLoginData &&
          nextProps.userLoginData.data &&
          nextProps.userLoginData.data.data &&
          nextProps.userLoginData.data.data.mobileVerified == "0"
        ) {
          this.props.history.push({
            pathname: "/otp",
            state: {
              userId: nextProps.userLoginData.data.data.userId,
              mobile: nextProps.userLoginData.data.data.mobile,
              countryCode: nextProps.userLoginData.data.data.countryCode,
              temp_Token: nextProps.userLoginData.data.data.token,
              location:
                this.props.location && this.props.location.pathname
                  ? this.props.location.pathname
                  : null
            }
          });
        }
        break;
      case LOGIN_USER_SUCCEEDED:
        let { userId } = nextProps.userLoginData;

        // Store auth token
        // Fix for auth token
        gblFunc.storeAuthToken(nextProps.userLoginData.access_token);
        this.props.fetchUserRules({
          userid: userId
        });

        break;
      case LOG_USER_OUT_SUCCEEDED:
        !nextProps.schDtdata
          ? this.props.loadScholarshipDetail(this.props.match.params.slug)
          : null;
        break;

      case FETCH_USER_RULES_SUCCESS:
        //SAVE USER RELEVANT DATA IN LOCALSTORAGE HERE.
        //USER IS AUTHENTICATED IN THIS CASE ONLY ( USER RULES ARE FETCHED AND TOKEN IS FETCHED)

        // Store login details here
        gblFunc.storeUserDetails(nextProps.userRulesData);

        gblFunc.storeAuthDetails(this.props.userLoginData);

        // if (!this.props.applicationStatus) {
        //   this.props.fetchApplicationStatus({ userId: user_list.userId });
        // }

        this.props.loadScholarshipDetail(this.props.match.params.slug);

        // this.showAlertMessagePopup(messages.login.success);

        break;
      case REGISTER_USER_SUCCEEDED:
        this.showAlertMessagePopup(messages.register.success);
        break;
      case REGISTER_USER_FAILED:
        break;
      case FETCH_APPLICATION_INSTRUCTION_SUCCESS:
        //nextProps.schDtdata.scholarshipId

        if (
          nextProps.schDtdata &&
          nextProps.schDtdata.contactNumber &&
          nextProps.schDtdata.contactEmail
        ) {
          this.setState({
            needHelpName: nextProps.schDtdata.contactEmail,
            needHelpPhone: nextProps.schDtdata.contactNumber
          });
        }
        if (gblFunc.isUserAuthenticated()) {
          this.props.scholarshipCategory(nextProps.schDtdata.scholarshipId);
        }
        break;
      case FETCH_SCHOLARSHIP_APPLY_FAILURE:
        this.setState({
          schApply: false
        });
        break;
      case FETCH_SCHOLARSHIP_APPLY_SUCCESS:
        if (
          nextProps.schApply &&
          nextProps.schApply.scholarshipTypeCategoryName
        ) {
          this.setState({
            schApply: true
          });
        }
        break;
      case FETCH_SCHOLARSHIP_CATEGORY_SUCCESS:
        this.props.getSchApply({
          scholarshipId: nextProps.schDtdata.scholarshipId,
          userId: user_list.userId
        });

        break;
      case FORGOT_USER_PASSWORD_SUCCESS:
        this.showAlertMessagePopup(messages.forgotPssword.success);
        break;
      case SAVE_SCHOLARSHIP_TYPE_SUCCESS:
          if (nextProps.schType && nextProps.schType.message) {
            return this.setState({
              alertMessage: nextProps.schType.message,
              showAlertMessagePopup: true,
              myProfileRedirect: true
            })
          }
        this.props.history.push(
          `/application/${this.props.match.params.slug.toUpperCase()}/form/personalInfo`
        );
        break;
      case FETCH_APPLICATION_INSTRUCTION_FAILURE:
        this.setState({
          isInstructionExist: 2
        });
        break;
      default:
        break;
    }
  }

  toggleTabs(tabName) {
    let isActiveTab = tabName == "Eligibility" ? true : false;
    this.setState(() => ({
      isActive: isActiveTab
    }));
  }

  rawMarkUp(data) {
    let rawMarkup = data;
    return { __html: rawMarkup };
  }

  hideAlertMessagePopup() {
    this.setState({
      showAlertMessagePopup: false,
      alertMessage: ""
    });
  }

  render() {
    const { isInstructionExist } = this.state;
    const { vleUser, applicationSchID } = gblFunc.getStoreUserDetails();
    let { match, location } = this.props;
    let dateExeedNot = null;
    let showInstructionContent = "";
    const isAuth = gblFunc.isUserAuthenticated();

    let scholarshipSlug = "",
      scholarshipName = "",
      scholarshipLogo = "",
      deadLine = "",
      scholarshipId = "",
      instruction = "",
      schCate = null,
      timeFormat = null;
    if (this.props.schDtdata) {
      scholarshipSlug = this.props.schDtdata.slug;
      scholarshipName = this.props.schDtdata.title;
      scholarshipLogo = this.props.schDtdata.logoUrl;
      instruction = this.props.schDtdata.instruction;
      scholarshipId = this.props.schDtdata.scholarshipId;
      // eligibility = this.props.schDtdata.scholarshipBodyMultilinguals[0]
      //   .eligibility;
      // importantInfo = this.props.schDtdata.scholarshipBodyMultilinguals[0]
      //   .moreDetails;
      scholarshipName = gblFunc.replaceWithLoreal(scholarshipName, true);
      deadLine = this.props.schDtdata.deadline;
      if (deadLine) {
        timeFormat = moment(deadLine, "YYYY-MM-DD").format("DD-MM-YYYY");
      }
      if (deadLine == null) {
        timeFormat = "Always Open";
      }
      if (isAuth) {
        gblFunc.storeApplicationScholarshipId(scholarshipId);
      }

      if (deadLine) {
        dateExeedNot = moment().isSameOrBefore(deadLine, "day");
      }

      if (deadLine == null) {
        dateExeedNot = true;
      }

      if (
        location &&
        location.search &&
        location.search.includes("code=") &&
        authorizeByCode[match.params.slug] &&
        typeof window !== undefined
      ) {
        const authDeadlineObj = authorizeByCode[match.params.slug];
        const extendDeadlineCode = location.search.split("=")[1];

        dateExeedNot =
          authDeadlineObj["code"] === extendDeadlineCode &&
            dateProcess(authDeadlineObj["exp"]) > dateProcess(getCurrentDate())
            ? true
            : false;

        sessionStorage.setItem("WFkwMjAzMjAxOUhVUzI=", dateExeedNot);
      } else {
        sessionStorage.setItem("WFkwMjAzMjAxOUhVUzI=", false);
      }
    }
    if (this.props.scholarshipCate) {
      schCate = this.props.scholarshipCate;
    }

    if (this.props.schDtdata && Object.keys(this.props.schDtdata).length) {
      showInstructionContent = (
        <article className="row">
          <article className="col-md-12">
            <a target="_blank" href={`/scholarship/${scholarshipSlug}`}>
              <img
                alt={scholarshipName}
                className="scholarshipLogo"
                src={scholarshipLogo}
              />
            </a>
            {match.params.slug === "SGA3" ? (
              <h3 className="text-center">{scholarshipName}</h3>
            ) : (
                <a target="_blank" href={`/scholarship/${scholarshipSlug}`}>
                  <h3 className="text-center">{scholarshipName}</h3>
                </a>
              )}
            <p className="text-center equial-padding-b">
              <strong>Deadline: {timeFormat}</strong>
            </p>
            <article className="col-md-7">
              <ul className="optional-links">
                <li className="active active1">Instructions</li>
              </ul>
              <article>
                <article className="eligibility unorderList">
                  <article>
                    <div
                      dangerouslySetInnerHTML={this.rawMarkUp(instruction)}
                    />
                  </article>
                </article>
              </article>
            </article>

            {/* Start login form */}

            <article className="col-md-5">
              <LoginRegisterSection
                isSwiggy={false}
                isAuth={isAuth}
                logUserOut={this.logUserOut}
                schCate={schCate}
                slug={this.props.match.params.slug}
                schCategory={this.state.schCategory}
                schApply={this.state.schApply}
                onSchCateHandler={this.onSchCateHandler}
                isDeadlineNotExceed={dateExeedNot}
                startApplication={this.startApplication}
              />
            </article>
          </article>
        </article>
      );
    }

    if (isInstructionExist == 2) {
      showInstructionContent = (
        <article className="row">
          <article className="col-md-12">
            <a target="_blank" href={`/scholarship/${scholarshipSlug}`}>
              <img
                alt={scholarshipName}
                className="scholarshipLogo"
                src={scholarshipLogo}
              />
            </a>
            {match.params.slug === "SGA3" ? (
              <h3 className="text-center">{scholarshipName}</h3>
            ) : (
                <a target="_blank" href={`/scholarship/${scholarshipSlug}`}>
                  <h3 className="text-center">{scholarshipName}</h3>
                </a>
              )}
            {/* <p className="text-center equial-padding-b">
          <strong>Deadline: {timeFormat}</strong>
        </p> */}
            <article className="col-md-12">
              <article>
                <article className="eligibility unorderList">
                  <article>
                    <p style={{ textAlign: "center" }}>
                      This scholarship is not available. Please click{" "}
                      <Link to={`/myscholarship`}>here</Link> to explore more
                      scholarship matching with your profile.
                    </p>
                  </article>
                </article>
              </article>
            </article>
          </article>
        </article>
      );
    }

    return (
      <section>
        {vleUser &&
          vleUser != "true" &&
          (applicationSchID == "null" ||
            applicationSchID == "undefined" ||
            applicationSchID == "") ? (
            <Redirect to={`vle/student-list`} />
          ) : (
            ""
          )}
        <AlertMessagePopup
          msg={this.state.alertMessage}
          isShow={this.state.showAlertMessagePopup}
          status
          close={this.hideAlertMessagePopup}
          myProfileRedirect={this.state.myProfileRedirect}
          redirectMyProfile={() => this.props.history.push('/myprofile')}
        />
        <section className="container application">
          <Loader isLoader={this.props.showLoader} />
          {showInstructionContent}
        </section>

        <Needapphelp
          needHelpName={this.state.needHelpName}
          needHelpPhone={this.state.needHelpPhone}
        />
      </section>
    );
  }
}

const TabItem = ({ tabName, toggleTabs, isActive }) => {
  return (
    <li
      onClick={() => toggleTabs(tabName)}
      className={
        tabName == "Eligibility" && isActive
          ? "active"
          : tabName == "Important info" && !isActive
            ? "active"
            : ""
      }
    >
      {tabName}
    </li>
  );
};

export default ApplicationInstructions;
