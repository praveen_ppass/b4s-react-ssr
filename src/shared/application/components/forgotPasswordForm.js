import React, { Component } from "react";

import { ruleRunner } from "../../../validation/ruleRunner";
import { required, isEmail } from "../../../validation/rules";
class ForgotPasswordForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      isFormValid: false,
      validations: {
        email: null
      }
    };
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.submitForgotPassword = this.submitForgotPassword.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.goLogin = this.goLogin.bind(this);
  }

  goLogin() {
    this.props.showLoginForm();
  }

  submitForgotPassword(event) {
    event.preventDefault();
    const { email } = this.state;

    if (this.checkFormValidations()) {
      const params = { email, portalId: 1 };
      this.props.forgotUserPassword({ params });
    }
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state[key],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.serverErrorForgotPassword) {
      let validations = { ...this.state.validations };
      validations.email = nextProps.serverErrorForgotPassword;
      this.setState({ validations });
    }
  }

  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    switch (fieldID) {
      case "email":
        validationObject.name = "* Email ";
        validationObject.validationFunctions = [required, isEmail];
        return validationObject;
    }
  }
  /************ end - validation part - get message and required validation******************* */

  /************ start - validation part - check form submit validation******************* */
  onFormFieldChange(event) {
    const { id, value } = event.target;
    const { name, validationFunctions } = this.getValidationRulesObject(id);
    const validationResult = ruleRunner(
      value,
      id,
      name,
      ...validationFunctions
    );
    let validations = { ...this.state.validations };
    validations[id] = validationResult[id];

    this.setState({
      [id]: value,
      validations
    });
  }

  render() {
    return (
      <article className="row applicationform">
        <form
          name="forgotForm"
          autocomplete="off"
          onSubmit={this.submitForgotPassword}
        >
          <h2 className="epadding">Forgot Login Password?</h2>
          <section className="text-center push-left">
            <dd className="epadding">
              Enter your e-mail below and we will send you reset instructions!
            </dd>
            <section className="ctrl-wrapper">
              <article className="form-group">
                <i className="fa fa-lock iconpos hide" />
                <input
                  type="email"
                  name="email"
                  id="email"
                  className="form-control"
                  placeholder="Enter your email"
                  autoComplete="off "
                  value={this.state.email}
                  onChange={this.onFormFieldChange}
                  required
                />
                {this.state.validations.email ? (
                  <span className="error animated bounce" name="email">
                    {this.state.validations.email}
                  </span>
                ) : null}
              </article>
            </section>
            <section className="ctrl-wrapper">
              <article className="form-group">
                <button type="submit" className="btn-block greenBtn margintop">
                  Send
                </button>
                <br />
                <a href="javascript:void(0);" onClick={this.goLogin}>
                  Login
                </a>
              </article>
            </section>
          </section>
        </form>
      </article>
    );
  }
}

export default ForgotPasswordForm;
