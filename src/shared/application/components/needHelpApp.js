import React, { Component } from "react";

{
  /* Start Need Help Application Popup */
}
export default class NeedAppHelp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showAppModal: false,
      showAppButton: true
    };
    this.showAppNeedHelp = this.showAppNeedHelp.bind(this);
  }
  showAppNeedHelp() {
    this.setState({
      ...this.state,
      showAppModal: !this.state.showAppModal,
      showAppButton: !this.state.showAppButton
    });
  }
  render() {
    return (
      <section
        className={
          this.state.showAppModal
            ? "needAppHelpBtn posChange"
            : "needAppHelpBtn"
        }
      >
        {this.state.showAppButton ? (
          <span
            className={this.state.showAppModal ? "fadeOut" : "fadeIn"}
            onClick={this.showAppNeedHelp}
          />
        ) : null}
        <section
          className={
            this.state.showAppModal ? "overlayNH show" : "overlayNH hide"
          }
        />
        <section
          className={
            this.state.showAppModal
              ? "nh-winPopup ani-in"
              : "nh-winPopup ani-out"
          }
        >
          <i className="nh-closeBtn" onClick={this.showAppNeedHelp} />
          <h5>Facing issues? Talk to us</h5>
          <p>
            <i className="fa fa-envelope-open-o" aria-hidden="true" />
            <a href={`mailto:${this.props.needHelpName}`}>
              {this.props.needHelpName}
            </a>
          </p>
          <p>
            <i className="fa fa-phone" aria-hidden="true" />{" "}
            <a href={`tel:${(this.props.needHelpPhone.substring(0, this.props.needHelpPhone.indexOf("("))).trim()}`}>
              {this.props.needHelpPhone}
            </a>
          </p>
        </section>
      </section>
    );
  }
}
