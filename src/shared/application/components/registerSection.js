import React, { Component } from "react";

import { RegistrationFormContainer as RegistrationForm } from "../containers/registrationFormContainer";
import { RegisterSocialFormContainer as RegisterSocialForm } from "../containers/socialRegistrationContainer";

class RegisterSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //TODO: Add form fields
      currentForm: "REGISTER_SOCIAL_BUTTONS"
    };

    this.showRegisterForm = this.showRegisterForm.bind(this);
    this.showSocialRegisterForm = this.showSocialRegisterForm.bind(this);
  }

  showRegisterForm() {
    this.setState({
      currentForm: "REGISTER_FORM"
    });
  }

  showSocialRegisterForm() {
    this.setState({
      currentForm: "REGISTER_SOCIAL_BUTTONS"
    });
  }

  renderCurrentForm() {
    const { currentForm } = this.state;

    switch (currentForm) {
      case "REGISTER_SOCIAL_BUTTONS":
        return (
          <RegisterSocialForm
            showRegisterForm={this.showRegisterForm}
            showLoginSection={this.props.showLoginSection}
          />
        );

      case "REGISTER_FORM":
        return (
          <RegistrationForm
            showSocialRegisterForm={this.showSocialRegisterForm}
            showLoginSection={this.props.showLoginSection}
            showRegisterSection={this.props.showRegisterSection}
          />
        );
    }
  }

  render() {
    return (
      <article className="bglogin pull-left paddingBoth">
        {this.renderCurrentForm()}
      </article>
    );
  }
}

export default RegisterSection;
