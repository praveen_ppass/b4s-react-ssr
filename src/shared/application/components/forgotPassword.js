import React, { Component } from "react";
import { breadCrumObj } from "../../../constants/breadCrum";
import BreadCrum from "../../components/bread-crum/breadCrum";
import { Helmet } from "react-helmet";
class Forgot extends Component {
  render() {
    return (
      <section className="forgot">
        <Helmet> </Helmet>
        <BreadCrum
          classes={breadCrumObj["forgotpassword"]["bgImage"]}
          listOfBreadCrum={breadCrumObj["forgotpassword"]["breadCrum"]}
          title={breadCrumObj["forgotpassword"]["title"]}
          subTitle={breadCrumObj["forgotpassword"]["subTitle"]}
        />

        <section className="container pos-relative equial-padding">
          <section className="row content-row">
            <article className="social-box">
              <article id="changePassword" className="row fwd">
                <form name="forgotPassForm" autocapitalize="off">
                  <section className="ctrl-wrapper widget-border">
                    <h2>Forgot Password</h2>
                    <article className="form-group">
                      <input
                        type="password"
                        placeholder="New Password"
                        required
                        className="form-control"
                      />
                    </article>

                    <article className="form-group">
                      <input
                        type="password"
                        className="form-control"
                        placeholder="Re-type Password"
                      />
                    </article>
                  </section>
                  <article className="btn-ctrl">
                    <button type="submit" className="btn btn-block greenBtn">
                      Submit
                    </button>
                  </article>
                </form>
              </article>
            </article>
          </section>
        </section>
      </section>
    );
  }
}
export default Forgot;
