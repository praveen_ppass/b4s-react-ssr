{
  /* <article>
  <article>
    <ul className="optional-links">
      <li className="active">
        <a href="javascript:void(0);">Login</a>
      </li>
      <li>
        <a href="javascript:void(0);">Registration</a>
      </li>
    </ul>

    <article className="bglogin pull-left paddingBoth">
      <h2>Please Log in/Register to start application</h2>
      <article className="applicationform">
        <section className="btn-wrapper">
          <article className="social-box ">
            <section className="ctrl-wrapper">
              <article className="form-group widthAuto">
                <article className="row mg-btm">
                  <a
                    href="javascript:void(0)"
                    className="btn btn-block facebook"
                  >
                    <i className="fa fa-facebook" aria-hidden="true" />Login
                    with Facebook
                  </a>
                  <a
                    href="javascript:void(0)"
                    className="btn btn-block googleplus"
                  >
                    <i className="fa fa-google-plus" aria-hidden="true" />Login
                    with Google
                  </a>
                  <a
                    href="javascript:void(0)"
                    ng-click="vmApplication.isLoginForm=true"
                    className="btn btn-block b4s"
                  >
                    <i className="fa fa-envelope" aria-hidden="true" />Login
                    with your email
                  </a>
                </article>
              </article>
            </section>
          </article>
          <dd>
            By clicking on any of the buttons, you agree to Buddy4Study's &nbsp;
            <a href="/terms-and-conditions" target="_blank">
              terms &amp; conditions
            </a>
          </dd>
          <article className="textPara">
            <p>
              <i>New here? Create a</i> &nbsp;
              <a href="" className="forgotpwd-link">
                Buddy4Study account
              </a>.
            </p>
            <a href="javascript:void(0)" className="forgotpwd-link">
              Forgot password
            </a>
          </article>
        </section>
      </article>

      <article className="row applicationform">
        <form name="forgotForm">
          <h2 className="epadding">Forgot Login Password?</h2>
          <section className="text-center push-left">
            <dd className="epadding">
              Enter your e-mail below and we will send you reset instructions!
            </dd>
            <section className="ctrl-wrapper">
              <article className="form-group">
                <i className="fa fa-lock iconpos hide" />
                <input
                  type="email"
                  name="email"
                  className="form-control"
                  placeholder="Enter your email"
                  id="uname "
                  autoComplete="off "
                  required
                />
                <span className="error animated bounce" name="email">
                  * Please enter valid email address.
                </span>
                <span className="error animated bounce error1 hide">
                  * Email is required.
                </span>
              </article>
            </section>
            <section className="ctrl-wrapper">
              <article className="form-group">
                <button type="submit" className="btn-block greenBtn margintop">
                  Send
                </button>
              </article>
            </section>
          </section>
        </form>
      </article>
    </article>
  </article>

  <article>
    <article className="bglogin topPadding pull-left">
      <article className="text-center">
        <strong className="fontsize23">Dear Raj Ror,</strong>
        <p className="margintop20">
          You have logged in successfully.
          <br />Now you can proceed to the application form.
        </p>
        <a className="btn margintop20">START APPLICATION</a>
      </article>
    </article>
  </article>
</article>;
 */
}
