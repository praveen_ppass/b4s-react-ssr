import React from "react";
import { Link } from "react-router-dom";
import gblFunc from "../../../../globals/globalFunctions";
import { contentOfNeedSchoalrship } from "../../formconfig";

const ApplicationStatusInfo = ({
  schCate,
  showDeadlineEnd,
  logUserOut,
  onSchCateHandler,
  schCategory,
  startApplication,
  schApply,
  slug,
  withCode
}) =>
  !withCode && (!showDeadlineEnd && typeof showDeadlineEnd == "boolean") ? (
    <article className="bglogin pull-left">
      <article className="text-center">
        <strong>{`Dear ${gblFunc.getFullName()}`},</strong>
        <p>
          Thank you for signing in.
          <br />
          <span className="red">This application deadline has expired.</span>
          <br />
          <br /> Find more scholarships to apply by completing your
          <Link to={`/myprofile`}>profile</Link> and
          <Link to={`/myscholarship`}> matched</Link> scholarships.
        </p>
      </article>
    </article>
  ) : withCode || (showDeadlineEnd && typeof showDeadlineEnd == "boolean") ? (
    <article className="bglogin pull-left paddingBoth">
      <article className="applicationform register">
        <article>
          <h2> {`Dear ${gblFunc.getFullName()}`},</h2>

          <p>
            You have logged in successfully.
            <br /> Now you can proceed to the application form
            <br />
            <br />
            {!schApply && typeof schApply == "boolean" ? (
              <article className="row">
                <article className="col-md-12">
                  <label htmlFor="cate_sch">
                    {contentOfNeedSchoalrship[slug.toUpperCase()]
                      ? contentOfNeedSchoalrship[slug.toUpperCase()]
                      : "I need scholarship for:"}
                  </label>
                </article>
                {!schCategory &&
                    !schApply &&
                    schCate &&
                    schCate.length === 1 &&
                    onSchCateHandler(schCate[0].id)}
                <article className="col-md-12">
                  {schCate && schCate.length > 0 && (
                    <select
                      id="cate_sch"
                      onChange={event => onSchCateHandler(event.target.value)}
                      className="form-control widthselect"
                    >
                      {schCate && schCate.length > 0 && (
                        <option key={schCate[0].id} value={schCate[0].id}>
                          {" "}
                          {schCate[0].name}
                        </option>
                      )}

                      {schCate && schCate.length > 0
                        ? schCate.map((cat, index) => {
                            if (index > 0) {
                              return (
                                <option key={cat.id} value={cat.id}>
                                  {cat.name}
                                </option>
                              );
                            }
                          })
                        : null}
                    </select>
                  )}
                  {!schCategory &&
                    !schApply &&
                    schCate &&
                    schCate.length === 1 &&
                    onSchCateHandler(schCate[0].id)}
                </article>
              </article>
            ) : null}
            {!schCategory &&
              !schApply &&
              (schCate && schCate.length == 1) &&
              onSchCateHandler(null, schCate[0].id)}
            <a
              disabled={
                !schCategory && !schApply && (schCate && schCate.length > 1)
                  ? true
                  : false
              }
              href="javascript:void(0)"
              className="btn btn-primary"
              onClick={() => {
                gblFunc.gaTrack.trackEvent([
                  "Scholarship",
                  "Application Start"
                ]);
                if (!withCode && !schCategory && !schApply) {
                  return;
                } else {
                  return startApplication(schCategory);
                }
              }}
            >
              START APPLICATION
            </a>
            {slug && slug.toUpperCase() === "CES7" ? (
              <p style={{ color: "red", marginTop: "10px" }}>
                RSSM should have joined on or prior to 30th April 2018 as per
                1SF data.
              </p>
            ) : null}
              {/* {slug && slug.toUpperCase() === "CES8" ? (
              <p style={{ color: "red", marginTop: "10px" }}>
                RSSM should have joined on or prior to 30th April 2018 as per
                1SF data.
              </p>
            ) : null} */}
            <br /> <br /> or
            <br /> <br />
            <a onClick={() => logUserOut()}>Log Out</a>
          </p>
        </article>
      </article>
    </article>
  ) : (
    ""
  );

export default ApplicationStatusInfo;
