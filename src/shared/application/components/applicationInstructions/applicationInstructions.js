import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import moment from "moment";
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import LoginRegisterSection from "../auth/loginRegisterSection";
import AlertMessagePopup from "../../../common/components/alertMsg";
import Needapphelp from "../../components/needHelpApp";
import gblFunc from "../../../../globals/globalFunctions";
import Loader from "../../../common/components/loader";

import { authorizeByCode } from "../../formconfig";
import {
  FETCH_USER_RULES_SUCCESS,
  FETCH_SCHOLARSHIP_APPLY_SUCCESS,
  FETCH_SCHOLARSHIP_APPLY_FAILURE
} from "../../../../constants/commonActions";
import {
  messages,
  dateProcess,
  getCurrentDate
} from "../../../../constants/constants";
import {
  REGISTER_USER_SUCCEEDED,
  LOGIN_USER_SUCCEEDED,
  LOG_USER_OUT_SUCCEEDED,
  FORGOT_USER_PASSWORD_SUCCESS
} from "../../../login/actions";
import {
  FETCH_SCHOLARSHIP_CATEGORY_SUCCESS,
  FETCH_APPLICATION_INSTRUCTION_SUCCESS,
  SAVE_SCHOLARSHIP_TYPE_SUCCESS,
  FETCH_APPLICATION_INSTRUCTION_FAILURE,
  SAVE_SCHOLARSHIP_TYPE_FAILURE
} from "../../actions/applicationInstructionActions";
import ConfirmMessagePopup from "../../../common/components/confirmMessagePopup";
var jwtDecode = require("jwt-decode");
class ApplicationInstructions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      needHelpName: "info@buddy4study.com",
      needHelpPhone: "+91-8929016461",
      tabs: ["Eligibility", "Important info"],
      isActive: true,
      load: true,
      showAlertMessagePopup: false,
      alertMessage: "",
      isDeadlineNotExceed: false,
      schCategory: null,
      schApply: false,
      isInstructionExist: 0,
      isSchApplyApi: true,
      renderState: "",
      withCode: false,
      myProfileRedirect: false,
      disbursal: false,
      schType: null,
      showConfirmationPopup: false,
      msg: ""
    };

    this.toggleTabs = this.toggleTabs.bind(this);
    this.rawMarkUp = this.rawMarkUp.bind(this);
    this.showAlertMessagePopup = this.showAlertMessagePopup.bind(this);
    this.hideAlertMessagePopup = this.hideAlertMessagePopup.bind(this);
    this.checkDeadlineExceedHandler = this.checkDeadlineExceedHandler.bind(
      this
    );
    this.logUserOut = this.logUserOut.bind(this);
    this.onSchCateHandler = this.onSchCateHandler.bind(this);
    this.startApplication = this.startApplication.bind(this);
    this.onConfirmationSuccess = this.onConfirmationSuccess.bind(this);
    this.hideConfirmationPopup = this.hideConfirmationPopup.bind(this);
  }

  componentDidMount() {
    if (
      this.props.match &&
      this.props.match.params &&
      this.props.match.params.slug &&
      ["SKKS1", "SSE4", "HUL1", "SCE3", "CKS1", "CES7", "CES8", "FAL9", "HCL2"].indexOf(
        this.props.match.params.slug.toUpperCase()
      ) > -1
    ) {
      this.props.logUserOut();
    }
    const { location } = this.props;

    if (
      location &&
      location.state &&
      location.state.from === "summary" &&
      location.state.isChildForm
    ) {
      this.props.logUserOut();
    }
    gblFunc.getAuthToken()
      ? this.props.loadScholarshipDetail(
        this.props.match.params.slug.toUpperCase()
      )
      : null;
  }

  showAlertMessagePopup(message) {
    this.setState({
      showAlertMessagePopup: true,
      alertMessage: message
    });
  }
  hideConfirmationPopup() {
    this.setState({
      showConfirmationPopup: false,
      msg: ""
    });
  }
  onConfirmationSuccess() {
    this.props.history.push({
      pathname: `/application/${this.props.match.params.slug.toUpperCase()}/form/summary`,
      state: { bsid: this.props.match.params.slug, editBtn: 'allow' }
    })
  }

  startApplication(schType) {
    const token = jwtDecode(localStorage.getItem('accessToken'))
    if (token && !token.authorities && this.props.schApply && this.props.schApply.step && this.props.schApply.step >= 2) {
      this.setState({
        showConfirmationPopup: true,
        msg: "You have already submitted the application form. You cannot edit the details. Kindly click continue button to check the summary details."
      });
      return;
    }
    const user_list = gblFunc.getStoreUserDetails();
    if (!this.state.disbursal) {
      return this.setState({ schType }, () => this.props.checkDisbursal())
    } else if (!this.state.schApply) {
      this.props.saveSchType({
        schType: schType,
        userId: user_list.userId,
        schID: this.props.schDtdata.scholarshipId
      });
    } else {
      this.props.history.push(
        `/application/${this.props.match.params.slug.toUpperCase()}/form/personalInfo`
      );
    }
  }

  logUserOut() {
    this.setState(
      {
        schApply: false,
        isSchApplyApi: true
      },
      () => this.props.logUserOut()
    );
  }

  onSchCateHandler(e, f = null) {
    if (!f && !e.target) {
      this.setState({
        schCategory: e
      });
    } else {
      if (f) {
        this.setState({
          schCategory: f
        });
      } else {
        this.setState({
          schCategory: e.target.value
        });
      }
    }
  }

  hideAlertMessagePopup() {
    this.setState({
      showAlertMessagePopup: false,
      alertMessage: ""
    });
  }

  checkDeadlineExceedHandler(date) {
    let isDateExceed = moment().isSameOrBefore(date, "day");

    return isDateExceed;
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    let user_list = gblFunc.getStoreUserDetails();
    switch (type) {
      case "CHECK_DISBURSAL_STATUS_SUCCESS":
        if (nextProps.disbursalStatus && nextProps.disbursalStatus.message) {
          this.setState({ alertMessage: nextProps.disbursalStatus.message, showAlertMessagePopup: true, myProfileRedirect: true })
        } else if (nextProps.disbursalStatus) {
          this.setState({ disbursal: true }, () => this.startApplication(this.state.schType))
        }
      case "VERIFY_TOKEN":
        if (
          nextProps.userLoginData &&
          nextProps.userLoginData.data &&
          nextProps.userLoginData.data.data &&
          nextProps.userLoginData.data.data.mobileVerified == "0"
        ) {
          this.props.history.push({
            pathname: "/otp",
            state: {
              userId: nextProps.userLoginData.data.data.userId,
              mobile: nextProps.userLoginData.data.data.mobile,
              countryCode: getNestedObjKey(nextProps, ["userLoginData", "data", "data", "countryCode"]),
              temp_Token: nextProps.userLoginData.data.data.token,
              location:
                this.props.location && this.props.location.pathname
                  ? this.props.location.pathname
                  : null
            }
          });
        }
        break;
      case LOGIN_USER_SUCCEEDED:
        let { userId } = nextProps.userLoginData;
        // Store auth token
        // Fix for auth token
        gblFunc.storeAuthToken(nextProps.userLoginData.access_token);
        this.props.fetchUserRules({
          userid: userId
        });

        break;
      case LOG_USER_OUT_SUCCEEDED:
        localStorage.removeItem("hulType");
        !nextProps.schDtdata
          ? this.setState({ isSchApplyApi: true }, () =>
            this.props.loadScholarshipDetail(
              this.props.match.params.slug.toUpperCase()
            )
          )
          : null;
        break;
      case FETCH_USER_RULES_SUCCESS:
        // Store login details here
        // gblFunc.storeUserDetails(nextProps.userRulesData);
        const cCode = getNestedObjKey(nextProps, ["userRulesData", "countryCode"])
        let requiredRule = 3
        let userInfo = {}
        if (cCode == "+977" || cCode == "+95") {
          requiredRule = 2
        }
        gblFunc.storeUserDetails(nextProps.userRulesData);
        gblFunc.storeAuthDetails(this.props.userLoginData);
        localStorage.setItem("profileLocked", nextProps.userRulesData && nextProps.userRulesData.profileLocked);
        const array = [];
        const userRule = getNestedObjKey(nextProps.userRulesData, ['userRules']);
        userRule.map(d => {
          if (d.ruleTypeId == 1 || d.ruleTypeId == 5 || d.ruleTypeId == 21) {
            array.push(d);
            userInfo[d.ruleTypeValue] = d.ruleId
          }
        });
        if (requiredRule > array.length) {
          this.setState({ isRedirectUserInfo: true, userInfo: userInfo }, () => {
            localStorage.setItem("userInfo", JSON.stringify(userInfo))
            localStorage.setItem("isAuth", 0);
            this.props.history.push({
              pathname: "/user-info",
              state: {
                userRulesData: nextProps.userRulesData,
                location: getNestedObjKey(this.props, ["location", "pathname"]),
                countryCode: getNestedObjKey(nextProps, ["userRulesData", "countryCode"])
              }
            })
          })
        } else {
          this.props.loadScholarshipDetail(
            this.props.match.params.slug.toUpperCase())
        }
        // if (!this.props.applicationStatus) {
        //   this.props.fetchApplicationStatus({ userId: user_list.userId });
        // }
        // this.showAlertMessagePopup(messages.login.success);

        break;
      case REGISTER_USER_SUCCEEDED:
        // this.showAlertMessagePopup(messages.register.success);
        break;
      case FETCH_APPLICATION_INSTRUCTION_SUCCESS:
        //nextProps.schDtdata.scholarshipId

        if (
          nextProps.schDtdata &&
          nextProps.schDtdata.contactNumber &&
          nextProps.schDtdata.contactEmail
        ) {
          this.setState({
            needHelpName: nextProps.schDtdata.contactEmail,
            needHelpPhone: nextProps.schDtdata.contactNumber
          });
        }
        if (gblFunc.isUserAuthenticated()) {
          this.props.scholarshipCategory(nextProps.schDtdata.scholarshipId);
        }
        if (nextProps.schDtdata.receiveApplication) {
          this.setState({
            renderState: "open"
          });
        } else if (
          !nextProps.schDtdata.receiveApplication &&
          this.props.location &&
          this.props.location.search &&
          this.props.location.search.code &&
          nextProps.schDtdata.code == this.props.location.search.code &&
          dateProcess(
            nextProps.schDtdata.expiryDate
              .split("-")
              .reverse()
              .join("-")
          ) > dateProcess(getCurrentDate())
        ) {
          this.setState({
            renderState: "openRaFalse"
          });
        } else {
          this.setState({
            renderState: "close"
          });
        }

        if (
          nextProps &&
          nextProps.schDtdata &&
          nextProps.schDtdata.code &&
          nextProps.schDtdata.expiryDate &&
          this.checkDeadlineExceedHandler(nextProps.schDtdata.expiryDate) &&
          this.props.location &&
          this.props.location.search &&
          this.props.location.search.includes("code=") &&
          (this.props.location.search === `?code=${nextProps.schDtdata.code}` ||
            this.props.location.search.includes(
              `code=${nextProps.schDtdata.code}&`
            ) ||
            this.props.location.search.includes(
              `&code=${nextProps.schDtdata.code}`
            ))
        ) {
          this.setState({
            isInstructionExist: 0,
            withCode: true
          });
        } else if (
          (!this.props.location.search ||
            !this.props.location.search.includes("code")) &&
          nextProps &&
          nextProps.schDtdata &&
          nextProps.schDtdata.deadline &&
          this.checkDeadlineExceedHandler(nextProps.schDtdata.deadline)
        ) {
          this.setState({
            isInstructionExist: 0
          });
        } else {
          this.setState({
            isInstructionExist: 2
          });
        }
        break;
      case SAVE_SCHOLARSHIP_TYPE_FAILURE:
        let faliureMessage = nextProps.schType
          ? nextProps.schType
          : "You are not applicable for this scholarship";
        if (this.state.needHelpName) {
          faliureMessage += ` Please reach out to ${this.state.needHelpName} for support.`;
        }
        this.showAlertMessagePopup(faliureMessage);
        break;
      case FETCH_SCHOLARSHIP_APPLY_FAILURE:
        this.setState({
          schApply: false
        });
        break;
      case FETCH_SCHOLARSHIP_APPLY_SUCCESS:
        if (
          nextProps.schApply &&
          nextProps.schApply.scholarshipTypeCategoryName
        ) {
          this.setState({
            schApply: true
          });
        }
        break;
      case FETCH_SCHOLARSHIP_CATEGORY_SUCCESS:
        if (this.state.isSchApplyApi) {
          this.setState({
            isSchApplyApi: false,
            schCategory: nextProps.scholarshipCate && nextProps.scholarshipCate.length > 0 && nextProps.scholarshipCate[0].id
          }, () =>
            this.props.getSchApply({
              scholarshipId: nextProps.schDtdata.scholarshipId,
              userId: user_list.userId
            })
          );
        }

        break;
      case FORGOT_USER_PASSWORD_SUCCESS:
        this.showAlertMessagePopup(messages.forgotPssword.success);
        break;
      case SAVE_SCHOLARSHIP_TYPE_SUCCESS:
        if (nextProps.schType && nextProps.schType.message) {
          return this.setState({
            alertMessage: nextProps.schType.message,
            showAlertMessagePopup: true,
            myProfileRedirect: true
          })
        }
        this.props.history.push(
          `/application/${this.props.match.params.slug.toUpperCase()}/form/personalInfo`
        );
        break;
      case FETCH_APPLICATION_INSTRUCTION_FAILURE:
        this.setState({
          isInstructionExist: 2
        });
        break;
      default:
        break;
    }

  }

  toggleTabs(tabName) {
    let isActiveTab = tabName == "Eligibility" ? true : false;
    this.setState(() => ({
      isActive: isActiveTab
    }));
  }

  rawMarkUp(data) {
    let rawMarkup = data;
    return { __html: rawMarkup };
  }

  hideAlertMessagePopup() {
    this.setState({
      showAlertMessagePopup: false,
      alertMessage: ""
    });
  }
  render() {
    const { isInstructionExist } = this.state;
    const { vleUser, applicationSchID } = gblFunc.getStoreUserDetails();
    let { match, location } = this.props;
    let dateExeedNot = null;
    let showInstructionContent = "";
    const isAuth = gblFunc.isUserAuthenticated();
    let scholarshipSlug = "",
      scholarshipName = "",
      scholarshipLogo = "",
      deadLine = "",
      scholarshipId = "",
      instruction = "",
      schCate = null,
      contactEmail = "",
      contactNumber = "",
      timeFormat = null,
      code = null,
      expiryDate = null;
    let receiveApplication = null;
    if (this.props.schDtdata) {
      scholarshipSlug = this.props.schDtdata.slug;
      scholarshipName = this.props.schDtdata.title;
      scholarshipLogo = this.props.schDtdata.logoUrl;
      instruction = this.props.schDtdata.instruction;
      scholarshipId = this.props.schDtdata.scholarshipId;
      contactEmail = this.props.schDtdata.contactEmail;
      contactNumber = this.props.schDtdata.contactNumber;
      receiveApplication = this.props.schDtdata.receiveApplication;
      (code = this.props.schDtdata.code),
        (expiryDate = this.props.schDtdata.expiryDate);
      // eligibility = this.props.schDtdata.scholarshipBodyMultilinguals[0]
      //   .eligibility;
      // importantInfo = this.props.schDtdata.scholarshipBodyMultilinguals[0]
      //   .moreDetails;
      scholarshipName = gblFunc.replaceWithLoreal(scholarshipName, true);
      deadLine = this.props.schDtdata.deadline;
      if (deadLine) {
        timeFormat = moment(deadLine, "YYYY-MM-DD").format("DD-MM-YYYY");
      }
      if (deadLine == null) {
        timeFormat = "Always Open";
      }
      if (isAuth) {
        gblFunc.storeApplicationScholarshipId(scholarshipId);
        /// gblFunc.storeNeedHelp([contactEmail, contactNumber]);
      }

      if (deadLine) {
        dateExeedNot = moment().isSameOrBefore(deadLine, "day");
      }

      if (deadLine == null) {
        dateExeedNot = true;
      }

      if (
        code &&
        expiryDate &&
        location &&
        location.search &&
        location.search.includes("code=")
      ) {
        let extendDeadlineCode = location.search.split("code=")[1];
        if (extendDeadlineCode.includes("&")) {
          extendDeadlineCode = extendDeadlineCode.split("&")[0];
        }
        dateExeedNot =
          code === extendDeadlineCode &&
            dateProcess(
              expiryDate
                .split("-")
                .reverse()
                .join("-")
            ) > dateProcess(getCurrentDate())
            ? true
            : false;

        sessionStorage.setItem(code, dateExeedNot);
      } else {
        sessionStorage.setItem(code, false);
      }
    }
    if (this.props.scholarshipCate) {
      schCate = this.props.scholarshipCate;
    }
    if (
      this.props.schDtdata &&
      (this.state.renderState === "open" || this.state.renderState === "")
    ) {
      showInstructionContent = (
        <article className="row">
          <article className="col-md-12">
            <a target="_blank" href={`/scholarship/${scholarshipSlug}`}>
              <img
                alt={scholarshipName}
                className="scholarshipLogo"
                src={scholarshipLogo}
              />
            </a>

            {this.props.schDtdata && this.props.schDtdata.status == 0 ? (
              <h3 className="text-center">{scholarshipName}</h3>
            ) : (
              <a target="_blank" href={`/scholarship/${scholarshipSlug}`}>
                <h3 className="text-center">{scholarshipName}</h3>
              </a>
            )}
            <p className="text-center equial-padding-b">
              <strong>Deadline: {timeFormat}</strong>
            </p>
            <article className="col-md-7">
              <ul className="optional-links">
                <li className="active active1">Instructions</li>
              </ul>
              <article>
                <article className="eligibility unorderList">
                  <article>
                    <div
                      dangerouslySetInnerHTML={this.rawMarkUp(instruction)}
                    />
                  </article>
                </article>
              </article>
            </article>

            {/* Start login form */}

            <article className="col-md-5">
              <LoginRegisterSection
                isAuth={isAuth}
                logUserOut={this.logUserOut}
                schCate={schCate}
                slug={this.props.match.params.slug.toUpperCase()}
                schCategory={this.state.schCategory}
                schApply={this.state.schApply}
                onSchCateHandler={this.onSchCateHandler}
                isDeadlineNotExceed={dateExeedNot}
                startApplication={this.startApplication}
                withCode={this.state.withCode}
              />
            </article>
          </article>
          
        </article>
      );
    }

    if (this.state.renderState !== "open") {
      this.setState({ renderState: "open" });
      showInstructionContent = (
        <article className="row">
          <article className="col-md-12">
            <a target="_blank" href={`/scholarship/${scholarshipSlug}`}>
              <img
                alt={scholarshipName}
                className="scholarshipLogo"
                src={scholarshipLogo}
              />
            </a>
            {match.params.slug === "SGA3" ? (
              <h3 className="text-center">{scholarshipName}</h3>
            ) : (
              <a target="_blank" href={`/scholarship/${scholarshipSlug}`}>
                <h3 className="text-center">{scholarshipName}</h3>
              </a>
            )}
            {/* <p className="text-center equial-padding-b">
          <strong>Deadline: {timeFormat}</strong>
        </p> */}
            <article className="col-md-12">
              <article>
                <article className="eligibility unorderList">
                  <article>
                    <p style={{ textAlign: "center" }}>
                      This scholarship is not available. Please click{" "}
                      <a href={`/myprofile/matched-scholarships`}>here</a> to explore more
                      scholarship matching with your profile.
                    </p>
                  </article>
                </article>
              </article>
            </article>
          </article>
        </article>
      );
    }
    if (isInstructionExist == 2 || this.state.renderState === "") {
      showInstructionContent = (
        <article className="row">
          <article className="col-md-12">
            <article>
              <article className="eligibility unorderList">
                <article>
                  <p style={{ textAlign: "center" }}>
                    This scholarship is not available. Please click{" "}
                    {gblFunc.isUserAuthenticated() ?
                      <a href={`/myprofile/matched-scholarships`}>here</a>
                      :
                      <a href={`/`}>here</a>
                    }
                     to explore more
                    scholarship matching with your profile.
                  </p>
                </article>
              </article>
            </article>
          </article>
        </article>
      );
    }

    return (
      <section>
        <ConfirmMessagePopup
          btnText={{ btn1: 'Continue', btn2: 'Cancel' }}
          message={this.state.msg}
          showPopup={this.state.showConfirmationPopup}
          onConfirmationSuccess={this.onConfirmationSuccess}
          onConfirmationFailure={this.hideConfirmationPopup}
        />

        {vleUser &&
          vleUser != "true" &&
          (applicationSchID == "null" ||
            applicationSchID == "undefined" ||
            applicationSchID == "") ? (
          <Redirect to={`vle/student-list`} />
        ) : (
          ""
        )}
        <AlertMessagePopup
          msg={this.state.alertMessage}
          isShow={this.state.showAlertMessagePopup}
          status
          close={this.hideAlertMessagePopup}
          myProfileRedirect={this.state.myProfileRedirect}
          redirectMyProfile={() => this.props.history.push('/myProfile/PersonalInfo')}
        />
        <section className="container application">
          <Loader isLoader={this.props.showLoader} />
          {showInstructionContent}
        </section>
      
        <section className="fixedfooter">          
          <span className="copyRight">
              All rights reserved © Smiling Star Advisory Pvt. Ltd.
            </span> 
        </section> 

        <Needapphelp
          needHelpName={this.state.needHelpName}
          needHelpPhone={this.state.needHelpPhone}
        />
      </section>
    );
  }
}

export default ApplicationInstructions;
