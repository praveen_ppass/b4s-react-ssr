import React from "react";
import { formsLocation } from "../formconfig";


import PersonalInfo from "../containers/genericFormContainers/PersonalInfoFormContainer";
import EducationInfo from "../containers/genericFormContainers/EducationInfoFormContainer";
import FamilyEarnings from "../containers/genericFormContainers/familyEarningsFormContainer";
import Reference from "../containers/referenceFormContainer";
import Questions from "../containers/genericFormContainers/QuestionFormContainer";
import AwardsWin from "../containers/applicationAwardsWinContainer";
import Summery from "../containers/genericFormContainers/SummaryFormContainer";
import Document from "../containers/genericFormContainers/DocumentsInfoFormContainer";
import BankDetails from "../containers/genericFormContainers/applicationBankDetailsContainer";
/**************** MIM Form************************************ */
import MIM2PersonalInfo from "../containers/customContainers/MIM4/MIM2PersonalInfoFormContainer";
import MIM4PersonalInfo from "../containers/customContainers/MIM4/MIM2PersonalInfoFormContainer";
/**************** FAC1 Form************************************ */
import FAC1PersonalInfo from "../containers/customContainers/FAC1/FAC1PersonalInfoFormContainer";
import FAC1EntranceExam from "../containers/customContainers/FAC1/FAC1EntranceExamFormContainer";
import FAC1EducationInfo from "../containers/customContainers/FAC1/FAC1EducationFormContainer";
import FAC1SummaryInfo from "../containers/customContainers/FAC1/FAC1SummaryContainer";
import FAC1document from "../containers/customContainers/FAC1/FAC1DocumentFormContainer";

/**************** FAC1 Form************************************ */

/**************** FAL9 Form *********************************** */
import FAL9PersonalInfo from "../containers/customContainers/FAL9/FAL9PersonalInfoFormContainer";
import FAL9QuestionInfo from "../containers/genericFormContainers/QuestionFormContainer";
import FAL9Documents from "../containers/genericFormContainers/DocumentsInfoFormContainer";
import FAL9EducationInfo from "../containers/genericFormContainers/EducationInfoFormContainer";
import FAL9SummaryInfo from "../containers/customContainers/FAL9/FAL9SummaryContainer";
import FAL9References from "../containers/customContainers/FAL9/FAL9ReferenceFormContainer";
/**************** FAL9 Form ************************************ */
/**************** HCL2 Form *********************************** */
import HCL2PersonalInfo from "../containers/customContainers/HCL2/HCL2PersonalInfoFormContainer";
import HCL2QuestionInfo from "../containers/genericFormContainers/QuestionFormContainer";
import HCL2Documents from "../containers/genericFormContainers/DocumentsInfoFormContainer";
import HCL2EducationInfo from "../containers/genericFormContainers/EducationInfoFormContainer";
import HCL2SummaryInfo from "../containers/genericFormContainers/SummaryFormContainer";
/**************** FAL9 Form ************************************ */
/**************** DSCI Form************************************ */
import DSCIPersonalInfo from "../containers/customContainers/DSCI/DSCIPersonalInfoFormContainer";
import DSCIEducationInfo from "../containers/customContainers/DSCI/DSCIEducationFormContainer";
import DSCISummaryInfo from "../containers/customContainers/DSCI/DSCISummaryFormContainer";

/**************** DSCI Form************************************ */

/**************** SGA3 Form************************************ */
import SGA3PersonalInfo from "../containers/customContainers/SGA3/SGA3PersonalInfoFormContainer";
import SGA3EducationInfo from "../containers/customContainers/SGA3/SGA3EducationInfoFormContainer";
import SGA3SummaryInfo from "../containers/customContainers/SGA3/SGA3SummaryFormContainer";
import SGA3Documents from "../containers/customContainers/SGA3/SGA3DocumentsInfoFormContainer";

/**************** SGA3 Form************************************ */
/**************** RHS1 Form *********************************** */
import RHS1PersonalInfo from "../containers/customContainers/RHS1/RHS1PersonalInfoFormContainer";
import RHS1QuestionInfo from "../containers/customContainers/RHS1/RHS1QuestionFormContainer";
import RHS1EducationInfo from "../containers/customContainers/RHS1/RHS1EducationFormContainer";
import RHS1SummaryInfo from "../containers/customContainers/RHS1/RHS1SummaryContainer";
import RHS1Document from "../containers/customContainers/RHS1/RHS1DocumentConatiner";
/**************** RHS1 Form ************************************ */

/**************** IS73 Form************************************ */
import IS73PersonalInfo from "../containers/customContainers/IS73/IS73PersonalInfoFormContainer";
import IS73EducationInfo from "../containers/customContainers/IS73/IS73EducationInfoFormContainer";
import IS73SummaryInfo from "../containers/customContainers/IS73/IS73SummaryFormContainer";
import IS73Documents from "../containers/customContainers/IS73/IS73DocumentsInfoFormContainer";

/**************** IS73 Form************************************ */

/**************** CIS10 Form************************************ */
import CIS10PersonalInfo from "../containers/customContainers/CIS10/CIS10PersonalInfoFormContainer";
import CIS10EducationInfo from "../containers/customContainers/CIS10/CIS10EducationInfoFormContainer";
import CIS10SummaryInfo from "../containers/customContainers/CIS10/CIS10SummaryFormContainer";
import CIS10Documents from "../containers/customContainers/CIS10/CIS10DocumentsInfoFormContainer";

/**************** CIS10 Form************************************ */
/**************** LFL2 Form************************************ */
import LFL2PersonalInfo from "../containers/customContainers/LFL2/LFL2PersonalInfoFormContainer";
import LFL2EducationInfo from "../containers/customContainers/LFL2/LFL2EducationInfoFormContainer";
import LFL2SummaryInfo from "../containers/customContainers/LFL2/LFL2SummaryFormContainer";
import LFL2Documents from "../containers/customContainers/LFL2/LFL2DocumentsInfoFormContainer";

/**************** LFL2 Form************************************ */

/**************** COS1 Form************************************ */
import COS1PersonalInfo from "../containers/customContainers/COS1/COS1PersonalInfoFormContainer";
import COS1EducationInfo from "../containers/customContainers/COS1/COS1EducationInfoFormContainer";
import COS1SummaryInfo from "../containers/customContainers/COS1/COS1SummaryFormContainer";
import COS1Documents from "../containers/customContainers/COS1/COS1DocumentsInfoFormContainer";

/**************** COS1 Form************************************ */

/**************** CHG2 Form************************************ */
import CHG2PersonalInfo from "../containers/customContainers/CHG2/CHG2PersonalInfoFormContainer";
import CHG2EducationInfo from "../containers/customContainers/CHG2/CHG2EducationInfoFormContainer";
import CHG2SummaryInfo from "../containers/customContainers/CHG2/CHG2SummaryFormContainer";
import CHG2Documents from "../containers/customContainers/CHG2/CHG2DocumentsInfoFormContainer";

/**************** CHG2 Form************************************ */

/**************** CH01 Form************************************ */
import CHO1PersonalInfo from "../containers/customContainers/CHO1/CHO1PersonalInfoFormContainer";
import CHO1SummaryInfo from "../containers/customContainers/CHO1/CHO1SummaryFormContainer";
import CHO1Documents from "../containers/customContainers/CHO1/CHO1DocumentsInfoFormContainer";

/**************** CHO1 Form************************************ */

/**************** SDS5 Form************************************ */
import SDS5PersonalInfo from "../containers/customContainers/SDS5/SDS5PersonalInfoFormContainer";
import SDS5EducationInfo from "../containers/customContainers/SDS5/SDS5EducationInfoFormContainer";
import SDS5SummaryInfo from "../containers/customContainers/SDS5/SDS5SummaryFormContainer";
import SDS5Documents from "../containers/customContainers/SDS5/SDS5DocumentsInfoFormContainer";

/**************** SDS5 Form************************************ */

/**************** SDS6 Form************************************ */
import SDS6PersonalInfo from "../containers/customContainers/SDS6/SDS6PersonalInfoFormContainer";
import SDS6EducationInfo from "../containers/customContainers/SDS6/SDS6EducationInfoFormContainer";
import SDS6SummaryInfo from "../containers/customContainers/SDS6/SDS6SummaryFormContainer";
import SDS6Documents from "../containers/customContainers/SDS6/SDS6DocumentsInfoFormContainer";

/**************** SDS6 Form************************************ */
/**************** CSP1 Form************************************ */
import CSP1PersonalInfo from "../containers/customContainers/CSP1/CSP1PersonalInfoFormContainer";
import CSP1EducationInfo from "../containers/customContainers/CSP1/CSP1EducationInfoFormContainer";
import CSP1SummaryInfo from "../containers/customContainers/CSP1/CSP1SummaryFormContainer";
import CSP1Documents from "../containers/customContainers/CSP1/CSP1DocumentsInfoFormContainer";
/**************** HUS2 Form************************************ */
import HUS2PersonalInfo from "../containers/customContainers/HUS2/HUS2PersonalInfoFormContainer";
import HUS2EducationInfo from "../containers/customContainers/HUS2/HUS2EducationFormContainer";
import HUS2SummaryInfo from "../containers/customContainers/HUS2/HUS2SummaryContainer";
import HUS2Documents from "../containers/customContainers/HUS2/HUS2DocumentConatiner";

/**************** HUS2 Form************************************ */

/**************** GGI6 Form************************************ */
import GGI6PersonalInfo from "../containers/customContainers/GGI6/GGI6PersonalInfoFormContainer";
import GGI6SummaryInfo from "../containers/customContainers/GGI6/GGI6SummaryFormContainer";

/**************** GGI6 Form************************************ */

/**************** CRS11 Form************************************ */
import CRS11PersonalInfo from "../containers/customContainers/CRS11/CRS11PersonalInfoFormContainer";
import CRS11EducationInfo from "../containers/customContainers/CRS11/CRS11EducationInfoFormContainer";
import CRS11SummaryInfo from "../containers/customContainers/CRS11/CRS11SummaryFormContainer";
import CRS11Questions from "../containers/customContainers/CRS11/CRS11QuestionFormContainer";
import CRS11Documents from "../containers/customContainers/CRS11/CRS11DocumentsInfoFormContainer";

/**************** CRS11 Form************************************ */
/**************** CRS9 Form************************************ */
import CRS9PersonalInfo from "../containers/customContainers/CRS9/CRS9PersonalInfoFormContainer";
import CRS9EducationInfo from "../containers/customContainers/CRS9/CRS9EducationInfoFormContainer";
import CRS9SummaryInfo from "../containers/customContainers/CRS9/CRS9SummaryFormContainer";
import CRS9Questions from "../containers/customContainers/CRS9/CRS9QuestionFormContainer";
import CRS9Documents from "../containers/customContainers/CRS9/CRS9DocumentsInfoFormContainer";

/**************** SDS6 Form************************************ */
/**************** SMS10 Form************************************ */
import SMS10PersonalInfo from "../containers/customContainers/SMS10/SMS10PersonalInfoFormContainer";
import SMS10EducationInfo from "../containers/customContainers/SMS10/SMS10EducationInfoFormContainer";
import SMS10SummaryInfo from "../containers/customContainers/SMS10/SMS10SummaryFormContainer";
import SMS10Documents from "../containers/customContainers/SMS10/SMS10DocumentsInfoFormContainer";

/**************** SMS10 Form************************************ */
/**************** SBA1 Form************************************ */
import SBA1PersonalInfo from "../containers/customContainers/SBA1/SBA1PersonalInfoFormContainer";
import SBA1EducationInfo from "../containers/customContainers/SBA1/SBA1EducationInfoFormContainer";
import SBA1SummaryInfo from "../containers/customContainers/SBA1/SBA1SummaryFormContainer";
import SBA1Documents from "../containers/customContainers/SBA1/SBA1DocumentsInfoFormContainer";

/**************** SBA1 Form************************************ */

/**************** CBI1 Form************************************ */
import CBI1PersonalInfo from "../containers/customContainers/CBI1/CBI1PersonalInfoFormContainer";
import CBI1EducationInfo from "../containers/customContainers/CBI1/CBI1EducationInfoFormContainer";
import CBI1SummaryInfo from "../containers/customContainers/CBI1/CBI6SummaryFormContainer";
import CBI1Documents from "../containers/customContainers/CBI1/CBI1DocumentsInfoFormContainer";

/**************** CBI1 Form************************************ */

/**************** CAL2 Form************************************ */
import CAL2PersonalInfo from "../containers/customContainers/CAL2/CAL2PersonalInfoFormContainer";
import CAL2EducationInfo from "../containers/customContainers/CAL2/CAL2EducationInfoFormContainer";
import CAL2SummaryInfo from "../containers/customContainers/CAL2/CAL2SummaryFormContainer";
import CAL2Documents from "../containers/customContainers/CAL2/CAL2DocumentsInfoFormContainer";
import CAL2Questions from "../containers/customContainers/CAL2/CAL2QuestionFormContainer";

/**************** CAL2 Form************************************ */

/**************** SCE3 Form************************************ */
import SCE3PersonalInfo from "../containers/customContainers/SCE3/SCE3PersonalInfoFormContainer";
import SCE3EducationInfo from "../containers/customContainers/SCE3/SCE3EducationInfoFormContainer";
import SCE3SummaryInfo from "../containers/customContainers/SCE3/SCE3SummaryFormContainer";
import SCE3Documents from "../containers/customContainers/SCE3/SCE3DocumentsInfoFormContainer";
import SCE3Questions from "../containers/customContainers/SCE3/SCE3QuestionFormContainer";

/**************** SCE3 Form************************************ */
/**************** BKS1 Form************************************ */
import BKS1PersonalInfo from "../containers/customContainers/BKS1/BKS1PersonalInfoFormContainer";
import BKS1EducationInfo from "../containers/customContainers/BKS1/BKS1EducationInfoFormContainer";
import BKS1SummaryInfo from "../containers/customContainers/BKS1/BKS1SummaryFormContainer";
import BKS1Documents from "../containers/customContainers/BKS1/BKS1DocumentsInfoFormContainer";

/**************** BKS1 Form************************************ */
/**************** CBI2 Form************************************ */
import CBI2PersonalInfo from "../containers/customContainers/CBI2/CBI2PersonalInfoFormContainer";
import CBI2EducationInfo from "../containers/customContainers/CBI2/CBI2EducationInfoFormContainer";
import CBI2SummaryInfo from "../containers/customContainers/CBI2/CBI2SummaryFormContainer";
import CBI2Questions from "../containers/customContainers/CBI2/CBI2QuestionFormContainer";
import CBI2Documents from "../containers/customContainers/CBI2/CBI2DocumentsInfoFormContainer";

/**************** CBI2 Form************************************ */
/**************** CBS3 Form************************************ */
import CBS3PersonalInfo from "../containers/customContainers/CBS3/CBS3PersonalInfoFormContainer";
import CBS3EducationInfo from "../containers/customContainers/CBS3/CBS3EducationInfoFormContainer";
import CBS3SummaryInfo from "../containers/customContainers/CBS3/CBS3SummaryFormContainer";

/**************** CBS3 Form************************************ */
/**************** TSL1 Form************************************ */
import TSL1PersonalInfo from "../containers/customContainers/TSL1/TSL1PersonalInfoFormContainer";
import TSL1EducationInfo from "../containers/customContainers/TSL1/TSL1EducationInfoFormContainer";
import TSL1SummaryInfo from "../containers/customContainers/TSL1/TSL1SummaryFormContainer";
import TSL1Questions from "../containers/customContainers/TSL1/TSL1QuestionFormContainer";
import TSL1Documents from "../containers/customContainers/TSL1/TSL1DocumentsInfoFormContainer";
/**************** HEC6 Form************************************ */
/**************** HEC6 Form************************************ */
import HEC6PersonalInfo from "../containers/customContainers/HEC6/HEC6PersonalInfoFormContainer";
import HEC6EducationInfo from "../containers/customContainers/HEC6/HEC6EducationInfoFormContainer";
import HEC6SummaryInfo from "../containers/customContainers/HEC6/HEC6SummaryFormContainer";
import HEC6Questions from "../containers/customContainers/HEC6/HEC6QuestionFormContainer";
import HEC6Documents from "../containers/customContainers/HEC6/HEC6DocumentsInfoFormContainer";
/**************** HEC6 Form************************************ */
/**************** CES7 Form************************************ */
import CES7PersonalInfo from "../containers/customContainers/CES7/CES7PersonalInfoFormContainer";
import CES7EducationInfo from "../containers/customContainers/CES7/CES7EducationInfoFormContainer";
import CES7SummaryInfo from "../containers/customContainers/CES7/CES7SummaryFormContainer";
import CES7Questions from "../containers/customContainers/CES7/CES7QuestionFormContainer";
import CES7Documents from "../containers/customContainers/CES7/CES7DocumentsInfoFormContainer";
/**************** CES7 Form************************************ */
/**************** CES8 Form************************************ */
import CES8PersonalInfo from "../containers/customContainers/CES8/CES8PersonalInfoFormContainer";
import CES8EducationInfo from "../containers/customContainers/CES8/CES8EducationInfoFormContainer";
import CES8SummaryInfo from "../containers/customContainers/CES8/CES8SummaryFormContainer";
import CES8Questions from "../containers/customContainers/CES8/CES8QuestionFormContainer";
import CES8Documents from "../containers/customContainers/CES8/CES8DocumentsInfoFormContainer";
/**************** CES8 Form************************************ */

/**************** CKS1 Form************************************ */
import CKS1PersonalInfo from "../containers/customContainers/CKS1/CKS1PersonalInfoFormContainer";
import CKS1EducationInfo from "../containers/customContainers/CKS1/CKS1EducationInfoFormContainer";
import CKS1SummaryInfo from "../containers/customContainers/CKS1/CKS1SummaryFormContainer";
import CKS1Questions from "../containers/customContainers/CKS1/CKS1QuestionFormContainer";
import CKS1Documents from "../containers/customContainers/CKS1/CKS1DocumentsInfoFormContainer";

/**************** CBI2 Form************************************ */

/**************** IGA1 Form************************************ */
import IGA1PersonalInfo from "../containers/customContainers/IGA1/IGA1PersonalInfoFormContainer";
import IGA1EducationInfo from "../containers/customContainers/IGA1/IGA1EducationInfoFormContainer";
import IGA1SummaryInfo from "../containers/customContainers/IGA1/IGA1SummaryFormContainer";
import IGA1Questions from "../containers/customContainers/IGA1/IGA1QuestionFormContainer";
import IGA1Documents from "../containers/customContainers/IGA1/IGA1DocumentsInfoFormContainer";

/**************** IGA1 Form************************************ */

/**************** HUL1 Form************************************ */
import HUL1PersonalInfo from "../containers/customContainers/HUL1/HUL1PersonalInfoFormContainer";
import HUL1EducationInfo from "../containers/customContainers/HUL1/HUL1EducationInfoFormContainer";
import HUL1SummaryInfo from "../containers/customContainers/HUL1/HUL1SummaryFormContainer";
import HUL1Questions from "../containers/customContainers/HUL1/HUL1QuestionFormContainer";
import HUL1Documents from "../containers/customContainers/HUL1/HUL1DocumentsInfoFormContainer";

/**************** HUL1 Form************************************ */

/**************** SSE4 Form************************************ */
import SSE4PersonalInfo from "../containers/customContainers/SSE4/SSE4PersonalInfoFormContainer";
import SSE4EducationInfo from "../containers/customContainers/SSE4/SSE4EducationInfoFormContainer";
import SSE4SummaryInfo from "../containers/customContainers/SSE4/SSE4SummaryFormContainer";
import SSE4Questions from "../containers/customContainers/SSE4/SSE4QuestionFormContainer";
import SSE4Documents from "../containers/customContainers/SSE4/SSE4DocumentsInfoFormContainer";

/**************** SSE4 Form************************************ */

/**************** GMM2 Form************************************ */
import GMM2PersonalInfo from "../containers/customContainers/GMM2/GMM2PersonalInfoFormContainer";
import GMM2EducationInfo from "../containers/customContainers/GMM2/GMM2EducationInfoFormContainer";
import GMM2SummaryInfo from "../containers/customContainers/GMM2/GMM2SummaryFormContainer";
import GMM2Documents from "../containers/customContainers/GMM2/GMM2DocumentsInfoFormContainer";

/**************** GMM2 Form************************************ */

/**************** ASF1 Form************************************ */
import ASF1PersonalInfo from "../containers/customContainers/ASF1/ASF1PersonalInfoFormContainer";
import ASF1EducationInfo from "../containers/customContainers/ASF1/ASF1EducationInfoFormContainer";
import ASF1SummaryInfo from "../containers/customContainers/ASF1/ASF1SummaryFormContainer";
import ASF1Questions from "../containers/customContainers/ASF1/ASF1QuestionFormContainer";
import ASF1Documents from "../containers/customContainers/ASF1/ASF1DocumentsInfoFormContainer";
/**************** CES7 Form************************************ */
/**************** DB1 Form************************************ */
import DB1PersonalInfo from "../containers/customContainers/DB1/DB1PersonalInfoFormContainer";
import DB1EducationInfo from "../containers/customContainers/DB1/DB1EducationInfoFormContainer";
import DB1SummaryInfo from "../containers/customContainers/DB1/DB1SummaryFormContainer";
import DB1Questions from "../containers/customContainers/DB1/DB1QuestionFormContainer";
import DB1Documents from "../containers/customContainers/DB1/DB1DocumentsInfoFormContainer";
/**************** DB1 Form************************************ */

/**************** LIF9 Form************************************ */
import LIF9PersonalInfo from "../containers/customContainers/LIF9/LIF9PersonalInfoFormContainer";
import LIF9EducationInfo from "../containers/customContainers/LIF9/LIF9EducationInfoFormContainer";
import LIF9SummaryInfo from "../containers/customContainers/LIF9/LIF9SummaryFormContainer";
import LIF9Questions from "../containers/customContainers/LIF9/LIF9QuestionFormContainer";
import LIF9Documents from "../containers/customContainers/LIF9/LIF9DocumentsInfoFormContainer";
import LIF9FamilyInfo from "../containers/customContainers/LIF9/LIF9FamilyEarningFormContainer";
/**************** LIF9 Form************************************ */

/**************** LIF2 Form************************************ */
import LIF2PersonalInfo from "../containers/customContainers/LIF2/LIF2PersonalInfoFormContainer";
import LIF2EducationInfo from "../containers/customContainers/LIF2/LIF2EducationInfoFormContainer";
import LIF2SummaryInfo from "../containers/customContainers/LIF2/LIF2SummaryFormContainer";
import LIF2Questions from "../containers/customContainers/LIF2/LIF2QuestionFormContainer";
import LIF2Documents from "../containers/customContainers/LIF2/LIF2DocumentsInfoFormContainer";
import LIF2FamilyInfo from "../containers/customContainers/LIF2/LIF2FamilyEarningFormContainer";
import LIF2Reference from "../containers/customContainers/LIF2/LIF2ReferenceFormContainer";
/**************** LIF2 Form************************************ */

/**************** CCS1 Form************************************ */
import CCS1PersonalInfo from "../containers/customContainers/CCS1/CCS1PersonalInfoFormContainer";
import CCS1EducationInfo from "../containers/customContainers/CCS1/CCS1EducationInfoFormContainer";
import CCS1Questions from "../containers/customContainers/CCS1/CCS1QuestionFormContainer";
import CCS1Documents from "../containers/customContainers/CCS1/CCS1DocumentsInfoFormContainer";
import CCS1FamilyInfo from "../containers/customContainers/CCS1/CCS1FamilyInfoContainer";
import CCS1SummaryInfo from "../containers/customContainers/CCS1/CCS1SummaryFormContainer";
/**************** CCS1 Form************************************ */

/**************** SWS3 Form************************************ */
import SWS3PersonalInfo from "../containers/customContainers/SWS3/SWS3PersonalInfoFormContainer";
import SWS3EducationInfo from "../containers/customContainers/SWS3/SWS3EducationInfoFormContainer";
import SWS3Questions from "../containers/customContainers/SWS3/SWS3QuestionFormContainer";
import SWS3Documents from "../containers/customContainers/SWS3/SWS3DocumentsInfoFormContainer";
import SWS3FamilyInfo from "../containers/customContainers/SWS3/SWS3FamilyInfoContainer";
import SWS3SummaryInfo from "../containers/customContainers/SWS3/SWS3SummaryFormContainer";
/**************** SWS3 Form************************************ */
/**************** SIHE1 Form************************************ */
import SIHE1PersonalInfo from "../containers/customContainers/SIHE1/SIHE1PersonalInfoFormContainer";
import SIHE1EducationInfo from "../containers/customContainers/SIHE1/SIHE1EducationInfoFormContainer";
import SIHE1Questions from "../containers/customContainers/SIHE1/SIHE1QuestionFormContainer";
import SIHE1Documents from "../containers/customContainers/SIHE1/SIHE1DocumentsInfoFormContainer";
import SIHE1FamilyInfo from "../containers/customContainers/SIHE1/SIHE1FamilyInfoContainer";
import SIHE1SummaryInfo from "../containers/customContainers/SIHE1/SIHE1SummaryFormContainer";
/**************** SIHE1 Form************************************ */
/**************** UBS9 Form************************************ */
import UBS9PersonalInfo from "../containers/customContainers/UBS9/UBS9PersonalInfoFormContainer";
import UBS9EducationInfo from "../containers/customContainers/UBS9/UBS9EducationInfoFormContainer";
import UBS9SummaryInfo from "../containers/customContainers/UBS9/UBS9SummaryFormContainer";
import UBS9Questions from "../containers/customContainers/UBS9/UBS9QuestionFormContainer";
import UBS9Documents from "../containers/customContainers/UBS9/UBS9DocumentsInfoFormContainer";
import UBS9familyInfo from "../containers/customContainers/UBS9/UBS9FamilyInfoFormContainer";

/**************** UBS9 Form************************************ */

/**************** CES3 Form************************************ */
import CES3PersonalInfo from "../containers/customContainers/CES3/CES3PersonalInfoFormContainer";
import CES3EducationInfo from "../containers/customContainers/CES3/CES3EducationInfoFormContainer";
import CES3SummaryInfo from "../containers/customContainers/CES3/CES3SummaryFormContainer";
import CES3Documents from "../containers/customContainers/CES3/CES3DocumentsInfoFormContainer";
/**************** CES3 Form************************************ */

/**************** LIF9 Form************************************ */
import IFBMS1PersonalInfo from "../containers/customContainers/IFBMS1/IFBMS1PersonalInfoFormContainer";
import IFBMS1EducationInfo from "../containers/customContainers/IFBMS1/IFBMS1EducationInfoFormContainer";
import IFBMS1SummaryInfo from "../containers/customContainers/IFBMS1/IFBMS1SummaryFormContainer";
import IFBMS1Questions from "../containers/customContainers/IFBMS1/IFBMS1QuestionFormContainer";
import IFBMS1Documents from "../containers/customContainers/IFBMS1/IFBMS1DocumentsInfoFormContainer";
/**************** LIF9 Form************************************ */
/**************** CVS1 Form************************************ */
import PMES01PersonalInfo from "../containers/customContainers/PMES01/PMES01PersonalInfoFormContainer";
import PMES01EducationInfo from "../containers/customContainers/PMES01/PMES01EducationInfoFormContainer";
import PMES01SummaryInfo from "../containers/customContainers/PMES01/PMES01SummaryFormContainer";
import PMES01Questions from "../containers/customContainers/PMES01/PMES01QuestionFormContainer";
/**************** CVS1 Form************************************ */

/**************** CVS1 Form************************************ */
import CVS1PersonalInfo from "../containers/customContainers/CVS1/CVS1PersonalInfoFormContainer";
import CVS1EducationInfo from "../containers/customContainers/CVS1/CVS1EducationInfoFormContainer";
import CVS1SummaryInfo from "../containers/customContainers/CVS1/CVS1SummaryFormContainer";
import CVS1Questions from "../containers/customContainers/CVS1/CVS1QuestionFormContainer";
import CVS1Documents from "../containers/customContainers/CVS1/CVS1DocumentsInfoFormContainer";
/**************** CVS1 Form************************************ */

/**************** TGSP1 Form************************************ */
import TGSP1PersonalInfo from "../containers/customContainers/TGSP1/TGSP1PersonalInfoFormContainer";
import TGSP1EducationInfo from "../containers/customContainers/TGSP1/TGSP1EducationInfoFormContainer";
import TGSP1SummaryInfo from "../containers/customContainers/TGSP1/TGSP1SummaryFormContainer";
import TGSP1Documents from "../containers/customContainers/TGSP1/TGSP1DocumentsInfoFormContainer";
/**************** TGSP1 Form************************************ */
/**************** MIM5 Form************************************ */
import MIM5PersonalInfo from "../containers/customContainers/MIM5/MIM5PersonalInfoFormContainer";
import MIM5EducationInfo from "../containers/customContainers/MIM5/MIM5EducationInfoFormContainer";
import MIM5SummaryInfo from "../containers/customContainers/MIM5/MIM5SummaryFormContainer";
import MIM5Documents from "../containers/customContainers/MIM5/MIM5DocumentsInfoFormContainer";
import MIM5Questions from "../containers/customContainers/MIM5/MIM5QuestionFormContainer";
/**************** MIM5 Form************************************ */
/**************** BML1 Form************************************ */
import BML1PersonalInfo from "../containers/customContainers/BML1/BML1PersonalInfoFormContainer";
import BML1EducationInfo from "../containers/customContainers/BML1/BML1EducationInfoFormContainer";
import BML1Questions from "../containers/customContainers/BML1/BML1QuestionFormContainer";
import BML1Documents from "../containers/customContainers/BML1/BML1DocumentsInfoFormContainer";
import BML1FamilyInfo from "../containers/customContainers/BML1/BML1FamilyInfoContainer";
import BML1SummaryInfo from "../containers/customContainers/BML1/BML1SummaryFormContainer";
/**************** BML1 Form************************************ */

/**************** BML3 Form************************************ */
import BML3PersonalInfo from "../containers/customContainers/BML3/BML3PersonalInfoFormContainer";
import BML3EducationInfo from "../containers/customContainers/BML3/BML3EducationInfoFormContainer";
import BML3Questions from "../containers/customContainers/BML3/BML3QuestionFormContainer";
import BML3Documents from "../containers/customContainers/BML3/BML3DocumentsInfoFormContainer";
import BML3FamilyInfo from "../containers/customContainers/BML3/BML3FamilyInfoContainer";
import BML3SummaryInfo from "../containers/customContainers/BML3/BML3SummaryFormContainer";
/**************** BML3 Form************************************ */
/**************** BCSOC1 Form************************************ */
import BCSOC1PersonalInfo from "../containers/customContainers/BCSOC1/BCSOC1PersonalInfoFormContainer";
import BCSOC1EducationInfo from "../containers/customContainers/BCSOC1/BCSOC1EducationInfoFormContainer";
import BCSOC1Questions from "../containers/customContainers/BCSOC1/BCSOC1QuestionFormContainer";
import BCSOC1Documents from "../containers/customContainers/BCSOC1/BCSOC1DocumentsInfoFormContainer";
import BCSOC1FamilyInfo from "../containers/customContainers/BCSOC1/BCSOC1FamilyInfoContainer";
import BCSOC1SummaryInfo from "../containers/customContainers/BCSOC1/BCSOC1SummaryFormContainer";
/**************** BCSOC1 Form************************************ */
/**************** BCS01 Form************************************ */
import BCS01PersonalInfo from "../containers/customContainers/BCS01/BCS01PersonalInfoFormContainer";
import BCS01EducationInfo from "../containers/customContainers/BCS01/BCS01EducationInfoFormContainer";
import BCS01Questions from "../containers/customContainers/BCS01/BCS01QuestionFormContainer";
import BCS01Documents from "../containers/customContainers/BCS01/BCS01DocumentsInfoFormContainer";
import BCS01FamilyInfo from "../containers/customContainers/BCS01/BCS01FamilyInfoContainer";
import BCS01SummaryInfo from "../containers/customContainers/BCS01/BCS01SummaryFormContainer";
/**************** BCS01 Form************************************ */
/**************** BCSMC1 Form************************************ */
import BCSMC1PersonalInfo from "../containers/customContainers/BCSMC1/BCSMC1PersonalInfoFormContainer";
import BCSMC1EducationInfo from "../containers/customContainers/BCSMC1/BCSMC1EducationInfoFormContainer";
import BCSMC1Questions from "../containers/customContainers/BCSMC1/BCSMC1QuestionFormContainer";
import BCSMC1Documents from "../containers/customContainers/BCSMC1/BCSMC1DocumentsInfoFormContainer";
import BCSMC1FamilyInfo from "../containers/customContainers/BCSMC1/BCSMC1FamilyInfoContainer";
import BCSMC1SummaryInfo from "../containers/customContainers/BCSMC1/BCSMC1SummaryFormContainer";
/**************** BCSMC1 Form************************************ */
/**************** MIC9 Form************************************ */
import MIC9PersonalInfo from "../containers/customContainers/MIC9/MIC9PersonalInfoFormContainer";
import MIC9EducationInfo from "../containers/customContainers/MIC9/MIC9EducationInfoFormContainer";
import MIC9SummaryInfo from "../containers/customContainers/MIC9/MIC9SummaryFormContainer";
import MIC9Questions from "../containers/customContainers/MIC9/MIC9QuestionFormContainer";
import MIC9Documents from "../containers/customContainers/MIC9/MIC9DocumentsInfoFormContainer";
/**************** MIC9 Form************************************ */
/**************** FAL10 Form************************************ */
import FAL10PersonalInfo from "../containers/customContainers/FAL10/FAL10PersonalInfoFormContainer";
import FAL10EducationInfo from "../containers/customContainers/FAL10/FAL10EducationInfoFormContainer";
import FAL10Questions from "../containers/customContainers/FAL10/FAL10QuestionFormContainer";
import FAL10Documents from "../containers/customContainers/FAL10/FAL10DocumentsInfoFormContainer";
import FAL10FamilyInfo from "../containers/customContainers/FAL10/FAL10FamilyInfoContainer";
import FAL10SummaryInfo from "../containers/customContainers/FAL10/FAL10SummaryFormContainer";
/**************** FAL10 Form************************************ */
/**************** SIMT1 Form************************************ */
import SIMT1PersonalInfo from "../containers/customContainers/SIMT1/SIMT1PersonalInfoFormContainer";
import SIMT1EducationInfo from "../containers/customContainers/SIMT1/SIMT1EducationInfoFormContainer";
import SIMT1Questions from "../containers/customContainers/SIMT1/SIMT1QuestionFormContainer";
import SIMT1Documents from "../containers/customContainers/SIMT1/SIMT1DocumentsInfoFormContainer";
import SIMT1FamilyInfo from "../containers/customContainers/SIMT1/SIMT1FamilyInfoContainer";
import SIMT1SummaryInfo from "../containers/customContainers/SIMT1/SIMT1SummaryFormContainer";
/**************** SIMT1 Form************************************ */
/**************** SIMSD1 Form************************************ */
import SIMSD1PersonalInfo from "../containers/customContainers/SIMSD1/SIMSD1PersonalInfoFormContainer";
import SIMSD1EducationInfo from "../containers/customContainers/SIMSD1/SIMSD1EducationInfoFormContainer";
import SIMSD1Questions from "../containers/customContainers/SIMSD1/SIMSD1QuestionFormContainer";
import SIMSD1Documents from "../containers/customContainers/SIMSD1/SIMSD1DocumentsInfoFormContainer";
import SIMSD1FamilyInfo from "../containers/customContainers/SIMSD1/SIMSD1FamilyInfoContainer";
import SIMSD1SummaryInfo from "../containers/customContainers/SIMSD1/SIMSD1SummaryFormContainer";
/**************** SIMSD1 Form************************************ */
/**************** SKKS1 Form************************************ */
import SKKS1PersonalInfo from "../containers/customContainers/SKKS1/SKKS1PersonalInfoFormContainer";
import SKKS1EducationInfo from "../containers/customContainers/SKKS1/SKKS1EducationInfoFormContainer";
import SKKS1Questions from "../containers/customContainers/SKKS1/SKKS1QuestionFormContainer";
import SKKS1Documents from "../containers/customContainers/SKKS1/SKKS1DocumentsInfoFormContainer";
import SKKS1FamilyInfo from "../containers/customContainers/SKKS1/SKKS1FamilyInfoContainer";
import SKKS1SummaryInfo from "../containers/customContainers/SKKS1/SKKS1SummaryFormContainer";
/**************** SKKS1 Form************************************ */
/**************** AFIS1 Form************************************ */
import AFIS1PersonalInfo from "../containers/customContainers/AFIS1/AFIS1PersonalInfoFormContainer";
import AFIS1EducationInfo from "../containers/customContainers/AFIS1/AFIS1EducationInfoFormContainer";
import AFIS1Questions from "../containers/customContainers/AFIS1/AFIS1QuestionFormContainer";
import AFIS1Documents from "../containers/customContainers/AFIS1/AFIS1DocumentsInfoFormContainer";
import AFIS1FamilyInfo from "../containers/customContainers/AFIS1/AFIS1FamilyInfoContainer";
import AFIS1SummaryInfo from "../containers/customContainers/AFIS1/AFIS1SummaryFormContainer";
/**************** AFIS1 Form************************************ */
/**************** AWFS2 Form************************************ */
import AWFS2PersonalInfo from "../containers/customContainers/AWFS2/AFIS1PersonalInfoFormContainer";
import AWFS2EducationInfo from "../containers/customContainers/AWFS2/AFIS1EducationInfoFormContainer";
import AWFS2Questions from "../containers/customContainers/AWFS2/AFIS1QuestionFormContainer";
import AWFS2Documents from "../containers/customContainers/AWFS2/AFIS1DocumentsInfoFormContainer";
import AWFS2FamilyInfo from "../containers/customContainers/AWFS2/AFIS1FamilyInfoContainer";
import AWFS2SummaryInfo from "../containers/customContainers/AWFS2/AFIS1SummaryFormContainer";
/**************** AFIS1 Form************************************ */
/**************** TILS1 Form************************************ */
import TILS1PersonalInfo from "../containers/customContainers/TILS1/TILS1PersonalInfoFormContainer";
import TILS1EducationInfo from "../containers/customContainers/TILS1/TILS1EducationInfoFormContainer";
import TILS1Questions from "../containers/customContainers/TILS1/TILS1QuestionFormContainer";
import TILS1Documents from "../containers/customContainers/TILS1/TILS1DocumentsInfoFormContainer";
import TILS1FamilyInfo from "../containers/customContainers/TILS1/TILS1FamilyInfoContainer";
import TILS1SummaryInfo from "../containers/customContainers/TILS1/TILS1SummaryFormContainer";
/**************** TILS1 Form************************************ */
/**************** TILE1 Form************************************ */
import TILE1PersonalInfo from "../containers/customContainers/TILE1/TILE1PersonalInfoFormContainer";
import TILE1EducationInfo from "../containers/customContainers/TILE1/TILE1EducationInfoFormContainer";
import TILE1Questions from "../containers/customContainers/TILE1/TILE1QuestionFormContainer";
import TILE1Documents from "../containers/customContainers/TILE1/TILE1DocumentsInfoFormContainer";
import TILE1FamilyInfo from "../containers/customContainers/TILE1/TILE1FamilyInfoContainer";
import TILE1SummaryInfo from "../containers/customContainers/TILE1/TILE1SummaryFormContainer";
/**************** TILE1 Form************************************ */
/****************  TISB1 Form************************************ */
import TISB1PersonalInfo from "../containers/customContainers/TISB1/TISB1PersonalInfoFormContainer";
import TISB1EducationInfo from "../containers/customContainers/TISB1/TISB1EducationInfoFormContainer";
import TISB1Questions from "../containers/customContainers/TISB1/TISB1QuestionFormContainer";
import TISB1Documents from "../containers/customContainers/TISB1/TISB1DocumentsInfoFormContainer";
import TISB1FamilyInfo from "../containers/customContainers/TISB1/TISB1FamilyInfoContainer";
import TISB1SummaryInfo from "../containers/customContainers/TISB1/TISB1SummaryFormContainer";
/****************  TISB1 Form************************************ */
var jwtDecode = require("jwt-decode");

const FormProvider = props => {
  const token = jwtDecode(localStorage.getItem('accessToken'))
  if (token && !token.authorities && props && props.schApply && props.schApply.step && props.schApply.step >= 2) {
    props.history.push({
      pathname: `/application/${props.match.params.bsid.toUpperCase()}/instruction`
    });
  }
  // props.scholarshipSteps.reduce()
  // const steps = [];
  // if(props.scholarshipSteps && props.scholarshipSteps.length) {
  //   steps.push(props.scholarshipSteps.key)
  // }
  const formType = formsLocation[props.BSID.toUpperCase()]
    ? formsLocation[props.BSID.toUpperCase()].indexOf(props.formName) > -1
      ? 1
      : 0
    : 0;
  const customFormName = props.BSID.toUpperCase() + props.formName;
  //console.log("=====customFormName=====", props.BSID, formType, props.formName, customFormName)
  switch (formType) {
    case 0:
      switch (props.formName) {
        case "personalInfo":
          return <PersonalInfo {...props} />;
        case "educationInfo":
          return <EducationInfo {...props} />;
        case "familyInfo":
          return <FamilyEarnings {...props} />;
        case "documents":
          return <Document {...props} />;
        case "references":
          return <Reference {...props} />;
        case "questions":
          return <Questions {...props} />;
        case "awardsWon":
          return <AwardsWin {...props} />;
        case "bankDetail":
          return <BankDetails {...props} />;
        // case "summary":
        //   return <Summery {...props} />;
      }
    case 1:
      switch (customFormName) {
        case "MIM2personalInfo":
          return <MIM2PersonalInfo {...props} />;
        case "MIM4personalInfo":
          return <MIM4PersonalInfo {...props} />;
        case "FAC1personalInfo":
          return <FAC1PersonalInfo {...props} />;
        case "FAC1entranceExam":
          return <FAC1EntranceExam {...props} />;
        case "FAC1educationInfo":
          return <FAC1EducationInfo {...props} />;
        case "FAC1summary":
          return <FAC1SummaryInfo {...props} />;
        case "FAC1document":
          return <FAC1document {...props} />;
        case "FAL9personalInfo":
          return <FAL9PersonalInfo {...props} />;
        case "FAL9educationInfo":
          return <FAL9EducationInfo {...props} />;
        case "FAL9documents":
          return <FAL9Documents {...props} />;
        case "FAL9summary":
          return <FAL9SummaryInfo {...props} />;
        case "FAL9questions":
          return <FAL9QuestionInfo {...props} />;
        case "FAL9references":
          return <FAL9References {...props} />;
        case "HCL2personalInfo":
          return <HCL2PersonalInfo {...props} />;
        case "HCL2educationInfo":
          return <HCL2EducationInfo {...props} />;
        case "HCL2documents":
          return <HCL2Documents {...props} />;
        case "HCL2summary":
          return <HCL2SummaryInfo {...props} />;
        case "HCL2questions":
          return <HCL2QuestionInfo {...props} />;
        case "CSS22personalInfo":
          return <DSCIPersonalInfo {...props} />;
        case "CSS22educationInfo":
          return <DSCIEducationInfo {...props} />;
        case "CSS22summary":
          return <DSCISummaryInfo {...props} />;
        case "SGA3personalInfo":
          return <SGA3PersonalInfo {...props} />;
        case "SGA3educationInfo":
          return <SGA3EducationInfo {...props} />;
        case "SGA3documents":
          return <SGA3Documents {...props} />;
        case "SGA3summary":
          return <SGA3SummaryInfo {...props} />;
        case "RHS1personalInfo":
          return <RHS1PersonalInfo {...props} />;
        case "RHS1educationInfo":
          return <RHS1EducationInfo {...props} />;
        case "RHS1summary":
          return <RHS1SummaryInfo {...props} />;
        case "RHS1questions":
          return <RHS1QuestionInfo {...props} />;
        case "RHS1documents":
          return <RHS1Document {...props} />;
        case "IS73personalInfo":
          return <IS73PersonalInfo {...props} />;
        case "IS73educationInfo":
          return <IS73EducationInfo {...props} />;
        case "IS73documents":
          return <IS73Documents {...props} />;
        case "IS73summary":
          return <IS73SummaryInfo {...props} />;
        case "CHO1personalInfo":
          return <CHO1PersonalInfo {...props} />;
        case "CHO1documents":
          return <CHO1Documents {...props} />;
        case "CHO1summary":
          return <CHO1SummaryInfo {...props} />;
        case "CIS10personalInfo":
          return <CIS10PersonalInfo {...props} />;
        case "CIS10educationInfo":
          return <CIS10EducationInfo {...props} />;
        case "CIS10documents":
          return <CIS10Documents {...props} />;
        case "CIS10summary":
          return <CIS10SummaryInfo {...props} />;
        case "LFL2personalInfo":
          return <LFL2PersonalInfo {...props} />;
        case "LFL2educationInfo":
          return <LFL2EducationInfo {...props} />;
        case "LFL2documents":
          return <LFL2Documents {...props} />;
        case "LFL2summary":
          return <LFL2SummaryInfo {...props} />;
        case "CHG2personalInfo":
          return <CHG2PersonalInfo {...props} />;
        case "CHG2educationInfo":
          return <CHG2EducationInfo {...props} />;
        case "CHG2documents":
          return <CHG2Documents {...props} />;
        case "CHG2summary":
          return <CHG2SummaryInfo {...props} />;
        case "COS1personalInfo":
          return <COS1PersonalInfo {...props} />;
        case "COS1educationInfo":
          return <COS1EducationInfo {...props} />;
        case "COS1documents":
          return <COS1Documents {...props} />;
        case "COS1summary":
          return <COS1SummaryInfo {...props} />;
        case "SDS5personalInfo":
          return <SDS5PersonalInfo {...props} />;
        case "SDS5educationInfo":
          return <SDS5EducationInfo {...props} />;
        case "SDS5documents":
          return <SDS5Documents {...props} />;
        case "SDS5summary":
          return <SDS5SummaryInfo {...props} />;
        case "SDS6personalInfo":
          return <SDS6PersonalInfo {...props} />;
        case "SDS6educationInfo":
          return <SDS6EducationInfo {...props} />;
        case "SDS6documents":
          return <SDS6Documents {...props} />;
        case "SDS6summary":
          return <SDS6SummaryInfo {...props} />;
        case "HUS2personalInfo":
          return <HUS2PersonalInfo {...props} />;
        case "HUS2educationInfo":
          return <HUS2EducationInfo {...props} />;
        case "HUS2documents":
          return <HUS2Documents {...props} />;
        case "HUS2summary":
          return <HUS2SummaryInfo {...props} />;
        case "GGI6personalInfo":
          return <GGI6PersonalInfo {...props} />;
        case "GGI6summary":
          return <GGI6SummaryInfo {...props} />;
        case "CRS11personalInfo":
          return <CRS11PersonalInfo {...props} />;
        case "CRS11educationInfo":
          return <CRS11EducationInfo {...props} />;
        case "CRS11documents":
          return <CRS11Documents {...props} />;
        case "CRS11questions":
          return <CRS11Questions {...props} />;
        case "CRS11summary":
          return <CRS11SummaryInfo {...props} />;
        case "CRS9personalInfo":
          return <CRS9PersonalInfo {...props} />;
        case "CRS9educationInfo":
          return <CRS9EducationInfo {...props} />;
        case "CRS9documents":
          return <CRS9Documents {...props} />;
        case "CRS9questions":
          return <CRS9Questions {...props} />;
        case "CRS9summary":
          return <CRS9SummaryInfo {...props} />;
        case "SMS10personalInfo":
          return <SMS10PersonalInfo {...props} />;
        case "SMS10educationInfo":
          return <SMS10EducationInfo {...props} />;
        case "SMS10documents":
          return <SMS10Documents {...props} />;
        case "SMS10summary":
          return <SMS10SummaryInfo {...props} />;
        case "SBA1personalInfo":
          return <SBA1PersonalInfo {...props} />;
        case "SBA1educationInfo":
          return <SBA1EducationInfo {...props} />;
        case "SBA1documents":
          return <SBA1Documents {...props} />;
        case "SBA1summary":
          return <SBA1SummaryInfo {...props} />;
        case "CBI1personalInfo":
          return <CBI1PersonalInfo {...props} />;
        case "CBI1educationInfo":
          return <CBI1EducationInfo {...props} />;
        case "CBI1documents":
          return <CBI1Documents {...props} />;
        case "CBI1summary":
          return <CBI1SummaryInfo {...props} />;
        case "CAL2personalInfo":
          return <CAL2PersonalInfo {...props} />;
        case "CAL2educationInfo":
          return <CAL2EducationInfo {...props} />;
        case "CAL2documents":
          return <CAL2Documents {...props} />;
        case "CAL2summary":
          return <CAL2SummaryInfo {...props} />;
        case "CAL2questions":
          return <CAL2Questions {...props} />;
        case "SCE3personalInfo":
          return <SCE3PersonalInfo {...props} />;
        case "SCE3educationInfo":
          return <SCE3EducationInfo {...props} />;
        case "SCE3documents":
          return <SCE3Documents {...props} />;
        case "SCE3summary":
          return <SCE3SummaryInfo {...props} />;
        case "SCE3questions":
          return <SCE3Questions {...props} />;
        case "BKS1personalInfo":
          return <BKS1PersonalInfo {...props} />;
        case "BKS1educationInfo":
          return <BKS1EducationInfo {...props} />;
        case "BKS1documents":
          return <BKS1Documents {...props} />;
        case "BKS1summary":
          return <BKS1SummaryInfo {...props} />;
        case "CBI2personalInfo":
          return <CBI2PersonalInfo {...props} />;
        case "CBI2educationInfo":
          return <CBI2EducationInfo {...props} />;
        case "CBI2documents":
          return <CBI2Documents {...props} />;
        case "CBI2questions":
          return <CBI2Questions {...props} />;
        case "CBI2summary":
          return <CBI2SummaryInfo {...props} />;
        case "CBS3personalInfo":
          return <CBS3PersonalInfo {...props} />;
        case "CBS3educationInfo":
          return <CBS3EducationInfo {...props} />;
        case "CBS3summary":
          return <CBS3SummaryInfo {...props} />;
        case "HEC6personalInfo":
          return <HEC6PersonalInfo {...props} />;
        case "HEC6educationInfo":
          return <HEC6EducationInfo {...props} />;
        case "HEC6documents":
          return <HEC6Documents {...props} />;
        case "HEC6questions":
          return <HEC6Questions {...props} />;
        case "HEC6summary":
          return <HEC6SummaryInfo {...props} />;
        case "TSL1personalInfo":
          return <TSL1PersonalInfo {...props} />;
        case "TSL1educationInfo":
          return <TSL1EducationInfo {...props} />;
        case "TSL1documents":
          return <TSL1Documents {...props} />;
        case "TSL1questions":
          return <TSL1Questions {...props} />;
        case "TSL1summary":
          return <TSL1SummaryInfo {...props} />;
        case "CES7personalInfo":
          return <CES7PersonalInfo {...props} />;
        case "CES7educationInfo":
          return <CES7EducationInfo {...props} />;
        case "CES7documents":
          return <CES7Documents {...props} />;
        case "CES7questions":
          return <CES7Questions {...props} />;
        case "CES7summary":
          return <CES7SummaryInfo {...props} />;
        case "CES8personalInfo":
          return <CES8PersonalInfo {...props} />;
        case "CES8educationInfo":
          return <CES8EducationInfo {...props} />;
        case "CES8documents":
          return <CES8Documents {...props} />;
        case "CES8questions":
          return <CES8Questions {...props} />;
        case "CES8summary":
          return <CES8SummaryInfo {...props} />;
        case "GMM2personalInfo":
          return <GMM2PersonalInfo {...props} />;
        case "GMM2educationInfo":
          return <GMM2EducationInfo {...props} />;
        case "GMM2documents":
          return <GMM2Documents {...props} />;
        case "GMM2summary":
          return <GMM2SummaryInfo {...props} />;
        case "CKS1personalInfo":
          return <CKS1PersonalInfo {...props} />;
        case "CKS1educationInfo":
          return <CKS1EducationInfo {...props} />;
        case "CKS1documents":
          return <CKS1Documents {...props} />;
        case "CKS1questions":
          return <CKS1Questions {...props} />;
        case "CKS1summary":
          return <CKS1SummaryInfo {...props} />;
        case "CSP1personalInfo":
          return <CSP1PersonalInfo {...props} />;
        case "CSP1educationInfo":
          return <CSP1EducationInfo {...props} />;
        case "CSP1documents":
          return <CSP1Documents {...props} />;
        case "CSP1summary":
          return <CSP1SummaryInfo {...props} />;
        case "IGA1personalInfo":
          return <IGA1PersonalInfo {...props} />;
        case "IGA1educationInfo":
          return <IGA1EducationInfo {...props} />;
        case "IGA1documents":
          return <IGA1Documents {...props} />;
        case "IGA1questions":
          return <IGA1Questions {...props} />;
        case "IGA1summary":
          return <IGA1SummaryInfo {...props} />;
        case "HUL1personalInfo":
          return <HUL1PersonalInfo {...props} />;
        case "HUL1educationInfo":
          return <HUL1EducationInfo {...props} />;
        case "HUL1documents":
          return <HUL1Documents {...props} />;
        case "HUL1questions":
          return <HUL1Questions {...props} />;
        case "HUL1summary":
          return <HUL1SummaryInfo {...props} />;
        case "SSE4personalInfo":
          return <SSE4PersonalInfo {...props} />;
        case "SSE4educationInfo":
          return <SSE4EducationInfo {...props} />;
        case "SSE4documents":
          return <SSE4Documents {...props} />;
        case "SSE4questions":
          return <SSE4Questions {...props} />;
        case "SSE4summary":
          return <SSE4SummaryInfo {...props} />;
        case "ASF1personalInfo":
          return <ASF1PersonalInfo {...props} />;
        case "ASF1educationInfo":
          return <ASF1EducationInfo {...props} />;
        case "ASF1documents":
          return <ASF1Documents {...props} />;
        case "ASF1questions":
          return <ASF1Questions {...props} />;
        case "ASF1summary":
          return <ASF1SummaryInfo {...props} />;
        case "DB1personalInfo":
          return <DB1PersonalInfo {...props} />;
        case "DB1educationInfo":
          return <DB1EducationInfo {...props} />;
        case "DB1documents":
          return <DB1Documents {...props} />;
        case "DB1questions":
          return <DB1Questions {...props} />;
        case "DB1summary":
          return <DB1SummaryInfo {...props} />;
        case "LIF9personalInfo":
          return <LIF9PersonalInfo {...props} />;
        case "LIF9educationInfo":
          return <LIF9EducationInfo {...props} />;
        case "LIF9documents":
          return <LIF9Documents {...props} />;
        case "LIF9questions":
          return <LIF9Questions {...props} />;
        case "LIF9summary":
          return <LIF9SummaryInfo {...props} />;
        case "LIF9familyInfo":
          return <LIF9FamilyInfo {...props} />;
        case "LIF2personalInfo":
          return <LIF2PersonalInfo {...props} />;
        case "LIF2educationInfo":
          return <LIF2EducationInfo {...props} />;
        case "LIF2documents":
          return <LIF2Documents {...props} />;
        case "LIF2questions":
          return <LIF2Questions {...props} />;
        case "LIF2summary":
          return <LIF2SummaryInfo {...props} />;
        case "LIF2familyInfo":
          return <LIF2FamilyInfo {...props} />;
        case "LIF2references":
          return <LIF2Reference {...props} />;
        case "CCS1personalInfo":
          return <CCS1PersonalInfo {...props} />;
        case "CCS1documents":
          return <CCS1Documents {...props} />;
        case "CCS1questions":
          return <CCS1Questions {...props} />;
        case "CCS1familyInfo":
          return <CCS1FamilyInfo {...props} />;
        case "CCS1educationInfo":
          return <CCS1EducationInfo {...props} />;
        case "CCS1summary":
          return <CCS1SummaryInfo {...props} />;
        case "SWS3personalInfo":
          return <SWS3PersonalInfo {...props} />;
        case "SWS3documents":
          return <SWS3Documents {...props} />;
        case "SWS3questions":
          return <SWS3Questions {...props} />;
        case "SWS3familyInfo":
          return <SWS3FamilyInfo {...props} />;
        case "SWS3educationInfo":
          return <SWS3EducationInfo {...props} />;
        case "SWS3summary":
          return <SWS3SummaryInfo {...props} />;
        case "SIHE1personalInfo":
          return <SIHE1PersonalInfo {...props} />;
        case "SIHE1documents":
          return <SIHE1Documents {...props} />;
        case "SIHE1questions":
          return <SIHE1Questions {...props} />;
        case "SIHE1familyInfo":
          return <SIHE1FamilyInfo {...props} />;
        case "SIHE1educationInfo":
          return <SIHE1EducationInfo {...props} />;
        case "SIHE1summary":
          return <SIHE1SummaryInfo {...props} />;
        case "UBS9personalInfo":
          return <UBS9PersonalInfo {...props} />;
        case "UBS9educationInfo":
          return <UBS9EducationInfo {...props} />;
        case "UBS9documents":
          return <UBS9Documents {...props} />;
        case "UBS9questions":
          return <UBS9Questions {...props} />;
        case "UBS9summary":
          return <UBS9SummaryInfo {...props} />;
        case "UBS9familyInfo":
          return <UBS9familyInfo {...props} />;
        case "IFBMS1personalInfo":
          return <IFBMS1PersonalInfo {...props} />;
        case "IFBMS1educationInfo":
          return <IFBMS1EducationInfo {...props} />;
        case "IFBMS1documents":
          return <IFBMS1Documents {...props} />;
        case "IFBMS1questions":
          return <IFBMS1Questions {...props} />;
        case "IFBMS1summary":
          return <IFBMS1SummaryInfo {...props} />;
        case "MIM5personalInfo":
          return <MIM5PersonalInfo {...props} />;
        case "MIM5educationInfo":
          return <MIM5EducationInfo {...props} />;
        case "MIM5documents":
          return <MIM5Documents {...props} />;
        case "MIM5summary":
          return <MIM5SummaryInfo {...props} />;
        case "MIM5questions":
          return <MIM5Questions {...props} />;
        case "TGSP1personalInfo":
          return <TGSP1PersonalInfo {...props} />;
        case "TGSP1educationInfo":
          return <TGSP1EducationInfo {...props} />;
        case "TGSP1documents":
          return <TGSP1Documents {...props} />;
        case "TGSP1summary":
          return <TGSP1SummaryInfo {...props} />;
        case "CVS1personalInfo":
          return <CVS1PersonalInfo {...props} />;
        case "CVS1educationInfo":
          return <CVS1EducationInfo {...props} />;
        case "CVS1documents":
          return <CVS1Documents {...props} />;
        case "CVS1questions":
          return <CVS1Questions {...props} />;
        case "CVS1summary":
          return <CVS1SummaryInfo {...props} />;
        case "PMES01personalInfo":
          return <PMES01PersonalInfo {...props} />;
        case "PMES01educationInfo":
          return <PMES01EducationInfo {...props} />;
        case "PMES01questions":
          return <PMES01Questions {...props} />;
        case "PMES01summary":
          return <PMES01SummaryInfo {...props} />;
        case "CES3personalInfo":
          return <CES3PersonalInfo {...props} />;
        case "CES3educationInfo":
          return <CES3EducationInfo {...props} />;
        case "CES3documents":
          return <CES3Documents {...props} />;
        case "CES3summary":
          return <CES3SummaryInfo {...props} />;

        case "BML1personalInfo":
          return <BML1PersonalInfo {...props} />;
        case "BML1documents":
          return <BML1Documents {...props} />;
        case "BML1questions":
          return <BML1Questions {...props} />;
        case "BML1familyInfo":
          return <BML1FamilyInfo {...props} />;
        case "BML1educationInfo":
          return <BML1EducationInfo {...props} />;
        case "BML1summary":
          return <BML1SummaryInfo {...props} />;

        case "BML3personalInfo":
          return <BML3PersonalInfo {...props} />;
        case "BML3documents":
          return <BML3Documents {...props} />;
        case "BML3questions":
          return <BML3Questions {...props} />;
        case "BML3familyInfo":
          return <BML3FamilyInfo {...props} />;
        case "BML3educationInfo":
          return <BML3EducationInfo {...props} />;
        case "BML3summary":
          return <BML3SummaryInfo {...props} />;
        case "BCS01personalInfo":
          return <BCS01PersonalInfo {...props} />;
        case "BCS01documents":
          return <BCS01Documents {...props} />;
        case "BCS01questions":
          return <BCS01Questions {...props} />;
        case "BCS01familyInfo":
          return <BCS01FamilyInfo {...props} />;
        case "BCS01educationInfo":
          return <BCS01EducationInfo {...props} />;
        case "BCS01summary":
          return <BCS01SummaryInfo {...props} />;
        case "BCSOC1personalInfo":
          return <BCSOC1PersonalInfo {...props} />;
        case "BCSOC1documents":
          return <BCSOC1Documents {...props} />;
        case "BCSOC1questions":
          return <BCSOC1Questions {...props} />;
        case "BCSOC1familyInfo":
          return <BCSOC1FamilyInfo {...props} />;
        case "BCSOC1educationInfo":
          return <BCSOC1EducationInfo {...props} />;
        case "BCSOC1summary":
          return <BCSOC1SummaryInfo {...props} />;
        case "BCSMC1personalInfo":
          return <BCSMC1PersonalInfo {...props} />;
        case "BCSMC1documents":
          return <BCSMC1Documents {...props} />;
        case "BCSMC1questions":
          return <BCSMC1Questions {...props} />;
        case "BCSMC1familyInfo":
          return <BCSMC1FamilyInfo {...props} />;
        case "BCSMC1educationInfo":
          return <BCSMC1EducationInfo {...props} />;
        case "BCSMC1summary":
          return <BCSMC1SummaryInfo {...props} />;

        case "MIC9personalInfo":
          return <MIC9PersonalInfo {...props} />;
        case "MIC9documents":
          return <MIC9Documents {...props} />;
        case "MIC9questions":
          return <MIC9Questions {...props} />;
        case "MIC9familyInfo":
          return <MIC9FamilyInfo {...props} />;
        case "MIC9educationInfo":
          return <MIC9EducationInfo {...props} />;
        case "MIC9summary":
          return <MIC9SummaryInfo {...props} />;

        case "FAL10personalInfo":
          return <FAL10PersonalInfo {...props} />;
        case "FAL10documents":
          return <FAL10Documents {...props} />;
        case "FAL10questions":
          return <FAL10Questions {...props} />;
        case "FAL10familyInfo":
          return <FAL10FamilyInfo {...props} />;
        case "FAL10educationInfo":
          return <FAL10EducationInfo {...props} />;
        case "FAL10summary":
          return <FAL10SummaryInfo {...props} />;
        case "SIMT1personalInfo":
          return <SIMT1PersonalInfo {...props} />;
        case "SIMT1documents":
          return <SIMT1Documents {...props} />;
        case "SIMT1questions":
          return <SIMT1Questions {...props} />;
        case "SIMT1familyInfo":
          return <SIMT1FamilyInfo {...props} />;
        case "SIMT1educationInfo":
          return <SIMT1EducationInfo {...props} />;
        case "SIMT1summary":
          return <SIMT1SummaryInfo {...props} />;
        case "SIMSD1personalInfo":
          return <SIMSD1PersonalInfo {...props} />;
        case "SIMSD1documents":
          return <SIMSD1Documents {...props} />;
        case "SIMSD1questions":
          return <SIMSD1Questions {...props} />;
        case "SIMSD1familyInfo":
          return <SIMSD1FamilyInfo {...props} />;
        case "SIMSD1educationInfo":
          return <SIMSD1EducationInfo {...props} />;
        case "SIMSD1summary":
          return <SIMSD1SummaryInfo {...props} />;
        case "SKKS1personalInfo":
          return <SKKS1PersonalInfo {...props} />;
        case "SKKS1documents":
          return <SKKS1Documents {...props} />;
        case "SKKS1questions":
          return <SKKS1Questions {...props} />;
        case "SKKS1familyInfo":
          return <SKKS1FamilyInfo {...props} />;
        case "SKKS1educationInfo":
          return <SKKS1EducationInfo {...props} />;
        case "SKKS1summary":
          return <SKKS1SummaryInfo {...props} />;
        case "AFIS1personalInfo":
          return <AFIS1PersonalInfo {...props} />;
        case "AFIS1documents":
          return <AFIS1Documents {...props} />;
        case "AFIS1questions":
          return <AFIS1Questions {...props} />;
        case "AFIS1familyInfo":
          return <AFIS1FamilyInfo {...props} />;
        case "AFIS1educationInfo":
          return <AFIS1EducationInfo {...props} />;
        case "AFIS1summary":
          return <AFIS1SummaryInfo {...props} />;
        case "AWFS2personalInfo":
          return <AWFS2PersonalInfo {...props} />;
        case "AWFS2documents":
          return <AWFS2Documents {...props} />;
        case "AWFS2questions":
          return <AWFS2Questions {...props} />;
        case "AWFS2familyInfo":
          return <AWFS2FamilyInfo {...props} />;
        case "AWFS2educationInfo":
          return <AWFS2EducationInfo {...props} />;
        case "AWFS2summary":
          return <AWFS2SummaryInfo {...props} />;
        case "TILS1personalInfo":
          return <TILS1PersonalInfo {...props} />;
        case "TILS1documents":
          return <TILS1Documents {...props} />;
        case "TILS1questions":
          return <TILS1Questions {...props} />;
        case "TILS1familyInfo":
          return <TILS1FamilyInfo {...props} />;
        case "TILS1educationInfo":
          return <TILS1EducationInfo {...props} />;
        case "TILS1summary":
          return <TILS1SummaryInfo {...props} />;
        case "TILE1personalInfo":
          return <TILE1PersonalInfo {...props} />;
        case "TILE1documents":
          return <TILE1Documents {...props} />;
        case "TILE1questions":
          return <TILE1Questions {...props} />;
        case "TILE1familyInfo":
          return <TILE1FamilyInfo {...props} />;
        case "TILE1educationInfo":
          return <TILE1EducationInfo {...props} />;
        case "TILE1summary":
          return <TILE1SummaryInfo {...props} />;
        case "TISB1personalInfo":
          return <TISB1PersonalInfo {...props} />;
        case "TISB1documents":
          return <TISB1Documents {...props} />;
        case "TISB1questions":
          return <TISB1Questions {...props} />;
        case "TISB1familyInfo":
          return <TISB1FamilyInfo {...props} />;
        case "TISB1educationInfo":
          return <TISB1EducationInfo {...props} />;
        case "TISB1summary":
          return <TISB1SummaryInfo {...props} />;
      }

    default:
  }
};

export default FormProvider;
