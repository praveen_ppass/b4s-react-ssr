import React, { Component } from "react";
import { Route, Link, Redirect } from "react-router-dom";
import FormProvider from "./formProvider";
import SubmitButton from "../containers/applicaitonSubmitContainer";
import queryString from "query-string";

import {
  formNames,
  stepNamesToDisplay,
  smsApplicationDetailConfig,
  needHelpConfig,
  authorizeByCode
} from "../formconfig";
import Loader from "../../common/components/loader";
import gblFunc from "../../../globals/globalFunctions";
import Needapphelp from "../components/needHelpApp";
import moment from "moment";
import {
  FETCH_APPLICATION_FORM_INSTRUCTION_SUCCESS,
  FETCH_SCHOLARSHIP_APPLICATION_STEP_SUCCESS
} from "../actions/applicationFormAction";
import {
  dateProcess,
  getCurrentDate,
  apiUrl
} from "../../../constants/constants";

class applicationForm extends Component {
  constructor(props) {
    super(props);
    this.applicationStepHandler = this.applicationStepHandler.bind(this);

    this.state = {
      _applicableSteps: null,
      needHelpName: "info@buddy4study.com",
      needHelpPhone: "+91-8929016461",
      genericEduTitle: "",
      isMin: false
    };
    this.handleStickyLoad = this.handleStickyLoad.bind(this);
  }
  componentDidMount() {
    // let { match } = this.props;
    // if (needHelpConfig[match.params.bsid]) {
    //   this.setState({
    //     needHelpName: needHelpConfig[match.params.bsid].name,
    //     needHelpPhone: needHelpConfig[match.params.bsid].mobile
    //   });
    // }
    let adminLite = false;
    const isAdmin = queryString.parse(location.search)["token"];
    if (isAdmin) {
      adminLite = true;
      //parse token....
      try {
        if (
          gblFunc.isUserAuthenticated() &&
          !smsApplicationDetailConfig.isSMSAdmin()
        ) {
          alert("Please logout first.");
          return;
        }
        sessionStorage.setItem("adminLite", adminLite); //TO differentiate lite call for admin and normal usetr
        const sid = queryString.parse(location.search)["sid"];
        sid && sessionStorage.setItem("sid", sid);
        this.props.history.push({
          pathname: `/auth/${btoa(isAdmin)}`,
          state: {
            redirectToApplicationDetail: this.props.location.pathname
          }
        });
      } catch (err) {
        console.log("Invalid Token");
      }
    } else {
      if (
        smsApplicationDetailConfig.isSMSAdmin() &&
        !sessionStorage.getItem("sid")
      ) {
        alert("Please logout first.");
        return;
      }
      sessionStorage.setItem("adminLite", adminLite); //TO differentiate lite call for admin and normal usetr
    }

    if (this.props.allRules && !this.props.allRules.rulesData) {
      this.props.getAllRules();
    }
    if (this.props.match.params.bsid) {
      this.props.loadScholarshipDetail(this.props.match.params.bsid);
    }
    typeof window !== "undefined"
      ? window.addEventListener("scroll", this.handleStickyLoad)
      : "";
  }

  applicationStepHandler(scholarshipId) {
    if (typeof window !== "undefined") {
      const userId = gblFunc.getStoreUserDetails()["userId"];

      this.props.getApplicationInstructionStep({
        userId,
        scholarshipId
      });

      this.props.getSchApply({
        scholarshipId,
        userId
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    const { type, instructions } = nextProps;
    switch (type) {
      case "FETCH_SCHOLARSHIP_APPLY_FAILURE":
        this.props.history.push(
          `/application/${this.props.match.params.bsid.toUpperCase()}/instruction`
        );
        break;
      case FETCH_APPLICATION_FORM_INSTRUCTION_SUCCESS:
        if (
          instructions &&
          instructions.contactNumber &&
          instructions.contactEmail
        ) {
          this.setState({
            needHelpName: instructions.contactEmail,
            needHelpPhone: instructions.contactNumber
          });
        }
        localStorage.setItem("applicationSchID", instructions.scholarshipId);
        this.props.fetchScholarshipApplicableStep({
          scholarshipId: instructions.scholarshipId
        });
        this.applicationStepHandler(instructions.scholarshipId);

        break;

      case FETCH_SCHOLARSHIP_APPLICATION_STEP_SUCCESS:
        const { scholarshipSteps } = nextProps;
        let checkSummary = true;
        //const checkSummary = scholarshipSteps.filter(step=>{return step})
        if (
          scholarshipSteps &&
          Array.isArray(scholarshipSteps) &&
          scholarshipSteps.length > 0
        ) {
          // scholarshipSteps.forEach(d => {
          //   if (d.step && d.step === "SUMMARY") {
          //     checkSummary = false;
          //     return;
          //   }
          // });
          // if (checkSummary) {
          //   scholarshipSteps.push({
          //     id: "",
          //     step: "SUMMARY",
          //     isOptional: false
          //   }); //add summary as default step if there is any step available
          // }
          const _applicableSteps = scholarshipSteps.map(list => {
            return { sch_steps: list, key: gblFunc.camcelCase(list.step) };
          });
          this.setState({
            _applicableSteps
          });
        }
        break;

      default:
        break;
    }
  }
  /* Sticky Handler  */
  handleStickyLoad() {
    if (typeof window !== "undefined") {
      /* Sticky Header */
      if (window.scrollY > 60) {
        this.setState({
          isMin: true
        });
      } else {
        this.setState({
          isMin: false
        });
      }
    }
  }

  render() {
    let applicableStepsFiltering = [];
    const { match } = this.props;
    const isUserAuthenticated =
      gblFunc.isUserAuthenticated() || this.props.isAuthenticated;
    const { vleUser, applicationSchID, sid } = gblFunc.getStoreUserDetails();
    if (
      !isUserAuthenticated &&
      !queryString.parse(this.props.location.search)["token"]
    ) {
      let url = ["HUS2"].includes(this.props.match.params.bsid.toUpperCase())
        ? `/application/${this.props.match.params.bsid.toUpperCase()}/instruction?code=XY03032019HUS2`
        : `/application/${this.props.match.params.bsid.toUpperCase()}/instruction`;
      return <Redirect to={url} />;
    }
    const activeFormArr = this.props.location.pathname.split(
      this.props.match.url + "/"
    );

    const { applicationStepInstruction } = this.props;
    const { _applicableSteps } = this.state;
    const rsType = gblFunc.getStoreHulAuthType("hulType");

    if (
      rsType &&
      rsType === "3P" &&
      _applicableSteps &&
      _applicableSteps.length
    ) {
      applicableStepsFiltering = _applicableSteps.filter(
        step => step.key !== "bankDetail"
      );
    } else {
      const smsApplicableSteps = smsApplicationDetailConfig.stepsToShow();
      applicableStepsFiltering =
        sid && _applicableSteps && smsApplicableSteps
          ? _applicableSteps.filter(step => {
            return smsApplicableSteps.indexOf(step.key) > -1;
          })
          : _applicableSteps;
    }
    const activeForm =
      activeFormArr[1] &&
        Object.values(formNames).indexOf(activeFormArr[1]) > -1
        ? activeFormArr[1]
        : applicableStepsFiltering && applicableStepsFiltering.length
          ? formNames[applicableStepsFiltering[0]["key"]]
          : formNames["personalInfo"];

    let isExpired = false;
    let scholarshipSlug = "",
      scholarshipName = "",
      scholarshipLogo = "",
      deadLine = "",
      scholarshipId = "",
      instruction = "",
      schCate = null,
      timeFormat = null;
    if (this.props.instructions) {
      scholarshipSlug = this.props.instructions.slug;
      scholarshipName = this.props.instructions.title;
      scholarshipLogo = this.props.instructions.logoUrl;
      instruction = this.props.instructions.instruction;
      scholarshipId = this.props.instructions.scholarshipId;
      // eligibility = this.props.instructions.scholarshipBodyMultilinguals[0]
      //   .eligibility;
      // importantInfo = this.props.instructions.scholarshipBodyMultilinguals[0]
      //   .moreDetails;
      scholarshipName = gblFunc.replaceWithLoreal(scholarshipName, true);
      deadLine = this.props.instructions.deadline;
      if (deadLine) {
        timeFormat = moment(deadLine, "YYYY-MM-DD").format("DD-MM-YYYY");
      }

      if (deadLine == null) {
        timeFormat = "Always Open";
      }
    }

    if (deadLine && typeof window !== undefined) {
      isExpired = moment().isSameOrBefore(deadLine, "day");
      if (
        (location &&
          location.search &&
          location.search.includes("code=") &&
          this.props.instructions.code &&
          this.props.instructions.expiryDate &&
          !this.props.instructions.receiveApplication &&
          location.search.includes("code=")[1] ===
          this.props.instructions.code &&
          dateProcess(
            this.props.instructions.expiryDate
              .split("-")
              .reverse()
              .join("-")
          ) < dateProcess(getCurrentDate())) ||
        (!isExpired &&
          this.props.instructions.code &&
          this.props.instructions.expiryDate &&
          sessionStorage.getItem(this.props.instructions.code) == "false")
      ) {
        return (
          <Redirect
            to={`/application/${this.props.match.params.bsid}/instruction`}
          />
        );
      }
    }
    let headerClass = "";
    if (this.state.isMin) {
      headerClass = this.props && this.props.location && (this.props.location.pathname.includes("HEC1/") || this.props.location.pathname.includes("HEC2/") || this.props.location.pathname.includes("HEC4/") || this.props.location.pathname.includes("HEC7/") || this.props.location.pathname.includes("HEC8/") || this.props.location.pathname.includes("HEC9/") || this.props.location.pathname.includes("HECS1/")) ? "fixFormHeader" : "";
    }
    return (
      <section>
        {vleUser &&
          vleUser != "true" &&
          (applicationSchID == "null" ||
            applicationSchID == "undefined" ||
            applicationSchID == "") ? (
            <Redirect to={`vle/student-list`} />
          ) : (
            ""
          )}
        <Loader isLoader={this.props.showLoader} />
        <section className="conatiner-fluid graybg">
          <section className="container application frmWrapper">
            <section className="row">
              <section className={`whiteColor ${headerClass}`}>
                <article className="col-md-3">
                  <Link to={`/scholarship/${scholarshipSlug}`} target="_blank">
                    <img
                      alt={scholarshipName}
                      className="scholarshipLogo1"
                      src={scholarshipLogo}
                    />
                  </Link>
                </article>
                <article className="col-md-9">
                  <h1>{scholarshipName}</h1>
                  <article className="date ">
                    <dd>
                      <span>
                        Deadline &nbsp;
                        <i className="fa fa-calendar" />
                        &nbsp; &nbsp;{timeFormat}
                      </span>
                    </dd>
                  </article>
                </article>
              </section>
              <section className="accordion col-md-12">
                {applicableStepsFiltering && applicableStepsFiltering.length > 0
                  ? applicableStepsFiltering.map(step => (
                    <article
                      key={step.key}
                      className={
                        activeForm == formNames[step.key]
                          ? "heightauto"
                          : "height0"
                      }
                    >
                      <Link to={`${this.props.match.url}/${step.key}`}>
                        <article className="heading topborder">
                          <i
                            className={
                              activeForm == formNames[step.key]
                                ? "fa fa-minus"
                                : "fa fa-plus"
                            }
                          >
                            &nbsp;
                            </i>

                          {stepNamesToDisplay[step["sch_steps"]["step"]]}

                          {applicationStepInstruction &&
                            applicationStepInstruction.indexOf(
                              step["sch_steps"].step
                            ) > -1 ? (
                              <span className="pull-right">
                                {step["sch_steps"].id != "" ? (
                                  <span>
                                    Completed &nbsp;
                                    <i className="fa fa-check-circle-o complete">
                                      &nbsp;
                                    </i>
                                  </span>
                                ) : (
                                    ""
                                  )}
                              </span>
                            ) : (
                              <span className="pull-right">
                                {step["sch_steps"].id != "" ? (
                                  <span>
                                    Pending&nbsp;&nbsp;
                                    <i className="fa fa-pencil-square-o pending">
                                      &nbsp;
                                    </i>
                                  </span>
                                ) : (
                                    ""
                                  )}
                              </span>
                            )}
                        </article>
                      </Link>
                      {activeForm == formNames[step.key] ? (
                        <FormProvider
                          genericEduTitle={this.state.genericEduTitle}
                          formName={`${step.key}`}
                          BSID={this.props.match.params.bsid.toUpperCase()}
                          {...this.props}
                        />
                      ) : null}
                    </article>
                  ))
                  : null}
                <SubmitButton {...this.props} />
              </section>
            </section>
          </section>
        </section>
        <Needapphelp
          needHelpName={this.state.needHelpName}
          needHelpPhone={this.state.needHelpPhone}
        />
      </section>
    );
  }
}

export default applicationForm;
