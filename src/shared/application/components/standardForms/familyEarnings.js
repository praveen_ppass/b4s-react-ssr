import React, { Component } from "react";
import gblFunc from "../../../../globals/globalFunctions";
import { mapValidationFunc } from "../../../../validation/rules";
import { ruleRunner } from "../../../../validation/ruleRunner";
import AlertMessage from "../../../common/components/alertMsg";
import Loader from "../../../common/components/loader";
import { camelCase, snakeCase } from "../../../../constants/constants";
import ConfirmMessagePopup from "../../../common/components/confirmMessagePopup";
import { Route, Link, Redirect } from "react-router-dom";
import { extractStepTemplate, childEduBSID } from "../../formconfig";
import moment from "moment";
import DatePicker from "react-datepicker";
import FamilyVideoPopup from "./FamilyVideoPopup";

class FamilyEarnings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenVideo: false,
      isFormButtonShow: true,
      configFieldsData: {},
      showLoader: false,
      msg: "",
      status: "",
      relation: null,
      class: null,
      familyEarningGetList: null,
      occupation: null,
      totalIncome: null,
      activeTab: null,
      formData: {
        addressLine: null,
        mobile: null,
        alternateMobile: null,
        type: null,
        qualification: null,
        panNumber: null,
        employer: null,
        absoluteIncome: null,
        email: null,
        name: null,
        occupation: null,
        otherOccupation: null,
        relation: null,
        customData: {
          employeeId: null
        },
        id: null
      },
      validations: {},
      showConfirmationPopup: false,
      deleteSuccessCallBack: null,
      isNext: false,
      isNextVisible: false,
      isOtherOccupation: false,
      isCompleted: false,
      isRemoved: false,
      minEntry: 0 // MIN ENTRY SHOULD BE LENGHT - 1 LIKE MAX 1 THEN PASS 0
    };
    this.showForm = this.showForm.bind(this);
    this.formChangeHandler = this.formChangeHandler.bind(this);
    this.submit = this.submit.bind(this);
    this.formClose = this.formClose.bind(this);
    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.hideConfirmationPopup = this.hideConfirmationPopup.bind(this);
    this.goNext = this.goNext.bind(this);
    this.brandVideo = this.brandVideo.bind(this);
    this.brandVideoClose = this.brandVideoClose.bind(this);
  }

  showForm() {
    //validate the list
    /*    if (this.state.relation != null && this.state.relation.length > 0) {
      let lenOfRel = this.state.relation.length;
      if (lenOfRel == this.state.familyEarningGetList.length) {
        return false;
      }
    } */
    const { match } = this.props;
    let employeeId = null;
    let age = null;
    if (
      match &&
      match.params &&
      match.params.bsid &&
      childEduBSID[match.params.bsid.toUpperCase()]
    ) {
      const email = gblFunc.getStoreUserDetails()["email"];

      if (email && email.includes("skks1")) {
        employeeId = email.split("_")[1].split("@")[0];
      } else {
        if (!!email) {
          employeeId = email.split("_")[2].split("@")[0];

        }
      }
    }
    this.state.isFormButtonShow = !this.state.isFormButtonShow;
    this.setState({
      isFormButtonShow: this.state.isFormButtonShow,
      formData: {
        addressLine: null,
        mobile: null,
        alternateMobile: null,
        type: null,
        qualification: null,
        panNumber: null,
        employer: null,
        absoluteIncome: null,
        email: null,
        name: null,
        occupation: null,
        otherOccupation: null,
        relation: null,
        customData: {
          employeeId
        },
        id: null
      }
    });
  }

  submit() {
    let userId = "";
    let scholarshipId = "";
    let isPosted = false;
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }

    if (this.state.isCompleted) {
      //CALL STEP POST FOR STEP HAS BEEN COMPLETED
      isPosted = true;
    }

    let isSubmit = this.checkFormValidations();
    if (isSubmit) {
      let updateFamilyForm = { ...this.state.formData };
      // for (let key in this.state.configFieldsData) {
      //   if (this.state.configFieldsData[key].custom) {
      //     updateFamilyForm.customData[key] = updateFamilyForm[key];
      //   }
      // }

      this.setState({ formData: updateFamilyForm, isFormButtonShow: true });
      this.props.saveFamilyEarningData({
        userId: userId,
        scholarshipId: scholarshipId,
        data: updateFamilyForm,
        isPosted: isPosted
      });
    }
  }

  componentDidMount() { }

  editfamilyEarningVal(items) {
    let totalIncome = this.state.familyEarningGetList.map(item => Number(item.absoluteIncome)).reduce((prev, next) => prev + next)
    let returnSnake = snakeCase(items);
    const editData = returnSnake;
    let isOtherOccupation = false;
    let validations = JSON.parse(JSON.stringify(this.state.validations));
    if (editData.occupation == 12) {
      validations["otherOccupation"] = null;
      isOtherOccupation = true;
    } else {
      delete validations["otherOccupation"];
    }
    console.log(editData.relation)
    this.setState({
      isFormButtonShow: this.state.activeTab == editData.relation ? true : false,
      formData: editData,
      isOtherOccupation,
      validations,
      totalIncome,
      activeTab: this.state.activeTab == editData.relation ? null : editData.relation
    });
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    let validationRules = [];
    if (this.state.validations.hasOwnProperty(fieldID)) {
      let configFields = this.state.configFieldsData[fieldID];
      if (configFields != undefined && configFields.validations != null) {
        configFields.validations.map(res => {
          if (res != null) {
            let validator = mapValidationFunc(res);
            if (validator != undefined) validationRules.push(validator);
          }
        });
        validationObject.name = "*Field";
        validationObject.validationFunctions = validationRules;
      }
    }
    return validationObject;
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      const value = ["joiningDate", "storeDetails", "employeeId"].includes(key)
        ? this.state.formData.customData[key]
        : this.state.formData[key];
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      if (name != undefined && validationFunctions != undefined) {
        let validationResult = ruleRunner(
          value,
          key,
          name,
          ...validationFunctions
        );
        validations[key] = validationResult[key];

        if (validationResult[key] !== null) {
          isFormValid = false;
        }
      }
    }
    console.log(validations)
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  removefamilyEarning(famId, relation) {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }

    let isDeleted = false;
    if (this.state.isRemoved) {
      isDeleted = true;
    }

    const deleteSuccessCallBack = () =>
      this.props.deleteFamilyEarningsData({
        userId: userId,
        scholarshipId: scholarshipId,
        familyId: famId,
        isDeleted: isDeleted,
        relationId: relation
      });

    this.setState({
      showConfirmationPopup: true,
      deleteSuccessCallBack,
      isFormButtonShow: true
    });
  }

  showConfirmationPopup() {
    this.setState({
      showConfirmationPopup: true
    });
  }

  hideConfirmationPopup() {
    this.setState({
      showConfirmationPopup: false,
      deleteSuccessCallBack: null
    });
  }

  formChangeHandler(event, dateId = undefined) {
    if (!event) return;
    let value;
    let id, isOtherOccupation;

    if (dateId) {
      value = moment(event).format("YYYY-MM-DD");
      id = dateId;
    } else {
      value = event.target.value;
      id = event.target.id;
    }
    let validations = { ...this.state.validations };

    if (id == "occupation") {
      //VALIDATION FOR OTHER FIELDS
      if (value == 12) {
        validations["otherOccupation"] = null;
        isOtherOccupation = true;
      } else {
        delete validations["otherOccupation"];
        isOtherOccupation = false;
      }
    }

    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      if (name != undefined && validationFunctions != undefined) {
        const validationResult = ruleRunner(
          value,
          id,
          name,
          ...validationFunctions
        );
        validations[id] = validationResult[id];
      }
    }

    //validate Other occupation data

    const updateFormData = JSON.parse(JSON.stringify(this.state.formData));
    if (["joiningDate", "employeeId", "storeDetails"].includes(id)) {
      updateFormData.customData[id] = value;
    } else {
      updateFormData[id] = value;
    }
    this.setState({
      formData: updateFormData,
      validations,
      isOtherOccupation
    });
  }

  getfamilyEarningsData() {
    let scholarshipId = "";
    if (window != undefined) {
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    this.props.getFamilyEarningData({
      scholarshipId: scholarshipId,
      step: "FAMILY_INFO"
    });
  }

  componentWillMount() {
    this.getfamilyEarningsData();
  }

  updateArrayStepCompletedCall() {
    let scholarshipId = "";
    if (window != undefined) {
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    this.props.applicationInstructionStep({
      scholarshipId: scholarshipId
    });
  }

  updateArrayStepForDelete() {
    let scholarshipId = "";
    if (window != undefined) {
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    this.props.applicationInstructionStep({
      scholarshipId: scholarshipId
    });
  }

  componentWillReceiveProps(nextProps) {
    const { applicationEducation, familyEarningsDataConfig } = nextProps;
    const { type } = familyEarningsDataConfig;
    switch (type) {
      case "FETCH_APPLICATION_FORMS_FAMILYEARNING_SUCCESS":
        if (
          nextProps.familyEarningsDataConfig["applicationFamilyEarningData"] !=
          null
        ) {
          const {
            familyEarningDataConfig,
            occupationList,
            familyEarningGetListByUser
          } = nextProps.familyEarningsDataConfig[
            "applicationFamilyEarningData"
            ];
          const configFieldsData = { ...this.state.configFieldsData };
          let validationInit = {};
          for (let key in familyEarningDataConfig) {
            if (
              familyEarningDataConfig[key] != null &&
              familyEarningDataConfig[key].active
            ) {
              let clearUnderScore = gblFunc.replace_underScore(key);
              validationInit[clearUnderScore] = null;
              configFieldsData[clearUnderScore] = familyEarningDataConfig[key];
            }
          }

          const earning_member = nextProps.allRules["rulesData"]
            ? nextProps.allRules["rulesData"].earning_member
            : [];
          const qualificationList = nextProps.allRules["rulesData"]
            ? nextProps.allRules["rulesData"].class
            : [];
          this.setState({
            configFieldsData,
            relation: earning_member,
            occupation: occupationList,
            familyEarningGetList: familyEarningGetListByUser.familyDetails,
            validations: validationInit,
            qualification: qualificationList
          });
          // if(this.state.familyEarningGetList && this.state.familyEarningGetList.length>0){
          //   let totalIncome= this.state.familyEarningGetList.map(item => Number(item.absoluteIncome)).reduce((prev, next) => prev + next)
          //   this.setState({
          //     totalIncome:totalIncome
          //   })
          // }

          //CHECK CONDITION FOR IF COMPLETED and REMOVEDD
          if (
            // FOR COMPLETED
            familyEarningGetListByUser && familyEarningGetListByUser.familyDetails != null &&
            familyEarningGetListByUser.familyDetails.length == this.state.minEntry
          ) {
            // SET
            this.setState({
              isCompleted: true
            });
          } else {
            this.setState({
              isCompleted: false
            });
          }

          if (
            // FOR REMOVED
            familyEarningGetListByUser && familyEarningGetListByUser.familyDetails != null &&
            familyEarningGetListByUser.familyDetails.length == this.state.minEntry + 1
          ) {
            // SET
            this.setState({
              isRemoved: true,
              isNextVisible: true
            });
          } else {
            this.setState({
              isRemoved: false,
              isNextVisible: false
            });
          }

          if (
            // FOR NEXT
            familyEarningGetListByUser && familyEarningGetListByUser.familyDetails != null &&
            familyEarningGetListByUser.familyDetails.length >= this.state.minEntry + 1
          ) {
            // SET
            this.setState({
              isNextVisible: true
            });
          } else {
            this.setState({
              isNextVisible: false
            });
          }
        }
        break;
      case "UPDATE_APPLICATION_FORMS_FAMILYEARNING_SUCCESS":
        const baseUrl = ["BML1", "BML3"];
        this.state.formData = {
          addressLine: "",
          mobile: "",
          alternateMobile: "",
          type: "",
          qualification: "",
          panNumber: "",
          employer: "",
          absoluteIncome: "",
          email: "",
          name: "",
          occupation: "",
          otherOccupation: "",
          relation: "",
          customData: {},
          id: ""
        };
        this.setState(
          {
            status: true,
            msg: "Record has been Updated",
            showLoader: true,
            formData: this.state.formData,
            totalIncome: null
          },
          () => this.getfamilyEarningsData()
        );

        if (this.state.isCompleted) {
          // CALL FOR UPDATE IF COMPLETED
          this.updateArrayStepCompletedCall();
        }
        // if (!baseUrl.includes(this.props.BSID)) {
        //   if (this.state.isNextVisible) {
        //     // go next if isNextVisible
        //     this.setState({
        //       isNext: true
        //     });
        //   }
        // }

        break;

      case "UPDATE_APPLICATION_FORMS_FAMILYEARNING_FAILURE":
        this.setState({
          status: false,
          msg: "Something went wrong",
          showLoader: this,
          totalIncome: null,
          activeTab: null

        });
        break;
      case "DELETE_APPLICATION_FORMS_FAMILYEARNING_SUCCESS":
        this.setState(
          {
            status: true,
            msg: "Your record has been deleted",
            showLoader: true,
            deleteSuccessCallBack: null,
            showConfirmationPopup: false,
            totalIncome: null

          },
          () => this.getfamilyEarningsData()
        );
        this.updateArrayStepForDelete(); // DELETE CALL FOR UPDATE STEPS

        break;
      case "DELETE_APPLICATION_FORMS_REFERENCE_FAILURE":
        this.setState({
          status: false,
          msg: "No Record Deleted",
          showLoader: true,
          deleteSuccessCallBack: null,
          showConfirmationPopup: false
        });
        break;
    }

    if (applicationEducation && applicationEducation.type === "UPDATE_EDUCATION_INFO_STEP_FAILURE") {
      const { error } = applicationEducation;
      this.setState({
        status: false,
        msg: error && error.response && error.response.data.message,
        showLoader: true,
      });
    }
    if (applicationEducation && applicationEducation.type === "UPDATE_EDUCATION_INFO_STEP_SUCCESS") {
      this.setState({
        isNext: true
      });
    }
  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
  }

  formClose() {
    this.setState({
      isFormButtonShow: true,
      activeTab: null
    });
  }
  brandVideo() {
    this.setState({
      isOpenVideo: true
    })
  }

  brandVideoClose() {
    this.setState({
      isOpenVideo: false
    })
  }


  goNext() {
    let scholarshipId = "";
    if (window != undefined) {
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    // this.setState(
    //   {
    //     isNext: true
    //   },
    //   () => {
    //     this.props.updateStep({
    //       userId: gblFunc.getStoreUserDetails()["userId"],
    //       scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
    //       step: "FAMILY_INFO"
    //     });
    //     setTimeout(() => {
    //       this.props.applicationInstructionStep({
    //         scholarshipId: gblFunc.getStoreApplicationScholarshipId()
    //       });
    //     }, 1000);
    //   }
    // );
    this.props.updateStep({
      userId: gblFunc.getStoreUserDetails()["userId"],
      scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
      step: "FAMILY_INFO"
    });

    setTimeout(() => {
      this.props.applicationInstructionStep({
        scholarshipId: gblFunc.getStoreApplicationScholarshipId()
      });
    }, 1000);
  }

  render() {
    const formConfig = this.state.configFieldsData;
    let familyInstructionTemplate = "";
    let relationData = formConfig && formConfig.relation && formConfig.relation.dataOptions ? formConfig.relation.dataOptions.sort((a, b) => parseFloat(a.id) - parseFloat(b.id)) : null;
    const { applicationStepInstruction, scholarshipSteps } = this.props;
    if (scholarshipSteps && scholarshipSteps.length) {
      familyInstructionTemplate = extractStepTemplate(
        scholarshipSteps,
        "FAMILY_INFO"
      );
    }
    var statusList = [],
      statusFlag = false;
    if (this.state.familyEarningGetList && this.state.familyEarningGetList.length > 0) {
      statusList = this.state.familyEarningGetList.map(function (el) { return el.familyMemberSaved; });
      statusFlag = statusList.reduce((p, c) => p === c ? p : false);
    }
    return (
      <section className="sectionwhite">
        {this.state.isNext ? (
          <Redirect
            to={`/application/${this.props.match.params.bsid.toUpperCase()}/form/${this.props.redirectionSteps[
              this.props.redirectionSteps.indexOf("familyInfo") + 1
            ]
              }`}
          />
        ) : (
            ""
          )}
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <ConfirmMessagePopup
          message={"Are you sure want to delete ?"}
          showPopup={this.state.showConfirmationPopup}
          onConfirmationSuccess={this.state.deleteSuccessCallBack}
          onConfirmationFailure={this.hideConfirmationPopup}
        />
        <article className="form">
          <article className="row familydetails">
            {familyInstructionTemplate.length &&
              familyInstructionTemplate[0].message ? (
                <article
                  dangerouslySetInnerHTML={{
                    __html: familyInstructionTemplate[0].message
                  }}
                  className="col-md-12 subheadingerror"
                />
              ) : (
                <article
                  dangerouslySetInnerHTML={{
                    __html: "* Please add minimum 1 earning family member"
                  }}
                  className="col-md-12 subheadingerror"
                />
              )}

            {this.state.isOpenVideo ? <FamilyVideoPopup brandVideoClose={this.brandVideoClose} /> : null}
            <article className="watch-video" onClick={this.brandVideo}> Watch Video</article>

            {!!this.state.familyEarningGetList && this.state.familyEarningGetList.length > 0 && !!this.state.familyEarningGetList[0].absoluteIncome ?
              <h3>
                {/* Total Annual Income: <span>{this.state.familyEarningGetList.map(item => Number(item.absoluteIncome)).reduce((prev, next) => prev + next)}</span> */}
                Total Annual Income: <span>{!!this.state.totalIncome ? this.state.totalIncome :
                  this.state.familyEarningGetList.map(item => Number(item.absoluteIncome)).reduce((prev, next) => prev + next)}</span>
              </h3>
              : ""}

          </article>



          {this.state.familyEarningGetList && this.state.familyEarningGetList.map((cls, index) => {
            let listItems = camelCase(cls);

            return (
              <article className="row">
                <article className="col-md-12">
                  <article className="newsubheading">
                    <article className="heading topborder"
                      onClick={event => this.editfamilyEarningVal(listItems)}

                    >
                      <p key={listItems.id} >
                        <i
                          className={(this.state.formData.relation && this.state.formData.relation == listItems.relation) && this.state.activeTab === listItems.relation ? "fa fa-minus" : "fa fa-plus"}
                        >
                        </i>
                        {listItems.relationName ? listItems.relationName : listItems.relation_name}
                      </p>


                      <span className="pull-right" style={{
                        position: "relative",
                        top: "-30px"
                      }}>
                        {
                          listItems.familyMemberSaved ?
                            (<span>Completed&nbsp;&nbsp;<i className="fa fa-check-circle-o complete">&nbsp;</i></span>) :
                            (<span>Pending&nbsp;&nbsp;<i className="fa fa-pencil-square-o pending">&nbsp;</i></span>)
                        }
                      </span>
                    </article>
                    <article>
                      {(!this.state.isFormButtonShow) && listItems.relationName == this.state.formData.relationName ? (
                        <article>
                          <article className="row">
                            <article className="familydetails">
                              <h4>
                                {"Family Member " + (index + 1)}
                                <article className="pull-right">
                                  <i className="fa fa-trash iconedit" onClick={this.removefamilyEarning.bind(
                                    this,
                                    listItems.id,
                                    listItems.relation
                                  )}> &nbsp;</i>
                                </article>
                              </h4>
                            </article>
                            {formConfig.name != null && formConfig.name.active ? (
                              <article className="col-md-4">
                                <article className="form-group">
                                  <input
                                    type="text"
                                    id="name"
                                    data-custom={formConfig.name.custom}
                                    onChange={this.formChangeHandler}
                                    value={this.state.formData.name || ""}
                                    required
                                  />
                                  <label htmlFor="name">
                                    {gblFunc.capitalText(formConfig.name.label)}
                                  </label>
                                  {this.state.validations["name"] ? (
                                    <span className="error animated bounce">
                                      {this.state.validations["name"]}
                                    </span>
                                  ) : null}
                                </article>
                              </article>
                            ) : (
                                ""
                              )}

                            {formConfig.mobile != null && formConfig.mobile.active ? (
                              <article className="col-md-4">
                                <article className="form-group">
                                  <input
                                    type="text"
                                    id="mobile"
                                    data-custom={formConfig.mobile.custom}
                                    onChange={this.formChangeHandler}
                                    value={this.state.formData.mobile || ""}
                                    maxLength="10"
                                    required
                                  />
                                  <label>
                                    {gblFunc.capitalText(formConfig.mobile.label)}
                                  </label>
                                  {this.state.validations["mobile"] ? (
                                    <span className="error animated bounce">
                                      {this.state.validations["mobile"]}
                                    </span>
                                  ) : null}
                                </article>
                              </article>
                            ) : (
                                ""
                              )}

                            {formConfig.alternateMobile != null &&
                              formConfig.alternateMobile.active ? (
                                <article className="col-md-4">
                                  <article className="form-group">
                                    <input
                                      type="text"
                                      id="alternateMobile"
                                      data-custom={formConfig.alternateMobile.custom}
                                      onChange={this.formChangeHandler}
                                      value={this.state.formData.alternateMobile || ""}
                                      maxLength="10"
                                      required
                                    />
                                    <label>
                                      {gblFunc.capitalText(
                                        formConfig.alternateMobile.label
                                      )}
                                    </label>
                                    {this.state.validations["alternateMobile"] ? (
                                      <span className="error animated bounce">
                                        {this.state.validations["alternateMobile"]}
                                      </span>
                                    ) : null}
                                  </article>
                                </article>
                              ) : (
                                ""
                              )}

                            {formConfig.panNumber != null &&
                              formConfig.panNumber.active ? (
                                <article className="col-md-4">
                                  <article className="form-group">
                                    <input
                                      type="text"
                                      id="panNumber"
                                      data-custom={formConfig.panNumber.custom}
                                      onChange={this.formChangeHandler}
                                      value={this.state.formData.panNumber || ""}
                                      required
                                    />
                                    <label>{`${formConfig.panNumber.label}`}</label>
                                  </article>
                                  {this.state.validations["panNumber"] ? (
                                    <span className="error animated bounce">
                                      {this.state.validations["panNumber"]}
                                    </span>
                                  ) : null}
                                </article>
                              ) : (
                                ""
                              )}

                            {formConfig.absoluteIncome != null &&
                              formConfig.absoluteIncome.active ? (
                                <article className="col-md-4">
                                  <article className="form-group">
                                    <input
                                      type="text"
                                      id="absoluteIncome"
                                      data-custom={formConfig.absoluteIncome.custom}
                                      onChange={this.formChangeHandler}
                                      value={this.state.formData.absoluteIncome || ""}
                                      required
                                    />
                                    <label>{formConfig.absoluteIncome.label}</label>
                                    {this.state.validations["absoluteIncome"] ? (
                                      <span className="error animated bounce">
                                        {this.state.validations["absoluteIncome"]}
                                      </span>
                                    ) : null}
                                  </article>
                                </article>
                              ) : (
                                ""
                              )}

                            {formConfig.employer != null &&
                              formConfig.employer.active ? (
                                <article className="col-md-4">
                                  <article className="form-group">
                                    <input
                                      type="text"
                                      id="employer"
                                      data-custom={formConfig.employer.custom}
                                      onChange={this.formChangeHandler}
                                      value={this.state.formData.employer || ""}
                                      required
                                    />
                                    <label>{formConfig.employer.label}</label>
                                    {this.state.validations["employer"] ? (
                                      <span className="error animated bounce">
                                        {this.state.validations["employer"]}
                                      </span>
                                    ) : null}
                                  </article>
                                </article>
                              ) : (
                                ""
                              )}

                            {formConfig.addressLine != null &&
                              formConfig.addressLine.active ? (
                                <article className="col-md-4">
                                  <article className="form-group">
                                    <input
                                      type="text"
                                      id="addressLine"
                                      data-custom={formConfig.addressLine.custom}
                                      onChange={this.formChangeHandler}
                                      value={this.state.formData.addressLine || ""}
                                      required
                                    />
                                    <label>{`${formConfig.addressLine.label}`}</label>
                                    {this.state.validations["addressLine"] ? (
                                      <span className="error animated bounce">
                                        {this.state.validations["addressLine"]}
                                      </span>
                                    ) : null}
                                  </article>
                                </article>
                              ) : (
                                ""
                              )}

                            {formConfig.email != null && formConfig.email.active ? (
                              <article className="col-md-4">
                                <article className="form-group">
                                  <input
                                    type="text"
                                    id="email"
                                    data-custom={formConfig.email.custom}
                                    onChange={this.formChangeHandler}
                                    value={this.state.formData.email || ""}
                                    required
                                  />
                                  <label>{`${formConfig.email.label}`}</label>
                                  {this.state.validations["email"] ? (
                                    <span className="error animated bounce">
                                      {this.state.validations["email"]}
                                    </span>
                                  ) : null}
                                </article>
                              </article>
                            ) : (
                                ""
                              )}

                            {formConfig.relation != null && formConfig.relation.active ? (
                              <article className="col-md-4">
                                <article className="form-group">
                                  <select
                                    id="relation"
                                    className="icon"
                                    onChange={this.formChangeHandler.bind(this)}
                                    value={this.state.formData.relation || ""}
                                    disabled={!this.state.isFormButtonShow && !!this.state.formData && !!this.state.formData.relation}
                                  >
                                    <option value="">Select Relation</option>
                                    {relationData ? (
                                      relationData.map((res, i) => {
                                        let isdisabled = this.state.familyEarningGetList && this.state.familyEarningGetList.length > 0 ? this.state.familyEarningGetList.sort((a, b) => parseFloat(a.relation) - parseFloat(b.relation)) : [];
                                        let familyDis = isdisabled[i] ? isdisabled[i]['relation'] : isdisabled[0] ? isdisabled[0]['relation'] : null;
                                        return (
                                          <option
                                            key={res.id}
                                            value={res.id}
                                            disabled={familyDis == res.id ? true : false}
                                          >
                                            {res.rulevalue}
                                          </option>
                                        );
                                      })
                                    ) : this.state.relation != null ? (
                                      this.state.relation.map((res, i) => {
                                        let isdisabled = this.state.familyEarningGetList[
                                          i
                                        ]
                                          ? this.state.familyEarningGetList[i]["relation"]
                                          : this.state.familyEarningGetList[0]
                                            ? this.state.familyEarningGetList[0]["relation"]
                                            : "";

                                        return (
                                          <option
                                            disabled={isdisabled == res.id ? true : false}
                                            key={res.id}
                                            value={res.id}
                                          >
                                            {res.rulevalue}
                                          </option>
                                        );
                                      })
                                    ) : (
                                          <option>Relation</option>
                                        )}
                                  </select>
                                  <label className="labelstyle">
                                    {gblFunc.capitalText(formConfig.relation.label)}
                                  </label>
                                  {this.state.validations["relation"] ? (
                                    <span className="error animated bounce">
                                      {this.state.validations["relation"]}
                                    </span>
                                  ) : null}
                                </article>
                              </article>
                            ) : (
                                ""
                              )}

                            {formConfig.qualification != null &&
                              formConfig.qualification.active ? (
                                <article className="col-md-4">
                                  <article className="form-group">
                                    <select
                                      id="qualification"
                                      className="icon"
                                      onChange={this.formChangeHandler.bind(this)}
                                      value={this.state.formData.qualification || ""}
                                    >
                                      <option value="">Select Qualification</option>
                                      {formConfig.qualification.dataOptions != null ? (
                                        formConfig.qualification.dataOptions.map(res => {
                                          return (
                                            <option key={res.id} value={res.id}>
                                              {res.rulevalue}
                                            </option>
                                          );
                                        })
                                      ) : this.state.qualification != null ? (
                                        this.state.qualification.map(res => {
                                          return (
                                            <option key={res.id} value={res.id}>
                                              {res.rulevalue}
                                            </option>
                                          );
                                        })
                                      ) : (
                                            <option>Qualification</option>
                                          )}
                                    </select>
                                    <label className="labelstyle">
                                      {gblFunc.capitalText(formConfig.qualification.label)}
                                    </label>
                                    {this.state.validations["qualification"] ? (
                                      <span className="error animated bounce">
                                        {this.state.validations["qualification"]}
                                      </span>
                                    ) : null}
                                  </article>
                                </article>
                              ) : (
                                ""
                              )}

                            {formConfig.occupation != null &&
                              formConfig.occupation.active ? (
                                <article className="col-md-4">
                                  <article className="form-group">
                                    <select
                                      className="icon"
                                      id="occupation"
                                      onChange={this.formChangeHandler.bind(this)}
                                      value={this.state.formData.occupation || ""}
                                    >
                                      <option value="">Select Occupation</option>
                                      {formConfig.occupation.dataOptions != null ? (
                                        formConfig.occupation.dataOptions.map(res => {
                                          return (
                                            <option key={res.id} value={res.id}>
                                              {res.occupationName}
                                            </option>
                                          );
                                        })
                                      ) : this.state.occupation != null ? (
                                        this.state.occupation.map(res => {
                                          return (
                                            <option key={res.id} value={res.id}>
                                              {res.occupationName}
                                            </option>
                                          );
                                        })
                                      ) : (
                                            <option>Occupation</option>
                                          )}
                                    </select>
                                    <label className="labelstyle">
                                      {gblFunc.capitalText(formConfig.occupation.label)}
                                    </label>
                                    {this.state.validations["occupation"] ? (
                                      <span className="error animated bounce">
                                        {this.state.validations["occupation"]}
                                      </span>
                                    ) : null}
                                  </article>
                                </article>
                              ) : (
                                ""
                              )}
                            {(this.state.formData && this.state.formData.occupation == '12') || this.state.isOtherOccupation ? (
                              <article className="col-md-4">
                                <article className="form-group">
                                  <input
                                    type="text"
                                    id="otherOccupation"
                                    required
                                    onChange={this.formChangeHandler}
                                    value={this.state.formData.otherOccupation || ""}
                                  />
                                  <label>Other Occupation</label>
                                  {this.state.validations["otherOccupation"] ? (
                                    <span className="error animated bounce">
                                      {this.state.validations["otherOccupation"]}
                                    </span>
                                  ) : null}
                                </article>
                              </article>
                            ) : (
                                ""
                              )}

                            {formConfig.annualIncome != null &&
                              formConfig.annualIncome.active ? (
                                <article className="col-md-4">
                                  <article className="form-group">
                                    <input
                                      type="text"
                                      id="annualIncome"
                                      data-custom={formConfig.annualIncome.custom}
                                      onChange={this.formChangeHandler}
                                      value={this.state.formData.annualIncome || ""}
                                      required
                                    />
                                    <label>
                                      {gblFunc.capitalText(formConfig.annualIncome.label)}
                                    </label>
                                    {this.state.validations["annualIncome"] ? (
                                      <span className="error animated bounce">
                                        {this.state.validations["annualIncome"]}
                                      </span>
                                    ) : null}
                                  </article>
                                </article>
                              ) : (
                                ""
                              )}
                            {formConfig.employeeId != null &&
                              formConfig.employeeId.active ? (
                                <article className="col-md-4">
                                  <article className="form-group">
                                    <input
                                      type="text"
                                      id="employeeId"
                                      // disabled="true"
                                      data-custom={formConfig.employeeId.custom}
                                      onChange={this.formChangeHandler}
                                      value={this.state.formData.customData.employeeId}
                                      required
                                    />
                                    <label>
                                      {gblFunc.capitalText(formConfig.employeeId.label)}
                                    </label>
                                    {this.state.validations["employeeId"] ? (
                                      <span className="error animated bounce">
                                        {this.state.validations["employeeId"]}
                                      </span>
                                    ) : null}
                                  </article>
                                </article>
                              ) : (
                                ""
                              )}
                            {formConfig.storeDetails != null &&
                              formConfig.storeDetails.active ? (
                                <article className="col-md-4">
                                  <article className="form-group">
                                    <input
                                      type="text"
                                      id="storeDetails"
                                      data-custom={formConfig.storeDetails.custom}
                                      onChange={this.formChangeHandler}
                                      value={
                                        (this.state.formData.customData &&
                                          this.state.formData.customData.storeDetails) ||
                                        ""
                                      }
                                      required
                                    />
                                    <label>
                                      {gblFunc.capitalText(formConfig.storeDetails.label)}
                                    </label>
                                    {this.state.validations["storeDetails"] ? (
                                      <span className="error animated bounce">
                                        {this.state.validations["storeDetails"]}
                                      </span>
                                    ) : null}
                                  </article>
                                </article>
                              ) : (
                                ""
                              )}
                            {formConfig.joiningDate != null &&
                              formConfig.joiningDate.active ? (
                                <article className="col-md-4">
                                  <article className="form-group">
                                    <DatePicker
                                      showYearDropdown
                                      scrollableYearDropdown
                                      // readOnly
                                      yearDropdownItemNumber={40}
                                      minDate={moment().subtract(100, "years")}
                                      maxDate={moment().add(1, "years")}
                                      name="joiningDate"
                                      id="joiningDate"
                                      // placeholder={`${formConfig.dob.label}`}
                                      data-custom={formConfig.joiningDate.custom}
                                      className="icon-date"
                                      autoComplete="off"
                                      selected={
                                        this.state.formData.customData &&
                                          this.state.formData.customData.joiningDate
                                          ? moment(
                                            moment(
                                              (this.state.formData.customData &&
                                                this.state.formData.customData
                                                  .joiningDate) ||
                                              ""
                                            ).format("DD-MM-YYYY"),
                                            "DD-MM-YYYY"
                                          )
                                          : null
                                      }
                                      onChange={event =>
                                        this.formChangeHandler(event, "joiningDate")
                                      }
                                      dateFormat="DD-MM-YYYY"
                                    />

                                    <label className="labelstyle">
                                      {gblFunc.capitalText(formConfig.joiningDate.label)}
                                    </label>
                                    {this.state.validations["joiningDate"] ? (
                                      <span className="error animated bounce">
                                        {this.state.validations["joiningDate"]}
                                      </span>
                                    ) : null}
                                  </article>
                                </article>
                              ) : (
                                ""
                              )}
                          </article>
                          <article className="row">
                            <article className="col-md-12 text-right">
                              <input
                                type="submit"
                                value="Save"
                                onClick={this.submit}
                                className="btn"
                              /> &nbsp; &nbsp;
                              <button onClick={this.formClose} className="btn">
                                Close
                    </button>
                            </article>
                          </article>
                        </article>
                      ) : null}
                    </article>

                  </article>
                </article>
              </article>
            )
          }
          )}
          <article className="row">


            {/*START DYNAMIC TABLE DISPLAY*/}

            {/* {this.state.familyEarningGetList != null &&
              this.state.familyEarningGetList.length > 0 ? (
                <article className="margintoptable table-responsive floatTable">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        {formConfig.name != null && formConfig.name.active ? (
                          <th>{formConfig.name.label}</th>
                        ) : (
                            ""
                          )}

                        {formConfig.employeeId != null &&
                          formConfig.employeeId.active ? (
                            <th>{formConfig.employeeId.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.storeDetails != null &&
                          formConfig.storeDetails.active ? (
                            <th>{formConfig.storeDetails.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.joiningDate != null &&
                          formConfig.joiningDate.active ? (
                            <th>{formConfig.joiningDate.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.mobile != null && formConfig.mobile.active ? (
                          <th>{formConfig.mobile.label}</th>
                        ) : (
                            ""
                          )}

                        {formConfig.alternateMobile != null &&
                          formConfig.alternateMobile.active ? (
                            <th>{formConfig.alternateMobile.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.panNumber != null &&
                          formConfig.panNumber.active ? (
                            <th>{formConfig.panNumber.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.absoluteIncome != null &&
                          formConfig.absoluteIncome.active ? (
                            <th>{formConfig.absoluteIncome.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.addressLine != null &&
                          formConfig.addressLine.active ? (
                            <th>{formConfig.addressLine.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.email != null && formConfig.email.active ? (
                          <th>{formConfig.email.label}</th>
                        ) : (
                            ""
                          )}

                        {formConfig.relation != null &&
                          formConfig.relation.active ? (
                            <th>{formConfig.relation.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.qualification != null &&
                          formConfig.qualification.active ? (
                            <th>{formConfig.qualification.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.occupation != null &&
                          formConfig.occupation.active ? (
                            <th>{formConfig.occupation.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.annualIncome != null &&
                          formConfig.annualIncome.active ? (
                            <th>{formConfig.annualIncome.label}</th>
                          ) : (
                            ""
                          )}
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.familyEarningGetList.map((res, i) => {
                        let listItems = camelCase(res);
                        return (
                          <tr key={i}>
                            {formConfig.name != null && formConfig.name.active ? (
                              <td>{listItems.name}</td>
                            ) : (
                                ""
                              )}

                            {formConfig.employeeId != null &&
                              formConfig.employeeId.active ? (
                                <td>
                                  {listItems.customData &&
                                    listItems.customData.employeeId
                                    ? listItems.customData.employeeId
                                    : null}
                                </td>
                              ) : (
                                ""
                              )}

                            {formConfig.storeDetails != null &&
                              formConfig.storeDetails.active ? (
                                <td>
                                  {listItems.customData &&
                                    listItems.customData.storeDetails
                                    ? listItems.customData.storeDetails
                                    : null}
                                </td>
                              ) : (
                                ""
                              )}

                            {formConfig.joiningDate != null &&
                              formConfig.joiningDate.active ? (
                                <td>
                                  {listItems.customData &&
                                    listItems.customData.joiningDate
                                    ? listItems.customData.joiningDate
                                    : null}
                                </td>
                              ) : (
                                ""
                              )}

                            {formConfig.mobile != null &&
                              formConfig.mobile.active ? (
                                <td>{listItems.mobile}</td>
                              ) : (
                                ""
                              )}

                            {formConfig.alternateMobile != null &&
                              formConfig.alternateMobile.active ? (
                                <td>{listItems.alternateMobile}</td>
                              ) : (
                                ""
                              )}

                            {formConfig.panNumber != null &&
                              formConfig.panNumber.active ? (
                                <td>{listItems.panNumber}</td>
                              ) : (
                                ""
                              )}

                            {formConfig.absoluteIncome != null &&
                              formConfig.absoluteIncome.active ? (
                                <td>{listItems.absoluteIncome}</td>
                              ) : (
                                ""
                              )}

                            {formConfig.addressLine != null &&
                              formConfig.addressLine.active ? (
                                <td>{listItems.addressLine}</td>
                              ) : (
                                ""
                              )}

                            {formConfig.email != null &&
                              formConfig.email.active ? (
                                <td>{listItems.email}</td>
                              ) : (
                                ""
                              )}

                            {formConfig.relation != null &&
                              formConfig.relation.active ? (
                                <td>{listItems.relationName}</td>
                              ) : (
                                ""
                              )}

                            {formConfig.qualification != null &&
                              formConfig.qualification.active ? (
                                <td>
                                  {this.state.qualification
                                    .filter(item => {
                                      return listItems.qualification == item.id;
                                    })
                                    .map(scan => {
                                      return scan.rulevalue;
                                    })}
                                </td>
                              ) : (
                                ""
                              )}

                            {formConfig.occupation != null &&
                              formConfig.occupation.active ? (
                                <td>
                                  {this.state.occupation
                                    .filter(item => {
                                      return listItems.occupation == item.id;
                                    })
                                    .map(scan => {
                                      return scan.occupationName;
                                    })}
                                </td>
                              ) : (
                                ""
                              )}

                            {formConfig.annualIncome != null &&
                              formConfig.annualIncome.active ? (
                                <td>{listItems.annualIncome}</td>
                              ) : (
                                ""
                              )}
                            <td>
                              <article className="pull-left">
                                <i
                                  onClick={event =>
                                    this.editfamilyEarningVal(listItems)
                                  }
                                  className="fa fa-edit iconedit"
                                >
                                  {" "}
                                  &nbsp;
                              </i>
                                <i

                                  onClick={this.removefamilyEarning.bind(
                                    this,
                                    listItems.id,
                                    listItems.relation
                                  )}
                                  className="fa fa-trash iconedit"
                                >
                                  {" "}
                                  &nbsp;
                              </i>
                              </article>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </article>
              ) : (
                ""
              )} */}

            {/*END DYNAMIC TABLE DISPLAY*/}
            {!this.state.isFormButtonShow && !this.state.familyEarningGetList.some(x => x.relation == parseInt(this.state.formData.relation)) ? (
              <article>
                <article className="col-md-12">
                  <article className="familydetails saddasd">
                    <h4>
                      {/* Family Member1 */}
                      {"Add Family Member " + (this.state.familyEarningGetList.length + 1)}
                      {/* <article className="pull-right">
                        <i className="fa fa-trash iconedit" onClick={this.removefamilyEarning.bind(
                                    this,
                                    this.state.formData.id,
                                    this.state.formData.relation
                                  )}> &nbsp;</i>
                      </article> */}
                    </h4>
                  </article>
                  {formConfig.name != null && formConfig.name.active ? (
                    <article className="col-md-4">
                      <article className="form-group">
                        <input
                          type="text"
                          id="name"
                          data-custom={formConfig.name.custom}
                          onChange={this.formChangeHandler}
                          value={this.state.formData.name || ""}
                          required
                        />
                        <label htmlFor="name">
                          {gblFunc.capitalText(formConfig.name.label)}
                        </label>
                        {this.state.validations["name"] ? (
                          <span className="error animated bounce">
                            {this.state.validations["name"]}
                          </span>
                        ) : null}
                      </article>
                    </article>
                  ) : (
                      ""
                    )}

                  {formConfig.mobile != null && formConfig.mobile.active ? (
                    <article className="col-md-4">
                      <article className="form-group">
                        <input
                          type="text"
                          id="mobile"
                          data-custom={formConfig.mobile.custom}
                          onChange={this.formChangeHandler}
                          value={this.state.formData.mobile || ""}
                          maxLength="10"
                          required
                        />
                        <label>
                          {gblFunc.capitalText(formConfig.mobile.label)}
                        </label>
                        {this.state.validations["mobile"] ? (
                          <span className="error animated bounce">
                            {this.state.validations["mobile"]}
                          </span>
                        ) : null}
                      </article>
                    </article>
                  ) : (
                      ""
                    )}

                  {formConfig.alternateMobile != null &&
                    formConfig.alternateMobile.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="alternateMobile"
                            data-custom={formConfig.alternateMobile.custom}
                            onChange={this.formChangeHandler}
                            value={this.state.formData.alternateMobile || ""}
                            maxLength="10"
                            required
                          />
                          <label>
                            {gblFunc.capitalText(
                              formConfig.alternateMobile.label
                            )}
                          </label>
                          {this.state.validations["alternateMobile"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["alternateMobile"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                      ""
                    )}

                  {formConfig.panNumber != null &&
                    formConfig.panNumber.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="panNumber"
                            data-custom={formConfig.panNumber.custom}
                            onChange={this.formChangeHandler}
                            value={this.state.formData.panNumber || ""}
                            required
                          />
                          <label>{`${formConfig.panNumber.label}`}</label>
                        </article>
                        {this.state.validations["panNumber"] ? (
                          <span className="error animated bounce">
                            {this.state.validations["panNumber"]}
                          </span>
                        ) : null}
                      </article>
                    ) : (
                      ""
                    )}

                  {formConfig.absoluteIncome != null &&
                    formConfig.absoluteIncome.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="absoluteIncome"
                            data-custom={formConfig.absoluteIncome.custom}
                            onChange={this.formChangeHandler}
                            value={this.state.formData.absoluteIncome || ""}
                            required
                          />
                          <label>{formConfig.absoluteIncome.label}</label>
                          {this.state.validations["absoluteIncome"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["absoluteIncome"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                      ""
                    )}

                  {formConfig.employer != null &&
                    formConfig.employer.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="employer"
                            data-custom={formConfig.employer.custom}
                            onChange={this.formChangeHandler}
                            value={this.state.formData.employer || ""}
                            required
                          />
                          <label>{formConfig.employer.label}</label>
                          {this.state.validations["employer"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["employer"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                      ""
                    )}

                  {formConfig.addressLine != null &&
                    formConfig.addressLine.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="addressLine"
                            data-custom={formConfig.addressLine.custom}
                            onChange={this.formChangeHandler}
                            value={this.state.formData.addressLine || ""}
                            required
                          />
                          <label>{`${formConfig.addressLine.label}`}</label>
                          {this.state.validations["addressLine"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["addressLine"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                      ""
                    )}

                  {formConfig.email != null && formConfig.email.active ? (
                    <article className="col-md-4">
                      <article className="form-group">
                        <input
                          type="text"
                          id="email"
                          data-custom={formConfig.email.custom}
                          onChange={this.formChangeHandler}
                          value={this.state.formData.email || ""}
                          required
                        />
                        <label>{`${formConfig.email.label}`}</label>
                        {this.state.validations["email"] ? (
                          <span className="error animated bounce">
                            {this.state.validations["email"]}
                          </span>
                        ) : null}
                      </article>
                    </article>
                  ) : (
                      ""
                    )}

                  {formConfig.relation != null && formConfig.relation.active ? (
                    <article className="col-md-4">
                      <article className="form-group">
                        <select
                          id="relation"
                          className="icon"
                          onChange={this.formChangeHandler.bind(this)}
                          value={this.state.formData.relation || ""}
                        // disabled={!this.state.isFormButtonShow && !!this.state.formData && !!this.state.formData.relation}
                        >
                          <option value="">Select Relation</option>
                          {relationData ? (
                            relationData.map((res, i) => {
                              //   let isdisabled = this.state.familyEarningGetList && this.state.familyEarningGetList.length > 0 ? this.state.familyEarningGetList.sort((a, b) => parseFloat(a.relation) - parseFloat(b.relation)) : [];
                              //   let familyDis = isdisabled[i] ? isdisabled[i]['relation'] : isdisabled[0] ? isdisabled[0]['relation'] : null;
                              //  console.log(familyDis,res.id,"disablefamil")
                              let isdisabled = this.state.familyEarningGetList.find((item) => {
                                if (("" + item.relation) == (res.id)) {
                                  return item
                                }
                              })
                              return (
                                <option
                                  key={res.id}
                                  value={res.id}
                                  // disabled={familyDis == Number(res.id) ? true : false}
                                  disabled={isdisabled && isdisabled.relation == Number(res.id) ? true : false}

                                >
                                  {res.rulevalue}
                                </option>
                              );
                            })
                          ) : this.state.relation != null ? (
                            this.state.relation.map((res, i) => {
                              let isdisabled = this.state.familyEarningGetList[
                                i
                              ]
                                ? this.state.familyEarningGetList[i]["relation"]
                                : this.state.familyEarningGetList[0]
                                  ? this.state.familyEarningGetList[0]["relation"]
                                  : "";

                              return (
                                <option
                                  disabled={isdisabled == res.id ? true : false}
                                  key={res.id}
                                  value={res.id}
                                >
                                  {res.rulevalue}
                                </option>
                              );
                            })
                          ) : (
                                <option>Relation</option>
                              )}
                        </select>
                        <label className="labelstyle">
                          {gblFunc.capitalText(formConfig.relation.label)}
                        </label>
                        {this.state.validations["relation"] ? (
                          <span className="error animated bounce">
                            {this.state.validations["relation"]}
                          </span>
                        ) : null}
                      </article>
                    </article>
                  ) : (
                      ""
                    )}

                  {formConfig.qualification != null &&
                    formConfig.qualification.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <select
                            id="qualification"
                            className="icon"
                            onChange={this.formChangeHandler.bind(this)}
                            value={this.state.formData.qualification || ""}
                          >
                            <option value="">Select Qualification</option>
                            {formConfig.qualification.dataOptions != null ? (
                              formConfig.qualification.dataOptions.map(res => {
                                return (
                                  <option key={res.id} value={res.id}>
                                    {res.rulevalue}
                                  </option>
                                );
                              })
                            ) : this.state.qualification != null ? (
                              this.state.qualification.map(res => {
                                return (
                                  <option key={res.id} value={res.id}>
                                    {res.rulevalue}
                                  </option>
                                );
                              })
                            ) : (
                                  <option>Qualification</option>
                                )}
                          </select>
                          <label className="labelstyle">
                            {gblFunc.capitalText(formConfig.qualification.label)}
                          </label>
                          {this.state.validations["qualification"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["qualification"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                      ""
                    )}

                  {formConfig.occupation != null &&
                    formConfig.occupation.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <select
                            className="icon"
                            id="occupation"
                            onChange={this.formChangeHandler.bind(this)}
                            value={this.state.formData.occupation || ""}
                          >
                            <option value="">Select Occupation</option>
                            {formConfig.occupation.dataOptions != null ? (
                              formConfig.occupation.dataOptions.map(res => {
                                return (
                                  <option key={res.id} value={res.id}>
                                    {res.occupationName}
                                  </option>
                                );
                              })
                            ) : this.state.occupation != null ? (
                              this.state.occupation.map(res => {
                                return (
                                  <option key={res.id} value={res.id}>
                                    {res.occupationName}
                                  </option>
                                );
                              })
                            ) : (
                                  <option>Occupation</option>
                                )}
                          </select>
                          <label className="labelstyle">
                            {gblFunc.capitalText(formConfig.occupation.label)}
                          </label>
                          {this.state.validations["occupation"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["occupation"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                      ""
                    )}
                  {(this.state.formData && this.state.formData.occupation == '12') || this.state.isOtherOccupation ? (
                    <article className="col-md-4">
                      <article className="form-group">
                        <input
                          type="text"
                          id="otherOccupation"
                          required
                          onChange={this.formChangeHandler}
                          value={this.state.formData.otherOccupation || ""}
                        />
                        <label>Other Occupation</label>
                        {this.state.validations["otherOccupation"] ? (
                          <span className="error animated bounce">
                            {this.state.validations["otherOccupation"]}
                          </span>
                        ) : null}
                      </article>
                    </article>
                  ) : (
                      ""
                    )}

                  {formConfig.annualIncome != null &&
                    formConfig.annualIncome.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="annualIncome"
                            data-custom={formConfig.annualIncome.custom}
                            onChange={this.formChangeHandler}
                            value={this.state.formData.annualIncome || ""}
                            required
                          />
                          <label>
                            {gblFunc.capitalText(formConfig.annualIncome.label)}
                          </label>
                          {this.state.validations["annualIncome"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["annualIncome"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                      ""
                    )}
                  {formConfig.employeeId != null &&
                    formConfig.employeeId.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="employeeId"
                            // disabled="true"
                            data-custom={formConfig.employeeId.custom}
                            onChange={this.formChangeHandler}
                            value={this.state.formData.customData.employeeId}
                            required
                          />
                          <label>
                            {gblFunc.capitalText(formConfig.employeeId.label)}
                          </label>
                          {this.state.validations["employeeId"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["employeeId"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                      ""
                    )}
                  {formConfig.storeDetails != null &&
                    formConfig.storeDetails.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="storeDetails"
                            data-custom={formConfig.storeDetails.custom}
                            onChange={this.formChangeHandler}
                            value={
                              (this.state.formData.customData &&
                                this.state.formData.customData.storeDetails) ||
                              ""
                            }
                            required
                          />
                          <label>
                            {gblFunc.capitalText(formConfig.storeDetails.label)}
                          </label>
                          {this.state.validations["storeDetails"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["storeDetails"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                      ""
                    )}
                  {formConfig.joiningDate != null &&
                    formConfig.joiningDate.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <DatePicker
                            showYearDropdown
                            scrollableYearDropdown
                            // readOnly
                            yearDropdownItemNumber={40}
                            minDate={moment().subtract(100, "years")}
                            maxDate={moment().add(1, "years")}
                            name="joiningDate"
                            id="joiningDate"
                            // placeholder={`${formConfig.dob.label}`}
                            data-custom={formConfig.joiningDate.custom}
                            className="icon-date"
                            autoComplete="off"
                            selected={
                              this.state.formData.customData &&
                                this.state.formData.customData.joiningDate
                                ? moment(
                                  moment(
                                    (this.state.formData.customData &&
                                      this.state.formData.customData
                                        .joiningDate) ||
                                    ""
                                  ).format("DD-MM-YYYY"),
                                  "DD-MM-YYYY"
                                )
                                : null
                            }
                            onChange={event =>
                              this.formChangeHandler(event, "joiningDate")
                            }
                            dateFormat="DD-MM-YYYY"
                          />

                          <label className="labelstyle">
                            {gblFunc.capitalText(formConfig.joiningDate.label)}
                          </label>
                          {this.state.validations["joiningDate"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["joiningDate"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                      ""
                    )}
                </article>
                <article className="row">
                  <article className="col-md-12 text-right">
                    <input
                      type="submit"
                      value="Save"
                      onClick={this.submit}
                      className="btn"
                    /> &nbsp; &nbsp;
                    <button onClick={this.formClose} className="btn">
                      Close
                    </button>
                  </article>
                </article>
              </article>
            ) : null}

            {this.state.isFormButtonShow ? (
              <article className="row">
                <article className="col-md-12">
                  {this.props.instructions.bsid == "SKKS1" ? (
                    this.state.familyEarningGetList != null &&
                      this.state.familyEarningGetList.length === 1 &&
                      childEduBSID[
                      this.props.match.params.bsid.toUpperCase()
                      ] ? null : !this.state.isNextVisible ? (
                        <a onClick={this.showForm} className="btn familybtnsize pull-left">
                          Add Family +
                        </a>
                      ) : null
                  ) : this.state.familyEarningGetList != null &&
                    this.state.familyEarningGetList.length === 1 &&
                    childEduBSID[
                    this.props.match.params.bsid.toUpperCase()
                    ] ? null : (
                        <a onClick={this.showForm} className="btn familybtnsize pull-left">
                          Add Family +
                        </a>
                      )}
                  {this.state.isNextVisible && statusFlag ? (
                    <a onClick={this.goNext} className="btn familybtnsize pull-right">
                      Save & Continue
                    </a>
                  ) : (
                      ""
                    )}
                </article>
              </article>
            ) : (
                ""
              )}
          </article>

        </article>
      </section>
    );
  }
}

export default FamilyEarnings;
