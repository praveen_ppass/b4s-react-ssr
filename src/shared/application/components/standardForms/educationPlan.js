import React, { Component } from "react";

class EducationPlan extends Component {
  render() {
    return (
      <section>
        <article className="form">
          <article className="row">
            <article className="col-md-12 subheading">
              &nbsp;&nbsp;What courses are you planning to pursue?
              <span>&nbsp;</span>
            </article>
            <section>
              <article className="row">
                <article className="col-md-4">
                  <article className="form-group">
                    <select className="icon">
                      <option>Doctorate</option>
                    </select>
                    <label className="selectLabel">Occupation</label>
                  </article>
                </article>
                <article className="col-md-4">
                  <article className="form-group">
                    <select className="icon">
                      <option>Phd</option>
                    </select>
                    <label className="selectLabel">Education</label>
                  </article>
                </article>
              </article>
              <article className="row">
                <article className="col-md-12 subheading">
                  Please let us know about top 3 colleges you are targeting
                  <span>&nbsp;</span>
                </article>

                <article className="col-md-4">
                  <article className="form-group">
                    <input type="text" id="College name" required />
                    <label htmlFor="College name">College name*</label>
                  </article>
                </article>
                <article className="col-md-4">
                  <article className="form-group">
                    <select className="icon">
                      <option>India</option>
                    </select>
                    <label className="selectLabel">Country*</label>
                  </article>
                </article>
                <article className="col-md-4">
                  <article className="form-group">
                    <select className="icon">
                      <option>Delhi</option>
                    </select>
                    <label className="selectLabel">State*</label>
                  </article>
                </article>
              </article>
              <article className="row">
                <article className="col-md-4">
                  <article className="form-group">
                    <input type="text" id="Total fee" required />
                    <label htmlFor="Total fee">Total fee*</label>
                  </article>
                </article>
                <article className="col-md-4">
                  <article className="form-group">
                    <input type="text" id="Course" required />
                    <label htmlFor="Course">Course*</label>
                  </article>
                </article>
                <article className="col-md-4">
                  <article className="form-group">
                    <input type="text" id="Tenure in months" required />
                    <label htmlFor="Tenure in months">Tenure in months*</label>
                  </article>
                </article>
                <article className="col-md-12">
                  <a className="btn-yellow">Add Class 12 +</a>
                </article>
              </article>
              <article className="row">
                <article className="col-md-12 subheading">
                  How do you plan to fund your course?
                </article>
                <article className="col-md-12 eduplancheck">
                  <span>
                    <label>
                      <input type="checkbox" />
                      <span />
                      <i>Self/Parents</i>
                    </label>
                  </span>
                  <span>
                    <label>
                      <input type="checkbox" />
                      <span />
                      <i>Education Loan</i>
                    </label>
                  </span>
                  <span>
                    <label>
                      <input type="checkbox" />
                      <span />
                      <i>Scholarship</i>
                    </label>
                  </span>
                </article>
              </article>
              <article className="row">
                <article className="col-md-12 subheading">
                  Are you looking to study abroad?
                </article>
                <article className="col-md-12 radio-btn">
                  <span>
                    <label>
                      <input type="radio" name="radio" checked={true} />
                      <span />
                      <i>Marks</i>
                    </label>
                  </span>
                  <span className="marginleft">
                    <label>
                      <input type="radio" name="radio" />
                      <span />
                      <i>CGPA</i>
                    </label>
                  </span>
                </article>
              </article>
              <article className="row">
                <article className="col-md-12">
                  <input
                    type="submit"
                    value="Save"
                    className="btn  pull-right"
                  />
                </article>
              </article>
            </section>
          </article>
        </article>
      </section>
    );
  }
}

export default EducationPlan;
