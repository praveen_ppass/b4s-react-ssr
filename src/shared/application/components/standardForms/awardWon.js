import React, { Component } from "react";
import gblFunc from "../../../../globals/globalFunctions";
import { mapValidationFunc } from "../../../../validation/rules";
import { ruleRunner } from "../../../../validation/ruleRunner";
import AlertMessage from "../../../common/components/alertMsg";
import Loader from "../../../common/components/loader";
import { yearArray } from "../../../../constants/constants";

class AwardsWon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFormButtonShow: true,
      configFieldsData: {},
      getSource: ["Private", "Government", "NGO"],
      showLoader: false,
      msg: "",
      status: "",
      AwardWonGetListByUser: null,
      formData: {
        description: null,
        level: null,
        name: null,
        source: null,
        year: null,
        customData: {},
        id: null
      },
      validations: {}
    };
    this.showForm = this.showForm.bind(this);
    this.formChangeHandler = this.formChangeHandler.bind(this);
    this.submit = this.submit.bind(this);
  }

  showForm() {
    this.state.isFormButtonShow = !this.state.isFormButtonShow;
    this.setState({
      isFormButtonShow: this.state.isFormButtonShow
    });
  }

  submit() {
    let userId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }

    let isSubmit = this.checkFormValidations();
    if (isSubmit) {
      for (let key in this.state.configFieldsData) {
        if (this.state.configFieldsData[key].custom) {
          this.state.formData.customData[key] = this.state.formData[key];
        }
      }
      this.setState({ formData: this.state.formData });
      this.props.saveAwardWonData({
        userId: userId,
        scholarshipId: scholarshipId,
        data: this.state.formData
      });
    }
  }

  componentDidMount() { }

  editAwardWonVal(items) {
    const {
      description,
      level,
      name,
      source,
      year,
      customData: { },
      id
    } = items;
    const editData = {
      description,
      level,
      name,
      source,
      year,
      customData: {},
      id
    };
    this.setState({
      isFormButtonShow: false,
      formData: editData
    });
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    let validationRules = [];
    if (this.state.validations.hasOwnProperty(fieldID)) {
      let configFields = this.state.configFieldsData[fieldID];
      if (configFields.validations != null) {
        configFields.validations.map(res => {
          if (res != null) {
            let validator = mapValidationFunc(res);
            if (validator != undefined) validationRules.push(validator);
          }
        });
        validationObject.name = "*Field";
        validationObject.validationFunctions = validationRules;
      }
    }
    return validationObject;
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state.formData[key],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  removeAwardWon(wonId) {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    this.props.deleteAwardWonData({
      userId: userId,
      scholarshipId: scholarshipId,
      wonId: wonId
    });
  }

  formChangeHandler(event) {
    const { id, value } = event.target;
    let validations = { ...this.state.validations };
    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);

      const validationResult = ruleRunner(
        value,
        id,
        name,
        ...validationFunctions
      );
      validations[id] = validationResult[id];
    }

    const updateFormData = { ...this.state.formData };
    updateFormData[id] = value;
    this.setState({
      formData: updateFormData,
      validations
    });
  }

  getAwardWonData() {
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    this.props.getAwardWonData({
      userId: userId,
      scholarshipId: scholarshipId,
      step: "SCHOLARSHIP_HISTORY"
    });
  }

  componentWillMount() {
    this.getAwardWonData();
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps.applicationAwardWon;

    switch (type) {
      case "FETCH_APPLICATION_FORMS_AWARDWON_SUCCESS":
        if (nextProps.applicationAwardWon["applicationAwardWonData"] != null) {
          const {
            AwardWonDataConfig,
            AwardWonGetListByUser
          } = nextProps.applicationAwardWon["applicationAwardWonData"];

          for (let key in AwardWonDataConfig) {
            if (
              AwardWonDataConfig[key].validations != null &&
              AwardWonDataConfig[key].active
            ) {
              this.state.validations[key] = null;
            }
          }

          this.setState({
            configFieldsData: AwardWonDataConfig,
            AwardWonGetListByUser: AwardWonGetListByUser,
            validations: this.state.validations
          });
        }
        break;
      case "UPDATE_APPLICATION_FORMS_AWARDWON_SUCCESS":
        this.setState({
          status: true,
          msg: "Record has been Updated",
          showLoader: true
        });

        this.state.formData = {
          address: "",
          mobile: "",
          name: "",
          occupation: "",
          other_occupation: "",
          relation: "",
          customData: {}
        };
        this.setState({
          formData: this.state.formData
        });

        this.getAwardWonData();
        break;

      case "UPDATE_APPLICATION_FORMS_AWARDWON_FAILURE":
        this.setState({
          status: false,
          msg: "Something went wrong",
          showLoader: this
        });
        break;
      case "DELETE_APPLICATION_FORMS_AWARDWON_SUCCESS":
        this.setState({
          status: true,
          msg: "Your record has been deleted",
          showLoader: true
        });
        this.getAwardWonData();
        break;
      case "DELETE_APPLICATION_FORMS_AWARDWON_FAILURE":
        this.setState({
          status: false,
          msg: "No Record Deleted",
          showLoader: true
        });
        break;
    }
  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
  }

  render() {
    const formConfig = this.state.configFieldsData;

    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <article className="form">
          <article className="row">
            <article className="col-md-12 subheading">
              Have you won any awards?
              <span>&nbsp;</span>
            </article>
            {this.state.AwardWonGetListByUser != null
              ? this.state.AwardWonGetListByUser.map(res => {
                return (
                  <article className="col-md-12 " key={res.id}>
                    <article className="graystrip">
                      <h2>{res.name}</h2>
                      <span>{res.level}</span>
                      <span>{res.year}</span>
                      <span>{res.source}</span>
                      <span>{res.description}</span>
                      <article className="pull-right">
                        <span
                          className="edit"
                          onClick={event => this.editAwardWonVal(res)}
                        >
                          &nbsp;
                          </span>
                        <span
                          className="delete"
                          onClick={this.removeAwardWon.bind(this, res.id)}
                        >
                          &nbsp;
                          </span>
                      </article>
                    </article>
                  </article>
                );
              })
              : ""}
          </article>
          {this.state.isFormButtonShow ? (
            <article className="row">
              <article className="col-md-12">
                <a onClick={this.showForm} className="btn-yellow pull-left">
                  Add Awards +
                </a>
              </article>
            </article>
          ) : (
              ""
            )}

          {!this.state.isFormButtonShow ? (
            <article>
              <article className="paddingborder">
                <article className="border">&nbsp;</article>
              </article>

              <article>
                <section>
                  <article className="row">
                    {formConfig.name != null && formConfig.name.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="name"
                            placeholder={`${formConfig.name.label}`}
                            data-custom={formConfig.name.custom}
                            onChange={this.formChangeHandler}
                            value={this.state.formData.name || ""}
                          />
                          <label>
                            {gblFunc.capitalText(formConfig.name.label)}
                          </label>
                        </article>
                      </article>
                    ) : (
                        ""
                      )}
                    {this.state.validations["name"] ? (
                      <span className="error animated bounce">
                        {this.state.validations["name"]}
                      </span>
                    ) : null}

                    {formConfig.source != null && formConfig.source.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <select
                            id="source"
                            className="icon"
                            onChange={this.formChangeHandler.bind(this)}
                            value={this.state.formData.source || ""}
                          >
                            <option value="">Select Source</option>
                            {formConfig.source.dataOptions != null ? (
                              formConfig.source.dataOptions.map(res => {
                                return (
                                  <option key={res} value={res}>
                                    {res}
                                  </option>
                                );
                              })
                            ) : this.state.getSource != null ? (
                              this.state.getSource.map(res => {
                                return (
                                  <option key={res} value={res}>
                                    {res}
                                  </option>
                                );
                              })
                            ) : (
                                  <option>Source</option>
                                )}
                          </select>
                          <label>
                            {gblFunc.capitalText(formConfig.source.label)}
                          </label>
                        </article>
                      </article>
                    ) : (
                        ""
                      )}
                    {this.state.validations["source"] ? (
                      <span className="error animated bounce">
                        {this.state.validations["source"]}
                      </span>
                    ) : null}

                    {formConfig.year != null && formConfig.year.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <select
                            id="year"
                            className="icon"
                            onChange={this.formChangeHandler.bind(this)}
                            value={this.state.formData.year || ""}
                          >
                            <option value="">Select Year</option>
                            {formConfig.year.dataOptions != null ? (
                              formConfig.year.dataOptions.map(res => {
                                return (
                                  <option key={res} value={res}>
                                    {res}
                                  </option>
                                );
                              })
                            ) : yearArray != null ? (
                              yearArray.map(res => {
                                return (
                                  <option key={res} value={res.toString()}>
                                    {res}
                                  </option>
                                );
                              })
                            ) : (
                                  <option>Year</option>
                                )}
                          </select>
                          <label>
                            {gblFunc.capitalText(formConfig.year.label)}
                          </label>
                        </article>
                      </article>
                    ) : (
                        ""
                      )}

                    {this.state.validations["year"] ? (
                      <span className="error animated bounce">
                        {this.state.validations["year"]}
                      </span>
                    ) : null}
                  </article>

                  {formConfig.description != null &&
                    formConfig.description.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="description"
                            placeholder={`${formConfig.description.label}`}
                            data-custom={formConfig.description.custom}
                            onChange={this.formChangeHandler}
                            value={this.state.formData.description || ""}
                          />
                          <label>
                            {gblFunc.capitalText(formConfig.description.label)}
                          </label>
                        </article>
                      </article>
                    ) : (
                      ""
                    )}

                  {this.state.validations["description"] ? (
                    <span className="error animated bounce">
                      {this.state.validations["description"]}
                    </span>
                  ) : null}

                  <article className="row">
                    <article className="col-md-12">
                      <input
                        type="submit"
                        value="Save"
                        className="btn  pull-right"
                        onClick={this.submit}
                      />
                    </article>
                  </article>
                </section>
              </article>
            </article>
          ) : (
              ""
            )}
        </article>
      </section>

      /*       <section>
                <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <article className="form">
          <article className="row">
            <article className="col-md-12 subheading">
              Have you won any awards?
              <span>&nbsp;</span>
            </article>
            <article className="col-md-12 ">
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th>Award Name</th>
                    <th>Type</th>
                    <th>Year</th>
                    <th>Description</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Honeywell sports award</td>
                    <td>Private</td>
                    <td>2008</td>
                    <td> Lorem Ipsum is simply dummy.</td>
                    <td>
                      {" "}
                      <span className="edit">&nbsp;</span>
                      <span className="delete">&nbsp;</span>
                    </td>
                  </tr>
                  <tr>
                    <td>Honeywell sports award</td>
                    <td>Private</td>
                    <td>2008</td>
                    <td> Lorem Ipsum is simply dummy .</td>
                    <td>
                      {" "}
                      <span className="edit">&nbsp;</span>
                      <span className="delete">&nbsp;</span>
                    </td>
                  </tr>
                  <tr>
                    <td>Honeywell sports award</td>
                    <td>Private</td>
                    <td>2008</td>
                    <td> Lorem Ipsum is simply dummy .</td>
                    <td>
                      {" "}
                      <span className="edit">&nbsp;</span>
                      <span className="delete">&nbsp;</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </article>
          </article>
          <article className="paddingborder">
            <article className="border">&nbsp;</article>
          </article>
          <article>
            <section>
              <article className="row">
                <article className="col-md-4">
                  <article className="form-group">
                    <input type="text" id="Award name" required />
                    <label for="Award name">Award name*</label>
                  </article>
                </article>
                <article className="col-md-4">
                  <article className="form-group">
                    <select className="icon">
                      <option>Award Source</option>
                    </select>
                    <label className="selectLabel">Award Source*</label>
                  </article>
                </article>
                <article className="col-md-4">
                  <article className="form-group">
                    <select className="icon">
                      <option>2016</option>
                    </select>
                    <label className="selectLabel">
                      Year in which you have received*
                    </label>
                  </article>
                </article>
              </article>
              <article className="row">
                <article className="col-md-4">
                  <article className="form-group">
                    <input type="text" id="Description" required />
                    <label for="Description">Description</label>
                  </article>
                </article>

              </article>

              <article className="row">
                <article className="col-md-12">
                  <a className="btn-yellow pull-left">Add award +</a>
                  <input
                    type="submit"
                    value="Save"
                    className="btn pull-right"
                  />
                  <input
                    type="submit"
                    value="Skip"
                    className="btn pull-right marginRight"
                  />
                </article>
              </article>
            </section>
          </article>
        </article>
      </section> */
    );
  }
}

export default AwardsWon;
