import React from "react";

const hideBoard = [
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  12,
  21,
  22,
  23,
  24,
  25,
  577,
  752
];

const elementryClass = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 25, 752];
const highSchoolClass = [16, 20];
const graduationClass = [22, 23, 24, 21];
const elevenClass = [12];
const othersClass = [577];

const hideSubject = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 25, 577, 752];

const editAcadmicClasses = ({
  allRules,
  classID,
  boardList,
  onDistrictHandler,
  district,
  subject,
  year,
  month,
  marksOrCgpa,
  customData,
  userAcademicInfo,
  onEducationSubmitHandler,
  userInstituteInfo,
  educationFormHandler,
  marksObtainedHandler
}) => {
  let className = null;

  if (
    userAcademicInfo.academicClass &&
    userAcademicInfo.academicClass.id &&
    allRules &&
    allRules.rulesData &&
    allRules.rulesData["class"]
  ) {
    className = allRules.rulesData["class"]
      .map(classObj => classObj)
      .filter(classObj => classObj.id == userAcademicInfo.academicClass.id);
  }

  return (
    <section>
      <form
        onSubmit={event => onEducationSubmitHandler(event)}
        autocomplete="off"
      >
        <article className="row">
          <article className="col-md-4">
            <select className="icon" value={userAcademicInfo.academicClass.id}>
              {className && className.length > 0
                ? className.map(cl => (
                    <option key={cl.id} value={cl.id}>
                      {cl.rulevalue}
                    </option>
                  ))
                : null}
            </select>
          </article>

          <article className="col-md-4">
            <select
              className="icon"
              value={userInstituteInfo.state}
              onChange={event => onDistrictHandler(event)}
            >
              <option value="">State</option>
              {allRules &&
                allRules.rulesData &&
                allRules.rulesData["state"].map(stateObj => (
                  <option key={stateObj.id} value={stateObj.id}>
                    {stateObj.rulevalue}
                  </option>
                ))}
            </select>
          </article>
          <article className="col-md-4">
            <select
              className="icon"
              value={userInstituteInfo.district}
              onClick={event =>
                educationFormHandler(event, "userInstituteInfo", "district")
              }
            >
              <option value="">District*</option>
              {district && district.length > 0
                ? district.map(dst => (
                    <option key={dst.id} value={dst.id}>
                      {dst.districtName}
                    </option>
                  ))
                : null}
            </select>
          </article>
        </article>
        <article className="row">
          {hideBoard.indexOf(parseInt(classID)) > -1 ? null : (
            <article className="col-md-4">
              <select
                className="icon"
                value={userAcademicInfo.board}
                onChange={event =>
                  educationFormHandler(event, "userAcademicInfo", "board")
                }
              >
                <option>Board*</option>
                {boardList &&
                  boardList.length > 0 &&
                  boardList.map(boardObj => (
                    <option key={boardObj.id} value={boardObj.id}>
                      {boardObj.name}
                    </option>
                  ))}
              </select>
            </article>
          )}

          <article className="col-md-4">
            {subject && subject.length ? (
              <select
                className="icon"
                value={userAcademicInfo.stream}
                onChange={event =>
                  educationFormHandler(event, "userAcademicInfo", "stream")
                }
              >
                <option value="">Subject*</option>
                {subject && subject.length > 0
                  ? subject.map(subObj => (
                      <option key={subObj.id} value={subObj.id}>
                        {subObj.rulevalue}
                      </option>
                    ))
                  : null}
              </select>
            ) : null}
          </article>
        </article>
        <article className="row">
          <article className="col-md-4">
            <input
              type="text"
              placeholder="City*"
              value={userInstituteInfo.city}
              autofill={null}
              onChange={event =>
                educationFormHandler(event, "userInstituteInfo", "city")
              }
            />
          </article>
          <article className="col-md-4">
            <input
              type="text"
              placeholder="Institute name*"
              value={userInstituteInfo.instituteName}
              onChange={event =>
                educationFormHandler(
                  event,
                  "userInstituteInfo",
                  "instituteName"
                )
              }
            />
          </article>
          <article className="col-md-4">
            <article className="row">
              <article className="col-md-6">
                <select
                  className="icon"
                  value={userAcademicInfo.passingYear}
                  onChange={event =>
                    educationFormHandler(
                      event,
                      "userAcademicInfo",
                      "passingYear"
                    )
                  }
                >
                  <option value="">Year*</option>
                  {year.map(y => (
                    <option key={y} value={y}>
                      {y}
                    </option>
                  ))}
                </select>
              </article>
              <article className="col-md-6">
                <select
                  className="icon"
                  value={userAcademicInfo.passingMonth}
                  onChange={event =>
                    educationFormHandler(
                      event,
                      "userAcademicInfo",
                      "passingMonth"
                    )
                  }
                >
                  <option value="">Month*</option>
                  {month.map(m => (
                    <option key={m} value={m}>
                      {m}
                    </option>
                  ))}
                </select>
              </article>
            </article>
          </article>
        </article>

        <article className="row">
          <article className="col-md-12 address">Marks details</article>
          <article className="col-md-12 radio-btn">
            <span>
              <label>
                <input
                  type="radio"
                  name="radio"
                  value="1"
                  checked={userAcademicInfo.markingType === "1"}
                  onChange={event => marksObtainedHandler(event)}
                />
                <span />
                <i>Marks</i>
              </label>
            </span>
            <span className="marginleft">
              <label>
                <input
                  type="radio"
                  value="2"
                  name="radio"
                  checked={userAcademicInfo.markingType === "2"}
                  onChange={event => marksObtainedHandler(event)}
                />
                <span />
                <i>CGPA</i>
              </label>
            </span>
          </article>
        </article>
        {userAcademicInfo.markingType == 1 ? (
          <article className="row">
            <article className="col-md-4">
              <input
                type="text"
                placeholder="Marks obtained"
                value={userAcademicInfo.marksObtained}
                onChange={event =>
                  educationFormHandler(
                    event,
                    "userAcademicInfo",
                    "marksObtained"
                  )
                }
              />
            </article>
            <article className="col-md-4">
              <input
                type="text"
                placeholder="Total Marks"
                value={userAcademicInfo.totalMarks}
                onChange={event =>
                  educationFormHandler(event, "userAcademicInfo", "totalMarks")
                }
              />
            </article>
          </article>
        ) : (
          <article className="row">
            <article className="col-md-4">
              <select
                className="icon"
                value={userAcademicInfo.grade}
                onChange={event =>
                  educationFormHandler(event, "userAcademicInfo", "grade")
                }
              >
                <option value="">Grade*</option>
                <option value="A1">A1</option>
                <option value="A2">A2</option>
                <option value="B1">B1</option>
                <option value="B2">B2</option>
                <option value="C1">C1</option>
                <option value="C2">C2</option>
                <option value="D1">D1</option>
                <option value="D2">D2</option>
                <option value="E">E</option>
              </select>
            </article>
            <article className="col-md-4">
              <input
                type="text"
                placeholder="CGPA"
                value={userAcademicInfo.marksObtained}
                onChange={event =>
                  educationFormHandler(
                    event,
                    "userAcademicInfo",
                    "marksObtained"
                  )
                }
                value={userAcademicInfo.marksObtained}
              />
            </article>
          </article>
        )}

        <article className="row">
          <article className="col-md-12">
            <input type="submit" value="Save" className="btn  pull-right" />
          </article>
        </article>
      </form>
    </section>
  );
};

export default editAcadmicClasses;
