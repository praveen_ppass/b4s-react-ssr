import React, { Component } from "react";
class Class10 extends Component {
  render() {
    return (
      <section>
        <article className="row">
          <article className="col-md-4">
            <select className="icon">
              <option>Class 10</option>
            </select>
          </article>
          <article className="col-md-4">
            <input type="text" placeholder="School name*" />
          </article>
          <article className="col-md-4">
            <select className="icon">
              <option>State</option>
            </select>
          </article>
        </article>

        <article className="row">
          <article className="col-md-4">
            <select className="icon">
              <option>District*</option>
            </select>
          </article>
          <article className="col-md-4">
            <input type="text" placeholder="City*" />
          </article>
          <article className="col-md-4">
            <select className="icon">
              <option>Subject*</option>
            </select>
          </article>
        </article>
        <article className="row">
          <article className="col-md-4">
            <select className="icon">
              <option>Board*</option>
            </select>
          </article>
          <article className="col-md-4">
            <input
              type="text"
              placeholder="Completion month and year*"
              className="icon-date"
            />
          </article>
        </article>
        <article className="row">
          <article className="col-md-12 address">Marks details</article>
          <article className="col-md-12 radio-btn">
            <span>
              <label>
                <input type="radio" name="radio" checked={true} />
                <span />
                <i>Marks</i>
              </label>
            </span>
            <span className="marginleft">
              <label>
                <input type="radio" name="radio" />
                <span />
                <i>CGPA</i>
              </label>
            </span>
          </article>
        </article>
        <article className="row">
          <article className="col-md-4">
            <input type="text" placeholder="Marks obtained" />
          </article>
          <article className="col-md-4">
            <input type="text" placeholder="Total Marks" />
          </article>
        </article>
        <article className="row">
          <article className="col-md-12">
            <input type="submit" value="Save" className="btn  pull-right" />
          </article>
          <article className="col-md-12">
            <a className="btn-yellow">Add Class 12 +</a>
          </article>
        </article>
      </section>
    );
  }
}

export default Class10;
