import React, { Component } from "react";
class GraduationForm extends Component {
  render() {
    return (
      <section>
        {/* <article className="row">
          <article className="col-md-12 ">
            <article className="graystrip">
              <h2>Graduation</h2>
              <span>Kirodimal College</span>
              <span>Delhi</span>
              <span>New Delhi</span>
              <span>Delhi</span>
              <span>Bachelor or arts</span>
              <span>June, 2015</span>
              <span>698/550</span>

              <article className="pull-right">
                <span className="edit">&nbsp;</span>
                <span className="delete">&nbsp;</span>
              </article>
            </article>
          </article>
        </article> */}

        <article className="row">
          <article className="col-md-4">
            <select className="icon">
              <option>Graduation</option>
            </select>
          </article>
          <article className="col-md-4">
            <input type="text" placeholder="Institute name*" />
          </article>
          <article className="col-md-4">
            <select className="icon">
              <option>State</option>
            </select>
          </article>
        </article>

        <article className="row">
          <article className="col-md-4">
            <select className="icon">
              <option>District*</option>
            </select>
          </article>
          <article className="col-md-4">
            <input type="text" placeholder="City*" />
          </article>
          <article className="col-md-4">
            <select className="icon">
              <option>Courses*</option>
            </select>
          </article>
        </article>
        <article className="row">
          <article className="col-md-4">
            <input
              type="text"
              placeholder="Completion month and year*"
              className="icon-date"
            />
          </article>
        </article>
        <article className="row">
          <article className="col-md-12 address">Marks details*</article>
          <article className="col-md-12 radio-btn">
            <span>
              <label>
                <input type="radio" name="radio" checked={true} />
                <span />
                <i>Marks</i>
              </label>
            </span>
            <span className="marginleft">
              <label>
                <input type="radio" name="radio" />
                <span />
                <i>CGPA</i>
              </label>
            </span>
          </article>
        </article>
        <article className="row">
          <article className="col-md-4">
            <input type="text" placeholder="Marks obtained" />
          </article>
          <article className="col-md-4">
            <input type="text" placeholder="Total Marks" />
          </article>
        </article>
        <article className="row">
          <article className="col-md-12">
            <input type="submit" value="Save" className="btn  pull-right" />
          </article>
        </article>
      </section>
    );
  }
}

export default GraduationForm;
