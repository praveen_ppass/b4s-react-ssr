import React, { Component } from "react";
import ValidationError from "../../../../components/validationError";
import OtherTextField from "../../otherTextField";
import {
  otherDistrictId,
  otherStateId,
  otherBoardId,
  graduationId,
  otherStreamIdGraduation,
  otherStreamIdTwelfth
} from "../../../formconfig";
const hideBoard = [
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  12,
  21,
  22,
  23,
  24,
  25,
  577,
  752
];

const elementryClass = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 25, 752];
const highSchoolClass = [16, 20];
const graduationClass = [22, 23, 24, 21];
const elevenClass = [12];
const othersClass = [577];

const hideSubject = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 25, 577, 752];

const acadmicClasses = ({
  allRules,
  classID,
  boardList,
  onDistrictHandler,
  districtObj,
  subject,
  year,
  month,
  marksOrCgpa,
  customData,
  userAcademicInfo,
  onEducationSubmitHandler,
  userInstituteInfo,
  educationFormHandler,
  marksObtainedHandler,
  educationDataApi,
  isEditable,
  validations,
  type,
  applicationStep
}) => {
  let className = null;

  if (!isEditable) {
    if (
      classID &&
      allRules &&
      allRules.rulesData &&
      allRules.rulesData["class"]
    ) {
      className = allRules.rulesData["class"]
        .map(classObj => classObj)
        .filter(classObj => classObj.id == parseInt(classID, 10));
    }
  } else {
    if (
      userAcademicInfo.academicClass &&
      userAcademicInfo.academicClass.id &&
      allRules &&
      allRules.rulesData &&
      allRules.rulesData["class"]
    ) {
      className = allRules.rulesData["class"]
        .map(classObj => classObj)
        .filter(classObj => classObj.id == userAcademicInfo.academicClass.id);
    }
  }

  return (
    <article>
      <article className="col-md-12 subheading ">
        Academic history
        <span>Please fill your education details</span>
      </article>
      <section className="padding30">
        <form autocomplete="off">
          <article className="row">
            {/* <article className="row">
          <article className="col-md-12 ">
            <article className="graystrip">
              <h2>Graduation</h2>
              <span>Kirodimal College</span>
              <span>Delhi</span>
              <span>New Delhi</span>
              <span>Delhi</span>
              <span>Bachelor or arts</span>
              <span>June, 2015</span>
              <span>698/550</span>

              <article className="pull-right">
                <span className="edit">&nbsp;</span>
                <span className="delete">&nbsp;</span>
              </article>
            </article>
          </article>
        </article> */}
            {educationDataApi["academic_class"].active ? (
              <article className="col-md-4 ">
                <article className="form-group">
                  <select
                    className="icon"
                    id="academicClass"
                    value={userAcademicInfo.academicClass.id}
                  >
                    <option value="">-Select-</option>
                    {className && className.length > 0
                      ? className.map(cl => (
                          <option key={cl.id} value={cl.id}>
                            {cl.rulevalue}
                          </option>
                        ))
                      : null}
                  </select>

                  <label className="labelstyle">
                    {educationDataApi["academic_class"].label}
                  </label>
                  <ValidationError
                    fieldValidation={validations["academicClass"]}
                  />
                </article>
              </article>
            ) : null}
            {educationDataApi["state"].active ? (
              <span>
                <article className="col-md-4">
                  <article className="form-group">
                    <select
                      className="icon"
                      id="state"
                      value={userInstituteInfo.state}
                      onChange={event => onDistrictHandler(event)}
                    >
                      <option value="">-Select-</option>

                      {educationDataApi.state.dataOptions != null
                        ? educationDataApi.state.dataOptions.map(res => {
                            return (
                              <option key={res.id} value={res.id}>
                                {res.rulevalue}
                              </option>
                            );
                          })
                        : allRules && allRules.rulesData
                          ? allRules.rulesData["state"].map(stateObj => {
                              return (
                                <option key={stateObj.id} value={stateObj.id}>
                                  {stateObj.rulevalue}
                                </option>
                              );
                            })
                          : null}
                      {/* {educationDataApi.state.dataOptions != null ?  
                  educationDataApi.state.dataOptions.map(stateObj => <option key={stateObj.id} value={stateObj.id}>
                    {stateObj.rulevalue}
                  </option>)
                  : allRules &&
                  allRules.rulesData &&
                  allRules.rulesData["state"].map(stateObj => (
                    <option key={stateObj.id} value={stateObj.id}>
                      {stateObj.rulevalue}
                    </option> } */}
                    </select>
                    <label className="labelstyle">
                      {educationDataApi["state"].label}
                    </label>
                    <ValidationError fieldValidation={validations["state"]} />
                  </article>
                </article>
                {userInstituteInfo.state == otherStateId ? (
                  <OtherTextField
                    custom={educationDataApi["otherState"].custom}
                    fieldValue={userInstituteInfo.otherState}
                    fieldId="otherState"
                    clickHandler={event =>
                      educationFormHandler(
                        event,
                        "userInstituteInfo",
                        "otherState"
                      )
                    }
                    fieldLabel={educationDataApi["otherState"].label}
                    fieldValidation={validations["otherState"]}
                  />
                ) : null}
              </span>
            ) : null}

            {educationDataApi["district"].active ? (
              <span>
                <article className="col-md-4">
                  <article className="form-group">
                    <select
                      className="icon"
                      value={userInstituteInfo.district}
                      id="district"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userInstituteInfo",
                          "district"
                        )
                      }
                    >
                      <option value="">-Select-</option>
                      {educationDataApi.district.dataOptions != null
                        ? educationDataApi.district.dataOptions.map(dst => {
                            return (
                              <option key={dst.id} value={dst.id}>
                                {dst.districtName}
                              </option>
                            );
                          })
                        : districtObj && districtObj.length > 0
                          ? districtObj.map(dst => {
                              return (
                                <option key={dst.id} value={dst.id}>
                                  {dst.districtName}
                                </option>
                              );
                            })
                          : null}
                      {/* {district && district.length > 0
                  ? district.map(dst => (
                      <option key={dst.id} value={dst.id}>
                        {dst.districtName}
                      </option>
                    ))
                  : null} */}
                    </select>
                    <label className="labelstyle">
                      {educationDataApi["district"].label}
                    </label>
                    <ValidationError
                      fieldValidation={validations["district"]}
                    />
                  </article>
                </article>
                {userInstituteInfo.district == otherDistrictId ? (
                  <OtherTextField
                    custom={educationDataApi["otherDistrict"].custom}
                    fieldValue={userInstituteInfo.otherDistrict}
                    fieldId="otherDistrict"
                    clickHandler={event =>
                      educationFormHandler(
                        event,
                        "userInstituteInfo",
                        "otherDistrict"
                      )
                    }
                    fieldLabel={educationDataApi["otherDistrict"].label}
                    fieldValidation={validations["otherDistrict"]}
                  />
                ) : null}
              </span>
            ) : null}

            {hideBoard.indexOf(parseInt(classID)) >
            -1 ? null : educationDataApi["board"].active ? (
              <span>
                <article className="col-md-4">
                  <article className="form-group">
                    <select
                      className="icon"
                      value={userAcademicInfo.board}
                      id="board"
                      onChange={event =>
                        educationFormHandler(event, "userAcademicInfo", "board")
                      }
                    >
                      <option value="">-Select-</option>
                      {educationDataApi.board.dataOptions != null
                        ? educationDataApi.board.dataOptions.map(boardObj => (
                            <option key={boardObj.id} value={boardObj.id}>
                              {boardObj.name}
                            </option>
                          ))
                        : boardList && boardList.length > 0
                          ? boardList.map(boardObj => (
                              <option key={boardObj.id} value={boardObj.id}>
                                {boardObj.name}
                              </option>
                            ))
                          : null}
                      {/* {boardList &&
                    boardList.length > 0 &&
                    boardList.map(boardObj => (
                      <option key={boardObj.id} value={boardObj.id}>
                        {boardObj.name}
                      </option>
                    ))} */}
                    </select>
                    <label className="labelstyle">
                      {educationDataApi["board"].label}
                    </label>
                    <ValidationError fieldValidation={validations["board"]} />
                  </article>
                </article>
                {userAcademicInfo.board == "31" ||
                userAcademicInfo.board == "37" ? (
                  <OtherTextField
                    custom={educationDataApi["board"].custom}
                    fieldValue={userAcademicInfo.otherBoard}
                    fieldId="otherBoard"
                    clickHandler={event =>
                      educationFormHandler(
                        event,
                        "userAcademicInfo",
                        "otherBoard"
                      )
                    }
                    fieldLabel={educationDataApi["otherBoard"].label}
                    fieldValidation={validations["otherBoard"]}
                  />
                ) : null}
              </span>
            ) : null}

            {educationDataApi["stream"].active && subject && subject.length ? (
              <span>
                <article className="col-md-4">
                  <article className="form-group">
                    <select
                      className="icon"
                      value={userAcademicInfo.stream}
                      id="stream"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "stream"
                        )
                      }
                    >
                      <option value="">-Select-</option>
                      {educationDataApi.stream.dataOptions != null
                        ? educationDataApi.stream.dataOptions.map(subObj => (
                            <option key={subObj.id} value={subObj.id}>
                              {subObj.rulevalue}
                            </option>
                          ))
                        : subject && subject.length > 0
                          ? subject.map(subObj => (
                              <option key={subObj.id} value={subObj.id}>
                                {subObj.rulevalue}
                              </option>
                            ))
                          : null}
                      {/* {subject && subject.length > 0
                    ? subject.map(subObj => (
                        <option key={subObj.id} value={subObj.id}>
                          {subObj.rulevalue}
                        </option>
                      ))
                    : null} */}
                    </select>

                    <label className="labelstyle">
                      {" "}
                      {educationDataApi["stream"].label}
                    </label>
                    <ValidationError fieldValidation={validations["stream"]} />
                  </article>
                </article>
                {userAcademicInfo.stream == otherStreamIdTwelfth ||
                userAcademicInfo.stream == otherStreamIdGraduation ? (
                  <OtherTextField
                    custom={educationDataApi["otherStream"].custom}
                    fieldValue={userAcademicInfo.otherStream}
                    fieldId="otherStream"
                    clickHandler={event =>
                      educationFormHandler(
                        event,
                        "userAcademicInfo",
                        "otherStream"
                      )
                    }
                    fieldLabel={educationDataApi["otherStream"].label}
                    fieldValidation={validations["otherStream"]}
                  />
                ) : null}
              </span>
            ) : null}
            {/* <article className="col-md-4">
          <select className="icon">
            <option value="">Courses*</option>
            {allRules &&
              allRules.rulesData &&
              allRules.rulesData["course"].map(courseObj => (
                <option key={courseObj.id}>{courseObj.rulevalue}</option>
              ))}
          </select>
        </article> */}
            {educationDataApi["city"].active ? (
              <article className="col-md-4">
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={educationDataApi["city"].custom}
                    value={userInstituteInfo.city}
                    id="city"
                    onChange={event =>
                      educationFormHandler(event, "userInstituteInfo", "city")
                    }
                    autocomplete="false"
                    required
                  />
                  <label>{educationDataApi["city"].label}</label>
                  <ValidationError fieldValidation={validations["city"]} />
                </article>
              </article>
            ) : null}
            {educationDataApi["institute_name"].active ? (
              <article className="col-md-4">
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={educationDataApi["institute_name"].custom}
                    value={userInstituteInfo.instituteName}
                    id="instituteName"
                    onChange={event =>
                      educationFormHandler(
                        event,
                        "userInstituteInfo",
                        "instituteName"
                      )
                    }
                    required
                  />
                  <label>{educationDataApi["institute_name"].label}</label>
                  <ValidationError
                    fieldValidation={validations["instituteName"]}
                  />
                </article>
              </article>
            ) : null}
            {educationDataApi["passing_year"].active &&
            classID != graduationId ? (
              <article className="col-md-4">
                <article className="form-group">
                  <select
                    className="icon"
                    value={userAcademicInfo.passingYear}
                    id="passingYear"
                    onChange={event =>
                      educationFormHandler(
                        event,
                        "userAcademicInfo",
                        "passingYear"
                      )
                    }
                  >
                    <option value="">-Select-</option>
                    {educationDataApi.passing_year.dataOptions != null
                      ? educationDataApi.passing_year.dataOptions.map(y => (
                          <option key={y} value={y}>
                            {y}
                          </option>
                        ))
                      : year
                        ? year.map(y => (
                            <option key={y} value={y}>
                              {y}
                            </option>
                          ))
                        : null}
                    {/* {year.map(y => (
                      <option key={y} value={y}>
                        {y}
                      </option>
                    ))} */}
                  </select>
                  <label className="labelstyle">
                    {educationDataApi["passing_year"].label}
                  </label>
                  <ValidationError
                    fieldValidation={validations["passingYear"]}
                  />
                </article>
              </article>
            ) : null}
            {educationDataApi["passing_month"].active &&
            classID != graduationId ? (
              <article className="col-md-4">
                <article className="form-group">
                  <select
                    className="icon"
                    id="passingMonth"
                    value={userAcademicInfo.passingMonth}
                    onChange={event =>
                      educationFormHandler(
                        event,
                        "userAcademicInfo",
                        "passingMonth"
                      )
                    }
                  >
                    <option value="">-Select-</option>
                    {educationDataApi.passing_month.dataOptions != null
                      ? educationDataApi.passing_month.dataOptions.map(m => (
                          <option key={m} value={m}>
                            {m}
                          </option>
                        ))
                      : month
                        ? month.map(m => (
                            <option key={m} value={m}>
                              {m}
                            </option>
                          ))
                        : null}

                    {/* {month.map(m => (
                      <option key={m} value={m}>
                        {m}
                      </option>
                    ))} */}
                  </select>
                  <label className="labelstyle">
                    {educationDataApi["passing_month"].label}
                  </label>
                  <ValidationError
                    fieldValidation={validations["passingMonth"]}
                  />
                </article>
              </article>
            ) : null}
            {/* <input
            type="text"
            placeholder="Completion month and year*"
            className="icon-date"
          /> */}
          </article>

          {
            /* classID != graduationId ? ( */
            <article className="row paddingtop0">
              <article className="col-md-12 address">Marks details</article>
              {educationDataApi["marking_type"].active && (
                /*   classID != graduationId ? ( */
                <article className="col-md-12 radio-btn">
                  <MARKS
                    marksObtainedHandler={marksObtainedHandler}
                    ID={userAcademicInfo.academicClass.id}
                    TYPE={type ? true : false}
                    userAcademicInfo={userAcademicInfo}
                  />
                  {/* <span>
                    <label>
                      <input
                        type="radio"
                        name="radio"
                        value="1"
                        checked={userAcademicInfo.markingType === "1"}
                        onChange={event => marksObtainedHandler(event)}
                      />
                      <span />
                      &nbsp;&nbsp; <i>Marks</i>
                    </label>
                  </span>
                  <span className="marginleft">
                    <label>
                      <input
                        type="radio"
                        value="2"
                        name="radio"
                        checked={userAcademicInfo.markingType === "2"}
                        onChange={event => marksObtainedHandler(event)}
                      />
                      <span />
                      &nbsp;&nbsp;<i>CGPA</i>
                    </label>
                  </span> */}
                </article>
              )
              /*    ) : null */
              }
            </article>
            /*     ) : null */
          }

          {userAcademicInfo.markingType ==
          "1" /* && classID != graduationId */ ? (
            <article className="row">
              {educationDataApi["marks_obtained"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <input
                      type="text"
                      data-custom={educationDataApi["marks_obtained"].custom}
                      value={userAcademicInfo.marksObtained}
                      id="marksObtained"
                      maxLength="4"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "marksObtained",
                          "Marks Obtained"
                        )
                      }
                      required
                    />
                    <label>{educationDataApi["marks_obtained"].label}</label>
                    <ValidationError
                      fieldValidation={validations["marksObtained"]}
                    />
                  </article>
                </article>
              ) : null}
              {educationDataApi["total_marks"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <input
                      type="text"
                      maxLength="4"
                      data-custom={educationDataApi["total_marks"].custom}
                      value={userAcademicInfo.totalMarks}
                      id="totalMarks"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "totalMarks",
                          "Total marks"
                        )
                      }
                      required
                    />
                    <label>{educationDataApi["total_marks"].label}</label>
                    <ValidationError
                      fieldValidation={validations["totalMarks"]}
                    />
                  </article>
                </article>
              ) : null}
            </article>
          ) : (
            <article className="row">
              {educationDataApi["grade"]
                .active /* && classID != graduationId */ ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <select
                      className="icon"
                      id="grade"
                      value={userAcademicInfo.grade}
                      onChange={event =>
                        educationFormHandler(event, "userAcademicInfo", "grade")
                      }
                    >
                      <option value="">-Select-</option>
                      <option value="A1">A1</option>
                      <option value="A2">A2</option>
                      <option value="B1">B1</option>
                      <option value="B2">B2</option>
                      <option value="C1">C1</option>
                      <option value="C2">C2</option>
                      <option value="D1">D1</option>
                      <option value="D2">D2</option>
                      <option value="E">E</option>
                    </select>
                    <label className="labelstyle">
                      {educationDataApi["grade"].label}
                    </label>
                    <ValidationError fieldValidation={validations["grade"]} />
                  </article>
                </article>
              ) : null}
              {educationDataApi["marks_obtained"].active ? (
                /* && classID != graduationId  */ <article className="col-md-4">
                  <article className="form-group">
                    <input
                      type="text"
                      id="marksObtained"
                      data-custom={educationDataApi["marks_obtained"].custom}
                      value={userAcademicInfo.marksObtained}
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "marksObtained"
                        )
                      }
                      required
                      value={userAcademicInfo.marksObtained}
                    />
                    <label>CGPA</label>
                    <ValidationError
                      fieldValidation={validations["marksObtained"]}
                    />
                  </article>
                </article>
              ) : null}
            </article>
          )}

          <article className="row">
            <article className="col-md-12">
              <input
                type="button"
                value="Save"
                className="btn  pull-right"
                onClick={event => onEducationSubmitHandler(event)}
              />
            </article>
          </article>
        </form>
      </section>
    </article>
  );
};

const MARKS = ({ TYPE, ID, userAcademicInfo, marksObtainedHandler }) => {
  let showMarkType = null;
  if (ID == 11 && TYPE) {
    showMarkType = (
      <span>
        <label>
          <input
            type="radio"
            name="radio"
            value="1"
            checked={userAcademicInfo.markingType == "1"}
            onChange={event => marksObtainedHandler(event)}
          />
          <span />
          &nbsp;&nbsp; <i>Marks</i>
        </label>
      </span>
    );
  } else if (ID == 12 && TYPE) {
    showMarkType = (
      <span>
        <label>
          <input
            type="radio"
            name="radio"
            value="2"
            checked={userAcademicInfo.markingType == "2"}
            onChange={event => marksObtainedHandler(event)}
          />
          <span />
          &nbsp;&nbsp; <i>CGPA</i>
        </label>
      </span>
    );
  } else if (ID == 16 && TYPE) {
    showMarkType = (
      <span>
        <label>
          <input
            type="radio"
            name="radio"
            value="2"
            checked={userAcademicInfo.markingType == "2"}
            onChange={event => marksObtainedHandler(event)}
          />
          <span />
          &nbsp;&nbsp; <i>CGPA</i>
        </label>
      </span>
    );
  } else {
    showMarkType = (
      <article>
        <span>
          <label>
            <input
              type="radio"
              name="radio"
              value="1"
              checked={userAcademicInfo.markingType == "1"}
              onChange={event => marksObtainedHandler(event)}
            />
            <span />
            &nbsp;&nbsp; <i>Marks</i>
          </label>
        </span>
        <span className="marginleft">
          <label>
            <input
              type="radio"
              value="2"
              name="radio"
              checked={userAcademicInfo.markingType == "2"}
              onChange={event => marksObtainedHandler(event)}
            />
            <span />
            &nbsp;&nbsp;<i>CGPA</i>
          </label>
        </span>
      </article>
    );
  }

  return showMarkType;
};

export default acadmicClasses;
