import React, { Component } from "react";
import gblFunc from "../../../../globals/globalFunctions";
import { mapValidationFunc } from "../../../../validation/rules";
import { ruleRunner } from "../../../../validation/ruleRunner";
import AlertMessage from "../../../common/components/alertMsg";
import Loader from "../../../common/components/loader";
import ConfirmMessagePopup from "../../../common/components/confirmMessagePopup";
import { Route, Link, Redirect } from "react-router-dom";
import { camelCase, snakeCase } from "../../../../constants/constants";
import { config, childEduBSID } from "../../formconfig";
import moment from "moment";
import DatePicker from "react-datepicker";

const MIN_REFERENCES_ONE = {
  HEC6: "* Please add minimum one reference",
  TSL1: "* Please add minimum one reference",
  SSE4: "* Please enter the details of your MTAS/ FM",
  LIF9:
    "* Please add minimum two references (Your references cannot be family members)",
  LIF2:
    "* Please add minimum two references (Your references cannot be family members)"
};

class Reference extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bankSection: false,
      isFormButtonShow: true,
      configFieldsData: {},
      showLoader: false,
      msg: "",
      status: "",
      relation: null,
      referenceGetList: null,
      occupation: null,
      activeTab: null,
      formData: {
        address: null,
        mobile: null,
        name: null,
        occupation: null,
        other_occupation: null,
        other_relation: null,
        relation: null,
        customData: {},
        id: null
      },
      validations: {},
      showConfirmationPopup: false,
      deleteSuccessCallBack: null,
      isNext: false,
      isOtherOccupation: false,
      isOtherRelation: false,
      isCompleted: false,
      isRemoved: false,
      minEntry: 1, // MIN ENTRY SHOULD BE LENGHT - 1 LIKE MAX 1 THEN PASS 0,
      isNextVisible: false
    };
    this.showForm = this.showForm.bind(this);
    this.formChangeHandler = this.formChangeHandler.bind(this);
    this.submit = this.submit.bind(this);
    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.hideConfirmationPopup = this.hideConfirmationPopup.bind(this);
    this.formClose = this.formClose.bind(this);
    this.goNext = this.goNext.bind(this);
  }

  formClose() {
    this.setState({
      isFormButtonShow: true,
      activeTab: null
    });
  }

  goNext() {
    this.setState({
      bankSection: true
    }, () => {
      this.props.updateReferenceStep({
        userId: gblFunc.getStoreUserDetails()["userId"],
        scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
        isPosted: true
      })
    })
  }

  showForm() {
    const { match } = this.props;
    let employeeId = null;
    if (
      match &&
      match.params &&
      match.params.bsid &&
      childEduBSID[match.params.bsid.toUpperCase()]
    ) {
      const email = gblFunc.getStoreUserDetails()["email"];
      employeeId = email && email.split("_").length > 2 && email.split("_")[2].split("@")[0];
    }
    this.state.isFormButtonShow = !this.state.isFormButtonShow;
    this.setState({
      isFormButtonShow: this.state.isFormButtonShow,
      formData: {
        address: null,
        mobile: null,
        name: null,
        occupation: null,
        other_occupation: null,
        relation: null,
        other_relation: null,
        customData: {
          employeeId
        },
        id: null
      }
    });
  }

  submit() {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    let isPosted = false;

    if (this.state.isCompleted) {
      //CALL STEP POST FOR STEP HAS BEEN COMPLETED
      isPosted = true;
    }

    let isSubmit = this.checkFormValidations();
    if (isSubmit) {
      // for (let key in this.state.configFieldsData) {
      // 	if (this.state.configFieldsData[key].custom) {
      // 		this.state.formData.customData[key] = this.state.formData[key];
      // 	}
      // }

      let parseFinalData = camelCase(this.state.formData);
      this.setState({ formData: this.state.formData, isFormButtonShow: true });
      this.props.saveReferenceData({
        userId: userId,
        scholarshipId: scholarshipId,
        data: parseFinalData,
        isPosted: isPosted
      });
    }
  }

  componentDidMount() {
    this.updateArrayStepCompletedCall();
  }

  editReferenceVal(items) {
    let returnSnake = camelCase(items);
    const {
      address,
      mobile,
      name,
      occupation,
      other_occupation,
      other_relation,
      relation,
      customData,
      id
    } = returnSnake;
    const editData = {
      address,
      mobile,
      name,
      occupation,
      other_occupation,
      other_relation,
      relation,
      customData,
      id
    };
    let validations = { ...this.state.validations };
    if (editData.occupation == 12) {
      validations["other_occupation"] = null;
      this.setState({
        isOtherOccupation: true,
        validations: validations
      });
    } else {
      delete validations["other_occupation"];
      this.setState({
        isOtherOccupation: false,
        validations: validations
      });
    }

    if (editData.relation == 596) {
      validations["other_relation"] = null;
      this.setState({
        isOtherRelation: true,
        validations: validations
      });
    } else {
      delete validations["other_relation"];
      this.setState({
        isOtherRelation: false,
        validations: validations
      });
    }
    this.setState({
      isFormButtonShow: this.state.activeTab == editData.relation ? true : false,
      formData: editData,
      activeTab: this.state.activeTab == editData.relation ? null : editData.relation
    });
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    let validationRules = [];
    if (this.state.validations.hasOwnProperty(fieldID)) {
      let configFields = this.state.configFieldsData[fieldID];
      if (configFields != undefined && configFields.validations != null) {
        configFields.validations.map(res => {
          if (res != null) {
            let validator = mapValidationFunc(res);
            if (validator != undefined) validationRules.push(validator);
          }
        });
        validationObject.name = "*Field";
        validationObject.validationFunctions = validationRules;
      }
    }
    return validationObject;
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      const value = ["joiningDate", "employeeId", "storeDetails"].includes(key)
        ? this.state.formData.customData[key]
        : this.state.formData[key];
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      if (name != undefined && validationFunctions != undefined) {
        let validationResult = ruleRunner(
          value,
          key,
          name,
          ...validationFunctions
        );
        validations[key] = validationResult[key];
        if (validationResult[key] !== null) {
          isFormValid = false;
        }
      }
    }

    console.log(validations)

    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  removeReference(refId) {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }

    let isDeleted = false;
    if (this.state.isRemoved) {
      isDeleted = true;
    }
    const deleteSuccessCallBack = () =>
      this.props.deleteReferenceData({
        userId: userId,
        scholarshipId: scholarshipId,
        referenceId: refId,
        isDeleted: isDeleted
      });
    this.setState({
      showConfirmationPopup: true,
      deleteSuccessCallBack
    });
  }

  showConfirmationPopup() {
    this.setState({
      showConfirmationPopup: true
    });
  }

  hideConfirmationPopup() {
    this.setState({
      showConfirmationPopup: false,
      deleteSuccessCallBack: null
    });
  }

  formChangeHandler(event, dateId = undefined) {
    let value;
    let id;

    if (dateId) {
      value = moment(event).format("YYYY-MM-DD");
      id = dateId;
    } else {
      value = event.target.value;
      id = event.target.id;
    }

    let validations = { ...this.state.validations };
    //validate Other occupation data
    if (id == "occupation") {
      if (value == 12) {
        validations["other_occupation"] = null;
        this.setState({
          isOtherOccupation: true,
          validations: validations
        });
      } else {
        delete validations["other_occupation"];
        this.setState({
          isOtherOccupation: false,
          validations: validations
        });
      }
    }

    //validate Other relation data
    if (id == "relation") {
      if (value == 596) {
        validations["other_relation"] = null;
        this.setState({
          isOtherRelation: true,
          validations: validations
        });
      } else {
        delete validations["other_relation"];
        this.setState({
          isOtherRelation: false,
          validations: validations
        });
      }
    }
    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      if (name != undefined && validationFunctions != undefined) {
        const validationResult = ruleRunner(
          value,
          id,
          name,
          ...validationFunctions
        );
        validations[id] = validationResult[id];
      }
    }

    const updateFormData = { ...this.state.formData };
    if (["joiningDate", "employeeId", "storeDetails"].includes(id)) {
      updateFormData.customData[id] = value;
    } else {
      updateFormData[id] = value;
    }
    this.setState({
      formData: updateFormData,
      validations
    });
  }

  updateArrayStepCompletedCall() {
    let scholarshipId = "";
    if (window != undefined) {
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    this.props.applicationInstructionStep({
      scholarshipId: scholarshipId
    });
  }

  updateArrayStepForDelete() {
    let scholarshipId = "";
    if (window != undefined) {
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    this.props.applicationInstructionStep({
      scholarshipId: scholarshipId
    });
  }

  getReferenceData() {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }

    this.props.getReferenceData({
      userId: this.props.userId,
      scholarshipId: scholarshipId,
      step: "REFERENCES"
    });
  }

  componentWillMount() {
    this.getReferenceData();
  }

  componentWillReceiveProps(nextProps) {
    const { scholarshipSteps } = nextProps;
    const { type, applicationReferenceData, payload } = nextProps.referenceDataConfig;
    switch (type) {
      case "UPDATE_STEP_REFERENCE_SUCCESS":
        this.props.applicationInstructionStep({
          scholarshipId: gblFunc.getStoreApplicationScholarshipId()
        });
        break;
      case "FETCH_APPLICATION_FORMS_REFERENCE_SUCCESS":
        let validationInit = {};
        if (applicationReferenceData) {
          for (let key in applicationReferenceData.referenceDataConfig) {
            if (
              applicationReferenceData.referenceDataConfig[key].validations !=
              null &&
              applicationReferenceData.referenceDataConfig[key].active
            ) {
              validationInit[key] = null;
            }
          }
          this.setState({
            configFieldsData: applicationReferenceData.referenceDataConfig,
            relation: applicationReferenceData.subscriber_relation,
            occupation: applicationReferenceData.occupationList,
            referenceGetList: applicationReferenceData.referenceGetListByUser.references,
            validations: validationInit
          });

          let rerList = applicationReferenceData.referenceGetListByUser.references;

          //CHECK CONDITION FOR IF COMPLETED and REMOVEDD
          if (
            // FOR COMPLETED
            rerList != null &&
            rerList.length == this.state.minEntry
          ) {
            // SET
            this.setState({
              isCompleted: true
            });
          } else {
            this.setState({
              isCompleted: false
            });
          }

          if (
            // FOR REMOVED
            rerList != null &&
            rerList.length ==
            (MIN_REFERENCES_ONE[this.props.match.params.bsid.toUpperCase()]
              ? 1
              : this.state.minEntry + 1)
          ) {
            // SET
            this.setState({
              isRemoved: true
            });
          } else {
            this.setState({
              isRemoved: false
            });
          }

          if (
            // FOR NEXT
            rerList != null &&
            rerList.length >=
            (MIN_REFERENCES_ONE[this.props.match.params.bsid.toUpperCase()]
              ? 1
              : this.state.minEntry + 1)
          ) {
            // SET
            this.setState({
              isNextVisible: true
            });
          } else {
            this.setState({
              isNextVisible: false
            });
          }
        }

        break;
      case "UPDATE_APPLICATION_FORMS_REFERENCE_SUCCESS":
        this.state.formData = {
          address: "",
          mobile: "",
          name: "",
          occupation: "",
          other_occupation: "",
          other_relation: "",
          relation: "",
          customData: {}
        };
        this.setState(
          {
            formData: this.state.formData,
            status: true,
            msg: "Record has been Updated",
            showLoader: true,
            isFormButtonShow: true
          },
          () => this.getReferenceData()
        );
        // if (this.state.isCompleted) {
        //   // CALL FOR UPDATE IF COMPLETED
        //   this.updateArrayStepCompletedCall();
        // }

        // if (this.state.isNextVisible) {
        //   // go next if isNextVisible
        //   this.setState({
        //     isNext: true
        //   });
        // }

        break;

      case "UPDATE_APPLICATION_FORMS_REFERENCE_FAILURE":
        this.setState({
          status: false,
          msg: "Something went wrong",
          showLoader: this,
          activeTab: null
        });
        break;
      case "DELETE_APPLICATION_FORMS_REFERENCE_SUCCESS":
        let scholarshipId = "";
        if (window != undefined) {
          scholarshipId = parseInt(
            window.localStorage.getItem("applicationSchID")
          );
        }
        this.setState(
          {
            status: true,
            msg: "Your record has been deleted",
            showLoader: true,
            showConfirmationPopup: false,
            deleteSuccessCallBack: null,
            isFormButtonShow: true
          },
          () => this.getReferenceData()
        );

        this.updateArrayStepForDelete(); // DELETE CALL FOR UPDATE STEPS

        break;
      case "DELETE_APPLICATION_FORMS_REFERENCE_FAILURE":
        this.setState({
          status: false,
          msg: "No Record Deleted",
          showLoader: true,
          showConfirmationPopup: false,
          deleteSuccessCallBack: null
        });
        break;
      case "UPDATE_STEP_REFERENCE_FAILURE":
        this.setState({
          status: false,
          msg: payload && payload.response && payload.response.data.message,
          showLoader: true,
        });
        break;
      case "FETCH_APPLICATION_STEP_SUCCESS":
        if (this.state.bankSection) {
          let steps = [];
          scholarshipSteps && scholarshipSteps.map(item => {
            steps.push(item.step);
          })
          const dat = steps.indexOf('REFERENCES');
          if (scholarshipSteps && scholarshipSteps.length === dat + 1) {
            this.setState({
              status: true,
              msg: "Record has been Updated",
              showLoader: true,
              bankSection: false
            })
          } else {
            this.setState({
              isNext: true,
              bankSection: false
            })
          }
        }
        break;
    }
  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
  }

  render() {
    const formConfig = this.state.configFieldsData;
    const { params } = this.props.match;
    return (
      <section className="sectionwhite">
        {this.state.isNext && params.bsid ? (
          <Redirect
            to={`/application/${params.bsid.toUpperCase()}/form/${
              this.props.redirectionSteps[
              this.props.redirectionSteps.indexOf("references") + 1
              ]
              }`}
          />
        ) : (
            ""
          )}
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <ConfirmMessagePopup
          message={"Are you sure want to delete ?"}
          showPopup={this.state.showConfirmationPopup}
          onConfirmationSuccess={this.state.deleteSuccessCallBack}
          onConfirmationFailure={this.hideConfirmationPopup}
        />
        <article className="form">
          <article className="row familydetails">
            <article className="col-md-12 subheadingerror reftext">
              {/* Do you have any prior work experience? */}
              {/* My References */}
              {MIN_REFERENCES_ONE[this.props.match.params.bsid.toUpperCase()]
                ? MIN_REFERENCES_ONE[this.props.match.params.bsid.toUpperCase()]
                : "* Please add minimum two references"}
            </article>

            {this.state.referenceGetList && this.state.referenceGetList.map((cls, index) => {
              let listItems = cls;

              return (
                <article className="reference">
                  <article className="col-md-12">
                    <article className="newsubheading">
                      <article className="heading topborder"
                        onClick={event => this.editReferenceVal(listItems)}

                      >
                        <p key={listItems.id} >
                          <i
                            className={(this.state.formData.relation && this.state.formData.relation == listItems.relation) && this.state.activeTab === listItems.relation ? "fa fa-minus" : "fa fa-plus"}
                          >
                          </i>
                          {listItems.relationName ? listItems.relationName : listItems.relation_name}
                        </p>


                        <span className="pull-right" style={{
                          position: "relative",
                          top: "-30px"
                        }}>
                          {/* {
                          this.handleClassStatus(cls.classId) ?
                            (<span>Completed&nbsp;&nbsp;<i className="fa fa-check-circle-o complete">&nbsp;</i></span>) :
                            (<span>Pending&nbsp;&nbsp;<i className="fa fa-pencil-square-o pending">&nbsp;</i></span>)
                        } */}
                        </span>
                      </article>
                      <article>
                        {!this.state.isFormButtonShow && listItems.relation == this.state.formData.relation ? (
                          <article>
                            <article>
                              <article>
                                <article className="familydetails">
                                  <h4>
                                    {"Reference " + (index + 1)}
                                    <article className="pull-right">
                                      <i className="fa fa-trash iconedit"
                                        onClick={this.removeReference.bind(
                                          this,
                                          listItems.relation
                                        )}
                                      > &nbsp;</i>
                                    </article>
                                  </h4>
                                </article>
                                <article className="row">
                                  {formConfig.name != null && formConfig.name.active ? (
                                    <article className="col-md-4">
                                      <article className="form-group">
                                        <input
                                          type="text"
                                          id="name"
                                          data-custom={formConfig.name.custom}
                                          onChange={this.formChangeHandler}
                                          value={this.state.formData.name || ""}
                                          required
                                        />
                                        <label>{`${formConfig.name.label}`}</label>
                                        {this.state.validations["name"] ? (
                                          <span className="error animated bounce">
                                            {this.state.validations["name"]}
                                          </span>
                                        ) : null}
                                      </article>
                                    </article>
                                  ) : (
                                      ""
                                    )}

                                  {formConfig.mobile != null && formConfig.mobile.active ? (
                                    <article className="col-md-4">
                                      <article className="form-group">
                                        <input
                                          type="text"
                                          id="mobile"
                                          data-custom={formConfig.mobile.custom}
                                          onChange={this.formChangeHandler}
                                          value={this.state.formData.mobile || ""}
                                          maxLength="10"
                                          required
                                        />
                                        <label>{`${formConfig.mobile.label}`}</label>
                                        {this.state.validations["mobile"] ? (
                                          <span className="error animated bounce">
                                            {this.state.validations["mobile"]}
                                          </span>
                                        ) : null}
                                      </article>
                                    </article>
                                  ) : (
                                      ""
                                    )}

                                  {formConfig.occupation != null &&
                                    formConfig.occupation.active ? (
                                      <article className="col-md-4">
                                        <article className="form-group">
                                          <select
                                            className="icon"
                                            id="occupation"
                                            onChange={this.formChangeHandler.bind(this)}
                                            value={this.state.formData.occupation || ""}
                                          >
                                            <option value="">Select Occupation</option>
                                            {formConfig.occupation.dataOptions != null ? (
                                              formConfig.occupation.dataOptions.map(res => {
                                                return (
                                                  <option key={res.id} value={res.id}>
                                                    {res.occupationName}
                                                  </option>
                                                );
                                              })
                                            ) : this.state.occupation != null ? (
                                              this.state.occupation.map(res => {
                                                return (
                                                  <option key={res.id} value={res.id}>
                                                    {res.occupationName}
                                                  </option>
                                                );
                                              })
                                            ) : (
                                                  <option>Occupation</option>
                                                )}
                                          </select>
                                          <label className="labelstyle">{`${
                                            formConfig.occupation.label
                                            }`}</label>

                                          {this.state.validations["occupation"] ? (
                                            <span className="error animated bounce">
                                              {this.state.validations["occupation"]}
                                            </span>
                                          ) : null}
                                        </article>
                                      </article>
                                    ) : (
                                      ""
                                    )}

                                  {this.state.isOtherOccupation ? (
                                    <article className="col-md-4">
                                      <article className="form-group">
                                        <input
                                          type="text"
                                          id="other_occupation"
                                          onChange={this.formChangeHandler}
                                          value={this.state.formData.other_occupation || ""}
                                          required
                                        />
                                        <label>Other Occupation</label>
                                        {this.state.validations["other_occupation"] ? (
                                          <span className="error animated bounce">
                                            {this.state.validations["other_occupation"]}
                                          </span>
                                        ) : null}
                                      </article>
                                    </article>
                                  ) : (
                                      ""
                                    )}

                                  {formConfig.relation != null &&
                                    formConfig.relation.active ? (
                                      <article className="col-md-4">
                                        <article className="form-group">
                                          <select
                                            id="relation"
                                            className="icon"
                                            onChange={this.formChangeHandler.bind(this)}
                                            value={this.state.formData.relation || ""}
                                            disabled={!this.state.isFormButtonShow && !!this.state.formData && !!this.state.formData.relation}
                                          >
                                            <option value="">Select Relation</option>
                                            {formConfig.relation.dataOptions != null ? (
                                              formConfig.relation.dataOptions.map((res, i) => {
                                                let isdisabled = this.state.referenceGetList.find((item) => {
                                                  if (("" + item.relation) == (res.id)) {
                                                    return item
                                                  }
                                                })
                                                return (
                                                  <option key={res.id} value={res.id}
                                                    disabled={isdisabled && isdisabled.relation == Number(res.id) ? true : false}

                                                  >
                                                    {res.rulevalue}
                                                  </option>
                                                );
                                              })
                                            ) : this.state.relation != null ? (
                                              this.state.relation.map((res, i) => {
                                                let isdisabled = this.state.referenceGetList.find((item) => {
                                                  if (("" + item.relation) == (res.id)) {
                                                    return item
                                                  }
                                                })
                                                return (
                                                  <option
                                                    selected={
                                                      res.id == this.state.formData.relation
                                                        ? true
                                                        : false
                                                    }
                                                    disabled={isdisabled && isdisabled.relation == Number(res.id) ? true : false}


                                                    key={res.id}
                                                    value={res.id}
                                                  >
                                                    {res.rulevalue}
                                                  </option>
                                                );
                                              })
                                            ) : (
                                                  <option>Relation</option>
                                                )}
                                          </select>
                                          <label className="labelstyle">
                                            {gblFunc.capitalText(formConfig.relation.label)}
                                          </label>
                                          {this.state.validations["relation"] ? (
                                            <span className="error animated bounce">
                                              {this.state.validations["relation"]}
                                            </span>
                                          ) : null}
                                        </article>
                                      </article>
                                    ) : (
                                      ""
                                    )}
                                  {this.state.isOtherRelation ? (
                                    <article className="col-md-4">
                                      <article className="form-group">
                                        <input
                                          type="text"
                                          id="other_relation"
                                          onChange={this.formChangeHandler}
                                          value={this.state.formData.other_relation || ""}
                                          required
                                        />
                                        <label>Other Relation</label>
                                        {this.state.validations["other_relation"] ? (
                                          <span className="error animated bounce">
                                            {this.state.validations["other_relation"]}
                                          </span>
                                        ) : null}
                                      </article>
                                    </article>
                                  ) : (
                                      " "
                                    )}

                                  {formConfig.address != null && formConfig.address.active ? (
                                    <article className="col-md-4">
                                      <article className="form-group">
                                        <input
                                          type="text"
                                          id="address"
                                          data-custom={formConfig.address.custom}
                                          onChange={this.formChangeHandler}
                                          value={this.state.formData.address || ""}
                                          required
                                        />
                                        <label>
                                          {gblFunc.capitalText(formConfig.address.label)}
                                        </label>
                                        {this.state.validations["address"] ? (
                                          <span className="error animated bounce">
                                            {this.state.validations["address"]}
                                          </span>
                                        ) : null}
                                      </article>
                                    </article>
                                  ) : (
                                      ""
                                    )}
                                  {formConfig.storeDetails != null &&
                                    formConfig.storeDetails.active ? (
                                      <article className="col-md-4">
                                        <article className="form-group">
                                          <input
                                            type="text"
                                            id="storeDetails"
                                            data-custom={formConfig.storeDetails.custom}
                                            onChange={this.formChangeHandler}
                                            value={
                                              this.state.formData.customData &&
                                                this.state.formData.customData.storeDetails
                                                ? this.state.formData.customData.storeDetails
                                                : ""
                                            }
                                          />
                                          <label>
                                            {gblFunc.capitalText(formConfig.storeDetails.label)}
                                          </label>
                                          {this.state.validations["storeDetails"] ? (
                                            <span className="error animated bounce">
                                              {this.state.validations["storeDetails"]}
                                            </span>
                                          ) : null}
                                        </article>
                                      </article>
                                    ) : (
                                      ""
                                    )}
                                  {formConfig.employeeId != null &&
                                    formConfig.employeeId.active ? (
                                      <article className="col-md-4">
                                        <article className="form-group">
                                          <input
                                            type="text"
                                            id="employeeId"
                                            disabled={false}
                                            data-custom={formConfig.employeeId.custom}
                                            onChange={this.formChangeHandler}
                                            value={
                                              this.state.formData.customData &&
                                                this.state.formData.customData.employeeId
                                                ? this.state.formData.customData.employeeId
                                                : ""
                                            }
                                          />
                                          <label>
                                            {gblFunc.capitalText(formConfig.employeeId.label)}
                                          </label>
                                          {this.state.validations["employeeId"] ? (
                                            <span className="error animated bounce">
                                              {this.state.validations["employeeId"]}
                                            </span>
                                          ) : null}
                                        </article>
                                      </article>
                                    ) : (
                                      ""
                                    )}
                                  {formConfig.joiningDate != null &&
                                    formConfig.joiningDate.active ? (
                                      <article className="col-md-4">
                                        <article className="form-group">
                                          <DatePicker
                                            showYearDropdown
                                            scrollableYearDropdown
                                            // readOnly
                                            yearDropdownItemNumber={40}
                                            minDate={moment().subtract(100, "years")}
                                            maxDate={moment().add(1, "years")}
                                            name="joiningDate"
                                            id="joiningDate"
                                            data-custom={formConfig.joiningDate.custom}
                                            className="icon-date"
                                            autoComplete="off"
                                            selected={
                                              this.state.formData.customData &&
                                                this.state.formData.customData.joiningDate
                                                ? moment(
                                                  moment(
                                                    (this.state.formData.customData &&
                                                      this.state.formData.customData
                                                        .joiningDate) ||
                                                    ""
                                                  ).format("DD-MM-YYYY"),
                                                  "DD-MM-YYYY"
                                                )
                                                : null
                                            }
                                            onChange={event =>
                                              this.formChangeHandler(event, "joiningDate")
                                            }
                                            dateFormat="DD-MM-YYYY"
                                          />

                                          <label className="labelstyle">
                                            {gblFunc.capitalText(formConfig.joiningDate.label)}
                                          </label>
                                          {this.state.validations["joiningDate"] ? (
                                            <span className="error animated bounce">
                                              {this.state.validations["joiningDate"]}
                                            </span>
                                          ) : null}
                                        </article>
                                      </article>
                                    ) : (
                                      ""
                                    )}
                                </article>
                                <article className="row">
                                  <article className="col-md-12">
                                    <button
                                      onClick={this.formClose}
                                      className="btn pull-right"
                                    >
                                      Close
                      </button>
                                    <input
                                      type="submit"
                                      value="Save"
                                      className="btn pull-right marginRight"
                                      onClick={this.submit}
                                    />
                                  </article>
                                </article>
                              </article>
                            </article>
                          </article>
                        ) : (
                            ""
                          )}

                      </article>

                    </article>
                  </article>
                </article>
              )
            }
            )}
            {/*START DYNAMIC TABLE DISPLAY*/}

            {/* {this.state.referenceGetList != null &&
              this.state.referenceGetList.length > 0 ? (
                <article className="margintoptable table-responsive floatTable">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        {formConfig.name != null && formConfig.name.active ? (
                          <th>{formConfig.name.label}</th>
                        ) : (
                            ""
                          )}

                        {formConfig.mobile != null && formConfig.mobile.active ? (
                          <th>{formConfig.mobile.label}</th>
                        ) : (
                            ""
                          )}

                        {formConfig.address != null &&
                          formConfig.address.active ? (
                            <th>{formConfig.address.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.relation != null &&
                          formConfig.relation.active ? (
                            <th>{formConfig.relation.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.occupation != null &&
                          formConfig.occupation.active ? (
                            <th>{formConfig.occupation.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.storeDetails != null &&
                          formConfig.storeDetails.active ? (
                            <th>{formConfig.storeDetails.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.employeeId != null &&
                          formConfig.employeeId.active ? (
                            <th>{formConfig.employeeId.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.joiningDate != null &&
                          formConfig.joiningDate.active ? (
                            <th>{formConfig.joiningDate.label}</th>
                          ) : (
                            ""
                          )}

                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.referenceGetList.map((res, i) => {
                        return (
                          <tr key={i}>
                            {formConfig.name != null && formConfig.name.active ? (
                              <td>{res.name}</td>
                            ) : (
                                ""
                              )}

                            {formConfig.mobile != null &&
                              formConfig.mobile.active ? (
                                <td>{res.mobile}</td>
                              ) : (
                                ""
                              )}

                            {formConfig.address != null &&
                              formConfig.address.active ? (
                                <td>{res.address}</td>
                              ) : (
                                ""
                              )}

                            {formConfig.relation != null &&
                              formConfig.relation.active ? (
                                <td>
                                  {this.state.relation
                                    .filter(item => {
                                      return res.relation == item.id;
                                    })
                                    .map(scan => {
                                      return scan.rulevalue;
                                    })}
                                </td>
                              ) : (
                                ""
                              )}

                            {formConfig.occupation != null &&
                              formConfig.occupation.active ? (
                                <td>
                                  {this.state.occupation
                                    .filter(item => {
                                      return res.occupation == item.id;
                                    })
                                    .map(scan => {
                                      return scan.occupationName;
                                    })}
                                </td>
                              ) : (
                                ""
                              )}

                            {formConfig.storeDetails != null &&
                              formConfig.storeDetails.active ? (
                                <td>
                                  {res.customData && res.customData.storeDetails}
                                </td>
                              ) : (
                                ""
                              )}

                            {formConfig.employeeId != null &&
                              formConfig.employeeId.active ? (
                                <td>
                                  {res.customData && res.customData.employeeId}
                                </td>
                              ) : (
                                ""
                              )}

                            {formConfig.joiningDate != null &&
                              formConfig.joiningDate.active ? (
                                <td>
                                  {res.customData && res.customData.joiningDate}
                                </td>
                              ) : (
                                ""
                              )}
                            <td>
                              <article className="pull-left">
                                <i
                                  onClick={event => this.editReferenceVal(res)}
                                  className="fa fa-edit iconedit"
                                >
                                  {" "}
                                  &nbsp;
                              </i>
                                <i
                                  onClick={this.removeReference.bind(
                                    this,
                                    res.relation
                                  )}
                                  className="fa fa-trash iconedit"
                                >
                                  {" "}
                                  &nbsp;
                              </i>
                              </article>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </article>
              ) : (
                ""
              )} */}

            {/*END DYNAMIC TABLE DISPLAY*/}

            {/* {this.state.referenceGetList != null
              ? this.state.referenceGetList.map(res => {
                  return (
                    <article className="col-md-12 " key={res.id}>
                      <article className="graystrip">
                        <h2>{res.name}</h2>
                        <span>{res.mobile}</span>
                        <span>
                          {this.state.relation
                            .filter(item => {
                              return res.relation == item.id;
                            })
                            .map(scan => {
                              return scan.rulevalue;
                            })}
                        </span>
                        <span>
                          {this.state.occupation
                            .filter(item => {
                              return res.occupation == item.id;
                            })
                            .map(scan => {
                              return scan.occupationName;
                            })}
                        </span>
                        <span>{res.address}</span>
                        <article className="pull-right">
                          <span
                            className="edit"
                            onClick={event => this.editReferenceVal(res)}
                          >
                            &nbsp;
                          </span>
                          <span
                            className="delete"
                            onClick={this.removeReference.bind(this, res.id)}
                          >
                            &nbsp;
                          </span>
                        </article>
                      </article>
                    </article>
                  );
                })
              : ""} */}
          </article>
          {this.state.isFormButtonShow ? (
            <article className="row">
              <article className="col-md-12">
                {this.state.referenceGetList != null &&
                  this.state.referenceGetList.length === 1 &&
                  childEduBSID[
                  this.props.match.params.bsid.toUpperCase()
                  ] ? null : (
                    <a onClick={this.showForm} className="btn pull-left">
                      Add Reference +
                    </a>
                  )}

                {this.state.isNextVisible ? (
                  <a onClick={this.goNext} className="btn pull-right">
                    Save & Continue
                  </a>
                ) : (
                    " "
                  )}
              </article>
            </article>
          ) : (
              ""
            )}

          {!this.state.isFormButtonShow && !this.state.referenceGetList.some(x => x.relation == parseInt(this.state.formData.relation)) ? (
            <article>
              <article>
                <article>
                  <article className="familydetails">
                    <h4>
                      {"Add Reference " + (this.state.referenceGetList.length + 1)}
                      {/* <article className="pull-right">
                        <i className="fa fa-trash iconedit"> &nbsp;</i>
                      </article> */}
                    </h4>
                  </article>
                  <article className="row">
                    {formConfig.name != null && formConfig.name.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="name"
                            data-custom={formConfig.name.custom}
                            onChange={this.formChangeHandler}
                            value={this.state.formData.name || ""}
                            required
                          />
                          <label>{`${formConfig.name.label}`}</label>
                          {this.state.validations["name"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["name"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                        ""
                      )}

                    {formConfig.mobile != null && formConfig.mobile.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="mobile"
                            data-custom={formConfig.mobile.custom}
                            onChange={this.formChangeHandler}
                            value={this.state.formData.mobile || ""}
                            maxLength="10"
                            required
                          />
                          <label>{`${formConfig.mobile.label}`}</label>
                          {this.state.validations["mobile"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["mobile"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                        ""
                      )}

                    {formConfig.occupation != null &&
                      formConfig.occupation.active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <select
                              className="icon"
                              id="occupation"
                              onChange={this.formChangeHandler.bind(this)}
                              value={this.state.formData.occupation || ""}
                            >
                              <option value="">Select Occupation</option>
                              {formConfig.occupation.dataOptions != null ? (
                                formConfig.occupation.dataOptions.map(res => {
                                  return (
                                    <option key={res.id} value={res.id}>
                                      {res.occupationName}
                                    </option>
                                  );
                                })
                              ) : this.state.occupation != null ? (
                                this.state.occupation.map(res => {
                                  return (
                                    <option key={res.id} value={res.id}>
                                      {res.occupationName}
                                    </option>
                                  );
                                })
                              ) : (
                                    <option>Occupation</option>
                                  )}
                            </select>
                            <label className="labelstyle">{`${
                              formConfig.occupation.label
                              }`}</label>

                            {this.state.validations["occupation"] ? (
                              <span className="error animated bounce">
                                {this.state.validations["occupation"]}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      ) : (
                        ""
                      )}

                    {this.state.isOtherOccupation ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="other_occupation"
                            onChange={this.formChangeHandler}
                            value={this.state.formData.other_occupation || ""}
                            required
                          />
                          <label>Other Occupation</label>
                          {this.state.validations["other_occupation"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["other_occupation"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                        ""
                      )}

                    {formConfig.relation != null &&
                      formConfig.relation.active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <select
                              id="relation"
                              className="icon"
                              onChange={this.formChangeHandler.bind(this)}
                              value={this.state.formData.relation || ""}
                            //  disabled={!this.state.isFormButtonShow && !!this.state.formData &&  !!this.state.formData.relation }
                            >
                              <option value="">Select Relation</option>
                              {formConfig.relation.dataOptions != null ? (
                                formConfig.relation.dataOptions.map((res, i) => {
                                  let isdisabled = this.state.referenceGetList.find((item) => {
                                    if (("" + item.relation) == (res.id)) {
                                      return item
                                    }
                                  })
                                  return (
                                    <option key={res.id} value={res.id}
                                      disabled={isdisabled && isdisabled.relation == Number(res.id) ? true : false}

                                    >
                                      {res.rulevalue}
                                    </option>
                                  );
                                })
                              ) : this.state.relation != null ? (
                                this.state.relation.map((res, i) => {
                                  let isdisabled = this.state.referenceGetList.find((item) => {
                                    if (("" + item.relation) == (res.id)) {
                                      return item
                                    }
                                  })
                                  return (
                                    <option
                                      selected={
                                        res.id == this.state.formData.relation
                                          ? true
                                          : false
                                      }
                                      disabled={isdisabled && isdisabled.relation == Number(res.id) ? true : false}


                                      key={res.id}
                                      value={res.id}
                                    >
                                      {res.rulevalue}
                                    </option>
                                  );
                                })
                              ) : (
                                    <option>Relation</option>
                                  )}
                            </select>
                            <label className="labelstyle">
                              {gblFunc.capitalText(formConfig.relation.label)}
                            </label>
                            {this.state.validations["relation"] ? (
                              <span className="error animated bounce">
                                {this.state.validations["relation"]}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      ) : (
                        ""
                      )}
                    {this.state.isOtherRelation ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="other_relation"
                            onChange={this.formChangeHandler}
                            value={this.state.formData.other_relation || ""}
                            required
                          />
                          <label>Other Relation</label>
                          {this.state.validations["other_relation"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["other_relation"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                        " "
                      )}

                    {formConfig.address != null && formConfig.address.active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            id="address"
                            data-custom={formConfig.address.custom}
                            onChange={this.formChangeHandler}
                            value={this.state.formData.address || ""}
                            required
                          />
                          <label>
                            {gblFunc.capitalText(formConfig.address.label)}
                          </label>
                          {this.state.validations["address"] ? (
                            <span className="error animated bounce">
                              {this.state.validations["address"]}
                            </span>
                          ) : null}
                        </article>
                      </article>
                    ) : (
                        ""
                      )}
                    {formConfig.storeDetails != null &&
                      formConfig.storeDetails.active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <input
                              type="text"
                              id="storeDetails"
                              data-custom={formConfig.storeDetails.custom}
                              onChange={this.formChangeHandler}
                              value={
                                this.state.formData.customData &&
                                  this.state.formData.customData.storeDetails
                                  ? this.state.formData.customData.storeDetails
                                  : ""
                              }
                            />
                            <label>
                              {gblFunc.capitalText(formConfig.storeDetails.label)}
                            </label>
                            {this.state.validations["storeDetails"] ? (
                              <span className="error animated bounce">
                                {this.state.validations["storeDetails"]}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      ) : (
                        ""
                      )}
                    {formConfig.employeeId != null &&
                      formConfig.employeeId.active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <input
                              type="text"
                              id="employeeId"
                              disabled={false}
                              data-custom={formConfig.employeeId.custom}
                              onChange={this.formChangeHandler}
                              value={
                                this.state.formData.customData &&
                                  this.state.formData.customData.employeeId
                                  ? this.state.formData.customData.employeeId
                                  : ""
                              }
                            />
                            <label>
                              {gblFunc.capitalText(formConfig.employeeId.label)}
                            </label>
                            {this.state.validations["employeeId"] ? (
                              <span className="error animated bounce">
                                {this.state.validations["employeeId"]}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      ) : (
                        ""
                      )}
                    {formConfig.joiningDate != null &&
                      formConfig.joiningDate.active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <DatePicker
                              showYearDropdown
                              scrollableYearDropdown
                              // readOnly
                              yearDropdownItemNumber={40}
                              minDate={moment().subtract(100, "years")}
                              maxDate={moment().add(1, "years")}
                              name="joiningDate"
                              id="joiningDate"
                              data-custom={formConfig.joiningDate.custom}
                              className="icon-date"
                              autoComplete="off"
                              selected={
                                this.state.formData.customData &&
                                  this.state.formData.customData.joiningDate
                                  ? moment(
                                    moment(
                                      (this.state.formData.customData &&
                                        this.state.formData.customData
                                          .joiningDate) ||
                                      ""
                                    ).format("DD-MM-YYYY"),
                                    "DD-MM-YYYY"
                                  )
                                  : null
                              }
                              onChange={event =>
                                this.formChangeHandler(event, "joiningDate")
                              }
                              dateFormat="DD-MM-YYYY"
                            />

                            <label className="labelstyle">
                              {gblFunc.capitalText(formConfig.joiningDate.label)}
                            </label>
                            {this.state.validations["joiningDate"] ? (
                              <span className="error animated bounce">
                                {this.state.validations["joiningDate"]}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      ) : (
                        ""
                      )}
                  </article>
                  <article className="row">
                    <article className="col-md-12">
                      <button
                        onClick={this.formClose}
                        className="btn pull-right"
                      >
                        Close
                      </button>
                      <input
                        type="submit"
                        value="Save"
                        className="btn pull-right marginRight"
                        onClick={this.submit}
                      />
                    </article>
                  </article>
                </article>
              </article>
            </article>
          ) : (
              ""
            )}
        </article>
      </section>
    );
  }
}

export default Reference;
