import React, { Component } from "react";

class WorkExperience extends Component {
  render() {
    return (
      <section className="sectionwhite">
        <article className="form">
          <article className="row">
            <article className="col-md-12 subheading">
              Do you have any prior work experience?
              <span>&nbsp;</span>
            </article>
            <article className="col-md-12 ">
              <article className="graystrip">
                <h2>TCS</h2>
                <span>Field Executive</span>
                <span>2008</span>
                <span>2 years</span>
                <span>1,78,000</span>
                <span>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry.
                </span>

                <article className="pull-right">
                  <span className="edit">&nbsp;</span>
                  <span className="delete">&nbsp;</span>
                </article>
              </article>
            </article>
          </article>
          <article className="paddingborder">
            <article className="border">&nbsp;</article>
          </article>
          <article>
            <section>
              <article className="row">
                <article className="col-md-4">
                  <article className="form-group">
                    <input type="text" id="Company name" required />
                    <label htmlFor="Company name">Company name*</label>
                  </article>
                </article>
                <article className="col-md-4">
                  <article className="form-group">
                    <input type="text" id="Designation" required />
                    <label htmlFor="Designation">Designation*</label>
                  </article>
                </article>
                <article className="col-md-4">
                  <article className="form-group">
                    <select className="icon">
                      <option>3</option>
                    </select>
                    <label className="selectLabel">Duration*</label>
                  </article>
                </article>
              </article>
              <article className="row">
                <article className="col-md-4">
                  <article className="form-group">
                    <input type="text" id="Annual income" required />
                    <label htmlFor="Annual income">Annual income*</label>
                  </article>
                </article>
                <article className="col-md-4">
                  <article className="form-group">
                    <input type="text" id="Decription" required />
                    <label htmlFor="Decription">Decription</label>
                  </article>
                </article>
              </article>

              <article className="row">
                <article className="col-md-12">
                  <a className="btn-yellow pull-left">Add work experience +</a>
                  <input
                    type="submit"
                    value="Save"
                    className="btn pull-right"
                  />
                  <input
                    type="submit"
                    value="Skip"
                    className="btn pull-right marginRight"
                  />
                </article>
              </article>
            </section>
          </article>
        </article>
      </section>
    );
  }
}

export default WorkExperience;
