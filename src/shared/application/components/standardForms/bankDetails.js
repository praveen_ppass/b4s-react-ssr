import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Loader from "../../../../shared/common/components/loader";
import { ruleRunner } from "../../../../validation/ruleRunner";
import { mapValidationFunc } from "../../../../validation/rules";

import ServerError from "../../../common/components/serverError";
import {
  FETCH_APPLICATION_BANK_DETAILS_SUCCESS,
  SAVE_APPLICATION_BANK_DETAILS_SUCCESS,
  FETCH_BANK_DTS_CONFIG_SUCCESS
} from "../../actions/applicationBankDetailsAction";
import { FETCH_BANK_DETAIL_SUCCEDED } from "../../../scholar/scholarAction";
import gblFunc from "../../../../globals/globalFunctions";
import AlertMessage from "../../../common/components/alertMsg";
import { camelCase, relationById } from "../../../../constants/constants";
import { extractStepTemplate, childEduBSID } from "../../formconfig";

class BankDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bankDetailInfo: [],
      validations: {},
      bankConfig: {},
      isFormValid: true,
      isNext: false,
      isNextVisible: false,
      status: "",
      msg: "",
      showLoader: false,
      formData: {
        id: null,
        accountHolderName: "",
        accountNumber: "",
        bankName: "",
        branchName: "",
        ifscCode: "",
        bankAccountType: "",
        relationId: "",
        customData: {}
      },
      bankDataIfsc: {
        bankNameIfsc: ""
      },
      isFormButtonShow: true,
      showAlert: false,

    };
    this.formChangeHandler = this.formChangeHandler.bind(this);
    this.mapToApiParams = this.mapToApiParams.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.goNext = this.goNext.bind(this);
    this.submit = this.submit.bind(this);
    this.showForm = this.showForm.bind(this);
    this.formClose = this.formClose.bind(this);
    this.resetValidation = this.resetValidation.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.close2 = this.close2.bind(this);
  }

  close2() {
    this.setState({
      showAlert: false,
      msg: "",
      status: false
    });
  }



  handleChange({ target }, cb) {
    let ifscCode = /^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/;
    if (target.name == "ifscCode" && target.value.length == 11) {
      let isValid = ifscCode.test(target.value);
      if (isValid) {
        this.props.fetchBankDetail({ ifscCode: target.value });
      }
    }
    // cb(target.name, target.value);
    // this.setState({ [target.name]: target.value });
    const { id, value } = target;
    let validations = { ...this.state.validations };
    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      if (name != undefined && validationFunctions != undefined) {
        const validationResult = ruleRunner(
          value,
          id,
          name,
          ...validationFunctions
        );
        validations[id] = validationResult[id];
      }
    }

    const updateFormData = { ...this.state.formData };
    updateFormData[id] = value;
    this.setState({
      formData: updateFormData,
      validations
    });
  }

  goNext() {
    this.props.updateBankDtStep({
      scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
      step: "BANK_DETAIL",
      userId: gblFunc.getStoreUserDetails()["userId"]
    });
    // setTimeout(() => {
    //   this.props.applicationInstructionStep({
    //     scholarshipId: gblFunc.getStoreApplicationScholarshipId()
    //   });
    // }, 1000);
  }

  mapToApiParams() {
    const {
      id,
      accountHolderName,
      accountNumber,
      bankName,
      branchName,
      ifscCode,
      bankAccountType
    } = this.state;

    let params = {
      accountHolderName,
      accountNumber,
      bankName,
      branchName,
      ifscCode,
      bankAccountType
    };

    if (id) {
      params.id = id;
    }

    return params;
  }

  /************ start - validation part - rules******************* */
  formChangeHandler(event) {
    const { id, value } = event.target;
    let validations = { ...this.state.validations };
    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);
      if (name != undefined && validationFunctions != undefined) {
        const validationResult = ruleRunner(
          value,
          id,
          name,
          ...validationFunctions
        );
        validations[id] = validationResult[id];
      }
    }

    const updateFormData = { ...this.state.formData };
    updateFormData[id] = value;
    this.setState({
      formData: updateFormData,
      validations
    });
  }

  /************ end - validation part - rules******************* */

  /************ start - validation part - get message and required validation******************* */
  getValidationRulesObject(fieldID) {
    let validationObject = {};
    let validationRules = [];
    if (this.state.validations.hasOwnProperty(fieldID)) {
      let configFields = this.state.bankConfig[fieldID];
      if (configFields != undefined && configFields.validations != null) {
        configFields.validations.map(res => {
          if (res != null) {
            let validator = mapValidationFunc(res);
            if (validator != undefined) validationRules.push(validator);
          }
        });
        validationObject.name = "*Field";
        validationObject.validationFunctions = validationRules;
      }
    }
    return validationObject;
  }
  /************ end - validation part - get message and required validation******************* */
  /************ start - validation part - check form submit validation******************* */
  checkFormValidations() {
    let validations = { ...this.state.validations };

    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      if (name != undefined && validationFunctions != undefined) {
        let validationResult = ruleRunner(
          this.state.formData[key],
          key,
          name,
          ...validationFunctions
        );
        validations[key] = validationResult[key];
        if (validationResult[key] !== null) {
          isFormValid = false;
        }
      }
    }
    console.log(validations)

    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }
  /************ end - validation part - check form submit validation******************* */
  componentDidMount() {
    const scholarshipId = gblFunc.getStoreApplicationScholarshipId();
    // this.props.loadPrivacyPolicy();
    this.props.getBankDetails({
      scholarshipId
    });

    this.props.bankDtsConfig({
      scholarshipId
    });
  }

  componentWillReceiveProps(nextProps) {
    const { type, type1, bankIfscData, scholarshipSteps } = nextProps;
    switch (type) {
      case FETCH_APPLICATION_BANK_DETAILS_SUCCESS:
        const bankdetails = nextProps.bankdetails.userApplicationBankDetail;

        if (bankdetails.length > 0) {
          this.setState({
            bankDetailInfo: bankdetails,
            isNextVisible: true
          });
        }
        break;
      case SAVE_APPLICATION_BANK_DETAILS_SUCCESS:
        this.props.getBankDetails({
          scholarshipId: gblFunc.getStoreApplicationScholarshipId()
        });
        break;
      case FETCH_BANK_DTS_CONFIG_SUCCESS:
        const { bankDetailsConfig } = nextProps;
        const bankConfigList = { ...this.state.bankConfig };
        if (bankDetailsConfig && Object.keys(bankDetailsConfig).length > 0) {
          let validationInit = {};
          for (let key in bankDetailsConfig) {
            if (bankDetailsConfig[key] && bankDetailsConfig[key].active) {
              let clearUnderScore = gblFunc.replace_underScore(key);
              bankConfigList[clearUnderScore] = bankDetailsConfig[key];
              validationInit[clearUnderScore] = null;
            }
          }

          this.setState({
            bankConfig: bankConfigList,
            validations: validationInit
          });
        }
        break;
      case "UPDATE_BANK_DT_STEP_SUCCESS":
        let steps = [];
        scholarshipSteps && scholarshipSteps.map(item => {
          steps.push(item.step);
        })
        const dat = steps.indexOf('BANK_DETAIL');
        if (scholarshipSteps && scholarshipSteps.length === dat + 1) {
          this.setState({
            showAlert: true,
            status: true,
            msg: "Record has been Updated."
          })
        } else {
          this.setState({
            isNext: true
          })
        }
        break;
      case "UPDATE_BANK_DT_STEP_FAILURE":
        this.setState({
          showAlert: true,
          status: false,
          msg: "Server Error."
        })
        break;
    }
    switch (type1) {
      case FETCH_BANK_DETAIL_SUCCEDED:
        if (!!bankIfscData) {
          this.setState(prevState => {
            let formData = Object.assign({}, prevState.formData);  // creating copy of state variable jasper
            formData.bankName = bankIfscData.bank;
            formData.branchName = bankIfscData.branch;                     // update the name property, assign a new value                 
            return { formData };                                 // return new object jasper object
          })
        }
        if (!!bankIfscData && bankIfscData.bank === null) {
          this.setState({
            showAlert: true,
            status: false,
            msg:
              "Your IFSC code is invalid. Please provide correct IFSC code."
          });
        }
        break;
    }
  }

  resetValidation() {
    const resetVal = { ...this.state.validations };
    for (let key in resetVal) {
      resetVal[key] = null;
    }

    return resetVal;
  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
  }

  formClose() {
    const validations = this.resetValidation();
    this.setState({
      isFormButtonShow: true,
      validations,
      formData: {
        id: null,
        accountHolderName: "",
        accountNumber: "",
        bankName: "",
        branchName: "",
        ifscCode: "",
        customData: {},
        relationId: ""
      }
    });
  }

  editReferenceVal(items) {
    const {
      id,
      ifscCode,
      accountHolderName,
      accountNumber,
      bankName,
      branchName,
      relationId,
      bankAccountType
    } = items;
    const editData = {
      id,
      ifscCode,
      accountHolderName,
      accountNumber,
      bankName,
      branchName,
      relationId,
      bankAccountType
    };

    this.setState({
      isFormButtonShow: false,
      formData: editData
    });
  }

  submit() {
    let scholarshipId = "";
    if (window != undefined) {
      scholarshipId = parseInt(gblFunc.getStoreApplicationScholarshipId());
    }
    let isPosted = false;

    if (this.state.isCompleted) {
      //CALL STEP POST FOR STEP HAS BEEN COMPLETED
      isPosted = true;
    }

    let isSubmit = this.checkFormValidations();

    if (isSubmit) {
      for (let key in this.state.bankConfig) {
        if (this.state.bankConfig[key].custom) {
          this.state.formData.customData[key] = this.state.formData[key];
        }
      }

      let parseFinalData = camelCase(this.state.formData);
      this.setState({ formData: this.state.formData, isFormButtonShow: true });
      this.props.saveBankDetails({
        scholarshipId,
        data: parseFinalData
      });
    }
  }

  showForm() {
    this.state.isFormButtonShow = !this.state.isFormButtonShow;
    this.setState({
      isFormButtonShow: this.state.isFormButtonShow,
      formData: {
        id: null,
        accountHolderName: "",
        accountNumber: "",
        bankName: "",
        branchName: "",
        ifscCode: "",
        relationId: "",
        customData: {}
      }
    });
  }

  markup(val) {
    return { __html: val };
  }

  render() {
    const { scholarshipSteps } = this.props;
    let bankDtsTemplate = "";
    if (scholarshipSteps && scholarshipSteps.length) {
      bankDtsTemplate = extractStepTemplate(scholarshipSteps, "BANK_DETAIL");
    }
    const formConfig = this.state.bankConfig;
    return (
      <section className="sectionwhite">
        {this.state.isNext && this.props.match.params.bsid ? (
          <Redirect
            to={`/application/${this.props.match.params.bsid}/form/${this.props.redirectionSteps[
              this.props.redirectionSteps.indexOf("bankDetail") + 1
              ]
              }`}
          />
        ) : (
            ""
          )}
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close2.bind(this)}
          isShow={this.state.showAlert}
          status={this.state.status}
          msg={this.state.msg}
        />

        <article className="form">
          <article className="row">
            <article
              dangerouslySetInnerHTML={this.markup(
                bankDtsTemplate.length &&
                  bankDtsTemplate[0] &&
                  bankDtsTemplate[0].message
                  ? bankDtsTemplate[0].message
                  : "* Please add minimum one bank detail"
              )}
              className="col-md-12 subheadingerror"
            />
            {/*START DYNAMIC TABLE DISPLAY*/}

            {this.state.bankDetailInfo != null &&
              this.state.bankDetailInfo.length > 0 ? (
                <article className="margintoptable table-responsive floatTable">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        {formConfig.accountHolderName != null &&
                          formConfig.accountHolderName.active ? (
                            <th>{formConfig.accountHolderName.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.accountNumber != null &&
                          formConfig.accountNumber.active ? (
                            <th>{formConfig.accountNumber.label}</th>
                          ) : (
                            ""
                          )}



                        {formConfig.ifscCode != null &&
                          formConfig.ifscCode.active ? (
                            <th>{formConfig.ifscCode.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.bankName != null &&
                          formConfig.bankName.active ? (
                            <th>{formConfig.bankName.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.branchName != null &&
                          formConfig.branchName.active ? (
                            <th>{formConfig.branchName.label}</th>
                          ) : (
                            ""
                          )}



                        {formConfig.bankAccountType != null &&
                          formConfig.bankAccountType.active ? (
                            <th>{formConfig.bankAccountType.label}</th>
                          ) : (
                            ""
                          )}

                        {formConfig.relationId != null &&
                          formConfig.relationId.active ? (
                            <th>{formConfig.relationId.label}</th>
                          ) : (
                            ""
                          )}

                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.bankDetailInfo.length &&
                        this.state.bankDetailInfo.map((res, i) => {
                          return (
                            <tr key={i}>
                              {formConfig.accountHolderName != null &&
                                formConfig.accountHolderName.active ? (
                                  <td>{res.accountHolderName}</td>
                                ) : (
                                  ""
                                )}

                              {formConfig.accountNumber != null &&
                                formConfig.accountNumber.active ? (
                                  <td>{res.accountNumber}</td>
                                ) : (
                                  ""
                                )}



                              {formConfig.ifscCode != null &&
                                formConfig.ifscCode.active ? (
                                  <td>{res.ifscCode}</td>
                                ) : (
                                  ""
                                )}

                              {formConfig.bankName != null &&
                                formConfig.bankName.active ? (
                                  <td>{res.bankName}</td>
                                ) : (
                                  ""
                                )}


                              {formConfig.branchName != null &&
                                formConfig.branchName.active ? (
                                  <td>{res.branchName}</td>
                                ) : (
                                  ""
                                )}

                              {formConfig.bankAccountType != null &&
                                formConfig.bankAccountType.active ? (
                                  <td>{res.bankAccountTypeName}</td>
                                ) : (
                                  ""
                                )}

                              {formConfig.relationId != null &&
                                formConfig.relationId.active ? (
                                  <td>{relationById(res.relationId)}</td>
                                ) : (
                                  ""
                                )}

                              <td>
                                <article className="pull-left">
                                  <i
                                    onClick={event => this.editReferenceVal(res)}
                                    className="fa fa-edit iconedit"
                                  >
                                    {" "}
                                    &nbsp;
                                </i>
                                  {/* <i
                                onClick={this.removeReference.bind(
                                  this,
                                  res.id
                                )}
                                className="fa fa-trash iconedit"
                              >
                                {" "}
                                &nbsp;
                              </i> */}
                                </article>
                              </td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </table>
                </article>
              ) : (
                ""
              )}
          </article>
          {this.state.isFormButtonShow ? (
            <article className="row">
              <article className="col-md-12">
                {!!this.state.bankDetailInfo &&
                  this.state.bankDetailInfo.length >= 1 ? null : (
                    <a onClick={this.showForm} className="btn pull-left familybtnsize">
                      Add Bank Detail +
                    </a>
                  )}
                {this.state.isNextVisible ? (
                  <a onClick={this.goNext} className="btn pull-right familybtnsize">
                    Save & Continue
                  </a>
                ) : (
                    " "
                  )}
              </article>
            </article>
          ) : (
              ""
            )}

          {!this.state.isFormButtonShow ? (
            <article>
              <article className="paddingborder">
                <article className="border">&nbsp;</article>
              </article>

              <article>
                <article>
                  <article className="row">
                    {formConfig.accountHolderName != null &&
                      formConfig.accountHolderName.active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <input
                              type="text"
                              id="accountHolderName"
                              data-custom={formConfig.accountHolderName.custom}
                              onChange={this.formChangeHandler}
                              value={this.state.formData.accountHolderName || ""}
                              required
                            />
                            <label>{`${formConfig.accountHolderName.label
                              }`}</label>
                            {this.state.validations["accountHolderName"] ? (
                              <span className="error animated bounce">
                                {this.state.validations["accountHolderName"]}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      ) : (
                        ""
                      )}

                    {formConfig.accountNumber != null &&
                      formConfig.accountNumber.active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <input
                              type="text"
                              id="accountNumber"
                              data-custom={formConfig.accountNumber.custom}
                              onChange={this.formChangeHandler}
                              value={this.state.formData.accountNumber || ""}
                              required
                            />
                            <label>{`${formConfig.accountNumber.label}`}</label>
                            {this.state.validations["accountNumber"] ? (
                              <span className="error animated bounce">
                                {this.state.validations["accountNumber"]}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      ) : (
                        ""
                      )}


                    {formConfig.ifscCode != null &&
                      formConfig.ifscCode.active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <input
                              type="text"
                              id="ifscCode"
                              name="ifscCode"
                              data-custom={formConfig.ifscCode.custom}
                              onChange={(e) => this.handleChange(e)}
                              value={this.state.formData.ifscCode || ""}
                              maxLength="11"
                              required
                            />
                            <label className="labelstyle">{`${formConfig.ifscCode.label
                              }`}  </label>


                            {this.state.validations["ifscCode"] ? (
                              <span className="error animated bounce">
                                {this.state.validations["ifscCode"]}
                              </span>
                            ) : null}

                          </article>
                          <article class="col-md-12">
                            <a
                              className="ifsc-link-gaze"
                              rel="nofollow"
                              href="https://www.rbi.org.in/Scripts/IFSCMICRDetails.aspx"
                              target="_blank"
                            >
                              Get IFSC Code Here
                             </a>
                          </article>
                        </article>

                      ) : (
                        ""
                      )}

                    {formConfig.bankName != null &&
                      formConfig.bankName.active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <input
                              type="text"
                              id="bankName"
                              data-custom={formConfig.bankName.custom}
                              onChange={this.formChangeHandler}
                              disabled={true}
                              value={this.state.formData.bankName || ""}
                            />
                            <label className="labelstyle">{`${formConfig.bankName.label
                              }`}</label>

                            {this.state.validations["bankName"] ? (
                              <span className="error animated bounce">
                                {this.state.validations["bankName"]}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      ) : (
                        ""
                      )}
                    {formConfig.bankAccountType != null &&
                      formConfig.bankAccountType.active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <select
                              className="icon"
                              value={this.state.formData.bankAccountType || ""}
                              id="bankAccountType"
                              onChange={this.formChangeHandler}
                            >
                              <option value="">-Select-</option>
                              {formConfig.bankAccountType.dataOptions != null
                                ? formConfig.bankAccountType.dataOptions.map(
                                  y => (
                                    <option key={y.id} value={y.id}>
                                      {y.rulevalue}
                                    </option>
                                  )
                                )
                                : null}
                              {/* {year.map(y => (
                      <option key={y} value={y}>
                        {y}
                      </option>
                    ))} */}
                            </select>
                            <label className="labelstyle">
                              {formConfig.bankAccountType.label}
                            </label>
                            {this.state.validations["bankAccountType"] ? (
                              <span className="error animated bounce">
                                {this.state.validations["bankAccountType"]}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      ) : null}

                    {formConfig.branchName != null &&
                      formConfig.branchName.active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <input
                              type="text"
                              id="branchName"
                              data-custom={formConfig.branchName.custom}
                              onChange={this.formChangeHandler}
                              disabled={true}
                              value={this.state.formData.branchName || ""}
                            />
                            <label className="labelstyle">
                              {formConfig.branchName.label}
                            </label>
                            {this.state.validations["branchName"] ? (
                              <span className="error animated bounce">
                                {this.state.validations["branchName"]}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      ) : (
                        ""
                      )}

                    {formConfig.relationId != null &&
                      formConfig.relationId.active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <select
                              className="icon"
                              value={this.state.formData.relationId || ""}
                              id="relationId"
                              onChange={this.formChangeHandler}
                            >
                              <option value="">-Select-</option>
                              {formConfig.relationId.dataOptions != null
                                ? formConfig.relationId.dataOptions.map(y => (
                                  <option key={y.id} value={y.id}>
                                    {y.rulevalue}
                                  </option>
                                ))
                                : null}
                              {/* {year.map(y => (
                      <option key={y} value={y}>
                        {y}
                      </option>
                    ))} */}
                            </select>
                            <label className="labelstyle">
                              {formConfig.relationId.label}
                            </label>
                            {this.state.validations["relationId"] ? (
                              <span className="error animated bounce">
                                {this.state.validations["relationId"]}
                              </span>
                            ) : null}
                          </article>
                        </article>
                      ) : null}
                  </article>
                  <article className="row">
                    <article className="col-md-12 text-right">
                      <input
                        type="submit"
                        value="Save"
                        className="btn"
                        onClick={this.submit}
                      /> &nbsp; &nbsp;
                      <button
                        onClick={this.formClose}
                        className="btn"
                      >
                        Close
                      </button>
                    </article>
                  </article>
                </article>
              </article>
            </article>
          ) : (
              ""
            )}
        </article>
      </section>
    );
  }
}

export default BankDetails;
