import React, { Component } from "react";

class LoginFormNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      mobile: "",
      email: "",
      password: "",
      conf_password: "",
      isFormValid: false,
      validations: {
        firstName: null,
        lastName: null,
        mobile: null,
        email: null,
        password: null,
        conf_password: null
      }
    };
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
  }

  onFormFieldChange(event) {
    const { id, value } = event.target;

    this.setState({
      [id]: value
    });
  }

  render() {
    return (
      // <article className="bglogin pull-left paddingBoth">
      //   <article className="applicationform register">
      //     <article>
      //       <h2>Please Log in/Register to start application</h2>

      //       <form name="regForm">
      //         <section className="ctrl-wrapper">
      //           <article className="form-group">
      //             <input
      //               type="text"
      //               className="form-control"
      //               maxLength="20"
      //               minLength="3"
      //               name="first_name"
      //               placeholder="Enter Email Address"
      //               id="firstName"
      //               value={this.state.firstName}
      //               onChange={this.onFormFieldChange}
      //               autoComplete="off"
      //               tabIndex="3"
      //             />
      //             {this.state.validations.firstName ? (
      //               <span className="error animated bounce">
      //                 this.state.validations.firstName
      //               </span>
      //             ) : null}
      //           </article>
      //         </section>
      //         <section className="ctrl-wrapper">
      //           <article className="form-group">
      //             <input
      //               type="text"
      //               className="form-control"
      //               maxLength="20"
      //               minLength="3"
      //               name="password"
      //               placeholder="Password"
      //               onChange={this.onFormFieldChange}
      //               value={this.state.lastName}
      //               id="password"
      //               autoComplete="off"
      //               required=""
      //               tabIndex="4"
      //             />
      //           </article>
      //         </section>

      //         <section className="ctrl-wrapper">
      //           <article className="form-group">
      //             <button
      //               type="submit"
      //               className="btn-block greenBtn margintop"
      //               tabIndex="10"
      //             >
      //               Sign In!
      //             </button>
      //           </article>
      //         </section>
      //       </form>

      //       <article className="textPara">
      //         <dd>
      //           By clicking on any of the buttons, you agree to Buddy4Study's
      //           &nbsp;
      //           <a href="/terms-and-conditions" target="_blank">
      //             terms &amp; conditions
      //           </a>
      //         </dd>
      //         <article className="textPara">
      //           <p>
      //             <i>New here? Create a</i> &nbsp;
      //             <a href="" className="forgotpwd-link">
      //               Buddy4Study account
      //             </a>.
      //           </p>
      //           <a href="javascript:void(0)" className="forgotpwd-link">
      //             Forgot password
      //           </a>
      //         </article>
      //       </article>
      //     </article>
      //   </article>
      // </article>
      <article className="bglogin pull-left paddingBoth">
        <article className="applicationform register">
          <article>
            <h2>Please Log in/Register to start application</h2>
            <h3>Forgot Login Password?</h3>
            <p>
              Enter your e-mail below and we will send you reset instructions!
            </p>
            <form name="regForm" autoCapitalize="off">
              <section className="ctrl-wrapper">
                <article className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    maxLength="20"
                    minLength="3"
                    name="first_name"
                    placeholder="Enter Email Address"
                    id="firstName"
                    value={this.state.firstName}
                    onChange={this.onFormFieldChange}
                    autoComplete="off"
                    tabIndex="3"
                  />
                  {this.state.validations.firstName ? (
                    <span className="error animated bounce">
                      this.state.validations.firstName
                    </span>
                  ) : null}
                </article>
              </section>

              <section className="ctrl-wrapper">
                <article className="form-group">
                  <button
                    type="submit"
                    className="btn-block greenBtn margintop"
                    tabIndex="10"
                  >
                    Send
                  </button>
                </article>
              </section>
            </form>

            <article className="textPara">
              <dd>
                By clicking on any of the buttons, you agree to Buddy4Study's
                &nbsp;
                <a href="/terms-and-conditions" target="_blank">
                  terms &amp; conditions
                </a>
              </dd>
              <article className="textPara">
                <p>
                  <i>New here? Create a</i> &nbsp;
                  <a href="" className="forgotpwd-link">
                    Buddy4Study account
                  </a>.
                </p>
                <a href="javascript:void(0)" className="forgotpwd-link">
                  Forgot password
                </a>
              </article>
            </article>
          </article>
        </article>
      </article>
    );
  }
}

export default LoginFormNew;
