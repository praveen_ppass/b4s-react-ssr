import React, { Component } from "react";
import index from "axios";
import { config } from "../../../formconfig";
import { minLength, required } from "../../../../../validation/rules";
import { ruleRunner } from "../../../../../validation/ruleRunner";
import AlertMessage from "../../../../common/components/alertMsg";
import Loader from "../../../../common/components/loader";

class Questions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questionList: [],
      isFormValid: false,
      validations: {},
      showLoader: false,
      msg: "",
      status: "",
      extraValidation: {}
    };
    this.onChangeValue = this.onChangeValue.bind(this);
    this.submitQuestion = this.submitQuestion.bind(this);
    this.countWords = this.countWords.bind(this);
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    if (this.state.validations.hasOwnProperty(fieldID)) {
      if (this.state.extraValidation.hasOwnProperty(fieldID)) {
        validationObject.name = "*Field";
        if (
          [31, 82, 84, 88, 89, 102, 103, 104, 105, 106, 107, 108, 109].indexOf(
            parseInt(fieldID)
          ) === -1
        ) {
          validationObject.validationFunctions = [required, minLength(25)];
        } else {
          validationObject.validationFunctions = [required];
        }
      } else {
        validationObject.name = "*Field";
        validationObject.validationFunctions = [required];
      }
    }

    /*     if(this.state.extraValidation.hasOwnProperty(fieldID)){
      validationObject.name = "*Fields is Required";
      validationObject.validationFunctions = [maxLength(25)];
    } */
    return validationObject;
  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);

      let questionVal = this.state.questionList
        .filter(res => {
          return res.question["id"] == key;
        })
        .map(item => {
          if (item.question["questionTypeId"] == 1) {
            return item.response;
          } else if (item.question["questionTypeId"] == 2) {
            return item.optionId != null && item.optionId.length > 0
              ? item.optionId
              : null;
          } else {
            return item.optionId != null && item.optionId.length > 0
              ? item.optionId
              : null;
          }
        });

      let validationResult = ruleRunner(
        questionVal[0],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  componentDidMount() {}

  getQuestion() {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(window.localStorage.getItem("userId"));
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    this.props.getQuestionData({
      userId: userId,
      scholarshipId: scholarshipId
    });
  }

  componentWillMount() {
    this.getQuestion();
  }

  submitQuestion() {
    let isSubmit = this.checkFormValidations();
    if (isSubmit) {
      let userId = "";
      let scholarshipId = "";
      if (window != undefined) {
        userId = parseInt(window.localStorage.getItem("userId"));
        scholarshipId = parseInt(
          window.localStorage.getItem("applicationSchID")
        );
      }
      this.props.saveQuestionData({
        userId: userId,
        scholarshipId: scholarshipId,
        question: this.state.questionList
      });
    }
  }

  onChangeValue(event) {
    const { id, value } = event.target;
    let validations = { ...this.state.validations };

    const flag = this.countWords(value);
    if (flag) {
      return;
    } else {
      if (validations.hasOwnProperty(id)) {
        const { name, validationFunctions } = this.getValidationRulesObject(id);

        const validationResult = ruleRunner(
          value,
          id,
          name,
          ...validationFunctions
        );
        validations[id] = validationResult[id];
      }

      if (this.state.questionList != null) {
        this.state.questionList
          .filter(res => {
            return res.question["id"] == id;
          })
          .map(items => {
            let index = this.state.questionList.findIndex(indexVal => {
              return indexVal.question["id"] == items.question["id"];
            });
            if (this.state.questionList[index].question.questionTypeId == 1) {
              this.state.questionList[index]["response"] = value;
            } else if (
              this.state.questionList[index].question.questionTypeId == 2
            ) {
              this.state.questionList[index]["optionId"] = Array(value);
            } else if (
              this.state.questionList[index].question.questionTypeId == 4
            ) {
              this.state.questionList[index]["optionId"] = Array(value);
            } else {
              if (event.target.checked) {
                this.state.questionList[index]["optionId"].push(
                  parseInt(value)
                );
                this.state.questionList[index]["optionId"] = [
                  ...new Set(this.state.questionList[index]["optionId"])
                ];
              } else {
                let multiIndexVal = this.state.questionList[index][
                  "optionId"
                ].indexOf(parseInt(value));
                if (multiIndexVal > -1) {
                  this.state.questionList[index]["optionId"].splice(
                    multiIndexVal,
                    1
                  );
                }
              }
            }
            this.setState({
              questionList: this.state.questionList,
              validations
            });
          });
      }
    }
  }

  countWords(str) {
    let counter = str.trim().split(/\s+/).length;
    if (counter > 600) {
      return true;
    } else {
      return false;
    }
  }

  componentWillReceiveProps(nextProps) {
    const { type, applicationQuestionData } = nextProps.QuestionDataList;

    switch (type) {
      case "FETCH_APPLICATION_FORMS_QUESTION_SUCCESS":
        if (applicationQuestionData) {
          this.setState({
            questionList: applicationQuestionData
          });

          applicationQuestionData.map(res => {
            if (res["question"].isMandatory) {
              this.state.validations[res.question["id"]] = null;
            }
            if (
              res["question"].isMandatory &&
              res.question["questionTypeId"] == 1
            ) {
              // extra only for the subjective fields
              this.state.extraValidation[res.question["id"]] = null;
            }
          });
        }
        break;
      case "UPDATE_APPLICATION_FORMS_QUESTION_SUCCESS":
        let scholarshipId = "";
        if (window != undefined) {
          scholarshipId = parseInt(
            window.localStorage.getItem("applicationSchID")
          );
        }
        if (
          this.props.match &&
          this.props.match.params &&
          this.props.match.params.bsid &&
          this.props.redirectionSteps &&
          this.props.redirectionSteps.length
        ) {
          this.props.history.push(
            `/application/${this.props.match.params.bsid}/form/${
              this.props.redirectionSteps[
                this.props.redirectionSteps.indexOf("questions") + 1
              ]
            }`
          );
        }

        this.setState(
          {
            status: true,
            msg: "Record has been Updated",
            showLoader: true
          },
          () => this.getQuestion()
        );
        setTimeout(() => {
          this.props.applicationInstructionStep({
            scholarshipId: scholarshipId
          });
        }, 1000);
        break;
    }
  }

  render() {
    return (
      <section className="sectionwhite">
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <article className="form">
          <article>
            <article>
              <article className="row">
                <article className="col-md-12">
                  <p>
                    {" "}
                    &#9679; Please answer the{" "}
                    <b>complete set of mandatory questions </b>with respect to
                    the selected <b>crisis category </b>(relevant set of
                    questions are listed below)
                  </p>
                  <p>
                    {" "}
                    &#9679; Please note that Q1 is mandatory for all crisis
                    categories.
                  </p>

                  <article className="table-responsive">
                    <table
                      className="table table-striped  table-bordered"
                      border="1"
                    >
                      <thead>
                        <tr>
                          <th> Type of Crisis</th>
                          <th>Mandatory Questions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Death in Family </td>
                          <td>Q2, Q3, Q4 </td>
                        </tr>
                        <tr>
                          <td>Job Loss in Family </td>
                          <td> Q5, Q6, Q7, Q8, Q9</td>
                        </tr>
                        <tr>
                          <td>Orphans/Single Parent/Deserted Children</td>
                          <td> Q10, Q11 </td>
                        </tr>
                        <tr>
                          <td>Self-employment Failure</td>
                          <td> Q12, Q13, Q14, Q15, Q16</td>
                        </tr>
                        <tr>
                          <td>Expenditure on Medical Treatments</td>
                          <td> Q17, Q18, Q19, Q20 </td>
                        </tr>
                      </tbody>
                    </table>
                  </article>
                  <p>
                    {" "}
                    &#9679; Please select <b>NA</b> for all the{" "}
                    <b> not applicable questions</b> before proceeding.{" "}
                  </p>
                  <hr />
                </article>

                <article className="col-md-12">
                  {this.state.questionList != null
                    ? this.state.questionList.map(res => {
                        if (res.question["questionTypeId"] == 1) {
                          // if question type subjective
                          return (
                            <article
                              className="form-group textareaque"
                              key={res.question["id"]}
                            >
                              <label htmlFor={res.question["id"]}>
                                {res.question["questionVerbiage"]}
                              </label>
                              <textarea
                                value={res.response != null ? res.response : ""}
                                onChange={event => this.onChangeValue(event)}
                                id={res.question["id"]}
                                row="5"
                              />

                              {this.state.validations[res.question["id"]] ? (
                                <span className="error animated bounce">
                                  {this.state.validations[res.question["id"]]}
                                </span>
                              ) : null}
                            </article>
                          );
                        } else if (res.question["questionTypeId"] == 2) {
                          // if question type single choice

                          return (
                            <article
                              className="form-group radio-btn"
                              key={res.question["id"]}
                            >
                              <article className="que">
                                <label htmlFor={res.question["id"]}>
                                  {res.question["questionVerbiage"]}
                                </label>
                              </article>
                              {res.question.questionOptions.map((option, i) => {
                                return (
                                  <div key={i}>
                                    <strong>
                                      <label>({option.optionKey})</label>
                                    </strong>
                                    <span>
                                      <label>
                                        <input
                                          type="radio"
                                          key={option["id"]}
                                          value={
                                            option["id"] != null
                                              ? option["id"]
                                              : ""
                                          }
                                          onChange={event =>
                                            this.onChangeValue(event)
                                          }
                                          id={res.question["id"]}
                                          name={res.question["id"] + "_val"}
                                          checked={
                                            res["optionId"][0] == option["id"]
                                              ? true
                                              : false
                                          }
                                        />
                                        <span />
                                        <i> {option.optionValue}</i>
                                      </label>
                                    </span>
                                  </div>
                                );
                              })}

                              {this.state.validations[res.question["id"]] ? (
                                <article className="form-group">
                                  <span className="error animated bounce">
                                    {this.state.validations[res.question["id"]]}
                                  </span>
                                </article>
                              ) : null}
                              <article className="paddingborder">
                                <span>&nbsp;</span>
                                <article className="border" />
                              </article>
                            </article>
                          );
                        } else if (res.question["questionTypeId"] == 4) {
                          return (
                            <article
                              className="form-group  width radio-btn"
                              key={res.question["id"]}
                            >
                              <article className="que">
                                <label htmlFor={res.question["id"]}>
                                  {res.question["questionVerbiage"]}
                                </label>
                              </article>
                              {}{" "}
                              <select
                                className="icon"
                                id={res.question["id"]}
                                name={res.question["id"] + "_val"}
                                onChange={event => this.onChangeValue(event)}
                                value={res.optionId.join("")}
                              >
                                <option value="">-Select-</option>
                                {res.question.questionOptions.map(option => {
                                  return (
                                    <option key={option.id} value={option.id}>
                                      {option.optionValue}
                                    </option>
                                  );
                                })}
                              </select>
                              {this.state.validations[res.question["id"]] ? (
                                <article className="form-group">
                                  <span className="error animated bounce">
                                    {this.state.validations[res.question["id"]]}
                                  </span>
                                </article>
                              ) : null}
                              <article className="paddingborder">
                                <span>&nbsp;</span>
                                <article className="border" />
                              </article>
                            </article>
                          );
                        } else {
                          // if the question type multiple choice

                          return (
                            <article
                              className="form-group checkbox"
                              key={res.question["id"]}
                            >
                              <article className="que">
                                <div htmlFor={res.question["id"]}>
                                  {res.question["questionVerbiage"]}
                                </div>
                              </article>
                              {res.question.questionOptions.map((option, i) => {
                                return (
                                  <div key={i}>
                                    <strong>
                                      <span>({option.optionKey})</span>
                                    </strong>

                                    <span>
                                      <label>
                                        <input
                                          key={option["id"]}
                                          type="checkbox"
                                          value={
                                            option["id"] != null
                                              ? option["id"]
                                              : ""
                                          }
                                          onChange={event =>
                                            this.onChangeValue(event)
                                          }
                                          id={res.question["id"]}
                                          defaultChecked={
                                            res["optionId"].indexOf(
                                              option["id"]
                                            ) > -1
                                              ? true
                                              : false
                                          }
                                        />
                                        <span />
                                        <i>{option.optionValue}</i>
                                      </label>
                                    </span>
                                  </div>
                                );
                              })}

                              {this.state.validations[res.question["id"]] ? (
                                <article className="form-group">
                                  <span className="error animated bounce">
                                    {this.state.validations[res.question["id"]]}
                                  </span>
                                </article>
                              ) : null}
                              <article className="paddingborder">
                                <span>&nbsp;</span>
                                <article className="border" />
                              </article>
                            </article>
                          );
                        }
                      })
                    : ""}
                </article>
              </article>

              <article className="row">
                <article className="col-md-12">
                  <input
                    type="submit"
                    value="Save & Continue"
                    className="btn  pull-right"
                    onClick={this.submitQuestion}
                  />
                </article>
              </article>
            </article>
          </article>
        </article>
      </section>
    );
  }
}

export default Questions;
