import React, { Component } from "react";
import { connect } from "react-redux";
import moment from 'moment';
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import gblFunc from "../../../../../globals/globalFunctions";
import {
  handleConditionalValidations,
  otherStateId,
  graduationId,
  otherStreamIdGraduation,
  otherStreamIdTwelfth,
  nextStepName,
  config,
  educationSpecialValidations,
  addSpecialValidations,
  elvenId,
  twelthId,
  classTenthId,
  otherBoardId,
  otherStreamIdEleven,
  postGradId,
  otherStreamIdPostGraduation,
  twelthPassId,
  extractStepTemplate,
  childEduBSID
} from "../../../formconfig";
import {
  FETCH_EDUCATION_INFO_FORM_SUCCESS,
  UPDATE_EDUCATION_INFO_FORM_SUCCESS,
  UPDATE_EDUCATION_INFO_FORM_FAILURE,
  FETCH_EDUCATION_INFO_CONFIG_SUCCESS,
  UPDATE_EDUCATION_INFO_STEP_SUCCESS,
  UPDATE_EDUCATION_INFO_STEP_FAILURE,
  FETCH_EDUCATION_CLASS_CONFIG_SUCCESS,
  DELETE_EDUCATION_INFO_SUCCESS,
  FETCH_EDUCATION_CLASS_CONFIG_FAILURE,
  PRESENT_CLASS_INFO_SUCCESS,
  fetchPresentClass as fetchPresentClassAction
} from "../../../actions/educationInfoAction";
import AcadmicClasses from "../../../../application/components/customForms/GenericPage/AcademicClass";
import { getYearOrMonth } from "../../../../../constants/constants";
import Loader from "../../../../common/components/loader";
import AlertMessage from "../../../../common/components/alertMsg";
import { mapValidationFunc } from "../../../../../validation/rules";
import { ruleRunner } from "../../../../../validation/ruleRunner";
import { FETCH_SCHOLARSHIP_APPLY_SUCCESS } from "../../../../../constants/commonActions";
import ConfirmMessagePopup from "../../../../common/components/confirmMessagePopup";
import { APPLICATION_FETCH_PERSONAL_INFO_SUCCEEDED } from "../../../actions/personalInfoActions";
import { filter } from "compression";
import EducationVideoPopup from "./EducationVideoPopup";


class EducationInfo extends Component {
  constructor(props) {
    super(props);
    this.checker = true;
    this.editEduction = this.editEduction.bind(this);
    this.onSelectClassHandler = this.onSelectClassHandler.bind(this);
    this.educationFormHandler = this.educationFormHandler.bind(this);
    this.marksObtainedHandler = this.marksObtainedHandler.bind(this);
    this.onEducationSubmitHandler = this.onEducationSubmitHandler.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.fireEducationConfigCall = this.fireEducationConfigCall.bind(this);
    this.goToNextStep = this.goToNextStep.bind(this);
    this.ifAllClassesFilled = this.ifAllClassesFilled.bind(this);
    this.ifClassDetailsAreCompleted = this.ifClassDetailsAreCompleted.bind(
      this
    );
    this.onSelectPresentClassHandler = this.onSelectPresentClassHandler.bind(
      this
    );

    this.onDeleteEducation = this.onDeleteEducation.bind(this);
    this.hideConfirmationPopup = this.hideConfirmationPopup.bind(this);
    this.onDeleteEducationListHandler = this.onDeleteEducationListHandler.bind(
      this
    );
    this.onEditEducationHandler = this.onEditEducationHandler.bind(this);
    this.degreeFilterDataOptions = this.degreeFilterDataOptions.bind(this);
    this.closeEducationTab = this.closeEducationTab.bind(this);
    this.checkCompletedApplicationStatus = this.checkCompletedApplicationStatus.bind(
      this
    );
    this.handleExistingClass = this.handleExistingClass.bind(this);
    this.handleClassStatus = this.handleClassStatus.bind(this);
    this.onChangeHanlder = this.onChangeHanlder.bind(this);


    // this.checkClassIdHandler = this.checkClassIdHandler.bind(this);
    // this.isStepsCompleted = this.isStepsCompleted.bind(this);

    this.brandVideo = this.brandVideo.bind(this);
    this.brandVideoClose = this.brandVideoClose.bind(this);

    this.state = {
      isOpenVideo: false,
      verifiedPresentClass: null,
      activeTab: null,
      completingStep: false,
      childVisible: true,
      deleteClassId: null,
      isClassIdDeleteable: false,
      applicationEducationData: null,
      classID: null,
      presentClassID: null,
      marksOrCgpa: true,
      isEditable: false,
      showLoader: false,
      deleteClass: null,
      isFormValid: true,
      onSelectClassMode: true,
      msgClassName: "",
      msg: "",
      status: false,
      stepUpdated: false,
      isStepCompleted: false,
      classConfigList: [],
      classConfigFields: null,
      configFieldsData: {},
      showConfirmationPopup: false,
      deleteSuccessCallBack: null,
      validations: {},
      defultValidations: {},
      editableConfig: null,
      educationList: {},
      eduStatus: "",
      eduState: {
        customData: {},
        userAcademicInfo: {
          academicClass: {
            id: "",
            value: ""
          },
          academicClassName: "",
          rollNo: "",
          board: "",
          boardName: "",
          courseDuration: "",
          courseStartDate: "",
          currentDegreeYear: "",
          degree: "",
          fee: "",
          grade: "",
          id: "",
          markingType: "1",
          marksObtained: "",
          otherBoard: "",
          passingMonth: "",
          passingYear: "",
          percentage: "",
          presentClass: 0,
          stream: "",
          totalMarks: ""
        },
        userInstituteInfo: {
          academicDetailId: "",
          address: "",
          city: "",
          country: "",
          description: "",
          district: "",
          districtName: "",
          id: "",
          instituteEmail: "",
          instituteName: "",
          institutePhone: "",
          pincode: "",
          principalName: "",
          state: "",
          stateName: ""
        }
      },
      eduInstructionTemplate: "",
      optionalClasses: []
    };
  }
  onClick() {
    this.setState(prevState => ({
      childVisible: !prevState.childVisible
    }));
  }

  componentDidMount() {
    const scholarshipId = gblFunc.getStoreApplicationScholarshipId();
    if (!!scholarshipId) {
      this.props.fetchPresentClass(scholarshipId);
    }
    if (this.props.schApply) {
      //If educatrion tab is not opened directly........@Pushpendra
      this.fireEducationConfigCall(this.props);
    }
  }

  closeEducationTab() {
    this.setState({ onSelectClassMode: false, isEditable: false, classID: "", activeTab: null });
  }

  degreeFilterDataOptions() {
    let data = [];
    if (this.props.classConfig && this.props.classConfig.length > 0) {
      this.props.classConfig.forEach(d => {
        if (
          d.classId == this.state.classID &&
          d.fields &&
          d.fields.degree &&
          d.fields.degree.dataOptions &&
          d.fields.degree.dataOptions.length > 0
        ) {
          d.fields.degree.dataOptions.forEach(f => {
            if (f.parentRuleId == this.state.classID) {
              data.push(f);
            }
          });
        }
      });
    }
    if (data.length === 0) {
      data = this.props.filterRules;
    }
    return data;
  }

  fireEducationConfigCall(nextProps) {
    this.props.getEducationConfig({
      scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
      step: "EDUCATION_INFO",
      scholarshipType: nextProps.schApply
        ? nextProps.schApply.scholarshipType
        : ""
    });
  }
  componentDidUpdate() {
    const { type } = this.props;
    switch (type) {
      case FETCH_EDUCATION_INFO_FORM_SUCCESS:
        this.checkCompletedApplicationStatus();
        break;
      default:
        break;
    }
  }

  componentWillReceiveProps(nextProps) {
    const { type, customType } = nextProps;
    switch (type) {
      case PRESENT_CLASS_INFO_SUCCESS: {
        if (
          nextProps.presentClassInfo &&
          nextProps.presentClassInfo.presentClass
          // nextProps.presentClassInfo.length > 0 &&
          // nextProps.presentClassInfo[0].userAcademicInfo &&
          // nextProps.presentClassInfo[0].userAcademicInfo.academicClass
        ) {
          return this.setState({ verifiedPresentClass: nextProps.presentClassInfo.presentClass })
        }
      }
      case FETCH_EDUCATION_INFO_CONFIG_SUCCESS:
        this.props.fetchApplicationPersonalInfo({
          userId: gblFunc.getStoreUserDetails()["userId"],
          scholarshipId: gblFunc.getStoreApplicationScholarshipId()
        });
        break;
      case APPLICATION_FETCH_PERSONAL_INFO_SUCCEEDED:
        const userinfo = nextProps.personalInfoData;
        const { academic_class } = !!nextProps.educationConfigInfo ? nextProps.educationConfigInfo : "";
        if (!!academic_class) {
          // const { userRules } = userinfo;
          // let presentClassInfo = userRules.filter(rule => rule.ruleId == nextProps.presentClassInfo.presentClass);
          if (!!nextProps.presentClassInfo && !!nextProps.presentClassInfo.presentClass) {
            if (
              academic_class &&
              academic_class.dataOptions &&
              academic_class.dataOptions.length
            ) {
              let filteredPresentClass = academic_class.dataOptions.filter(
                option => option.id == nextProps.presentClassInfo.presentClass
              );
              if (filteredPresentClass && filteredPresentClass.length > 0) {
                let event = {
                  target: {
                    value: filteredPresentClass[0].id
                  }
                };
                this.onSelectPresentClassHandler(event);
              }
            }
          }
        }

        break;
      case FETCH_EDUCATION_INFO_FORM_SUCCESS:
        const { applicationEducationData } = nextProps;
        let event = {
          target: {
            value: this.state.presentClassID
          }
        };
        if (
          applicationEducationData &&
          applicationEducationData.userEducationList &&
          applicationEducationData.userEducationList.educationDetails &&

          applicationEducationData.userEducationList.educationDetails.length
        ) {
          const educations = applicationEducationData.userEducationList.educationDetails;
          const classes = {};
          let ClassUpdatedRecently = false;
          let UserPresentUpdatedValue = 0;
          educations.map(e => {
            if (
              e.userAcademicInfo &&
              e.userAcademicInfo.presentClass == 1 &&
              this.state.onSelectClassMode
            ) {
              classes[this.state.presentClassID] = {
                deleteClassId: e.userAcademicInfo.id,
                value: e.userAcademicInfo.academicClass.value,
                id: e.userAcademicInfo.academicClass.id,
                presentClass: e.userAcademicInfo.presentClass,
                eduList: e
              };
            }

            if (
              e.userAcademicInfo &&
              e.userAcademicInfo.presentClass != 1 &&
              e.userAcademicInfo.academicClass.id ==
              this.state.presentClassID &&
              this.state.onSelectClassMode
            ) {
              classes[this.state.presentClassID] = {
                deleteClassId: e.userAcademicInfo.id,
                value: e.userAcademicInfo.academicClass.value,
                id: e.userAcademicInfo.academicClass.id,
                presentClass: e.userAcademicInfo.presentClass,
                eduList: e
              };
            }

            if (e.userAcademicInfo.isUpdatable) {
              ClassUpdatedRecently = e.userAcademicInfo.isUpdatable;
            }
            if (e.userAcademicInfo.userCurrentClassId) {
              UserPresentUpdatedValue = e.userAcademicInfo.userCurrentClassId;
            }

          });
          // Map closed here condition started
          // if (UserPresentUpdatedValue == this.state.presentClassID) {
          //   // Nothing to do with current class just show them
          // } else {
          //   if (!ClassUpdatedRecently) {
          //     this.setState({
          //       showLoader: true,
          //       classConfigList: [],
          //       msg: `You have updated your present class on Buddy4Study within past 4 months. 
          //       Changing present class impacts your existing applications on Buddy4Study, hence presently it is not allowed.
          //       Please contact info@buddy4study.com if you still need further help.`
          //     });
          //   }
          // }
          // if (classes && Object.keys(classes).length) {
          //   //this.onDeleteEducation(classes, this.state.presentClassID);
          // }

          if (this.state.presentClassID == 22) {

            let { optionalClassesList } = this.state;
            let filteredClasses = optionalClassesList && optionalClassesList.length > 0 && optionalClassesList.filter(cls => cls.isOptional == true && cls.classId);
            filteredClasses && filteredClasses.length > 0 && filteredClasses.map((item, i) => {
              const checkUsername = obj => obj.academicClassName === item.className;
              if (educations.some(checkUsername)) {
                let event = {
                  target: {
                    checked: true
                  }
                };
                this.onChangeHanlder(
                  event,
                  item
                )

              }
            })
          }
        }
        this.setState(
          {
            applicationEducationData
          },
          () => {
            if (
              this.state.classConfigList &&
              this.state.classConfigList.length < 2
            ) {
              this.onSelectClassHandler(this.state.presentClassID);
              if (
                applicationEducationData &&
                applicationEducationData.userEducationList &&
                applicationEducationData.userEducationList.educationDetails &&

                applicationEducationData.userEducationList.educationDetails.length < 1
              ) {
                this.setState({
                  isEditable: true
                });
              } else {
                this.setState({
                  isEditable: false
                });
              }
            }
          }
        );

        break;
      case FETCH_SCHOLARSHIP_APPLY_SUCCESS: //If education tab is opened directly.....@Pushpendra
        if (!this.props.educationConfigInfo) {
          this.fireEducationConfigCall(nextProps);
        }
        break;

      case UPDATE_EDUCATION_INFO_FORM_SUCCESS:
        this.setState({
          completingStep: true,
          status: true,
          msg: "Successfully Submitted",
          showLoader: true,
          eduState: {
            customData: {},
            userAcademicInfo: {
              academicClass: {
                id: "",
                value: ""
              },
              academicClassName: "",
              board: "",
              boardName: "",
              courseDuration: "",
              currentDegreeYear: "",
              degree: "",
              fee: "",
              grade: "",
              id: "",
              markingType: "1",
              marksObtained: "",
              otherBoard: "",
              passingMonth: "",
              passingYear: "",
              percentage: "",
              presentClass: 0,
              stream: "",
              totalMarks: ""
            },
            userInstituteInfo: {
              academicDetailId: "",
              address: "",
              city: "",
              country: "",
              description: "",
              district: "",
              districtName: "",
              id: "",
              instituteEmail: "",
              instituteName: "",
              institutePhone: "",
              pincode: "",
              principalName: "",
              state: "",
              stateName: ""
            }
          },
          isEditable: false,
          isStepCompleted: this.ifAllClassesFilled(true), //form submitted
          classID: ""
        });

        this.props.fetchEduInfo({
          scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
          step: "EDUCATION_INFO",
          presentClassId: this.state.presentClassID
        });

        break;
      case UPDATE_EDUCATION_INFO_FORM_FAILURE:
        let errCode = getNestedObjKey(nextProps.payload, ["response", "data", "errorCode"])
        let errMsg = getNestedObjKey(nextProps.payload, ["response", "data", "message"])
        if (errCode == 2001) {
          this.setState({
            status: false,
            msg: errMsg,
            showLoader: true,
            isEditable: this.state.isEditable ? true : false
          });
        } else {
          this.setState({
            status: false,
            msg: "Something went wrong, please try again!",
            showLoader: true,
            isEditable: this.state.isEditable ? true : false
          });
        }
        break;
      case UPDATE_EDUCATION_INFO_STEP_SUCCESS:
        if (this.state.stepUpdated) {
          this.setState({ stepUpdated: false });

          if (
            nextProps.updatedStep &&
            nextProps.updatedStep.step === "EDUCATION_INFO"
          ) {
            this.props.getCompletedSteps({
              userId: gblFunc.getStoreUserDetails()["userId"],
              scholarshipId: gblFunc.getStoreApplicationScholarshipId()
            });
          }

          setTimeout(() => {
            if (
              this.props.match.params.bsid &&
              this.props.redirectionSteps &&
              this.props.redirectionSteps.length
            ) {
              this.props.history.push(
                `/application/${this.props.match.params.bsid.toUpperCase()}/form/${this.props.redirectionSteps[
                this.props.redirectionSteps.indexOf("educationInfo") + 1
                ]
                }`
              );
            }
          }, 2000);
        }
        break;
      case UPDATE_EDUCATION_INFO_STEP_FAILURE:
        let errStepCode = getNestedObjKey(nextProps.error, ["response", "data", "errorCode"])
        let errStepMsg = getNestedObjKey(nextProps.error, ["response", "data", "message"])
        if (errStepCode == 2001) {
          this.setState({
            status: false,
            msg: errStepMsg,
            showLoader: true,
          });
        } else {
          this.setState({
            status: false,
            msg: "Something went wrong, please try again!",
            showLoader: true,
          });
        }
        break
      case FETCH_EDUCATION_CLASS_CONFIG_SUCCESS:
        let { profileLocked } = gblFunc.getStoreUserDetails();
        if (!this.state.isEditable) {
          this.setState(
            {
              classConfigList: nextProps.classConfig,
              optionalClassesList: nextProps.classConfig
            },
            () => {
              this.props.fetchEduInfo({
                scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
                step: "EDUCATION_INFO",
                presentClassId: this.state.presentClassID
              });
            }
          );
          return;
        } else {
          const configFieldsData = { ...this.state.configFieldsData };
          const validate = {};
          const classConfig = nextProps.classConfig.find(
            list => list.classId == this.state.classID
          );

          if (classConfig && Object.keys(classConfig).length > 0) {
            const validateFields = classConfig.fields;
            for (let key in validateFields) {
              if (
                validateFields[key].validations !== null &&
                validateFields[key].active
              ) {
                let clearUnderScore = gblFunc.replace_underScore(key);
                validate[clearUnderScore] = null;
                configFieldsData[clearUnderScore] = validateFields[key];
              }
            }
            this.setState(
              {
                eduState: this.state.educationList,
                showConfirmationPopup: false,
                defultValidations: validate,
                validations: validate,
                classConfigFields:
                  classConfig && classConfig.fields ? classConfig.fields : [],
                classID: this.state.classID,
                configFieldsData
              },
              () => {
                if (
                  this.state.educationList.userInstituteInfo &&
                  this.state.educationList.userInstituteInfo.state
                ) {
                  this.props.fetchDistrictList({
                    id: this.state.educationList.userInstituteInfo.state
                  });
                }
              }
            );
          }
        }

        break;
      case DELETE_EDUCATION_INFO_SUCCESS:
        this.setState(
          {
            showConfirmationPopup: false
          },
          () => {
            this.props.fetchEduInfo({
              scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
              step: "EDUCATION_INFO",
              presentClassId: this.state.presentClassID
            });
            this.props.getCompletedSteps({
              userId: gblFunc.getStoreUserDetails()["userId"],
              scholarshipId: gblFunc.getStoreApplicationScholarshipId()
            });
          }
        );
        break;
      case FETCH_EDUCATION_CLASS_CONFIG_FAILURE:
        this.setState({
          showConfirmationPopup: false,
          presentClassID: this.state.deleteClass
        });
        break;
      default:
        break;
    }
  }

  onDeleteEducation(classes, presentClssId, applicationEducationData) {
    const { id, presentClass, deleteClassId, value, eduList } = classes[
      presentClssId
    ];
    if (!eduList.userAcademicInfo.isUpdatable) {
      if (presentClass == 1 && id != presentClssId) {
        this.setState({
          showLoader: true,
          classConfigList: [],
          msg: `You have updated your present class on Buddy4Study within past 4 months which is ${value}.
           Changing present class impacts your existing applications on Buddy4Study, hence presently it is not allowed.
           Please contact info@buddy4study.com if you still need further help.`
        });

      }
    }
    else {
      if (presentClass == 1 && id != presentClssId) {
        this.setState({
          showConfirmationPopup: true,
          deleteClassId: deleteClassId,
          isClassIdDeleteable: true,
          deleteClass: id,
          msg: `You have already saved ${value} as a present class, would like to delete it ?`
        });
      }

      if (presentClass != 1) {
        this.setState({
          showConfirmationPopup: true,
          isClassIdDeleteable: true,
          deleteClassId: deleteClassId,
          deleteClass: id,
          msg: `You have already saved ${value}, would like to delete it ?`
        });
      }

    }
  }

  onEditEducationHandler(list, value) {
    const { userInstituteInfo, userAcademicInfo } = list;

    const validations = { ...this.state.validations };
    for (let key in validations) {
      validations[key] = null;
    }

    // this.props.getSubject({ id: list.userAcademicInfo.academicClass.id });
    this.setState({
      eduState: list,
      isEditable: true,
      validations,
      marksOrCgpa: list.userAcademicInfo.markingType == "1" ? true : false,
      classID: list.userAcademicInfo.academicClass.id
    });
  }

  onDeleteEducationListHandler() {
    if (this.state.isClassIdDeleteable) {
      this.props.deleteAcademic({
        userId: gblFunc.getStoreUserDetails()["userId"],
        scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
        classId: this.state.deleteClassId
      });
    }
  }

  ifAllClassesFilled(formSubmitted = 0) {
    //Called on form submission flag
    let flag = [];
    const { applicationEducationData } = this.state;
    if (applicationEducationData && applicationEducationData.userEducationList && applicationEducationData.userEducationList.educationDetails) {
      applicationEducationData && applicationEducationData.userEducationList.educationDetails.map(list => {
        flag.push(list.classSaved);
      });
    }
    return flag.length > 0 && (flag.indexOf(false) == -1) && (flag.indexOf(undefined) == -1);
  }

  getDistrictHandler(event) {
    this.educationFormHandler(event, "userInstituteInfo", "state");
  }

  onSelectClassHandler(event) {
    let value = null;
    if (event instanceof Object) {
      value = event.target.value;
    } else {
      value = event;
      this.checker = false;
    }
    let updatedValidation = { ...this.state.validations };

    const updatedConfigFieldsData = { ...this.state.configFieldsData };
    const updatedClassConfig = this.state.classConfigList.find(
      clss => clss.classId === parseInt(value, 10)
    );

    let e = {
      target: {
        id: updatedClassConfig && updatedClassConfig.classId,
        value: updatedClassConfig && updatedClassConfig.className
      }
    };
    // empty the previous validation if any
    if (Object.keys(updatedValidation).length) {
      updatedValidation = [];
    }

    if (updatedClassConfig && Object.keys(updatedClassConfig).length > 0) {
      const updateFields = updatedClassConfig && updatedClassConfig.fields;
      for (let key in updateFields) {
        if (
          updateFields[key].validations !== null &&
          updateFields[key].active
        ) {
          let clearUnderScore = gblFunc.replace_underScore(key);
          updatedValidation[clearUnderScore] = null;
          updatedConfigFieldsData[clearUnderScore] = updateFields[key];
        }
      }
    }

    this.setState(
      {
        classID: value,
        classConfigFields:
          updatedClassConfig && updatedClassConfig.fields
            ? updatedClassConfig.fields
            : null,
        isEditable: true,
        validations: updatedValidation,
        defultValidations: updatedValidation,
        eduState: {
          customData: {},
          userAcademicInfo: {
            academicClass: {
              id: "",
              value: ""
            },
            academicClassName: "",
            rollNo: "",
            board: "",
            boardName: "",
            courseDuration: "",
            currentDegreeYear: "",
            degree: "",
            fee: "",
            grade: "",
            id: "",
            markingType: "1",
            marksObtained: "",
            otherBoard: "",
            passingMonth: "",
            passingYear: "",
            percentage: "",
            presentClass: 0,
            stream: "",
            totalMarks: ""
          },
          userInstituteInfo: {
            academicDetailId: "",
            address: "",
            city: "",
            country: "",
            description: "",
            district: "",
            districtName: "",
            id: "",
            instituteEmail: "",
            instituteName: "",
            institutePhone: "",
            pincode: "",
            principalName: "",
            state: "",
            stateName: ""
          }
        },
        configFieldsData: updatedConfigFieldsData
      },
      () => {
        this.props.getSubject({ id: this.state.classID });

        this.props.fetchFilterRuleById(this.state.classID);
        this.educationFormHandler(e, "userAcademicInfo", "academicClass");
      }
    );
  }

  onSelectPresentClassHandler(event) {
    const { value } = event.target;
    //gblFunc.getStoreApplicationScholarshipId()
    this.setState(
      {
        isEditable: false,
        onSelectClassMode: true,
        presentClassID: value ? value : "",
        classID: ""
      },
      () => {
        if (this.state.presentClassID) {
          this.props.getConfigOfClss({
            scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
            classID: value
          });
        }
      }
    );
  }

  marksObtainedHandler(event) {
    const { value } = event.target;
    // const validations = { ...this.state.validations };
    const { userAcademicInfo } = { ...this.state.eduState };
    if (value && value == "1") {
      userAcademicInfo.grade = "";
      userAcademicInfo.marksObtained = "";
      userAcademicInfo.totalMarks = "";
      // validations["marksObtained"] = null;
      // validations["totalMarks"] = null;
      // validations["grade"] = null;
    } else {
      userAcademicInfo.grade = "";
      userAcademicInfo.marksObtained = "";
      userAcademicInfo.totalMarks = 10;
      // validations["grade"] = null;
      // validations["cgpa"] = null;
    }
    userAcademicInfo.markingType = value;
    this.setState({
      ...this.state.eduState,
      userAcademicInfo,
      //    validations,
      marksOrCgpa: value === "1" ? true : false
    });
  }

  handleClassStatus(cls) {
    const { applicationEducationData } = this.state;
    let flag = false;
    if (applicationEducationData && applicationEducationData.userEducationList && applicationEducationData.userEducationList.educationDetails) {
      applicationEducationData && applicationEducationData.userEducationList.educationDetails.map(list => {
        if (list.userAcademicInfo.academicClass.id == cls) {
          if (list.classSaved) {
            flag = list.classSaved;
          }
        }
      });
    }
    return flag;
  }

  handleExistingClass(cls) {
    const { applicationEducationData } = this.state;
    if (applicationEducationData && applicationEducationData.userEducationList && applicationEducationData.userEducationList.educationDetails) {
      applicationEducationData && applicationEducationData.userEducationList.educationDetails.map(list => {
        if (list.userAcademicInfo.academicClass.id == cls) {
          this.editEduction(list, true)
        }
      });
    }
  }

  editEduction(eduList) {
    const { userInstituteInfo, userAcademicInfo } = eduList;

    const validations = { ...this.state.validations };

    for (let key in validations) {
      validations[key] = null;
    }

    // this.props.getSubject({ id: eduList.userAcademicInfo.academicClass.id });
    this.setState(
      {
        educationList: eduList,
        isEditable: true,
        showConfirmationPopup: false,
        validations,
        marksOrCgpa: eduList.userAcademicInfo.markingType == "1" ? true : false,
        classID: eduList.userAcademicInfo.academicClass.id
      },
      () => {
        this.props.getConfigOfClss({
          scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
          classID: this.state.presentClassID,
          query: userAcademicInfo.academicClass.id
        });

        this.props.getSubject({ id: this.state.classID });
        this.props.fetchFilterRuleById(this.state.classID);
      }
    );
  }

  educationFormHandler(event, type, subType, validationName) {
    // event.preventDefault();
    const { value, id } = event.target;

    let validations = { ...this.state.validations };

    const updateEducationForm = {
      ...this.state.eduState
    };

    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(
        id,
        validationName
      );
      if (validationFunctions) {
        const validationResult = ruleRunner(
          value,
          id,
          name,
          ...validationFunctions
        );
        validations[id] = validationResult[id];
      }
    }

    // const updateFormData = { ...this.state.formData };
    // updateFormData[id] = value;
    // this.setState({
    //   formData: updateFormData,
    //   validations
    // });

    switch (type) {
      case "userAcademicInfo":
        if (subType && subType == "academicClass") {
          updateEducationForm.userAcademicInfo.academicClass = {
            id,
            value
          };
        } else {
          updateEducationForm.userAcademicInfo[subType] = value;
        }
        break;
      case "userInstituteInfo":
        if (subType == "state" && value) {
          this.props.fetchDistrictList({ id: value });
        }

        updateEducationForm.userInstituteInfo[subType] = value;
        break;
      case "customInfo":
        updateEducationForm.customData[subType] = value;
        break;
      default:
        break;
    }

    this.setState({
      eduState: updateEducationForm,
      validations
    });
  }

  getValidationRulesObject(fieldID, name = "*Field") {
    let validationObject = {};
    let validationRules = [];
    if (this.state.defultValidations.hasOwnProperty(fieldID)) {
      let configFields = this.state.configFieldsData[fieldID];
      if (configFields && configFields.validations != null) {
        let validationArr = configFields.validations;
        if (educationSpecialValidations.indexOf(fieldID) > -1) {
          addSpecialValidations(validationArr, fieldID, [this.state.eduState]);
        }
        validationArr.map(res => {
          if (res != null) {
            let validator = mapValidationFunc(res);
            if (validator != undefined) {
              validationRules.push(validator);
            }
          }
        });
        validationObject.name = name;
        validationObject.validationFunctions = validationRules;
      }
    }
    return validationObject;
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };

    for (let key in validations) {
      validations[key] = null;
    }
    /************* start Update conditional validations.........****/

    if (
      this.state.eduState.userAcademicInfo &&
      this.state.eduState.userAcademicInfo.currentAcademicYear == 1 &&
      this.state.eduState.userAcademicInfo.academicClass.id >= 21
    ) {
      validations = handleConditionalValidations(
        true,
        true,
        null,
        ["grade", "cgpa", "totalMarks", "marksObtained"],
        validations
      );
    }
    if (this.state.marksOrCgpa) {
      validations = handleConditionalValidations(
        this.state.marksOrCgpa,
        true,
        null,
        ["grade", "cgpa"],
        validations
      );
    } else {
      validations = handleConditionalValidations(
        this.state.marksOrCgpa,
        false,
        null,
        ["totalMarks", "marksObtained"],
        validations
      );

    }
    if (this.state.classID == elvenId) {
      validations = handleConditionalValidations(
        this.state.classID,
        elvenId,
        null,
        ["board", "otherBoard"],
        validations
      );
    }
    if (this.state.classID == graduationId) {
      validations = handleConditionalValidations(
        this.state.classID,
        graduationId,
        null,
        ["board", "otherBoard"],
        validations
      );
    }

    if (this.state.classID == classTenthId) {
      validations = handleConditionalValidations(
        this.state.classID,
        classTenthId,
        null,
        ["stream", "otherStream"],
        validations
      );
    }

    if (this.state.classID == postGradId) {
      validations = handleConditionalValidations(
        this.state.classID,
        postGradId,
        null,
        ["board", "otherBoard"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.degree != otherStreamIdGraduation &&
      this.state.classID == graduationId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.degree,
        otherStreamIdGraduation,
        "notEqual",
        ["otherStream"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.degree !=
      otherStreamIdPostGraduation &&
      this.state.classID == postGradId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.degree,
        otherStreamIdPostGraduation,
        "notEqual",
        ["otherStream"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.stream != otherStreamIdTwelfth &&
      this.state.classID == twelthId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.stream,
        otherStreamIdTwelfth,
        "notEqual",
        ["otherStream"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.stream != 753 &&
      this.state.classID == twelthPassId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.stream,
        753,
        "notEqual",
        ["otherStream"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.stream != otherStreamIdEleven &&
      this.state.classID == elvenId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.stream,
        otherStreamIdEleven,
        "notEqual",
        ["otherStream"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.board != 31 &&
      this.state.classID == twelthId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.board,
        31,
        "notEqual",
        ["otherBoard"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.board != 31 &&
      this.state.classID == twelthPassId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.board,
        31,
        "notEqual",
        ["otherBoard"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.board != 31 &&
      this.state.classID == classTenthId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.board,
        31,
        "notEqual",
        ["otherBoard"],
        validations
      );
    }

    if (this.state.eduState.userInstituteInfo.state != otherStateId) {
      validations = handleConditionalValidations(
        this.state.eduState.userInstituteInfo.state,
        otherStateId,
        "notEqual",
        ["otherState", "otherDistrict"],
        validations
      );
    }

    // if (
    //   this.state.eduState.userAcademicInfo.board != 31 &&
    //   this.state.classID == twelthPassId
    // ) {
    //   validations = handleConditionalValidations(
    //     this.state.eduState.userAcademicInfo.board,
    //     31,
    //     "notEqual",
    //     ["otherBoard"],
    //     validations
    //   );
    // }

    // if (
    //   this.state.eduState.userAcademicInfo.stream != 753 &&
    //   this.state.classID == twelthPassId
    // ) {
    //   validations = handleConditionalValidations(
    //     this.state.eduState.userAcademicInfo.stream,
    //     753,
    //     "notEqual",
    //     ["otherStream"],
    //     validations
    //   );
    // }

    // if (this.state.eduState.userAcademicInfo.stream == 244) {
    //   validations = handleConditionalValidations(
    //     this.state.eduState.userAcademicInfo.stream,
    //     244,
    //     null,
    //     ["mathMarksObtained", "mathTotalMarks"],
    //     validations
    //   );
    // }
    // if (this.state.eduState.userAcademicInfo.stream == 248) {
    //   validations = handleConditionalValidations(
    //     this.state.eduState.userAcademicInfo.stream,
    //     244,
    //     null,
    //     ["biologyMarksObtained", "biologyTotalMarks"],
    //     validations
    //   );
    // }
    /************* end Update conditional validations.........****/

    let isFormValid = true;
    for (let key in validations) {
      let checkKey = key === "cgpa" ? "marksObtained" : key;
      if (validations[key] !== undefined) {
        let { name, validationFunctions } = this.getValidationRulesObject(key);
        if (this.state.eduState["userAcademicInfo"]["markingType"] === "1") {
          validationFunctions =
            validationFunctions &&
            validationFunctions.filter(list => list.name !== "isCGPA");
        }
        let validationResult = ruleRunner(
          this.state.eduState["userAcademicInfo"][checkKey] ||
          this.state.eduState["userInstituteInfo"][checkKey] ||
          this.state.eduState["customData"][checkKey],
          key,
          name,
          ...validationFunctions
        );

        validations[key] = validationResult[key];
        if (validationResult[key] !== null) {
          isFormValid = false;
        }
      }
    }
    console.log(validations)
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  checkStreamBasedMarksValue() {
    const updateCustomData = { ...this.state.eduState };

    const { userAcademicInfo } = updateCustomData;

    if (userAcademicInfo && userAcademicInfo.stream) {
      const { stream } = userAcademicInfo;
      switch (stream) {
        case "244":
          updateCustomData["customData"]["mathMarksObtained"] = "";
          updateCustomData["customData"]["mathTotalMarks"] = "";
          break;
        case "248":
          updateCustomData["customData"]["biologyMarksObtained"] = "";
          updateCustomData["customData"]["biologyTotalMarks"] = "";
          break;
      }
    }
    return updateCustomData["customData"];
  }

  onEducationSubmitHandler(e) {
    e.preventDefault();
    let isSubmit = this.checkFormValidations();
    if (isSubmit) {
      // for (let key in this.state.configFieldsData) {
      //   if (this.state.configFieldsData[key].custom) {
      //     this.state.eduState.customData[key] = this.state.eduState[key];
      //   }
      // }
      let updatedEducationData;
      let updatedEducation = { ...this.state.eduState };
      if (
        updatedEducation &&
        updatedEducation.userAcademicInfo &&
        updatedEducation.userAcademicInfo.academicClass
      ) {
        if (
          updatedEducation.userAcademicInfo.academicClass.id ==
          this.state.presentClassID
        ) {
          updatedEducation.userAcademicInfo["presentClass"] = 1;
        } else {
          updatedEducation.userAcademicInfo["presentClass"] = 0;
        }
      }

      updatedEducation.customData = this.checkStreamBasedMarksValue();
      if (
        updatedEducation.userAcademicInfo &&
        updatedEducation.userAcademicInfo.markingType == 2
      )
        updatedEducation.userAcademicInfo["percentage"] = "";
      this.setState(
        {
          onSelectClassMode: false,
          isEditable: true,
          activeTab: null,
        },
        () =>
          this.props.saveEducationInfo({
            scholarshipId:
              typeof window !== "undefined"
                ? localStorage.getItem("applicationSchID")
                : "",
            presentClassId: this.state.presentClassID,
            education: updatedEducation
          })
      );
    }
  }
  checkCompletedApplicationStatus() {
    if (this.state.completingStep) {
      const { applicationEducationData } = this.props;
      const { classConfigList } = this.state;
      const completedStatus = [];
      const isOptionalStatus = [];
      if (
        applicationEducationData &&
        applicationEducationData.userEducationList &&
        applicationEducationData.userEducationList.educationDetails &&
        applicationEducationData.userEducationList.educationDetails.length > 0 &&
        classConfigList &&
        classConfigList.length
      ) {
        classConfigList.forEach(d => {
          if (d.disabled) {
            completedStatus.push(d);
          } else if (!d.disabled && d.isOptional) {
            isOptionalStatus.push(d);
            completedStatus.push(d);
          }
        });

        if (
          completedStatus &&
          isOptionalStatus &&
          isOptionalStatus.length > 0 &&
          completedStatus.length === classConfigList.length
        ) {
          return this.setState({
            status: false,
            msg:
              "There are some optional classes for which you can provide additional information. Please fill if it is applicable to you.",
            showLoader: true,
            completingStep: false,
            eduStatus: "nocompleted"
          });
        } else if (
          completedStatus &&
          completedStatus.length === classConfigList.length
        ) {
          return this.setState({ completingStep: false }, () =>
            this.goToNextStep()
          );
        }
      }
    }
  }
  close() {
    this.setState(
      {
        showLoader: false,
        msg: "",
        status: false
      },
      () => {
        if (this.state.eduStatus == "completed") {
          this.goToNextStep();
        }
      }
    );
  }
  ifClassDetailsAreCompleted() {
    const classesToCheck = []; //10th and graduation
    const usedClasses = (this.state.applicationEducationData && this.state.applicationEducationData.userEducationList
      ? this.state.applicationEducationData.userEducationList.educationDetails &&
        Array.isArray(this.state.applicationEducationData.userEducationList.educationDetails)
        ? this.state.applicationEducationData.userEducationList.educationDetails.filter(
          item =>
            classesToCheck.indexOf(
              item.userAcademicInfo
                ? item.userAcademicInfo.academicClass
                  ? item.userAcademicInfo.academicClass.id
                  : -1
                : -1
            ) > -1
        )
        : []
      : []
    ).filter(
      item =>
        !item.userAcademicInfo.marksObtained && !item.userAcademicInfo.grade
    );
    return usedClasses.length;
  }
  goToNextStep() {
    if (this.ifAllClassesFilled()) {
      if (this.ifClassDetailsAreCompleted()) {
        //Classes details are incomplete....
        this.setState({
          status: false,
          msg: this.props.genericEduTitle ? this.props.genericEduTitle : "",
          showLoader: true
        });
      } else {
        this.setState(
          {
            stepUpdated: true,
            status: false,
            msg: "",
            showLoader: false,
            completingStep: false
          },
          () => {
            this.props.updateStep({
              scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
              step: "EDUCATION_INFO",
              userId: gblFunc.getStoreUserDetails()["userId"],
              presentClassId: this.state.presentClassID
            });
          }
        );
      }
    }
  }

  markup(val) {
    return { __html: val };
  }

  brandVideo() {
    this.setState({
      isOpenVideo: true
    })
  }

  brandVideoClose() {
    this.setState({
      isOpenVideo: false
    })
  }


  hideConfirmationPopup() {
    this.setState({
      showConfirmationPopup: false,
      presentClassID: "",
      classID: "",
      classConfigList: [],
      isEditable: false
    });
  }

  onChangeHanlder(e, s) {
    //    const dayArr = [...this.state.optionalClasses];
    if (e.target.checked) {
      this.state.optionalClasses.push(s)
    } else {
      const index = this.state.optionalClasses.findIndex((ch) => ch.classId === s.classId);
      this.state.optionalClasses.splice(index, 1);
    }
    var filtered_Classes = [];
    if (this.state.optionalClasses.length > 0) {
      filtered_Classes = this.state.optionalClassesList.filter((x) => {
        if (x.isOptional == false) {
          return x
        }
        else if (this.state.optionalClasses && this.state.optionalClasses.length > 0 && this.state.optionalClasses.some(vendor => vendor.classId == x.classId)) {
          return x
        }

      });
    }
    this.setState({
      classConfigList: filtered_Classes,
      optionalClasses: this.state.optionalClasses
    });
  }

  render() {
    // this.checkCompletedApplicationStatus();
    const {
      applicationEducationData,
      classConfigList,
      optionalClassesList,
      classID,
      isEditable
    } = this.state;
    const {
      allRules,
      applicationStepInstruction,
      educationConfigInfo,
      scholarshipSteps,
      match
    } = this.props;
    const year = getYearOrMonth("year", [1950, 2024]);
    const month = getYearOrMonth("month", []);

    let academicClassOptions = null,
      eduInstructionTemplate = "";

    if (
      educationConfigInfo &&
      educationConfigInfo.academic_class &&
      educationConfigInfo.academic_class.dataOptions &&
      educationConfigInfo.academic_class.dataOptions.length > 0
    ) {
      academicClassOptions = educationConfigInfo.academic_class.dataOptions;
    }

    if (
      applicationEducationData &&
      applicationEducationData.userEducationList &&
      applicationEducationData.userEducationList.educationDetails &&
      applicationEducationData.userEducationList.educationDetails.length > 0 &&
      classConfigList &&
      classConfigList.length
    ) {
      let classesId = applicationEducationData.userEducationList.educationDetails.map(
        list => list.userAcademicInfo.academicClass.id
      );

      classesId
        ? classConfigList.map(acdClss => {
          if (
            classesId &&
            classesId.length > 0 &&
            classesId.indexOf(parseInt(acdClss.classId)) > -1
          ) {
            acdClss.disabled = true;
            return acdClss;
          } else {
            acdClss.disabled = false;
            return acdClss;
          }
        })
        : classConfigList.map(acdClss => {
          acdClss.disabled = false;
          return acdClss;
        });
    } else {
      classConfigList.map(acdClss => {
        acdClss.disabled = false;
        return acdClss;
      });
    }
    if (scholarshipSteps && scholarshipSteps.length) {
      eduInstructionTemplate = extractStepTemplate(
        scholarshipSteps,
        "EDUCATION_INFO"
      );
    }
    let checkDisabledClass = [];
    let filteredClasses = optionalClassesList && optionalClassesList.length > 0 && optionalClassesList.filter(cls => cls.isOptional == true);
    academicClassOptions && academicClassOptions.length > 0 && academicClassOptions.map(cls => {
      if (this.state.verifiedPresentClass && !(this.state.verifiedPresentClass == cls.id || gblFunc.checkPresentClass(this.state.verifiedPresentClass, cls.id))) {
        checkDisabledClass.push(cls.id);
      }
    }
    )
    return (
      <section className="sectionwhite">
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <ConfirmMessagePopup
          message={this.state.msg}
          showPopup={this.state.showConfirmationPopup}
          onConfirmationSuccess={this.onDeleteEducationListHandler}
          onConfirmationFailure={this.hideConfirmationPopup}
        />
        {academicClassOptions && academicClassOptions.length > 0 ? (
          <article>
            <article className="row">
              <article className="col-md-12 noPaddingSelectPresentClass">
                {this.state.isOpenVideo ?
                  <EducationVideoPopup brandVideoClose={this.brandVideoClose} /> : null}
                <article className="watch-video" onClick={this.brandVideo}> Watch Video</article>
                <label className="selectPresentClass">
                  Present/Aspiring Class:{" "}
                  <select
                    className="icon"
                    value={this.state.presentClassID}
                    onChange={event => this.onSelectPresentClassHandler(event)}
                  >
                    <option value="">--Select Present/Aspiring Class--</option>
                    {academicClassOptions.map(cls => (
                      <option key={cls.id} value={cls.id} disabled={this.state.verifiedPresentClass ?
                        !(this.state.verifiedPresentClass == cls.id ||
                          // (moment(moment().format('YYYY-MM-DD')).diff(moment(this.props.presentClassInfo[0].userAcademicInfo.lastUpdatedAt, 'YYYY-MM-DD'), 'months') >= 4
                          //  &&
                          gblFunc.checkPresentClass(this.state.verifiedPresentClass, cls.id)) : false
                      }
                      >
                        {cls.rulevalue}
                      </option>
                    ))}
                  </select>
                </label>
                {checkDisabledClass && checkDisabledClass.length == academicClassOptions.length && <p style={{ background: '#ffffff', padding: '1%', color: 'red', fontSize: '13px', textAlign: 'left' }}>Your present class does not match with the scholarship eligibility criteria. You cannot proceed with this application.</p>
                }
              </article>
            </article>
          </article>
        ) : null}

        {filteredClasses && filteredClasses.length > 0 && (
          <article className="form-group OptionalClasses">
            <label className="labelStyle">Please select classes you have already completed from the list below:</label>

            {filteredClasses.map(list => {
              return (
                <article className="radionewgp">
                  <label className="labelStyleLabel">{list.className}</label>
                          &nbsp;
                  <input
                    type="checkbox"
                    id="optionalClasses"
                    value={list.classId}
                    checked={
                      this.state.optionalClasses && this.state.optionalClasses.length > 0 && this.state.optionalClasses.find(o => o.classId === list.classId)
                    }
                    onChange={event =>
                      this.onChangeHanlder(
                        event,
                        list
                      )
                    }
                    disabled={applicationEducationData && applicationEducationData.userEducationList && applicationEducationData.userEducationList.educationDetails && applicationEducationData.userEducationList.educationDetails.length > 0 &&
                      applicationEducationData.userEducationList.educationDetails.some(item => item.userAcademicInfo.academicClass.id === list.classId)
                    }
                  />
                </article>
              );
            })}
            {/* <ValidationError
                      fieldValidation={
                        this.state.validationsObj[
                        "preferredLanguageForInterview"
                        ]
                      }
                    /> */}
          </article>
          // </article>
        )}

        {filteredClasses && filteredClasses.length > 0 ?

          this.state.optionalClasses && this.state.optionalClasses.length > 0 &&

          <article className="form">
            <article className="row">
              {
                eduInstructionTemplate.length &&
                  eduInstructionTemplate[0] &&
                  eduInstructionTemplate[0].message
                  ?
                  <article
                    dangerouslySetInnerHTML={this.markup(eduInstructionTemplate[0].message)}
                    className="col-md-12 subheadingerror"
                  />
                  : ""
              }
              {/* <article
      dangerouslySetInnerHTML={this.markup(
        eduInstructionTemplate.length &&
          eduInstructionTemplate[0] &&
          eduInstructionTemplate[0].message
          ? eduInstructionTemplate[0].message
          : this.props.genericEduTitle
      )}
      className="col-md-12 subheadingerror"
    /> */}

              {this.state.presentClassID && classConfigList && classConfigList.length > 0 ? (

                <article className="col-md-12">
                  {classConfigList.map(cls => (
                    <article className="newsubheading">
                      <article className="heading topborder" onClick={() => this.state.classID == cls.classId && this.state.isEditable ? this.setState({ activeTab: null, classID: null, isEditable: false }) : this.setState({ activeTab: `classConfig-${cls.classId}` }, () => cls.disabled ? this.handleExistingClass(cls.classId) : this.onSelectClassHandler(cls.classId))} >
                        <p key={cls.classId} >
                          <i
                            className={(this.state.classID && this.state.classID == cls.classId && this.state.isEditable) || this.state.activeTab === `classConfig-${cls.classId}` ? "fa fa-minus" : "fa fa-plus"}
                          >
                          </i>
                          {cls.className}
                        </p>

                        <span className="pull-right" style={{
                          position: "relative",
                          top: "-30px"
                        }}>
                          {
                            this.handleClassStatus(cls.classId) ?
                              (<span>Completed&nbsp;&nbsp;<i className="fa fa-check-circle-o complete">&nbsp;</i></span>) :
                              (<span>Pending&nbsp;&nbsp;<i className="fa fa-pencil-square-o pending">&nbsp;</i></span>)
                          }
                        </span>
                      </article>
                      {this.state.classID && this.state.classID == cls.classId && this.state.isEditable && (
                        <AcadmicClasses
                          allRules={allRules}
                          classID={this.state.classID}
                          presentClassID={this.state.presentClassID}
                          boardList={this.props.boardList}
                          district={this.props.district}
                          subject={this.props.subject}
                          isEditable={this.state.isEditable}
                          degree={this.degreeFilterDataOptions()}
                          year={year}
                          month={month}
                          marksOrCgpa={this.state.marksOrCgpa}
                          {...this.state.eduState}
                          onDistrictHandler={this.getDistrictHandler.bind(this)}
                          educationFormHandler={this.educationFormHandler}
                          marksObtainedHandler={this.marksObtainedHandler}
                          onEducationSubmitHandler={this.onEducationSubmitHandler}
                          {...applicationEducationData}
                          educationDataApi={this.state.classConfigFields}
                          applicationStep={applicationStepInstruction}
                          validations={this.state.validations}
                          closeTab={this.closeEducationTab}
                        />
                      )}
                    </article>
                  ))}
                  {this.checker &&
                    classConfigList &&
                    classConfigList.length === 1 &&
                    this.onSelectClassHandler(
                      classConfigList[0].classId,
                      true
                    )}
                </article>
              ) : null}
            </article>
          </article>

          :

          <article className="form">
            <article className="row">
              {
                eduInstructionTemplate.length &&
                  eduInstructionTemplate[0] &&
                  eduInstructionTemplate[0].message
                  ?
                  <article
                    dangerouslySetInnerHTML={this.markup(eduInstructionTemplate[0].message)}
                    className="col-md-12 subheadingerror"
                  />
                  : ""
              }
              {/* <article
            dangerouslySetInnerHTML={this.markup(
              eduInstructionTemplate.length &&
                eduInstructionTemplate[0] &&
                eduInstructionTemplate[0].message
                ? eduInstructionTemplate[0].message
                : this.props.genericEduTitle
            )}
            className="col-md-12 subheadingerror"
          /> */}

              {this.state.presentClassID && classConfigList && classConfigList.length > 0 ? (

                <article className="col-md-12">
                  {classConfigList.map(cls => (
                    <article className="newsubheading">
                      <article className="heading topborder" onClick={() => this.state.classID == cls.classId && this.state.isEditable ? this.setState({ activeTab: null, classID: null, isEditable: false }) : this.setState({ activeTab: `classConfig-${cls.classId}` }, () => cls.disabled ? this.handleExistingClass(cls.classId) : this.onSelectClassHandler(cls.classId))} >
                        <p key={cls.classId} >
                          <i
                            className={(this.state.classID && this.state.classID == cls.classId && this.state.isEditable) || this.state.activeTab === `classConfig-${cls.classId}` ? "fa fa-minus" : "fa fa-plus"}
                          >
                          </i>
                          {cls.className}
                        </p>

                        <span className="pull-right" style={{
                          position: "relative",
                          top: "-30px"
                        }}>
                          {
                            this.handleClassStatus(cls.classId) ?
                              (<span>Completed&nbsp;&nbsp;<i className="fa fa-check-circle-o complete">&nbsp;</i></span>) :
                              (<span>Pending&nbsp;&nbsp;<i className="fa fa-pencil-square-o pending">&nbsp;</i></span>)
                          }
                        </span>
                      </article>
                      {this.state.classID && this.state.classID == cls.classId && this.state.isEditable && (
                        <AcadmicClasses
                          allRules={allRules}
                          classID={this.state.classID}
                          presentClassID={this.state.presentClassID}
                          boardList={this.props.boardList}
                          district={this.props.district}
                          subject={this.props.subject}
                          isEditable={this.state.isEditable}
                          degree={this.degreeFilterDataOptions()}
                          year={year}
                          month={month}
                          marksOrCgpa={this.state.marksOrCgpa}
                          {...this.state.eduState}
                          onDistrictHandler={this.getDistrictHandler.bind(this)}
                          educationFormHandler={this.educationFormHandler}
                          marksObtainedHandler={this.marksObtainedHandler}
                          onEducationSubmitHandler={this.onEducationSubmitHandler}
                          {...applicationEducationData}
                          educationDataApi={this.state.classConfigFields}
                          applicationStep={applicationStepInstruction}
                          validations={this.state.validations}
                          closeTab={this.closeEducationTab}
                        />
                      )}
                    </article>
                  ))}
                  {this.checker &&
                    classConfigList &&
                    classConfigList.length === 1 &&
                    this.onSelectClassHandler(
                      classConfigList[0].classId,
                      true
                    )}
                </article>
              ) : null}
            </article>
          </article>
        }

        <article className="border">&nbsp;</article>

        {/* {(this.state.presentClassID && this.ifAllClassesFilled() &&
        !this.state.isEditable) ? (
          <article className="row">
            <article className="col-md-12">
              <input
                type="button"
                value="Save & Continue!!"
                className="btn  pull-right"
                onClick={this.goToNextStep}
              />
            </article>
          </article>
        ) : null} */}
      </section>
    );
  }
}

const EducationLists = ({
  applicationEducationData,
  editEduction,
  match,
  BSID
}) => {
  let eduList = null;
  if (
    applicationEducationData &&
    Object.keys(applicationEducationData).length > 0 &&
    applicationEducationData.userEducationList &&
    applicationEducationData.userEducationList.educationDetails &&
    applicationEducationData.userEducationList.educationDetails.length > 0
  ) {
    const eduLists = applicationEducationData.userEducationList.educationDetails;
    eduList = eduLists.map((list, index) =>
      match.params && childEduBSID[match.params.bsid.toUpperCase()] ? (
        <tr key={index}>
          <td>{list.userAcademicInfo.academicClass.value}</td>
          <td>
            {list.userInstituteInfo.instituteName ? (
              list.userInstituteInfo.instituteName
            ) : (
              <span>&mdash;</span>
            )}
          </td>
          <td>
            {list.userAcademicInfo.tuitionFee ? (
              list.userAcademicInfo.tuitionFee
            ) : (
              <span>&mdash;</span>
            )}
          </td>

          {BSID != "SKKS1" && (
            <td>
              {list.userInstituteInfo.stateName ? (
                list.userInstituteInfo.stateName
              ) : (
                <span>&mdash;</span>
              )}
            </td>
          )}

          <td>
            {" "}
            <i
              onClick={() => editEduction(list, true)}
              className="fa fa-edit iconedit"
            >
              {" "}
              &nbsp;
            </i>
          </td>
        </tr>
      ) : (
        <tr key={index}>
          <td>{list.userAcademicInfo.academicClass.value}</td>
          <td>
            {list.userInstituteInfo.instituteName ? (
              list.userInstituteInfo.instituteName
            ) : (
              <span>&mdash;</span>
            )}
          </td>
          <td>
            {list.userAcademicInfo.percentage ? (
              list.userAcademicInfo.percentage.toFixed(2) + " %"
            ) : (
              <span>&mdash;</span>
            )}
          </td>

          <td>
            {list.userAcademicInfo.grade ? (
              list.userAcademicInfo.grade
            ) : (
              <span>&mdash;</span>
            )}
          </td>

          <td>
            {" "}
            <i
              onClick={() => editEduction(list, true)}
              className="fa fa-edit iconedit"
            >
              {" "}
                &nbsp;
            </i>
          </td>
        </tr>
      )
    );
  }
  return eduList;
};

const mapStateToProps = ({
  applicationEducation,
}) => ({
  presentClassInfo: applicationEducation.presentClassInfo,
});

const mapDispatchToProps = dispatch => ({
  fetchPresentClass: inputData => dispatch(fetchPresentClassAction(inputData))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EducationInfo);
