import React, { Component } from "react";
import { connect } from "react-redux";
import index from "axios";
import {
  required,
  maxFileSize,
  hasValidExtension
} from "../../../../../validation/rules";
import { ruleRunner } from "../../../../../validation/ruleRunner";
import AlertMessage from "../../../../common/components/alertMsg";
import Loader from "../../../..//common/components/loader";
import DocumentVideoPopup from "./DocumentVideoPopup";
import { Route, Link, Redirect } from "react-router-dom";
import {
  allowedDocumentExtensions,
  imageFileExtension
} from "../../../../../constants/constants";
import Cropper from "react-cropper";
if (typeof window !== "undefined") {
  require("cropperjs/dist/cropper.css");
}
import ConfirmMessagePopup from "../../../../common/components/confirmMessagePopup";
import gblFunc from "../../../../../globals/globalFunctions";
import { extractStepTemplate } from "../../../formconfig";
import {
  fetchPresentClass as fetchPresentClassAction

} from '../../../actions/educationInfoAction';
class DocumentNeeded extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenVideo: false,
      bankSection: false,
      showPopup: false,
      presentClass: null,
      redirectToEducation: false,
      documentList: [],
      showLoader: false,
      docs: {},
      _applicableSteps: [],
      msg: "",
      status: "",
      isDropDownCategory: {},
      isDropDownOption: {},
      isFormValid: false,
      presentClassId: null,
      year: [
        { id: "1", value: "1st Year" },
        { id: "2", value: "2nd Year" },
        { id: "3", value: "3rd Year" },
        { id: "4", value: "4th Year" },
        { id: "5", value: "5th Year" },
        { id: "6", value: "6th Year" }
      ],
      semester: [
        { id: "7", value: "1st Semester" },
        { id: "8", value: "2nd Semester" },
        { id: "9", value: "3rd Semester" },
        { id: "10", value: "4th Semester" },
        { id: "11", value: "5th Semester" },
        { id: "12", value: "6th Semester" },
        { id: "13", value: "7th Semester" },
        { id: "14", value: "8th Semester" },
        { id: "15", value: "9th Semester" },
        { id: "16", value: "10th Semester" }
      ],
      validations: {},
      docFile: null,
      isShowCropPup: false,
      cropSrc: "",
      cropResult: null,
      showConfirmationPopup: false,
      deleteSuccessCallBack: null,
      isNext: false,
      isCompleted: false
    };
    this.cropImage = this.cropImage.bind(this);
    this.dataURLtoFile = this.dataURLtoFile.bind(this);
    this.checkValidation = this.checkValidation.bind(this);
    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.hideConfirmationPopup = this.hideConfirmationPopup.bind(this);
    this.brandVideo = this.brandVideo.bind(this);
    this.brandVideoClose = this.brandVideoClose.bind(this);
    this.setColor = this.setColor.bind(this);
  }
  setColor(status) {
    return {
      color: status == 1 ? 'grey' : 'none',
      border: `1px solid ${status == 1 ? 'grey' : '#fdb701'}`,
      'pointer-events': status == 1 ? 'none' : 'auto'
    }
  }
  uploadDocumentWithOutCrop(docObj, files, multi) {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
    }
    const nullIfFalsy = value => value || null;

    if ([6, 1].indexOf(docObj.documentType["id"]) > -1) {
      const stringifiedData = JSON.stringify({
        documentType: nullIfFalsy(docObj["documentType"]["id"]),
        documentDescription: nullIfFalsy(docObj["documentType"]["description"]),
        documentTypeCategory: multi && multi.documentTypeCategory ? multi.documentTypeCategory : nullIfFalsy(
          this.state.isDropDownCategory[docObj.documentType["id"]]
        ), // pass type Year or Semester
        documentTypeCategoryOption: multi && multi.documentTypeCategoryOption ? multi.documentTypeCategoryOption : nullIfFalsy(
          this.state.isDropDownOption[docObj.documentType["id"]]
        ) // pass year Value or Semester Value
      });
      this.props.uploadAllDocuement({
        docFile: files[0],
        userDocRequest: stringifiedData,
        userId: userId,
        isPosted: this.state.isCompleted,

      });
    } else {
      const stringifiedData = JSON.stringify({
        documentType: nullIfFalsy(docObj["documentType"]["id"]),
        documentTypeCategory: multi && multi.documentTypeCategory ? multi.documentTypeCategory : nullIfFalsy(null), // pass type Year or Semester
        documentTypeCategoryOption: multi && multi.documentTypeCategoryOption ? multi.documentTypeCategoryOption : nullIfFalsy(null) // pass year Value or Semester Value
      });
      console.log(stringifiedData, "stringifiedData else")
      this.props.uploadAllDocuement({
        docFile: files[0],
        userDocRequest: stringifiedData,
        userId: userId,
        isPosted: this.state.isCompleted
      });
    }
  }

  getDocData(event, docObj, multiDoc) {
    let validations = { ...this.state.validations };
    let validationResult = null;
    let isFormValid = true;
    const { files } = event.target;
    const uploadedFile = files[0];
    const sizeInMb = Math.ceil(uploadedFile.size / (1024 * 1024));
    const fileExtension = uploadedFile.name.slice(
      uploadedFile.name.lastIndexOf(".")
    );
    if (docObj.documentType["id"] == 1) {
      validationResult = ruleRunner(
        fileExtension,
        docObj.documentType["id"],
        "File",
        hasValidExtension(imageFileExtension)
      );
    } else {
      validationResult = ruleRunner(
        fileExtension,
        docObj.documentType["id"],
        "File",
        hasValidExtension(allowedDocumentExtensions)
      );
    }
    if (validationResult[docObj.documentType["id"]] === null) {
      validationResult = ruleRunner(
        sizeInMb,
        docObj.documentType["id"],
        "Uploaded file",
        maxFileSize(1)
      );
    }
    validations[docObj.documentType["id"]] =
      validationResult[docObj.documentType["id"]];
    if (validationResult[docObj.documentType["id"]] !== null) {
      // if invalid extention and size
      isFormValid = false;
      this.setState({
        isCompleted: false
      });
    }
    //show croper if document type would be 1
    if (isFormValid) {
      this.setState({
        isCompleted: true
      });
      // if form should be valid
      if (docObj.documentType["id"] == 1) {
        this.state.isShowCropPup = true;
      }

      let reader = new FileReader(); // read upload file
      reader.onload = e => {
        this.setState({ cropSrc: e.target.result });
      };
      reader.readAsDataURL(event.target.files[0]);
    }

    this.setState({
      validations,
      isShowCropPup: this.state.isShowCropPup,
      docFile: files
    });
    if (docObj.documentType["id"] != 1 && isFormValid) {
      // only those document having no 1 Id
      this.uploadDocumentWithOutCrop(docObj, files, multiDoc);
      var elements = document.getElementById(
        "document-catagory-dropdown-option"
      )
        ? document.getElementById("document-catagory-dropdown-option").options
        : null;
      if (elements && elements != null && elements.length > 0) {
        elements[0].selected = true;
      }
    }
  }

  deleteDocument(id, docObj) {
    let userId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
    }
    const clearSelected = () => {
      var elements = document.getElementById("document-catagory-dropdown")
        ? document.getElementById("document-catagory-dropdown").options
        : null;
      if (elements && elements != null && elements.length > 0) {
        elements[0].selected = true;
      }
    };
    const deleteSuccessCallBack = docs => {
      const { instructions } = this.props;
      clearSelected();
      this.props.deleteDocuement({
        docId: docs.id,
        userId: userId,
        isDeleted: true,
        isMandatory: docs.docObj.isMandatory,
        scholarshipId: instructions.scholarshipId
      });
    };
    if (!this.state.showConfirmationPopup) {
      this.setState({
        showConfirmationPopup: true,
        docs: { id, docObj }
      });
    } else {
      let dropdown = Object.assign({}, this.state.isDropDownCategory);
      let dropdownOptions = Object.assign({}, this.state.isDropDownOption);
      delete dropdown[this.state.docs.docObj.documentType[this.state.docs.id]];
      delete dropdownOptions[
        this.state.docs.docObj.documentType[this.state.docs.id]
      ];
      this.setState(
        {
          showConfirmationPopup: false,
          isDropDownCategory: dropdown,
          isDropDownOption: dropdownOptions
        },
        () => deleteSuccessCallBack(this.state.docs)
      );
    }
  }

  showConfirmationPopup() {
    this.setState({
      showConfirmationPopup: true
    });
  }

  hideConfirmationPopup() {
    this.setState({
      showConfirmationPopup: false,
      deleteSuccessCallBack: null
    });
  }

  getAllDocument() {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }

    this.props.getAllDocuement({
      userId: userId,
      scholarshipId: scholarshipId,
      presentClass: this.props.presentClassInfo && this.props.presentClassInfo.presentClass ? this.props.presentClassInfo.presentClass : this.state.presentClass
    });
  }

  componentDidMount() {
    this.props.fetchUserRule({
      userid: gblFunc.getStoreUserDetails()["userId"]
    });
    let scholarshipId = ""; // PASS SCHOLARSHIP ID FOR COMPLETED STEPS
    if (window != undefined) {
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    if (!!scholarshipId && !!this.props.fetchPresentClass) {
      this.props.fetchPresentClass(scholarshipId);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { scholarshipSteps, allDocument } = nextProps;
    const { type } = allDocument;
    let scholarshipId = ""; // PASS SCHOLARSHIP ID FOR COMPLETED STEPS
    if (window != undefined) {
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }

    switch (type) {
      case "FETCH_USER_RULES_SUCCESS":
        const { userRules } = nextProps.userRuleInfo;
        if (userRules && userRules.length) {
          const presentClass = userRules.filter(rule => rule.ruleTypeId == 1);
          if (presentClass && presentClass.length > 0) {
            this.setState(
              {
                presentClass: presentClass[0].ruleId
              },
              () =>
                this.props.applicationInstructionStep({
                  scholarshipId: gblFunc.getStoreApplicationScholarshipId()
                })
            );
          } else {
            this.props.applicationInstructionStep({
              scholarshipId: gblFunc.getStoreApplicationScholarshipId()
            });
          }
        } else {
          this.props.applicationInstructionStep({
            scholarshipId: gblFunc.getStoreApplicationScholarshipId()
          });
        }
        break;
      // case"PRESENT_CLASS_INFO_SUCCESS":
      // if( nextProps.presentClassInfo &&
      //   nextProps.presentClassInfo.presentClass){
      //     this.setState({
      //         presentClassId:nextProps.presentClassInfo.presentClass
      //     })

      //   }
      //   break;

      case "GET_ALL_DOCUMENT_SUCCESS":
        nextProps.allDocument["documentDataList"].applcationDocuments.map(res => {
          const rsType = gblFunc.getStoreHulAuthType("hulType");
          let resDocumentType = Object.assign({}, res.documentType);
          let validations = Object.assign({}, this.state.validations);
          // let isDropDownOption = Object.assign({}, this.state.isDropDownOption);

          if (
            rsType &&
            rsType !== "3P" &&
            res.isMandatory &&
            res.documentType["id"] === 30
          ) {
            this.state.validations[res.documentType["id"]] = null;
          } else if (
            rsType &&
            rsType === "3P" &&
            res.isMandatory &&
            res.documentType["id"] !== 30
          ) {
            this.state.validations[res.documentType["id"]] = null;
          } else {
            if (res.isMandatory) {
              this.state.validations[resDocumentType["id"]] = null;
            }
          }

          // if ([6, 7].indexOf(res.documentType["id"]) > -1) {
          //   return (isDropDownOption[res.documentType["id"]] = "");
          // } else {
          //   return {};
          // }
          this.setState({
            documentList: nextProps.allDocument["documentDataList"].applcationDocuments,
            validations: this.state.validations,
            isDropDownOption: {}
          });

        });

        //VALIDATE THE DOCUMENT HAS BEEN COMPLETED
        let tempArray = [];
        if (
          nextProps.allDocument["documentDataList"] != null &&
          nextProps.allDocument["documentDataList"].applcationDocuments != null &&

          nextProps.allDocument["documentDataList"].applcationDocuments.length > 0
        ) {
          nextProps.allDocument["documentDataList"].applcationDocuments.map((res, i) => {
            if (res.userDocument != null) {
              tempArray.push(i);
            }
          });
        }
        /*
        if (
          tempArray.length + 1 ==
          nextProps.allDocument["documentDataList"].length
        ) {
          this.setState({
            isCompleted: true
          });
        } else {
          this.setState({
            isCompleted: false
          });
        } */

        break;
      case "GET_ALL_DOCUMENT_UPLOAD_SUCCESS":
        this.setState(
          {
            status: true,
            msg: "Document has been Uploaded",
            showLoader: true
          },
          () => this.getAllDocument()
        );

        break;
      case "GET_ALL_DOCUMENT_UPLOAD_DELETE_SUCCESS":
        this.setState(
          {
            status: true,
            msg: "Document has been Deleted",
            showLoader: true,
            showConfirmationPopup: false,
            deleteSuccessCallBack: null
          },
          () => {
            this.getAllDocument();
          }
        );

        if (this.state.isCompleted) {
          //UPDATE THE SETP CALL
          this.props.applicationInstructionStep({
            scholarshipId: scholarshipId
          });
        }
        break;

      case "GET_ALL_DOCUMENT_UPLOAD_DELETE_FAILURE":
        this.setState({
          status: false,
          msg: "No Record Deleted",
          showLoader: true,
          deleteSuccessCallBack: null,
          showConfirmationPopup: false
        });
        break;
      case "GET_ALL_DOCUMENT_UPLOAD_COMPLETE_SUCCESS":
        this.setState(
          {
            status: true,
            msg: "All Document has been Uploaded",
            showLoader: true,
            showConfirmationPopup: false,
            deleteSuccessCallBack: null
          },
          () => {
            this.getAllDocument();
            this.props.applicationInstructionStep({
              scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
              userId: gblFunc.getStoreUserDetails()["userId"]
            })
          }
        );
        break;
      case "FETCH_APPLICATION_DOCS_STEP_SUCCESS":
        const { applicationStepInstruction, scholarshipSteps } = nextProps;
        var result = [];
        if (!!scholarshipSteps && scholarshipSteps.length >= 0) {
          result = scholarshipSteps.filter(obj => {
            return obj.step == "EDUCATION_INFO"
          })

        }
        if (
          result && result.length > 0 &&
          applicationStepInstruction &&
          applicationStepInstruction.indexOf("EDUCATION_INFO") == -1 &&
          !["IGA1"].includes(this.props.match.params.bsid.toUpperCase()) &&
          !["CHO1"].includes(this.props.match.params.bsid.toUpperCase())
        ) {
          this.setState({
            msg:
              "Please complete your education details, before proceeding to the documents section.",
            showLoader: true,
            status: false,
            redirectToEducation: true
          });
        } else {
          if (this.state.bankSection) {
            let steps = [];
            scholarshipSteps && scholarshipSteps.map(item => {
              steps.push(item.step);
            })
            const dat = steps.indexOf("DOCUMENTS");
            if (scholarshipSteps && scholarshipSteps.length === dat + 1) {
              this.setState({
                bankSection: false
              }, () => {
                this.getAllDocument();
              })
            } else {
              this.setState({
                isNext: true,
                bankSection: false
              })
            }
          } else {
            this.getAllDocument();
          }
        }

        break;
    }
  }

  getCategory(event, docId) {
    this.state.isDropDownCategory[docId];
    let dropdown = Object.assign({}, this.state.isDropDownCategory);
    if (event.target.value == 1) {
      // for year
      dropdown[docId] = event.target.value;
    } else if (event.target.value == 3) {
      dropdown[docId] = event.target.value;
    } else {
      dropdown[docId] = "";
    }
    this.setState({
      isDropDownCategory: dropdown
    });
  }

  getCategoryOption(event, docId) {
    let dropdown = this.state.isDropDownOption;
    dropdown[docId] = event.target.value;

    this.setState({
      isDropDownOption: dropdown
    });
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    if (this.state.validations.hasOwnProperty(fieldID)) {
      validationObject.name = "Field";
      validationObject.validationFunctions = [required];
    }
    return validationObject;
  }

  checkValidation() {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    let isValidate = this.checkFormValidations();
    if (isValidate) {
      this.props.completeDocument({
        userId: userId,
        scholarshipId: scholarshipId,
        steps: "DOCUMENTS",
        presentClassId: this.props.presentClassInfo && this.props.presentClassInfo.presentClass ? this.props.presentClassInfo.presentClass : null
      });
      this.setState({
        bankSection: true
      })
    }
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      const rsType = gblFunc.getStoreHulAuthType("hulType");
      let documentList1 = [];
      if (rsType && rsType === "3P") {
        documentList1 = this.state.documentList.filter(
          f => f.documentType.id !== 30
        );
      }
      else {
        documentList1 = this.state.documentList;
      }
      let documentList = documentList1
        .filter(res => {
          return res.documentType["id"] == key;
        })
        .map(item => {
          return item.userDocument;
        });
      let validationResult = ruleRunner(
        documentList[0],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }
  close() {
    if (this.state.redirectToEducation) {
      this.props.history.push(
        `/application/${this.props.match.params.bsid.toUpperCase()}/form/educationInfo`
      );
    } else {
      this.setState({
        showLoader: false,
        msg: "",
        status: false
      });
    }
  }

  dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(","),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }

  cropImage(docObj) {
    if (typeof this.cropper.getCroppedCanvas() === "undefined") {
      return;
    }

    const selectedFile = this.state.docFile[0];

    let customizeFileName =
      selectedFile.name.substr(0, selectedFile.name.lastIndexOf(".")) +
      selectedFile.name
        .substr(selectedFile.name.lastIndexOf("."))
        .toLowerCase();

    var file = this.dataURLtoFile(
      this.cropper
        .getCroppedCanvas({ width: 200, height: 200 })
        .toDataURL("image/jpeg", 0.8),
      customizeFileName
    );

    let reader = new FileReader(); // read upload file
    reader.readAsDataURL(file);

    this.setState(
      {
        cropResult: file,
        isShowCropPup: false
      },
      () => this.uploadDocumentWithOutCrop(docObj, Array(file))
    );
  }

  closePopUp() {
    this.setState({
      isShowCropPup: false
    });
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  markup(val) {
    return { __html: val };
  }

  brandVideo() {
    this.setState({
      isOpenVideo: true
    })
  }

  brandVideoClose() {
    this.setState({
      isOpenVideo: false
    })
  }


  render() {
    const { scholarshipSteps } = this.props;
    let documentsTemplate = "";
    if (scholarshipSteps && scholarshipSteps.length) {
      documentsTemplate = extractStepTemplate(scholarshipSteps, "DOCUMENTS");
    }
    const rsType = gblFunc.getStoreHulAuthType("hulType");
    let documentList = [];
    let goTo = "";
    if (rsType && rsType === "NON_3P") {
      documentList = this.state.documentList;
      goTo = `${this.props.redirectionSteps[
        this.props.redirectionSteps.indexOf("documents") + 1
      ]
        }`;
    } else if (rsType && rsType === "3P") {
      goTo = `${this.props.redirectionSteps[
        this.props.redirectionSteps.indexOf("documents") + 2
      ]
        }`;
      documentList = this.state.documentList.filter(
        f => f.documentType.id !== 30
      );
    } else {
      goTo = `${this.props.redirectionSteps[
        this.props.redirectionSteps.indexOf("documents") + 1
      ]
        }`;
      documentList = this.state.documentList;
    }

    return (
      <section className="sectionwhite">
        {this.state.isNext &&
          this.props.redirectionSteps &&
          this.props.redirectionSteps.length > 0 && goTo != "summary" ? (
            <Redirect
              to={`/application/${this.props.match.params.bsid.toUpperCase()}/form/${goTo}`}
            />
          ) : (
            ""
          )}
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={() => {
            this.close.bind(this)();
            if (this.state.msg == "Document has been Deleted") {
              return document.location.reload(true);
            }
          }}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <ConfirmMessagePopup
          message={"Are you sure want to delete ?"}
          showPopup={this.state.showConfirmationPopup}
          onConfirmationSuccess={() => {
            this.deleteDocument();
          }}
          onConfirmationFailure={this.hideConfirmationPopup}
        />
        <article className="form">
          <article className="row">
            {this.state.isOpenVideo ? <DocumentVideoPopup brandVideoClose={this.brandVideoClose} /> : null}
            <article className="watch-video" onClick={this.brandVideo}> Watch Video</article>
            <article
              dangerouslySetInnerHTML={this.markup(
                documentsTemplate &&
                  documentsTemplate.length &&
                  documentsTemplate[0] &&
                  documentsTemplate[0].message
                  ? documentsTemplate[0].message
                  : " Make sure that the document is in .pdf or .jpeg format with file size not exceeding 1 MB"
              )}
              className="col-md-12 subheadingerror"
            />
            <article className="col-md-12 paddingBoth">
              {/* <p className="msgString">
                Make sure that the document is in .pdf or .jpeg format with file
                size not exceeding 1 MB.
              </p> */}
              {documentList != null && documentList.length > 0
                ? documentList.map((res, i) => {
                  if ([6, 7].indexOf(res.documentType["id"]) > -1) {
                    // if the category and option are availables
                    return (
                      <article key={i} className="graystrip tbl docError">
                        <article className="docRow">
                          <article className="ctrlRow">
                            <span>{res.documentType && res.documentType["description"]}</span>
                            <article className="spacer" />
                            <article>
                              <article className="yearSemCtrl">
                                {this.state.validations != null &&
                                  this.state.validations[
                                  res.documentType["id"]
                                  ] ? (
                                    <span className="error">
                                      {
                                        this.state.validations[
                                        res.documentType && res.documentType["id"]
                                        ]
                                      }
                                    </span>
                                  ) : null}

                                <span className="doc-need">
                                  <select
                                    id="document-catagory-dropdown"
                                    onChange={event =>
                                      this.getCategory(
                                        event,
                                        res.documentType && res.documentType["id"]
                                      )
                                    }
                                  >
                                    <option value="">Select Category</option>
                                    <option
                                      value="1"
                                      disabled={
                                        res.userDocument != null &&
                                          res.userDocument[0][
                                          "documentTypeCategory"
                                          ] &&
                                          res.userDocument[0][
                                          "documentTypeCategory"
                                          ]["id"] == 3
                                          ? true
                                          : false
                                      }
                                    >
                                      Year
                                      </option>
                                    <option
                                      value="3"
                                      disabled={
                                        res.userDocument != null &&
                                          res.userDocument[0][
                                          "documentTypeCategory"
                                          ] &&
                                          res.userDocument[0][
                                          "documentTypeCategory"
                                          ]["id"] == 1
                                          ? true
                                          : false
                                      }
                                    >
                                      Semester
                                      </option>
                                  </select>
                                </span>
                                {this.state.isDropDownCategory[
                                  res.documentType["id"]
                                ] == 1 ? (
                                    <span className="doc-need">
                                      <select
                                        id="document-catagory-dropdown-option"
                                        onChange={event =>
                                          this.getCategoryOption(
                                            event,
                                            res.documentType && res.documentType["id"]
                                          )
                                        }
                                      >
                                        <option>Select Year</option>
                                        {this.state.year && this.state.year.length > 0 && this.state.year.map((year, i) => {
                                          if (res.userDocument == null) {
                                            return (
                                              <option key={i} value={year.id}>
                                                {year.value}
                                              </option>
                                            );
                                          } else {
                                            let filterYr = res.userDocument.filter(
                                              filterYear => {
                                                return (
                                                  filterYear[
                                                  "documentTypeCategoryOption"
                                                  ] != null &&
                                                  filterYear[
                                                  "documentTypeCategoryOption"
                                                  ]["id"] == year.id
                                                );
                                              }
                                            );

                                            return (
                                              <option
                                                disabled={
                                                  filterYr.length > 0 &&
                                                    filterYr[0][
                                                    "documentTypeCategoryOption"
                                                    ] != null &&
                                                    filterYr[0][
                                                    "documentTypeCategoryOption"
                                                    ]["id"] == year.id
                                                    ? true
                                                    : false
                                                }
                                                key={i}
                                                value={year.id}
                                              >
                                                {year.value}
                                              </option>
                                            );
                                          }
                                        })}
                                      </select>
                                    </span>
                                  ) : (
                                    ""
                                  )}

                                {this.state.isDropDownCategory[
                                  res.documentType["id"]
                                ] == 3 ? (
                                    <span className="doc-need">
                                      <select
                                        onChange={event =>
                                          this.getCategoryOption(
                                            event,
                                            res.documentType["id"]
                                          )
                                        }
                                      >
                                        <option value="">
                                          Select Semester
                                        </option>
                                        {this.state.semester.map((sem, i) => {
                                          if (res.userDocument == null) {
                                            return (
                                              <option key={i} value={sem.id}>
                                                {sem.value}
                                              </option>
                                            );
                                          } else {
                                            let filterSem = res.userDocument.filter(
                                              filterSem => {
                                                return (
                                                  filterSem[
                                                  "documentTypeCategoryOption"
                                                  ] != null &&
                                                  filterSem[
                                                  "documentTypeCategoryOption"
                                                  ]["id"] == sem.id
                                                );
                                              }
                                            );
                                            return (
                                              <option
                                                disabled={
                                                  filterSem.length > 0 &&
                                                    filterSem[0][
                                                    "documentTypeCategoryOption"
                                                    ] != null &&
                                                    filterSem[0][
                                                    "documentTypeCategoryOption"
                                                    ]["id"] == sem.id
                                                    ? true
                                                    : false
                                                }
                                                key={i}
                                                value={sem.id}
                                              >
                                                {sem.value}
                                              </option>
                                            );
                                          }
                                        })}
                                      </select>
                                    </span>
                                  ) : (
                                    ""
                                  )}
                              </article>
                              <article className="document">
                                <label className="custom-file-upload" style={res.userDocument && res.userDocument.length > 0 && this.setColor(res.userDocument[0].verificationStatus)}>
                                  <input
                                    onClick={event => {
                                      event.target.value = null;
                                    }}
                                    key={i}
                                    type="file"
                                    // disabled={
                                    //   this.state.isDropDownCategory[
                                    //     res.documentType["id"]
                                    //   ] == ""
                                    //     ? true
                                    //     : false
                                    // }
                                    disabled={
                                      Object.keys(this.state.isDropDownCategory).length === 0 && this.state.isDropDownCategory.constructor === Object
                                      || Object.keys(this.state.isDropDownOption).length === 0 && this.state.isDropDownOption.constructor === Object || res.userDocument && res.userDocument.length > 0 && res.userDocument[0].verificationStatus === 1
                                    }
                                    className="btn top0"
                                    onChange={event =>
                                      this.getDocData(event, res)
                                    }
                                  />
                                  Browse
                                  </label>
                              </article>
                            </article>
                          </article>
                          <article className="tblRow">
                            {res.userDocument != null ? (
                              <article className="tblOverflow hScroll">
                                <table>
                                  <thead>
                                    <tr>
                                      <th>File name</th>
                                      <th>Category</th>
                                      <th>Year/Semester</th>
                                      <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {res.userDocument.map((doc, i) => {
                                      return (
                                        <tr key={i}>
                                          <td>
                                            <Link
                                              target="_blank"
                                              to={`${doc["location"]
                                                ? doc["location"]
                                                : ""
                                                }`}
                                              style={{ pointerEvents: 'none' }}
                                            >
                                              {doc["documentDescription"]
                                                ? doc["documentDescription"]
                                                : ""}
                                            </Link>
                                          </td>
                                          <td>
                                            {doc["documentTypeCategory"] !=
                                              null &&
                                              doc["documentTypeCategory"][
                                              "categoryName"
                                              ]}
                                          </td>
                                          <td>
                                            {doc[
                                              "documentTypeCategoryOption"
                                            ] != null &&
                                              doc[
                                              "documentTypeCategoryOption"
                                              ]["categoryName"]}
                                          </td>
                                          <td>
                                            {doc[
                                              "location"
                                            ] ? <a
                                              target="_blank"
                                              href={`${doc[
                                                "location"
                                              ]
                                                }`}
                                            >
                                                <i className="fa fa-download Checkicon" />
                                              </a> :
                                              <a
                                                target="_blank"
                                              >
                                                <i className="fa fa-download Checkicon" style={{ color: "grey" }} />
                                              </a>
                                            }
                                            <article className="document">
                                              <label className="custom-file-upload" style={doc.verificationStatus && this.setColor(doc.verificationStatus)}>
                                                <input
                                                  onClick={event => {
                                                    event.target.value = null;
                                                  }}
                                                  key={i}
                                                  type="file"
                                                  disabled={doc.verificationStatus === 1
                                                    ? true
                                                    : false
                                                  }
                                                  className="btn top0"
                                                  onChange={event => this.getDocData(event, res, { documentType: res.documentType.id, documentTypeCategory: doc.documentTypeCategory.id, documentTypeCategoryOption: String(doc.documentTypeCategoryOption.id) })
                                                  }
                                                />
                                  Browse
                                              </label>
                                            </article>
                                          </td>
                                        </tr>
                                      );
                                    })}
                                  </tbody>
                                </table>
                              </article>
                            ) : (
                                ""
                              )}
                          </article>
                        </article>
                      </article>
                    );
                  } else {
                    // if category and option are not available
                    return (
                      <article key={i} className="tableRow">
                        <article className="graystrip tbl">
                          <span>
                            {res.docNameLabel
                              ? res.docNameLabel
                              : res.documentType && res.documentType["description"]}
                          </span>
                          <article className="spacer" />
                          <article className="docError">
                            <span className="iconWrapper">
                              {res.userDocument && res.userDocument.length > 0 && res.userDocument[0] && res.userDocument[0]["location"] ?
                                <a
                                  target="_blank"
                                  href={`${res.userDocument[0]["location"]}`}
                                >
                                  <i className="fa fa-download Checkicon" />
                                </a>
                                :
                                <a
                                  target="_blank"
                                  style={{ 'pointer-events': 'none' }}
                                >
                                  <i className="fa fa-download Checkicon" style={{ color: "grey" }} />
                                </a>
                              }
                              {/* <span
                                    onClick={event => {
                                      if (res.userDocument[0].verificationStatus === 1) {
                                        return false;
                                      }
                                      this.deleteDocument(
                                        res.userDocument[0]["id"],
                                        res
                                      )
                                    }
                                    }
                                    key={i}
                                    className="delete"
                                  >
                                    <i
                                      style={this.setColor(res.userDocument[0].verificationStatus)}
                                      class="fa fa-trash"
                                      aria-hidden="true"
                                    />
                                  </span> */}
                            </span>
                            {/* )} */}
&nbsp; &nbsp;
<article className="document">
                              <label className="custom-file-upload" style={res.userDocument && res.userDocument.length > 0 && this.setColor(res.userDocument[0].verificationStatus)}>
                                <input
                                  onClick={event => {
                                    event.target.value = null;
                                  }}
                                  key={i}
                                  type="file"
                                  disabled={
                                    (this.state.isDropDownCategory[
                                      res.documentType["id"]
                                    ] == "" || res.userDocument && res.userDocument.length > 0 && res.userDocument[0].verificationStatus === 1)
                                      ? true
                                      : false
                                  }
                                  className="btn top0"
                                  onChange={event =>
                                    this.getDocData(event, res)
                                  }
                                />
                                  Browse
                                  </label>
                            </article>
                            {this.state.validations != null &&
                              this.state.validations[res.documentType["id"]] ? (
                                <i className="error animated bounce">
                                  {
                                    this.state.validations[
                                    res.documentType["id"]
                                    ]
                                  }
                                </i>
                              ) : null}
                          </article>
                        </article>

                        {this.state.isShowCropPup &&
                          res.documentType["id"] == 1 ? (
                            <section className="cropImg">
                              <section className="cropImgbg">
                                <article className="popup-header">
                                  <button
                                    onClick={() => this.closePopUp()}
                                    type="button"
                                    className="close"
                                  >
                                    ×
                                  </button>
                                  <h3 className="popup-title">CROP IMAGE</h3>
                                </article>
                                <article className="popup-body">
                                  <article className="row">
                                    <article className="col-md-12">
                                      <article className="cropArea">
                                        <Cropper
                                          style={{
                                            height: "50vh",
                                            width: "100%"
                                          }}
                                          aspectRatio={1}
                                          zoomable={true}
                                          autoCropArea={1}
                                          preview=".img-preview"
                                          guides={false}
                                          src={this.state.cropSrc}
                                          minCropBoxWidth={50}
                                          minCropBoxHeight={50}
                                          /* cropBoxMovable= {false}
                                          cropBoxResizable = {false} */
                                          ref={cropper => {
                                            this.cropper = cropper;
                                          }}
                                        />
                                      </article>
                                    </article>
                                  </article>
                                  <article className="row">
                                    <article className="col-md-12 text-center">
                                      <button
                                        className="updatePicBtn"
                                        onClick={event => this.cropImage(res)}
                                      >
                                        Update Picture
                                      </button>
                                    </article>
                                  </article>
                                </article>
                                <article className="popup-footer" />
                              </section>
                            </section>
                          ) : (
                            ""
                          )}
                      </article>
                    );
                  }
                })
                : ""}

              <article className="row">
                <article className="col-md-12">
                  <input
                    type="submit"
                    value="Save &amp; Continue"
                    onClick={this.checkValidation}
                    className="btn pull-right"
                  />
                </article>
              </article>
            </article>
          </article>
          <article />
        </article>
      </section>
    );
  }
}
const mapStateToProps = ({ applicationEducation }) => ({
  presentClassInfo: applicationEducation.presentClassInfo,

});

const mapDispatchToProps = dispatch => ({
  fetchPresentClass: inputData => dispatch(fetchPresentClassAction(inputData))

});

export default connect(mapStateToProps, mapDispatchToProps)(DocumentNeeded);
