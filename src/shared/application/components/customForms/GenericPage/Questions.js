import React, { Component } from "react";
import index from "axios";
import {
  config,
  nonValidateIds,
  extractStepTemplate
} from "../../../formconfig";
import { minLength, required, maxLength } from "../../../../../validation/rules";
import { ruleRunner } from "../../../../../validation/ruleRunner";
import AlertMessage from "../../../../common/components/alertMsg";
import Loader from "../../../../common/components/loader";
import MaskedInput from 'react-text-mask'

class Questions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questionList: [],
      isFormValid: false,
      validations: {},
      showLoader: false,
      msg: "",
      status: "",
      extraValidation: {}
    };
    this.onChangeValue = this.onChangeValue.bind(this);
    this.submitQuestion = this.submitQuestion.bind(this);
    this.countWords = this.countWords.bind(this);
  }

  getValidationRulesObject(fieldID, question) {
    let validationObject = {};
    if (this.state.validations.hasOwnProperty(fieldID)) {
      // if (this.state.extraValidation.hasOwnProperty(fieldID)) {
      validationObject.name = "*Field";
      // if (!nonValidateIds.includes(parseInt(fieldID)))
      if (question) {

        if (question.maxValue && question.minValue) {
          validationObject.validationFunctions = [required, minLength(question.minValue), maxLength(question.maxValue)];

        } else {
          validationObject.validationFunctions = [required];

        }
      } else {
        validationObject.name = "*Field";
        validationObject.validationFunctions = [required];
      }
      // } else {
      //   validationObject.name = "*Field";
      //   validationObject.validationFunctions = [required];
      // }
    }

    /*     if(this.state.extraValidation.hasOwnProperty(fieldID)){
      validationObject.name = "*Fields is Required";
      validationObject.validationFunctions = [maxLength(25)];
    } */
    return validationObject;
  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false,
    });
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    var question = {}
    for (let key in validations) {

      question = this.state.questionList && this.state.questionList.length > 0 && this.state.questionList.filter(obj => {
        return obj.question.id == parseInt(key)

      })
      let { name, validationFunctions } = this.getValidationRulesObject(key, question && question[0] && question[0].question);

      let questionVal = this.state.questionList
        .filter(res => {
          return res.question["id"] == key;
        })
        .map(item => {
          if (item.question["questionTypeId"] == 1 || item.question["questionTypeId"] == 4 || item.question["questionTypeId"] == 6 || item.question["questionTypeId"] == 7 || item.question["questionTypeId"] == 8 || item.question["questionTypeId"] == 9 || item.question["questionTypeId"] == 5) {
            return item.response;
          }

          else if (item.question["questionTypeId"] == 2  || item.question["questionTypeId"] == 10) {
            return item.optionId != null && item.optionId.length > 0
              ? item.optionId
              : null;
          } else {
            return item.optionId != null && item.optionId.length > 0
              ? item.optionId
              : null;
          }
        });
      let validationResult = ruleRunner(
        questionVal[0],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    console.log(validations)
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  componentDidMount() { 
    this.getQuestion();

  }

  getQuestion() {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(window.localStorage.getItem("userId"));
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    this.props.getQuestionData({
      userId: userId,
      scholarshipId: scholarshipId
    });
  }

  // componentWillMount() {
  //   this.getQuestion();
  // }

  submitQuestion() {
    let isSubmit = this.checkFormValidations();
    if (isSubmit) {
      let userId = "";
      let scholarshipId = "";
      if (window != undefined) {
        userId = parseInt(window.localStorage.getItem("userId"));
        scholarshipId = parseInt(
          window.localStorage.getItem("applicationSchID")
        );
      }
      this.props.saveQuestionData({
        userId: userId,
        scholarshipId: scholarshipId,
        question: this.state.questionList
      });
    }
  }

  onChangeValue(event, question) {
    const { id, value } = event.target;
    let validations = { ...this.state.validations };

    const flag = this.countWords(value);
    if (flag) {
      return;
    } else {
      if (validations.hasOwnProperty(id)) {
        const { name, validationFunctions } = this.getValidationRulesObject(id, question);

        const validationResult = ruleRunner(
          value,
          id,
          name,
          ...validationFunctions
        );
        validations[id] = validationResult[id];
      }

      if (this.state.questionList != null) {
        this.state.questionList
          .filter(res => {
            return res.question["id"] == id;
          })
          .map(items => {
            let index = this.state.questionList.findIndex(indexVal => {
              return indexVal.question["id"] == items.question["id"];
            });
            if (this.state.questionList[index].question.questionTypeId == 1) {
              this.state.questionList[index]["response"] = value;
            } else if (
              this.state.questionList[index].question.questionTypeId == 2
            ) {
              this.state.questionList[index]["optionId"] = Array(value);
            }
            else if (
              this.state.questionList[index].question.questionTypeId == 4
            ) {
              this.state.questionList[index]["response"] = value;
            }
            else if (
              this.state.questionList[index].question.questionTypeId == 5
            ) {
              this.state.questionList[index]["response"] = value;
            }
            else if (
              this.state.questionList[index].question.questionTypeId == 6
            ) {
              this.state.questionList[index]["response"] = value;
            }
            else if (
              this.state.questionList[index].question.questionTypeId == 7
            ) {
              
              this.state.questionList[index]["response"] = value;
            }
            else if (
              this.state.questionList[index].question.questionTypeId == 8
            ) {
              this.state.questionList[index]["response"] = value;
            }
            else if (
              this.state.questionList[index].question.questionTypeId == 9
            ) {
              this.state.questionList[index]["response"] = value;
            }
            else if (
              this.state.questionList[index].question.questionTypeId == 10
            ) {
              this.state.questionList[index]["optionId"] = Array(value);
            }
            else {
              if (event.target.checked) {
                this.state.questionList[index]["optionId"].push(
                  parseInt(value)
                );
                this.state.questionList[index]["optionId"] = [
                  ...new Set(this.state.questionList[index]["optionId"])
                ];
              } else {
                let multiIndexVal = this.state.questionList[index][
                  "optionId"
                ].indexOf(parseInt(value));
                if (multiIndexVal > -1) {
                  this.state.questionList[index]["optionId"].splice(
                    multiIndexVal,
                    1
                  );
                }
              }
            }
            this.setState({
              questionList: this.state.questionList,
              validations
            });
          });
      }
    }
  }

  countWords(str) {
    let counter = str.trim().split(/\s+/).length;
    if (counter > 600) {
      return true;
    } else {
      return false;
    }
  }

  componentWillReceiveProps(nextProps) {
    const { type, applicationQuestionData, payload } = nextProps.QuestionDataList;
    switch (type) {
      case "FETCH_APPLICATION_FORMS_QUESTION_SUCCESS":
        if (applicationQuestionData) {
          this.setState({
            questionList: applicationQuestionData
          });
          applicationQuestionData.map(res => {
            if (res["question"].isMandatory) {
              this.state.validations[res.question["id"]] = null;
            }
            if (
              res["question"].isMandatory && res.question["minValue"] && res.question["maxValue"]
            ) {
              // extra only for the subjective fields
              this.state.extraValidation[res.question["id"]] = null;
            }
          });
        }
        break;
      case "UPDATE_APPLICATION_FORMS_QUESTION_SUCCESS":
        // if (
        //   this.props.match.params.bsid &&
        //   this.props.redirectionSteps &&
        //   this.props.redirectionSteps.length > 0
        // ) {
        //   this.props.history.push(
        //     `/application/${this.props.match.params.bsid.toUpperCase()}/form/${
        //     this.props.redirectionSteps[
        //     this.props.redirectionSteps.indexOf("questions") + 1
        //     ]
        //     }`
        //   );
        // }
        this.setState(
          {
            status: true,
            msg: "More info has been saved",
            showLoader: true
          },
          () => this.getQuestion()
        );
        setTimeout(() => {
          this.props.applicationInstructionStep({
            scholarshipId: payload && payload.scholarshipId && payload.scholarshipId
          });
        }, 1000);
        break;
    }
  }

  markup(val) {
    return { __html: val };
  }

  render() {
    const { scholarshipSteps } = this.props;
    let quesetionInsTemplate;
    if (scholarshipSteps && scholarshipSteps.length) {
      quesetionInsTemplate = extractStepTemplate(scholarshipSteps, "QUESTIONS");
    }
    return (
      <section className="sectionwhite">
        <Loader isLoader={this.props.showLoader || this.props.QuestionDataList.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <article className="form">
          <article>
            <article>
              <article className="row">
                {
                  quesetionInsTemplate && quesetionInsTemplate.length > 0 &&
                    quesetionInsTemplate[0] &&
                    quesetionInsTemplate[0].message
                    ?
                    <article
                      dangerouslySetInnerHTML={this.markup(quesetionInsTemplate[0].message)}
                      className="col-md-12 subheadingerror"
                    />
                    : ""
                }

                <article className="col-md-12">
                  {this.state.questionList != null
                    ? this.state.questionList.map(res => {
                      if (res.question["questionTypeId"] == 1) {
                        // if question type subjective
                        return (
                          <article
                            className="form-group textareaque"
                            key={res.question["id"]}
                          >
                            <label htmlFor={res.question["id"]}>
                              {res.question["questionVerbiage"]}
                            </label>
                            <textarea
                              value={res.response != null ? res.response : ""}
                              onChange={event => this.onChangeValue(event, res.question)}
                              id={res.question["id"]}
                              row="5"
                            />

                            {this.state.validations[res.question["id"]] ? (
                              <span className="error animated bounce">
                                {this.state.validations[res.question["id"]]}
                              </span>
                            ) : null}
                          </article>
                        );
                      } else if (res.question["questionTypeId"] == 2) {
                        // if question type single choice

                        return (
                          <article
                            className="form-group"
                            key={res.question["id"]}
                          >
                            <article className="que">
                              <label htmlFor={res.question["id"]}>
                                {res.question["questionVerbiage"]}
                              </label>
                            </article>
                            <article className="ctrlWrapper favColor">

                              {res.question.questionOptions.map((option, i) => {
                                return (
                                  // <label>({option.optionKey})</label>
                                  <label>
                                    <input
                                      type="radio"
                                      key={option["id"]}
                                      value={
                                        option["id"] != null
                                          ? option["id"]
                                          : ""
                                      }
                                      onChange={event =>
                                        this.onChangeValue(event, res.question)
                                      }
                                      id={res.question["id"]}
                                      name={res.question["id"] + "_val"}
                                      checked={
                                        res["optionId"] && res["optionId"].length > 0 && res["optionId"][0] == option["id"]
                                          ? true
                                          : false
                                      }
                                    />
                                    {/* <span>{option.optionKey}</span> */}
                                    <span>{option.optionValue}</span>


                                    {/* <i> {option.optionValue}</i> */}
                                  </label>

                                );
                              })}
                            </article>


                            {this.state.validations[res.question["id"]] ? (
                              <article className="form-group">
                                <span className="error animated bounce">
                                  {this.state.validations[res.question["id"]]}
                                </span>
                              </article>
                            ) : null}
                            {/* <article className="paddingborder">
                              <span>&nbsp;</span>
                              <article className="border" />
                            </article> */}
                          </article>
                        );
                      }
                      else if (res.question["questionTypeId"] == 5) {
                        // if question type single choice

                        return (

                          <article
                            className="form-group"
                            key={res.question["id"]}
                          >
                            {/* <article className="que"> */}
                            <label htmlFor={res.question["id"]}>
                              {res.question["questionVerbiage"]}
                            </label>
                            {/* </article> */}
                            <article className="ctrlWrapper rating">
                              {res["response"] && <span className="ratingNumber">{
                                res["response"]
                              }/<i>{res.question.maxValue}</i></span>}

                              {[...Array(res.question.maxValue)].map((option, i) => {
                                return (


                                  <label
                                  //  htmlFor={`rating_${i+1}`}
                                  // htmlFor=""
                                  >
                                   
                                    <input
                                      // type="checkbx"
                                      // key={i+1}
                                      value={
                                        i + 1
                                      }
                                      type="checkbox"
                                      // name={`rating_${i}`} 
                                      // id="rating_1"
                                      onChange={event =>
                                        this.onChangeValue(event, res.question)
                                      }
                                      id={res.question["id"]}
                                      name={res.question["id"] + "_val"}
                                      checked={
                                        parseInt(res["response"]) >= i + 1
                                          ? true
                                          : false
                                      }
                                    />
                                    {/* <i> {option.optionValue}</i> */}
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                  </label>

                                );
                              })}
                            </article>



                            {this.state.validations[res.question["id"]] ? (
                              <article className="form-group">
                                <span className="error animated bounce">
                                  {this.state.validations[res.question["id"]]}
                                </span>
                              </article>
                            ) : null}
                            {/* <article className="paddingborder">
                              <span>&nbsp;</span>
                              <article className="border" />
                            </article> */}

                          </article>

                        );

                      }
                      else if (res.question["questionTypeId"] == 6) {
                        // if question type single choice

                        return (
                          <article
                            className="form-group"
                            key={res.question["id"]}
                          >
                            <label >
                              {res.question["questionVerbiage"]}
                            </label>
                            <input
                              value={res.response != null ? res.response : ""}
                              onChange={event => this.onChangeValue(event, res.question)}
                              id={res.question["id"]}
                              type="number"

                            />

                            {this.state.validations[res.question["id"]] ? (
                              <span className="error ">
                                {this.state.validations[res.question["id"]]}
                              </span>
                            ) : null}
                          </article>
                        );
                      }
                      else if (res.question["questionTypeId"] == 7) {
                        // if question type single choice

                        return (
                          <article
                            className="form-group"
                            key={res.question["id"]}
                          >
                            <label >
                              {res.question["questionVerbiage"]}
                            </label>
                            {/* <input
                            value={res.response != null ? res.response : ""}
                            onChange={event => this.onChangeValue(event)}
                            id={res.question["id"]}
                            type="number"

                          /> */}
                            <MaskedInput
                              type="text"
                              mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}

                              keepCharPositions={true}
                              guide={true}
                              data-custom={res.response != null ? res.response : ""}
                              onChange={event => this.onChangeValue(event, res.question)}
                              defaultValue={res.response}
                              id={res.question["id"]}
                              required />

                            {this.state.validations[res.question["id"]] ? (
                              <span className="error ">
                                {this.state.validations[res.question["id"]]}
                              </span>
                            ) : null}
                          </article>


                        );
                      }
                      else if (res.question["questionTypeId"] == 8) {
                        // if question type single choice

                        return (
                          <article
                            className="form-group"
                            key={res.question["id"]}
                          >
                            <label >
                              {res.question["questionVerbiage"]}
                            </label>
                            {/* <input
                            value={res.response != null ? res.response : ""}
                            onChange={event => this.onChangeValue(event)}
                            id={res.question["id"]}
                            type="number"

                          /> */}
                            <MaskedInput
                              type="text"
                              mask={[/\d/, /\d/, ':', /\d/, /\d/, ':', /\d/, /\d/]}

                              keepCharPositions={true}
                              guide={true}
                              data-custom={res.response != null ? res.response : ""}
                              defaultValue={res.response}
                              onChange={event => this.onChangeValue(event, res.question)}

                              id={res.question["id"]}

                              required />

                            {this.state.validations[res.question["id"]] ? (
                              <span className="error ">
                                {this.state.validations[res.question["id"]]}
                              </span>
                            ) : null}
                          </article>
                        );
                      }
                      else if (res.question["questionTypeId"] == 9) {
                        // if question type single choice

                        return (
                          <article
                            className="form-group"
                            key={res.question["id"]}
                          >
                            <label >
                              {res.question["questionVerbiage"]}
                            </label>
                            <input
                              value={res.response != null ? res.response : ""}
                              onChange={event => this.onChangeValue(event, res.question)}
                              id={res.question["id"]}
                              type="tel"
                            // maxLength="4"

                            />

                            {this.state.validations[res.question["id"]] ? (
                              <span className="error ">
                                {this.state.validations[res.question["id"]]}
                              </span>
                            ) : null}
                          </article>
                        );
                      }
                      else if (res.question["questionTypeId"] == 10) {
                        // if question type single choice

                        // return (
                        //   <article
                        //     className="form-group radio-btn"
                        //     key={res.question["id"]}
                        //   >
                        //     <article className="que">
                        //       <label htmlFor={res.question["id"]}>
                        //         {res.question["questionVerbiage"]}
                        //       </label>
                        //     </article>
                        //     {res.question.questionOptions.map((option, i) => {
                        //       return (
                        //         <div key={i}>
                        //           <strong>
                        //             <label>({option.optionKey})</label>
                        //           </strong>
                        //           <span>
                        //             <label>
                        //               <input
                        //                 type="radio"
                        //                 key={option["id"]}
                        //                 value={
                        //                   option["id"] != null
                        //                     ? option["id"]
                        //                     : ""
                        //                 }
                        //                 onChange={event =>
                        //                   this.onChangeValue(event,res.question)
                        //                 }
                        //                 id={res.question["id"]}
                        //                 name={res.question["id"] + "_val"}
                        //                 checked={
                        //                   res["optionId"][0] == option["id"]
                        //                     ? true
                        //                     : false
                        //                 }
                        //               />
                        //               <span />
                        //               <i> {option.optionValue}</i>
                        //             </label>
                        //           </span>
                        //         </div>
                        //         // <span
                        //         //   style={{ float: "left", display: "inline" }}
                        //         // >
                        //         //   <strong>
                        //         //     <label>({option.optionKey})</label>
                        //         //   </strong>

                        //         // <input
                        //         //   type="radio"
                        //         //   value={
                        //         //     option["id"] != null ? option["id"] : ""
                        //         //   }
                        //         //   onChange={event =>
                        //         //     this.onChangeValue(event)
                        //         //   }
                        //         //   id={option["id"]}
                        //         //   name={res.question["id"] + "_val"}
                        //         // />

                        //         //   <label htmlFor={option["id"]}>
                        //         //     {option.optionValue}
                        //         //   </label>
                        //         // </span>
                        //       );
                        //     })}

                        //     {this.state.validations[res.question["id"]] ? (
                        //       <article className="form-group">
                        //         <span className="error animated bounce">
                        //           {this.state.validations[res.question["id"]]}
                        //         </span>
                        //       </article>
                        //     ) : null}
                        //     {/* <article className="paddingborder">
                        //       <span>&nbsp;</span>
                        //       <article className="border" />
                        //     </article> */}
                        //   </article>
                        // );


                        return (
                          <article
                            className="form-group"
                            key={res.question["id"]}
                          >
                            <article className="que">
                              <label htmlFor={res.question["id"]}>
                                {res.question["questionVerbiage"]}
                              </label>
                            </article>
                            <article className="ctrlWrapper favColor">

                              {res.question.questionOptions.map((option, i) => {
                                return (
                                  // <label>({option.optionKey})</label>
                                  <label>
                                    <input
                                      type="radio"
                                      key={option["id"]}
                                      value={
                                        option["id"] != null
                                          ? option["id"]
                                          : ""
                                      }
                                      onChange={event =>
                                        this.onChangeValue(event, res.question)
                                      }
                                      id={res.question["id"]}
                                      name={res.question["id"] + "_val"}
                                      checked={
                                        res["optionId"][0] == option["id"]
                                          ? true
                                          : false
                                      }
                                    />
                                    {/* <span>{option.optionKey}</span> */}
                                    <span>{option.optionValue}</span>


                                    {/* <i> {option.optionValue}</i> */}
                                  </label>

                                );
                              })}
                            </article>


                            {this.state.validations[res.question["id"]] ? (
                              <article className="form-group">
                                <span className="error animated bounce">
                                  {this.state.validations[res.question["id"]]}
                                </span>
                              </article>
                            ) : null}
                            {/* <article className="paddingborder">
                              <span>&nbsp;</span>
                              <article className="border" />
                            </article> */}
                          </article>
                        );


                      }
                      // else if (res.question["questionTypeId"] == 11) {
                      //   // if question type single choice

                      //   return (
                      //     <article
                      //       className="form-group radio-btn"
                      //       key={res.question["id"]}
                      //     >
                      //       <article className="que">
                      //         <label htmlFor={res.question["id"]}>
                      //           {res.question["questionVerbiage"]}
                      //         </label>
                      //       </article>
                      //       {res.question.questionOptions.map((option, i) => {
                      //         return (
                      //           <div key={i}>
                      //             <strong>
                      //               <label>({option.optionKey})</label>
                      //             </strong>
                      //             <span>
                      //               <label>
                      //                 <input
                      //                   type="radio"
                      //                   key={option["id"]}
                      //                   value={
                      //                     option["id"] != null
                      //                       ? option["id"]
                      //                       : ""
                      //                   }
                      //                   onChange={event =>
                      //                     this.onChangeValue(event, res.question)
                      //                   }
                      //                   id={res.question["id"]}
                      //                   name={res.question["id"] + "_val"}
                      //                   checked={
                      //                     res["optionId"][0] == option["id"]
                      //                       ? true
                      //                       : false
                      //                   }
                      //                 />
                      //                 <span />
                      //                 <i> {option.optionValue}</i>
                      //               </label>
                      //             </span>
                      //           </div>
                      //           // <span
                      //           //   style={{ float: "left", display: "inline" }}
                      //           // >
                      //           //   <strong>
                      //           //     <label>({option.optionKey})</label>
                      //           //   </strong>

                      //           // <input
                      //           //   type="radio"
                      //           //   value={
                      //           //     option["id"] != null ? option["id"] : ""
                      //           //   }
                      //           //   onChange={event =>
                      //           //     this.onChangeValue(event)
                      //           //   }
                      //           //   id={option["id"]}
                      //           //   name={res.question["id"] + "_val"}
                      //           // />

                      //           //   <label htmlFor={option["id"]}>
                      //           //     {option.optionValue}
                      //           //   </label>
                      //           // </span>
                      //         );
                      //       })}

                      //       {this.state.validations[res.question["id"]] ? (
                      //         <article className="form-group">
                      //           <span className="error animated bounce">
                      //             {this.state.validations[res.question["id"]]}
                      //           </span>
                      //         </article>
                      //       ) : null}
                      //       {/* <article className="paddingborder">
                      //         <span>&nbsp;</span>
                      //         <article className="border" />
                      //       </article> */}
                      //     </article>
                      //   );
                      // }
                      else if (res.question["questionTypeId"] == 11) {
                        // if question type single choice

                        return (
                          <article
                            className="form-group radio-btn"
                            key={res.question["id"]}
                          >
                            <article className="que highlights">
                              <label htmlFor={res.question["id"]}>
                                {res.question["questionVerbiage"]}
                              </label>
                                                          
                            </article>
                            {this.state.validations[res.question["id"]] ? (
                              <article className="form-group">
                                <span className="error animated bounce">
                                  {this.state.validations[res.question["id"]]}
                                </span>
                              </article>
                            ) : null}
                            {/* <article className="paddingborder">
                              <span>&nbsp;</span>
                              <article className="border" />
                            </article> */}
                          </article>
                        );
                      }
                      else if (res.question["questionTypeId"] == 4) {
                        return (
                          // <article
                          //   className="form-group  width radio-btn"
                          //   key={res.question["id"]}
                          // >
                          //   <article className="que">
                          //     <label htmlFor={res.question["id"]}>
                          //       {res.question["questionVerbiage"]}
                          //     </label>
                          //   </article>
                          //   {}{" "}
                          //   <select
                          //     className="icon"
                          //     id={res.question["id"]}
                          //     name={res.question["id"] + "_val"}
                          //     onChange={event => this.onChangeValue(event)}
                          //     value={res.optionId.join("")}
                          //   >
                          //     <option value="">-Select-</option>
                          //     {res.question.questionOptions.map(option => {
                          //       return (
                          //         <option key={option.id} value={option.id}>
                          //           {option.optionValue}
                          //         </option>
                          //       );
                          //     })} 
                          //   </select>
                          //   {this.state.validations[res.question["id"]] ? (
                          //     <article className="form-group">
                          //       <span className="error animated bounce">
                          //         {this.state.validations[res.question["id"]]}
                          //       </span>
                          //     </article>
                          //   ) : null}
                          //   <article className="paddingborder">
                          //     <span>&nbsp;</span>
                          //     <article className="border" />
                          //   </article>
                          // </article>
                          <article
                            className="form-group"
                            key={res.question["id"]}
                          >
                            <label >
                              {res.question["questionVerbiage"]}
                            </label>
                            <input
                              value={res.response != null ? res.response : ""}
                              onChange={event => this.onChangeValue(event, res.question)}
                              id={res.question["id"]}
                            // maxlength="4"

                            />

                            {this.state.validations[res.question["id"]] ? (
                              <span className="error ">
                                {this.state.validations[res.question["id"]]}
                              </span>
                            ) : null}
                          </article>

                        );
                      } else {
                        // if the question type multiple choice

                        return (
                          <article
                            className="form-group checkbox"
                            key={res.question["id"]}
                          >
                            <article className="que">
                              <div htmlFor={res.question["id"]}>
                                {res.question["questionVerbiage"]}
                              </div>
                            </article>
                            {res.question.questionOptions.map((option, i) => {
                              return (
                                <div key={i}>
                                  <strong>
                                    <span>({option.optionKey})</span>
                                  </strong>

                                  <span>
                                    <label>
                                      <input
                                        key={option["id"]}
                                        type="checkbox"
                                        value={
                                          option["id"] != null
                                            ? option["id"]
                                            : ""
                                        }
                                        onChange={event =>
                                          this.onChangeValue(event,res.question)
                                        }
                                        id={res.question["id"]}
                                        defaultChecked={
                                          res["optionId"] && res["optionId"].indexOf(
                                            option["id"]
                                          ) > -1
                                            ? true
                                            : false
                                        }
                                      />
                                      <span />
                                      <i>{option.optionValue}</i>
                                    </label>
                                  </span>
                                </div>
                              );
                            })}

                            {this.state.validations[res.question["id"]] ? (
                              <article className="form-group">
                                <span className="error animated bounce">
                                  {this.state.validations[res.question["id"]]}
                                </span>
                              </article>
                            ) : null}
                            {/* <article className="paddingborder">
                              <span>&nbsp;</span>
                              <article className="border" />
                            </article> */}
                          </article>
                        );

                        // return (
                        //   <article
                        //     className="form-group"
                        //     key={res.question["id"]}
                        //   >
                        //     <article className="que">
                        //       <label htmlFor={res.question["id"]}>
                        //         {res.question["questionVerbiage"]}
                        //       </label>
                        //     </article>
                        //     <article className="ctrlWrapper favColor">

                        //       {res.question.questionOptions.map((option, i) => {
                        //         return (
                        //           // <label>({option.optionKey})</label>
                        //           <label>
                        //             <input
                        //               type="radio"
                        //               key={option["id"]}
                        //               value={
                        //                 option["id"] != null
                        //                   ? option["id"]
                        //                   : ""
                        //               }
                        //               onChange={event =>
                        //                 this.onChangeValue(event, res.question)
                        //               }
                        //               id={res.question["id"]}
                        //               name={res.question["id"] + "_val"}
                        //               // checked={
                        //               //   res["optionId"][0] == option["id"]
                        //               //     ? true
                        //               //     : false
                        //               // }
                        //               defaultChecked={
                        //                 res["optionId"].indexOf(
                        //                   option["id"]
                        //                 ) > -1
                        //                   ? true
                        //                   : false
                        //               }
                        //             />
                        //             {/* <span>{option.optionKey}</span> */}
                        //             <span>{option.optionValue}</span>


                        //             {/* <i> {option.optionValue}</i> */}
                        //           </label>

                        //         );
                        //       })}
                        //     </article>


                        //     {this.state.validations[res.question["id"]] ? (
                        //       <article className="form-group">
                        //         <span className="error animated bounce">
                        //           {this.state.validations[res.question["id"]]}
                        //         </span>
                        //       </article>
                        //     ) : null}
                        //     {/* <article className="paddingborder">
                        //       <span>&nbsp;</span>
                        //       <article className="border" />
                        //     </article> */}
                        //   </article>
                        // );
                      }
                    })
                    : ""}
                </article>

                {/* Add new question type */}
                {/* <article className="col-md-12"> */}
                {/* <article className="form-group">
                    <label>
                      What is your name? *
                    </label>
                    <input type="text" />
                    <span className="error">
                      Error
                    </span>
                  </article> */}
                {/* <article className="form-group">
                    <label>
                      What is your fav color? *
                    </label>
                    <article className="ctrlWrapper favColor">
                      <label htmlFor="red">
                        <input type="radio" name="fav_color" id="red" value="red" />
                        <span>Red</span>
                      </label>
                      <label htmlFor="green">
                        <input type="radio" name="fav_color" id="green" value="green" />
                        <span>Green</span>
                      </label>
                      <label htmlFor="yellow">
                        <input type="radio" name="fav_color" id="yellow" value="yellow" />
                        <span>Yellow</span>
                      </label>
                    </article>
                    <span className="error">
                      Error
                    </span>
                  </article> */}
                {/* <article className="form-group">
                    <label>
                      What is your rating?
                    </label>
                    <article className="ctrlWrapper rating">
                      <span className="ratingNumber">3/<i>10</i></span>
                      <label htmlFor="">
                        <input type="radio" name="rating" id="" />
                        <i className="fa fa-star" aria-hidden="true"></i>
                      </label>
                      <label htmlFor="">
                        <input type="radio" name="rating" id="" />
                        <i className="fa fa-star" aria-hidden="true"></i>
                      </label>
                      <label htmlFor="">
                        <input type="radio" name="rating" id="" />
                        <i className="fa fa-star" aria-hidden="true"></i>
                      </label>
                      <label htmlFor="">
                        <input type="radio" name="rating" id="" />
                        <i className="fa fa-star" aria-hidden="true"></i>
                      </label>
                      <label htmlFor="">
                        <input type="radio" name="rating" id="" />
                        <i className="fa fa-star" aria-hidden="true"></i>
                      </label>
                      <label htmlFor="">
                        <input type="radio" name="rating" id="" />
                        <i className="fa fa-star" aria-hidden="true"></i>
                      </label>
                      <label htmlFor="rating">
                        <input type="radio" name="rating" id="" />
                        <i className="fa fa-star" aria-hidden="true"></i>
                      </label>
                      <label htmlFor="">
                        <input type="radio" name="rating" id="" />
                        <i className="fa fa-star" aria-hidden="true"></i>
                      </label>
                      <label htmlFor="">
                        <input type="radio" name="rating" id="" />
                        <i className="fa fa-star" aria-hidden="true"></i>
                      </label>
                      <label htmlFor="">
                        <input type="radio" name="rating" id="" />
                        <i className="fa fa-star" aria-hidden="true"></i>
                      </label>
                    </article>
                  </article> */}
                {/* </article> */}
                {/* <article className="col-md-12 form-group-wrapper">
                  <article className="form-group">
                    <label>
                      In which year you have passed graduation?
                    </label>
                    <input type="text" />
                  </article>
                  <article className="form-group">
                    <label>
                      What is date of birth?
                    </label>
                    <input type="text" />
                  </article>
                  <article className="form-group">
                    <label>
                      Which time you prefer?
                    </label>
                    <input type="text" />
                  </article>
                  <article className="form-group">
                    <label>
                      How old are you?
                    </label>
                    <input type="text" />
                  </article>
                </article> */}
                {/* <article className="col-md-12"> */}
                {/* <article className="form-group">
                    <label>
                      Which one your fav fruit?
                    </label>
                    <article className="ctrlWrapper favFruit">
                      <label htmlFor="apple">
                        <input type="radio" name="fav_fruit" id="apple" />
                        <span>Apple</span>
                      </label>
                      <label htmlFor="orange">
                        <input type="radio" name="fav_fruit" id="orange" />
                        <span>Orange</span>
                      </label>
                    </article>
                  </article> */}
                {/* <article className="form-group">
                    <label>
                      Arrange in order.
                    </label>
                    <article className="ctrlWrapper dragDropItems">
                      <article className="item">4</article>
                      <article className="item">1</article>
                      <article className="item">5</article>
                    </article>
                  </article> */}
                {/* </article> */}


                {/* <article className="col-md-12 radio-btn">
                  <h3>How did you know about this scholarship?</h3>
                  <span>
                    <label>
                      <input type="radio" name="radio" checked={true} />
                      <span />
                      <i>Newspaper</i>
                    </label>
                  </span>
                  <span className="marginleft">
                    <label>
                      <input type="radio" name="radio" />
                      <span />
                      <i>Radio</i>
                    </label>
                  </span>
                  <span className="marginleft">
                    <label>
                      <input type="radio" name="radio" />
                      <span />
                      <i>Buddy4study</i>
                    </label>
                  </span>
                  <span className="marginleft">
                    <label>
                      <input type="radio" name="radio" />
                      <span />
                      <i>Friends</i>
                    </label>
                  </span>
                  <span className="marginleft">
                    <label>
                      <input type="radio" name="radio" />
                      <span />
                      <i>College/institute</i>
                    </label>
                  </span>
                  <span className="marginleft">
                    <label>
                      <input type="radio" name="radio" />
                      <span />
                      <i>Social media</i>
                    </label>
                  </span>
                  <span className="marginleft">
                    <label>
                      <input type="radio" name="radio" />
                      <span />
                      <i>TV</i>
                    </label>
                  </span>
                  <span className="marginleft">
                    <label>
                      <input type="radio" name="radio" />
                      <span />
                      <i>Email</i>
                    </label>
                  </span>
                </article> */}
              </article>

              <article className="row">
                <article className="col-md-12">
                  <input
                    type="submit"
                    value="Save & Continue"
                    className="btn  pull-right"
                    onClick={this.submitQuestion}
                  />
                </article>
              </article>
            </article>
          </article >
        </article >
      </section >
    );
  }
}

export default Questions;
