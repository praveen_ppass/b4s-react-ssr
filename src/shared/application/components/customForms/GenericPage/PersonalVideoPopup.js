import React, { Component } from "react";

class PersonalVideoPopup extends Component {
  render() {   
    return (
      <section className={'alertOverlay'}>
			<article className={'alertPopup'}>
			    <span className={'icoTimes'} onClick={this.props.brandVideoClose}></span>
				<article className={'innerContainer'}>
					<iframe src="https://www.youtube.com/embed/5HA733teJeA?autoplay=1" frameborder="0" allowfullscreen="0"></iframe>

				</article>
			</article>
		</section>
      );
  }
}

export default PersonalVideoPopup;
