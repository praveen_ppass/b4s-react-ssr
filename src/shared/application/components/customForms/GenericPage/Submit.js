import React, { Component } from "react";
import Loader from "../../../../common/components/loader";
import AlertMessage from "../../../../common/components/alertMsg";
import { all } from "redux-saga/effects";
import { stepNamesToDisplay, childEduBSID } from "../../../formconfig";
import { Route, Link, Redirect } from "react-router-dom";
import gblFunc from "../../../../../globals/globalFunctions";
import { smsApplicationDetailConfig } from "../../../formconfig";

class SubmitButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      presentClass: null,
      submitted: false,
      redirectToEducation: false,
      historyList: {
        personalInfo: {},
        educationInfos: [],
        familyInfos: [],
        interests: [],
        references: [],
        entranceExams: [],
        scholarshipHistories: [],
        scholarshipQuestion: [],
        entranceExams: [],
        bankDetail: []
      },
      personalConfig: [],
      educationConfig: [],
      familyConfig: [],
      refrenceConfig: [],
      isSubmitDisabled: true,
      msg: "",
      status: "",
      showLoader: false,
      isCompleted: false,
      decHtml: "",
      showpopupTerms: false,
      isRedirect: false,
      tnc: false
    };

    this.submitApplicaitonForm = this.submitApplicaitonForm.bind(this);
    this.checkCompleted = this.checkCompleted.bind(this);
    this.checkSteps = this.checkSteps.bind(this);
  }

  checkSteps(applicationStepInstruction, scholarshipSteps) {
    let isSummary = false;
    let isBankDetail = false;
    const rsType = gblFunc.getStoreHulAuthType("hulType");
    // console.log("UserType", rsType);

    // if (rsType && rsType === "3P")
    scholarshipSteps.map(d => {
      if (d.step == "" || d.step == " " || d.step.toUpperCase() == "SUMMARY") {
        isSummary = true;
      }
      if (rsType && rsType == "3P" && d.step == "BANK_DETAIL") {
        isBankDetail = true;
      }
    });
    if (isSummary && isBankDetail) {
      return applicationStepInstruction.length === scholarshipSteps.length - 2;
    } else if (isSummary) {
      return applicationStepInstruction.length === scholarshipSteps.length - 1;
    } else if (isBankDetail) {
      return applicationStepInstruction.length === scholarshipSteps.length - 1;
    }
    return applicationStepInstruction.length === scholarshipSteps.length;
  }

  close() {
    if (this.state.redirectToEducation) {
      this.setState({ redirectToEducation: false }, () =>
        this.props.history.push(
          `/application/${this.props.match.params.bsid.toUpperCase()}/form/educationInfo`
        )
      );
    } else {
      this.setState({
        showLoader: false,
        msg: "",
        status: false
      });
    }
  }
  togglePopup() {
    this.setState({
      showpopupTerms: !this.state.showpopupTerms
    });
  }
  componentWillReceiveProps(nextProps) {
    const { type } = nextProps.applicationSummery;
    switch (type) {
      case "FETCH_APPLICATION_STEP_SUCCESS":
        const { applicationStepInstruction, scholarshipSteps } = nextProps;
        if (
          !this.state.submitted &&
          applicationStepInstruction &&
          scholarshipSteps &&
          this.props.applicationStepInstruction &&
          this.props.scholarshipSteps &&
          applicationStepInstruction.length ===
          this.props.applicationStepInstruction.length &&
          this.props.scholarshipSteps.length === scholarshipSteps.length
        ) {
          return false;
        }
        if (
          applicationStepInstruction &&
          scholarshipSteps &&
          this.checkSteps(applicationStepInstruction, scholarshipSteps)
        ) {
          this.setState({ isCompleted: true }, () => {
            if (this.state.submitted) {
              this.submitApplicaitonForm();
            }
          });
        } else {
          this.setState({ isCompleted: false }, () => {
            if (this.state.submitted) {
              this.submitApplicaitonForm();
            }
          });
        }

        break;
      case "FETCH_SCHOLARSHIP_APPLY_SUCCESS_SUMMARY":
        this.setState({
          isRedirect: true
        });

        break;
      case "FETCH_SCHOLARSHIP_APPLY_FAILURE_SUMMARY":
        this.setState({
          status: false,
          msg: "Sorry!! Server Error",
          showLoader: true
        });

        break;
      default:
        break;
    }
  }

  isSubmitDisabled(event) {
    if (event.target.checked) {
      this.setState({
        isSubmitDisabled: false,
        tnc: true
      });
    } else {
      this.setState({
        isSubmitDisabled: true,
        tnc: false
      });
    }
  }

  checkCompleted(nextProps) {
    const { applicationStepInstruction, scholarshipSteps } = nextProps;
    if (
      applicationStepInstruction &&
      scholarshipSteps &&
      applicationStepInstruction.length === scholarshipSteps.length - 1
    ) {
      this.setState(
        {
          isCompleted: true
        },
        () => this.state.isCompleted
      );
    } else {
      this.setState(
        {
          isCompleted: false
        },
        () => this.state.isCompleted
      );
    }
  }
  submitApplicaitonForm() {
    // console.log(this.props);

    if (this.state.isCompleted) {
      let scholarshipId = "";
      if (window != undefined) {
        scholarshipId = parseInt(
          window.localStorage.getItem("applicationSchID")
        );
      }
      // this.setState({ submitted: false }, () =>
      //   this.props.fetchSchApply({
      //     scholarshipId: scholarshipId
      //   })
      // );
      this.props.history.push({pathname:  `/application/${this.props.match.params.bsid.toUpperCase()}/form/summary`,
      state:this.props.match.params});
    //   this.props.history.push({pathname:"/summary",
    // state:this.props.match.params})
      return false;
    } else {
      this.setState({
        status: false,
        msg: "Kindly complete all steps",
        showLoader: true,
        submitted: false
      });
      return false;
    }
  }

  markup(val) {
    return { __html: val };
  }

  displayQuestionAns(res) {
    if (res.question["questionTypeId"] == 1) {
      return res.response;
    } else if (res.question["questionTypeId"] == 2) {
      if (res.optionId != null && res.optionId.length > 0) {
        let signleOptionAns = res.question["questionOptions"].filter(items => {
          return items["id"] == res.optionId[0];
        });

        if (signleOptionAns != null && signleOptionAns.length > 0) {
          return signleOptionAns[0].optionValue;
        } else {
          return "No Result Found";
        }
      }
    } else {
      if (res.optionId != null && res.optionId.length > 0) {
        let multiList = [];
        let multiOptionAns = res.question["questionOptions"].filter(items => {
          return res.optionId.indexOf(items["id"]) > -1;
        });

        if (multiOptionAns != null && multiOptionAns.length > 0) {
          multiOptionAns.map(items => {
            multiList.push(items.optionValue);
          });
          return multiList.length ? multiList.join(", ") : "";
        } else {
          return "No Result Found";
        }
      }
    }
  }

  render() {
    if (
      this.state.decHtml === "" &&
      this.props.instructions &&
      this.props.instructions.consent
    ) {
      this.setState({ decHtml:this.props.instructions ? this.props.instructions.consent:"" });
    }
    const { applicationStepInstruction, scholarshipSteps, match } = this.props;
    let isSubmitArray = [];
    let redirecTo = null;

    if (applicationStepInstruction && scholarshipSteps) {
      if (
        Array.isArray(applicationStepInstruction) &&
        Array.isArray(scholarshipSteps)
      ) {
        if (
          applicationStepInstruction.length > 0 &&
          scholarshipSteps.length > 0
        ) {
          isSubmitArray = scholarshipSteps.map(schStep => {
            return applicationStepInstruction.indexOf(schStep.step) > -1
              ? true
              : false;
          });
        }
      }
    }
    if (
      this.state.isRedirect &&
      gblFunc.getStoreUserDetails()["vleUser"] == "true"
    ) {
      redirecTo = <Redirect to={`/vle/student-list`} />;
    } else if (
      this.state.isRedirect &&
      ["CBS3", "CBI2"].includes(this.props.match.params.bsid)
    ) {
      redirecTo = (
        <Redirect
          to={`/payment/${this.props.match.params.bsid}/collegeboard`}
        />
      );
    } else if (
      this.state.isRedirect &&
      ["PMES01"].includes(this.props.match.params.bsid.toUpperCase())
    ) {
      redirecTo = (
        <Redirect to={`/payment/${this.props.match.params.bsid}/pearson`} />
      );
    } else if (this.state.isRedirect && this.props.instructions && this.props.instructions.enableSharing) {
      redirecTo = (
        <Redirect
          to={`/${this.props.match.params.bsid.toUpperCase()}/application-submitted`}
        />
      );
    } else if (
      this.state.isRedirect &&
      ["SKKS1", "CES7","CES8", "SCE3", "CKS1", "SSE4", "HUL1", "FAL9","HCL2"].includes(
        this.props.match.params.bsid.toUpperCase()
      )
    ) {
      redirecTo = (
        <Redirect
          to={{
            pathname: `/application/${this.props.match.params.bsid.toUpperCase()}/instruction`,
            state: {
              from: "summary",
              isChildForm:
                childEduBSID[this.props.match.params.bsid.toUpperCase()] ||
                  ["FAL9"].includes(this.props.match.params.bsid.toUpperCase())
                  ? true
                  : false
            }
          }}
        />
      );
    } else if (this.state.isRedirect && this.props.instructions && this.props.instructions.enableSharing) {
      redirecTo = (
        <Redirect
          to={`/${this.props.match.params.bsid.toUpperCase()}/application-submitted`}
        />
      );
    } else if (
      this.state.isRedirect &&
      this.props.instructions &&
      this.props.instructions.redirectUrl != null
    ) {
      if (
        this.props.instructions && ( this.props.instructions.redirectUrl.includes("http") ||
        this.props.instructions.redirectUrl.includes("https"))
      ) {
        window.open(this.props.instructions && this.props.instructions.redirectUrl, "_blank");
      } else {
        redirecTo = <Redirect to={this.props.instructions && this.props.instructions.redirectUrl} />;
      }
    } else if (this.state.isRedirect) {
      redirecTo = <Redirect to={`/myscholarship/?tab=application-status`} />;
    }

    return (
      <section className="sectionwhite">
        {!smsApplicationDetailConfig.isSMSAdmin() && redirecTo}
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <section>
          {(!smsApplicationDetailConfig.isSMSAdmin() ||
            smsApplicationDetailConfig.isSMSWriteAdmin()) && (
              <article className="row summarySec">
                <div className="col-md-12">
                  <p dangerouslySetInnerHTML={this.markup(this.state.decHtml)} />
                  {["CBI2", "CBS3"].includes(
                    this.props.match.params.bsid.toUpperCase()
                  ) ? (
                      <p style={{ color: "red" }}>
                        College Board reserves the right at any time for any reason
                        to request a proof of your eligibility. In case College
                        Board is unable to verify a student's eligibility to its
                        satisfaction, the student's scholarship and/or registration
                        may be revoked at any time at College Board's sole
                        discretion.
                  </p>
                    ) : null}

                  <p>
                    <label>
                      <input
                        type="checkbox"
                        value="2"
                        id="20"
                        name="20_val"
                        onChange={event => this.isSubmitDisabled(event)}
                      />
                      <span className="red" />
                    </label>
                    &nbsp;&nbsp;I have read the{" "}
                    {["IFBMS1"].includes(
                      this.props.match.params.bsid.toUpperCase()
                    ) ? (
                        <span
                          className="cursor"
                          onClick={this.togglePopup.bind(this)}
                        >
                          terms and conditions
                    </span>
                      ) : this.props.instructions &&
                        this.props.instructions.tnc &&
                        this.props.instructions.tnc != null ? (
                          <span
                            className="cursor"
                            onClick={this.togglePopup.bind(this)}
                          >
                            terms and conditions
                    </span>
                        ) : (
                          <a
                            target="_blank"
                            href="https://buddy4study.com/terms-and-conditions"
                          >
                            terms and conditions
                    </a>
                        )}{" "}
                    carefully, I agree that by submitting this application, I am
                    electronically signing the application.
                </p>
                  {this.state.showpopupTerms ? (
                    <Terms
                      text="Close Me"
                      closePopup={this.togglePopup.bind(this)}
                      tnc={
                        this.props.instructions && this.props.instructions.tnc
                          ? this.props.instructions.tnc
                          : null
                      }
                    />
                  ) : null}
                </div>
                {this.props.match &&
                  this.props.match.params &&
                  ["CBI2", "CBS3", "PMES01"].includes(
                    this.props.match.params.bsid.toUpperCase()
                  ) ? (
                    <article className="col-md-12">
                      <input
                        onClick={() => {
                          if (!this.state.tnc) {
                            this.setState({
                              status: false,
                              msg: "Kindly agree to the terms and conditions.",
                              showLoader: true
                            });
                            return false;
                          }
                          // console.log(this.props);
                          const userId = gblFunc.getStoreUserDetails()["userId"];
                          const scholarshipId =this.props.instructions && this.props.instructions
                            .scholarshipId;
                          this.props.getApplicationInstructionStep({
                            userId,
                            scholarshipId
                          });
                          if (!this.state.submitted) {
                            this.setState({ submitted: true });
                          }
                        }}
                        type="button"
                        value="Submit & Pay"
                        className="btn-yellow btnsubmit"
                      />
                    </article>
                  ) : (
                    <article className="col-md-12">
                      <input
                        onClick={() => {
                          if (!this.state.tnc) {
                            this.setState({
                              status: false,
                              msg: "Kindly agree to the terms and conditions.",
                              showLoader: true
                            });
                            return false;
                          }
                          // console.log(this.props);
                          const userId = gblFunc.getStoreUserDetails()["userId"];
                          const scholarshipId = this.props.instructions && this.props.instructions
                            .scholarshipId;
                          this.props.getApplicationInstructionStep({
                            userId,
                            scholarshipId
                          });
                          if (!this.state.submitted) {
                            this.setState({ submitted: true });
                          }
                        }}
                        type="button"
                        value="Preview"
                        className="btn-yellow btnsubmit"
                      />
                    </article>
                  )}
              </article>
            )}
        </section>
      </section>
    );
  }
}

class Terms extends React.Component {
  render() {
    return (
      <article className="popup">
        <article className="popupsms">
          <article className="popup_inner loginpopup">
            <article className="LoginRegPopup">
              <section className="modal fade modelAuthPopup1">
                <section className="modal-dialog">
                  <section className="modal-content modelBg">
                    <article className="modal-header">
                      <h3 className="modal-title">Terms and Conditions</h3>
                      <button
                        type="button"
                        className="close btnPos"
                        onClick={this.props.closePopup}
                      >
                        <i>&times;</i>
                      </button>
                    </article>

                    <article className="modal-body forgot terms">
                      <article className="row">
                        <article className="col-md-12 ">
                          {/* <h1>Terms and Conditions :</h1> */}
                          {this.props.tnc && this.props.tnc != null ? (
                            <span
                              dangerouslySetInnerHTML={{
                                __html: this.props.tnc
                              }}
                            />
                          ) : (
                              <ul>
                                <li>
                                  The Scholarships are open for studies in India
                                  and for Indian Nationals only.
                              </li>
                                <li>
                                  The Candidates applying for the scholarship
                                  under this program should be pursuing MBA and/
                                  or would be applying for MBA during the academic
                                year 2019-20.{" "}
                                </li>
                                <li>
                                  The amount of Scholarship that will be provided
                                  under this program will be a maximum of Rs.1
                                  Lakh per student, per academic year and the
                                  scholarship amount will be only towards tuition
                                fees and books.{" "}
                                </li>
                                <li>
                                  The Gross annual family income, i.e. income of
                                  the parents/guardians from all sources should
                                  not be more than Rs.6 Lakhs per annum. Students
                                  having the lowest family income shall be given
                                preference in the ascending order.{" "}
                                </li>
                                <li>
                                  Candidates will be required to upload documents,
                                  e.g. Income Tax Returns of their
                                  parents/guardian(s) or in the alternative, Bank
                                  Statements of parents/guardian(s).
                              </li>
                                <li>
                                  The final acceptance of the proposed candidates
                                  shall be on the basis of internal due diligence
                                  and checks done by the IDFC FIRST Bank Selection
                                  Panel and will be at all times subject to its
                                  management decision.
                              </li>
                                <li>
                                  The final disbursement of Scholarship amount to
                                  the applicant's college/university shall be on
                                  the basis of the fee receipt evidencing part
                                  payment, submitted by the applicant. Receipt of
                                  the balance amount paid towards fees out of the
                                  Scholarship disbursed, shall be produced to the
                                  IDFC FIRST Bank immediately on payment thus
                                  being made. Further, these fee receipts shall be
                                  retained with the beneficiaries and submitted
                                  along with other documents for availing the
                                  Scholarship for the consecutive academic year,
                                as the case may be.{" "}
                                </li>
                                <li>
                                  The candidates who are selected for the
                                  Scholarship will be intimated by the IDFC FIRST
                                  Bank via email.
                              </li>
                                <li>
                                  The IDFC FIRST Bank reserves the right, in its
                                  sole discretion, to the fullest extent permitted
                                  by law: (a) to reject or disqualify any
                                  application; or (b) subject to any written
                                  directions from a regulatory authority, to
                                  modify, suspend, terminate or cancel the
                                scholarship, as appropriate.{" "}
                                </li>
                                <li>
                                  The Scholarship Application is open to only
                                  those desirous candidates who have all the
                                  requisite certificates and mark-sheets at the
                                  time of making the Application. No provisional
                                  Application will be entertained on any
                                  undertaking to the effect or otherwise that
                                  certificates and/or required documents will be
                                  furnished at a later stage.
                              </li>
                                <li>
                                  IDFC FIRST Bank reserves the right, to use the
                                  photograph /profile of the scholarship
                                  applicant/beneficiary for future publication
                                  use; and/or disclosures; and/or for
                                  communication purposes with IDFC FIRST Bank 's
                                  stakeholders as this initiative is under the
                                  Corporate Social Responsibility of IDFC FIRST
                                  Bank; and/or for submissions before various
                                  authorities and interested parties from time to
                                  time for reporting progress thereof; and/or for
                                  use in Corporate brochures, websites, archives
                                  and other material publicity, if any in relation
                                  to CSR activities.
                              </li>
                                <li>
                                  As there shall be only 150 beneficiaries granted
                                  the Scholarship under this program, IDFC FIRST
                                  Bank reserves the right to stop the application
                                  process post achieving the final number of
                                  scholarships to be granted in a particular year,
                                  hence there will be no due date for closing
                                  applications.
                              </li>
                                <li>
                                  The students who receive the Scholarship should
                                  be regular in attendance and excel in
                                  performance for which the yardstick shall be
                                  decided by the competent authority of the
                                  school/college/University as also abide by the
                                  rules and regulations of such Institute.
                                  Intimation to the University of the availed
                                  Scholarship is mandatory and shall be
                                  accompanied by a request made by the Beneficiary
                                  to the University for reporting the progress of
                                  the beneficiary/student to IDFC FIRST Bank. The
                                  Beneficiary shall complete this documentation
                                  and furnish a copy to IDFC FIRST Bank before
                                  disbursement of the Scholarship.
                              </li>
                                <li>
                                  In case of selection for the award of
                                  scholarship under this program, the awarders
                                  should not be the recipients of any other
                                  scholarships, from any other sources -
                                  Government, Corporate Private Bodies, Trusts,
                                  etc.
                              </li>
                                <li>
                                  The Scholarship shall be cancelled/ discontinued
                                  forthwith and the amount of the scholarship paid
                                  shall be recovered, at the discretion of the
                                  concerned department of IDFC FIRST Bank if
                                <br />
                                  <li>
                                    (a) A student is found to have obtained a
                                    Scholarship by false statement/ certificates.
                                </li>
                                  <li>
                                    (b) A student violates any of the terms and
                                    conditions of the Scholarship.
                                </li>
                                  <li>
                                    (c) A student does not attend classes of the
                                    course or is found to be in violation of the
                                    Institutes' Rules and Regulations.
                                </li>
                                  <li>
                                    (d) Fails to obtain and maintain minimum
                                    performance/passing standard as required by
                                    the University Authorities and/or affiliates.
                                </li>
                                  <li>
                                    (e) Any misconduct on the part of the
                                    beneficiary, decision whereof will be at the
                                    sole discretion of the Selection Panel of IDFC
                                    FIRST Bank.
                                </li>
                                </li>
                                <li>
                                  This Scheme and Scholarship shall be evaluated
                                  at regular intervals by IDFC FIRST BANK and
                                  terms and conditions thereof may be modified,
                                  withdrawn or cancelled without prior notice to
                                  the Applicants at the discretion of the
                                  Selection Panel of IDFC FIRST BANK.
                              </li>
                                <li>
                                  IDFC FIRST BANK reserves its right to
                                  independently verify and assess the claims made
                                  by candidates/beneficiaries regarding their
                                  family financial status and other criteria.
                              </li>
                                <li>
                                  This is a beneficiary scheme as part of the
                                  Corporate Social Responsibility of IDFC FIRST
                                  BANK and is not as a matter of right to be
                                  claimed by candidates of economically weaker
                                  sections of society or otherwise. Candidates
                                  shall not have the right to raise any disputes
                                  regarding the Scholarship with IDFC FIRST BANK
                                  and shall have no right to claim any amounts or
                                  initiate any proceedings against IDFC FIRST BANK
                                  in respect thereof.
                              </li>
                              </ul>
                            )}
                        </article>
                      </article>
                    </article>
                  </section>
                </section>
              </section>
            </article>
          </article>
        </article>
      </article>
    );
  }
}

export default SubmitButton;
