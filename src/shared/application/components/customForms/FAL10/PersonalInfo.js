import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import DatePicker from "react-datepicker";
import moment from "moment";
import { ruleRunner } from "../../../../../validation/ruleRunner";
import ValidationError from "../../../../components/validationError";
import OtherTextField from "../../otherTextField";
import {
  APPLICATION_FETCH_PERSONAL_INFO_SUCCEEDED,
  APPLICATION_UPDATE_PERSONAL_INFO_SUCCEEDED,
  FETCH_PERSONAL_INFO_CONFIG_SUCCEEDED
} from "../../../actions/personalInfoActions";
import Loader from "../../../../common/components/loader";
import AlertMessage from "../../../../common/components/alertMsg";
import {
  applicationAPIKeys,
  userAddressFields,
  formNames,
  idValueElement,
  PIUserRules,
  hasChildCall,
  setValidationsInitialState,
  handleConditionalValidations,
  otherDistrictId,
  otherStateId,
  extractStepTemplate
} from "../../../formconfig";
import gblFunc from "../../../../../globals/globalFunctions";
import { config as formConfigConst } from "../../../formconfig";
import MaskedInput from 'react-text-mask'

if (typeof window !== "undefined") {
  require("react-datepicker/dist/react-datepicker.css");
}
const fieldOfCorrAddrss = {
  caAddressLine: "addressLine",
  caCity: "city",
  caState: "state",
  caDistrict: "district",
  caPincode: "pincode",
  caOtherState: "otherState",
  caOtherDistrict: "otherDistrict"
};

class PersonalInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPreferLangCheck: [],
      formData: {
        portalId: 1,
        customData: {
          relationship: { id: "" },
          govt_emp: "",
          employeeContact: "",
          employeeDesignation: "",
          employeeId: "",
          employeeName: "",
          preferredLanguageForInterview: "",
          employeeDetails: "",
          passport: "",
          workExperience: "",
          siblingsNo: "",
		  cityName: null,
          otherCityName: ""
						 
						   
        },
        correspondenceAddress: {
          country: 151,
          id: null,
          type: 2,
          state: { id: "" },
          district: { id: "" },
          addressLine: "",
          city: ""
        },
        firstName: "",
        lastName: "",
        dob: "",
        email: "",
        familyIncome: "",
        mobile: "",
        userAddress: {
          country: 151,
          id: null,
          type: 1,
          state: { id: "" },
          district: { id: "" },
          addressLine: "",
          city: ""
        },
        userRules: []
      },
      dropDownData: {
        gender: { id: "" },
        orphan: { id: "" },
        quota: { id: "" },
        religion: { id: "" }
      },
      showAlert: false,
      status: false,
      msg: "",
      isCompleted: false,
      validationsObj: {},
      isNext: false,
      dobValidation: "",
      dobValue: ""

    };
    this.formChangeHandler = this.formChangeHandler.bind(this);
    this.mapServerDataToModel = this.mapServerDataToModel.bind(this);
    this.findRuleValue = this.findRuleValue.bind(this);
    this.updateUserData = this.updateUserData.bind(this);
    this.getIdValue = this.getIdValue.bind(this);
    this.triggerChildCall = this.triggerChildCall.bind(this);
    this.close = this.close.bind(this);
    this.getFieldValue = this.getFieldValue.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.updateUserRulesArray = this.updateUserRulesArray.bind(this);
    this.onBlurChangeMethod = this.onBlurChangeMethod.bind(this);
  }

  async onBlurChangeMethod(value) {
    // let dob = e.target.value
    const value1 = value;
    var date = value1.split('/');
    var date2 = date.reverse();
    var date3 = date2.join('-');
    var updatedate1 = date3;
    if (/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/.test(value)) {
      await this.setState(prevState => {
        let formData = Object.assign({}, prevState.formData);  // creating copy of state variable jasper
        formData.dob = updatedate1;                     // update the name property, assign a new value                 
        return { formData };                                 // return new object jasper object
      })
    }
    else {
      await this.setState(prevState => {
        let formData = Object.assign({}, prevState.formData);  // creating copy of state variable jasper
        formData.dob = "";                     // update the name property, assign a new value                 
        return { formData };                                 // return new object jasper object
      })

    }
    let validationsObj = { ...this.state.validationsObj };

    let conditionBasedId = "dob" === "disabled" ? "physicallyChallenged" : "dob";
    conditionBasedId = gblFunc.replace_underScore(conditionBasedId);
    if (
      validationsObj[conditionBasedId + "Validation"] &&
      validationsObj[conditionBasedId] !== undefined
    ) {
      const validationResult = ruleRunner(
        this.state.formData.dob,
        conditionBasedId,
        "Field",
        ...validationsObj[conditionBasedId + "Validation"]
      );
      validationsObj[conditionBasedId] = validationResult[conditionBasedId];
    }
    await this.setState({
      validationsObj: validationsObj
    })

  }

  componentDidMount() {
    this.props.fetchPersonalInfoConfig({
      scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
      step: applicationAPIKeys.personalInfo
    });
    this.props.fetchApplicationPersonalInfo({
      userId: gblFunc.getStoreUserDetails()["userId"],
      scholarshipId: gblFunc.getStoreApplicationScholarshipId()
    });
  }
  componentWillReceiveProps(nextProps) {
    switch (nextProps.type) {
      case FETCH_PERSONAL_INFO_CONFIG_SUCCEEDED:
        if (Object.keys(this.state.validationsObj).length < 1) {
          this.setState({
            //create validation config and display object....
            validationsObj: setValidationsInitialState(
              nextProps.personalInfoConfig
            )
          });
        }
        break;
      case APPLICATION_FETCH_PERSONAL_INFO_SUCCEEDED:
        !this.state.formData.id
          ? this.mapServerDataToModel(nextProps.personalInfoData)
          : null;
        break;
      case APPLICATION_UPDATE_PERSONAL_INFO_SUCCEEDED:
        if (this.state.isCompleted) {
          this.setState({
            msg: "Submitted Successfully.",
            status: true,
            showAlert: true,
            isCompleted: false,
            isNext: true
          });

          this.props.getCompletedSteps({
            userId: gblFunc.getStoreUserDetails()["userId"],
            scholarshipId: gblFunc.getStoreApplicationScholarshipId()
          });
        }
        break;
      default:
        return;
    }
  }

  triggerChildCall(childCallCase, childValue) {
    switch (childCallCase) {
      case "state": //get district
        this.props.getDistrictsByState({
          KEY: childCallCase,
          DATA: childValue
        });
        break;
      case "caState": //get district
        this.props.getCaDistrictByState({
          KEY: childCallCase,
          DATA: childValue
        });
        break;
      default:
        break;
    }
  }
  close() {
    this.setState({
      showAlert: false,
      msg: "",
      status: false
    });
  }
  mapServerDataToModel(outData) {
    var formData = { ...this.state.formData };
    let isPreferChecked = [...this.state.isPreferLangCheck];
    const ruleValue = this.findRuleValue(
      ["gender", "disabled", "orphan", "quota", "religion"],
      outData.userRules
    );
    const updatedDropDownData = { ...this.state.dropDownData, ...ruleValue };
    const userAddressMerged = {
      userAddress: {
        ...this.state.formData.userAddress,
        ...outData.userAddress
      }
    };
    const userCorresAddressMerged = {
      userCorresAddress: {
        ...this.state.formData.userCorresAddress,
        ...outData.userCorresAddress
      }
    };
    userCorresAddressMerged["country"] = 151;
    userAddressMerged["country"] =151;
    const {
      firstName,
      lastName,
      familyIncome,
      email,
      mobile,
      dob,
      aadharCard,
      customData,
      userAddress,
      correspondenceAddress,
      userRules,
      portalId,
      id,
      alternateMobile
    } = {
      ...formData,
      ...outData,
      ...userAddressMerged,
      ...userCorresAddressMerged
    };
    const updatedData = {
      firstName,
      lastName,
      familyIncome,
      email,
      mobile,
      dob,
      aadharCard,
      customData,
      userAddress,
      correspondenceAddress,
      userRules,
      portalId,
      id,
      alternateMobile
    };
    if (
      userAddress &&
      userAddress.state &&
      userAddress.state.id &&
      (!Array.isArray(this.props.district) || this.props.district.length < 1)
    ) {
      this.triggerChildCall("state", userAddress.state.id);
    }
    if (
      correspondenceAddress &&
      correspondenceAddress.state &&
      correspondenceAddress.state.id &&
      (!Array.isArray(this.props.district) || this.props.district.length < 1)
    ) {
      this.triggerChildCall("caState", correspondenceAddress.state.id);
    }

    if (
      updatedData &&
      updatedData.customData &&
      updatedData.customData.preferredLanguageForInterview
    ) {
      let preferLang =
        updatedData["customData"]["preferredLanguageForInterview"];
      isPreferChecked = preferLang.split(",");
    }

    this.setState({
      isPreferLangCheck: isPreferChecked,
      formData: updatedData,
      dropDownData: updatedDropDownData
    });
  }

  getIdValue(customKey, ev) {
    switch (customKey) {
      case "dob":
        return {
          id: "dob",
          value: moment(ev).format("YYYY-MM-DD")
        };
      case "govt_emp":
        return {
          id: "govt_emp",
          value: ev.target.value
        };
      case "employeeDetails":
        return {
          id: "employeeDetails",
          value: ev.target.value
        };
      case "type":
        return {
          id: "type",
          value: ev.target.checked ? 1 : 2
        };
      case "passport":
        return {
          id: "passport",
          value: ev.target.value
        };
      case "workExperience":
        return {
          id: "workExperience",
          value: ev.target.value
        };
      case "siblingsNo":
        return {
          id: "siblingsNo",
          value: ev.target.value
        };
      default:
        return ev.target;
    }
  }

  updateUserRulesArray(value, id, userRules) {
    const ruleIndex = userRules.findIndex(
      elem => elem.ruleTypeId == PIUserRules.get(id)
    );
    let ruleObj = {};
    if (value == 700 || value == 903) {
      ruleObj = {
        ruleId: parseInt(value),
        ruleTypeId: PIUserRules.get(id),
        ruleValue: value == 700 ? "disabled" : "disabled-no",
        ruleTypeValue: "Disabled"
      };
    } else {
      ruleObj = {
        ruleId: parseInt(value),
        ruleTypeId: PIUserRules.get(id)
      };
    }

    ruleIndex > -1 ? (userRules[ruleIndex] = ruleObj) : userRules.push(ruleObj);
  }

  formChangeHandler(event, customKey = false, isCustom = false) {
    if (!event) return;
    let { id, value, nodeName } = this.getIdValue(customKey, event);
    const preferLang = [...this.state.isPreferLangCheck];
    let index =
      event.nativeEvent && event.nativeEvent.target
        ? event.nativeEvent.target.selectedIndex
        : "";
    var custom = customKey
      ? isCustom
      : event.target.attributes.getNamedItem("data-custom").value;
    const updateFormData = { ...this.state.formData };
    const customData = { ...updateFormData["customData"] };
    const userAddress = { ...updateFormData["userAddress"] };

    const userRules = [...updateFormData["userRules"]];
    const dropDownData = { ...this.state.dropDownData };
    custom.toString() == "true" && id != "otherPersonalDetails"
      ? nodeName
        ? nodeName.includes(idValueElement)
          ? (customData[id] = { id: value })
          : (customData[id] = value)
        : (customData[id] = value)
      : userAddressFields.indexOf(id) > -1
        ? (userAddress[id] = nodeName
          ? nodeName.includes(idValueElement)
            ? { id: value }
            : value
          : value)
        : (dropDownData[id] = nodeName
          ? nodeName.includes(idValueElement)
            ? { id: value }
            : (updateFormData[id] = value)
          : (updateFormData[id] = value));
    PIUserRules.has(id) && !userAddressFields[id]
      ? this.updateUserRulesArray(value, id, userRules)
      : false;
	      if (id === "cityName") {
      customData['otherCityName'] = "";
      customData[id] = value;
    } 
    if (id === "otherPersonalDetails") {
      dropDownData["orphan"] = {
        id: value
      };
      //id = "otherPersonalDetails";
    }

    if (id === "preferredLanguageForInterview") {
      if (preferLang.length > 0) {
        if (event.target.checked) {
          preferLang.push(value);
          customData[id] = preferLang.join(",");
        } else {
          preferLang.splice(preferLang.indexOf(value), 1);
          customData[id] = preferLang.join(",");
        }
      } else {
        preferLang.push(value);
        customData[id] = preferLang.join(",");
      }
    }

    if (fieldOfCorrAddrss[id]) {
      let getId = fieldOfCorrAddrss[id];
      if (!updateFormData["correspondenceAddress"]) {
        updateFormData["correspondenceAddress"] = {
          country: 28,
          id: null,
          type: 2,
          state: { id: "" },
          district: { id: "" },
          addressLine: "",
          city: ""
        };
      }
      switch (getId) {
        case "state":
          updateFormData["correspondenceAddress"][getId]["id"] = value;
          updateFormData["correspondenceAddress"][getId]["value"] =
            event.nativeEvent.target[index].text;

          if (!value) {
            updateFormData["correspondenceAddress"]["district"]["id"] = "";
            updateFormData["correspondenceAddress"]["district"][
              "districtName"
            ] = "";
          }
          break;
        case "district":
          updateFormData["correspondenceAddress"][getId]["id"] = value;
          updateFormData["correspondenceAddress"][getId]["districtName"] =
            event.nativeEvent.target[index].text;
          break;
        default:
          updateFormData["correspondenceAddress"][getId] = value;
          break;
      }
    }

    updateFormData["customData"] = customData;
    updateFormData["userAddress"] = userAddress;

    updateFormData["userRules"] = userRules;
    /******* Run validation on every field*************** */
    let validationsObj = { ...this.state.validationsObj };

    let conditionBasedId = id === "disabled" ? "physicallyChallenged" : id;
    conditionBasedId = gblFunc.replace_underScore(conditionBasedId);
    if (
      validationsObj[conditionBasedId + "Validation"] &&
      validationsObj[conditionBasedId] !== undefined
    ) {
      const validationResult = ruleRunner(
        value,
        conditionBasedId,
        "Field",
        ...validationsObj[conditionBasedId + "Validation"]
      );
      validationsObj[conditionBasedId] = validationResult[conditionBasedId];
    }
    /******* Run validation on every field*************** */

    this.setState(
      {
        formData: updateFormData,
        dropDownData: dropDownData,
        isPreferLangCheck: preferLang,
        validationsObj: validationsObj
      },
      () => {
        hasChildCall.has(id) ? this.triggerChildCall(id, value) : null;
      }
    );
  }

  findRuleValue(rulesArr = [], rulesValArr = []) {
    var ruleValObj = {};
    rulesArr.map(function (rule) {
      let ruleVlaueFiltered = rulesValArr.filter(function (ruleObj) {
        return rule == ruleObj.ruleTypeValue;
      });
      ruleValObj[rule] = {
        id: ruleVlaueFiltered.length ? ruleVlaueFiltered[0]["ruleId"] : ""
      };
    });
    return ruleValObj;
  }
  getFieldValue(field) {
    // if (fieldOfCorrAddrss[field]) {
    //   field = fieldOfCorrAddrss[field];
    // }
    let correctField =
      this.state.formData["userAddress"] &&
        this.state.formData["userAddress"][field]
        ? this.state.formData["userAddress"][field]
        : "" ||
          (this.state.formData["correspondenceAddress"] &&
            fieldOfCorrAddrss[field] &&
            this.state.formData["correspondenceAddress"][
            fieldOfCorrAddrss[field]
            ])
          ? this.state.formData["correspondenceAddress"][fieldOfCorrAddrss[field]]
          : "" ||
          this.state.formData[field] ||
          (field === "physicallyChallenged"
            ? this.state.dropDownData["disabled"]
            : this.state.dropDownData[field]) ||
          (field === "category"
            ? this.state.dropDownData["quota"]
            : this.state.dropDownData[field]) ||
          (field === "otherPersonalDetails"
            ? this.state.dropDownData["orphan"]
            : this.state.dropDownData[field]) ||
          this.state.formData["customData"][field];
    return typeof correctField !== "undefined"
      ? correctField.hasOwnProperty("id")
        ? correctField["id"]
        : correctField
      : "";
  }
  validateForm() {
    var validationsObj = { ...this.state.validationsObj };
    /************* start Update conditional validations.........****/

    if (
      !this.state.formData.customData.govt_emp ||
      (this.state.formData.customData.govt_emp &&
        this.state.formData.customData.govt_emp == "no")
    ) {
      validationsObj = handleConditionalValidations(
        !this.state.formData.customData.govt_emp
          ? "no"
          : this.state.formData.customData.govt_emp,
        "no",
        null,
        ["designation", "organization_name", "relationship"],
        validationsObj
      );
    }
    if (
      !this.state.formData.customData.employeeDetails ||
      (this.state.formData.customData.employeeDetails &&
        this.state.formData.customData.employeeDetails == "no")
    ) {
      validationsObj = handleConditionalValidations(
        !this.state.formData.customData.employeeDetails
          ? "no"
          : this.state.formData.customData.employeeDetails,
        "no",
        null,
        [
          "employeeDesignation",
          "employeeName",
          "employeeId",
          "employeeContact"
        ],
        validationsObj
      );
    }
    if (this.state.formData.userAddress.state) {
      validationsObj = handleConditionalValidations(
        this.state.formData.userAddress.state.id,
        otherStateId,
        "notEqual",
        ["otherState", "otherDistrict"],
        validationsObj
      );
    }
    if (
      this.state.formData.correspondenceAddress &&
      this.state.formData.correspondenceAddress.state
    ) {
      validationsObj = handleConditionalValidations(
        this.state.formData.correspondenceAddress.state.id
          ? this.state.formData.correspondenceAddress.state.id
          : 0,
        otherStateId,
        "notEqual",
        ["caOtherState", "caOtherDistrict"],
        validationsObj
      );
    }

    /************* start Update conditional validations.........****/

    let isFormValid = true;
    for (let field in validationsObj) {
      if (
        !field.includes("Validation") &&
        validationsObj[field] !== undefined &&
        validationsObj[field + "Validation"]
      ) {
        let isPhysicalChallenged =
          field === "disabled" ? "physicallyChallenged" : field;
        //don't check for valdation keys...
        const validationResult = ruleRunner(
          this.getFieldValue(field),
          isPhysicalChallenged,
          "Field",
          ...validationsObj[isPhysicalChallenged + "Validation"]
        );
        validationsObj[isPhysicalChallenged] =
          validationResult[isPhysicalChallenged];
        if (validationsObj[isPhysicalChallenged] !== null) {
          isFormValid = false;
        }
      }
    }
    this.setState({
      validationsObj,
      isFormValid
    });
    return isFormValid;
  }
  updateUserData() {
    if (this.validateForm()) {
      const updateCountry = { ...this.state.formData };

      if (this.state.formData && this.state.formData.dob) {
        //   var date=this.state.formData.dob.split('-');

        //  var date2= date.reverse();
        //  var date3=date2.join('-');

        updateCountry.dob = this.state.formData.dob;
      }
      const updatedCheckedPreferLang = [...this.state.isPreferLangCheck];

      updateCountry.customData.preferredLanguageForInterview = updatedCheckedPreferLang.join(
        ","
      );

      if (
        (updateCountry.correspondenceAddress &&
          Object.keys(updateCountry.correspondenceAddress).length == 0) ||
        !updateCountry.correspondenceAddress
      ) {
        updateCountry.correspondenceAddress = null;
      } else {
        updateCountry.correspondenceAddress["country"] =151;
      }
      if (
        (updateCountry.userAddress &&
          Object.keys(updateCountry.userAddress).length == 0) ||
        !updateCountry.userAddress
      ) {
        updateCountry.userAddress = null;
      } else {
        updateCountry.userAddress["country"] = 151;
      }

      if (
        updateCountry.customData &&
        updateCountry.customData.employeeDetails &&
        updateCountry.customData.employeeDetails == "no"
      ) {
        updateCountry.customData["employeeContact"] = "";
        updateCountry.customData["employeeDesignation"] = "";
        updateCountry.customData["employeeId"] = "";
        updateCountry.customData["employeeName"] = "";
      }

      this.props.updatePersonalInfo({
        formData: updateCountry,
        userId: gblFunc.getStoreUserDetails()["userId"],
        step: formNames["personalInfo"],
        scholarshipId: gblFunc.getStoreApplicationScholarshipId()
      });
      this.setState({ isCompleted: true });
    }
  }

  markup(val) {
    return { __html: val };
  }

  render() {
    const { scholarshipSteps, personalInfoData } = this.props;
    let personalInfoTemplate = "";
    if (scholarshipSteps && scholarshipSteps.length) {
      personalInfoTemplate = extractStepTemplate(
        scholarshipSteps,
        "PERSONAL_INFO"
      );
    }
    var updatedate1 = ""

    if (!!this.state.formData && this.state.formData.dob) {
      var date = this.state.formData.dob.split('-');

      var date2 = date.reverse();
      var date3 = date2.join('/');

      updatedate1 = date3;
    }
    const formConfig = this.props.personalInfoConfig || null;
    if (formConfig) {
      let physicallyChallenged = ""; // get physical chalange value
      let otherPersonalDetails = "";
      if (
        this.state.formData.userRules != null &&
        this.state.formData.userRules.length > 0
      ) {
        physicallyChallenged = this.state.formData.userRules.filter(res => {
          return res.ruleId == 700 || res.ruleId == 903;
        });
      }

      // const formConfig = personalInfoConfig;
      const genderOptions = formConfig.gender.dataOptions
        ? formConfig.gender.dataOptions
        : this.props.allRules.rulesData
          ? this.props.allRules.rulesData.gender
          : []; //from rules call or from application config
      const preferredLanguageOption =
        formConfig.preferredLanguageForInterview &&
          formConfig.preferredLanguageForInterview.dataOptions
          ? formConfig.preferredLanguageForInterview.dataOptions
          : [];
      //   ? formConfig.preferredLanguage.dataOptions
      //   : this.props.allRules.rulesData
      //     ? this.props.allRules.rulesData.preferredLanguage
      //     : []; //from rules call or from application config
      const districtOptions = this.props.district ? this.props.district : []; //from district call result
      const caDistrictOptions = this.props.otherdistrict
        ? this.props.otherdistrict
        : []; //from district call result
      const stateOptions = formConfig.state.dataOptions
        ? formConfig.state.dataOptions
        : this.props.allRules.rulesData
          ? this.props.allRules.rulesData.state
          : []; //from rules call or from application config
      const relationOptions =
        formConfig.relationship && formConfig.relationship.dataOptions
          ? formConfig.relationship.dataOptions
          : []; //from rules call or from application config
      const otherPersonalDts =
        formConfig.otherPersonalDetails &&
          formConfig.otherPersonalDetails.dataOptions
          ? formConfig.otherPersonalDetails.dataOptions
          : [];
      const categoryOptions =
        formConfig.category && formConfig.category.dataOptions
          ? formConfig.category.dataOptions
          : this.props.allRules.rulesData
            ? this.props.allRules.rulesData.quota
            : [];
      const religionOptions =
        formConfig.religion && formConfig.religion.dataOptions
          ? formConfig.religion.dataOptions
          : this.props.allRules.rulesData
            ? this.props.allRules.rulesData.religion
            : [];
      const workExperienceOptions =
        formConfig.workExperience && formConfig.workExperience.dataOptions
          ? formConfig.workExperience.dataOptions
          : [];
      const siblingsNoObj =
        formConfig.siblingsNo && formConfig.siblingsNo.dataOptions
          ? formConfig.siblingsNo.dataOptions
          : [];
		        const cityNameOption = formConfig.cityName
        ? formConfig.cityName.dataOptions : []; //from rules call or from application config																							
      return (
        <section className="sectionwhite">
          {this.state.isNext &&
            this.props.redirectionSteps &&
            this.props.redirectionSteps.length ? (
              <Redirect
                to={`/application/${this.props.match.params.bsid.toUpperCase()}/form/${
                  this.props.redirectionSteps[
                  this.props.redirectionSteps.indexOf("personalInfo") + 1
                  ]
                  }`}
              />
            ) : (
              ""
            )}
          <Loader isLoader={this.props.showLoader} />
          <AlertMessage
            close={this.close.bind(this)}
            isShow={this.state.showAlert}
            status={this.state.status}
            msg={this.state.msg}
          />
          <article className="form">
            {/* {formConfigConst.customInstruction.has(
              this.props.match.params.bsid
            ) ? (
              <span className="error">
                {formConfigConst.customInstruction.get(
                  this.props.match.params.bsid
                )}
              </span>
            ) : (
              ""
            )} */}
            {
												   
              personalInfoTemplate && personalInfoTemplate.length &&
                personalInfoTemplate[0] &&
                personalInfoTemplate[0].message
                ?
                <article
                  dangerouslySetInnerHTML={this.markup(personalInfoTemplate[0].message)}
					  
				
                  className="col-md-12 subheadingerror"
                />
                : ""
            }
            <article className="paddingborder">&nbsp;</article>

            <article className="row">
              <article
                className={`${
                  formConfig.firstName && formConfig.firstName.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    id="firstName"
                    data-custom={
                      formConfig.firstName && formConfig.firstName.custom
                        ? formConfig.firstName.custom
                        : ""
                    }
                    disabled={personalInfoData && personalInfoData.firstName ? true : false}
                    value={this.state.formData.firstName}
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label htmlFor="firstName">{`${formConfig.firstName.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["firstName"]}
                  />
                </article>
              </article>
              {/* <article
              className={`${
                formConfig.employeeId
                  ? formConfig.employeeId.active
                    ? "show"
                    : "hide"
                  : "hide"
              } col-md-4`}
            >
              <input
                type="text"
                placeholder={`${formConfig.employeeId.label}`}
                id="employeeId"
                data-custom={formConfig.employeeId.custom}
                value={this.state.formData.customData.employeeId}
                onChange={this.formChangeHandler}
              />
              <ValidationError
                fieldValidation={this.state.validationsObj["employeeId"]}
              />
            </article> */}
              <article
                className={`${
                  formConfig.lastName && formConfig.lastName.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    id="lastName"
                    data-custom={
                      formConfig.lastName && formConfig.lastName.custom
                        ? formConfig.lastName.custom
                        : ""
                    }
                    disabled={personalInfoData && personalInfoData.lastName ? true : false}
                    value={this.state.formData.lastName}
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.lastName.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["lastName"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.email &&
                    formConfig.email.active &&
                    this.props.BSID != "SKKS1"
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    id="email"
                    data-custom={
                      formConfig.email && formConfig.email.custom
                        ? formConfig.email.custom
                        : ""
                    }
                    value={this.state.formData.email}
                    onChange={this.formChangeHandler}
                    disabled
                  />
                  <label>{`${formConfig.email.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["email"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.mobile && formConfig.mobile.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={
                      formConfig.mobile && formConfig.mobile.custom
                        ? formConfig.mobile.custom
                        : ""
                    }
                    id="mobile"
                    maxLength="10"
                    value={this.state.formData.mobile}
                    onChange={this.formChangeHandler}
                    disabled={personalInfoData && personalInfoData.mobile ? true : false}
                    required
                  />
                  <label>{`${formConfig.mobile.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["mobile"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.alternateMobile &&
                    formConfig.alternateMobile.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={
                      formConfig.alternateMobile &&
                        formConfig.alternateMobile.custom
                        ? formConfig.alternateMobile.custom
                        : ""
                    }
                    id="alternateMobile"
                    maxLength="10"
                    value={this.state.formData.alternateMobile}
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.alternateMobile &&
                    formConfig.alternateMobile.label}`}</label>
                  <ValidationError
                    fieldValidation={
                      this.state.validationsObj["alternateMobile"]
                    }
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.landlineNo && formConfig.landlineNo.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={
                      formConfig.landlineNo && formConfig.landlineNo.custom
                        ? formConfig.landlineNo.custom
                        : ""
                    }
                    id="landlineNo"
                    maxLength="15"
                    value={
                      this.state.formData.customData &&
                        this.state.formData.customData.landlineNo
                        ? this.state.formData.customData.landlineNo
                        : ""
                    }
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.landlineNo &&
                    formConfig.landlineNo.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["landlineNo"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.dob && formConfig.dob.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  {/* <DatePicker
                    showYearDropdown
                    scrollableYearDropdown
                    yearDropdownItemNumber={40}
                    customInput={
                  //     <MaskedInput
                  //       // pipe={autoCorrectedDatePipe}
                  //       mask={[/\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
                  //       keepCharPositions={true}
                  //       guide={true}
                  //     />

                  //   }

                  //   minDate={moment().subtract(100, "years")}
                  //   maxDate={moment().add(1, "years")}
                  //   name="dob"
                  //   id="dob"
                  //   // placeholder={`${formConfig.dob.label}`}
                  //   data-custom={
                  //     formConfig.dob && formConfig.dob.custom
                  //       ? formConfig.dob.custom
                  //       : ""
                  //   }
                  //   className="icon-date"
                  //   autoComplete="off"
                  //   selected={
                  //     this.state.formData.dob
                  //       ? moment(
                  //         moment(this.state.formData.dob).format(
                  //           "DD-MM-YYYY"
                  //         ),
                  //         "DD-MM-YYYY"
                  //       )
                  //       : null
                  //   }
                  //   onChange={event =>
                  //     this.formChangeHandler(
                  //       event,
                  //       "dob",
                  //       formConfig.dob && formConfig.dob.custom
                  //     )
                  //   }
                  //   dateFormat="DD-MM-YYYY"
                  // /> */}
                  <MaskedInput
                    type="text"
                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}

                    keepCharPositions={true}
                    guide={true}
                    data-custom={
                      formConfig.dob &&
                        formConfig.dob.custom
                        ? formConfig.dob.custom
                        : ""
                    }
										 
									  
							  
											 
								 
																 
										
							
									  
						 
							  
					 

                    id="dob"
                    key={`${Math.floor((Math.random() * 1000))}-min`}

                    // minLength="8"
                    //  maxLength="10"
                    // placeholder={"dd/mm/yyyy"}
                    // value={this.state.formData.dob}
                    defaultValue={updatedate1}
                    onBlur={(e) => this.onBlurChangeMethod(e.target.value)}

                    // onChange={this.formChangeHandler}
										   
                    required />
                  <label className="labelstyle">{`${formConfig.dob &&
                    formConfig.dob.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["dob"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.age && formConfig.age.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={
                      formConfig.age && formConfig.age.custom
                        ? formConfig.age.custom
                        : ""
                    }
                    id="age"
                    value={
                      this.state.formData.customData &&
                        this.state.formData.customData.age
                        ? this.state.formData.customData.age
                        : ""
                    }
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.age && formConfig.age.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["age"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.birthPlace && formConfig.birthPlace.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={
                      formConfig.birthPlace && formConfig.birthPlace.custom
                        ? formConfig.birthPlace.custom
                        : ""
                    }
                    id="birthPlace"
                    value={
                      this.state.formData.customData &&
                        this.state.formData.customData.birthPlace
                        ? this.state.formData.customData.birthPlace
                        : ""
                    }
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.birthPlace &&
                    formConfig.birthPlace.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["birthPlace"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.gender && formConfig.gender.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <select
                    className="icon"
                    id="gender"
                    disabled
                    // placeholder={`${formConfig.gender.label}`}
                    value={this.state.dropDownData.gender.id}
                    data-custom={
                      formConfig.gender && formConfig.gender.custom
                        ? formConfig.gender.custom
                        : ""
                    }
                    onChange={this.formChangeHandler}
                  >
                    <option value="">-Select-</option>
                    {genderOptions.map(genderItem => {
                      return (
                        <option value={`${genderItem.id}`}>
                          {genderItem.rulevalue}
                        </option>
                      );
                    })}
                  </select>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["gender"]}
                  />
                  <label className="labelstyle">{`${formConfig.gender.label}`}</label>
                </article>
              </article>
              <article
                className={`${
                  formConfig.category && formConfig.category.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <select
                    className="icon"
                    id="quota"
                    // placeholder={`${formConfig.gender.label}`}
                    value={this.state.dropDownData.quota.id}
                    data-custom={
                      formConfig.category && formConfig.category.custom
                        ? formConfig.category.custom
                        : ""
                    }
                    onChange={this.formChangeHandler}
                  >
                    <option value="">-Select-</option>
                    {categoryOptions.map(category => {
                      return (
                        <option value={`${category.id}`}>
                          {category.rulevalue}
                        </option>
                      );
                    })}
                  </select>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["category"]}
                  />
                  <label className="labelstyle">{`${formConfig.category.label}`}</label>
                </article>
              </article>
              <article
                className={`${
                  formConfig.aadharCard && formConfig.aadharCard.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    // type="text"
                    maxLength="12"
                    // pattern="/(^1$)|(^[7-9]\d{0,9}$)/"
                    type="tel"
                    data-custom={
                      formConfig.aadharCard && formConfig.aadharCard.custom
                        ? formConfig.aadharCard.custom
                        : ""
                    }
                    value={this.state.formData.aadharCard}
                    id="aadharCard"
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.aadharCard.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["aadharCard"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.familyIncome && formConfig.familyIncome.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    maxLength="10"
                    data-custom={
                      formConfig.familyIncome && formConfig.familyIncome.custom
                        ? formConfig.familyIncome.custom
                        : ""
                    }
                    value={this.state.formData.familyIncome}
                    id="familyIncome"
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.familyIncome.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["familyIncome"]}
                  />
                </article>
              </article>
              {formConfig.physicallyChallenged != null ? (
                <article
                  className={`${
                    formConfig.physicallyChallenged &&
                      formConfig.physicallyChallenged.active
                      ? "show"
                      : "hide"
                    } col-md-4`}
                >
                  <article className="form-group">
                    <select
                      className="icon"
                      id="disabled"
                      // placeholder={`${formConfig.gender.label}`}
                      /*  value={this.state.dropDownData.physicallyChallenged.id} */
                      data-custom={
                        formConfig.physicallyChallenged &&
                          formConfig.physicallyChallenged.custom
                          ? formConfig.physicallyChallenged.custom
                          : ""
                      }
                      onChange={this.formChangeHandler}
                      value={
                        physicallyChallenged[0]
                          ? physicallyChallenged[0].ruleId
                          : "" || ""
                      }
                    >
                      <option value="">--Select--</option>

                      <option value="700">Yes</option>
                      <option value="903">No</option>
                    </select>
                    {
                      <ValidationError
                        fieldValidation={
                          this.state.validationsObj["physicallyChallenged"]
                        }
                      />
                    }
                    <label className="labelstyle">{`${formConfig.physicallyChallenged.label}`}</label>
                  </article>
                </article>
              ) : (
                  ""
                )}

              <article
                className={`${
                  formConfig.workExperience && formConfig.workExperience.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <select
                    className="icon"
                    id="workExperience"
                    value={
                      this.state.formData.customData &&
                        this.state.formData.customData.workExperience
                        ? this.state.formData.customData.workExperience
                        : ""
                    }
                    data-custom={
                      formConfig.workExperience &&
                        formConfig.workExperience.custom
                        ? formConfig.workExperience.custom
                        : ""
                    }
                    onChange={event =>
                      this.formChangeHandler(
                        event,
                        "workExperience",
                        formConfig.workExperience.custom
                      )
                    }
                  >
                    <option value="">-Select-</option>
                    {workExperienceOptions.map(workItem => {
                      return (
                        <option value={`${workItem.id}`} key={workItem.id}>
                          {workItem.rulevalue}
                        </option>
                      );
                    })}
                  </select>
                  <label className="labelstyle">{`${
                    formConfig.workExperience && formConfig.workExperience.label
                      ? formConfig.workExperience.label
                      : ""
                    }`}</label>
                  <ValidationError
                    fieldValidation={
                      this.state.validationsObj["workExperience"]
                    }
                  />
                </article>
              </article>

              {formConfig.otherPersonalDetails != null ? (
                <article
                  className={`${
                    formConfig.otherPersonalDetails &&
                      formConfig.otherPersonalDetails.active
                      ? "show"
                      : "hide"
                    } col-md-4`}
                >
                  <article className="form-group">
                    <select
                      className="icon"
                      id="otherPersonalDetails"
                      // placeholder={`${formConfig.gender.label}`}
                      value={this.state.dropDownData.orphan.id}
                      data-custom={
                        formConfig.otherPersonalDetails &&
                          formConfig.otherPersonalDetails.custom
                          ? formConfig.otherPersonalDetails.custom
                          : ""
                      }
                      onChange={event =>
                        this.formChangeHandler(
                          event,
                          true,
                          formConfig.otherPersonalDetails.custom
                        )
                      }
                    //   value={
                    //     otherPersonalDetails[0]
                    //       ? otherPersonalDetails[0].ruleId
                    //       : "" || ""
                    //   }
                    >
                      <option value="">--Select--</option>

                      {otherPersonalDts.map(otherPerDT => {
                        return (
                          <option value={`${otherPerDT.id}`}>
                            {otherPerDT.rulevalue}
                          </option>
                        );
                      })}
                    </select>
                    {
                      <ValidationError
                        fieldValidation={
                          this.state.validationsObj["otherPersonalDetails"]
                        }
                      />
                    }
                    <label className="labelstyle">{`${formConfig.otherPersonalDetails.label}`}</label>
                  </article>
                </article>
              ) : (
                  ""
                )}
              {formConfig.siblingsNo != null ? (
                <article
                  className={`${
                    formConfig.siblingsNo && formConfig.siblingsNo.active
                      ? "show"
                      : "hide"
                    } col-md-4`}
                >
                  <article className="form-group">
                    <select
                      className="icon"
                      id="siblingsNo"
                      // placeholder={`${formConfig.gender.label}`}
                      value={
                        this.state.formData.customData &&
                          this.state.formData.customData.siblingsNo
                          ? this.state.formData.customData.siblingsNo
                          : ""
                      }
                      data-custom={
                        formConfig.siblingsNo && formConfig.siblingsNo.custom
                          ? formConfig.siblingsNo.custom
                          : ""
                      }
                      onChange={event =>
                        this.formChangeHandler(
                          event,
                          "siblingsNo",
                          formConfig.siblingsNo.custom
                        )
                      }
                    >
                      <option value="">--Select--</option>

                      {siblingsNoObj.map(otherPerDT => {
                        return (
                          <option value={`${otherPerDT.id}`}>
                            {otherPerDT.rulevalue}
                          </option>
                        );
                      })}
                    </select>
                    {
                      <ValidationError
                        fieldValidation={
                          this.state.validationsObj["siblingsNo"]
                        }
                      />
                    }
                    <label className="labelstyle">{`${formConfig.siblingsNo.label}`}</label>
                  </article>
                </article>
              ) : (
                  ""
                )}

              <article
                className={`${
                  formConfig.religion && formConfig.religion.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <select
                    className="icon"
                    id="religion"
                    placeholder={`${
                      formConfig.religion ? formConfig.religion.label : ""
                      }`}
                    value={this.state.dropDownData.religion.id}
                    data-custom={
                      formConfig.religion && formConfig.religion.custom
                    }
                    onChange={this.formChangeHandler}
                  >
                    <option value="">-Select-</option>
                    {religionOptions.map(religion => {
                      return (
                        <option value={`${religion.id}`}>
                          {religion.rulevalue}
                        </option>
                      );
                    })}
                  </select>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["religion"]}
                  />
                  <label className="labelstyle">{`${formConfig.religion &&
                    formConfig.religion.label}`}</label>
                </article>
              </article>

              {formConfig.preferredLanguageForInterview != null ? (
                <article
                  className={`${
                    formConfig.preferredLanguageForInterview &&
                      formConfig.preferredLanguageForInterview.active
                      ? "show"
                      : "hide"
                    } col-md-4`}
                >
                  <article className="form-group preferredLanguageForInterview">
                    <label>{`${formConfig.preferredLanguageForInterview.label}`}</label>
                    {preferredLanguageOption.map(list => {
                      return (
                        <article className="radionewgp">
                          <label>{list.rulevalue}</label>
                          <input
                            type="checkbox"
                            data-custom={
                              formConfig.preferredLanguageForInterview &&
                                formConfig.preferredLanguageForInterview.custom
                                ? formConfig.preferredLanguageForInterview
                                  .custom
                                : ""
                            }
                            id="preferredLanguageForInterview"
                            value={list.id}
                            checked={
                              this.state.isPreferLangCheck.indexOf(list.id) > -1
                            }
                            onChange={event =>
                              this.formChangeHandler(
                                event,
                                true,
                                formConfig.preferredLanguageForInterview.custom
                              )
                            }
                          />
                        </article>
                      );
                    })}

                    <ValidationError
                      fieldValidation={
                        this.state.validationsObj[
                        "preferredLanguageForInterview"
                        ]
                      }
                    />
                  </article>
                </article>
              ) : (
                  ""
                )}
              {/* <article
              className={`${
                formConfig.preferredLanguage.active ? "show" : "hide"
              } col-md-4`}
            >
              <select
                className="icon"
                id="preferredLanguage"
                value={this.state.dropDownData.preferredLanguage.id}
                data-custom={formConfig.preferredLanguage.custom}
                onChange={this.formChangeHandler}
              >
                {prefLanguaguesOptions.map(languageItem => {
                  return (
                    <option value={`${languageItem.id}`}>
                      {languageItem.value}
                    </option>
                  );
                })}
              </select>
            </article> */}
              <article className="row">
                <article
                  className={`${
                    formConfig.passport && formConfig.passport.active
                      ? "show"
                      : "hide"
                    } col-md-12`}
                >
                  <article className="form-group">
                    <article
                      className="radio-btn"
                      data-custom={
                        formConfig.passport && formConfig.passport.custom
                          ? formConfig.passport.custom
                          : ""
                      }
                      id="passport"
                      onChange={event =>
                        this.formChangeHandler(
                          event,
                          "passport",
                          formConfig.passport.custom
                        )
                      }
                    >
                      <h3>
                        {formConfig.passport && formConfig.passport.label
                          ? formConfig.passport.label
                          : ""}
                      </h3>
                      <span>
                        <label>
                          <input
                            type="radio"
                            name="radio"
                            checked={
                              this.state.formData.customData &&
                                this.state.formData.customData.passport
                                ? this.state.formData.customData.passport ==
                                "Yes"
                                : ""
                            }
                            value="Yes"
                          />
                          <span />
                          &nbsp;<i>Yes</i>
                        </label>
                      </span>
                      <span className="paddingborder">&nbsp;</span>
                      <span className="marginleft">
                        <label>
                          <input
                            type="radio"
                            value="No"
                            checked={
                              this.state.formData.customData &&
                                this.state.formData.customData.passport
                                ? this.state.formData.customData.passport ==
                                "No"
                                : ""
                            }
                            name="radio"
                          />
                          <span />
                          &nbsp;<i>No</i>
                        </label>
                      </span>
                    </article>
                    <ValidationError
                      fieldValidation={this.state.validationsObj["passport"]}
                    />
                  </article>
                </article>
              </article>
            </article>

            {formConfig.addressLine && formConfig.addressLine.active && (
              <article className="row">
                <article className="col-md-12 address">
                  Permanent address
                  {/* <article className="paddingborder">
                    <article className="border">&nbsp;</article>
                  </article> */}
                  <span
                    className={`${
                      formConfig.type && formConfig.type.active
                        ? "show"
                        : "hide"
                      }`}
                  >
                    <label>
                      <input
                        type="checkbox"
                        data-custom={
                          formConfig.type && formConfig.type.custom
                            ? formConfig.type.custom
                            : ""
                        }
                        value={this.state.formData.userAddress.type}
                        checked={this.state.formData.userAddress.type}
                        id="type"
                        onChange={event =>
                          this.formChangeHandler(
                            event,
                            "type",
                            formConfig.type.custom
                          )
                        }
                      />
                      <span />
                      {`${formConfig.type.label}`}
                    </label>
                  </span>
                </article>
              </article>
            )}

            <article className="row">
              <article
                className={`${
                  formConfig.addressLine && formConfig.addressLine.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={
                      formConfig.addressLine && formConfig.addressLine.custom
                        ? formConfig.addressLine.custom
                        : ""
                    }
                    value={this.state.formData.userAddress.addressLine}
                    id="addressLine"
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.addressLine.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["addressLine"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.state && formConfig.state.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <select
                    className="icon"
                    id="state"
                    value={
                      this.state.formData.userAddress.state
                        ? this.state.formData.userAddress.state.id
                        : ""
                    }
                    data-custom={
                      formConfig.state && formConfig.state.custom
                        ? formConfig.state.custom
                        : ""
                    }
                    onChange={this.formChangeHandler}
                  >
                    <option value="">-Select-</option>
                    {stateOptions.map(stateItem => {
                      return (
                        <option value={`${stateItem.id}`}>
                          {stateItem.rulevalue}
                        </option>
                      );
                    })}
                  </select>
                  <label className="labelstyle">{formConfig.state.label}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["state"]}
                  />
                </article>
              </article>
              {this.state.formData.userAddress.state &&
                this.state.formData.userAddress.state.id == otherStateId ? (
                  <OtherTextField
                    custom={
                      formConfig.otherState && formConfig.otherState.custom
                        ? formConfig.otherState.custom
                        : ""
                    }
                    fieldValue={this.state.formData.userAddress.otherState}
                    fieldId="otherState"
                    clickHandler={this.formChangeHandler}
                    fieldLabel={
                      formConfig.otherState && formConfig.otherState.label
                    }
                    fieldValidation={this.state.validationsObj["otherState"]}
                  />
                ) : null}

              <article
                className={`${
                  formConfig.district && formConfig.district.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <select
                    className="icon"
                    id="district"
                    value={
                      this.state.formData.userAddress.district
                        ? this.state.formData.userAddress.district.id
                        : ""
                    }
                    data-custom={
                      formConfig.district && formConfig.district.custom
                        ? formConfig.district.custom
                        : ""
                    }
                    onChange={this.formChangeHandler}
                  >
                    <option value="">-Select-</option>
                    {districtOptions.map(districtItem => {
                      return (
                        <option value={`${districtItem.id}`}>
                          {districtItem.districtName}
                        </option>
                      );
                    })}
                  </select>
                  <label className="labelstyle">
                    {formConfig.district.label}
                  </label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["district"]}
                  />
                </article>
              </article>
              {this.state.formData.userAddress.district &&
                this.state.formData.userAddress.district.id == otherDistrictId ? (
                  <OtherTextField
                    custom={
                      formConfig.otherDistrict && formConfig.otherDistrict.custom
                        ? formConfig.otherDistrict.custom
                        : ""
                    }
                    fieldValue={this.state.formData.userAddress.otherDistrict}
                    fieldId="otherDistrict"
                    clickHandler={this.formChangeHandler}
                    fieldLabel={
                      formConfig.otherDistrict && formConfig.otherDistrict.label
                    }
                    fieldValidation={this.state.validationsObj["otherDistrict"]}
                  />
                ) : null}

              <article
                className={`${
                  formConfig.city && formConfig.city.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={
                      formConfig.city && formConfig.city.custom
                        ? formConfig.city.custom
                        : ""
                    }
                    value={this.state.formData.userAddress.city}
                    id="city"
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.city.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["city"]}
                  />
                </article>
              </article>
					             {/* City name dropdown set */}

              <article
                className={`${
                  formConfig.cityName && formConfig.cityName.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <select
                    className="icon"
                    id="cityName"
                    value={this.state.formData.customData.cityName ?
                      this.state.formData.customData.cityName : ""
                    }
                    data-custom={
                      formConfig.cityName && formConfig.cityName.dataOptions
                        ? formConfig.cityName.dataOptions
                        : ""
                    }
                    onChange={this.formChangeHandler}
                  >
                    <option value="">-Select-</option>
                    {cityNameOption.map(item => {
                      return (
                        <option value={`${item.id}`}>
                          {item.rulevalue}
                        </option>
                      );
                    })}
                  </select>
                  <label className="labelstyle">
                    {formConfig.cityName && formConfig.cityName.label
                      ? formConfig.cityName.label
                      : ""}
                  </label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["cityName"]}
                  />
                </article>
              </article>
              {
                this.state.formData.customData.cityName == "Others" ? <OtherTextField
                  custom={
                    formConfig.otherCityName && formConfig.otherCityName.custom
                      ? formConfig.otherCityName.custom
                      : ""
                  }
                  fieldValue={this.state.formData.customData.otherCityName}
                  fieldId="otherCityName"
                  clickHandler={this.formChangeHandler}
                  fieldLabel={
                    formConfig.otherCityName && formConfig.otherCityName.label
                  }
                  disabled={this.state.formData.customData.cityName == "Others" ? false : true}
                  fieldValidation={this.state.validationsObj["otherCityName"]}
                /> : null
              }
              { /* End City dropdown set */}															
              <article
                className={`${
                  formConfig.pincode && formConfig.pincode.active
                    ? "show"
                    : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    // type="text"
                    maxLength="6"
                    // pattern="/(^1$)|(^[7-9]\d{0,9}$)/"
                    type="tel"
                    data-custom={
                      formConfig.pincode && formConfig.pincode.custom
                        ? formConfig.pincode.custom
                        : ""
                    }
                    value={this.state.formData.userAddress.pincode}
                    id="pincode"
                    // maxLength="6"
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.pincode.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["pincode"]}
                  />
                </article>
              </article>
            </article>
            <article className="row">
              <article
                className={`${
                  formConfig.caAddressLine && formConfig.caAddressLine.active
                    ? "show"
                    : "hide"
                  } col-md-12 address`}
              >
                Correspondence address
                <article className="paddingborder">
                  <article className="border">&nbsp;</article>
                </article>
                <article
                  className={`${
                    formConfig.caAddressLine && formConfig.caAddressLine.active
                      ? "show"
                      : "hide"
                    } col-md-4`}
                >
                  <article className="form-group">
                    <input
                      type="text"
                      data-custom={
                        formConfig.caAddressLine &&
                          formConfig.caAddressLine.custom
                          ? formConfig.caAddressLine.custom
                          : ""
                      }
                      value={
                        this.state.formData.correspondenceAddress &&
                          this.state.formData.correspondenceAddress.addressLine
                          ? this.state.formData.correspondenceAddress
                            .addressLine
                          : ""
                      }
                      id="caAddressLine"
                      onChange={this.formChangeHandler}
                      required
                    />
                    <label>{`${
                      formConfig.caAddressLine && formConfig.caAddressLine.label
                        ? formConfig.caAddressLine.label
                        : ""
                      }`}</label>
                    <ValidationError
                      fieldValidation={
                        this.state.validationsObj["caAddressLine"]
                      }
                    />
                  </article>
                </article>
                <article
                  className={`${
                    formConfig.caState && formConfig.caState.active
                      ? "show"
                      : "hide"
                    } col-md-4`}
                >
                  <article className="form-group">
                    <select
                      className="icon"
                      id="caState"
                      value={
                        this.state.formData.correspondenceAddress &&
                          this.state.formData.correspondenceAddress.state
                          ? this.state.formData.correspondenceAddress.state.id
                          : ""
                      }
                      data-custom={
                        formConfig.caState && formConfig.caState.custom
                          ? formConfig.caState.custom
                          : ""
                      }
                      onChange={this.formChangeHandler}
                    >
                      <option value="">-Select-</option>
                      {stateOptions.map(stateItem => {
                        return (
                          <option value={`${stateItem.id}`}>
                            {stateItem.rulevalue}
                          </option>
                        );
                      })}
                    </select>
                    <label className="labelstyle">
                      {formConfig.caState && formConfig.caState.label
                        ? formConfig.caState.label
                        : ""}
                    </label>
                    <ValidationError
                      fieldValidation={this.state.validationsObj["caState"]}
                    />
                  </article>
                </article>
                {this.state.formData.correspondenceAddress &&
                  this.state.formData.correspondenceAddress.state &&
                  this.state.formData.correspondenceAddress.state.id ==
                  otherStateId ? (
                    <OtherTextField
                      custom={
                        formConfig.caOtherState && formConfig.caOtherState.custom
                          ? formConfig.caOtherState.custom
                          : ""
                      }
                      fieldValue={
                        this.state.formData.correspondenceAddress &&
                          this.state.formData.correspondenceAddress.otherState
                          ? this.state.formData.correspondenceAddress.otherState
                          : ""
                      }
                      fieldId="caOtherState"
                      clickHandler={this.formChangeHandler}
                      fieldLabel={
                        formConfig.caOtherState && formConfig.caOtherState.label
                          ? formConfig.caOtherState.label
                          : ""
                      }
                      fieldValidation={this.state.validationsObj["caOtherState"]}
                    />
                  ) : null}
                <article
                  className={`${
                    formConfig.caDistrict && formConfig.caDistrict.active
                      ? "show"
                      : "hide"
                    } col-md-4`}
                >
                  <article className="form-group">
                    <select
                      className="icon"
                      id="caDistrict"
                      value={
                        this.state.formData.correspondenceAddress &&
                          this.state.formData.correspondenceAddress.district
                          ? this.state.formData.correspondenceAddress.district
                            .id
                          : ""
                      }
                      data-custom={
                        formConfig.caDistrict && formConfig.caDistrict.custom
                          ? formConfig.caDistrict.custom
                          : ""
                      }
                      onChange={this.formChangeHandler}
                    >
                      <option value="">-Select-</option>
                      {caDistrictOptions.map(districtItem => {
                        return (
                          <option value={`${districtItem.id}`}>
                            {districtItem.districtName}
                          </option>
                        );
                      })}
                    </select>
                    <label className="labelstyle">
                      {formConfig.caDistrict && formConfig.caDistrict.label
                        ? formConfig.caDistrict.label
                        : ""}
                    </label>
                    <ValidationError
                      fieldValidation={this.state.validationsObj["caDistrict"]}
                    />
                  </article>
                </article>
                {this.state.formData.correspondenceAddress &&
                  this.state.formData.correspondenceAddress.district &&
                  this.state.formData.correspondenceAddress.district.id ==
                  otherDistrictId ? (
                    <OtherTextField
                      custom={
                        formConfig.caOtherDistrict &&
                          formConfig.caOtherDistrict.custom
                          ? formConfig.caOtherDistrict.custom
                          : ""
                      }
                      fieldValue={
                        this.state.formData.correspondenceAddress &&
                          this.state.formData.correspondenceAddress.otherDistrict
                          ? this.state.formData.correspondenceAddress
                            .otherDistrict
                          : ""
                      }
                      fieldId="caOtherDistrict"
                      clickHandler={this.formChangeHandler}
                      fieldLabel={
                        formConfig.caOtherDistrict &&
                          formConfig.caOtherDistrict.label
                          ? formConfig.caOtherDistrict.label
                          : ""
                      }
                      fieldValidation={
                        this.state.validationsObj["caOtherDistrict"]
                      }
                    />
                  ) : null}
                <article
                  className={`${
                    formConfig.caCity && formConfig.caCity.active
                      ? "show"
                      : "hide"
                    } col-md-4`}
                >
                  <article className="form-group">
                    <input
                      type="text"
                      data-custom={
                        formConfig.caCity && formConfig.caCity.custom
                          ? formConfig.caCity.custom
                          : ""
                      }
                      value={
                        this.state.formData.correspondenceAddress &&
                          this.state.formData.correspondenceAddress.city
                          ? this.state.formData.correspondenceAddress.city
                          : ""
                      }
                      id="caCity"
                      onChange={this.formChangeHandler}
                      required
                    />
                    <label>{`${
                      formConfig.caCity && formConfig.caCity.label
                        ? formConfig.caCity.label
                        : ""
                      }`}</label>
                    <ValidationError
                      fieldValidation={this.state.validationsObj["caCity"]}
                    />
                  </article>
                </article>
                <article
                  className={`${
                    formConfig.caPincode && formConfig.caPincode.active
                      ? "show"
                      : "hide"
                    } col-md-4`}
                >
                  <article className="form-group">
                    <input
                      type="text"
                      data-custom={
                        formConfig.caPincode && formConfig.caPincode.custom
                          ? formConfig.caPincode.custom
                          : ""
                      }
                      value={
                        this.state.formData.correspondenceAddress &&
                          this.state.formData.correspondenceAddress.pincode
                          ? this.state.formData.correspondenceAddress.pincode
                          : ""
                      }
                      id="caPincode"
                      maxLength="6"
                      onChange={this.formChangeHandler}
                      required
                    />
                    <label>{`${
                      formConfig.caPincode && formConfig.caPincode.label
                        ? formConfig.caPincode.label
                        : ""
                      }`}</label>
                    <ValidationError
                      fieldValidation={this.state.validationsObj["caPincode"]}
                    />
                  </article>
                </article>
              </article>
            </article>

            <article className="row">
              <article
                className={`${
                  formConfig.employeeDetails &&
                    formConfig.employeeDetails.active
                    ? "show"
                    : "hide"
                  } col-md-12 col-xs-12 col-sm-12 form-group lefterror`}
              >
                <article
                  className="radio-btn "
                  data-custom={
                    formConfig.employeeDetails &&
                      formConfig.employeeDetails.custom
                      ? formConfig.employeeDetails.custom
                      : ""
                  }
                  id="employeeDetails"
                  onChange={event =>
                    this.formChangeHandler(
                      event,
                      "employeeDetails",
                      formConfig.employeeDetails.custom
                    )
                  }
                >
                  <h3>
                    {formConfig.employeeDetails &&
                      formConfig.employeeDetails.label}
                  </h3>
                  <span>
                    <label>
                      <input
                        type="radio"
                        name="radio"
                        checked={
                          this.state.formData.customData.employeeDetails ==
                          "yes"
                        }
                        value="yes"
                      />
                      <span />
                      &nbsp;<i>Yes</i>
                    </label>
                  </span>
                  <span className="paddingborder">&nbsp;</span>
                  <span className="marginleft">
                    <label>
                      <input
                        type="radio"
                        value="no"
                        checked={
                          this.state.formData.customData.employeeDetails == "no"
                        }
                        name="radio"
                      />
                      <span />
                      &nbsp;<i>No</i>
                    </label>
                  </span>
                </article>
                <ValidationError
                  fieldValidation={this.state.validationsObj["employeeDetails"]}
                />
                {this.state.formData.customData.employeeDetails == "yes" ? (
                  <div className="clearboth">
                    <article
                      className={`${
                        formConfig.employeeId && formConfig.employeeId.active
                          ? "show"
                          : "hide"
                        } col-md-4`}
                    >
                      <article className="form-group">
                        <input
                          type="text"
                          required
                          id="employeeId"
                          value={this.state.formData.customData.employeeId}
                          data-custom={
                            formConfig.employeeId &&
                              formConfig.employeeId.custom
                              ? formConfig.employeeId.custom
                              : ""
                          }
                          onChange={this.formChangeHandler}
                        />
                        <label>{formConfig.employeeId.label}</label>
                        <ValidationError
                          fieldValidation={
                            this.state.validationsObj["employeeId"]
                          }
                        />
                      </article>
                    </article>
                    <article
                      className={`${
                        formConfig.employeeName &&
                          formConfig.employeeName.active
                          ? "show"
                          : "hide"
                        } col-md-4`}
                    >
                      <article className="form-group">
                        <input
                          type="text"
                          id="employeeName"
                          required
                          value={this.state.formData.customData.employeeName}
                          data-custom={
                            formConfig.employeeName &&
                              formConfig.employeeName.custom
                              ? formConfig.employeeName.custom
                              : ""
                          }
                          onChange={this.formChangeHandler}
                        />
                        <label>{formConfig.employeeName.label}</label>
                        <ValidationError
                          fieldValidation={
                            this.state.validationsObj["employeeName"]
                          }
                        />
                      </article>
                    </article>
                    <article
                      className={`${
                        formConfig.employeeContact &&
                          formConfig.employeeContact.active
                          ? "show"
                          : "hide"
                        } col-md-4`}
                    >
                      <article className="form-group">
                        <input
                          type="text"
                          id="employeeContact"
                          maxLength="10"
                          required
                          value={this.state.formData.customData.employeeContact}
                          data-custom={
                            formConfig.employeeContact &&
                              formConfig.employeeContact.custom
                              ? formConfig.employeeContact.custom
                              : ""
                          }
                          onChange={this.formChangeHandler}
                        />
                        <label>
                          {formConfig.employeeContact &&
                            formConfig.employeeContact.label}
                        </label>
                        <ValidationError
                          fieldValidation={
                            this.state.validationsObj["employeeContact"]
                          }
                        />
                      </article>
                    </article>
                    <article
                      className={`${
                        formConfig.employeeDesignation &&
                          formConfig.employeeDesignation.active
                          ? "show"
                          : "hide"
                        } col-md-4`}
                    >
                      <article className="form-group">
                        <input
                          type="text"
                          id="employeeDesignation"
                          required
                          value={
                            this.state.formData.customData.employeeDesignation
                          }
                          data-custom={
                            formConfig.employeeDesignation &&
                              formConfig.employeeDesignation.custom
                              ? formConfig.employeeDesignation.custom
                              : ""
                          }
                          onChange={this.formChangeHandler}
                        />
                        <label>{formConfig.employeeDesignation.label}</label>
                        <ValidationError
                          fieldValidation={
                            this.state.validationsObj["employeeDesignation"]
                          }
                        />
                      </article>
                    </article>
                    {/* <article className="row">
                    <article className="col-md-12 notetext">
                      Note: If you have more than one family member as a
                      government employee. Kindly mention their occupation in
                      the family member section.
                    </article>
                  </article> */}
                  </div>
                ) : null}
              </article>
              <article
                className={`${
                  formConfig.govt_emp && formConfig.govt_emp.active
                    ? "show"
                    : "hide"
                  } col-md-6 col-xs-12 col-sm-12 form-group `}
              >
                <article
                  className="radio-btn"
                  data-custom={
                    formConfig.govt_emp && formConfig.govt_emp.custom
                      ? formConfig.govt_emp.custom
                      : ""
                  }
                  id="govt_emp"
                  onChange={event =>
                    this.formChangeHandler(
                      event,
                      "govt_emp",
                      formConfig.govt_emp && formConfig.govt_emp.custom
                    )
                  }
                >
                  <h3>{formConfig.govt_emp && formConfig.govt_emp.label}</h3>
                  <span>
                    <label>
                      <input
                        type="radio"
                        name="radio"
                        checked={
                          this.state.formData.customData.govt_emp == "yes"
                        }
                        value="yes"
                      />
                      <span />
                      &nbsp;<i>Yes</i>
                    </label>
                  </span>
                  <span className="paddingborder">&nbsp;</span>
                  <span className="marginleft">
                    <label>
                      <input
                        type="radio"
                        value="no"
                        checked={
                          this.state.formData.customData.govt_emp == "no"
                        }
                        name="radio"
                      />
                      <span />
                      &nbsp;<i>No</i>
                    </label>
                  </span>
                </article>
                <ValidationError
                  fieldValidation={this.state.validationsObj["govt_emp"]}
                />
              </article>
              {this.state.formData.customData.govt_emp == "yes" ? (
                <div className="clearboth">
                  <article
                    className={`${
                      formConfig.relationship && formConfig.relationship.active
                        ? "show"
                        : "hide"
                      } col-md-4`}
                  >
                    <article className="form-group">
                      <select
                        className="icon"
                        id="relationship"
                        value={
                          this.state.formData.customData.relationship
                            ? this.state.formData.customData.relationship.id
                            : ""
                        }
                        data-custom={
                          formConfig.relationship &&
                            formConfig.relationship.custom
                            ? formConfig.relationship.custom
                            : ""
                        }
                        onChange={this.formChangeHandler}
                      >
                        <option value="">-Select-</option>
                        {relationOptions.map(rItem => {
                          return (
                            <option value={rItem.id}>{rItem.rulevalue}</option>
                          );
                        })}
                      </select>
                      <label className="labelstyle">
                        {formConfig.relationship.label}
                      </label>
                      <ValidationError
                        fieldValidation={
                          this.state.validationsObj["relationship"]
                        }
                      />
                    </article>
                  </article>
                  <article
                    className={`${
                      formConfig.designation && formConfig.designation.active
                        ? "show"
                        : "hide"
                      } col-md-4`}
                  >
                    <article className="form-group">
                      <input
                        type="text"
                        required
                        id="designation"
                        value={this.state.formData.customData.designation}
                        data-custom={
                          formConfig.designation &&
                            formConfig.designation.custom
                            ? formConfig.designation.custom
                            : ""
                        }
                        onChange={this.formChangeHandler}
                      />
                      <label>{formConfig.designation.label}</label>
                      <ValidationError
                        fieldValidation={
                          this.state.validationsObj["designation"]
                        }
                      />
                    </article>
                  </article>
                  <article
                    className={`${
                      formConfig.organization_name &&
                        formConfig.organization_name.active
                        ? "show"
                        : "hide"
                      } col-md-4`}
                  >
                    <article className="form-group">
                      <input
                        type="text"
                        id="organization_name"
                        required
                        value={this.state.formData.customData.organization_name}
                        data-custom={
                          formConfig.organization_name &&
                            formConfig.organization_name.custom
                            ? formConfig.organization_name.custom
                            : ""
                        }
                        onChange={this.formChangeHandler}
                      />
                      <label>{formConfig.organization_name.label}</label>
                      <ValidationError
                        fieldValidation={
                          this.state.validationsObj["organization_name"]
                        }
                      />
                    </article>
                  </article>
                  <article className="row">
                    <article className="col-md-12 notetext">
                      Note: If you have more than one family member as a
                      government employee. Kindly mention their occupation in
                      the family member section.
                    </article>
                  </article>
                </div>
              ) : null}
            </article>
            <article className="row">
              <article className="col-md-12">
                <input
                  type="button"
                  value="Save & Continue"
                  className="btn  pull-right"
                  onClick={this.updateUserData}
                />
              </article>
            </article>
          </article>
        </section>
      );
    } else {
      return <p>Sorry! No Data Available.</p>;
    }
  }
}

export default PersonalInfo;
