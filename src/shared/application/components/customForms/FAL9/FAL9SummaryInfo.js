import React, { Component } from "react";
import Loader from "../../../../common/components/loader";
import AlertMessage from "../../../../common/components/alertMsg";
import { all } from "redux-saga/effects";
import { stepNamesToDisplay, childEduBSID } from "../../../formconfig";
import { Route, Link, Redirect } from "react-router-dom";
import gblFunc from "../../../../../globals/globalFunctions";

class Summary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      presentClass: null,
      redirectToEducation: false,
      historyList: {
        personalInfo: {},
        educationInfos: [],
        familyInfos: [],
        interests: [],
        references: [],
        entranceExams: [],
        scholarshipHistories: [],
        scholarshipQuestion: [],
        entranceExams: [],
        bankDetail: []
      },
      personalConfig: [],
      educationConfig: [],
      familyConfig: [],
      refrenceConfig: [],
      isSubmitDisabled: true,
      msg: "",
      status: "",
      showLoader: false,
      isCompleted: false,
      decHtml: "",
      showpopupTerms: false,
      isRedirect: false
    };

    this.submitApplicaitonForm = this.submitApplicaitonForm.bind(this);
  }

  componentDidMount() {
    this.props.fetchUserRule({
      userid: gblFunc.getStoreUserDetails()["userId"]
    });
  }

  close() {
    if (this.state.redirectToEducation) {
      this.props.history.push(
        `/application/${this.props.match.params.bsid.toUpperCase()}/form/educationInfo`
      );
    } else {
      this.setState({
        showLoader: false,
        msg: "",
        status: false
      });
    }
  }
  togglePopup() {
    this.setState({
      showpopupTerms: !this.state.showpopupTerms
    });
  }
  componentWillReceiveProps(nextProps) {
    const { type } = nextProps.applicationSummery;
    const { match } = this.props;
    switch (type) {
      case "FETCH_USER_RULES_SUCCESS":
        const { userRules } = nextProps.userRuleInfo;
        if (userRules && userRules.length > 0) {
          const presentClass = userRules.filter(rule => rule.ruleTypeId == 1);
          if (presentClass && presentClass.length) {
            this.setState(
              {
                presentClass: presentClass[0].ruleId
              },
              () =>
                this.props.applicationInstructionStep({
                  scholarshipId: gblFunc.getStoreApplicationScholarshipId()
                })
            );
          } else {
            this.props.applicationInstructionStep({
              scholarshipId: gblFunc.getStoreApplicationScholarshipId()
            });
          }
        } else {
          this.props.applicationInstructionStep({
            scholarshipId: gblFunc.getStoreApplicationScholarshipId()
          });
        }
        break;
      case "GET_ALL_SUMMERY_RULES_SUCCESS":
        let summary = nextProps["applicationSummery"]
          ? nextProps["applicationSummery"]["summeryData"]["summary"]
          : [];

        let allSteps = nextProps["scholarshipSteps"]
          ? nextProps["scholarshipSteps"]
          : [];
        let completedSteps = nextProps["applicationStepInstruction"]
          ? nextProps["applicationStepInstruction"]
          : [];
        let innerHtml = nextProps["instructions"]["consent"];
        let isCompleted = false;
        let countSteps = 0;
        if (allSteps != null && allSteps.length > 0) {
          allSteps.map(res => {
            const rsType = gblFunc.getStoreHulAuthType("hulType");

            if (rsType && rsType === "3P") {
              if (res.step != "SUMMARY" && res.step !== "BANK_DETAIL") {
                if (completedSteps.indexOf(res.step) > -1) {
                  ++countSteps;
                  if (allSteps.length - 2 == countSteps) {
                    isCompleted = true;
                  }
                }
              }
            } else {
              if (res.step != "SUMMARY") {
                if (completedSteps.indexOf(res.step) > -1) {
                  ++countSteps;
                  if (allSteps.length - 1 == countSteps) {
                    isCompleted = true;
                  }
                }
              }
            }
          });
        }

        this.setState({
          historyList: {
            personalInfo: summary["personalInfo"],
            educationInfos: summary["educationInfos"],
            familyInfos: summary["familyInfos"],
            interests: summary["interests"],
            references: summary["references"],
            entranceExams: summary["entranceExams"],
            scholarshipHistories: summary["scholarshipHistories"],
            userScholarshipDocuments: summary["userScholarshipDocuments"],
            scholarshipQuestion: summary["scholarshipQuestions"],
            entranceExams: summary["entranceExams"],
            bankDetail: summary["bankDetails"]
          },
          isCompleted: isCompleted,
          decHtml: innerHtml
        });
        break;
      case "FETCH_SCHOLARSHIP_APPLY_SUCCESS_SUMMARY":
        this.setState({
          status: true,
          msg: "Application Form has been submitted successfully",
          showLoader: true
        });

        setTimeout(() => {
          this.setState({
            isRedirect: true // redirect to application status page
          });
        }, 1000);

        break;
      case "FETCH_SCHOLARSHIP_APPLY_FAILURE_SUMMARY":
        this.setState({
          status: false,
          msg: "Sorry!! Server Error",
          showLoader: true
        });

        break;

      case "FETCH_APPLICATION_STEP_SUCCESS":
        const { applicationStepInstruction } = nextProps;
        if (nextProps.userRuleInfo) {
          if (
            applicationStepInstruction.indexOf("EDUCATION_INFO") == -1 &&
            !["IGA1"].includes(this.props.match.params.bsid.toUpperCase())
          ) {
            this.setState({
              msg:
                "Please complete your education details, before proceeding further.",
              showLoader: true,
              status: false,
              redirectToEducation: true
            });
          } else {
            this.props.getAllSummery({
              scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
              presentClass: this.state.presentClass
            });
          }
        }

        break;
      default:
        break;
    }
  }

  isSubmitDisabled(event) {
    if (event.target.checked) {
      this.setState({
        isSubmitDisabled: false
      });
    } else {
      this.setState({
        isSubmitDisabled: true
      });
    }
  }

  submitApplicaitonForm() {
    if (this.state.isSubmitDisabled) {
      this.setState({
        status: false,
        msg: "Kindly agree to the terms and conditions.",
        showLoader: true
      });
      return false;
    } else {
      if (this.state.isCompleted) {
        let scholarshipId = "";
        if (window != undefined) {
          scholarshipId = parseInt(
            window.localStorage.getItem("applicationSchID")
          );
        }
        this.props.fetchSchApply({
          scholarshipId: scholarshipId
        });
        return false;
      } else {
        this.setState({
          status: false,
          msg: "Kindly complete all steps",
          showLoader: true
        });
        return false;
      }
    }
  }

  markup(val) {
    return { __html: val };
  }

  displayQuestionAns(res) {
    if (res.question["questionTypeId"] == 1) {
      return res.response;
    } else if (res.question["questionTypeId"] == 2) {
      if (res.optionId != null && res.optionId.length > 0) {
        let signleOptionAns = res.question["questionOptions"].filter(items => {
          return items["id"] == res.optionId[0];
        });

        if (signleOptionAns != null && signleOptionAns.length > 0) {
          return signleOptionAns[0].optionValue;
        } else {
          return "No Result Found";
        }
      }
    } else {
      if (res.optionId != null && res.optionId.length > 0) {
        let multiList = [];
        let multiOptionAns = res.question["questionOptions"].filter(items => {
          return res.optionId.indexOf(items["id"]) > -1;
        });

        if (multiOptionAns != null && multiOptionAns.length > 0) {
          multiOptionAns.map(items => {
            multiList.push(items.optionValue);
          });
          return multiList.length ? multiList.join(", ") : "";
        } else {
          return "No Result Found";
        }
      }
    }
  }

  render() {
    const { applicationStepInstruction, scholarshipSteps, match } = this.props;
    let isSubmitArray = [];
    let redirecTo = null;

    if (applicationStepInstruction && scholarshipSteps) {
      if (
        Array.isArray(applicationStepInstruction) &&
        Array.isArray(scholarshipSteps)
      ) {
        if (
          applicationStepInstruction.length > 0 &&
          scholarshipSteps.length > 0
        ) {
          isSubmitArray = scholarshipSteps.map(schStep => {
            return applicationStepInstruction.indexOf(schStep.step) > -1
              ? true
              : false;
          });
        }
      }
    }

    if (
      this.state.isRedirect &&
      gblFunc.getStoreUserDetails()["vleUser"] == "true"
    ) {
      redirecTo = <Redirect to={`/vle/student-list`} />;
    } else if (
      this.state.isRedirect &&
      ["CBS3", "CBI2"].includes(this.props.match.params.bsid.toUpperCase())
    ) {
      redirecTo = (
        <Redirect
          to={`/payment/${this.props.match.params.bsid}/collegeboard`}
        />
      );
    } else if (
      this.state.isRedirect &&
      ["CES7","CES8", "SCE3", "CKS1", "SSE4", "HUL1"].includes(
        this.props.match.params.bsid.toUpperCase()
      )
    ) {
      redirecTo = (
        <Redirect
          to={{
            pathname: `/application/${this.props.match.params.bsid.toUpperCase()}/instruction`,
            state: {
              from: "summary",
              isChildForm: childEduBSID[
                this.props.match.params.bsid.toUpperCase()
              ]
                ? true
                : false
            }
          }}
        />
      );
    } else if (this.state.isRedirect) {
      redirecTo = <Redirect to={`/myscholarship/?tab=application-status`} />;
    }

    return (
      <section className="sectionwhite">
        {redirecTo}

        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <article className="row">
          <article className="col-md-12 subheading">
            <p style={{ color: "red" }}>
              *Preview of entered details are visible below. Please verify them
              before clicking on the Submit button available at the end of the
              summary for final submission of your application.
            </p>
          </article>
        </article>
        <article className="form">
          {this.state.historyList["personalInfo"] != null ? (
            <article>
              <article className="row">
                <article className="col-md-12 subheading">
                  {stepNamesToDisplay["PERSONAL_INFO"]}
                </article>
                <article className="col-md-12  table-responsive paddingtable0">
                  <table className="table table-striped">
                    <thead>
                      {match.params &&
                        childEduBSID[match.params.bsid.toUpperCase()] ? (
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>DOB</th>
                            <th>Address</th>
                            {/*  <th>AadharCard</th> */}
                          </tr>
                        ) : (
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>DOB</th>
                            <th>Annual Family Income (INR)</th>
                            {/*  <th>AadharCard</th> */}
                          </tr>
                        )}
                    </thead>
                    <tbody>
                      {match.params &&
                        childEduBSID[match.params.bsid.toUpperCase()] ? (
                          <tr>
                            <td>
                              {this.state.historyList["personalInfo"].firstName}&nbsp;{
                                this.state.historyList["personalInfo"].lastName
                              }
                            </td>
                            <td>
                              {this.state.historyList["personalInfo"].email ? (
                                this.state.historyList["personalInfo"].email
                              ) : (
                                  <span>&mdash;</span>
                                )}
                            </td>
                            <td>
                              {this.state.historyList["personalInfo"].mobile ? (
                                this.state.historyList["personalInfo"].mobile
                              ) : (
                                  <span>&mdash;</span>
                                )}
                            </td>
                            <td>
                              {this.state.historyList["personalInfo"].dob ? (
                                this.state.historyList["personalInfo"].dob
                              ) : (
                                  <span>&mdash;</span>
                                )}
                            </td>
                            <td>
                              {this.state.historyList["personalInfo"]
                                .userAddress &&
                                this.state.historyList["personalInfo"].userAddress
                                  .addressLine ? (
                                  this.state.historyList["personalInfo"].userAddress
                                    .addressLine
                                ) : (
                                  <span>&mdash;</span>
                                )}{" "}
                            </td>
                            {/*    <td>
                          {this.state.historyList["personalInfo"].aadharCard}
                        </td> */}
                          </tr>
                        ) : (
                          <tr>
                            <td>
                              {this.state.historyList["personalInfo"].firstName}&nbsp;{
                                this.state.historyList["personalInfo"].lastName
                              }
                            </td>
                            <td>
                              {this.state.historyList["personalInfo"].email ? (
                                this.state.historyList["personalInfo"].email
                              ) : (
                                  <span>&mdash;</span>
                                )}
                            </td>
                            <td>
                              {this.state.historyList["personalInfo"].mobile ? (
                                this.state.historyList["personalInfo"].mobile
                              ) : (
                                  <span>&mdash;</span>
                                )}
                            </td>
                            <td>
                              {this.state.historyList["personalInfo"].dob ? (
                                this.state.historyList["personalInfo"].dob
                              ) : (
                                  <span>&mdash;</span>
                                )}
                            </td>
                            <td>
                              {this.state.historyList["personalInfo"]
                                .familyIncome ? (
                                  this.state.historyList["personalInfo"]
                                    .familyIncome
                                ) : (
                                  <span>&mdash;</span>
                                )}{" "}
                            </td>
                            {/*    <td>
                          {this.state.historyList["personalInfo"].aadharCard}
                        </td> */}
                          </tr>
                        )}

                      {this.state.historyList["personalInfo"] == null ? (
                        <tr>
                          <td align="center" colSpan="6">
                            <article className="text-center">
                              No Result Found
                            </article>
                          </td>
                        </tr>
                      ) : (
                          ""
                        )}
                    </tbody>
                  </table>
                </article>
              </article>
              <article className="paddingborder">
                <article className="border">&nbsp;</article>
              </article>
            </article>
          ) : (
              ""
            )}

          {this.state.historyList["educationInfos"] != null ? (
            <article>
              <article className="row">
                <article className="col-md-12 subheading">
                  {stepNamesToDisplay["EDUCATION_INFO"]}
                </article>
                <article className="col-md-12  table-responsive paddingtable0">
                  <table className="table table-striped">
                    <thead>
                      {match.params &&
                        childEduBSID[match.params.bsid.toUpperCase()] ? (
                          <tr>
                            {/* <th>Institute Name</th> */}
                            <th>Class Name</th>
                            <th>Institute/School/College</th>
                            <th>Annual Fee</th>
                            <th>State</th>
                            {/*  <th>Year</th> */}
                          </tr>
                        ) : (
                          <tr>
                            {/* <th>Institute Name</th> */}
                            <th>Class Name</th>
                            <th>Marking Type</th>
                            <th>Marks</th>
                            <th>Percent/Grade Value</th>
                            {/*  <th>Year</th> */}
                          </tr>
                        )}
                    </thead>
                    <tbody>
                      {this.state.historyList["educationInfos"].map(
                        (res, i) => {
                          return match.params &&
                            childEduBSID[match.params.bsid] ? (
                              <tr key={i}>
                                {/* <td>
                        {res.userInstituteInfo
                          ? res.userInstituteInfo.instituteName
                          : ""}
                      </td> */}
                                <td>
                                  {res.userAcademicInfo ? (
                                    res.userAcademicInfo.academicClassName
                                  ) : (
                                      <span>&mdash;</span>
                                    )}
                                </td>
                                <td>
                                  {res.userInstituteInfo ? (
                                    res.userInstituteInfo.instituteName
                                  ) : (
                                      <span>&mdash;</span>
                                    )}
                                </td>
                                <td>
                                  {res.userAcademicInfo ? (
                                    res.userAcademicInfo.tuitionFee
                                  ) : (
                                      <span>&mdash;</span>
                                    )}
                                </td>
                                <td>
                                  {res.userInstituteInfo ? (
                                    res.userInstituteInfo.stateName
                                  ) : (
                                      <span>&mdash;</span>
                                    )}
                                </td>
                                {/*    <td>
                        {res.userAcademicInfo
                          ? res.userAcademicInfo.passingYear
                          : ""}
                      </td> */}
                              </tr>
                            ) : (
                              <tr key={i}>
                                {/* <td>
                                {res.userInstituteInfo
                                  ? res.userInstituteInfo.instituteName
                                  : ""}
                              </td> */}
                                <td>
                                  {res.userAcademicInfo ? (
                                    res.userAcademicInfo.academicClassName
                                  ) : (
                                      <span>&mdash;</span>
                                    )}
                                </td>
                                <td>
                                  {res.userAcademicInfo.markingType == 1
                                    ? "Marks"
                                    : "Grades"}
                                </td>
                                <td>
                                  {res.userAcademicInfo ? (
                                    res.userAcademicInfo.marksObtained
                                  ) : (
                                      <span>&mdash;</span>
                                    )}

                                  {res.userAcademicInfo &&
                                    res.userAcademicInfo.marksObtained != null ? (
                                      "/"
                                    ) : (
                                      <span>&mdash;</span>
                                    )}

                                  {res.userAcademicInfo ? (
                                    res.userAcademicInfo.totalMarks
                                  ) : (
                                      <span>&mdash;</span>
                                    )}
                                </td>
                                <td>
                                  {res.userAcademicInfo.markingType == 1 ? (
                                    res.userAcademicInfo.percentage != null ? (
                                      res.userAcademicInfo.percentage.toFixed(2)
                                    ) : (
                                        <span>&mdash;</span>
                                      )
                                  ) : res.userAcademicInfo.grade != null ? (
                                    res.userAcademicInfo.grade
                                  ) : (
                                        <span>&mdash;</span>
                                      )}
                                </td>
                                {/*    <td>
                                {res.userAcademicInfo
                                  ? res.userAcademicInfo.passingYear
                                  : ""}
                              </td> */}
                              </tr>
                            );
                        }
                      )}

                      {this.state.historyList["educationInfos"].length == 0 ? (
                        <tr>
                          <td align="center" colSpan="6">
                            <article className="text-center">
                              No Result Found
                            </article>
                          </td>
                        </tr>
                      ) : (
                          ""
                        )}
                    </tbody>
                  </table>
                </article>
              </article>
              <article className="paddingborder">
                <article className="border">&nbsp;</article>
              </article>
            </article>
          ) : (
              ""
            )}

          {this.state.historyList["familyInfos"] != null ? (
            <article>
              <article className="row">
                <article className="col-md-12 subheading">
                  {stepNamesToDisplay["FAMILY_INFO"]}
                </article>
                <article className="col-md-12  table-responsive paddingtable0">
                  <table className="table table-striped">
                    <thead>
                      {match.params &&
                        childEduBSID[match.params.bsid.toUpperCase()] ? (
                          <tr>
                            <th>Name</th>
                            <th>Employee Id</th>
                            <th>Store Details</th>
                            <th>Date of joining</th>
                            <th>Mobile</th>
                          </tr>
                        ) : (
                          <tr>
                            <th>Name</th>
                            <th>Occupation</th>
                            <th>Absolute Income (INR)</th>
                            <th>Relation</th>
                          </tr>
                        )}
                    </thead>
                    <tbody>
                      {this.state.historyList["familyInfos"].map((res, i) => {
                        return match.params &&
                          childEduBSID[match.params.bsid.toUpperCase()] ? (
                            <tr key={i}>
                              <td>
                                {res.name ? res.name : <span>&mdash;</span>}
                              </td>
                              <td>
                                {res.customData ? (
                                  res.customData.employeeId
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                              <td>
                                {res.customData ? (
                                  res.customData.storeDetails
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                              <td>
                                {res.customData ? (
                                  res.customData.joiningDate
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                              <td>
                                {res.mobile ? res.mobile : <span>&mdash;</span>}
                              </td>
                            </tr>
                          ) : (
                            <tr key={i}>
                              <td>
                                {res.name ? res.name : <span>&mdash;</span>}
                              </td>
                              <td>
                                {res.occupationName ? (
                                  res.occupationName
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                              <td>
                                {res.absoluteIncome ? (
                                  res.absoluteIncome
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                              <td>
                                {res.relationName ? (
                                  res.relationName
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                            </tr>
                          );
                      })}

                      {this.state.historyList["familyInfos"].length == 0 ? (
                        <tr>
                          <td align="center" colSpan="6">
                            <article className="text-center">
                              No Result Found
                            </article>
                          </td>
                        </tr>
                      ) : (
                          ""
                        )}
                    </tbody>
                  </table>
                </article>
              </article>
              <article className="paddingborder">
                <article className="border" />
              </article>
            </article>
          ) : (
              ""
            )}

          {this.state.historyList["references"] != null ? (
            <article>
              <article className="row">
                <article className="col-md-12 subheading">
                  {stepNamesToDisplay["REFERENCES"]}
                </article>
                <article className="col-md-12  table-responsive paddingtable0">
                  <table className="table table-striped">
                    <thead>
                      {match.params &&
                        childEduBSID[match.params.bsid.toUpperCase()] ? (
                          <tr>
                            <th>MTAS/FM Name of the Employee</th>
                            <th>MTAS/FM Mobile No</th>
                            <th>Employee Id</th>
                            <th>Store Details</th>
                            <th>Date of joining</th>
                          </tr>
                        ) : (
                          <tr>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Address</th>
                            <th>Occupation</th>
                            <th>Relation</th>
                          </tr>
                        )}
                    </thead>
                    <tbody>
                      {this.state.historyList["references"].map((res, i) => {
                        return match.params &&
                          childEduBSID[match.params.bsid.toUpperCase()] ? (
                            <tr key={i}>
                              <td>
                                {res.name ? res.name : <span>&mdash;</span>}
                              </td>
                              <td>
                                {res.mobile ? res.mobile : <span>&mdash;</span>}
                              </td>
                              <td>
                                {res.customData.employeeId ? (
                                  res.customData.employeeId
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                              <td>
                                {res.customData.storeDetails ? (
                                  res.customData.storeDetails
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                              <td>
                                {res.customData.joiningDate ? (
                                  res.customData.joiningDate
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                            </tr>
                          ) : (
                            <tr key={i}>
                              <td>
                                {res.name ? res.name : <span>&mdash;</span>}
                              </td>
                              <td>
                                {res.mobile ? res.mobile : <span>&mdash;</span>}
                              </td>
                              <td>
                                {res.address ? res.address : <span>&mdash;</span>}
                              </td>
                              <td>
                                {res.occupationName ? (
                                  res.occupationName
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                              <td>
                                {res.relationName ? (
                                  res.relationName
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                            </tr>
                          );
                      })}

                      {this.state.historyList["references"].length == 0 ? (
                        <tr>
                          <td align="center" colSpan="6">
                            <article className="text-center">
                              No Result Found
                            </article>
                          </td>
                        </tr>
                      ) : (
                          ""
                        )}
                    </tbody>
                  </table>
                </article>
              </article>
              <article className="paddingborder">
                <article className="border">&nbsp;</article>
              </article>
            </article>
          ) : (
              ""
            )}

          {this.state.historyList["bankDetail"] != null &&
            this.state.historyList["bankDetail"].length ? (
              <article>
                <article className="row">
                  <article className="col-md-12 subheading">
                    {stepNamesToDisplay["BANK_DETAIL"]}
                  </article>
                  <article className="col-md-12  table-responsive paddingtable0">
                    <table className="table table-striped">
                      <thead>
                        <tr>
                          <th>Account Holder Name</th>
                          <th>Account Number</th>
                          <th>Bank Name</th>
                          {match.params &&
                            childEduBSID[match.params.bsid.toUpperCase()] ? null : (
                              <th>Branch Name</th>
                            )}
                          <th>IFSC Code</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.historyList["bankDetail"].map((res, i) => {
                          return (
                            <tr key={i}>
                              <td>
                                {res.accountHolderName ? (
                                  res.accountHolderName
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                              <td>
                                {res.accountNumber ? (
                                  res.accountNumber
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                              <td>
                                {res.bankName ? (
                                  res.bankName
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                              {match.params &&
                                childEduBSID[
                                match.params.bsid.toUpperCase()
                                ] ? null : (
                                  <td>
                                    {res.branchName ? (
                                      res.branchName
                                    ) : (
                                        <span>&mdash;</span>
                                      )}
                                  </td>
                                )}

                              <td>
                                {res.ifscCode ? (
                                  res.ifscCode
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                            </tr>
                          );
                        })}

                        {this.state.historyList["bankDetail"].length == 0 ? (
                          <tr>
                            <td align="center" colSpan="6">
                              <article className="text-center">
                                No Result Found
                            </article>
                            </td>
                          </tr>
                        ) : (
                            ""
                          )}
                      </tbody>
                    </table>
                  </article>
                </article>
                <article className="paddingborder">
                  <article className="border">&nbsp;</article>
                </article>
              </article>
            ) : (
              ""
            )}

          {this.state.historyList["userScholarshipDocuments"] != null ? (
            <article>
              <article className="row">
                <article className="col-md-12 subheading">
                  {stepNamesToDisplay["DOCUMENTS"]}
                </article>
                <article className="col-md-12  table-responsive paddingtable0">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>Document Name</th>
                        <th>Document Location</th>
                        {/*     <th>Income</th>
                        <th>Mobile</th>
                        <th>Relation</th> */}
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.historyList["userScholarshipDocuments"].map(
                        (res, i) => {
                          return (
                            <tr key={i}>
                              <td>
                                {res.docNameLabel != null ? (
                                  res.docNameLabel
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                              <td>
                                {res.userDocument != null ? (
                                  // ? res.userDocument[0].location
                                  <a
                                    href={res.userDocument[0].location}
                                    target="_blank"
                                  >
                                    <i className="fa fa-download Checkicon" />
                                  </a>
                                ) : (
                                    "No document uploaded"
                                  )}
                              </td>
                            </tr>
                          );
                        }
                      )}

                      {this.state.historyList["userScholarshipDocuments"]
                        .length == 0 ? (
                          <tr>
                            <td align="center" colSpan="6">
                              <article className="text-center">
                                No Result Found
                            </article>
                            </td>
                          </tr>
                        ) : (
                          ""
                        )}
                    </tbody>
                  </table>
                </article>
              </article>
              <article className="paddingborder">
                <article className="border">&nbsp;</article>
              </article>
            </article>
          ) : (
              ""
            )}

          {this.state.historyList["scholarshipQuestion"] != null ? (
            <article>
              <article className="row">
                <article className="col-md-12 subheading">
                  {stepNamesToDisplay["QUESTIONS"]}
                </article>
                <article className="col-md-12  table-responsive paddingtable0">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>Question</th>
                        <th>Answer</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.historyList["scholarshipQuestion"].map(
                        (res, i) => {
                          return (
                            <tr key={i}>
                              <td>
                                {res.question != null ? (
                                  res.question.questionVerbiage
                                ) : (
                                    <span>&mdash;</span>
                                  )}
                              </td>
                              <td className="docicon">
                                {res.question != null
                                  ? this.displayQuestionAns(res)
                                  : "No Response"}
                              </td>
                            </tr>
                          );
                        }
                      )}

                      {this.state.historyList["scholarshipQuestion"].length ==
                        0 ? (
                          <tr>
                            <td align="center" colSpan="6">
                              <article className="text-center">
                                No Result Found
                            </article>
                            </td>
                          </tr>
                        ) : (
                          ""
                        )}
                    </tbody>
                  </table>

                  {this.state.historyList["entranceExams"] != null ? (
                    <article>
                      <article className="row">
                        <article className="col-md-12 subheading">
                          {stepNamesToDisplay["ENTRANCE_EXAM"]}
                        </article>
                        <article className="col-md-12  table-responsive paddingtable0">
                          <table className="table table-striped">
                            <thead>
                              <tr>
                                <th>Exam Name</th>
                                <th>Score</th>
                                <th>Year</th>
                                <th>Month</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.historyList["entranceExams"].map(
                                (res, i) => {
                                  return (
                                    <tr key={i}>
                                      <td>
                                        {res.entranceExamName != null ? (
                                          res.entranceExamName
                                        ) : (
                                            <span>&mdash;</span>
                                          )}
                                      </td>
                                      <td className="docicon">
                                        {res.rank != null ? (
                                          res.rank
                                        ) : (
                                            <span>&mdash;</span>
                                          )}
                                      </td>
                                      <td className="docicon">
                                        {res.year != null ? (
                                          res.year
                                        ) : (
                                            <span>&mdash;</span>
                                          )}
                                      </td>

                                      <td className="docicon">
                                        {res.month != null ? (
                                          res.month
                                        ) : (
                                            <span>&mdash;</span>
                                          )}
                                      </td>
                                    </tr>
                                  );
                                }
                              )}

                              {this.state.historyList["entranceExams"].length ==
                                0 ? (
                                  <tr>
                                    <td align="center" colSpan="6">
                                      <article className="text-center">
                                        No Result Found
                                    </article>
                                    </td>
                                  </tr>
                                ) : (
                                  ""
                                )}
                            </tbody>
                          </table>
                        </article>
                      </article>
                      <article className="paddingborder">
                        <article className="border">&nbsp;</article>
                      </article>
                    </article>
                  ) : (
                      ""
                    )}
                </article>
              </article>
              <article className="paddingborder">
                <article className="border">&nbsp;</article>
              </article>
            </article>
          ) : (
              ""
            )}
        </article>

        <article className="row summarySec">
          <div className="col-md-12">
            {/* <p>
              Before you, as an applicant, can submit your application to
              Marubeni India, you must read and agree to these&nbsp;
              <span className="cursor" onClick={this.togglePopup.bind(this)}>
                terms and conditions
              </span>
            </p> */}

            {/* {this.state.showpopupTerms ? (
              <Terms text="Close Me" closePopup={this.togglePopup.bind(this)} />
            ) : null} */}

            <p dangerouslySetInnerHTML={this.markup(this.state.decHtml)} />
            {["CBI2", "CBS3"].includes(
              this.props.match.params.bsid.toUpperCase()
            ) ? (
                <p style={{ color: "red" }}>
                  College Board reserves the right at any time for any reason to
                  request a proof of your eligibility. In case College Board is
                  unable to verify a student's eligibility to its satisfaction,
                  the student's scholarship and/or registration may be revoked at
                  any time at College Board's sole discretion.
              </p>
              ) : null}
            {/* <p>Declaration</p>
            <p>
              Before you, as an applicant, can submit your application to
              Marubeni India, you must read and agree to these
              <a
                href="#"
                data-toggle="modal"
                data-target="#myTermConditionModal"
              >
                &nbsp;terms and conditions.
              </a>
            </p>
            <p>
              I, hereby declare that the information and the documents furnished
              in/ along with this Scholarship Application Form is true and
              correct and that I have not withheld any relevant/material
              particulars/information.
            </p>
            <p>
              I acknowledge that Marubeni India reserves the right to
              discontinue this Scholarship at any time without any notice to us
              and it shall not be held liable for any consequences thereof.
            </p> */}
            <p>
              <label>
                <input
                  type="checkbox"
                  value="2"
                  id="20"
                  name="20_val"
                  onChange={event => this.isSubmitDisabled(event)}
                />
                <span className="red" />
              </label>
              &nbsp;&nbsp;I have read the{" "}
              <a
                target="_blank"
                href="https://buddy4study.com/terms-and-conditions"
              >
                terms and conditions
              </a>{" "}
              carefully, I agree that by submitting this application, I am
              electronically signing the application.
            </p>
          </div>
          {this.props.match &&
            this.props.match.params &&
            ["CBI2", "CBS3"].includes(
              this.props.match.params.bsid.toUpperCase()
            ) ? (
              <article className="col-md-12">
                <input
                  /*  disabled={
                  isSubmitArray.length == 0 || isSubmitArray.indexOf(false) > -1
                    ? true
                    : false
                } */

                  /*  disabled={this.state.isSubmitDisabled} */
                  onClick={this.submitApplicaitonForm}
                  type="button"
                  value="Submit & Pay"
                  className="btn-yellow btnsubmit"
                />
              </article>
            ) : (
              <article className="col-md-12">
                <input
                  /*  disabled={
                  isSubmitArray.length == 0 || isSubmitArray.indexOf(false) > -1
                    ? true
                    : false
                } */

                  /*  disabled={this.state.isSubmitDisabled} */
                  onClick={this.submitApplicaitonForm}
                  type="button"
                  value="Submit"
                  className="btn-yellow btnsubmit"
                />
              </article>
            )}
        </article>
      </section>
    );
  }
}

export default Summary;
