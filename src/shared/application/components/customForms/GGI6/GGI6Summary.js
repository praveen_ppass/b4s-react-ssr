import React, { Component } from "react";
import Loader from "../../../../common/components/loader";
import AlertMessage from "../../../../common/components/alertMsg";
import { all } from "redux-saga/effects";
import { stepNamesToDisplay } from "../../../formconfig";
import { Route, Link, Redirect } from "react-router-dom";
import gblFunc from "../../../../../globals/globalFunctions";

class Summary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      presentClass: null,
      redirectToEducation: false,
      historyList: {
        personalInfo: {},
        educationInfos: [],
        familyInfos: [],
        interests: [],
        references: [],
        entranceExams: [],
        scholarshipHistories: [],
        scholarshipQuestion: [],
        entranceExams: []
      },
      personalConfig: [],
      educationConfig: [],
      familyConfig: [],
      refrenceConfig: [],
      isSubmitDisabled: true,
      msg: "",
      status: "",
      showLoader: false,
      isCompleted: false,
      decHtml: "",
      showpopupTerms: false,
      isRedirect: false
    };

    this.submitApplicaitonForm = this.submitApplicaitonForm.bind(this);
  }

  componentDidMount() {
    this.props.fetchUserRule({
      userid: gblFunc.getStoreUserDetails()["userId"]
    });
  }

  close() {
    if (this.state.redirectToEducation) {
      this.props.history.push(
        `/application/${this.props.match.params.bsid.toUpperCase()}/form/educationInfo`
      );
    } else {
      this.setState({
        showLoader: false,
        msg: "",
        status: false
      });
    }
  }
  togglePopup() {
    this.setState({
      showpopupTerms: !this.state.showpopupTerms
    });
  }
  componentWillReceiveProps(nextProps) {
    const { type } = nextProps.applicationSummery;

    switch (type) {
      case "FETCH_USER_RULES_SUCCESS":
        const { userRules } = nextProps.userRuleInfo;
        if (userRules && userRules.length) {
          const presentClass = userRules.filter(rule => rule.ruleTypeId == 1);
          if (presentClass && presentClass.length) {
            this.setState(
              {
                presentClass: presentClass[0].ruleId
              },
              () =>
                this.props.applicationInstructionStep({
                  scholarshipId: gblFunc.getStoreApplicationScholarshipId()
                })
            );
          } else {
            this.props.applicationInstructionStep({
              scholarshipId: gblFunc.getStoreApplicationScholarshipId()
            });
          }
        } else {
          this.props.applicationInstructionStep({
            scholarshipId: gblFunc.getStoreApplicationScholarshipId()
          });
        }
        break;
      case "GET_ALL_SUMMERY_RULES_SUCCESS":
        let summary = nextProps["applicationSummery"]
          ? nextProps["applicationSummery"]["summeryData"]["summary"]
          : [];

        let allSteps = nextProps["scholarshipSteps"]
          ? nextProps["scholarshipSteps"]
          : [];
        let completedSteps = nextProps["applicationStepInstruction"]
          ? nextProps["applicationStepInstruction"]
          : [];
        let innerHtml = nextProps["instructions"]["consent"];
        let isCompleted = false;
        let countSteps = 0;
        if (allSteps != null && allSteps.length > 0) {
          allSteps.map(res => {
            if (res.step != "SUMMARY") {
              if (completedSteps.indexOf(res.step) > -1) {
                ++countSteps;
                if (allSteps.length - 1 == countSteps) {
                  isCompleted = true;
                }
              }
            }
          });
        }

        this.setState({
          historyList: {
            personalInfo: summary["personalInfo"],
            educationInfos: summary["educationInfos"],
            familyInfos: summary["familyInfos"],
            interests: summary["interests"],
            references: summary["references"],
            entranceExams: summary["entranceExams"],
            scholarshipHistories: summary["scholarshipHistories"],
            userScholarshipDocuments: summary["userScholarshipDocuments"],
            scholarshipQuestion: summary["scholarshipQuestions"],
            entranceExams: summary["entranceExams"]
          },
          isCompleted: isCompleted,
          decHtml: innerHtml
        });
        break;
      case "FETCH_SCHOLARSHIP_APPLY_SUCCESS_SUMMARY":
        this.setState({
          status: true,
          msg: "Application Form has been submitted successfully",
          showLoader: true
        });

        setTimeout(() => {
          this.setState({
            isRedirect: true // redirect to application status page
          });
        }, 1000);

        break;
      case "FETCH_SCHOLARSHIP_APPLY_FAILURE_SUMMARY":
        this.setState({
          status: false,
          msg: "Sorry!! Server Error",
          showLoader: true
        });

        break;

      case "FETCH_APPLICATION_STEP_SUCCESS":
        const { applicationStepInstruction } = nextProps;
        if (
          !this.state.historyList.personalInfo ||
          Object.keys(this.state.historyList.personalInfo).length == 0
        ) {
          this.props.getAllSummery({
            scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
            presentClass: this.state.presentClass
          });
        }

        // if (applicationStepInstruction.indexOf("EDUCATION_INFO") == -1) {
        //   this.setState({
        //     msg:
        //       "Please complete your education details, before proceeding further.",
        //     showLoader: true,
        //     status: false,
        //     redirectToEducation: true
        //   });
        // } else {
        //   this.props.getAllSummery({
        //     scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
        //     presentClass: this.state.presentClass
        //   });
        // }

        break;
    }
  }

  isSubmitDisabled(event) {
    if (event.target.checked) {
      this.setState({
        isSubmitDisabled: false
      });
    } else {
      this.setState({
        isSubmitDisabled: true
      });
    }
  }

  submitApplicaitonForm() {
    if (this.state.isSubmitDisabled) {
      this.setState({
        status: false,
        msg: "Kindly agree to the terms and conditions.",
        showLoader: true
      });
      return false;
    } else {
      if (this.state.isCompleted) {
        let scholarshipId = "";
        if (window != undefined) {
          scholarshipId = parseInt(
            window.localStorage.getItem("applicationSchID")
          );
        }
        this.props.fetchSchApply({
          scholarshipId: scholarshipId
        });
        return false;
      } else {
        this.setState({
          status: false,
          msg: "Kindly complete all steps",
          showLoader: true
        });
        return false;
      }
    }
  }

  markup(val) {
    return { __html: val };
  }

  displayQuestionAns(res) {
    if (res.question["questionTypeId"] == 1) {
      return res.response;
    } else if (res.question["questionTypeId"] == 2) {
      if (res.optionId != null && res.optionId.length > 0) {
        let signleOptionAns = res.question["questionOptions"].filter(items => {
          return items["id"] == res.optionId[0];
        });

        if (signleOptionAns != null && signleOptionAns.length > 0) {
          return signleOptionAns[0].optionValue;
        } else {
          return "No Result Found";
        }
      }
    } else {
      if (res.optionId != null && res.optionId.length > 0) {
        let multiList = [];
        let multiOptionAns = res.question["questionOptions"].filter(items => {
          return res.optionId.indexOf(items["id"]) > -1;
        });

        if (multiOptionAns != null && multiOptionAns.length > 0) {
          multiOptionAns.map(items => {
            multiList.push(items.optionValue);
          });
          return multiList.length ? multiList.join(", ") : "";
        } else {
          return "No Result Found";
        }
      }
    }
  }

  render() {
    const { applicationStepInstruction, scholarshipSteps } = this.props;
    let isSubmitArray = [];

    if (applicationStepInstruction && scholarshipSteps) {
      if (
        Array.isArray(applicationStepInstruction) &&
        Array.isArray(scholarshipSteps)
      ) {
        if (
          applicationStepInstruction.length > 0 &&
          scholarshipSteps.length > 0
        ) {
          isSubmitArray = scholarshipSteps.map(schStep => {
            return applicationStepInstruction.indexOf(schStep.step) > -1
              ? true
              : false;
          });
        }
      }
    }

    return (
      <section className="sectionwhite">
        {this.state.isRedirect &&
          gblFunc.getStoreUserDetails()["vleUser"] == "true" ? (
            <Redirect to={`/vle/student-list`} />
          ) : this.state.isRedirect && typeof window != undefined ? (
            (location.href = "https://bit.ly/2CnwxcB")
          ) : null}

        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <article className="form">
          {this.state.historyList["personalInfo"] != null ? (
            <article>
              <article className="row">
                <article className="col-md-12 subheading">
                  {stepNamesToDisplay["PERSONAL_INFO"]}
                </article>
                <article className="col-md-12  table-responsive paddingtable0">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>DOB</th>
                        <th>Annual Family Income (INR)</th>
                        {/*  <th>AadharCard</th> */}
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          {this.state.historyList["personalInfo"].firstName}&nbsp;{
                            this.state.historyList["personalInfo"].lastName
                          }
                        </td>
                        <td>{this.state.historyList["personalInfo"].email}</td>
                        <td>{this.state.historyList["personalInfo"].mobile}</td>
                        <td>{this.state.historyList["personalInfo"].dob}</td>
                        <td>
                          {this.state.historyList["personalInfo"].familyIncome}{" "}
                        </td>
                        {/*    <td>
                          {this.state.historyList["personalInfo"].aadharCard}
                        </td> */}
                      </tr>

                      {this.state.historyList["personalInfo"] == null ? (
                        <tr>
                          <td align="center" colSpan="6">
                            <article className="text-center">
                              No Result Found
                            </article>
                          </td>
                        </tr>
                      ) : (
                          ""
                        )}
                    </tbody>
                  </table>
                </article>
              </article>
              <article className="paddingborder">
                <article className="border">&nbsp;</article>
              </article>
            </article>
          ) : (
              ""
            )}
        </article>
      </section>
    );
  }
}

export default Summary;
