import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import DatePicker from "react-datepicker";
import moment from "moment";
import { ruleRunner } from "../../../../../validation/ruleRunner";
import ValidationError from "../../../../components/validationError";
import OtherTextField from "../../otherTextField";
import {
  APPLICATION_FETCH_PERSONAL_INFO_SUCCEEDED,
  APPLICATION_UPDATE_PERSONAL_INFO_SUCCEEDED,
  FETCH_PERSONAL_INFO_CONFIG_SUCCEEDED
} from "../../../actions/personalInfoActions";
import Loader from "../../../../common/components/loader";
import AlertMessage from "../../../../common/components/alertMsg";
import {
  applicationAPIKeys,
  userAddressFields,
  formNames,
  idValueElement,
  PIUserRules,
  hasChildCall,
  setValidationsInitialState,
  handleConditionalValidations,
  otherDistrictId,
  otherStateId
} from "../../../formconfig";
import gblFunc from "../../../../../globals/globalFunctions";

if (typeof window !== "undefined") {
  require("react-datepicker/dist/react-datepicker.css");
}

class PersonalInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPreferLangCheck: [],
      formData: {
        portalId: 1,
        customData: {
          relationship: { id: "" },
          govt_emp: "",
          preferredLanguageForInterview: ""
        },
        firstName: "",
        lastName: "",
        dob: "",
        email: "",
        familyIncome: "",
        mobile: "",
        userAddress: {
          country: 28,
          id: null,
          type: 1,
          state: { id: "" },
          district: { id: "" },
          addressLine: "",
          city: ""
        },
        userRules: []
      },
      dropDownData: { gender: { id: "" }, orphan: { id: "" } },
      showAlert: false,
      status: false,
      msg: "",
      isCompleted: false,
      validationsObj: {},
      isNext: false
    };
    this.formChangeHandler = this.formChangeHandler.bind(this);
    this.mapServerDataToModel = this.mapServerDataToModel.bind(this);
    this.findRuleValue = this.findRuleValue.bind(this);
    this.updateUserData = this.updateUserData.bind(this);
    this.getIdValue = this.getIdValue.bind(this);
    this.triggerChildCall = this.triggerChildCall.bind(this);
    this.close = this.close.bind(this);
    this.getFieldValue = this.getFieldValue.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.updateUserRulesArray = this.updateUserRulesArray.bind(this);
  }
  componentDidMount() {
    this.props.fetchPersonalInfoConfig({
      scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
      step: applicationAPIKeys.personalInfo
    });
    this.props.fetchApplicationPersonalInfo({
      userId: gblFunc.getStoreUserDetails()["userId"],
      scholarshipId: gblFunc.getStoreApplicationScholarshipId()
    });
  }
  componentWillReceiveProps(nextProps) {
    switch (nextProps.type) {
      case FETCH_PERSONAL_INFO_CONFIG_SUCCEEDED:
        if (Object.keys(this.state.validationsObj).length < 1) {
          this.setState({
            //create validation config and display object....
            validationsObj: setValidationsInitialState(
              nextProps.personalInfoConfig
            )
          });
        }
        break;
      case APPLICATION_FETCH_PERSONAL_INFO_SUCCEEDED:
        !this.state.formData.id
          ? this.mapServerDataToModel(nextProps.personalInfoData)
          : null;
        break;
      case APPLICATION_UPDATE_PERSONAL_INFO_SUCCEEDED:
        if (this.state.isCompleted) {
          this.setState({
            msg: "Submitted Successfully.",
            status: true,
            showAlert: true,
            isCompleted: false,
            isNext: true
          });

          this.props.getCompletedSteps({
            userId: gblFunc.getStoreUserDetails()["userId"],
            scholarshipId: gblFunc.getStoreApplicationScholarshipId()
          });
        }
        break;
      default:
        return;
    }
  }

  triggerChildCall(childCallCase, childValue) {
    switch (childCallCase) {
      case "state": //get district
        this.props.getDistrictsByState({
          KEY: childCallCase,
          DATA: childValue
        });
        break;
      default:
        break;
    }
  }
  close() {
    this.setState({
      showAlert: false,
      msg: "",
      status: false
    });
  }
  mapServerDataToModel(outData) {
    var formData = { ...this.state.formData };
    let isPreferChecked = [...this.state.isPreferLangCheck];
    const ruleValue = this.findRuleValue(
      ["gender", "disabled", "orphan"],
      outData.userRules
    );
    const updatedDropDownData = { ...this.state.dropDownData, ...ruleValue };
    const userAddressMerged = {
      userAddress: {
        ...this.state.formData.userAddress,
        ...outData.userAddress
      }
    };
    userAddressMerged["country"] = 28;
    const {
      firstName,
      lastName,
      familyIncome,
      email,
      mobile,
      dob,
      aadharCard,
      customData,
      userAddress,
      userRules,
      portalId,
      id
    } = {
      ...formData,
      ...outData,
      ...userAddressMerged
    };
    const updatedData = {
      firstName,
      lastName,
      familyIncome,
      email,
      mobile,
      dob,
      aadharCard,
      customData,
      userAddress,
      userRules,
      portalId,
      id
    };
    if (
      userAddress &&
      userAddress.state &&
      userAddress.state.id &&
      (!Array.isArray(this.props.district) || this.props.district.length < 1)
    ) {
      this.triggerChildCall("state", userAddress.state.id);
    }

    if (
      updatedData &&
      updatedData.customData &&
      updatedData.customData.preferredLanguageForInterview
    ) {
      let preferLang =
        updatedData["customData"]["preferredLanguageForInterview"];
      isPreferChecked = preferLang.split(",");
    }

    this.setState({
      isPreferLangCheck: isPreferChecked,
      formData: updatedData,
      dropDownData: updatedDropDownData
    });
  }

  getIdValue(customKey, ev) {
    switch (customKey) {
      case "dob":
        return {
          id: "dob",
          value: moment(ev).format("YYYY-MM-DD")
        };
      case "govt_emp":
        return {
          id: "govt_emp",
          value: ev.target.value
        };
      case "type":
        return {
          id: "type",
          value: ev.target.checked ? 1 : 2
        };
      default:
        return ev.target;
    }
  }
  updateUserRulesArray(value, id, userRules) {
    const ruleIndex = userRules.findIndex(
      elem => elem.ruleTypeId == PIUserRules.get(id)
    );
    let ruleObj = {};
    if (value == 700 || value == 903) {
      ruleObj = {
        ruleId: parseInt(value),
        ruleTypeId: PIUserRules.get(id),
        ruleValue: value == 700 ? "disabled" : "disabled-no",
        ruleTypeValue: "Disabled"
      };
    } else {
      ruleObj = {
        ruleId: parseInt(value),
        ruleTypeId: PIUserRules.get(id)
      };
    }

    ruleIndex > -1 ? (userRules[ruleIndex] = ruleObj) : userRules.push(ruleObj);
  }

  formChangeHandler(event, customKey = false, isCustom = false) {
    let { id, value, nodeName } = this.getIdValue(customKey, event);
    const preferLang = [...this.state.isPreferLangCheck];

    var custom = customKey
      ? isCustom
      : event.target.attributes.getNamedItem("data-custom").value;
    const updateFormData = { ...this.state.formData };
    const customData = { ...updateFormData["customData"] };
    const userAddress = { ...updateFormData["userAddress"] };
    const userRules = [...updateFormData["userRules"]];
    const dropDownData = { ...this.state.dropDownData };
    custom.toString() == "true" && id != "otherPersonalDetails"
      ? nodeName
        ? nodeName.includes(idValueElement)
          ? (customData[id] = { id: value })
          : (customData[id] = value)
        : (customData[id] = value)
      : userAddressFields.indexOf(id) > -1
        ? (userAddress[id] = nodeName
          ? nodeName.includes(idValueElement)
            ? { id: value }
            : value
          : value)
        : (dropDownData[id] = nodeName
          ? nodeName.includes(idValueElement)
            ? { id: value }
            : (updateFormData[id] = value)
          : (updateFormData[id] = value));
    PIUserRules.has(id) && userAddressFields.indexOf(id) < 0
      ? this.updateUserRulesArray(value, id, userRules)
      : false;

    if (id === "otherPersonalDetails") {
      dropDownData["orphan"] = {
        id: value
      };
      //id = "otherPersonalDetails";
    }

    if (id === "preferredLanguageForInterview") {
      if (preferLang.length > 0) {
        if (event.target.checked) {
          preferLang.push(value);
          customData[id] = preferLang.join(",");
        } else {
          preferLang.splice(preferLang.indexOf(value), 1);
          customData[id] = preferLang.join(",");
        }
      } else {
        preferLang.push(value);
        customData[id] = preferLang.join(",");
      }
    }

    updateFormData["customData"] = customData;
    updateFormData["userAddress"] = userAddress;
    updateFormData["userRules"] = userRules;
    /******* Run validation on every field*************** */
    let validationsObj = { ...this.state.validationsObj };

    let conditionBasedId = id === "disabled" ? "physicallyChallenged" : id;

    if (
      validationsObj[conditionBasedId + "Validation"] &&
      validationsObj[conditionBasedId] !== undefined
    ) {
      const validationResult = ruleRunner(
        value,
        conditionBasedId,
        "Field",
        ...validationsObj[conditionBasedId + "Validation"]
      );
      validationsObj[conditionBasedId] = validationResult[conditionBasedId];
    }
    /******* Run validation on every field*************** */

    this.setState(
      {
        formData: updateFormData,
        dropDownData: dropDownData,
        isPreferLangCheck: preferLang,
        validationsObj: validationsObj
      },
      () => {
        hasChildCall.has(id) ? this.triggerChildCall(id, value) : null;
      }
    );
  }

  findRuleValue(rulesArr = [], rulesValArr = []) {
    var ruleValObj = {};
    rulesArr.map(function (rule) {
      let ruleVlaueFiltered = rulesValArr.filter(function (ruleObj) {
        return rule == ruleObj.ruleTypeValue;
      });
      ruleValObj[rule] = {
        id: ruleVlaueFiltered.length ? ruleVlaueFiltered[0]["ruleId"] : ""
      };
    });
    return ruleValObj;
  }
  getFieldValue(field) {
    let correctField =
      this.state.formData["userAddress"][field] ||
      this.state.formData[field] ||
      (field === "physicallyChallenged"
        ? this.state.dropDownData["disabled"]
        : this.state.dropDownData[field]) ||
      (field === "otherPersonalDetails"
        ? this.state.dropDownData["orphan"]
        : this.state.dropDownData[field]) ||
      this.state.formData["customData"][field];
    return typeof correctField !== "undefined"
      ? correctField.hasOwnProperty("id")
        ? correctField["id"]
        : correctField
      : "";
  }
  validateForm() {
    var validationsObj = { ...this.state.validationsObj };
    /************* start Update conditional validations.........****/

    if (
      !this.state.formData.customData.govt_emp ||
      (this.state.formData.customData.govt_emp &&
        this.state.formData.customData.govt_emp == "no")
    ) {
      validationsObj = handleConditionalValidations(
        !this.state.formData.customData.govt_emp
          ? "no"
          : this.state.formData.customData.govt_emp,
        "no",
        null,
        ["designation", "organization_name", "relationship"],
        validationsObj
      );
    }
    if (
      this.state.formData.userAddress.state &&
      this.state.formData.userAddress.state.id
    ) {
      validationsObj = handleConditionalValidations(
        this.state.formData.userAddress.state.id,
        otherStateId,
        "notEqual",
        ["otherState", "otherDistrict"],
        validationsObj
      );
    }

    /************* start Update conditional validations.........****/

    let isFormValid = true;
    for (let field in validationsObj) {
      if (
        !field.includes("Validation") &&
        validationsObj[field] !== undefined &&
        validationsObj[field + "Validation"]
      ) {
        let isPhysicalChallenged =
          field === "disabled" ? "physicallyChallenged" : field;
        //don't check for valdation keys...
        const validationResult = ruleRunner(
          this.getFieldValue(field),
          isPhysicalChallenged,
          "Field",
          ...validationsObj[isPhysicalChallenged + "Validation"]
        );
        validationsObj[isPhysicalChallenged] =
          validationResult[isPhysicalChallenged];
        if (validationsObj[isPhysicalChallenged] !== null) {
          isFormValid = false;
        }
      }
    }
    this.setState({
      validationsObj,
      isFormValid
    });
    return isFormValid;
  }
  updateUserData() {
    if (this.validateForm()) {
      const updateCountry = { ...this.state.formData };
      const updatedCheckedPreferLang = [...this.state.isPreferLangCheck];

      updateCountry.userAddress["country"] = 28;
      updateCountry.customData.preferredLanguageForInterview = updatedCheckedPreferLang.join(
        ","
      );
      this.props.updatePersonalInfo({
        formData: updateCountry,
        userId: gblFunc.getStoreUserDetails()["userId"],
        step: formNames["personalInfo"],
        scholarshipId: gblFunc.getStoreApplicationScholarshipId()
      });
      this.setState({ isCompleted: true });
    }
  }

  render() {
    const { personalInfoData } = this.props
    const formConfig = this.props.personalInfoConfig || null;
    if (formConfig) {
      let physicallyChallenged = ""; // get physical chalange value
      let otherPersonalDetails = "";
      if (
        this.state.formData.userRules != null &&
        this.state.formData.userRules.length > 0
      ) {
        physicallyChallenged = this.state.formData.userRules.filter(res => {
          return res.ruleId == 700 || res.ruleId == 903;
        });
      }

      // const formConfig = personalInfoConfig;
      const genderOptions = formConfig.gender.dataOptions
        ? formConfig.gender.dataOptions
        : this.props.allRules.rulesData
          ? this.props.allRules.rulesData.gender
          : []; //from rules call or from application config
      const preferredLanguageOption =
        formConfig.preferredLanguageForInterview &&
          formConfig.preferredLanguageForInterview.dataOptions
          ? formConfig.preferredLanguageForInterview.dataOptions
          : [];
      //   ? formConfig.preferredLanguage.dataOptions
      //   : this.props.allRules.rulesData
      //     ? this.props.allRules.rulesData.preferredLanguage
      //     : []; //from rules call or from application config
      const districtOptions = this.props.district ? this.props.district : []; //from district call result
      const stateOptions = formConfig.state.dataOptions
        ? formConfig.state.dataOptions
        : this.props.allRules.rulesData
          ? this.props.allRules.rulesData.state
          : []; //from rules call or from application config
      const relationOptions = formConfig.relationship.dataOptions
        ? formConfig.relationship.dataOptions
        : []; //from rules call or from application config
      const otherPersonalDts =
        formConfig.otherPersonalDetails &&
          formConfig.otherPersonalDetails.dataOptions
          ? formConfig.otherPersonalDetails.dataOptions
          : [];
      return (
        <section className="sectionwhite">
          {this.state.isNext ? (
            <Redirect
              to={`/application/${this.props.match.params.bsid.toUpperCase()}/form/educationInfo`}
            />
          ) : (
              ""
            )}
          <Loader isLoader={this.props.showLoader} />
          <AlertMessage
            close={this.close.bind(this)}
            isShow={this.state.showAlert}
            status={this.state.status}
            msg={this.state.msg}
          />
          <article className="form">
            <article className="paddingborder">&nbsp;</article>
            <article className="row">
              <article
                className={`${
                  formConfig.firstName.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    id="firstName"
                    data-custom={formConfig.firstName.custom}
                    value={this.state.formData.firstName}
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label htmlFor="firstName">{`${
                    formConfig.firstName.label
                    }`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["firstName"]}
                  />
                </article>
              </article>
              {/* <article
              className={`${
                formConfig.employeeId
                  ? formConfig.employeeId.active
                    ? "show"
                    : "hide"
                  : "hide"
              } col-md-4`}
            >
              <input
                type="text"
                placeholder={`${formConfig.employeeId.label}`}
                id="employeeId"
                data-custom={formConfig.employeeId.custom}
                value={this.state.formData.customData.employeeId}
                onChange={this.formChangeHandler}
              />
              <ValidationError
                fieldValidation={this.state.validationsObj["employeeId"]}
              />
            </article> */}
              <article
                className={`${
                  formConfig.lastName.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    id="lastName"
                    data-custom={formConfig.lastName.custom}
                    value={this.state.formData.lastName}
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.lastName.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["lastName"]}
                  />
                </article>
              </article>
              <article className="col-md-4">
                <article className="form-group">
                  <input
                    type="text"
                    id="email"
                    data-custom={formConfig.email.custom}
                    value={this.state.formData.email}
                    onChange={this.formChangeHandler}
                    disabled
                  />
                  <label>{`${formConfig.email.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["email"]}
                  />
                </article>
              </article>
              <article className="col-md-4">
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={formConfig.mobile.custom}
                    id="mobile"
                    maxLength="10"
                    value={this.state.formData.mobile}
                    onChange={this.formChangeHandler}
                    disabled={personalInfoData && personalInfoData.mobile ? true : false}
                    required
                  />
                  <label>{`${formConfig.mobile.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["mobile"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.dob.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <DatePicker
                    showYearDropdown
                    scrollableYearDropdown
                    readOnly
                    yearDropdownItemNumber={40}
                    minDate={moment().subtract(100, "years")}
                    maxDate={moment().add(1, "years")}
                    name="dob"
                    id="dob"
                    // placeholder={`${formConfig.dob.label}`}
                    data-custom={formConfig.dob.custom}
                    className="icon-date"
                    autoComplete="off"
                    selected={
                      this.state.formData.dob
                        ? moment(
                          moment(this.state.formData.dob).format(
                            "DD-MM-YYYY"
                          ),
                          "DD-MM-YYYY"
                        )
                        : null
                    }
                    onChange={event =>
                      this.formChangeHandler(
                        event,
                        "dob",
                        formConfig.dob.custom
                      )
                    }
                    dateFormat="DD-MM-YYYY"
                  />
                  <label className="labelstyle">{`${
                    formConfig.dob.label
                    }`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["dob"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.gender.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <select
                    className="icon"
                    id="gender"
                    // placeholder={`${formConfig.gender.label}`}
                    value={this.state.dropDownData.gender.id}
                    data-custom={formConfig.gender.custom}
                    onChange={this.formChangeHandler}
                  >
                    <option value="">-Select-</option>
                    {genderOptions.map(genderItem => {
                      return (
                        <option value={`${genderItem.id}`}>
                          {genderItem.rulevalue}
                        </option>
                      );
                    })}
                  </select>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["gender"]}
                  />
                  <label className="labelstyle">{`${
                    formConfig.gender.label
                    }`}</label>
                </article>
              </article>
              <article
                className={`${
                  formConfig.aadharCard.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={formConfig.aadharCard.custom}
                    value={this.state.formData.aadharCard}
                    id="aadharCard"
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.aadharCard.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["aadharCard"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.familyIncome.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    maxLength="10"
                    data-custom={formConfig.familyIncome.custom}
                    value={this.state.formData.familyIncome}
                    id="familyIncome"
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.familyIncome.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["familyIncome"]}
                  />
                </article>
              </article>
              {formConfig.physicallyChallenged != null ? (
                <article
                  className={`${
                    formConfig.physicallyChallenged.active ? "show" : "hide"
                    } col-md-4`}
                >
                  <article className="form-group">
                    <select
                      className="icon"
                      id="disabled"
                      // placeholder={`${formConfig.gender.label}`}
                      /*  value={this.state.dropDownData.physicallyChallenged.id} */
                      data-custom={formConfig.physicallyChallenged.custom}
                      onChange={this.formChangeHandler}
                      value={
                        physicallyChallenged[0]
                          ? physicallyChallenged[0].ruleId
                          : "" || ""
                      }
                    >
                      <option value="">--Select--</option>

                      <option value="700">Yes</option>
                      <option value="903">No</option>
                    </select>
                    {
                      <ValidationError
                        fieldValidation={
                          this.state.validationsObj["physicallyChallenged"]
                        }
                      />
                    }
                    <label className="labelstyle">{`${
                      formConfig.physicallyChallenged.label
                      }`}</label>
                  </article>
                </article>
              ) : (
                  ""
                )}
              {formConfig.otherPersonalDetails != null ? (
                <article
                  className={`${
                    formConfig.otherPersonalDetails.active ? "show" : "hide"
                    } col-md-4`}
                >
                  <article className="form-group">
                    <select
                      className="icon"
                      id="otherPersonalDetails"
                      // placeholder={`${formConfig.gender.label}`}
                      value={this.state.dropDownData.orphan.id}
                      data-custom={formConfig.otherPersonalDetails.custom}
                      onChange={event =>
                        this.formChangeHandler(
                          event,
                          true,
                          formConfig.otherPersonalDetails.custom
                        )
                      }
                    //   value={
                    //     otherPersonalDetails[0]
                    //       ? otherPersonalDetails[0].ruleId
                    //       : "" || ""
                    //   }
                    >
                      <option value="">--Select--</option>

                      {otherPersonalDts.map(otherPerDT => {
                        return (
                          <option value={`${otherPerDT.id}`}>
                            {otherPerDT.rulevalue}
                          </option>
                        );
                      })}
                    </select>
                    {
                      <ValidationError
                        fieldValidation={
                          this.state.validationsObj["otherPersonalDetails"]
                        }
                      />
                    }
                    <label className="labelstyle">{`${
                      formConfig.otherPersonalDetails.label
                      }`}</label>
                  </article>
                </article>
              ) : (
                  ""
                )}

              {formConfig.preferredLanguageForInterview != null ? (
                <article
                  className={`${
                    formConfig.preferredLanguageForInterview.active
                      ? "show"
                      : "hide"
                    } col-md-4`}
                >
                  <article className="form-group preferredLanguageForInterview">
                    <label>{`${
                      formConfig.preferredLanguageForInterview.label
                      }`}</label>
                    {preferredLanguageOption.map(list => {
                      return (
                        <article className="radionewgp">
                          <label>{list.rulevalue}</label>
                          <input
                            type="checkbox"
                            data-custom={
                              formConfig.preferredLanguageForInterview.custom
                            }
                            id="preferredLanguageForInterview"
                            value={list.id}
                            checked={
                              this.state.isPreferLangCheck.indexOf(list.id) > -1
                            }
                            onChange={event =>
                              this.formChangeHandler(
                                event,
                                true,
                                formConfig.preferredLanguageForInterview.custom
                              )
                            }
                          />
                        </article>
                      );
                    })}

                    <ValidationError
                      fieldValidation={
                        this.state.validationsObj[
                        "preferredLanguageForInterview"
                        ]
                      }
                    />
                  </article>
                </article>
              ) : (
                  ""
                )}
              {/* <article
              className={`${
                formConfig.preferredLanguage.active ? "show" : "hide"
              } col-md-4`}
            >
              <select
                className="icon"
                id="preferredLanguage"
                value={this.state.dropDownData.preferredLanguage.id}
                data-custom={formConfig.preferredLanguage.custom}
                onChange={this.formChangeHandler}
              >
                {prefLanguaguesOptions.map(languageItem => {
                  return (
                    <option value={`${languageItem.id}`}>
                      {languageItem.value}
                    </option>
                  );
                })}
              </select>
            </article> */}
            </article>

            <article className="row">
              <article className="col-md-12 address">
                Present address
                <article className="paddingborder">
                  <article className="border">&nbsp;</article>
                </article>
                <span className={`${formConfig.type.active ? "show" : "hide"}`}>
                  <label>
                    <input
                      type="checkbox"
                      data-custom={formConfig.type.custom}
                      value={this.state.formData.userAddress.type}
                      checked={this.state.formData.userAddress.type}
                      id="type"
                      onChange={event =>
                        this.formChangeHandler(
                          event,
                          "type",
                          formConfig.type.custom
                        )
                      }
                    />
                    <span />
                    {`${formConfig.type.label}`}
                  </label>
                </span>
              </article>
            </article>

            <article className="row">
              <article
                className={`${
                  formConfig.addressLine.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={formConfig.addressLine.custom}
                    value={this.state.formData.userAddress.addressLine}
                    id="addressLine"
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.addressLine.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["addressLine"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.state.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <select
                    className="icon"
                    id="state"
                    value={
                      this.state.formData.userAddress.state
                        ? this.state.formData.userAddress.state.id
                        : ""
                    }
                    data-custom={formConfig.state.custom}
                    onChange={this.formChangeHandler}
                  >
                    <option value="">-Select-</option>
                    {stateOptions.map(stateItem => {
                      return (
                        <option value={`${stateItem.id}`}>
                          {stateItem.rulevalue}
                        </option>
                      );
                    })}
                  </select>
                  <label className="labelstyle">{formConfig.state.label}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["state"]}
                  />
                </article>
              </article>
              {this.state.formData.userAddress.state &&
                this.state.formData.userAddress.state.id == otherStateId ? (
                  <OtherTextField
                    custom={formConfig.otherState.custom}
                    fieldValue={this.state.formData.userAddress.otherState}
                    fieldId="otherState"
                    clickHandler={this.formChangeHandler}
                    fieldLabel={formConfig.otherState.label}
                    fieldValidation={this.state.validationsObj["otherState"]}
                  />
                ) : null}

              <article
                className={`${
                  formConfig.district.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <select
                    className="icon"
                    id="district"
                    value={
                      this.state.formData.userAddress.district
                        ? this.state.formData.userAddress.district.id
                        : ""
                    }
                    data-custom={formConfig.district.custom}
                    onChange={this.formChangeHandler}
                  >
                    <option value="">-Select-</option>
                    {districtOptions.map(districtItem => {
                      return (
                        <option value={`${districtItem.id}`}>
                          {districtItem.districtName}
                        </option>
                      );
                    })}
                  </select>
                  <label className="labelstyle">
                    {formConfig.district.label}
                  </label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["district"]}
                  />
                </article>
              </article>
              {this.state.formData.userAddress.district &&
                this.state.formData.userAddress.district.id == otherDistrictId ? (
                  <OtherTextField
                    custom={formConfig.otherDistrict.custom}
                    fieldValue={this.state.formData.userAddress.otherDistrict}
                    fieldId="otherDistrict"
                    clickHandler={this.formChangeHandler}
                    fieldLabel={formConfig.otherDistrict.label}
                    fieldValidation={this.state.validationsObj["otherDistrict"]}
                  />
                ) : null}
              <article
                className={`${
                  formConfig.city.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={formConfig.city.custom}
                    value={this.state.formData.userAddress.city}
                    id="city"
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.city.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["city"]}
                  />
                </article>
              </article>
              <article
                className={`${
                  formConfig.pincode.active ? "show" : "hide"
                  } col-md-4`}
              >
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={formConfig.pincode.custom}
                    value={this.state.formData.userAddress.pincode}
                    id="pincode"
                    maxLength="6"
                    onChange={this.formChangeHandler}
                    required
                  />
                  <label>{`${formConfig.pincode.label}`}</label>
                  <ValidationError
                    fieldValidation={this.state.validationsObj["pincode"]}
                  />
                </article>
              </article>

              <article
                className={`${
                  formConfig.govt_emp.active ? "show" : "hide"
                  } col-md-6 col-xs-12 col-sm-12 form-group `}
              >
                <article
                  className="radio-btn"
                  data-custom={formConfig.govt_emp.custom}
                  id="govt_emp"
                  onChange={event =>
                    this.formChangeHandler(
                      event,
                      "govt_emp",
                      formConfig.govt_emp.custom
                    )
                  }
                >
                  <h3>{formConfig.govt_emp.label}</h3>
                  <span>
                    <label>
                      <input
                        type="radio"
                        name="radio"
                        checked={
                          this.state.formData.customData.govt_emp == "yes"
                        }
                        value="yes"
                      />
                      <span />
                      &nbsp;<i>Yes</i>
                    </label>
                  </span>
                  <span className="paddingborder">&nbsp;</span>
                  <span className="marginleft">
                    <label>
                      <input
                        type="radio"
                        value="no"
                        checked={
                          this.state.formData.customData.govt_emp == "no"
                        }
                        name="radio"
                      />
                      <span />
                      &nbsp;<i>No</i>
                    </label>
                  </span>
                </article>
                <ValidationError
                  fieldValidation={this.state.validationsObj["govt_emp"]}
                />
              </article>
              {this.state.formData.customData.govt_emp == "yes" ? (
                <div className="clearboth">
                  <article
                    className={`${
                      formConfig.relationship.active ? "show" : "hide"
                      } col-md-4`}
                  >
                    <article className="form-group">
                      <select
                        className="icon"
                        id="relationship"
                        value={
                          this.state.formData.customData.relationship
                            ? this.state.formData.customData.relationship.id
                            : ""
                        }
                        data-custom={formConfig.relationship.custom}
                        onChange={this.formChangeHandler}
                      >
                        <option value="">-Select-</option>
                        {relationOptions.map(rItem => {
                          return (
                            <option value={rItem.id}>{rItem.rulevalue}</option>
                          );
                        })}
                      </select>
                      <label className="labelstyle">
                        {formConfig.relationship.label}
                      </label>
                      <ValidationError
                        fieldValidation={
                          this.state.validationsObj["relationship"]
                        }
                      />
                    </article>
                  </article>
                  <article
                    className={`${
                      formConfig.designation.active ? "show" : "hide"
                      } col-md-4`}
                  >
                    <article className="form-group">
                      <input
                        type="text"
                        required
                        id="designation"
                        value={this.state.formData.customData.designation}
                        data-custom={formConfig.designation.custom}
                        onChange={this.formChangeHandler}
                      />
                      <label>{formConfig.designation.label}</label>
                      <ValidationError
                        fieldValidation={
                          this.state.validationsObj["designation"]
                        }
                      />
                    </article>
                  </article>
                  <article
                    className={`${
                      formConfig.organization_name.active ? "show" : "hide"
                      } col-md-4`}
                  >
                    <article className="form-group">
                      <input
                        type="text"
                        id="organization_name"
                        required
                        value={this.state.formData.customData.organization_name}
                        data-custom={formConfig.organization_name.custom}
                        onChange={this.formChangeHandler}
                      />
                      <label>{formConfig.organization_name.label}</label>
                      <ValidationError
                        fieldValidation={
                          this.state.validationsObj["organization_name"]
                        }
                      />
                    </article>
                  </article>
                  <article className="row">
                    <article className="col-md-12 notetext">
                      Note: If you have more than one family member as a
                      government employee. Kindly mention their occupation in
                      the family member section.
                    </article>
                  </article>
                </div>
              ) : null}
            </article>

            <article className="row">
              <article className="col-md-12">
                <input
                  type="button"
                  value="Save & Continue"
                  className="btn  pull-right"
                  onClick={this.updateUserData}
                />
              </article>
            </article>
          </article>
        </section>
      );
    } else {
      return <p>Sorry! No Data Available.</p>;
    }
  }
}

export default PersonalInfo;
