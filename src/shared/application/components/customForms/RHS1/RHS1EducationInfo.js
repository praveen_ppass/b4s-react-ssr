import React, { Component } from "react";
import GraduationForm from "../../standardForms/sub-forms/graduation-form";
import Class12 from "../../standardForms/sub-forms/class12-form";
import Class10 from "../../standardForms/sub-forms/class10-form";
import getNestedObjKey from "pushpendra-find-nested-obj-key";
import gblFunc from "../../../../../globals/globalFunctions";
import {
  handleConditionalValidations,
  otherStateId,
  graduationId,
  otherStreamIdGraduation,
  otherStreamIdTwelfth,
  nextStepName,
  config,
  educationSpecialValidations,
  addSpecialValidations,
  elvenId,
  twelthId,
  classTenthId,
  otherBoardId,
  otherStreamIdEleven,
  otherStreamIdPostGraduation,
  postGradId,
  classNineId
} from "../../../formconfig";
import {
  FETCH_EDUCATION_INFO_FORM_SUCCESS,
  UPDATE_EDUCATION_INFO_FORM_SUCCESS,
  UPDATE_EDUCATION_INFO_FORM_FAILURE,
  FETCH_EDUCATION_INFO_CONFIG_REQUEST,
  FETCH_EDUCATION_INFO_CONFIG_SUCCESS,
  UPDATE_EDUCATION_INFO_STEP_SUCCESS,
  UPDATE_EDUCATION_INFO_STEP_FAILURE
} from "../../../actions/educationInfoAction";
import AcadmicClasses from "./RHS1AcademicClass";
import Loader from "../../../../common/components/loader";
import AlertMessage from "../../../../common/components/alertMsg";
import { mapValidationFunc } from "../../../../../validation/rules";
import { ruleRunner } from "../../../../../validation/ruleRunner";
import { FETCH_SCHOLARSHIP_APPLY_SUCCESS } from "../../../../../constants/commonActions";
import { FETCH_APPLICATION_STEP_SUCCESS } from "../../../actions/applicationFormAction";
import { getYearOrMonth } from "../../../../../constants/constants";

class EducationInfo extends Component {
  constructor(props) {
    super(props);

    this.editEduction = this.editEduction.bind(this);
    this.onSelectClassHandler = this.onSelectClassHandler.bind(this);
    this.educationFormHandler = this.educationFormHandler.bind(this);
    this.marksObtainedHandler = this.marksObtainedHandler.bind(this);
    this.onEducationSubmitHandler = this.onEducationSubmitHandler.bind(this);
    this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
    this.checkFormValidations = this.checkFormValidations.bind(this);
    this.fireEducationConfigCall = this.fireEducationConfigCall.bind(this);
    this.goToNextStep = this.goToNextStep.bind(this);
    this.ifAllClassesFilled = this.ifAllClassesFilled.bind(this);
    this.ifClassDetailsAreCompleted = this.ifClassDetailsAreCompleted.bind(
      this
    );

    this.state = {
      childVisible: true,
      applicationEducationData: null,
      presentClass: {},
      classID: null,
      marksOrCgpa: true,
      presentClassID: "",
      isEditable: false,
      showLoader: false,
      isFormValid: true,
      msg: "",
      status: false,
      stepUpdated: false,
      isStepCompleted: false,
      configFieldsData: {},
      validations: {},
      eduState: {
        customData: {},
        userAcademicInfo: {
          academicClass: {
            id: "",
            value: ""
          },
          academicClassName: "",
          board: "",
          boardName: "",
          courseDuration: "",
          currentDegreeYear: "",
          degree: "",
          fee: "",
          grade: "",
          id: "",
          markingType: "1",
          marksObtained: "",
          otherBoard: "",
          passingMonth: "",
          passingYear: "",
          percentage: "",
          presentClass: 0,
          stream: "",
          totalMarks: ""
        },
        userInstituteInfo: {
          academicDetailId: "",
          address: "",
          city: "",
          country: "",
          description: "",
          district: "",
          districtName: "",
          id: "",
          instituteEmail: "",
          instituteName: "",
          institutePhone: "",
          pincode: "",
          principalName: "",
          state: "",
          stateName: ""
        }
      },
      defaultEduState: {
        customData: {},
        userAcademicInfo: {
          academicClass: {
            id: "",
            value: ""
          },
          academicClassName: "",
          board: "",
          boardName: "",
          courseDuration: "",
          currentDegreeYear: "",
          degree: "",
          fee: "",
          grade: "",
          id: "",
          markingType: "1",
          marksObtained: "",
          otherBoard: "",
          passingMonth: "",
          passingYear: "",
          percentage: "",
          presentClass: 0,
          stream: "",
          totalMarks: ""
        },
        userInstituteInfo: {
          academicDetailId: "",
          address: "",
          city: "",
          country: "",
          description: "",
          district: "",
          districtName: "",
          id: "",
          instituteEmail: "",
          instituteName: "",
          institutePhone: "",
          pincode: "",
          principalName: "",
          state: "",
          stateName: ""
        }
      }
    };
  }
  onClick() {
    this.setState(prevState => ({
      childVisible: !prevState.childVisible
    }));
  }

  componentDidMount() {
    this.props.fetchEduInfo({
      // scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
      scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
      step: "EDUCATION_INFO"
    });
    if (this.props.schApply) {
      //If educatrion tab is not opened directly........@Pushpendra
      this.fireEducationConfigCall(this.props);
    }
  }

  fireEducationConfigCall(nextProps) {
    this.props.getEducationConfig({
      scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
      step: "EDUCATION_INFO",
      scholarshipType: nextProps.schApply
        ? nextProps.schApply.scholarshipType
        : ""
    });
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps;
    switch (type) {
      case FETCH_EDUCATION_INFO_FORM_SUCCESS:
        const { applicationEducationData } = nextProps;

        this.setState({
          applicationEducationData
        });
        break;
      case FETCH_SCHOLARSHIP_APPLY_SUCCESS: //If education tab is opened directly.....@Pushpendra
        this.fireEducationConfigCall(nextProps);
        break;

      case FETCH_EDUCATION_INFO_CONFIG_SUCCESS:
        const { educationConfigInfo } = nextProps;
        const validate = { ...this.state.validations };
        const configFieldsData = { ...this.state.configFieldsData };
        if (
          educationConfigInfo &&
          Object.keys(educationConfigInfo).length > 0
        ) {
          for (let key in educationConfigInfo) {
            if (
              educationConfigInfo[key].validations !== null &&
              educationConfigInfo[key].active
            ) {
              let clearUnderScore = gblFunc.replace_underScore(key);
              validate[clearUnderScore] = null;
              configFieldsData[clearUnderScore] = educationConfigInfo[key];
            }
          }
        }
        this.setState({
          validations: validate,
          configFieldsData
        });
        break;
      case UPDATE_EDUCATION_INFO_FORM_SUCCESS:
        this.setState({
          status: true,
          msg: "Successfully Submitted",
          showLoader: true,
          eduState: { ...this.state.defaultEduState },
          isEditable: false,
          isStepCompleted: this.ifAllClassesFilled(true), //form submitted
          classID: ""
        });

        this.props.fetchEduInfo({
          // scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
          scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
          step: "EDUCATION_INFO"
        });
        break;
      case UPDATE_EDUCATION_INFO_FORM_FAILURE:
        let errCode = getNestedObjKey(nextProps.payload, ["response", "data", "errorCode"])
        let errMsg = getNestedObjKey(nextProps.payload, ["response", "data", "message"])
        if (errCode == 2001) {
          this.setState({
            status: false,
            msg: errMsg,
            showLoader: true,
            isEditable: this.state.isEditable ? true : false
          });
        } else {
          this.setState({
            status: false,
            msg: "Something went wrong, please try again!",
            showLoader: true,
            isEditable: this.state.isEditable ? true : false
          });
        }
        break;
      case UPDATE_EDUCATION_INFO_STEP_SUCCESS:
        if (this.state.stepUpdated) {
          this.setState({ stepUpdated: false }, () => {
            this.props.getCompletedSteps({
              userId: gblFunc.getStoreUserDetails()["userId"],
              scholarshipId: gblFunc.getStoreApplicationScholarshipId()
            });

            setTimeout(() => {
              this.props.history.push(
                `/application/${this.props.match.params.bsid.toUpperCase()}/form/familyInfo`
              );
            }, 2000);
          });
        }
        break;
      case UPDATE_EDUCATION_INFO_STEP_FAILURE:
        let errStepCode = getNestedObjKey(nextProps.error, ["response", "data", "errorCode"])
        let errStepMsg = getNestedObjKey(nextProps.error, ["response", "data", "message"])
        if (errStepCode == 2001) {
          this.setState({
            status: false,
            msg: errStepMsg,
            showLoader: true,
          });
        } else {
          this.setState({
            status: false,
            msg: "Something went wrong, please try again!",
            showLoader: true,
          });
        }
        break
      default:
        break;
    }
  }

  ifAllClassesFilled(formSubmitted = 0) {
    //Called on form submission flag
    const isStepCompleted =
      this.state.configFieldsData.academicClass &&
        this.state.applicationEducationData
        ? this.state.configFieldsData.academicClass.dataOptions &&
          this.state.applicationEducationData.userEducationList
          ? this.state.applicationEducationData.userEducationList.length +
            formSubmitted
            ? 1
            : 0
              ? true
              : false
          : false
        : false;
    return isStepCompleted;
  }

  getDistrictHandler(event) {
    const { value } = event.target;
    const { userInstituteInfo } = { ...this.state.eduState };

    userInstituteInfo.state = value;

    this.educationFormHandler(event, "userInstituteInfo", "state");
    this.setState(
      {
        eduState: {
          ...this.state.eduState,
          userInstituteInfo: { ...userInstituteInfo }
        }
      },
      () => {
        this.props.fetchDistrictList({ id: value });
      }
    );
  }

  onSelectClassHandler(event) {
    const { value, disabled } = event.target;
    const { applicationEducationData } = this.state;

    if (!disabled) {
      if (
        applicationEducationData &&
        Object.keys(applicationEducationData).length > 0 &&
        applicationEducationData.userEducationList &&
        applicationEducationData.userEducationList.length > 0
      ) {
        const eduLists = applicationEducationData.userEducationList;

        this.editEduction(eduLists[0]);
        return;
      }
    }

    let eduState = { ...this.state.eduState };
    let defaultEduState = { ...this.state.defaultEduState };
    const updatePresentClass = { ...this.state.presentClass };

    const { validations } = this.state;

    if (value == 11) {
      eduState.userAcademicInfo.markingType = 1;
      defaultEduState.userAcademicInfo.markingType = 1;
    } else if (value == 16) {
      eduState.userAcademicInfo.markingType = 2;
      defaultEduState.userAcademicInfo.markingType = 2;
    } else if (value == 12) {
      eduState.userAcademicInfo.markingType = 2;
      defaultEduState.userAcademicInfo.markingType = 2;
    } else {
      eduState.userAcademicInfo.markingType = 1;
      defaultEduState.userAcademicInfo.markingType = 1;
    }
    let index =
      event.nativeEvent && event.nativeEvent.target
        ? event.nativeEvent.target.selectedIndex
        : "";

    let e = {
      target: {
        id: value,
        value: event.nativeEvent.target[index].text
      }
    };

    const presentClassId = event.nativeEvent.target[index].getAttribute(
      "data-presentclass"
    );

    if (presentClassId == 1) {
      (updatePresentClass["id"] = value),
        (updatePresentClass["presentClassID"] = presentClassId);
    } else {
      (updatePresentClass["id"] = value),
        (updatePresentClass["presentClassID"] = presentClassId);
    }
    // eduState.userAcademicInfo.presentClass = presentClassId;
    // defaultEduState.userAcademicInfo.presentClass = presentClassId;

    for (let key in validations) {
      validations[key] =
        presentClassId == 1 &&
          ["marksObtained", "cgpa", "totalMarks", "grade"].includes(key)
          ? undefined
          : null;
    }

    this.setState(
      {
        classID: value,
        isEditable: false,
        validations,
        presentClass: updatePresentClass,
        marksOrCgpa:
          eduState.userAcademicInfo.markingType == 1 ||
            defaultEduState.userAcademicInfo.markingType == 1
            ? true
            : false,
        eduState: this.state.isEditable
          ? { ...defaultEduState }
          : { ...eduState }
      },
      () => this.props.getSubject({ id: this.state.classID })
    );

    this.educationFormHandler(e, "userAcademicInfo", "academicClass");
  }

  marksObtainedHandler(event) {
    const { value } = event.target;

    const { userAcademicInfo } = { ...this.state.eduState };
    if (value && value == "1") {
      userAcademicInfo.marksObtained = "";
      userAcademicInfo.totalMarks = "";
    } else {
      userAcademicInfo.grade = "";
      userAcademicInfo.marksObtained = "";
      userAcademicInfo.totalMarks = 10;
    }
    userAcademicInfo.markingType = value;
    this.setState({
      eduState: { ...this.state.eduState, ...userAcademicInfo },
      marksOrCgpa: value === "1" ? true : false
    });
  }

  editEduction(eduList) {
    const { validations } = this.state;
    const updpateThePresentClass = { ...this.state.presentClass };

    for (let key in validations) {
      validations[key] =
        eduList.userAcademicInfo.presentClass == 1 &&
          ["marksObtained", "cgpa", "totalMarks", "grade"].includes(key)
          ? undefined
          : null;
    }

    if (eduList.userAcademicInfo.presentClass == 1) {
      updpateThePresentClass.id = eduList.userAcademicInfo.academicClass.id;
      updpateThePresentClass.presentClassID = 1;
    } else {
      updpateThePresentClass.id = eduList.userAcademicInfo.academicClass.id;
      updpateThePresentClass.presentClassID = 0;
    }
    const updateEduList = {
      ...eduList
    };
    // const { userInstituteInfo } = eduList;
    this.props.getSubject({ id: eduList.userAcademicInfo.academicClass.id });
    this.setState(
      {
        eduState: updateEduList,
        isEditable: true,
        validations: validations,
        presentClass: updpateThePresentClass,
        marksOrCgpa:
          updateEduList.userAcademicInfo.markingType == "1" ? true : false,
        classID: updateEduList.userAcademicInfo.academicClass.id
      },
      () => {
        if (updateEduList && updateEduList.userInstituteInfo.state) {
          this.props.fetchDistrictList({
            id: updateEduList.userInstituteInfo.state
          });
        }
      }
    );
  }

  educationFormHandler(event, type, subType, validationName) {
    // event.preventDefault();
    const { value, id } = event.target;
    let validations = { ...this.state.validations };

    const { customData, userAcademicInfo, userInstituteInfo } = {
      ...this.state.eduState
    };

    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(
        id,
        validationName
      );
      if (validationFunctions) {
        const validationResult = ruleRunner(
          value,
          id,
          name,
          ...validationFunctions
        );
        validations[id] = validationResult[id];
      }
    }

    // const updateFormData = { ...this.state.formData };
    // updateFormData[id] = value;
    // this.setState({
    //   formData: updateFormData,
    //   validations
    // });

    switch (type) {
      case "userAcademicInfo":
        if (subType && subType == "academicClass") {
          userAcademicInfo.academicClass = {
            id,
            value
          };
        } else {
          userAcademicInfo[subType] = value;
        }
        break;
      case "userInstituteInfo":
        userInstituteInfo[subType] = value;
        break;
      case "customInfo":
        customData[subType] = value;
        break;
      default:
        break;
    }

    this.setState({
      eduState: {
        ...this.state.eduState,
        custom: { ...customData },
        userAcademicInfo: { ...userAcademicInfo },
        userInstituteInfo: { ...userInstituteInfo }
      },
      validations
    });
  }

  getValidationRulesObject(fieldID, name = "*Field") {
    let validationObject = {};
    let validationRules = [];
    if (this.state.validations.hasOwnProperty(fieldID)) {
      let configFields = this.state.configFieldsData[fieldID];
      if (configFields && configFields.validations != null) {
        let validationArr = configFields.validations;
        if (educationSpecialValidations.indexOf(fieldID) > -1) {
          addSpecialValidations(validationArr, fieldID, [
            this.state.eduState.userAcademicInfo
          ]);
        }
        validationArr.map(res => {
          if (res != null) {
            let validator = mapValidationFunc(res);
            if (validator != undefined) {
              validationRules.push(validator);
            }
          }
        });
        validationObject.name = name;
        validationObject.validationFunctions = validationRules;
      }
    }
    return validationObject;
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };

    for (let key in validations) {
      validations[key] =
        this.state.presentClass &&
          this.state.presentClass.presentClassID == 1 &&
          ["marksObtained", "cgpa", "totalMarks", "grade"].includes(key)
          ? undefined
          : null;
    }
    /************* start Update conditional validations.........****/
    if (this.state.marksOrCgpa) {
      validations = handleConditionalValidations(
        this.state.marksOrCgpa,
        true,
        null,
        ["grade", "cgpa"],
        validations
      );
    } else {
      validations = handleConditionalValidations(
        this.state.marksOrCgpa,
        false,
        null,
        ["totalMarks", "cgpa"],
        validations
      );
    }
    if (this.state.classID == elvenId) {
      validations = handleConditionalValidations(
        this.state.classID,
        elvenId,
        null,
        ["board", "otherBoard"],
        validations
      );
    }
    if (this.state.classID == graduationId) {
      validations = handleConditionalValidations(
        this.state.classID,
        graduationId,
        null,
        ["board", "otherBoard"],
        validations
      );
    }

    if (this.state.classID == classTenthId) {
      validations = handleConditionalValidations(
        this.state.classID,
        classTenthId,
        null,
        ["stream", "otherStream"],
        validations
      );
    }
    if (this.state.classID == classNineId) {
      validations = handleConditionalValidations(
        this.state.classID,
        classNineId,
        null,
        ["stream", "otherStream", "board", "otherBoard", "degree"],
        validations
      );
    }

    if (this.state.classID == postGradId) {
      validations = handleConditionalValidations(
        this.state.classID,
        postGradId,
        null,
        ["board", "otherBoard"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.stream != otherStreamIdGraduation &&
      this.state.classID == graduationId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.stream,
        otherStreamIdGraduation,
        "notEqual",
        ["otherStream"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.stream !=
      otherStreamIdPostGraduation &&
      this.state.classID == postGradId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.stream,
        otherStreamIdPostGraduation,
        "notEqual",
        ["otherStream"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.stream != otherStreamIdTwelfth &&
      this.state.classID == twelthId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.stream,
        otherStreamIdTwelfth,
        "notEqual",
        ["otherStream"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.stream != otherStreamIdEleven &&
      this.state.classID == elvenId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.stream,
        otherStreamIdEleven,
        "notEqual",
        ["otherStream"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.board != 37 &&
      this.state.classID == twelthId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.board,
        37,
        "notEqual",
        ["otherBoard"],
        validations
      );
    }

    if (
      this.state.eduState.userAcademicInfo.board != 37 &&
      this.state.classID == classTenthId
    ) {
      validations = handleConditionalValidations(
        this.state.eduState.userAcademicInfo.board,
        37,
        "notEqual",
        ["otherBoard"],
        validations
      );
    }

    if (this.state.eduState.userInstituteInfo.state != otherStateId) {
      validations = handleConditionalValidations(
        this.state.eduState.userInstituteInfo.state,
        otherStateId,
        "notEqual",
        ["otherState", "otherDistrict"],
        validations
      );
    }
    /************* end Update conditional validations.........****/

    let isFormValid = true;
    for (let key in validations) {
      let checkKey = key === "cgpa" ? "marks_obtained" : key;
      if (validations[key] !== undefined) {
        let { name, validationFunctions } = this.getValidationRulesObject(key);
        if (this.state.eduState["userAcademicInfo"]["markingType"] === "1") {
          validationFunctions = validationFunctions.filter(
            list => list.name !== "isCGPA"
          );
        }
        let validationResult = ruleRunner(
          this.state.eduState["userAcademicInfo"][checkKey] ||
          this.state.eduState["userInstituteInfo"][checkKey] ||
          this.state.eduState["customData"][checkKey],
          checkKey,
          name,
          ...validationFunctions
        );

        validations[checkKey] = validationResult[checkKey];
        if (validationResult[checkKey] !== null) {
          isFormValid = false;
        }
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  onEducationSubmitHandler(e) {
    e.preventDefault();
    let isSubmit = false;
    const updateEduData = { ...this.state.eduState };

    if (
      this.state.presentClass &&
      this.state.presentClass.presentClassID == 1
    ) {
      updateEduData.userAcademicInfo.presentClass = 1;
    }

    isSubmit = this.checkFormValidations();
    if (isSubmit) {
      // for (let key in this.state.configFieldsData) {
      //   if (this.state.configFieldsData[key].custom) {
      //     this.state.eduState.customData[key] = this.state.eduState[key];
      //   }
      // }
      if (updateEduData && updateEduData.userAcademicInfo.markingType == 1) {
        updateEduData.userAcademicInfo["grade"] = "";
      } else {
        updateEduData.userAcademicInfo["totalMarks"] = "";
        updateEduData.userAcademicInfo["percentage"] = "";
      }
      this.props.saveEducationInfo({
        scholarshipId:
          typeof window !== "undefined"
            ? localStorage.getItem("applicationSchID")
            : "",
        presentClassId: this.state.presentClassID,
        education: updateEduData
      });
    }
  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
  }

  ifClassDetailsAreCompleted() {
    const classesToCheck = []; //10th and graduation
    const usedClasses = (this.state.applicationEducationData
      ? this.state.applicationEducationData.userEducationList &&
        Array.isArray(this.state.applicationEducationData.userEducationList)
        ? this.state.applicationEducationData.userEducationList.filter(
          item =>
            classesToCheck.indexOf(
              item.userAcademicInfo
                ? item.userAcademicInfo.academicClass
                  ? item.userAcademicInfo.academicClass.id
                  : -1
                : -1
            ) > -1
        )
        : []
      : []
    ).filter(
      item =>
        !item.userAcademicInfo.marksObtained && !item.userAcademicInfo.grade
    );
    return usedClasses.length;
  }

  goToNextStep() {
    if (this.ifAllClassesFilled()) {
      if (this.ifClassDetailsAreCompleted()) {
        //Classes details are incomplete....
        this.setState({
          status: false,
          msg: this.props.genericEduTitle ? this.props.genericEduTitle : "",
          showLoader: true
        });
      } else {
        this.setState(
          {
            stepUpdated: true,
            status: false,
            msg: "",
            showLoader: false
          },
          () => {
            this.props.updateStep({
              scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
              step: "EDUCATION_INFO",
              userId: gblFunc.getStoreUserDetails()["userId"],
              presentClassId: this.state.presentClassID
            });
          }
        );
      }
    }
  }

  markup(val) {
    return { __html: val };
  }

  render() {
    const { applicationEducationData } = this.state;
    const {
      allRules,
      applicationStepInstruction,
      educationConfigInfo
    } = this.props;
    const year = getYearOrMonth("year", [1950, 2024]);
    const month = getYearOrMonth("month", []);

    let academicClassOptions = null;
    if (
      educationConfigInfo &&
      educationConfigInfo.academic_class &&
      educationConfigInfo.academic_class.dataOptions &&
      educationConfigInfo.academic_class.dataOptions.length > 0
    ) {
      academicClassOptions = educationConfigInfo.academic_class.dataOptions;
    }
    if (
      applicationEducationData &&
      applicationEducationData.userEducationList &&
      applicationEducationData.userEducationList.length > 0 &&
      educationConfigInfo &&
      educationConfigInfo.academic_class &&
      educationConfigInfo.academic_class.dataOptions &&
      educationConfigInfo.academic_class.dataOptions.length > 0
    ) {
      let classesId = applicationEducationData.userEducationList.map(
        list => list.userAcademicInfo.academicClass.id
      );

      academicClassOptions = academicClassOptions.filter(acdClss => {
        if (
          classesId &&
          classesId.length > 0 &&
          classesId.indexOf(parseInt(acdClss.id)) === -1
        ) {
          acdClss.disabled = true;
          return acdClss;
        }
      });
    }

    return (
      <section className="sectionwhite">
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <article className="form">
          <article className="row ">
            <article
              dangerouslySetInnerHTML={this.markup(this.props.genericEduTitle)}
              className="col-md-12 subheadingerror"
            />
            <article className="col-md-12 table-responsive">
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th>Class</th>
                    <th>Institute/School/College</th>
                    <th>Percentage/Grade</th>

                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <EducationLists
                    editEduction={this.editEduction}
                    applicationEducationData={applicationEducationData}
                  />
                </tbody>
              </table>
            </article>

            {academicClassOptions && academicClassOptions.length > 0 ? (
              <article>
                <article className="row">
                  <article className="col-md-4">
                    <select
                      className="icon"
                      disabled={this.state.isEditable}
                      value={this.state.classID}
                      onChange={event => this.onSelectClassHandler(event)}
                    >
                      <option value="">--Select Present Class--</option>
                      {academicClassOptions.map(cls => (
                        <option
                          disabled={cls.disabled}
                          key={cls.id}
                          value={cls.id}
                          data-presentclass={cls.presentClass ? 1 : 0}
                        >
                          {cls.rulevalue}
                        </option>
                      ))}
                    </select>
                  </article>
                </article>
              </article>
            ) : null}
            {this.state.classID ? (
              <AcadmicClasses
                allRules={allRules}
                classID={this.state.classID}
                boardList={this.props.boardList}
                districtObj={this.props.district}
                subject={this.props.subject}
                isEditable={this.state.isEditable}
                year={year}
                month={month}
                marksOrCgpa={this.state.marksOrCgpa}
                {...this.state.eduState}
                presentClass={this.state.presentClass}
                onDistrictHandler={this.getDistrictHandler.bind(this)}
                educationFormHandler={this.educationFormHandler}
                marksObtainedHandler={this.marksObtainedHandler}
                onEducationSubmitHandler={this.onEducationSubmitHandler}
                {...applicationEducationData}
                educationDataApi={educationConfigInfo}
                applicationStep={applicationStepInstruction}
                validations={this.state.validations}
                type={true}
              />
            ) : null}
            {(this.state.isStepCompleted || this.ifAllClassesFilled()) &&
              !this.state.isEditable ? (
                <input
                  type="button"
                  value="Save & Continue"
                  className="btn  pull-right"
                  onClick={this.goToNextStep}
                />
              ) : null}
          </article>
        </article>

        {/* <article className="form">
          <article className="row">
            <article className="col-md-12 subheading">
              Competitive Exam Scores
              <span>You can fill your any competitive exam score details</span>
              <a className="btn-yellow" onClick={() => this.onClick()}>
                Add competitive exam +
              </a>
            </article>
          </article>
          {this.state.childVisible ? (
            <article>
              <article className="row">
                <article className="col-md-4">
                  <select className="icon">
                    <option>Courses*</option>
                  </select>
                </article>
                <article className="col-md-4">
                  <input type="text" placeholder="Score*" />
                </article>
                <article className="col-md-4">
                  <input
                    type="text"
                    placeholder="Completion month and year*"
                    className="icon-date"
                  />
                </article>
                <article className="col-md-12">
                  <span className="romove"> Romove x</span>
                </article>
              </article>
              <article className="row">
                <article className="col-md-4">
                  <select className="icon">
                    <option>Courses*</option>
                  </select>
                </article>
                <article className="col-md-4">
                  <input type="text" placeholder="Score*" />
                </article>
                <article className="col-md-4">
                  <input
                    type="text"
                    placeholder="Completion month and year*"
                    className="icon-date"
                  />
                </article>
                <article className="col-md-12">
                  <span className="romove"> Romove x</span>
                </article>
              </article>
            </article>
          ) : null}
        </article> */}
      </section>
    );
  }
}

const EducationLists = ({ applicationEducationData, editEduction }) => {
  let eduList = null;
  if (
    applicationEducationData &&
    Object.keys(applicationEducationData).length > 0 &&
    applicationEducationData.userEducationList &&
    applicationEducationData.userEducationList.length > 0
  ) {
    const eduLists = applicationEducationData.userEducationList;
    eduList = eduLists.map((list, index) => (
      <tr key={index}>
        <td>{list.userAcademicInfo.academicClass.value}</td>
        <td>{list.userInstituteInfo.instituteName}</td>
        <td>{`${
          list.userAcademicInfo.percentage &&
            list.userAcademicInfo.markingType == "1"
            ? parseInt(list.userAcademicInfo.percentage, 10)
            : "NA"
          } / ${list.userAcademicInfo.grade ? list.userAcademicInfo.grade : "NA"}
        `}</td>

        <td>
          {" "}
          {/* <span onClick={() => editEduction(list)} className="edit">

          </span> */}
          <i onClick={() => editEduction(list)} className="fa fa-edit iconedit">
            {" "}
            &nbsp;
          </i>
          {/* <span className="delete">&nbsp;</span> */}
        </td>
      </tr>
    ));
  }
  return eduList;
};

export default EducationInfo;
