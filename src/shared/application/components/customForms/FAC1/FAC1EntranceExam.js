import React, { Component } from "react";
import { config } from "../../../formconfig";
import gblFunc from "../../../../../globals/globalFunctions";
import { mapValidationFunc } from "../../../../../validation/rules";
import { ruleRunner } from "../../../../../validation/ruleRunner";
import AlertMessage from "../../../../common/components/alertMsg";
import Loader from "../../../../common/components/loader";
import { yearArray } from "../../../../../constants/constants";
import { camelCase, snakeCase } from "../../../../../constants/constants";
class EntranceExam extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFormButtonShow: true,
      configFieldsData: {},
      getSource: ["Private", "Government", "NGO"],
      showLoader: false,
      msg: "",
      status: "",
      AwardWonGetListByUser: null,
      formData: {
        month: null,
        year: null,
        rank: null,
        entrance_exam_id: null,
        customData: {},
        id: null
      },
      validations: {},
      isCompleted: false,
      isSave: true,
      hideForm: false,
      isShowSave: false
    };
    this.showForm = this.showForm.bind(this);
    this.formChangeHandler = this.formChangeHandler.bind(this);
    this.submit = this.submit.bind(this);
    this.redirectNextSection = this.redirectNextSection.bind(this);
    this.hideExamSection = this.hideExamSection.bind(this);
  }

  showForm() {
    this.state.isFormButtonShow = !this.state.isFormButtonShow;
    this.setState({
      isFormButtonShow: this.state.isFormButtonShow,
      hideForm: false,
      isSave: true
    });
  }

  getYear() {
    let year = [];
    for (let i = 1950; i <= 2050; i++) {
      year.push(i);
    }
    this.setState({
      year: year
    });
  }

  submit() {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }

    let isSubmit = this.checkFormValidations();
    if (isSubmit) {
      for (let key in this.state.configFieldsData) {
        if (this.state.configFieldsData[key].custom) {
          this.state.formData.customData[key] = this.state.formData[key];
        }
      }
      let parseFinalData = camelCase(this.state.formData);
      this.setState({ formData: this.state.formData });
      this.props.saveExamData({
        userId: userId,
        scholarshipId: scholarshipId,
        data: parseFinalData
      });
    }
  }

  componentDidMount() { }

  editExamVal(items) {
    let returnSnake = snakeCase(items);
    const { month, year, rank, entrance_exam_id, customData, id } = returnSnake;
    const editData = {
      month,
      year,
      rank,
      entrance_exam_id,
      customData,
      id
    };
    this.setState({
      isFormButtonShow: false,
      formData: editData,
      isSave: true,
      isShowSave: true,
      hideForm: false
    });
  }

  hideExamSection() {
    this.state.isFormButtonShow = !this.state.isFormButtonShow;
    this.setState({
      isSave: false,
      hideForm: true,
      isFormButtonShow: this.state.isFormButtonShow,
      isShowSave: false
    });
  }

  redirectNextSection() {
    this.props.history.push(
      `/application/${this.props.match.params.bsid}/form/${
      config[this.props.match.params.bsid].nextStepName.entranceExam
      }`
    );
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    let validationRules = [];
    if (this.state.validations.hasOwnProperty(fieldID)) {
      let configFields = this.state.configFieldsData[fieldID];
      if (configFields.validations != null) {
        configFields.validations.map(res => {
          if (res != null) {
            let validator = mapValidationFunc(res);
            if (validator != undefined) validationRules.push(validator);
          }
        });
        validationObject.name = "*Field";
        validationObject.validationFunctions = validationRules;
      }
    }
    return validationObject;
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);
      let validationResult = ruleRunner(
        this.state.formData[key],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  removeAwardWon(wonId) {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    this.props.deleteAwardWonData({
      userId: userId,
      scholarshipId: scholarshipId,
      wonId: wonId
    });
  }

  formChangeHandler(event) {
    const { id, value } = event.target;
    let validations = { ...this.state.validations };
    if (validations.hasOwnProperty(id)) {
      const { name, validationFunctions } = this.getValidationRulesObject(id);

      const validationResult = ruleRunner(
        value,
        id,
        name,
        ...validationFunctions
      );
      validations[id] = validationResult[id];
    }

    const updateFormData = { ...this.state.formData };
    updateFormData[id] = value;
    this.setState({
      formData: updateFormData,
      validations
    });
  }

  getExamData() {
    let scholarshipId = "";
    let userId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    this.props.getExamData({
      userId: userId,
      scholarshipId: scholarshipId,
      step: "ENTRANCE_EXAM"
    });
  }

  componentWillMount() {
    this.getExamData();
    this.getYear();
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps.applicationExam;

    switch (type) {
      case "FETCH_APPLICATION_FORMS_EXAM_SUCCESS":
        if (nextProps.applicationExam["applicationExam"] != null) {
          const ExamConfig =
            nextProps.applicationExam["applicationExam"]["examConfig"];
          const ExamGet =
            nextProps.applicationExam["applicationExam"]["examGet"];
          for (let key in ExamConfig) {
            if (ExamConfig[key].validations != null && ExamConfig[key].active) {
              this.state.validations[key] = null;
            }
          }

          if (ExamGet != null && ExamGet.length > 0) {
            this.setState({
              isCompleted: true
            });
          }

          this.setState({
            configFieldsData: ExamConfig,
            validations: this.state.validations,
            examList: ExamGet
          });
        }
        break;
      case "UPDATE_APPLICATION_FORMS_EXAM_SUCCESS":
        this.setState({
          status: true,
          msg: "Record has been Updated",
          showLoader: true
        });
        this.state.formData = {
          year: "",
          month: "",
          rank: "",
          entrance_exam_id: "",
          customData: {}
        };
        this.setState({
          formData: this.state.formData,
          isSave: false,
          isShowSave: false
        });

        this.getExamData();
        break;

      case "UPDATE_APPLICATION_FORMS_EXAM_FAILURE":
        this.setState({
          status: false,
          msg: "Something went wrong",
          showLoader: this
        });
        break;
      /*   case "DELETE_APPLICATION_FORMS_AWARDWON_SUCCESS":
        this.setState({
          status: true,
          msg: "Your record has been deleted",
          showLoader: true
        });
        this.getAwardWonData();
        break;
      case "DELETE_APPLICATION_FORMS_AWARDWON_FAILURE":
        this.setState({
          status: false,
          msg: "No Record Deleted",
          showLoader: true
        });
        break; */
    }
  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
  }

  render() {
    const formConfig = this.state.configFieldsData;
    return (
      <section>
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <section className="sectionwhite">
          <article className="form">
            <article className="row">
              <article className="col-md-12 subheading">
                Please enter all details
              </article>
              {this.state.examList != null
                ? this.state.examList.map(res => {
                  return (
                    <article className="graystrip" key={res.id}>
                      <article className="table-responsive">
                        <table className="table">
                          <thead>
                            <tr>
                              <th>Entrance Exam</th>
                              <th>Month</th>
                              <th>Year</th>
                              <th>Score</th>
                              <th>&nbsp;</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                {res.entranceExamName ||
                                  res.entrance_exam_name}
                              </td>
                              {/*  <td>{res.level}</td> */}
                              <td>{res.month}</td>
                              <td>{res.year}</td>
                              <td>{res.rank}</td>
                              <td>
                                <span
                                  className="edit"
                                  onClick={event => this.editExamVal(res)}
                                />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </article>
                    </article>
                  );
                })
                : ""}
              {!this.state.isShowSave &&
                this.state.isCompleted && (
                  <article className="row">
                    <article className="col-md-12">
                      <input
                        type="submit"
                        value="Save &amp; Continue"
                        onClick={this.redirectNextSection}
                        className="btn pull-right btnPos"
                      />
                    </article>
                  </article>
                )}
            </article>
            {this.state.isFormButtonShow && !this.state.isCompleted ? (
              <article className="row">
                <article className="col-md-12">
                  <a onClick={this.showForm} className="btn-yellow pull-left">
                    Add Entrance Exam +
                  </a>
                </article>
              </article>
            ) : (
                ""
              )}

            {!this.state.isFormButtonShow &&
              this.state.isSave &&
              !this.state.hideForm ? (
                <article>
                  <article className="paddingborder">
                    <article className="border">&nbsp;</article>
                  </article>

                  <article>
                    <section>
                      <article className="row">
                        {formConfig.entrance_exam_id != null &&
                          formConfig.entrance_exam_id.active ? (
                            <article className="col-md-4">
                              <article className="form-group">
                                <select
                                  id="entrance_exam_id"
                                  className="icon"
                                  onChange={this.formChangeHandler.bind(this)}
                                  value={this.state.formData.entrance_exam_id || ""}
                                >
                                  <option value="">Select Exam</option>
                                  {formConfig.entrance_exam_id.dataOptions != null
                                    ? formConfig.entrance_exam_id.dataOptions.map(
                                      res => {
                                        return (
                                          <option key={res.id} value={res.id}>
                                            {res.rulevalue}
                                          </option>
                                        );
                                      }
                                    )
                                    : ""}
                                </select>
                                <label className="labelstyle">
                                  {gblFunc.capitalText(
                                    formConfig.entrance_exam_id.label
                                  )}
                                </label>
                                {this.state.validations["entrance_exam_id"] ? (
                                  <span className="error animated bounce">
                                    {this.state.validations["entrance_exam_id"]}
                                  </span>
                                ) : null}
                              </article>
                            </article>
                          ) : (
                            ""
                          )}

                        {formConfig.year != null && formConfig.year.active ? (
                          <article className="col-md-4">
                            <article className="form-group">
                              <select
                                id="year"
                                className="icon"
                                onChange={this.formChangeHandler.bind(this)}
                                value={this.state.formData.year || ""}
                              >
                                <option value="">Select Year</option>
                                {formConfig.year.dataOptions != null
                                  ? formConfig.year.dataOptions.map(res => {
                                    return (
                                      <option key={res.id} value={res.id}>
                                        {res.rulevalue}
                                      </option>
                                    );
                                  })
                                  : ""}
                              </select>
                              <label className="labelstyle">
                                {gblFunc.capitalText(formConfig.year.label)}
                              </label>
                              {this.state.validations["year"] ? (
                                <span className="error animated bounce">
                                  {this.state.validations["year"]}
                                </span>
                              ) : null}
                            </article>
                          </article>
                        ) : (
                            ""
                          )}

                        {formConfig.month != null && formConfig.month.active ? (
                          <article className="col-md-4">
                            <article className="form-group">
                              <select
                                id="month"
                                className="icon"
                                onChange={this.formChangeHandler.bind(this)}
                                value={this.state.formData.month || ""}
                              >
                                <option value="">Select Month</option>
                                {formConfig.month.dataOptions != null
                                  ? formConfig.month.dataOptions.map(res => {
                                    return (
                                      <option key={res.id} value={res.id}>
                                        {res.rulevalue}
                                      </option>
                                    );
                                  })
                                  : ""}
                              </select>
                              <label className="labelstyle">
                                {gblFunc.capitalText(formConfig.month.label)}
                              </label>
                              {this.state.validations["month"] ? (
                                <span className="error animated bounce">
                                  {this.state.validations["month"]}
                                </span>
                              ) : null}
                            </article>
                          </article>
                        ) : (
                            ""
                          )}

                        {formConfig.rank != null && formConfig.rank.active ? (
                          <article className="col-md-4">
                            <article className="form-group">
                              <input
                                type="text"
                                id="rank"
                                placeholder={`${formConfig.rank.label}`}
                                data-custom={formConfig.rank.custom}
                                onChange={this.formChangeHandler}
                                value={this.state.formData.rank || ""}
                              />
                              <label>
                                {gblFunc.capitalText(formConfig.rank.label)}
                              </label>
                              {this.state.validations["rank"] ? (
                                <span className="error animated bounce">
                                  {this.state.validations["rank"]}
                                </span>
                              ) : null}
                            </article>
                          </article>
                        ) : (
                            ""
                          )}
                      </article>

                      <article className="row">
                        <article className="col-md-12 noPaddingBG">
                          <input
                            type="submit"
                            value="Cancel"
                            className="btn pull-right"
                            onClick={this.hideExamSection}
                          />
                          <input
                            style={{ marginRight: "10px" }}
                            type="submit"
                            value="Save"
                            className="btn pull-right"
                            onClick={this.submit}
                          />
                        </article>
                      </article>
                    </section>
                  </article>
                </article>
              ) : (
                ""
              )}
          </article>
        </section>
      </section>
    );
  }
}

export default EntranceExam;
