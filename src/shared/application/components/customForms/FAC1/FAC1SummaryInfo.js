import React, { Component } from "react";
import Loader from "../../../../common/components/loader";
import AlertMessage from "../../../../common/components/alertMsg";
import { all } from "redux-saga/effects";
import { stepNamesToDisplay } from "../../../formconfig";
import { Route, Link, Redirect } from "react-router-dom";

class Summary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      historyList: {
        personalInfo: {},
        educationInfos: [],
        familyInfos: [],
        interests: [],
        references: [],
        entranceExams: [],
        scholarshipHistories: [],
        scholarshipQuestion: [],
        entranceExams: []
      },
      personalConfig: [],
      educationConfig: [],
      familyConfig: [],
      refrenceConfig: [],
      isSubmitDisabled: true,
      msg: "",
      status: "",
      showLoader: false,
      isCompleted: false,
      decHtml: "",
      showpopupTerms: false,
      isRedirect: false
    };

    this.submitApplicaitonForm = this.submitApplicaitonForm.bind(this);
  }

  componentDidMount() {
    let scholarshipId = "";
    if (window != undefined) {
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    this.props.getAllSummery({
      scholarshipId: scholarshipId
    });
  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
  }
  togglePopup() {
    this.setState({
      showpopupTerms: !this.state.showpopupTerms
    });
  }
  componentWillReceiveProps(nextProps) {
    const { type } = nextProps.applicationSummery;

    switch (type) {
      case "GET_ALL_SUMMERY_RULES_SUCCESS":
        let summary = nextProps["applicationSummery"]
          ? nextProps["applicationSummery"]["summeryData"]["summary"]
          : [];

        let allSteps = nextProps["scholarshipSteps"]
          ? nextProps["scholarshipSteps"]
          : [];
        let completedSteps = nextProps["applicationStepInstruction"]
          ? nextProps["applicationStepInstruction"]
          : [];
        let innerHtml = nextProps["instructions"]["consent"];
        let isCompleted = false;
        let countSteps = 0;
        if (allSteps != null && allSteps.length > 0) {
          allSteps.map(res => {
            if (res.step != "SUMMARY") {
              if (completedSteps.indexOf(res.step) > -1) {
                ++countSteps;
                if (allSteps.length - 1 == countSteps) {
                  isCompleted = true;
                }
              }
            }
          });
        }

        this.setState({
          historyList: {
            personalInfo: summary["personalInfo"],
            educationInfos: summary["educationInfos"],
            familyInfos: summary["familyInfos"],
            interests: summary["interests"],
            references: summary["references"],
            entranceExams: summary["entranceExams"],
            scholarshipHistories: summary["scholarshipHistories"],
            userScholarshipDocuments: summary["userScholarshipDocuments"],
            scholarshipQuestion: summary["scholarshipQuestions"],
            entranceExams: summary["entranceExams"]
          },
          isCompleted: isCompleted,
          decHtml: innerHtml
        });
        break;
      case "FETCH_SCHOLARSHIP_APPLY_SUCCESS_SUMMARY":
        this.setState({
          status: true,
          msg: "Application Form has been submitted successfully",
          showLoader: true
        });

        setTimeout(() => {
          this.setState({
            isRedirect: true // redirect to application status page
          });
        }, 1000);

        break;
      case "FETCH_SCHOLARSHIP_APPLY_FAILURE_SUMMARY":
        this.setState({
          status: false,
          msg: "Sorry!! Server Error",
          showLoader: true
        });

        break;
    }
  }

  isSubmitDisabled(event) {
    if (event.target.checked) {
      this.setState({
        isSubmitDisabled: false
      });
    } else {
      this.setState({
        isSubmitDisabled: true
      });
    }
  }

  submitApplicaitonForm() {
    if (this.state.isSubmitDisabled) {
      this.setState({
        status: false,
        msg: "Kindly agree to the terms and conditions.",
        showLoader: true
      });
      return false;
    } else {
      if (this.state.isCompleted) {
        let scholarshipId = "";
        if (window != undefined) {
          scholarshipId = parseInt(
            window.localStorage.getItem("applicationSchID")
          );
        }
        this.props.fetchSchApply({
          scholarshipId: scholarshipId
        });
        return false;
      } else {
        this.setState({
          status: false,
          msg: "Kindly complete all pending steps.",
          showLoader: true
        });
        return false;
      }
    }
  }

  markup(val) {
    return { __html: val };
  }

  displayQuestionAns(res) {
    if (res.question["questionTypeId"] == 1) {
      return res.response;
    } else if (res.question["questionTypeId"] == 2) {
      if (res.optionId != null && res.optionId.length > 0) {
        let signleOptionAns = res.question["questionOptions"].filter(items => {
          return items["id"] == res.optionId[0];
        });

        if (signleOptionAns != null && signleOptionAns.length > 0) {
          return signleOptionAns[0].optionValue;
        } else {
          return "No Result Found";
        }
      }
    } else {
      if (res.optionId != null && res.optionId.length > 0) {
        let multiOptionAns = res.question["questionOptions"].filter(items => {
          return res.optionId.indexOf(items["id"]) > -1;
        });

        if (multiOptionAns != null && multiOptionAns.length > 0) {
          multiOptionAns.map(items => {
            return items.optionValue;
          });
        } else {
          return "No Result Found";
        }
      }
    }
  }

  render() {
    const { applicationStepInstruction, scholarshipSteps } = this.props;
    let isSubmitArray = [];

    if (applicationStepInstruction && scholarshipSteps) {
      if (
        Array.isArray(applicationStepInstruction) &&
        Array.isArray(scholarshipSteps)
      ) {
        if (
          applicationStepInstruction.length > 0 &&
          scholarshipSteps.length > 0
        ) {
          isSubmitArray = scholarshipSteps.map(schStep => {
            return applicationStepInstruction.indexOf(schStep.step) > -1
              ? true
              : false;
          });
        }
      }
    }

    return (
      <section className="sectionwhite">
        {this.state.isRedirect ? (
          <Redirect to={`/myscholarship/?tab=application-status`} />
        ) : (
            ""
          )}

        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <article className="row">
          <article className="col-md-12 subheading">
            <p style={{ color: "red" }}>
              *Preview of entered details are visible below. Please verify them
              before clicking on the Submit button available at the end of the
              summary for final submission of your application.
            </p>
          </article>
        </article>
        <article className="form">
          {this.state.historyList["personalInfo"] != null ? (
            <article>
              <article className="row">
                <article className="col-md-12 subheading">
                  {stepNamesToDisplay["PERSONAL_INFO"]}
                </article>
                <article className="col-md-12  table-responsive paddingtable0">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>DOB</th>
                        <th>Annual Family Income (INR)</th>
                        {/*  <th>AadharCard</th> */}
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          {this.state.historyList["personalInfo"].firstName}&nbsp;{
                            this.state.historyList["personalInfo"].lastName
                          }
                        </td>
                        <td>{this.state.historyList["personalInfo"].email}</td>
                        <td>{this.state.historyList["personalInfo"].mobile}</td>
                        <td>{this.state.historyList["personalInfo"].dob}</td>
                        <td>
                          {this.state.historyList["personalInfo"].familyIncome}{" "}
                        </td>
                        {/*    <td>
                          {this.state.historyList["personalInfo"].aadharCard}
                        </td> */}
                      </tr>

                      {this.state.historyList["personalInfo"] == null ? (
                        <tr>
                          <td align="center" colSpan="6">
                            <article className="text-center">
                              No Result Found
                            </article>
                          </td>
                        </tr>
                      ) : (
                          ""
                        )}
                    </tbody>
                  </table>
                </article>
              </article>
              <article className="paddingborder">
                <article className="border">&nbsp;</article>
              </article>
            </article>
          ) : (
              ""
            )}

          {this.state.historyList["educationInfos"] != null ? (
            <article>
              <article className="row">
                <article className="col-md-12 subheading">
                  {stepNamesToDisplay["EDUCATION_INFO"]}
                </article>
                <article className="col-md-12  table-responsive paddingtable0">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        {/*    <th>Institute Name</th> */}
                        <th>Class Name</th>
                        <th>Marking Type</th>
                        <th>Marks</th>
                        <th>Percent/Grade Value</th>
                        {/*  <th>Year</th> */}
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.historyList["educationInfos"].map(
                        (res, i) => {
                          return (
                            <tr key={i}>
                              {/*   <td>
                                {res.userInstituteInfo
                                  ? res.userInstituteInfo.instituteName
                                  : ""}
                              </td> */}
                              <td>
                                {res.userAcademicInfo
                                  ? res.userAcademicInfo.academicClassName
                                  : ""}
                              </td>
                              <td>
                                {res.userAcademicInfo.markingType == 1
                                  ? "Marks"
                                  : "Grades"}
                              </td>
                              <td>
                                {res.userAcademicInfo
                                  ? res.userAcademicInfo.marksObtained
                                  : "N/A"}

                                {res.userAcademicInfo &&
                                  res.userAcademicInfo.marksObtained != null
                                  ? "/"
                                  : "N/A"}

                                {res.userAcademicInfo
                                  ? res.userAcademicInfo.totalMarks
                                  : "N/A"}
                              </td>
                              <td>
                                {res.userAcademicInfo.markingType == 1
                                  ? res.userAcademicInfo.percentage != null
                                    ? res.userAcademicInfo.percentage.toFixed(2)
                                    : ""
                                  : res.userAcademicInfo.grade != null
                                    ? res.userAcademicInfo.grade
                                    : ""}
                              </td>
                              {/*    <td>
                                {res.userAcademicInfo
                                  ? res.userAcademicInfo.passingYear
                                  : ""}
                              </td> */}
                            </tr>
                          );
                        }
                      )}

                      {this.state.historyList["educationInfos"].length == 0 ? (
                        <tr>
                          <td align="center" colSpan="6">
                            <article className="text-center">
                              No Result Found
                            </article>
                          </td>
                        </tr>
                      ) : (
                          ""
                        )}
                    </tbody>
                  </table>
                </article>
              </article>
              <article className="paddingborder">
                <article className="border">&nbsp;</article>
              </article>
            </article>
          ) : (
              ""
            )}

          {this.state.historyList["familyInfos"] != null ? (
            <article>
              <article className="row">
                <article className="col-md-12 subheading">
                  {stepNamesToDisplay["FAMILY_INFO"]}
                </article>
                <article className="col-md-12  table-responsive paddingtable0">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Occupation</th>
                        <th>Absolute Income (INR)</th>
                        <th>Relation</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.historyList["familyInfos"].map((res, i) => {
                        return (
                          <tr key={i}>
                            <td>{res.name}</td>
                            <td>{res.occupationName}</td>
                            <td>{res.absoluteIncome}</td>
                            <td>{res.relationName}</td>
                          </tr>
                        );
                      })}

                      {this.state.historyList["familyInfos"].length == 0 ? (
                        <tr>
                          <td align="center" colSpan="6">
                            <article className="text-center">
                              No Result Found
                            </article>
                          </td>
                        </tr>
                      ) : (
                          ""
                        )}
                    </tbody>
                  </table>
                </article>
              </article>
              <article className="paddingborder">
                <article className="border" />
              </article>
            </article>
          ) : (
              ""
            )}

          {this.state.historyList["references"] != null ? (
            <article>
              <article className="row">
                <article className="col-md-12 subheading">
                  {stepNamesToDisplay["REFERENCES"]}
                </article>
                <article className="col-md-12  table-responsive paddingtable0">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Address</th>
                        <th>Occupation</th>
                        <th>Relation</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.historyList["references"].map((res, i) => {
                        return (
                          <tr key={i}>
                            <td>{res.name}</td>
                            <td>{res.mobile}</td>
                            <td>{res.address}</td>
                            <td>{res.occupationName}</td>
                            <td>{res.relationName}</td>
                          </tr>
                        );
                      })}

                      {this.state.historyList["references"].length == 0 ? (
                        <tr>
                          <td align="center" colSpan="6">
                            <article className="text-center">
                              No Result Found
                            </article>
                          </td>
                        </tr>
                      ) : (
                          ""
                        )}
                    </tbody>
                  </table>
                </article>
              </article>
              <article className="paddingborder">
                <article className="border">&nbsp;</article>
              </article>
            </article>
          ) : (
              ""
            )}

          {this.state.historyList["userScholarshipDocuments"] != null ? (
            <article>
              <article className="row">
                <article className="col-md-12 subheading">
                  {stepNamesToDisplay["DOCUMENTS"]}
                </article>
                <article className="col-md-12  table-responsive paddingtable0">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>Document Name</th>
                        <th>Document Location</th>
                        {/*     <th>Income</th>
                        <th>Mobile</th>
                        <th>Relation</th> */}
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.historyList["userScholarshipDocuments"].map(
                        (res, i) => {
                          return (
                            <tr key={i}>
                              <td>
                                {res.documentType != null
                                  ? res.documentType.description
                                  : ""}
                              </td>
                              <td className="docicon">
                                {res.userDocument != null ? (
                                  // ? res.userDocument[0].location
                                  <a
                                    href={res.userDocument[0].location}
                                    target="_blank"
                                  >
                                    <i className="fa fa-download Checkicon" />
                                  </a>
                                ) : (
                                    "No document uploaded"
                                  )}
                              </td>
                            </tr>
                          );
                        }
                      )}

                      {this.state.historyList["userScholarshipDocuments"]
                        .length == 0 ? (
                          <tr>
                            <td align="center" colSpan="6">
                              <article className="text-center">
                                No Result Found
                            </article>
                            </td>
                          </tr>
                        ) : (
                          ""
                        )}
                    </tbody>
                  </table>
                </article>
              </article>
              <article className="paddingborder">
                <article className="border">&nbsp;</article>
              </article>
            </article>
          ) : (
              ""
            )}

          {this.state.historyList["scholarshipQuestion"] != null ? (
            <article>
              <article className="row">
                <article className="col-md-12 subheading">
                  {stepNamesToDisplay["QUESTIONS"]}
                </article>
                <article className="col-md-12  table-responsive paddingtable0">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>Question</th>
                        <th>Answer</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.historyList["scholarshipQuestion"].map(
                        (res, i) => {
                          return (
                            <tr key={i}>
                              <td>
                                {res.question != null
                                  ? res.question.questionVerbiage
                                  : ""}
                              </td>
                              <td className="docicon">
                                {res.question != null
                                  ? this.displayQuestionAns(res)
                                  : "No Response"}
                              </td>
                            </tr>
                          );
                        }
                      )}

                      {this.state.historyList["scholarshipQuestion"].length ==
                        0 ? (
                          <tr>
                            <td align="center" colSpan="6">
                              <article className="text-center">
                                No Result Found
                            </article>
                            </td>
                          </tr>
                        ) : (
                          ""
                        )}
                    </tbody>
                  </table>

                  {this.state.historyList["entranceExams"] != null ? (
                    <article>
                      <article className="row">
                        <article className="col-md-12 subheading">
                          {stepNamesToDisplay["ENTRANCE_EXAM"]}
                        </article>
                        <article className="col-md-12  table-responsive paddingtable0">
                          <table className="table table-striped">
                            <thead>
                              <tr>
                                <th>Exam Name</th>
                                <th>Score</th>
                                <th>Year</th>
                                <th>Month</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.historyList["entranceExams"].map(
                                (res, i) => {
                                  return (
                                    <tr key={i}>
                                      <td>
                                        {res.entranceExamName != null
                                          ? res.entranceExamName
                                          : ""}
                                      </td>
                                      <td className="docicon">
                                        {res.rank != null ? res.rank : ""}
                                      </td>
                                      <td className="docicon">
                                        {res.year != null ? res.year : ""}
                                      </td>

                                      <td className="docicon">
                                        {res.month != null ? res.month : ""}
                                      </td>
                                    </tr>
                                  );
                                }
                              )}

                              {this.state.historyList["entranceExams"].length ==
                                0 ? (
                                  <tr>
                                    <td align="center" colSpan="6">
                                      <article className="text-center">
                                        No Result Found
                                    </article>
                                    </td>
                                  </tr>
                                ) : (
                                  ""
                                )}
                            </tbody>
                          </table>
                        </article>
                      </article>
                      <article className="paddingborder">
                        <article className="border">&nbsp;</article>
                      </article>
                    </article>
                  ) : (
                      ""
                    )}
                </article>
              </article>
              <article className="paddingborder">
                <article className="border">&nbsp;</article>
              </article>
            </article>
          ) : (
              ""
            )}
        </article>

        <article className="row summarySec">
          <div className="col-md-12">
            {/*   <p>
              Before you, as an applicant, can submit your application to
              Marubeni India, you must read and agree to these&nbsp;
              <span className="cursor" onClick={this.togglePopup.bind(this)}>
                terms and conditions
              </span>
            </p> */}

            {this.state.showpopupTerms ? (
              <Terms text="Close Me" closePopup={this.togglePopup.bind(this)} />
            ) : null}

            <p dangerouslySetInnerHTML={this.markup(this.state.decHtml)} />

            {/* <p>Declaration</p>
            <p>
              Before you, as an applicant, can submit your application to
              Marubeni India, you must read and agree to these
              <a
                href="#"
                data-toggle="modal"
                data-target="#myTermConditionModal"
              >
                &nbsp;terms and conditions.
              </a>
            </p>
            <p>
              I, hereby declare that the information and the documents furnished
              in/ along with this Scholarship Application Form is true and
              correct and that I have not withheld any relevant/material
              particulars/information.
            </p>
            <p>
              I acknowledge that Marubeni India reserves the right to
              discontinue this Scholarship at any time without any notice to us
              and it shall not be held liable for any consequences thereof.
            </p> */}
            <p>
              <label>
                <input
                  type="checkbox"
                  value="2"
                  id="20"
                  name="20_val"
                  onChange={event => this.isSubmitDisabled(event)}
                />
                <span className="red" />
              </label>
              &nbsp;&nbsp;I have read the terms and conditions carefully, I
              agree that by submitting this application, I am electronically
              signing the application.
            </p>
          </div>
          <article className="col-md-12 noPaddingBG">
            <input
              /*  disabled={
                isSubmitArray.length == 0 || isSubmitArray.indexOf(false) > -1
                  ? true
                  : false
              } */

              /*  disabled={this.state.isSubmitDisabled} */
              onClick={this.submitApplicaitonForm}
              type="button"
              value="Submit"
              className="btn-yellow btnsubmit"
            />
          </article>
        </article>
      </section>
    );
  }
}

class Terms extends React.Component {
  render() {
    return (
      <article className="popup">
        <article className="popupsms">
          <article className="popup_inner loginpopup">
            <article className="LoginRegPopup">
              <section className="modal fade modelAuthPopup1">
                <section className="modal-dialog">
                  <section className="modal-content modelBg">
                    <article className="modal-header">
                      <h3 className="modal-title">Terms and Conditions</h3>
                      <button
                        type="button"
                        className="close btnPos"
                        onClick={this.props.closePopup}
                      >
                        <i>&times;</i>
                      </button>
                    </article>

                    <article className="modal-body forgot terms">
                      <article className="row">
                        <article className="col-md-12 ">
                          {/* <h1>Terms and Conditions :</h1> */}
                          <ul>
                            <li>
                              Marubeni India Private Limited (“Marubeni India”)
                              reserves the right to modify, cancel or withdraw
                              this Scholarship program at any stage without
                              giving any prior notice or assigning any reason
                              and accepts no responsibility for the consequences
                              of such modification/cancellation or withdrawal.
                            </li>
                            <li>
                              The selection of candidates for awarding
                              scholarship shall be done solely by Marubeni India
                              and shall be final and binding.{" "}
                            </li>
                            <li>
                              This Scholarship Program aims to provide financial
                              assistance to meritorious and needy students and
                              to promote their higher education. Thus,
                              Scholarship cannot be demanded as a right.{" "}
                            </li>
                            <li>
                              The Scholarship amount shall be paid to the
                              selected candidates through normal banking
                              channels either through direct bank transfer or by
                              account payee cheque / demand draft. Thus, all
                              selected candidates must have active bank account
                              in their own name at the time of remittance of
                              scholarship amount or issue of account payee
                              cheque / demand draft. Marubeni India shall not be
                              liable for any loss or damage of the cheque /
                              demand draft once it has been handed over to the
                              selected candidate.{" "}
                            </li>
                            <li>
                              The candidates shall be required to produce the
                              original certificates/documents relating to their
                              education, family income etc. at the time or
                              before face to face interview. Failure to produce
                              such certificates/documents (in original) shall
                              disqualify the candidate from this Scholarship
                              Program.
                            </li>
                            <li>
                              CANVASSING IN ANY FORM SHALL BE A
                              DISQUALIFICATION.
                            </li>
                            <li>
                              The scholarship for a candidate may be withdrawn
                              at any stage if you provide incorrect, false or
                              misleading information or withhold relevant
                              information.{" "}
                            </li>
                            <li>The scholarship shall be non-transferable.</li>
                            <li>
                              Marubeni India shall not be responsible for
                              payment of any taxes or levies that may be
                              applicable on/charged to the scholarship amount.{" "}
                            </li>
                            <li>
                              Marubeni India shall not be liable or held
                              responsible for any lack or lapse in any
                              communication on account of failure or delay by
                              any of the Internet, Telecom, SMS and E-mails
                              service provider. No correspondence in this regard
                              will be entertained.
                            </li>
                            <li>
                              Disputes, if any, will be subject to exclusive
                              jurisdiction of the courts at New Delhi only.
                            </li>
                          </ul>
                        </article>
                      </article>
                    </article>
                  </section>
                </section>
              </section>
            </article>
          </article>
        </article>
      </article>
    );
  }
}

export default Summary;
