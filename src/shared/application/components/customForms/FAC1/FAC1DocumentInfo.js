import React, { Component } from "react";
import index from "axios";
import {
  required,
  maxFileSize,
  hasValidExtension
} from "../../../../../../validation/rules";
import { ruleRunner } from "../../../../../../validation/ruleRunner";
import AlertMessage from "../../../../common/components/alertMsg";
import Loader from "../../../..//common/components/loader";
import { Route, Link, Redirect } from "react-router-dom";
import {
  allowedDocumentExtensions,
  imageFileExtension
} from "../../../../../constants/constants";
import Cropper from "react-cropper";
if (typeof window !== "undefined") {
  require("cropperjs/dist/cropper.css");
}
import ConfirmMessagePopup from "../../../../common/components/confirmMessagePopup";
import gblFunc from "../../../../../globals/globalFunctions";

class DocumentNeeded extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPopup: false,
      documentList: [],
      showLoader: false,
      msg: "",
      status: "",
      isDropDownCategory: {},
      isDropDownOption: {},
      isFormValid: false,
      year: [
        { id: "28", value: "1st Year" },
        { id: "29", value: "2nd Year" },
        { id: "32", value: "3rd Year" },
        { id: "33", value: "4th Year" },
        { id: "34", value: "5th Year" },
        { id: "35", value: "6th Year" }
      ],
      semester: [
        { id: "36", value: "1st Semester" },
        { id: "37", value: "2nd Semester" },
        { id: "38", value: "3rd Semester" },
        { id: "39", value: "4th Semester" },
        { id: "40", value: "5th Semester" },
        { id: "41", value: "6th Semester" },
        { id: "42", value: "7th Semester" },
        { id: "43", value: "8th Semester" },
        { id: "44", value: "9th Semester" },
        { id: "45", value: "10th Semester" }
      ],
      validations: {},
      docFile: null,
      isShowCropPup: false,
      cropSrc: "",
      cropResult: null,
      showConfirmationPopup: false,
      deleteSuccessCallBack: null,
      isNext: false,
      isCompleted: false
    };
    this.cropImage = this.cropImage.bind(this);
    this.dataURLtoFile = this.dataURLtoFile.bind(this);
    this.checkValidation = this.checkValidation.bind(this);
    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.hideConfirmationPopup = this.hideConfirmationPopup.bind(this);
  }

  uploadDocumentWithOutCrop(docObj, files) {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
    }
    const nullIfFalsy = value => value || null;

    if ([6, 7].indexOf(docObj.documentType["id"]) > -1) {
      const stringifiedData = JSON.stringify({
        documentType: nullIfFalsy(docObj["documentType"]["id"]),
        documentTypeCategory: nullIfFalsy(
          this.state.isDropDownCategory[docObj.documentType["id"]]
        ), // pass type Year or Semester
        documentTypeCategoryOption: nullIfFalsy(
          this.state.isDropDownOption[docObj.documentType["id"]]
        ) // pass year Value or Semester Value
      });

      this.props.uploadAllDocuement({
        docFile: files[0],
        userDocRequest: stringifiedData,
        userId: userId,
        isPosted: this.state.isCompleted
      });
    } else {
      const stringifiedData = JSON.stringify({
        documentType: nullIfFalsy(docObj["documentType"]["id"]),
        documentTypeCategory: nullIfFalsy(null), // pass type Year or Semester
        documentTypeCategoryOption: nullIfFalsy(null) // pass year Value or Semester Value
      });

      this.props.uploadAllDocuement({
        docFile: files[0],
        userDocRequest: stringifiedData,
        userId: userId,
        isPosted: this.state.isCompleted
      });
    }
  }

  getDocData(event, docObj) {
    let validations = { ...this.state.validations };
    let validationResult = null;
    let isFormValid = true;
    const { id, value, files } = event.target;
    const uploadedFile = files[0];
    const sizeInMb = Math.ceil(uploadedFile.size / (1024 * 1024));
    const fileExtension = uploadedFile.name.slice(
      uploadedFile.name.lastIndexOf(".")
    );
    if (docObj.documentType["id"] == 1) {
      validationResult = ruleRunner(
        fileExtension,
        docObj.documentType["id"],
        "File",
        hasValidExtension(imageFileExtension)
      );
    } else {
      validationResult = ruleRunner(
        fileExtension,
        docObj.documentType["id"],
        "File",
        hasValidExtension(allowedDocumentExtensions)
      );
    }
    if (validationResult[docObj.documentType["id"]] === null) {
      validationResult = ruleRunner(
        sizeInMb,
        docObj.documentType["id"],
        "Uploaded file",
        maxFileSize(1)
      );
    }
    validations[docObj.documentType["id"]] =
      validationResult[docObj.documentType["id"]];
    if (validationResult[docObj.documentType["id"]] !== null) {
      // if invalid extention and size
      isFormValid = false;
      this.setState({
        isCompleted: false
      });
    }
    //show croper if document type would be 1
    if (isFormValid) {
      this.setState({
        isCompleted: true
      });
      // if form should be valid
      if (docObj.documentType["id"] == 1) {
        this.state.isShowCropPup = true;
      }

      let reader = new FileReader(); // read upload file
      reader.onload = e => {
        this.setState({ cropSrc: e.target.result });
      };
      reader.readAsDataURL(event.target.files[0]);
    }

    this.setState({
      validations,
      isShowCropPup: this.state.isShowCropPup,
      docFile: files
    });

    if (docObj.documentType["id"] != 1 && isFormValid) {
      // only those document having no 1 Id
      this.uploadDocumentWithOutCrop(docObj, files);
    }
  }

  deleteDocument(id, docObj) {
    let userId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
    }

    const deleteSuccessCallBack = () =>
      this.props.deleteDocuement({
        docId: id,
        userId: userId,
        isDeleted: true
      });

    this.setState({
      showConfirmationPopup: true,
      deleteSuccessCallBack
    });

    delete this.state.isDropDownCategory[docObj.documentType["id"]];
    delete this.state.isDropDownOption[docObj.documentType["id"]];
  }

  showConfirmationPopup() {
    this.setState({
      showConfirmationPopup: true
    });
  }

  hideConfirmationPopup() {
    this.setState({
      showConfirmationPopup: false,
      deleteSuccessCallBack: null
    });
  }

  getAllDocuent() {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }

    this.props.getAllDocuement({
      userId: userId,
      scholarshipId: scholarshipId
    });
  }

  componentDidMount() {
    this.getAllDocuent();
  }

  componentWillReceiveProps(nextProps) {
    const { type } = nextProps.allDocument;
    let scholarshipId = ""; // PASS SCHOLARSHIP ID FOR COMPLETED STEPS
    if (window != undefined) {
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }

    switch (type) {
      case "GET_ALL_DOCUMENT_SUCCESS":
        nextProps.allDocument["documentDataList"].map(res => {
          if (res.isMandatory) {
            this.state.validations[res.documentType["id"]] = null;
          }

          if ([6, 7].indexOf(res.documentType["id"]) > -1) {
            return (this.state.isDropDownCategory[res.documentType["id"]] = "");
          } else {
            return {};
          }
        });
        this.setState({
          documentList: nextProps.allDocument["documentDataList"],
          isDropDownCategory: this.state.isDropDownCategory,
          validations: this.state.validations
        });

        //VALIDATE THE DOCUMENT HAS BEEN COMPLETED
        let tempArray = [];
        if (
          nextProps.allDocument["documentDataList"] != null &&
          nextProps.allDocument["documentDataList"].length > 0
        ) {
          nextProps.allDocument["documentDataList"].map((res, i) => {
            if (res.userDocument != null) {
              tempArray.push(i);
            }
          });
        }
        /*
        if (
          tempArray.length + 1 ==
          nextProps.allDocument["documentDataList"].length
        ) {
          this.setState({
            isCompleted: true
          });
        } else {
          this.setState({
            isCompleted: false
          });
        } */

        break;
      case "GET_ALL_DOCUMENT_UPLOAD_SUCCESS":
        this.setState(
          {
            status: true,
            msg: "Document has been Uploaded",
            showLoader: true
          },
          () => this.getAllDocuent()
        );

        if (this.state.isCompleted) {
          //UPDATE THE SETP CALL
          this.props.applicationInstructionStep({
            scholarshipId: scholarshipId
          });
        }

        break;
      case "GET_ALL_DOCUMENT_UPLOAD_DELETE_SUCCESS":
        this.setState(
          {
            status: true,
            msg: "Document has been Deleted",
            showLoader: true,
            showConfirmationPopup: false,
            deleteSuccessCallBack: null
          },
          () => this.getAllDocuent()
        );

        if (this.state.isCompleted) {
          //UPDATE THE SETP CALL
          this.props.applicationInstructionStep({
            scholarshipId: scholarshipId
          });
        }
        break;

      case "GET_ALL_DOCUMENT_UPLOAD_DELETE_FAILURE":
        this.setState({
          status: false,
          msg: "No Record Deleted",
          showLoader: true,
          deleteSuccessCallBack: null,
          showConfirmationPopup: false
        });
        break;
      case "GET_ALL_DOCUMENT_UPLOAD_COMPLETE_SUCCESS":
        this.setState(
          {
            status: true,
            msg: "All Document has been Uploaded",
            showLoader: true,
            showConfirmationPopup: false,
            deleteSuccessCallBack: null
          },
          () => this.getAllDocuent()
        );
        break;
    }
  }

  getCategory(event, docId) {
    if (event.target.value == 7) {
      // for year
      this.state.isDropDownCategory[docId] = event.target.value;
    } else if (event.target.value == 8) {
      this.state.isDropDownCategory[docId] = event.target.value;
    } else {
      this.state.isDropDownCategory[docId] = "";
    }
    this.setState({
      isDropDownCategory: this.state.isDropDownCategory
    });
  }

  getCategoryOption(event, docId) {
    this.state.isDropDownOption[docId] = event.target.value;
    this.setState({
      isDropDownOption: this.state.isDropDownOption
    });
  }

  getValidationRulesObject(fieldID) {
    let validationObject = {};
    if (this.state.validations.hasOwnProperty(fieldID)) {
      validationObject.name = "Field";
      validationObject.validationFunctions = [required];
    }
    return validationObject;
  }

  checkValidation() {
    let userId = "";
    let scholarshipId = "";
    if (window != undefined) {
      userId = parseInt(gblFunc.getStoreUserDetails()["userId"]);
      scholarshipId = parseInt(window.localStorage.getItem("applicationSchID"));
    }
    let isValidate = this.checkFormValidations();
    if (isValidate) {
      this.props.completeDocument({
        userId: userId,
        scholarshipId: scholarshipId,
        steps: "DOCUMENTS"
      });
      setTimeout(() => {
        this.props.applicationInstructionStep({
          scholarshipId: scholarshipId
        });
      }, 1000);

      this.setState({
        // update state for go next
        isNext: true
      });
    }
  }

  checkFormValidations() {
    let validations = { ...this.state.validations };
    let isFormValid = true;
    for (let key in validations) {
      let { name, validationFunctions } = this.getValidationRulesObject(key);

      let documentList = this.state.documentList
        .filter(res => {
          return res.documentType["id"] == key;
        })
        .map(item => {
          return item.userDocument;
        });

      let validationResult = ruleRunner(
        documentList[0],
        key,
        name,
        ...validationFunctions
      );
      validations[key] = validationResult[key];
      if (validationResult[key] !== null) {
        isFormValid = false;
      }
    }
    this.setState({
      validations,
      isFormValid
    });
    return isFormValid;
  }

  close() {
    this.setState({
      showLoader: false,
      msg: "",
      status: false
    });
  }

  dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(","),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }

  cropImage(docObj) {
    if (typeof this.cropper.getCroppedCanvas() === "undefined") {
      return;
    }

    const selectedFile = this.state.docFile[0];

    let customizeFileName =
      selectedFile.name.substr(0, selectedFile.name.lastIndexOf(".")) +
      selectedFile.name
        .substr(selectedFile.name.lastIndexOf("."))
        .toLowerCase();

    var file = this.dataURLtoFile(
      this.cropper
        .getCroppedCanvas({ width: 640 })
        .toDataURL("image/jpeg", 0.8),
      customizeFileName
    );

    let reader = new FileReader(); // read upload file
    reader.readAsDataURL(file);

    this.setState(
      {
        cropResult: file,
        isShowCropPup: false
      },
      () => this.uploadDocumentWithOutCrop(docObj, Array(file))
    );
  }

  closePopUp() {
    this.setState({
      isShowCropPup: false
    });
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  render() {
    return (
      <section className="sectionwhite">
        {this.state.isNext ? (
          <Redirect
            to={`/application/${this.props.match.params.bsid}/form/entranceExam`}
          />
        ) : (
          ""
        )}
        <Loader isLoader={this.props.showLoader} />
        <AlertMessage
          close={this.close.bind(this)}
          isShow={this.state.showLoader}
          status={this.state.status}
          msg={this.state.msg}
        />
        <ConfirmMessagePopup
          message={"Are you sure want to delete ?"}
          showPopup={this.state.showConfirmationPopup}
          onConfirmationSuccess={this.state.deleteSuccessCallBack}
          onConfirmationFailure={this.hideConfirmationPopup}
        />
        <article className="form">
          <article className="row">
            <article className="col-md-12 ">
              {this.state.documentList != null &&
              this.state.documentList.length > 0
                ? this.state.documentList.map((res, i) => {
                    if ([6, 7].indexOf(res.documentType["id"]) > -1) {
                      // if the category and option are availables
                      return (
                        <article key={i} className="graystrip">
                          <span>{res.documentType["description"]}</span>
                          <article className="pull-right">
                            <article>
                              <input
                                onClick={event => {
                                  event.target.value = null;
                                }}
                                key={i}
                                type="file"
                                disabled={
                                  this.state.isDropDownCategory[
                                    res.documentType["id"]
                                  ] == ""
                                    ? true
                                    : false
                                }
                                className="btn top0"
                                onChange={event => this.getDocData(event, res)}
                              />
                              {this.state.validations != null &&
                              this.state.validations[res.documentType["id"]] ? (
                                <span className="error animated bounce">
                                  {
                                    this.state.validations[
                                      res.documentType["id"]
                                    ]
                                  }
                                </span>
                              ) : null}

                              <span className="doc-need pull-right">
                                <select
                                  onChange={event =>
                                    this.getCategory(
                                      event,
                                      res.documentType["id"]
                                    )
                                  }
                                >
                                  <option value="">Select Category</option>
                                  <option
                                    value="7"
                                    disabled={
                                      res.userDocument != null &&
                                      res.userDocument[0][
                                        "documentTypeCategory"
                                      ] &&
                                      res.userDocument[0][
                                        "documentTypeCategory"
                                      ]["id"] == 8
                                        ? true
                                        : false
                                    }
                                  >
                                    Year
                                  </option>
                                  <option
                                    value="8"
                                    disabled={
                                      res.userDocument != null &&
                                      res.userDocument[0][
                                        "documentTypeCategory"
                                      ] &&
                                      res.userDocument[0][
                                        "documentTypeCategory"
                                      ]["id"] == 7
                                        ? true
                                        : false
                                    }
                                  >
                                    Semester
                                  </option>
                                </select>
                              </span>
                              {this.state.isDropDownCategory[
                                res.documentType["id"]
                              ] == 7 ? (
                                <span className="doc-need pull-right">
                                  <select
                                    onChange={event =>
                                      this.getCategoryOption(
                                        event,
                                        res.documentType["id"]
                                      )
                                    }
                                  >
                                    <option>Select Year</option>
                                    {this.state.year.map((year, i) => {
                                      if (res.userDocument == null) {
                                        return (
                                          <option key={i} value={year.id}>
                                            {year.value}
                                          </option>
                                        );
                                      } else {
                                        let filterYr = res.userDocument.filter(
                                          filterYear => {
                                            return (
                                              filterYear[
                                                "documentTypeCategoryOption"
                                              ] != null &&
                                              filterYear[
                                                "documentTypeCategoryOption"
                                              ]["id"] == year.id
                                            );
                                          }
                                        );

                                        return (
                                          <option
                                            disabled={
                                              filterYr.length > 0 &&
                                              filterYr[0][
                                                "documentTypeCategoryOption"
                                              ] != null &&
                                              filterYr[0][
                                                "documentTypeCategoryOption"
                                              ]["id"] == year.id
                                                ? true
                                                : false
                                            }
                                            key={i}
                                            value={year.id}
                                          >
                                            {year.value}
                                          </option>
                                        );
                                      }
                                    })}
                                  </select>
                                </span>
                              ) : (
                                ""
                              )}

                              {this.state.isDropDownCategory[
                                res.documentType["id"]
                              ] == 8 ? (
                                <span className="doc-need pull-right">
                                  <select
                                    onChange={event =>
                                      this.getCategoryOption(
                                        event,
                                        res.documentType["id"]
                                      )
                                    }
                                  >
                                    <option value="">Select Semester</option>
                                    {this.state.semester.map((sem, i) => {
                                      if (res.userDocument == null) {
                                        return (
                                          <option key={i} value={sem.id}>
                                            {sem.value}
                                          </option>
                                        );
                                      } else {
                                        let filterSem = res.userDocument.filter(
                                          filterSem => {
                                            return (
                                              filterSem[
                                                "documentTypeCategoryOption"
                                              ] != null &&
                                              filterSem[
                                                "documentTypeCategoryOption"
                                              ]["id"] == sem.id
                                            );
                                          }
                                        );
                                        return (
                                          <option
                                            disabled={
                                              filterSem.length > 0 &&
                                              filterSem[0][
                                                "documentTypeCategoryOption"
                                              ] != null &&
                                              filterSem[0][
                                                "documentTypeCategoryOption"
                                              ]["id"] == sem.id
                                                ? true
                                                : false
                                            }
                                            key={i}
                                            value={sem.id}
                                          >
                                            {sem.value}
                                          </option>
                                        );
                                      }
                                    })}
                                  </select>
                                </span>
                              ) : (
                                ""
                              )}
                            </article>

                            {res.userDocument != null ? (
                              <ul>
                                {res.userDocument.map((doc, i) => {
                                  return (
                                    <li key={i}>
                                      <Link
                                        target="_blank"
                                        to={`${
                                          doc["location"] ? doc["location"] : ""
                                        }`}
                                      >
                                        {doc["documentDescription"]
                                          ? doc["documentDescription"]
                                          : ""}
                                      </Link>
                                      &nbsp;&nbsp;
                                      <strong>
                                        {doc["documentTypeCategory"] != null &&
                                          doc["documentTypeCategory"][
                                            "categoryName"
                                          ]}
                                      </strong>
                                      &nbsp;&nbsp;
                                      <strong>
                                        {doc["documentTypeCategoryOption"] !=
                                          null &&
                                          doc["documentTypeCategoryOption"][
                                            "categoryName"
                                          ]}
                                      </strong>
                                      &nbsp;&nbsp;
                                      <span
                                        onClick={event =>
                                          this.deleteDocument(doc["id"], res)
                                        }
                                        key={i}
                                        className="delete top"
                                      >
                                        &nbsp;
                                      </span>
                                    </li>
                                  );
                                })}
                              </ul>
                            ) : (
                              ""
                            )}
                          </article>
                        </article>
                      );
                    } else {
                      // if category and option are not available
                      return (
                        <article key={i}>
                          <article className="graystrip">
                            <span>{res.documentType["description"]}</span>
                            <article className="pull-right docicon">
                              {res.userDocument == null ? (
                                <article>
                                  <article className="document">
                                    <label className="custom-file-upload">
                                      <input
                                        onClick={event => {
                                          event.target.value = null;
                                        }}
                                        key={i}
                                        type="file"
                                        disabled={
                                          this.state.isDropDownCategory[
                                            res.documentType["id"]
                                          ] == ""
                                            ? true
                                            : false
                                        }
                                        className="btn top0"
                                        onChange={event =>
                                          this.getDocData(event, res)
                                        }
                                      />
                                      Browse
                                    </label>
                                  </article>

                                  {this.state.validations != null &&
                                  this.state.validations[
                                    res.documentType["id"]
                                  ] ? (
                                    <article className="form-group">
                                      <span className="error errordoc animated bounce">
                                        {
                                          this.state.validations[
                                            res.documentType["id"]
                                          ]
                                        }
                                      </span>
                                    </article>
                                  ) : null}
                                </article>
                              ) : (
                                <span>
                                  <Link
                                    target="_blank"
                                    to={`${res.userDocument[0]["location"]}`}
                                  >
                                    {/* {res.userDocument[0]["documentDescription"]}{" "} */}
                                    <i className="fa fa-download Checkicon">
                                      &nbsp;
                                    </i>
                                    &nbsp;&nbsp;
                                  </Link>

                                  {res.verificationStatus != 1 && (
                                    <span
                                      onClick={event =>
                                        this.deleteDocument(
                                          res.userDocument[0]["id"],
                                          res
                                        )
                                      }
                                      key={i}
                                      className="deleteicon"
                                    >
                                      &nbsp;
                                    </span>
                                  )}
                                </span>
                              )}
                            </article>
                          </article>

                          {this.state.isShowCropPup &&
                          res.documentType["id"] == 1 ? (
                            <section className="cropImg">
                              <section className="cropImgbg">
                                <article className="popup-header">
                                  <button
                                    onClick={() => this.closePopUp()}
                                    type="button"
                                    className="close"
                                  >
                                    ×
                                  </button>
                                  <h3 className="popup-title">CROP IMAGE</h3>
                                </article>
                                <article className="popup-body">
                                  <article className="row">
                                    <article className="col-md-12">
                                      <article className="cropArea">
                                        <Cropper
                                          style={{
                                            height: "50vh",
                                            width: "100%"
                                          }}
                                          aspectRatio={1}
                                          zoomable={true}
                                          autoCropArea={1}
                                          preview=".img-preview"
                                          guides={false}
                                          src={this.state.cropSrc}
                                          /* minCropBoxWidth = {128}
                                          minCropBoxHeight= {128}
                                          cropBoxMovable= {false}
                                          cropBoxResizable = {false} */
                                          ref={cropper => {
                                            this.cropper = cropper;
                                          }}
                                        />
                                      </article>
                                    </article>
                                  </article>
                                  <article className="row">
                                    <article className="col-md-12 text-center">
                                      <button
                                        className="updatePicBtn"
                                        onClick={event => this.cropImage(res)}
                                      >
                                        Update Picture
                                      </button>
                                    </article>
                                  </article>
                                </article>
                                <article className="popup-footer" />
                              </section>
                            </section>
                          ) : (
                            ""
                          )}
                        </article>
                      );
                    }
                  })
                : ""}

              <article className="row">
                <article className="col-md-12 noPaddingBG">
                  <input
                    type="submit"
                    value="Save & Continue"
                    onClick={this.checkValidation}
                    className="btn pull-right"
                  />
                </article>
              </article>
            </article>
          </article>
          <article />
        </article>
      </section>
    );
  }
}

// <article popup-box-effect="" onClick={this.togglePopup.bind(this)}>
//   <article className="teamData box-height boxEffect border-bottom lineHover position">
//     {/* <img src={manjeet} /> */}
//     <h3>Manjeet Singh (Director &amp; Co-CEO)</h3>
//     <h4>Co-Founder</h4>
//   </article>
// </article>
// {this.state.showPopup ? (
//   <CropImg text="Close Me" closePopup={this.togglePopup.bind(this)} />

class CropImg extends React.Component {
  render() {
    return (
      <article className="popup">
        <article className="popupsms">
          <article className="popup_inner loginpopup">
            <article className="LoginRegPopup">
              <section className="modal fade modelAuthPopup1">
                <section className="modal-dialog">
                  <section className="modal-content modelBg  emailverification">
                    <article className="modal-header">
                      <h3 className="modal-title">Forgot Password</h3>
                      <button type="button" className="close btnPos">
                        <i>&times;</i>
                      </button>
                    </article>

                    <article className="modal-body forgot">
                      <article className="row">
                        <article className="col-md-12">
                          <article className="cropArea">
                            <Cropper
                              style={{ height: "50vh", width: "100%" }}
                              aspectRatio={1}
                              zoomable={true}
                              autoCropArea={1}
                              preview=".img-preview"
                              guides={false}
                              src={this.state.cropSrc}
                              ref={cropper => {
                                this.cropper = cropper;
                              }}
                            />
                          </article>
                        </article>
                      </article>
                    </article>
                  </section>
                </section>
              </section>
            </article>
          </article>
        </article>
      </article>
    );
  }
}

export default DocumentNeeded;
