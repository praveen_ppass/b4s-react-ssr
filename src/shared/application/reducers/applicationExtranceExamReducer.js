import {
  FETCH_APPLICATION_FORMS_EXAM_REQUEST,
  FETCH_APPLICATION_FORMS_EXAM_SUCCESS,
  FETCH_APPLICATION_FORMS_EXAM_FAILURE,
  UPDATE_APPLICATION_FORMS_EXAM_REQUEST,
  UPDATE_APPLICATION_FORMS_EXAM_SUCCESS,
  UPDATE_APPLICATION_FORMS_EXAM_FAILURE,
  DELETE_APPLICATION_FORMS_EXAM_REQUEST,
  DELETE_APPLICATION_FORMS_EXAM_SUCCESS,
  DELETE_APPLICATION_FORMS_EXAM_FAILURE
} from "../actions/applicationEntranceExamAction";

const initialState = {
  applicationExam: null,
  isError: false,
  error: null
};

const applicationExamReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_APPLICATION_FORMS_EXAM_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case FETCH_APPLICATION_FORMS_EXAM_SUCCESS:
      return {
        ...state,
        applicationExam: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case FETCH_APPLICATION_FORMS_EXAM_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case UPDATE_APPLICATION_FORMS_EXAM_REQUEST:
      return {
        ...state,
        type,
        showLoader: true
      };

    case UPDATE_APPLICATION_FORMS_EXAM_SUCCESS:
      return {
        ...state,
        type,
        showLoader: false
      };

    case UPDATE_APPLICATION_FORMS_EXAM_FAILURE:
      return {
        ...state,
        type,
        showLoader: false
      };
    case DELETE_APPLICATION_FORMS_EXAM_REQUEST:
      return {
        ...state,
        type,
        showLoader: true
      };

    case DELETE_APPLICATION_FORMS_EXAM_SUCCESS:
      return {
        ...state,
        type,
        showLoader: false
      };

    case DELETE_APPLICATION_FORMS_EXAM_FAILURE:
      return {
        ...state,
        type,
        showLoader: false
      };

    default:
      return state;
  }
};

export default applicationExamReducer;
