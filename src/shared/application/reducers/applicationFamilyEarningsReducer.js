import {
  FETCH_APPLICATION_FORMS_FAMILYEARNING_REQUEST,
  FETCH_APPLICATION_FORMS_FAMILYEARNING_SUCCESS,
  FETCH_APPLICATION_FORMS_FAMILYEARNING_FAILURE,
  UPDATE_APPLICATION_FORMS_FAMILYEARNING_REQUEST,
  UPDATE_APPLICATION_FORMS_FAMILYEARNING_SUCCESS,
  UPDATE_APPLICATION_FORMS_FAMILYEARNING_FAILURE,
  DELETE_APPLICATION_FORMS_FAMILYEARNING_REQUEST,
  DELETE_APPLICATION_FORMS_FAMILYEARNING_SUCCESS,
  DELETE_APPLICATION_FORMS_FAMILYEARNING_FAILURE
} from "../actions/familyEarningsActions";

const initialState = {
  applicationFamilyEarningData: null,
  isError: false,
  error: null
};
const applicationFamilyEarningsReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case FETCH_APPLICATION_FORMS_FAMILYEARNING_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case FETCH_APPLICATION_FORMS_FAMILYEARNING_SUCCESS:
      return {
        ...state,
        applicationFamilyEarningData: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case FETCH_APPLICATION_FORMS_FAMILYEARNING_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case UPDATE_APPLICATION_FORMS_FAMILYEARNING_REQUEST:
      return {
        ...state,
        type,
        showLoader: true
      };

    case UPDATE_APPLICATION_FORMS_FAMILYEARNING_SUCCESS:
      return {
        ...state,
        type,
        showLoader: false
      };

    case UPDATE_APPLICATION_FORMS_FAMILYEARNING_FAILURE:
      return {
        ...state,
        type,
        showLoader: false
      };
    case DELETE_APPLICATION_FORMS_FAMILYEARNING_REQUEST:
      return {
        ...state,
        type,
        showLoader: true
      };

    case DELETE_APPLICATION_FORMS_FAMILYEARNING_SUCCESS:
      return {
        ...state,
        type,
        showLoader: false
      };

    case DELETE_APPLICATION_FORMS_FAMILYEARNING_FAILURE:
      return {
        ...state,
        type,
        showLoader: false
      };

    default:
      return state;
  }
};

export default applicationFamilyEarningsReducer;
