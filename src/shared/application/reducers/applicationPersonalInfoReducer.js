import {
  APPLICATION_FETCH_PERSONAL_INFO_REQUESTED,
  APPLICATION_FETCH_PERSONAL_INFO_SUCCEEDED,
  APPLICATION_FETCH_PERSONAL_INFO_FAILED,
  FETCH_PERSONAL_INFO_CONFIG_REQUESTED,
  FETCH_PERSONAL_INFO_CONFIG_SUCCEEDED,
  FETCH_PERSONAL_INFO_CONFIG_FAILED,
  APPLICATION_UPDATE_PERSONAL_INFO_REQUESTED,
  APPLICATION_UPDATE_PERSONAL_INFO_SUCCEEDED,
  APPLICATION_UPDATE_PERSONAL_INFO_FAILED
} from "../actions/personalInfoActions";

const initialState = {
  personalInfoConfig: null,
  showLoader: false,
  personalInfoData: null
};
const applicationPersonalInfoReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    /************** Personal Info  get start  ************************** */

    case "APPLICATION_FETCH_PERSONAL_INFO_REQUESTED":
      return {
        ...state,
        payload,
        showLoader: true,
        type
      };
    case "APPLICATION_FETCH_PERSONAL_INFO_SUCCEEDED":
      return {
        ...state,
        personalInfoData: payload,
        showLoader: false,
        type
      };
    case "APPLICATION_FETCH_PERSONAL_INFO_FAILED":
      return {
        ...state,
        payload,
        showLoader: false,
        type
      };

    /************** Personal Info  get end  ************************** */

    /************** Personal Info config get start  ************************** */

    case "FETCH_PERSONAL_INFO_CONFIG_REQUESTED":
      return Object.assign({}, state, {
        showLoader: true,
        isError: false,
        type
      });

    case "FETCH_PERSONAL_INFO_CONFIG_SUCCEEDED":
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        personalInfoConfig: payload
      });

    case "FETCH_PERSONAL_INFO_CONFIG_FAILED":
      return {
        ...state,
        userInfoData: payload,
        showLoader: false,
        type,
        updateError: "",
        isUpdateError: false
      };
    /************** Personal Info config get end  ************************** */

    /************** Personal Info update start  ************************** */
    case "APPLICATION_UPDATE_PERSONAL_INFO_REQUESTED":
      return {
        ...state,
        payload,
        showLoader: true,
        type
      };

    case "APPLICATION_UPDATE_PERSONAL_INFO_SUCCEEDED":
      return {
        ...state,
        payload,
        showLoader: false,
        type
      };

    case "APPLICATION_FETCH_PERSONAL_INFO_FAILED":
      return {
        ...state,
        payload,
        showLoader: false,
        type
      };
    /************** Personal Info update end  ************************** */

    default:
      return state;
  }
};

export default applicationPersonalInfoReducer;
