import {
  FETCH_APPLICATION_FORMS_AWARDWON_REQUEST,
  FETCH_APPLICATION_FORMS_AWARDWON_SUCCESS,
  FETCH_APPLICATION_FORMS_AWARDWON_FAILURE,
  UPDATE_APPLICATION_FORMS_AWARDWON_REQUEST,
  UPDATE_APPLICATION_FORMS_AWARDWON_SUCCESS,
  UPDATE_APPLICATION_FORMS_AWARDWON_FAILURE,
  DELETE_APPLICATION_FORMS_AWARDWON_REQUEST,
  DELETE_APPLICATION_FORMS_AWARDWON_SUCCESS,
  DELETE_APPLICATION_FORMS_AWARDWON_FAILURE
} from "../actions/applicationAwardWonAction";

const initialState = {
  applicationAwardWonData: null,
  isError: false,
  error: null
};

const applicationAwardWonReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case FETCH_APPLICATION_FORMS_AWARDWON_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case FETCH_APPLICATION_FORMS_AWARDWON_SUCCESS:
      return {
        ...state,
        applicationAwardWonData: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case FETCH_APPLICATION_FORMS_AWARDWON_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case UPDATE_APPLICATION_FORMS_AWARDWON_REQUEST:
      return {
        ...state,
        type,
        showLoader: true
      };

    case UPDATE_APPLICATION_FORMS_AWARDWON_SUCCESS:
      return {
        ...state,
        type,
        showLoader: false
      };

    case UPDATE_APPLICATION_FORMS_AWARDWON_FAILURE:
      return {
        ...state,
        type,
        showLoader: false
      };
    case DELETE_APPLICATION_FORMS_AWARDWON_REQUEST:
      return {
        ...state,
        type,
        showLoader: true
      };

    case DELETE_APPLICATION_FORMS_AWARDWON_SUCCESS:
      return {
        ...state,
        type,
        showLoader: false
      };

    case DELETE_APPLICATION_FORMS_AWARDWON_FAILURE:
      return {
        ...state,
        type,
        showLoader: false
      };

    default:
      return state;
  }
};

export default applicationAwardWonReducer;
