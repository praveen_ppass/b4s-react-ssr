import {
  LOGIN_USER_REQUESTED,
  LOGIN_USER_SUCCEEDED,
  LOGIN_USER_FAILED,
  FORGOT_USER_PASSWORD_REQUEST,
  FORGOT_USER_PASSWORD_FAILURE,
  FORGOT_USER_PASSWORD_SUCCESS,
  SOCIAL_LOGIN_USER_REQUESTED,
  SOCIAL_LOGIN_USER_SUCCEEDED,
  SOCIAL_LOGIN_USER_FAILED,
  LOG_USER_OUT,
  LOG_USER_OUT_FAILED,
  LOG_USER_OUT_SUCCEEDED,
  REGISTER_USER_REQUEST,
  REGISTER_USER_FAILED,
  REGISTER_USER_SUCCEEDED
} from "../../login/actions";

import {
  FETCH_APPLICATION_INSTRUCTION_REQUEST,
  FETCH_APPLICATION_INSTRUCTION_SUCCESS,
  FETCH_APPLICATION_INSTRUCTION_FAILURE,
  FETCH_SCHOLARSHIP_CATEGORY_REQUEST,
  FETCH_SCHOLARSHIP_CATEGORY_SUCCESS,
  FETCH_SCHOLARSHIP_CATEGORY_FAILURE,
  SAVE_SCHOLARSHIP_TYPE_REQUEST,
  SAVE_SCHOLARSHIP_TYPE_SUCCESS,
  SAVE_SCHOLARSHIP_TYPE_FAILURE,
  CHECK_DISBURSAL_STATUS_REQUEST,
  CHECK_DISBURSAL_STATUS_SUCCESS,
  CHECK_DISBURSAL_STATUS_FAILURE
} from "../actions/applicationInstructionActions";
import {
  FETCH_USER_RULES_REQUEST,
  FETCH_USER_RULES_SUCCESS,
  FETCH_USER_RULES_FAILURE,
  FETCH_SCHOLARSHIP_APPLY_REQUEST,
  FETCH_SCHOLARSHIP_APPLY_SUCCESS,
  FETCH_SCHOLARSHIP_APPLY_FAILURE
} from "../../../constants/commonActions";

import {
  FETCH_APPLICATION_STATUS_REQUEST,
  FETCH_APPLICATION_STATUS_SUCCESS,
  FETCH_APPLICATION_STATUS_FAIL
} from "../../dashboard/actions";

const initialState = {
  applicationInstruction: null,
  isError: false,
  error: null
};

const applicationInstructionReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    /* Login related actions */
    case LOGIN_USER_REQUESTED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case REGISTER_USER_REQUEST:
      return {
        ...state,
        showLoader: true,
        isError: false
      };

    case REGISTER_USER_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        serverErrorRegister:payload
      };

    case REGISTER_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type
      };
    case "VERIFY_TOKEN":
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload,
      //  isAuthenticated: true,
        userRulesPresent: false
      };

    case LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        isError: false,
        type,
        userLoginData: payload
      };

    case LOGIN_USER_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        serverErrorLogin: payload,
        isError: true
      };

    /* Forgot password related actions*/
    case FORGOT_USER_PASSWORD_REQUEST:
      return {
        ...state,
        type,
        showLoader: true,
        serverErrorForgotPassword: ""
      };

    case FORGOT_USER_PASSWORD_SUCCESS:
      return { ...state, type, showLoader: false };

    case FORGOT_USER_PASSWORD_FAILURE:
      return {
        ...state,
        type,
        showLoader: false,
        isError: true,
        serverErrorForgotPassword: payload
      };

    /*  Social login related actions */
    case SOCIAL_LOGIN_USER_REQUESTED:
      return { ...state, type, showLoader: true };

    case SOCIAL_LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type,
        userLoginData: payload,
        isAuthenticated: true
      };

    case SOCIAL_LOGIN_USER_FAILED:
      return {
        ...state,
        showLoader: false,
        isError: true,
        type,
        serverErrorLogin: payload
      };

    case LOG_USER_OUT:
      return { ...state, showLoader: true, isError: false, type };

    case LOG_USER_OUT_FAILED:
      return { ...state, showLoader: false, isError: true, type };

    case LOG_USER_OUT_SUCCEEDED:
      return { ...state, showLoader: false, isError: false, type };

    case FETCH_APPLICATION_STATUS_REQUEST:
      return { ...state, showLoader: true, isError: false, type };

    case FETCH_APPLICATION_STATUS_SUCCESS:
      return {
        ...state,
        showLoader: false,
        applicationStatus: payload,
        isError: false,
        type
      };

    case FETCH_APPLICATION_STATUS_FAIL:
      return { ...state, showLoader: false, isError: true, type };

    case FETCH_APPLICATION_INSTRUCTION_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        applicationInstruction: null
      };

    case FETCH_APPLICATION_INSTRUCTION_SUCCESS:
      return {
        ...state,
        applicationInstruction: payload,
        type,
        showLoader: false
      };
    case FETCH_APPLICATION_INSTRUCTION_FAILURE:
      return { ...state, showLoader: false, isError: true, type };

    case FETCH_SCHOLARSHIP_CATEGORY_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        scholarshipCategory: null
      };

    case FETCH_SCHOLARSHIP_CATEGORY_SUCCESS:
      return {
        ...state,
        scholarshipCategory: payload,
        type,
        showLoader: false
      };
    case FETCH_SCHOLARSHIP_CATEGORY_FAILURE:
      return { ...state, showLoader: false, isError: true, type };
    case SAVE_SCHOLARSHIP_TYPE_REQUEST:
      return {
        ...state,
        showLoader: true,
        type
      };

    case SAVE_SCHOLARSHIP_TYPE_SUCCESS:
      return {
        ...state,
        schType: payload,
        type,
        showLoader: false
      };
    case SAVE_SCHOLARSHIP_TYPE_FAILURE:
      return {
        ...state,
        showLoader: false,
        schType: payload,
        isError: true,
        type
      };
    case FETCH_USER_RULES_REQUEST:
      return {
        ...state,
        showLoader: true,
        isError: false,
        type
      };

    case FETCH_USER_RULES_SUCCESS:
      return {
        ...state,
        showLoader: false,
        userRulesData: payload,
        isError: false,
        type
      };

    case FETCH_USER_RULES_FAILURE:
      return {
        ...state,
        showLoader: false,
        isError: true,
        type
      };
    case FETCH_SCHOLARSHIP_APPLY_REQUEST:
      return Object.assign({}, state, { showLoader: true, isError: false });

    case FETCH_SCHOLARSHIP_APPLY_SUCCESS:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        schApply: payload
      });

    case FETCH_SCHOLARSHIP_APPLY_FAILURE:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });
    case CHECK_DISBURSAL_STATUS_REQUEST:
        return {
        ...state,
        showLoader: true,
        isError: false,
        type
        };
    
      case CHECK_DISBURSAL_STATUS_SUCCESS:
        return {
        ...state,
        showLoader: false,
        disbursalStatus: payload,
        isError: false,
        type
        };
    
      case CHECK_DISBURSAL_STATUS_FAILURE:
        return {
        ...state,
        showLoader: false,
        isError: true,
        type
        };
    default:
       return state;
  }
};

export default applicationInstructionReducer;
