import {
  GET_ALL_SUMMERY_RULES_REQUEST,
  GET_ALL_SUMMERY_RULES_SUCCESS,
  GET_ALL_SUMMERY_RULES_FAILURE,
  FETCH_SCHOLARSHIP_APPLY_REQUEST_SUMMARY,
  FETCH_SCHOLARSHIP_APPLY_SUCCESS_SUMMARY,
  FETCH_SCHOLARSHIP_APPLY_FAILURE_SUMMARY
} from "../actions/applicationSummeryAction";

import {
  SAVE_SCHOLARSHIP_TYPE_REQUEST,
  SAVE_SCHOLARSHIP_TYPE_SUCCESS,
  SAVE_SCHOLARSHIP_TYPE_FAILURE
} from "../actions/applicationInstructionActions";
import {
  FETCH_APPLICATION_STEP_REQUEST,
  FETCH_APPLICATION_STEP_SUCCESS,
  FETCH_APPLICATION_STEP_FAILURE
} from "../actions/applicationFormAction";
import {
  FETCH_USER_RULES_REQUEST,
  FETCH_USER_RULES_SUCCESS
} from "../../../constants/commonActions";

const initialState = {
  summeryData: null,
  isError: false,
  error: null
};

const applicationSummeryReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_ALL_SUMMERY_RULES_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case GET_ALL_SUMMERY_RULES_SUCCESS:
      return {
        ...state,
        summeryData: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case GET_ALL_SUMMERY_RULES_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case SAVE_SCHOLARSHIP_TYPE_REQUEST:
      return {
        ...state,
        showLoader: true,
        type
      };

    case SAVE_SCHOLARSHIP_TYPE_SUCCESS:
      return {
        ...state,
        schType: payload,
        type,
        showLoader: false
      };
    case SAVE_SCHOLARSHIP_TYPE_FAILURE:
      return { ...state, showLoader: false, isError: true, type };

    case FETCH_SCHOLARSHIP_APPLY_REQUEST_SUMMARY:
      return Object.assign({}, state, { showLoader: true, isError: false });

    case FETCH_SCHOLARSHIP_APPLY_SUCCESS_SUMMARY:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        schApply: payload
      });

    case FETCH_SCHOLARSHIP_APPLY_FAILURE_SUMMARY:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });
    case FETCH_APPLICATION_STEP_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        instructionsStep: null
      };

    case FETCH_APPLICATION_STEP_SUCCESS:
      return {
        ...state,
        instructionsStep: payload,
        type,
        showLoader: false
      };
    case FETCH_APPLICATION_STEP_FAILURE:
      return { ...state, showLoader: false, isError: true, type };
    case FETCH_USER_RULES_REQUEST:
      return { ...state, showLoader: true, type, isError: false };

    case FETCH_USER_RULES_SUCCESS:
      return {
        ...state,
        showLoader: false,
        userRulesData: payload,
        type,
        isError: false
      };
    default:
      return state;
  }
};

export default applicationSummeryReducer;
