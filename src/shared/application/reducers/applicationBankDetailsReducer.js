import {
  FETCH_APPLICATION_BANK_DETAILS_SUCCESS,
  FETCH_APPLICATION_BANK_DETAILS_REQUEST,
  FETCH_APPLICATION_BANK_DETAILS_FAILURE,
  SAVE_APPLICATION_BANK_DETAILS_REQUEST,
  SAVE_APPLICATION_BANK_DETAILS_SUCCESS,
  SAVE_APPLICATION_BANK_DETAILS_FAILURE,
  FETCH_BANK_DTS_CONFIG_REQUEST,
  FETCH_BANK_DTS_CONFIG_SUCCESS,
  FETCH_BANK_DTS_CONFIG_FAILURE,
  UPDATE_BANK_DT_STEP_REQUEST,
  UPDATE_BANK_DT_STEP_SUCCESS,
  UPDATE_BANK_DT_STEP_FAILURE
} from "../actions/applicationBankDetailsAction";

const initialState = {
  bankDetails: [],
  updateBankDetails: [],
  bankDtsConfig: {},
  isError: false,
  error: null
};

const applicationBankDtsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_APPLICATION_BANK_DETAILS_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case FETCH_APPLICATION_BANK_DETAILS_SUCCESS:
      return {
        ...state,
        bankDetails: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case FETCH_APPLICATION_BANK_DETAILS_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case SAVE_APPLICATION_BANK_DETAILS_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case SAVE_APPLICATION_BANK_DETAILS_SUCCESS:
      return {
        ...state,
        updateBankDetails: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case SAVE_APPLICATION_BANK_DETAILS_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case FETCH_BANK_DTS_CONFIG_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case FETCH_BANK_DTS_CONFIG_SUCCESS:
      return {
        ...state,
        bankDtsConfig: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case FETCH_BANK_DTS_CONFIG_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case UPDATE_BANK_DT_STEP_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case UPDATE_BANK_DT_STEP_SUCCESS:
      return {
        ...state,
        bankDtsStep: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case UPDATE_BANK_DT_STEP_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    default:
      return state;
  }
};

export default applicationBankDtsReducer;
