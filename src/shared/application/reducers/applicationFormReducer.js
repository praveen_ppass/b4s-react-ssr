import {
  GET_ALL_RULES_REQUEST,
  GET_ALL_RULES_SUCCESS,
  GET_ALL_RULES_FAILURE,
  FETCH_APPLICATION_STEP_REQUEST,
  FETCH_APPLICATION_STEP_SUCCESS,
  FETCH_APPLICATION_STEP_FAILURE,
  FETCH_SCHOLARSHIP_APPLICATION_STEP_FAILURE,
  FETCH_SCHOLARSHIP_APPLICATION_STEP_REQUEST,
  FETCH_SCHOLARSHIP_APPLICATION_STEP_SUCCESS,
  FETCH_APPLICATION_FORM_INSTRUCTION_REQUEST,
  FETCH_APPLICATION_FORM_INSTRUCTION_SUCCESS,
  FETCH_APPLICATION_FORM_INSTRUCTION_FAILURE
} from "../actions/applicationFormAction";
import {
  FETCH_SCHOLARSHIP_APPLY_REQUEST,
  FETCH_SCHOLARSHIP_APPLY_SUCCESS,
  FETCH_SCHOLARSHIP_APPLY_FAILURE
} from "../../../constants/commonActions";
import gblFunc from "../../../globals/globalFunctions";

const initialState = {
  rulesData: null,
  isError: false,
  error: null
};

const applicationFormReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_ALL_RULES_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case GET_ALL_RULES_SUCCESS:
      return {
        ...state,
        rulesData: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case GET_ALL_RULES_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case FETCH_APPLICATION_FORM_INSTRUCTION_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        instructions: null
      };

    case FETCH_APPLICATION_FORM_INSTRUCTION_SUCCESS:
      return {
        ...state,
        instructions: payload,
        type,
        showLoader: false
      };
    case FETCH_APPLICATION_FORM_INSTRUCTION_FAILURE:
      return { ...state, showLoader: false, isError: true, type };

    case FETCH_APPLICATION_STEP_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        instructionsStep: null
      };

    case FETCH_APPLICATION_STEP_SUCCESS:
      return {
        ...state,
        instructionsStep: payload,
        type,
        showLoader: false
      };
    case FETCH_APPLICATION_STEP_FAILURE:
      return { ...state, showLoader: false, isError: true, type };

    case FETCH_SCHOLARSHIP_APPLICATION_STEP_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        schApplicableStep: null,
        redirectionSteps: []
      };

    case FETCH_SCHOLARSHIP_APPLICATION_STEP_SUCCESS:
      let redirectionSteps = [];
      if (payload && payload.length) {
        redirectionSteps = payload.map(list => {
          return gblFunc.camcelCase(list.step);
        });

        redirectionSteps.push("summary");
      }
      return {
        ...state,
        schApplicableStep: payload,
        redirectionSteps,
        type,
        showLoader: false
      };
    case FETCH_SCHOLARSHIP_APPLICATION_STEP_FAILURE:
      return { ...state, showLoader: false, isError: true, type };
    case FETCH_SCHOLARSHIP_APPLY_REQUEST:
      return Object.assign({}, state, { showLoader: true, isError: false });

    case FETCH_SCHOLARSHIP_APPLY_SUCCESS:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        schApply: payload
      });

    case FETCH_SCHOLARSHIP_APPLY_FAILURE:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });
    default:
      return state;
  }
};

export default applicationFormReducer;
