import {
  FETCH_APPLICATION_FORMS_REFERENCE_REQUEST,
  FETCH_APPLICATION_FORMS_REFERENCE_SUCCESS,
  FETCH_APPLICATION_FORMS_REFERENCE_FAILURE,
  UPDATE_APPLICATION_FORMS_REFERENCE_REQUEST,
  UPDATE_APPLICATION_FORMS_REFERENCE_SUCCESS,
  UPDATE_APPLICATION_FORMS_REFERENCE_FAILURE,
  DELETE_APPLICATION_FORMS_REFERENCE_REQUEST,
  DELETE_APPLICATION_FORMS_REFERENCE_SUCCESS,
  DELETE_APPLICATION_FORMS_REFERENCE_FAILURE,
  UPDATE_STEP_REFERENCE_FAILURE,
  UPDATE_STEP_REFERENCE_SUCCESS,
  UPDATE_STEP_REFERENCE_REQUEST
} from "../actions/referenceAction";
import {
  FETCH_APPLICATION_STEP_REQUEST,
  FETCH_APPLICATION_STEP_SUCCESS,
  FETCH_APPLICATION_STEP_FAILURE
} from "../actions/applicationFormAction";

const initialState = {
  applicationReferenceData: null,
  isError: false,
  error: null
};

const applicationReferenceReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case FETCH_APPLICATION_FORMS_REFERENCE_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case FETCH_APPLICATION_FORMS_REFERENCE_SUCCESS:
      return {
        ...state,
        applicationReferenceData: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case FETCH_APPLICATION_FORMS_REFERENCE_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case UPDATE_APPLICATION_FORMS_REFERENCE_REQUEST:
      return {
        ...state,
        type,
        showLoader: true
      };

    case UPDATE_APPLICATION_FORMS_REFERENCE_SUCCESS:
      return {
        ...state,
        type,
        showLoader: false
      };

    case UPDATE_APPLICATION_FORMS_REFERENCE_FAILURE:
      return {
        ...state,
        type,
        showLoader: false
      };
    case DELETE_APPLICATION_FORMS_REFERENCE_REQUEST:
      return {
        ...state,
        type,
        showLoader: true
      };

    case DELETE_APPLICATION_FORMS_REFERENCE_SUCCESS:
      return {
        ...state,
        type,
        showLoader: false
      };

    case DELETE_APPLICATION_FORMS_REFERENCE_FAILURE:
      return {
        ...state,
        type,
        showLoader: false
      };
    case UPDATE_STEP_REFERENCE_REQUEST:
      return {
        ...state,
        type,
        showLoader: true
      };

    case UPDATE_STEP_REFERENCE_SUCCESS:
      return {
        ...state,
        type,
        showLoader: false
      };

    case UPDATE_STEP_REFERENCE_FAILURE:
      return {
        ...state,
        type,
payload,
        showLoader: false
      };
    case FETCH_APPLICATION_STEP_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        instructionsStep: null
      };

    case FETCH_APPLICATION_STEP_SUCCESS:
      return {
        ...state,
        instructionsStep: payload,
        type,
        showLoader: false
      };
    case FETCH_APPLICATION_STEP_FAILURE:
      return { ...state, showLoader: false, isError: true, type };
    default:
      return state;
  }
};

export default applicationReferenceReducer;
