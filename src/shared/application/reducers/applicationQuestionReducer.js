import {
  FETCH_APPLICATION_FORMS_QUESTION_REQUEST,
  FETCH_APPLICATION_FORMS_QUESTION_SUCCESS,
  FETCH_APPLICATION_FORMS_QUESTION_FAILURE,
  UPDATE_APPLICATION_FORMS_QUESTION_REQUEST,
  UPDATE_APPLICATION_FORMS_QUESTION_SUCCESS,
  UPDATE_APPLICATION_FORMS_QUESTION_FAILURE
} from "../actions/questionAction";

const initialState = {
  applicationQuestionData: null,
  isError: false,
  error: null
};

const applicationQuestionReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case FETCH_APPLICATION_FORMS_QUESTION_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case FETCH_APPLICATION_FORMS_QUESTION_SUCCESS:
      return {
        ...state,
        applicationQuestionData: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case FETCH_APPLICATION_FORMS_QUESTION_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case UPDATE_APPLICATION_FORMS_QUESTION_REQUEST:
      return {
        ...state,
        type: type,
        showLoader: true
      };

    case UPDATE_APPLICATION_FORMS_QUESTION_SUCCESS:
      return {
        ...state,
        type: type,
        showLoader: false
      };

    case UPDATE_APPLICATION_FORMS_QUESTION_FAILURE:
      return {
        ...state,
        type: type,
        showLoader: false
      };

    default:
      return state;
  }
};

export default applicationQuestionReducer;
