import {
  FETCH_USER_CODE_REQUEST,
  FETCH_USER_CODE_SUCCESS,
  FETCH_USER_CODE_FAIL
} from "../actions/hulLoginFormAction";
import {
  LOG_USER_OUT,
  LOG_USER_OUT_FAILED,
  LOG_USER_OUT_SUCCEEDED
} from "../../login/actions";

const initialState = {
  userCode: null,
  showLoader: false
};

const HulLoginFormReducer = (state = initialState, { payload, type }) => {
  switch (type) {
    case FETCH_USER_CODE_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type
      };

    case FETCH_USER_CODE_SUCCESS:
      return {
        ...state,
        userCode: payload,
        showLoader: false,
        type
      };

    case FETCH_USER_CODE_FAIL:
      return {
        ...state,
        serverUserCodeErrors: "Invalid User Code",
        showLoader: false,
        type
      };
    case LOG_USER_OUT:
      return { ...state, showLoader: true, isError: false, type };

    case LOG_USER_OUT_FAILED:
      return { ...state, showLoader: false, isError: true, type };

    case LOG_USER_OUT_SUCCEEDED:
      return { ...state, showLoader: false, isError: false, type };
    default:
      return state;
  }
};

export default HulLoginFormReducer;
