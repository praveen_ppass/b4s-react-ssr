import {
  GET_ALL_DOCUMENT_REQUEST,
  GET_ALL_DOCUMENT_SUCCESS,
  GET_ALL_DOCUMENT_FAILURE,
  GET_ALL_DOCUMENT_UPLOAD_REQUEST,
  GET_ALL_DOCUMENT_UPLOAD_SUCCESS,
  GET_ALL_DOCUMENT_UPLOAD_FAILURE,
  GET_ALL_DOCUMENT_UPLOAD_DELETE_REQUEST,
  GET_ALL_DOCUMENT_UPLOAD_DELETE_SUCCESS,
  GET_ALL_DOCUMENT_UPLOAD_DELETE_FAILURE,
  GET_ALL_DOCUMENT_UPLOAD_COMPLETE_REQUEST,
  GET_ALL_DOCUMENT_UPLOAD_COMPLETE_SUCCESS,
  GET_ALL_DOCUMENT_UPLOAD_COMPLETE_FAILURE,
  FETCH_APPLICATION_DOCS_STEP_REQUEST,
  FETCH_APPLICATION_DOCS_STEP_SUCCESS,
  FETCH_APPLICATION_DOCS_STEP_FAILURE
} from "../actions/applicationDocumentAction";
import {
  FETCH_APPLICATION_STEP_REQUEST,
  FETCH_APPLICATION_STEP_SUCCESS,
  FETCH_APPLICATION_STEP_FAILURE
} from "../actions/applicationFormAction";
import {
  FETCH_USER_RULES_REQUEST,
  FETCH_USER_RULES_SUCCESS
} from "../../../constants/commonActions";

const initialState = {
  documentDataList: null,
  isError: false,
  error: null
};

const applicationDocumentReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case GET_ALL_DOCUMENT_REQUEST:
      return {
        ...state,

        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case GET_ALL_DOCUMENT_SUCCESS:
      return {
        ...state,
        documentDataList: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case GET_ALL_DOCUMENT_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case GET_ALL_DOCUMENT_UPLOAD_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_SUCCESS:
      return {
        ...state,
        education: payload,
        showLoader: false,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_DELETE_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_DELETE_SUCCESS:
      return {
        ...state,
        education: payload,
        showLoader: false,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_DELETE_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_COMPLETE_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_COMPLETE_SUCCESS:
      return {
        ...state,
        education: payload,
        showLoader: false,
        type: type
      };

    case GET_ALL_DOCUMENT_UPLOAD_COMPLETE_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type
      };
    case FETCH_APPLICATION_DOCS_STEP_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        instructionsStep: null
      };

    case FETCH_APPLICATION_DOCS_STEP_SUCCESS:
      return {
        ...state,
        instructionsStep: payload,
        type,
        showLoader: false
      };
    case FETCH_APPLICATION_DOCS_STEP_FAILURE:
      return { ...state, showLoader: false, isError: true, type };
    case FETCH_USER_RULES_REQUEST:
      return { ...state, showLoader: true, type, isError: false };

    case FETCH_USER_RULES_SUCCESS:
      return {
        ...state,
        showLoader: false,
        userRulesData: payload,
        type,
        isError: false
      };
    default:
      return state;
  }
};

export default applicationDocumentReducer;
