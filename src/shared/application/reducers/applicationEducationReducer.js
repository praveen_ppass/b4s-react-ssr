import {
  FETCH_EDUCATION_INFO_FORM_REQUEST,
  FETCH_EDUCATION_INFO_FORM_SUCCESS,
  FETCH_EDUCATION_INFO_FORM_FAILURE,
  UPDATE_EDUCATION_INFO_FORM_REQUEST,
  UPDATE_EDUCATION_INFO_FORM_SUCCESS,
  UPDATE_EDUCATION_INFO_FORM_FAILURE,
  FETCH_DISTRICT_LIST_REQUEST,
  FETCH_DISTRICT_LIST_SUCCESS,
  FETCH_DISTRICT_LIST_FAILURE,
  FETCH_SUBJECT_REQUEST,
  FETCH_SUBJECT_SUCCESS,
  FETCH_SUBJECT_FAILURE,
  FETCH_EDUCATION_INFO_CONFIG_REQUEST,
  FETCH_EDUCATION_INFO_CONFIG_SUCCESS,
  FETCH_EDUCATION_INFO_CONFIG_FAILURE,
  UPDATE_EDUCATION_INFO_STEP_REQUEST,
  UPDATE_EDUCATION_INFO_STEP_SUCCESS,
  UPDATE_EDUCATION_INFO_STEP_FAILURE,
  FETCH_EDUCATION_CLASS_CONFIG_REQUEST,
  FETCH_EDUCATION_CLASS_CONFIG_SUCCESS,
  FETCH_EDUCATION_CLASS_CONFIG_FAILURE,
  DELETE_EDUCATION_INFO_REQUEST,
  DELETE_EDUCATION_INFO_FAILURE,
  DELETE_EDUCATION_INFO_SUCCESS,
  PRESENT_CLASS_INFO_REQUEST,
  PRESENT_CLASS_INFO_FAILURE,
  PRESENT_CLASS_INFO_SUCCESS
} from "../actions/educationInfoAction";
import {
  FETCH_SCHOLARSHIP_APPLY_REQUEST,
  FETCH_SCHOLARSHIP_APPLY_SUCCESS,
  FETCH_SCHOLARSHIP_APPLY_FAILURE,
  FETCH_FILTER_RULES_REQUESTED,
  FETCH_FILTER_RULES_SUCCEEDED,
  FETCH_FILTER_RULES_FAILED
} from "../../../constants/commonActions";
import {
  FETCH_APPLICATION_STEP_REQUEST,
  FETCH_APPLICATION_STEP_SUCCESS,
  FETCH_APPLICATION_STEP_FAILURE
} from "../actions/applicationFormAction";

const initialState = {
  applicationEducationData: null,
  isError: false,
  error: null
};

const applicationEducationReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case FETCH_EDUCATION_INFO_FORM_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case FETCH_EDUCATION_INFO_FORM_SUCCESS:
      return {
        ...state,
        applicationEducationData: payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };

    case FETCH_EDUCATION_INFO_FORM_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type,
        updateError: "",
        isUpdateError: false
      };
    case FETCH_DISTRICT_LIST_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type
      };

    case FETCH_DISTRICT_LIST_SUCCESS:
      return {
        ...state,
        district: payload,
        showLoader: false,
        type: type
      };

    case FETCH_DISTRICT_LIST_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type
      };

    case FETCH_SUBJECT_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type
      };

    case FETCH_SUBJECT_SUCCESS:
      return {
        ...state,
        subject: payload,
        showLoader: false,
        type: type
      };

    case FETCH_SUBJECT_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type
      };

    case UPDATE_EDUCATION_INFO_FORM_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type
      };

    case UPDATE_EDUCATION_INFO_FORM_SUCCESS:
      return {
        ...state,
        education: payload,
        showLoader: false,
        type: type
      };

    case UPDATE_EDUCATION_INFO_FORM_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type
      };
    case FETCH_EDUCATION_INFO_CONFIG_REQUEST:
      return {
        ...state,
        payload,
        showLoader: true,
        type: type
      };

    case FETCH_EDUCATION_INFO_CONFIG_SUCCESS:
      return {
        ...state,
        educationConfig: payload,
        showLoader: false,
        type: type
      };

    case FETCH_EDUCATION_INFO_CONFIG_FAILURE:
      return {
        ...state,
        payload,
        showLoader: false,
        type: type
      };
    case FETCH_SCHOLARSHIP_APPLY_REQUEST:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });

    case FETCH_SCHOLARSHIP_APPLY_SUCCESS:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        schApply: payload
      });

    case FETCH_SCHOLARSHIP_APPLY_FAILURE:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });
    /*****  update step of form actions**************** */
    case UPDATE_EDUCATION_INFO_STEP_REQUEST:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });

    case UPDATE_EDUCATION_INFO_STEP_SUCCESS:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        updateStep: payload,
        type
      });

    case UPDATE_EDUCATION_INFO_STEP_FAILURE:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });
    /*****  update step of form actions**************** */

    /***** Class Config actions **************** */
    case FETCH_EDUCATION_CLASS_CONFIG_REQUEST:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });

    case FETCH_EDUCATION_CLASS_CONFIG_SUCCESS:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        classConfig: payload,
        type
      });

    case FETCH_EDUCATION_CLASS_CONFIG_FAILURE:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });
    /*****  update step of form actions**************** */

    /***** Delete academic list actions **************** */
    case DELETE_EDUCATION_INFO_REQUEST:
      return Object.assign({}, state, { showLoader: true, isError: false });

    case DELETE_EDUCATION_INFO_SUCCESS:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        deleteInfo: payload,
        type
      });

    case DELETE_EDUCATION_INFO_FAILURE:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });
    /*****  Delete academic list actions**************** */
    /************** Personal Info  get start  ************************** */

    case "APPLICATION_FETCH_PERSONAL_INFO_REQUESTED":
      return {
        ...state,
        payload,
        showLoader: true,
        type
      };
    case "APPLICATION_FETCH_PERSONAL_INFO_SUCCEEDED":
      return {
        ...state,
        personalInfoData: payload,
        showLoader: false,
        type
      };
    case "APPLICATION_FETCH_PERSONAL_INFO_FAILED":
      return {
        ...state,
        payload,
        showLoader: false,
        type
      };

    /************** Personal Info  get end  ************************** */

    case FETCH_FILTER_RULES_REQUESTED:
      return Object.assign({}, state, {
        showLoader: true,
        type,
        isError: false
      });

    case FETCH_FILTER_RULES_SUCCEEDED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: false,
        type,
        filterRules: payload
      });

    case FETCH_FILTER_RULES_FAILED:
      return Object.assign({}, state, {
        showLoader: false,
        isError: true,
        type,
        error: payload
      });
    case FETCH_APPLICATION_STEP_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        instructionsStep: null
      };

    case FETCH_APPLICATION_STEP_SUCCESS:
      return {
        ...state,
        instructionsStep: payload,
        type,
        showLoader: false
      };
    case FETCH_APPLICATION_STEP_FAILURE:
      return { ...state, showLoader: false, isError: true, type };

    case PRESENT_CLASS_INFO_REQUEST:
      return {
        ...state,
        showLoader: true,
        type,
        presentClassInfo: null
      };
  
    case PRESENT_CLASS_INFO_SUCCESS:
      return {
        ...state,
        presentClassInfo: payload,
        type,
        showLoader: false
      };
    case PRESENT_CLASS_INFO_FAILURE:
       return { ...state, showLoader: false, isError: true, type };
    default:
      return state;
  }
};

export default applicationEducationReducer;
