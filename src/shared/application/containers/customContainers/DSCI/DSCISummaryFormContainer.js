import { connect } from "react-redux";
import summary from "../../../components/customForms/DSCI/DSCISummaryInfo";
import {
  getAllSummery as getAllSummeryAction,
  fetchSchApply as fetchSchApplyAction
} from "../../../actions/applicationSummeryAction";

const mapStateToProps = ({ applicationSummery }) => ({
  applicationSummery: applicationSummery,
  showLoader: applicationSummery.showLoader
});

const mapDispatchToProps = dispatch => ({
  getAllSummery: input => dispatch(getAllSummeryAction(input)),
  fetchSchApply: inputData => dispatch(fetchSchApplyAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(summary);
