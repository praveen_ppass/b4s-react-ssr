import { connect } from "react-redux";
import document from "../../../components/customForms/GenericPage/Documents";
import {
  getAllDocuement as getAllDocuementAction,
  uploadAllDocuement as uploadAllDocuementAction,
  deleteDocuement as deleteDocuementAction,
  completeDocument as completeDocumentAction
} from "../../../actions/applicationDocumentAction";
import { applicationInstructionStep as applicationInstructionStepAction } from "../../../actions/applicationFormAction";
import { fetchUserRules as fetchUserRulesAction } from "../../../../../constants/commonActions";
const mapStateToProps = ({ applicationDocument, applicationForm, common }) => ({
  allDocument: applicationDocument,
  showLoader: applicationDocument.showLoader,
  applicationStepInstruction: applicationForm.instructionsStep,
  userRuleInfo: common.userRulesData
});

const mapDispatchToProps = dispatch => ({
  getAllDocuement: inputData => dispatch(getAllDocuementAction(inputData)),
  uploadAllDocuement: inputData =>
    dispatch(uploadAllDocuementAction(inputData)),
  deleteDocuement: inputData => dispatch(deleteDocuementAction(inputData)),
  completeDocument: inputData => dispatch(completeDocumentAction(inputData)),
  applicationInstructionStep: inputData =>
    dispatch(applicationInstructionStepAction(inputData)),
  fetchUserRule: inputData => dispatch(fetchUserRulesAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(document);
