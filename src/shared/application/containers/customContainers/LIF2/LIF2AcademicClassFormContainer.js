import React, { Component } from "react";
import ValidationError from "../../../../components/validationError";
import OtherTextField from "../../../components/otherTextField";
import {
  otherDistrictId,
  otherStateId,
  otherBoardId,
  graduationId,
  otherStreams
} from "../../../formconfig";
const hideBoard = [
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  12,
  21,
  22,
  23,
  24,
  25,
  577,
  752
];

const elementryClass = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 25, 752];
const highSchoolClass = [16, 20];
const graduationClass = [22, 23, 24, 21];
const elevenClass = [12];
const othersClass = [577];

const hideSubject = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 25, 577, 752];

class AcadmicClasses extends Component {
  constructor(props) {
    super(props);
    this.validate = this.validate.bind(this);
  }
  validate(stream, customData) {
    console.log(stream, customData)
    if (!customData["physicsMarksObtained"]) {
      console.log("Some issue with Maths");
      this.props.onEducationSubmitHandler(
        "PHYSICS",
        "You have not entered PHYSICS subject marks obtained."
      );
    }
    if (!customData["physicsTotalMarks"]) {
      console.log("Some issue with Maths");
      this.props.onEducationSubmitHandler(
        "PHYSICS",
        "You have not entered PHYSICS subject total marks."
      );
    }
    if (customData["physicsMarksObtained"] < 0) {
      console.log("Some issue with Maths");
      this.props.onEducationSubmitHandler(
        "PHYSICS",
        "PHYSICS subject marks obtained can not be less than 0"
      );
    }
    if (customData["physicsTotalMarks"] < 0) {
      console.log("Some issue with Maths");
      this.props.onEducationSubmitHandler(
        "PHYSICS",
        "PHYSICS subject marks total can not be less than 0"
      );
    }
    if (customData["physicsTotalMarks"] < customData["physicsMarksObtained"]) {
      console.log("Some issue with Maths");
      this.props.onEducationSubmitHandler(
        "PHYSICS",
        "For PHYSICS Total marks can not less than marks obtained"
      );
    }

    if (!customData["chemistryMarksObtained"]) {
      console.log("Some issue with Maths");
      this.props.onEducationSubmitHandler(
        "CHEMISTRY",
        "You have not entered CHEMISTRY subject marks obtained."
      );
    }
    if (!customData["chemistryTotalMarks"]) {
      console.log("Some issue with Maths");
      this.props.onEducationSubmitHandler(
        "CHEMISTRY",
        "You have not entered CHEMISTRY subject total marks."
      );
    }
    if (customData["chemistryMarksObtained"] < 0) {
      console.log("Some issue with Maths");
      this.props.onEducationSubmitHandler(
        "CHEMISTRY",
        "CHEMISTRY subject marks obtained can not be less than 0"
      );
    }
    if (customData["chemistryTotalMarks"] < 0) {
      console.log("Some issue with Maths");
      this.props.onEducationSubmitHandler(
        "CHEMISTRY",
        "CHEMISTRY subject marks total can not be less than 0"
      );
    }
    if (
      customData["chemistryTotalMarks"] < customData["chemistryMarksObtained"]
    ) {
      console.log("Some issue with Maths");
      this.props.onEducationSubmitHandler(
        "CHEMISTRY",
        "For CHEMISTRY Total marks can not less than marks obtained"
      );
    }

    if (stream == 1273) {
      if (!customData["biologyMarksObtained"]) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "BIOLOGY",
          "You have not entered BIOLOGY subject marks obtained."
        );
      }
      if (!customData["biologyTotalMarks"]) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "BIOLOGY",
          "You have not entered BIOLOGY subject total marks."
        );
      }
      if (customData["biologyMarksObtained"] < 0) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "BIOLOGY",
          "BIOLOGY subject marks obtained can not be less than 0"
        );
      }
      if (customData["biologyTotalMarks"] < 0) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "BIOLOGY",
          "BIOLOGY subject marks total can not be less than 0"
        );
      }
      if (
        customData["biologyTotalMarks"] < customData["biologyMarksObtained"]
      ) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "BIOLOGY",
          "For BIOLOGY Total marks can not less than marks obtained"
        );
      }
    }

    if (stream == 1277) {
      if (!customData["mathMarksObtained"]) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "MATH",
          "You have not entered MATH subject marks obtained."
        );
      }
      if (!customData["mathTotalMarks"]) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "MATH",
          "You have not entered MATH subject total marks."
        );
      }
      if (customData["mathMarksObtained"] < 0) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "MATH",
          "MATH subject marks obtained can not be less than 0"
        );
      }
      if (customData["mathTotalMarks"] < 0) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "MATH",
          "MATH subject marks total can not be less than 0"
        );
      }
      if (customData["mathTotalMarks"] < customData["mathMarksObtained"]) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "MATH",
          "For Maths Total marks can not less than marks obtained"
        );
      }
    }
    if (stream == 1274) {
      if (!customData["mathMarksObtained"]) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "MATH",
          "You have not entered MATH subject marks obtained."
        );
      }
      if (!customData["mathTotalMarks"]) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "MATH",
          "You have not entered MATH subject total marks."
        );
      }
      if (customData["mathMarksObtained"] < 0) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "MATH",
          "MATH subject marks obtained can not be less than 0"
        );
      }
      if (customData["mathTotalMarks"] < 0) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "MATH",
          "MATH subject marks total can not be less than 0"
        );
      }
      if (customData["mathTotalMarks"] < customData["mathMarksObtained"]) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "MATH",
          "For Maths Total marks can not less than marks obtained"
        );
      }
      if (!customData["biologyMarksObtained"]) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "BIOLOGY",
          "You have not entered BIOLOGY subject marks obtained."
        );
      }
      if (!customData["biologyTotalMarks"]) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "BIOLOGY",
          "You have not entered BIOLOGY subject total marks."
        );
      }
      if (customData["biologyMarksObtained"] < 0) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "BIOLOGY",
          "BIOLOGY subject marks obtained can not be less than 0"
        );
      }
      if (customData["biologyTotalMarks"] < 0) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "BIOLOGY",
          "BIOLOGY subject marks total can not be less than 0"
        );
      }
      if (
        customData["biologyTotalMarks"] < customData["biologyMarksObtained"]
      ) {
        console.log("Some issue with Maths");
        this.props.onEducationSubmitHandler(
          "BIOLOGY",
          "For BIOLOGY Total marks can not less than marks obtained"
        );
      }
    }
  }
  render() {
    const {
      allRules,
      classID,
      boardList,
      onDistrictHandler,
      district,
      subject,
      year,
      month,
      marksOrCgpa,
      customData,
      userAcademicInfo,
      onEducationSubmitHandler,
      userInstituteInfo,
      educationFormHandler,
      marksObtainedHandler,
      educationDataApi,
      isEditable,
      validations,
      applicationStep,
      degree,
      presentClassID,
      failevent
    } = this.props;
    let className = null;
    let degreeOption, streamOption;
    const degreeList = educationDataApi && educationDataApi.degree;
    const streamList = educationDataApi && educationDataApi.stream;

    if (!isEditable) {
      if (
        classID &&
        allRules &&
        allRules.rulesData &&
        allRules.rulesData["class"]
      ) {
        className = allRules.rulesData["class"]
          .map(classObj => classObj)
          .filter(classObj => classObj.id == parseInt(classID, 10));
      }
    } else {
      if (
        userAcademicInfo.academicClass &&
        userAcademicInfo.academicClass.id &&
        allRules &&
        allRules.rulesData &&
        allRules.rulesData["class"]
      ) {
        className = allRules.rulesData["class"]
          .map(classObj => classObj)
          .filter(classObj => classObj.id == userAcademicInfo.academicClass.id);
      }
    }

    // check degree on the basis of parent class selection
    if (degreeList && degreeList.dataOptions && degreeList.dataOptions.length) {
      degreeOption = degreeList.dataOptions.filter(
        list => list.parentRuleId == presentClassID
      );
    } else {
      degreeOption = degreeList && degreeList.dataOptions;
    }

    if (
      userAcademicInfo.degree &&
      streamList.dataOptions &&
      streamList.dataOptions.length
    ) {
      let temp;
      temp = streamList.dataOptions.filter(
        list => list.parentRuleId == userAcademicInfo.degree
      );
      if (temp.length) {
        streamOption = temp;
      } else {
        streamOption = streamList.dataOptions;
      }
    } else {
      streamOption = subject;
    }

    customData["physicsMarksObtained"] = customData["physicsMarksObtained"]
      ? parseInt(customData["physicsMarksObtained"])
      : 0;
    customData["chemistryMarksObtained"] = customData["chemistryMarksObtained"]
      ? parseInt(customData["chemistryMarksObtained"])
      : 0;
    customData["mathMarksObtained"] = customData["mathMarksObtained"]
      ? parseInt(customData["mathMarksObtained"])
      : 0;
    customData["biologyMarksObtained"] = customData["biologyMarksObtained"]
      ? parseInt(customData["biologyMarksObtained"])
      : 0;
    customData["physicsTotalMarks"] = customData["physicsTotalMarks"]
      ? parseInt(customData["physicsTotalMarks"])
      : 0;
    customData["chemistryTotalMarks"] = customData["chemistryTotalMarks"]
      ? parseInt(customData["chemistryTotalMarks"])
      : 0;
    customData["mathTotalMarks"] = customData["mathTotalMarks"]
      ? parseInt(customData["mathTotalMarks"])
      : 0;
    customData["biologyTotalMarks"] = customData["biologyTotalMarks"]
      ? parseInt(customData["biologyTotalMarks"])
      : 0;
    let pcmbPercentage = 0;
    let pcbPercentage = 0;
    let pcmPercentage = 0;
    if (
      customData["physicsMarksObtained"] &&
      customData["chemistryMarksObtained"] &&
      customData["biologyMarksObtained"] &&
      customData["physicsTotalMarks"] &&
      customData["chemistryTotalMarks"] &&
      customData["biologyTotalMarks"]
    ) {
      pcbPercentage =
        (customData["physicsMarksObtained"] +
          customData["chemistryMarksObtained"] +
          customData["biologyMarksObtained"]) /
        (customData["physicsTotalMarks"] +
          customData["chemistryTotalMarks"] +
          customData["biologyTotalMarks"]);
    }

    if (
      customData["physicsMarksObtained"] &&
      customData["chemistryMarksObtained"] &&
      customData["mathMarksObtained"] &&
      customData["physicsTotalMarks"] &&
      customData["chemistryTotalMarks"] &&
      customData["mathTotalMarks"]
    ) {
      pcmPercentage =
        (customData["physicsMarksObtained"] +
          customData["chemistryMarksObtained"] +
          customData["mathMarksObtained"]) /
        (customData["physicsTotalMarks"] +
          customData["chemistryTotalMarks"] +
          customData["mathTotalMarks"]);
    }

    if (
      customData["physicsMarksObtained"] &&
      customData["chemistryMarksObtained"] &&
      customData["biologyMarksObtained"] &&
      customData["mathMarksObtained"] &&
      customData["physicsTotalMarks"] &&
      customData["chemistryTotalMarks"] &&
      customData["biologyTotalMarks"] &&
      customData["mathTotalMarks"]
    ) {
      pcmbPercentage =
        (customData["physicsMarksObtained"] +
          customData["chemistryMarksObtained"] +
          customData["mathMarksObtained"] +
          customData["biologyMarksObtained"]) /
        (customData["physicsTotalMarks"] +
          customData["chemistryTotalMarks"] +
          customData["mathTotalMarks"] +
          customData["biologyTotalMarks"]);
    }
    pcmPercentage =
      pcmPercentage > 1 || pcmPercentage < 0
        ? 0
        : Number(parseFloat(pcmPercentage).toFixed(2));
    pcbPercentage =
      pcbPercentage > 1 || pcbPercentage < 0
        ? 0
        : Number(parseFloat(pcbPercentage).toFixed(2));
    pcmbPercentage =
      pcmbPercentage > 1 || pcmbPercentage < 0
        ? 0
        : Number(parseFloat(pcmbPercentage).toFixed(2));
    let phyPercentage = Number(
      parseFloat(
        customData["physicsMarksObtained"] *
        100 /
        customData["physicsTotalMarks"]
      ).toFixed(2)
    );
    let chemPercentage = Number(
      parseFloat(
        customData["chemistryMarksObtained"] *
        100 /
        customData["chemistryTotalMarks"]
      ).toFixed(2)
    );
    let bioPercentage = Number(
      parseFloat(
        customData["biologyMarksObtained"] *
        100 /
        customData["biologyTotalMarks"]
      ).toFixed(2)
    );
    let mathPercentage = Number(
      parseFloat(
        customData["mathMarksObtained"] * 100 / customData["mathTotalMarks"]
      ).toFixed(2)
    );

    if (streamList && streamList.dataOptions && streamList.dataOptions.length) {
      let temp;
      temp = streamList.dataOptions.filter(
        list => list.parentRuleId == userAcademicInfo.degree
      );
      if (temp.length) {
        streamOption = temp;
        // } else {
        //   streamOption = streamList.dataOptions;
        // }
      }
      else {
        if (classID == 16) {
          let classId12th = [1273, 1274, 1277]
          var result = streamList && streamList.dataOptions.filter(obj => {
            return obj.id == 1273 || obj.id == 1274 || obj.id == 1277;
          })
          streamOption = result;
        } else {
          streamOption = [];

        }
      }
    }

    return (
      <article>
        <article className="col-md-12 subheading ">
          Academic history
          <span>Please fill your education details</span>
        </article>
        <section className="padding30">
          <form autoComplete="off">
            <article className="row noMarginLR">
              {true ? (
                <article className="col-md-4 ">
                  <article className="form-group new">
                    <select
                      className="icon"
                      id="academicClass"
                      value={userAcademicInfo.academicClass.id}
                    >
                      {className && className.length > 0
                        ? className.map(cl => (
                          <option key={cl.id} value={cl.id}>
                            {cl.rulevalue}
                          </option>
                        ))
                        : null}
                    </select>

                    <label className="labelstyle">
                      {educationDataApi &&
                        educationDataApi["academic_class"].label}
                    </label>
                    <ValidationError
                      fieldValidation={validations["academicClass"]}
                    />
                  </article>
                </article>
              ) : null}
              {educationDataApi && educationDataApi["board"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <select
                      className="icon"
                      value={userAcademicInfo.board}
                      id="board"
                      onChange={event =>
                        educationFormHandler(event, "userAcademicInfo", "board")
                      }
                    >
                      <option value="">-Select-</option>
                      {educationDataApi &&
                        educationDataApi.board.dataOptions != null
                        ? educationDataApi &&
                        educationDataApi.board.dataOptions.map(boardObj => (
                          <option key={boardObj.id} value={boardObj.id}>
                            {boardObj.name}
                          </option>
                        ))
                        : boardList && boardList.length > 0
                          ? boardList.map(boardObj => (
                            <option key={boardObj.id} value={boardObj.id}>
                              {boardObj.name}
                            </option>
                          ))
                          : null}
                    </select>
                    <label className="labelstyle">
                      {educationDataApi && educationDataApi["board"].label}
                    </label>
                    <ValidationError fieldValidation={validations["board"]} />
                  </article>

                  {userAcademicInfo.board == otherBoardId ? (
                    <OtherTextField
                      custom={
                        educationDataApi && educationDataApi["board"].custom
                      }
                      fieldValue={userAcademicInfo.otherBoard}
                      fieldId="otherBoard"
                      clickHandler={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "otherBoard"
                        )
                      }
                      fieldLabel={
                        educationDataApi && educationDataApi["otherBoard"].label
                      }
                      fieldValidation={validations["otherBoard"]}
                    />
                  ) : null}
                </article>
              ) : null}
              {educationDataApi &&
                educationDataApi["degree"].active &&
                degree &&
                degree.length ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <select
                        className="icon"
                        value={userAcademicInfo.degree}
                        id="degree"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "userAcademicInfo",
                            "degree"
                          )
                        }
                      >
                        <option value="">-Select-</option>
                        {educationDataApi &&
                          educationDataApi.degree.dataOptions != null
                          ? degreeOption.map(subObj => (
                            <option key={subObj.id} value={subObj.id}>
                              {subObj.rulevalue}
                            </option>
                          ))
                          : degree && degree.length > 0
                            ? degree.map(subObj => (
                              <option key={subObj.id} value={subObj.id}>
                                {subObj.rulevalue}
                              </option>
                            ))
                            : null}
                      </select>

                      <label className="labelstyle">
                        {" "}
                        {educationDataApi && educationDataApi["degree"].label}
                      </label>
                      <ValidationError fieldValidation={validations["degree"]} />
                    </article>
                    {[281, 309, 908].indexOf(parseInt(userAcademicInfo.degree)) >
                      -1 ? (
                        <OtherTextField
                          custom={
                            educationDataApi["otherDegree"] &&
                              educationDataApi["otherDegree"].custom
                              ? educationDataApi["otherDegree"].custom
                              : ""
                          }
                          fieldValue={userAcademicInfo.otherDegree}
                          fieldId="otherDegree"
                          // placeHolder="Other Degree/Course"
                          clickHandler={event =>
                            educationFormHandler(
                              event,
                              "userAcademicInfo",
                              "otherDegree"
                            )
                          }
                          fieldLabel={
                            educationDataApi["otherDegree"] &&
                              educationDataApi["otherDegree"].label
                              ? educationDataApi["otherDegree"].label
                              : ""
                          }
                          fieldValidation={validations["otherDegree"]}
                        />
                      ) : null}
                  </article>
                ) : null}
              {educationDataApi && educationDataApi["stream"].active && streamOption.length > 0 ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <select
                      className="icon"
                      value={userAcademicInfo.stream}
                      id="stream"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "stream"
                        )
                      }
                    >
                      <option value="">-Select-</option>
                      {streamOption
                        ? streamOption.map(subObj => (
                          <option key={subObj.id} value={subObj.id}>
                            {subObj.rulevalue}
                          </option>
                        ))
                        : null}
                    </select>

                    <label className="labelstyle">
                      {" "}
                      {educationDataApi && educationDataApi["stream"].label}
                    </label>
                    <ValidationError fieldValidation={validations["stream"]} />
                  </article>
                  {
                    // otherStreams.indexOf(parseInt(userAcademicInfo.stream, 10)) >
                    // -1 ?
                    userAcademicInfo.stream == "1098" ?
                      (

                        <OtherTextField
                          custom={
                            educationDataApi &&
                            educationDataApi["otherStream"].custom
                          }
                          fieldValue={userAcademicInfo.otherStream}
                          fieldId="otherStream"
                          clickHandler={event =>
                            educationFormHandler(
                              event,
                              "userAcademicInfo",
                              "otherStream"
                            )
                          }
                          fieldLabel={
                            educationDataApi &&
                            educationDataApi["otherStream"].label
                          }
                          fieldValidation={validations["otherStream"]}
                        />
                      ) : null}
                </article>
              ) : null}
            </article>
            <article className="row noMarginLR">
              {educationDataApi &&
                educationDataApi["physicsMarksObtained"] &&
                educationDataApi["physicsMarksObtained"].active &&
                (userAcademicInfo.stream == 1274 ||
                  userAcademicInfo.stream == 1273 ||
                  userAcademicInfo.stream == 1277) ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi["physicsMarksObtained"] &&
                            educationDataApi["physicsMarksObtained"].custom
                            ? educationDataApi["physicsMarksObtained"].custom
                            : ""
                        }
                        value={
                          customData.physicsMarksObtained
                            ? customData.physicsMarksObtained
                            : ""
                        }
                        id="physicsMarksObtained"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "customInfo",
                            "physicsMarksObtained"
                          )
                        }
                        autoComplete="false"
                        required
                      />
                      <label>
                        {educationDataApi["physicsMarksObtained"] &&
                          educationDataApi["physicsMarksObtained"].label
                          ? educationDataApi["physicsMarksObtained"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["physicsMarksObtained"]}
                      />
                    </article>
                  </article>
                ) : null}
              {educationDataApi &&
                educationDataApi["physicsTotalMarks"] &&
                educationDataApi["physicsTotalMarks"].active &&
                (userAcademicInfo.stream == 1274 ||
                  userAcademicInfo.stream == 1273 ||
                  userAcademicInfo.stream == 1277) ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi["physicsTotalMarks"] &&
                            educationDataApi["physicsTotalMarks"].custom
                            ? educationDataApi["physicsTotalMarks"].custom
                            : ""
                        }
                        value={
                          customData.physicsTotalMarks
                            ? customData.physicsTotalMarks
                            : ""
                        }
                        id="physicsTotalMarks"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "customInfo",
                            "physicsTotalMarks"
                          )
                        }
                        autoComplete="false"
                        required
                      />
                      <label>
                        {educationDataApi["physicsTotalMarks"] &&
                          educationDataApi["physicsTotalMarks"].label
                          ? educationDataApi["physicsTotalMarks"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["physicsTotalMarks"]}
                      />
                    </article>
                  </article>
                ) : null}
              {(userAcademicInfo.stream == 1274 ||
                userAcademicInfo.stream == 1273 ||
                userAcademicInfo.stream == 1277) &&
                customData["physicsMarksObtained"] &&
                customData["physicsTotalMarks"] &&
                customData["physicsTotalMarks"] > 0 ? (
                  <article className="col-md-4 lastCell">
                    <article className="form-group">
                      <lable>
                        `Physics{" "}
                        {customData["physicsMarksObtained"] &&
                          customData["physicsTotalMarks"] &&
                          customData["physicsTotalMarks"] > 0 &&
                          customData["physicsTotalMarks"] >=
                          customData["physicsMarksObtained"]
                          ? `${phyPercentage} %`
                          : "-"}
                      `
                    </lable>
                    </article>
                  </article>
                ) : null}
            </article>

            <article className="row noMarginLR">
              {educationDataApi &&
                educationDataApi["chemistryMarksObtained"] &&
                educationDataApi["chemistryMarksObtained"].active &&
                (userAcademicInfo.stream == 1274 ||
                  userAcademicInfo.stream == 1273 ||
                  userAcademicInfo.stream == 1277) ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi["chemistryMarksObtained"] &&
                            educationDataApi["chemistryMarksObtained"].custom
                            ? educationDataApi["chemistryMarksObtained"].custom
                            : ""
                        }
                        value={
                          customData.chemistryMarksObtained
                            ? customData.chemistryMarksObtained
                            : ""
                        }
                        id="chemistryMarksObtained"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "customInfo",
                            "chemistryMarksObtained"
                          )
                        }
                        autoComplete="false"
                        required
                      />
                      <label>
                        {educationDataApi["chemistryMarksObtained"] &&
                          educationDataApi["chemistryMarksObtained"].label
                          ? educationDataApi["chemistryMarksObtained"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["chemistryMarksObtained"]}
                      />
                    </article>
                  </article>
                ) : null}
              {educationDataApi &&
                educationDataApi["chemistryTotalMarks"] &&
                educationDataApi["chemistryTotalMarks"].active &&
                (userAcademicInfo.stream == 1274 ||
                  userAcademicInfo.stream == 1273 ||
                  userAcademicInfo.stream == 1277) ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi["chemistryTotalMarks"] &&
                            educationDataApi["chemistryTotalMarks"].custom
                            ? educationDataApi["chemistryTotalMarks"].custom
                            : ""
                        }
                        value={
                          customData.chemistryTotalMarks
                            ? customData.chemistryTotalMarks
                            : ""
                        }
                        id="chemistryTotalMarks"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "customInfo",
                            "chemistryTotalMarks"
                          )
                        }
                        autoComplete="false"
                        required
                      />
                      <label>
                        {educationDataApi["chemistryTotalMarks"] &&
                          educationDataApi["chemistryTotalMarks"].label
                          ? educationDataApi["chemistryTotalMarks"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["chemistryTotalMarks"]}
                      />
                    </article>
                  </article>
                ) : null}
              {(userAcademicInfo.stream == 1274 ||
                userAcademicInfo.stream == 1273 ||
                userAcademicInfo.stream == 1277) &&
                customData["chemistryMarksObtained"] &&
                customData["chemistryTotalMarks"] &&
                customData["chemistryTotalMarks"] > 0 ? (
                  <article className="col-md-4 lastCell">
                    <article className="form-group">
                      <lable>
                        `Chemistry{" "}
                        {customData["chemistryMarksObtained"] &&
                          customData["chemistryTotalMarks"] &&
                          customData["chemistryTotalMarks"] > 0 &&
                          customData["chemistryTotalMarks"] >=
                          customData["chemistryMarksObtained"]
                          ? `${chemPercentage} %`
                          : "-"}
                      `
                    </lable>
                    </article>
                  </article>
                ) : null}
            </article>
            <article className="row noMarginLR">
              {educationDataApi &&
                educationDataApi["biologyMarksObtained"] &&
                educationDataApi["biologyMarksObtained"].active &&
                (userAcademicInfo.stream == 1274 ||
                  userAcademicInfo.stream == 1273) ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi["biologyMarksObtained"] &&
                            educationDataApi["biologyMarksObtained"].custom
                            ? educationDataApi["biologyMarksObtained"].custom
                            : ""
                        }
                        value={
                          customData.biologyMarksObtained
                            ? customData.biologyMarksObtained
                            : ""
                        }
                        id="biologyMarksObtained"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "customInfo",
                            "biologyMarksObtained"
                          )
                        }
                        autoComplete="false"
                        required
                      />
                      <label>
                        {educationDataApi["biologyMarksObtained"] &&
                          educationDataApi["biologyMarksObtained"].label
                          ? educationDataApi["biologyMarksObtained"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["biologyMarksObtained"]}
                      />
                    </article>
                  </article>
                ) : null}
              {educationDataApi &&
                educationDataApi["biologyTotalMarks"] &&
                educationDataApi["biologyTotalMarks"].active &&
                (userAcademicInfo.stream == 1274 ||
                  userAcademicInfo.stream == 1273) ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi["biologyTotalMarks"] &&
                            educationDataApi["biologyTotalMarks"].custom
                            ? educationDataApi["biologyTotalMarks"].custom
                            : ""
                        }
                        value={
                          customData.biologyTotalMarks
                            ? customData.biologyTotalMarks
                            : ""
                        }
                        id="biologyTotalMarks"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "customInfo",
                            "biologyTotalMarks"
                          )
                        }
                        autoComplete="false"
                        required
                      />
                      <label>
                        {educationDataApi["biologyTotalMarks"] &&
                          educationDataApi["biologyTotalMarks"].label
                          ? educationDataApi["biologyTotalMarks"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["biologyTotalMarks"]}
                      />
                    </article>
                  </article>
                ) : null}
              {(userAcademicInfo.stream == 1274 ||
                userAcademicInfo.stream == 1273) &&
                customData["biologyMarksObtained"] &&
                customData["biologyTotalMarks"] &&
                customData["biologyTotalMarks"] > 0 ? (
                  <article className="col-md-4 lastCell">
                    <article className="form-group">
                      <lable>
                        `Biology{" "}
                        {customData["biologyMarksObtained"] &&
                          customData["biologyTotalMarks"] &&
                          customData["biologyTotalMarks"] > 0 &&
                          customData["biologyTotalMarks"] >=
                          customData["biologyMarksObtained"]
                          ? `${bioPercentage} %`
                          : "-"}
                      `
                    </lable>
                    </article>
                  </article>
                ) : null}
            </article>
            <article className="row noMarginLR">
              {educationDataApi &&
                educationDataApi["mathMarksObtained"] &&
                educationDataApi["mathMarksObtained"].active &&
                (userAcademicInfo.stream == 1274 ||
                  userAcademicInfo.stream == 1277) ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi["mathMarksObtained"] &&
                            educationDataApi["mathMarksObtained"].custom
                            ? educationDataApi["mathMarksObtained"].custom
                            : ""
                        }
                        value={
                          customData.mathMarksObtained
                            ? customData.mathMarksObtained
                            : ""
                        }
                        id="mathMarksObtained"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "customInfo",
                            "mathMarksObtained"
                          )
                        }
                        autoComplete="false"
                        required
                      />
                      <label>
                        {educationDataApi["mathMarksObtained"] &&
                          educationDataApi["mathMarksObtained"].label
                          ? educationDataApi["mathMarksObtained"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["mathMarksObtained"]}
                      />
                    </article>
                  </article>
                ) : null}
              {educationDataApi &&
                educationDataApi["mathTotalMarks"] &&
                educationDataApi["mathTotalMarks"].active &&
                (userAcademicInfo.stream == 1274 ||
                  userAcademicInfo.stream == 1277) ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi["mathTotalMarks"] &&
                            educationDataApi["mathTotalMarks"].custom
                            ? educationDataApi["mathTotalMarks"].custom
                            : ""
                        }
                        value={
                          customData.mathTotalMarks
                            ? customData.mathTotalMarks
                            : ""
                        }
                        id="mathTotalMarks"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "customInfo",
                            "mathTotalMarks"
                          )
                        }
                        autoComplete="false"
                        required
                      />
                      <label>
                        {educationDataApi["mathTotalMarks"] &&
                          educationDataApi["mathTotalMarks"].label
                          ? educationDataApi["mathTotalMarks"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["mathTotalMarks"]}
                      />
                    </article>
                  </article>
                ) : null}
              {(userAcademicInfo.stream == 1274 ||
                userAcademicInfo.stream == 1277) &&
                customData["mathMarksObtained"] &&
                customData["mathTotalMarks"] &&
                customData["mathTotalMarks"] > 0 ? (
                  <article className="col-md-4 lastCell">
                    <article className="form-group">
                      <lable>
                        `Math{" "}
                        {customData["mathMarksObtained"] &&
                          customData["mathTotalMarks"] &&
                          customData["mathTotalMarks"] > 0 &&
                          customData["mathTotalMarks"] >=
                          customData["mathMarksObtained"]
                          ? `${mathPercentage} %`
                          : "-"}
                      `
                    </lable>
                    </article>
                  </article>
                ) : null}
            </article>
            <article className="row noMarginLR">
              {userAcademicInfo &&
                (userAcademicInfo.stream === "1273" ||
                  userAcademicInfo.stream === "1274" ||
                  userAcademicInfo.stream === "1277" ||
                  userAcademicInfo.stream === 1273 ||
                  userAcademicInfo.stream === 1274 ||
                  userAcademicInfo.stream === 1277) ? (
                  <article className="col-md-12">
                    <article className="form-group">
                      <lable className="titleLabel">
                        `
                      {userAcademicInfo.stream === "1273" ||
                          userAcademicInfo.stream === 1273
                          ? `PCB Percentage ${pcbPercentage * 100}%`
                          : userAcademicInfo.stream === "1274" ||
                            userAcademicInfo.stream === 1274
                            ? `PCM Percentage ${pcmPercentage *
                            100}% AND PCB Percentage ${pcbPercentage * 100}%`
                            : userAcademicInfo.stream === "1277" ||
                              userAcademicInfo.stream === 1277
                              ? `PCM Percentage ${pcmPercentage * 100}%`
                              : ""}
                      `
                    </lable>
                    </article>
                  </article>
                ) : null}
            </article>
            <article className="row noMarginLR">
              {educationDataApi &&
                educationDataApi["course_duration"].active ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <select
                        className="icon"
                        value={userAcademicInfo.courseDuration}
                        id="courseDuration"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "userAcademicInfo",
                            "courseDuration"
                          )
                        }
                      >
                        <option value="">-Select-</option>
                        {educationDataApi &&
                          educationDataApi.course_duration.dataOptions != null
                          ? educationDataApi &&
                          educationDataApi.course_duration.dataOptions.map(
                            y => (
                              <option key={y.id} value={y.id}>
                                {y.ruleValue}
                              </option>
                            )
                          )
                          : null}
                      </select>
                      <label className="labelstyle">
                        {educationDataApi["course_duration"] &&
                          educationDataApi["course_duration"].label
                          ? educationDataApi["course_duration"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["courseDuration"]}
                      />
                    </article>
                  </article>
                ) : null}

              {educationDataApi &&
                educationDataApi["courseStartDate"] &&
                educationDataApi["courseStartDate"].active ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi &&
                            educationDataApi["courseStartDate"] &&
                            educationDataApi["courseStartDate"].custom
                            ? educationDataApi["courseStartDate"].custom
                            : ""
                        }
                        value={
                          customData.courseStartDate
                            ? customData.courseStartDate
                            : ""
                        }
                        id="courseStartDate"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "customInfo",
                            "courseStartDate"
                          )
                        }
                        autoComplete="false"
                        required
                      />
                      <label>
                        {educationDataApi &&
                          educationDataApi["courseStartDate"] &&
                          educationDataApi["courseStartDate"].label
                          ? educationDataApi["courseStartDate"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["courseStartDate"]}
                      />
                    </article>
                  </article>
                ) : null}
              {educationDataApi &&
                educationDataApi["current_academic_year"].active ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <select
                        className="icon"
                        value={userAcademicInfo.currentAcademicYear}
                        id="currentAcademicYear"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "userAcademicInfo",
                            "currentAcademicYear"
                          )
                        }
                      >
                        <option value="">-Select-</option>
                        {educationDataApi &&
                          educationDataApi.current_academic_year.dataOptions != null
                          ? educationDataApi &&
                          educationDataApi.current_academic_year.dataOptions.map(
                            y => (
                              <option key={y.id} value={y.id}>
                                {y.ruleValue}
                              </option>
                            )
                          )
                          : year
                            ? year.map(y => (
                              <option key={y} value={y}>
                                {y}
                              </option>
                            ))
                            : null}
                      </select>
                      <label className="labelstyle">
                        {educationDataApi["current_academic_year"] &&
                          educationDataApi["current_academic_year"].label
                          ? educationDataApi["current_academic_year"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["currentAcademicYear"]}
                      />
                    </article>
                  </article>
                ) : null}
            </article>
            <article className="row noMarginLR">
              {educationDataApi &&
                educationDataApi["current_academic_sem"].active ? (
                  <article className="col-md-4 currentAcademicSem">
                    <article className="form-group">
                      <select
                        className="icon"
                        value={userAcademicInfo.currentAcademicSem}
                        id="currentAcademicSem"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "userAcademicInfo",
                            "currentAcademicSem"
                          )
                        }
                      >
                        <option value="">-Select-</option>
                        {educationDataApi &&
                          educationDataApi.current_academic_sem.dataOptions != null
                          ? educationDataApi &&
                          educationDataApi.current_academic_sem.dataOptions.map(
                            y => (
                              <option key={y.id} value={y.id}>
                                {y.ruleValue}
                              </option>
                            )
                          )
                          : null}
                      </select>
                      <label className="labelstyle">
                        {educationDataApi["current_academic_sem"] &&
                          educationDataApi["current_academic_sem"].label
                          ? educationDataApi["current_academic_sem"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["currentAcademicSem"]}
                      />
                    </article>
                  </article>
                ) : null}

              {educationDataApi && educationDataApi["principal_name"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <input
                      type="text"
                      data-custom={
                        educationDataApi["principal_name"] &&
                          educationDataApi["principal_name"].custom
                          ? educationDataApi["principal_name"].custom
                          : ""
                      }
                      value={userInstituteInfo.principalName}
                      id="principalName"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userInstituteInfo",
                          "principalName"
                        )
                      }
                      required
                    />
                    <label>
                      {educationDataApi["principal_name"] &&
                        educationDataApi["principal_name"].label
                        ? educationDataApi["principal_name"].label
                        : ""}
                    </label>
                    <ValidationError
                      fieldValidation={validations["principalName"]}
                    />
                  </article>
                </article>
              ) : null}
            </article>

            <article className="row noMarginLR">
              {educationDataApi &&
                educationDataApi["clatScore"] &&
                educationDataApi["clatScore"].active ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi &&
                            educationDataApi["clatScore"] &&
                            educationDataApi["clatScore"].custom
                            ? educationDataApi["clatScore"].custom
                            : ""
                        }
                        value={customData.clatScore ? customData.clatScore : ""}
                        id="clatScore"
                        onChange={event =>
                          educationFormHandler(event, "customInfo", "clatScore")
                        }
                        autoComplete="false"
                        required
                      />
                      <label>
                        {educationDataApi &&
                          educationDataApi["clatScore"] &&
                          educationDataApi["clatScore"].label
                          ? educationDataApi["clatScore"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["clatScore"]}
                      />
                    </article>
                  </article>
                ) : null}
              {educationDataApi && educationDataApi["institute_name"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <input
                      type="text"
                      data-custom={
                        educationDataApi["institute_name"] &&
                          educationDataApi["institute_name"].custom
                          ? educationDataApi["institute_name"].custom
                          : ""
                      }
                      value={userInstituteInfo.instituteName}
                      id="instituteName"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userInstituteInfo",
                          "instituteName"
                        )
                      }
                      required
                    />
                    <label>
                      {educationDataApi["institute_name"] &&
                        educationDataApi["institute_name"].label
                        ? educationDataApi["institute_name"].label
                        : ""}
                    </label>
                    <ValidationError
                      fieldValidation={validations["instituteName"]}
                    />
                  </article>
                </article>
              ) : null}
              {educationDataApi &&
                educationDataApi["institute_email"].active ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi["institute_email"] &&
                            educationDataApi["institute_email"].custom
                            ? educationDataApi["institute_email"].custom
                            : ""
                        }
                        value={userInstituteInfo.instituteEmail}
                        id="instituteEmail"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "userInstituteInfo",
                            "instituteEmail"
                          )
                        }
                        required
                      />
                      <label>
                        {educationDataApi["institute_email"] &&
                          educationDataApi["institute_email"].label
                          ? educationDataApi["institute_email"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["instituteEmail"]}
                      />
                    </article>
                  </article>
                ) : null}

              {educationDataApi &&
                educationDataApi["institute_phone"].active ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi["institute_phone"] &&
                            educationDataApi["institute_phone"].custom
                            ? educationDataApi["institute_phone"].custom
                            : ""
                        }
                        value={userInstituteInfo.institutePhone}
                        id="institutePhone"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "userInstituteInfo",
                            "institutePhone"
                          )
                        }
                        required
                      />
                      <label>
                        {educationDataApi["institute_phone"] &&
                          educationDataApi["institute_phone"].label
                          ? educationDataApi["institute_phone"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["institutePhone"]}
                      />
                    </article>
                  </article>
                ) : null}


              {/* {
  educationDataApi &&
  educationDataApi["graduationStatus"] &&
  educationDataApi["graduationStatus"].active ? (
    <article className="row paddingtop0">
    {educationDataApi &&
                      educationDataApi.graduationStatus &&
                      educationDataApi.graduationStatus.dataOptions != null ?
    
                        
    (
      educationDataApi.graduationStatus.dataOptions.map((option,id)=>{
        console.log(option)
        return(
          <article className="col-md-12 radio-btn">
          <span>
            <label>
              <input
                type="radio"
                name="graduationStatus"
                id="graduationStatus"
                      data-custom={
                        educationDataApi &&
                        educationDataApi["graduationStatus"] &&
                        educationDataApi["graduationStatus"].custom
                          ? educationDataApi["graduationStatus"].custom
                          : ""
                      }
                value={option.id}
                // checked={userAcademicInfo.markingType === "1"}
                onChange={event =>
                  educationFormHandler(
                    event,
                    "customInfo",
                    "graduationStatus"
                  )
                }
              />
              <span />
        &nbsp;&nbsp; <i>{option.rulevalue}</i>
            </label>
          </span>
        </article>
        )
      }) 

    ) : null}
  </article>

  ):""
} */}

              {educationDataApi &&
                educationDataApi["miscellaneous_fee"].active ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi["miscellaneous_fee"] &&
                            educationDataApi["miscellaneous_fee"].custom
                            ? educationDataApi["miscellaneous_fee"].custom
                            : ""
                        }
                        value={userAcademicInfo.miscellaneousFee}
                        id="miscellaneousFee"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "userAcademicInfo",
                            "miscellaneousFee"
                          )
                        }
                        required
                      />
                      <label>
                        {educationDataApi["miscellaneous_fee"] &&
                          educationDataApi["miscellaneous_fee"].label
                          ? educationDataApi["miscellaneous_fee"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["miscellaneousFee"]}
                      />
                    </article>
                  </article>
                ) : null}
              {educationDataApi && educationDataApi["admission_fee"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <input
                      type="text"
                      data-custom={
                        educationDataApi["admission_fee"] &&
                          educationDataApi["admission_fee"].custom
                          ? educationDataApi["admission_fee"].custom
                          : ""
                      }
                      value={userAcademicInfo.admissionFee}
                      id="admissionFee"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "admissionFee"
                        )
                      }
                      required
                    />
                    <label>
                      {educationDataApi["admission_fee"] &&
                        educationDataApi["admission_fee"].label
                        ? educationDataApi["admission_fee"].label
                        : ""}
                    </label>
                    <ValidationError
                      fieldValidation={validations["admissionFee"]}
                    />
                  </article>
                </article>
              ) : null}

              {educationDataApi && educationDataApi["hostel_fee"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <input
                      type="text"
                      data-custom={
                        educationDataApi["hostel_fee"] &&
                          educationDataApi["hostel_fee"].custom
                          ? educationDataApi["hostel_fee"].custom
                          : ""
                      }
                      value={userAcademicInfo.hostelFee}
                      id="hostelFee"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "hostelFee"
                        )
                      }
                      required
                    />
                    <label>
                      {educationDataApi["hostel_fee"] &&
                        educationDataApi["hostel_fee"].label
                        ? educationDataApi["hostel_fee"].label
                        : ""}
                    </label>
                    <ValidationError
                      fieldValidation={validations["hostelFee"]}
                    />
                  </article>
                </article>
              ) : null}
              {educationDataApi && educationDataApi["tuition_fee"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <input
                      type="text"
                      data-custom={
                        educationDataApi["tuition_fee"] &&
                          educationDataApi["tuition_fee"].custom
                          ? educationDataApi["tuition_fee"].custom
                          : ""
                      }
                      value={userAcademicInfo.tuitionFee}
                      id="tuitionFee"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "tuitionFee"
                        )
                      }
                      required
                    />
                    <label>
                      {educationDataApi["tuition_fee"] &&
                        educationDataApi["tuition_fee"].label
                        ? educationDataApi["tuition_fee"].label
                        : ""}
                    </label>
                    <ValidationError
                      fieldValidation={validations["tuitionFee"]}
                    />
                  </article>
                </article>
              ) : null}
              {educationDataApi && educationDataApi["mode_of_course"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <select
                      className="icon"
                      value={userAcademicInfo.modeOfCourse}
                      id="modeOfCourse"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "modeOfCourse"
                        )
                      }
                    >
                      <option value="">-Select-</option>
                      {educationDataApi &&
                        educationDataApi.mode_of_course.dataOptions != null
                        ? educationDataApi.mode_of_course.dataOptions.map(y => (
                          <option key={y.id} value={y.id}>
                            {y.ruleValue}
                          </option>
                        ))
                        : null}
                    </select>
                    <label className="labelstyle">
                      {educationDataApi["mode_of_course"] &&
                        educationDataApi["mode_of_course"].label
                        ? educationDataApi["mode_of_course"].label
                        : ""}
                    </label>
                    <ValidationError
                      fieldValidation={validations["modeOfCourse"]}
                    />
                  </article>
                </article>
              ) : null}
              {educationDataApi && educationDataApi["mode_of_study"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <select
                      className="icon"
                      value={userAcademicInfo.modeOfStudy}
                      id="modeOfStudy"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "modeOfStudy"
                        )
                      }
                    >
                      <option value="">-Select-</option>
                      {educationDataApi &&
                        educationDataApi.mode_of_study.dataOptions != null
                        ? educationDataApi.mode_of_study.dataOptions.map(y => (
                          <option key={y.id} value={y.id}>
                            {y.ruleValue}
                          </option>
                        ))
                        : null}
                    </select>
                    <label className="labelstyle">
                      {educationDataApi["mode_of_study"] &&
                        educationDataApi["mode_of_study"].label
                        ? educationDataApi["mode_of_study"].label
                        : ""}
                    </label>
                    <ValidationError
                      fieldValidation={validations["modeOfStudy"]}
                    />
                  </article>
                </article>
              ) : null}

              {educationDataApi && educationDataApi["passing_year"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <select
                      className="icon"
                      value={userAcademicInfo.passingYear}
                      id="passingYear"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "passingYear"
                        )
                      }
                    >
                      <option value="">-Select-</option>
                      {educationDataApi &&
                        educationDataApi.passing_year.dataOptions != null
                        ? educationDataApi &&
                        educationDataApi.passing_year.dataOptions.map(y => (
                          <option key={y.id} value={y.rulevalue}>
                            {y.rulevalue}
                          </option>
                        ))
                        : year
                          ? year.map(y => (
                            <option key={y.id} value={y.rulevalue}>
                              {y.rulevalue}
                            </option>
                          ))
                          : null}
                    </select>
                    <label className="labelstyle">
                      {educationDataApi["passing_year"] &&
                        educationDataApi["passing_year"].label
                        ? educationDataApi["passing_year"].label
                        : ""}
                    </label>
                    <ValidationError
                      fieldValidation={validations["passingYear"]}
                    />
                  </article>
                </article>
              ) : null}
              {educationDataApi && educationDataApi["passing_month"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <select
                      className="icon"
                      id="passingMonth"
                      value={userAcademicInfo.passingMonth}
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "passingMonth"
                        )
                      }
                    >
                      <option value="">-Select-</option>
                      {educationDataApi &&
                        educationDataApi.passing_month.dataOptions != null
                        ? educationDataApi &&
                        educationDataApi.passing_month.dataOptions.map(m => (
                          <option key={m} value={m}>
                            {m}
                          </option>
                        ))
                        : month
                          ? month.map(m => (
                            <option key={m} value={m}>
                              {m}
                            </option>
                          ))
                          : null}
                    </select>
                    <label className="labelstyle">
                      {educationDataApi["passing_month"] &&
                        educationDataApi["passing_month"].label
                        ? educationDataApi["passing_month"].label
                        : ""}
                    </label>
                    <ValidationError
                      fieldValidation={validations["passingMonth"]}
                    />
                  </article>
                </article>
              ) : null}
              {educationDataApi &&
                educationDataApi["attendancePercentage"] &&
                educationDataApi["attendancePercentage"].active ? (
                  <article className="col-md-4">
                    <article className="form-group">
                      <input
                        type="text"
                        data-custom={
                          educationDataApi &&
                            educationDataApi["attendancePercentage"] &&
                            educationDataApi["attendancePercentage"].custom
                            ? educationDataApi["attendancePercentage"].custom
                            : ""
                        }
                        value={
                          customData.attendancePercentage
                            ? customData.attendancePercentage
                            : ""
                        }
                        id="attendancePercentage"
                        onChange={event =>
                          educationFormHandler(
                            event,
                            "customInfo",
                            "attendancePercentage"
                          )
                        }
                        autoComplete="false"
                        required
                      />
                      <label>
                        {educationDataApi &&
                          educationDataApi["attendancePercentage"] &&
                          educationDataApi["attendancePercentage"].label
                          ? educationDataApi["attendancePercentage"].label
                          : ""}
                      </label>
                      <ValidationError
                        fieldValidation={validations["attendancePercentage"]}
                      />
                    </article>
                  </article>
                ) : null}
            </article>
            <article className="row paddingtop0">
              {educationDataApi &&
                educationDataApi["graduationStatus"] &&
                educationDataApi["graduationStatus"].active ? (
                  <article >
                    <article className="col-md-12 address">
                      {/* <label className="labelstyle"> */}
                      {educationDataApi["graduationStatus"] &&
                        educationDataApi["graduationStatus"].label
                        ? educationDataApi["graduationStatus"].label
                        : ""}
                      {/* </label> */}
                    </article>
                    {educationDataApi &&
                      educationDataApi.graduationStatus &&
                      educationDataApi.graduationStatus.dataOptions != null ?
                      (
                        educationDataApi.graduationStatus.dataOptions.map((option, id) => {
                          return (
                            <article className="col-md-3  radio-btn">
                              <span>
                                <label>
                                  <input
                                    type="radio"
                                    name="graduationStatus"
                                    id="graduationStatus"
                                    data-custom={
                                      educationDataApi &&
                                        educationDataApi["graduationStatus"] &&
                                        educationDataApi["graduationStatus"].custom
                                        ? educationDataApi["graduationStatus"].custom
                                        : ""
                                    }
                                    value={option.id}
                                    checked={customData["graduationStatus"] === option.id}
                                    onChange={event =>
                                      educationFormHandler(
                                        event,
                                        "customInfo",
                                        "graduationStatus"
                                      )
                                    }
                                  />
                                  <span />
        &nbsp;&nbsp; <i>{option.rulevalue}</i>
                                </label>
                              </span>
                            </article>
                          )
                        })

                      ) : null}

                    <article>
                      <ValidationError
                        fieldValidation={validations["graduationStatus"]}
                      />
                    </article>

                  </article>


                ) : null}
            </article>
            <article className="row noMarginLR">

              {educationDataApi && educationDataApi["roll_no"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <input
                      type="text"
                      data-custom={
                        educationDataApi && educationDataApi["roll_no"].custom
                      }
                      value={userAcademicInfo.rollNo}
                      id="rollNo"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userAcademicInfo",
                          "rollNo"
                        )
                      }
                      required
                    />
                    <label>
                      {educationDataApi && educationDataApi["roll_no"].label}
                    </label>
                    <ValidationError fieldValidation={validations["rollNo"]} />
                  </article>
                </article>
              ) : null}
              {educationDataApi && educationDataApi["state"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <select
                      className="icon"
                      id="state"
                      value={userInstituteInfo.state}
                      onChange={event => onDistrictHandler(event)}
                    >
                      <option value="">-Select-</option>

                      {educationDataApi &&
                        educationDataApi.state.dataOptions != null
                        ? educationDataApi &&
                        educationDataApi.state.dataOptions.map(res => {
                          return (
                            <option key={res.id} value={res.id}>
                              {res.rulevalue}
                            </option>
                          );
                        })
                        : allRules && allRules.rulesData
                          ? allRules.rulesData["state"].map(stateObj => {
                            return (
                              <option key={stateObj.id} value={stateObj.id}>
                                {stateObj.rulevalue}
                              </option>
                            );
                          })
                          : null}
                      {/* {educationDataApi && educationDataApi.state.dataOptions != null ?
                  educationDataApi && educationDataApi.state.dataOptions.map(stateObj => <option key={stateObj.id} value={stateObj.id}>
                    {stateObj.rulevalue}
                  </option>)
                  : allRules &&
                  allRules.rulesData &&
                  allRules.rulesData["state"].map(stateObj => (
                    <option key={stateObj.id} value={stateObj.id}>
                      {stateObj.rulevalue}
                    </option> } */}
                    </select>
                    <label className="labelstyle">
                      {educationDataApi && educationDataApi["state"].label}
                    </label>
                    <ValidationError fieldValidation={validations["state"]} />
                  </article>

                  {userInstituteInfo.state == otherStateId ? (
                    <OtherTextField
                      custom={
                        educationDataApi &&
                        educationDataApi["otherState"].custom
                      }
                      fieldValue={userInstituteInfo.otherState}
                      fieldId="otherState"
                      clickHandler={event =>
                        educationFormHandler(
                          event,
                          "userInstituteInfo",
                          "otherState"
                        )
                      }
                      fieldLabel={
                        educationDataApi && educationDataApi["otherState"].label
                      }
                      fieldValidation={validations["otherState"]}
                    />
                  ) : null}
                </article>
              ) : null}
              {educationDataApi && educationDataApi["district"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <select
                      className="icon"
                      value={userInstituteInfo.district}
                      id="district"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userInstituteInfo",
                          "district"
                        )
                      }
                    >
                      <option value="">-Select-</option>
                      {educationDataApi &&
                        educationDataApi.district.dataOptions != null
                        ? educationDataApi &&
                        educationDataApi.district.dataOptions.map(dst => {
                          return (
                            <option key={dst.id} value={dst.id}>
                              {dst.districtName}
                            </option>
                          );
                        })
                        : district && district.length > 0
                          ? district.map(dst => {
                            return (
                              <option key={dst.id} value={dst.id}>
                                {dst.districtName}
                              </option>
                            );
                          })
                          : null}
                    </select>
                    <label className="labelstyle">
                      {educationDataApi && educationDataApi["district"].label}
                    </label>
                    <ValidationError
                      fieldValidation={validations["district"]}
                    />
                  </article>

                  {userInstituteInfo.district == otherDistrictId ? (
                    <OtherTextField
                      custom={
                        educationDataApi &&
                        educationDataApi["otherDistrict"].custom
                      }
                      fieldValue={userInstituteInfo.otherDistrict}
                      fieldId="otherDistrict"
                      clickHandler={event =>
                        educationFormHandler(
                          event,
                          "userInstituteInfo",
                          "otherDistrict"
                        )
                      }
                      fieldLabel={
                        educationDataApi &&
                        educationDataApi["otherDistrict"].label
                      }
                      fieldValidation={validations["otherDistrict"]}
                    />
                  ) : null}
                </article>
              ) : null}

              {educationDataApi && educationDataApi["city"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <input
                      type="text"
                      data-custom={
                        educationDataApi["city"] &&
                          educationDataApi["city"].custom
                          ? educationDataApi["city"].custom
                          : ""
                      }
                      value={userInstituteInfo.city}
                      id="city"
                      onChange={event =>
                        educationFormHandler(event, "userInstituteInfo", "city")
                      }
                      autoComplete="false"
                      required
                    />
                    <label>
                      {educationDataApi["city"] &&
                        educationDataApi["city"].label
                        ? educationDataApi["city"].label
                        : ""}
                    </label>
                    <ValidationError fieldValidation={validations["city"]} />
                  </article>
                </article>
              ) : null}
              {educationDataApi && educationDataApi["pincode"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <input
                      type="text"
                      data-custom={
                        educationDataApi["pincode"] &&
                          educationDataApi["pincode"].custom
                          ? educationDataApi["pincode"].custom
                          : ""
                      }
                      value={userInstituteInfo.pincode}
                      id="pincode"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userInstituteInfo",
                          "pincode"
                        )
                      }
                      autoComplete="false"
                      required
                      maxLength="6"
                    />
                    <label>
                      {educationDataApi["pincode"] &&
                        educationDataApi["pincode"].label
                        ? educationDataApi["pincode"].label
                        : ""}
                    </label>
                    <ValidationError fieldValidation={validations["pincode"]} />
                  </article>
                </article>
              ) : null}
              {educationDataApi && educationDataApi["country"].active ? (
                <article className="col-md-4">
                  <article className="form-group">
                    <input
                      type="text"
                      data-custom={
                        educationDataApi["country"] &&
                          educationDataApi["country"].custom
                          ? educationDataApi["country"].custom
                          : ""
                      }
                      value={userInstituteInfo.country}
                      id="country"
                      onChange={event =>
                        educationFormHandler(
                          event,
                          "userInstituteInfo",
                          "country"
                        )
                      }
                      autoComplete="false"
                      required
                    />
                    <label>
                      {educationDataApi["country"] &&
                        educationDataApi["country"].label
                        ? educationDataApi["country"].label
                        : ""}
                    </label>
                    <ValidationError fieldValidation={validations["country"]} />
                  </article>
                </article>
              ) : null}
            </article>
            {educationDataApi && educationDataApi["address"].active ? (
              <article className="col-md-12">
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={
                      educationDataApi["address"] &&
                        educationDataApi["address"].custom
                        ? educationDataApi["address"].custom
                        : ""
                    }
                    value={userInstituteInfo.address}
                    id="address"
                    onChange={event =>
                      educationFormHandler(
                        event,
                        "userInstituteInfo",
                        "address"
                      )
                    }
                    required
                  />
                  <label>
                    {educationDataApi["address"] &&
                      educationDataApi["address"].label
                      ? educationDataApi["address"].label
                      : ""}
                  </label>
                  <ValidationError fieldValidation={validations["address"]} />
                </article>
              </article>
            ) : null}

            {educationDataApi && educationDataApi["description"].active ? (
              <article className="col-md-12">
                <article className="form-group">
                  <input
                    type="text"
                    data-custom={
                      educationDataApi["description"] &&
                        educationDataApi["description"].custom
                        ? educationDataApi["description"].custom
                        : ""
                    }
                    value={userInstituteInfo.description}
                    id="description"
                    onChange={event =>
                      educationFormHandler(
                        event,
                        "userInstituteInfo",
                        "description"
                      )
                    }
                    required
                  />
                  <label>
                    {educationDataApi["description"] &&
                      educationDataApi["description"].label
                      ? educationDataApi["description"].label
                      : ""}
                  </label>
                  <ValidationError
                    fieldValidation={validations["description"]}
                  />
                </article>
              </article>
            ) : null}

            {
              /* classID != graduationId ? ( */
              <article className="row paddingtop0">
                {educationDataApi &&
                  educationDataApi["marking_type"].active &&
                  classID == presentClassID &&
                  userAcademicInfo.currentAcademicYear == 1 ? (
                    ""
                  ) : educationDataApi &&
                    educationDataApi["marking_type"].active ? (
                      <article className="col-md-12 address">Marks details</article>
                    ) : null}
                {educationDataApi &&
                  educationDataApi["marking_type"].active &&
                  classID == presentClassID &&
                  userAcademicInfo.currentAcademicYear ==
                  1 ? null : educationDataApi &&
                    educationDataApi["marking_type"].active ? (
                      <article className="col-md-12 radio-btn">
                        <span>
                          <label>
                            <input
                              type="radio"
                              name="radio"
                              value="1"
                              checked={userAcademicInfo.markingType === "1"}
                              onChange={event => marksObtainedHandler(event)}
                            />
                            <span />
                        &nbsp;&nbsp; <i>Marks</i>
                          </label>
                        </span>
                        <span className="marginleft">
                          <label>
                            <input
                              type="radio"
                              value="2"
                              name="radio"
                              checked={userAcademicInfo.markingType === "2"}
                              onChange={event => marksObtainedHandler(event)}
                            />
                            <span />
                        &nbsp;&nbsp;<i>CGPA</i>
                          </label>
                        </span>
                      </article>
                    ) : null}
              </article>
            }
            {userAcademicInfo.markingType == "1" ? (
              <article className="row">
                {educationDataApi &&
                  educationDataApi["marks_obtained"].active &&
                  classID == presentClassID &&
                  userAcademicInfo.currentAcademicYear ==
                  1 ? null : educationDataApi &&
                    educationDataApi["marks_obtained"].active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            data-custom={
                              educationDataApi["marks_obtained"] &&
                                educationDataApi["marks_obtained"].custom
                                ? educationDataApi["marks_obtained"].custom
                                : ""
                            }
                            value={userAcademicInfo.marksObtained}
                            id="marksObtained"
                            maxLength="4"
                            onChange={event =>
                              educationFormHandler(
                                event,
                                "userAcademicInfo",
                                "marksObtained",
                                "Marks Obtained"
                              )
                            }
                          />
                          <label>
                            {educationDataApi["marks_obtained"] &&
                              educationDataApi["marks_obtained"].label
                              ? educationDataApi["marks_obtained"].label
                              : ""}
                          </label>
                          <ValidationError
                            fieldValidation={validations["marksObtained"]}
                          />
                        </article>
                      </article>
                    ) : null}
                {userAcademicInfo.markingType === "1" &&
                  educationDataApi &&
                  educationDataApi["total_marks"].active &&
                  classID == presentClassID &&
                  userAcademicInfo.currentAcademicYear ==
                  1 ? null : educationDataApi &&
                    educationDataApi["total_marks"].active ? (
                      <article className="col-md-4">
                        <article className="form-group">
                          <input
                            type="text"
                            maxLength="4"
                            data-custom={
                              educationDataApi["total_marks"] &&
                                educationDataApi["total_marks"].custom
                                ? educationDataApi["total_marks"].custom
                                : ""
                            }
                            value={userAcademicInfo.totalMarks}
                            id="totalMarks"
                            onChange={event =>
                              educationFormHandler(
                                event,
                                "userAcademicInfo",
                                "totalMarks",
                                "Total marks"
                              )
                            }
                            required
                          />
                          <label>
                            {educationDataApi["total_marks"] &&
                              educationDataApi["total_marks"].label
                              ? educationDataApi["total_marks"].label
                              : ""}
                          </label>
                          <ValidationError
                            fieldValidation={validations["totalMarks"]}
                          />
                        </article>
                      </article>
                    ) : null}
              </article>
            ) : (
                <article className="row">
                  {userAcademicInfo.markingType === "2" &&
                    educationDataApi &&
                    educationDataApi["grade"].active &&
                    classID == presentClassID &&
                    userAcademicInfo.currentAcademicYear ==
                    1 ? null : educationDataApi &&
                      educationDataApi["grade"].active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <select
                              className="icon"
                              id="grade"
                              value={userAcademicInfo.grade}
                              onChange={event =>
                                educationFormHandler(
                                  event,
                                  "userAcademicInfo",
                                  "grade"
                                )
                              }
                            >
                              <option value="">-Select-</option>
                              <option value="A1">A1</option>
                              <option value="A2">A2</option>
                              <option value="B1">B1</option>
                              <option value="B2">B2</option>
                              <option value="C1">C1</option>
                              <option value="C2">C2</option>
                              <option value="D1">D1</option>
                              <option value="D2">D2</option>
                              <option value="E">E</option>
                            </select>
                            <label className="labelstyle">
                              {educationDataApi["grade"] &&
                                educationDataApi["grade"].label
                                ? educationDataApi["grade"].label
                                : ""}
                            </label>
                            <ValidationError fieldValidation={validations["grade"]} />
                          </article>
                        </article>
                      ) : null}
                  {userAcademicInfo.markingType === "2" &&
                    educationDataApi &&
                    educationDataApi["marks_obtained"].active &&
                    classID == presentClassID &&
                    userAcademicInfo.currentAcademicYear == 1 ? (
                  /* && classID != graduationId  */ <article className="col-md-4">
                        null
                      </article>
                    ) : educationDataApi &&
                      educationDataApi["marks_obtained"].active ? (
                        <article className="col-md-4">
                          <article className="form-group">
                            <input
                              type="text"
                              id="cgpa"
                              data-custom={
                                educationDataApi &&
                                educationDataApi["marks_obtained"].custom
                              }
                              value={userAcademicInfo.marksObtained}
                              onChange={event => {
                                if (
                                  event.target.value <= 10 &&
                                  event.target.value >= 0
                                ) {
                                  educationFormHandler(
                                    event,
                                    "userAcademicInfo",
                                    "marksObtained"
                                  );
                                } else {
                                  event.target.value = 0;
                                }
                              }}
                            />
                            <label>CGPA</label>
                            <ValidationError fieldValidation={validations["cgpa"]} />
                          </article>
                        </article>
                      ) : null}
                </article>
              )}
            <article className="row">
              <article className="col-md-12">
                <input
                  type="button"
                  value="Save"
                  className="btn  pull-right"
                  onClick={e => {
                    if (userAcademicInfo.academicClass.id == 16) {
                      this.validate(userAcademicInfo.stream, customData);
                      if (userAcademicInfo.stream == 1274) {
                        if (pcbPercentage >= 0.85 || pcmPercentage >= 0.85) {
                          if (phyPercentage > 100 || chemPercentage > 100 || bioPercentage > 100 || mathPercentage > 100) {
                            this.props.onEducationSubmitHandler(
                              "GreaterThan100",
                              "You have not entered PHYSICS subject marks obtained."
                            );
                          }
                          else if (customData["biologyMarksObtained"] <= 0) {
                            console.log("please input the biology marks")
                          }
                          else if (customData["mathMarksObtained"] <= 0) {
                            console.log("please input the maths marks")

                          }
                          else {
                            this.props.onEducationSubmitHandler(e);
                          }

                        } else {
                          this.props.onEducationSubmitHandler("fail");
                        }
                      }
                      if (userAcademicInfo.stream == 1273) {
                        if (pcbPercentage >= 0.85) {
                          if (phyPercentage > 100 || chemPercentage > 100 || bioPercentage > 100) {
                            this.props.onEducationSubmitHandler(
                              "GreaterThan100",
                              "You have not entered PHYSICS subject marks obtained."
                            );

                          } else {
                            this.props.onEducationSubmitHandler(e);
                          }
                        } else {
                          this.props.onEducationSubmitHandler("fail");
                        }
                      }
                      if (userAcademicInfo.stream == 1277) {
                        if (pcmPercentage >= 0.85) {
                          if (phyPercentage > 100 || chemPercentage > 100 || mathPercentage > 100) {
                            this.props.onEducationSubmitHandler(
                              "GreaterThan100",
                              "You have not entered PHYSICS subject marks obtained."
                            );

                          } else {
                            console.log(`Pushing data for submit`);
                            this.props.onEducationSubmitHandler(e);
                          }
                        } else {
                          this.props.onEducationSubmitHandler("fail");
                        }
                      }
                    } else {
                      this.props.onEducationSubmitHandler(e);
                    }
                  }}
                />
              </article>
            </article>
          </form>
        </section>
      </article>
    );
  }
}

export default AcadmicClasses;
