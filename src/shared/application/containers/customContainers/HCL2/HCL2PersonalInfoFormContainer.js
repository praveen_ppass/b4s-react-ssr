import { connect } from "react-redux";
import personalInfoForm from '../../../components/customForms/HCL2/HCL2PersonalInfo';
import {
  fetchUserApplicationPersonalInfo as fetchUserApplicationPersonalInfoAction,
  updatePersonalInfo as updatePersonalInfoAction,
  fetchPersonalInfoConfig as fetchPersonalInfoConfigAction
} from "../../../actions/personalInfoActions";
import {
  fetchDependantData as fetchDependantAction,
  fetchOtherDependantData as fetchOtherDependantDataAction
} from "../../../../../constants/commonActions";
import { applicationInstructionStep as applicationInstructionStepAction } from "../../../actions/applicationFormAction";
const mapStateToProps = ({ applicationPersonalInfo, common }) => ({
  ...applicationPersonalInfo,
  district: common.district,
  otherdistrict: common.otherdistrict
});

const mapDispatchToProps = dispatch => ({
  getDistrictsByState: inputData => dispatch(fetchDependantAction(inputData)),
  fetchApplicationPersonalInfo: inputData =>
    dispatch(fetchUserApplicationPersonalInfoAction(inputData)),
  fetchPersonalInfoConfig: inputData =>
    dispatch(fetchPersonalInfoConfigAction(inputData)),
  updatePersonalInfo: inputData =>
    dispatch(updatePersonalInfoAction(inputData)),
  getCompletedSteps: inputData =>
    dispatch(applicationInstructionStepAction(inputData)),
  getCaDistrictByState: inputData =>
    dispatch(fetchOtherDependantDataAction(inputData))
});
export default connect(mapStateToProps, mapDispatchToProps)(personalInfoForm);
