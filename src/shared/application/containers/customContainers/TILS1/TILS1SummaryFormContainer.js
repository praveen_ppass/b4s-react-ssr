import { connect } from "react-redux";
import summary from "../../../components/customForms/GenericPage/SummaryInfo";
import {
  getAllSummery as getAllSummeryAction,
  fetchSchApply as fetchSchApplyAction
} from "../../../actions/applicationSummeryAction";
import { applicationInstructionStep as applicationInstructionStepAction } from "../../../actions/applicationFormAction";
import { fetchUserRules as fetchUserRulesAction } from "../../../../../constants/commonActions";

const mapStateToProps = ({ applicationSummery, common }) => ({
  applicationSummery: applicationSummery,
  showLoader: applicationSummery.showLoader,
  userRuleInfo: common.userRulesData
});

const mapDispatchToProps = dispatch => ({
  getAllSummery: input => dispatch(getAllSummeryAction(input)),
  fetchSchApply: inputData => dispatch(fetchSchApplyAction(inputData)),
  applicationInstructionStep: inputData =>
    dispatch(applicationInstructionStepAction(inputData)),
  fetchUserRule: inputData => dispatch(fetchUserRulesAction(inputData))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(summary);
