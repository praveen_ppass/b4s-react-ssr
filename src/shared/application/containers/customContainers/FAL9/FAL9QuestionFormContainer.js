import { connect } from "react-redux";

import {
  saveQuestionData as saveQuestionDataAction,
  getQuestionData as getQuestionDataAction
} from "../../../actions/questionAction";

import { updateStep as updateStepAction } from "../../../actions/educationInfoAction";

import questions from "../../../components/customForms/FAL9/FAL9QuestionInfo";
import { applicationInstructionStep as applicationInstructionStepAction } from "../../../actions/applicationFormAction";

const mapStateToProps = ({ applicationQuestion }) => ({
  QuestionDataList: applicationQuestion
});

const mapDispatchToProps = dispatch => ({
  saveQuestionData: inputData => dispatch(saveQuestionDataAction(inputData)),
  getQuestionData: inputData => dispatch(getQuestionDataAction(inputData)),
  updateStep: inputData => dispatch(updateStepAction(inputData)),
  applicationInstructionStep: inputData =>
    dispatch(applicationInstructionStepAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(questions);
