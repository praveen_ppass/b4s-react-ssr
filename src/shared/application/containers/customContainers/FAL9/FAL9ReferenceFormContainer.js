import { connect } from "react-redux";

import {
  saveReferenceData as saveReferenceDataAction,
  getReferenceData as getReferenceDataAction,
  deleteReferenceData as deleteReferenceDataAction
} from "../../../actions/referenceAction";
import reference from "../../../components/customForms/FAL9/FAL9Reference";
import { updateStep as updateStepAction } from "../../../actions/educationInfoAction";
import { applicationInstructionStep as applicationInstructionStepAction } from "../../../actions/applicationFormAction";

const mapStateToProps = ({ applicationReference }) => ({
  referenceDataConfig: applicationReference,
  showLoader: applicationReference.showLoader
});

const mapDispatchToProps = dispatch => ({
  saveReferenceData: inputData => dispatch(saveReferenceDataAction(inputData)),
  getReferenceData: inputData => dispatch(getReferenceDataAction(inputData)),
  deleteReferenceData: inputData =>
    dispatch(deleteReferenceDataAction(inputData)),
  applicationInstructionStep: inputData =>
    dispatch(applicationInstructionStepAction(inputData)),
  updateStep: inputData => dispatch(updateStepAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(reference);
