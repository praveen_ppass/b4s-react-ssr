import React, { Component } from "react";
import { connect } from "react-redux";
import EducationInfo from "../../../components/customForms/GenericPage/EducationInfo";
import {
  getEducationInfo as fetchEducationInfoAction,
  getDistrictInfo as getDistrictInfoAction,
  getSubjectInfo as getSubjectInfoAction,
  saveEducationData as saveEducationInfoAction,
  getEduConfig as getEducationConfigAction,
  updateStep as updateStepAction,
  getClassConfig as getClassConfigAction,
  deleteAcademicList as deleteAcademicListAction,
  saveLif9PresentClass as saveLife9PresentData
} from "../../../actions/educationInfoAction";
import { fetchUserApplicationPersonalInfo as fetchUserApplicationPersonalInfoAction } from "../../../actions/personalInfoActions";
import { applicationInstructionStep as applicationInstructionStepAction } from "../../../actions/applicationFormAction";
import { fetchFilterRule as fetchFilterRuleAction } from "../../../../../constants/commonActions";

const mapStateToProps = ({
  applicationEducation,
  applicationForm,
  applicationPersonalInfo,
  common
}) => ({
  ...applicationEducation,
  educationConfigInfo: applicationEducation.educationConfig,
  allRules: applicationForm,
  district: applicationEducation.district,
  type: applicationEducation.type,
  showLoader: applicationEducation.showLoader,
  classConfig: applicationEducation.classConfig,
  personalInfoData: applicationPersonalInfo.personalInfoData,
  filterRules: common.filterRules,
  updatedStep: applicationEducation.updateStep,
  customType: common.type
});

const mapDispatchToProps = dispatch => ({
  fetchEduInfo: inputData => dispatch(fetchEducationInfoAction(inputData)),
  getSubject: inputData => dispatch(getSubjectInfoAction(inputData)),
  fetchDistrictList: inputData => dispatch(getDistrictInfoAction(inputData)),
  saveEducationInfo: inputData => dispatch(saveEducationInfoAction(inputData)),
  getEducationConfig: inputData =>
    dispatch(getEducationConfigAction(inputData)),
  updateStep: inputData => dispatch(updateStepAction(inputData)),
  getCompletedSteps: inputData =>
    dispatch(applicationInstructionStepAction(inputData)),
  getConfigOfClss: inputData => dispatch(getClassConfigAction(inputData)),
  deleteAcademic: inputData => dispatch(deleteAcademicListAction(inputData)),
  fetchApplicationPersonalInfo: inputData =>
    dispatch(fetchUserApplicationPersonalInfoAction(inputData)),
  fetchFilterRuleById: inputData => dispatch(fetchFilterRuleAction(inputData))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EducationInfo);
