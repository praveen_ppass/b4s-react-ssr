import React, { Component } from "react";
import { connect } from "react-redux";
import EducationInfo from "../../../components/customForms/GenericPage/EducationInfo";
// import gblFunc from "../../../../../globals/globalFunctions";
// import {
//   handleConditionalValidations,
//   otherStateId,
//   graduationId,
//   otherStreamIdGraduation,
//   otherStreamIdTwelfth,
//   nextStepName,
//   config,
//   educationSpecialValidations,
//   addSpecialValidations,
//   elvenId,
//   twelthId,
//   classTenthId,
//   otherBoardId,
//   otherStreamIdEleven,
//   postGradId,
//   otherStreamIdPostGraduation,
//   twelthPassId,
//   extractStepTemplate,
//   childEduBSID,
//   streamRelatedValidation
// } from "../../../formconfig";
// import {
//   FETCH_EDUCATION_INFO_FORM_SUCCESS,
//   UPDATE_EDUCATION_INFO_FORM_SUCCESS,
//   UPDATE_EDUCATION_INFO_FORM_FAILURE,
//   FETCH_EDUCATION_INFO_CONFIG_REQUEST,
//   FETCH_EDUCATION_INFO_CONFIG_SUCCESS,
//   UPDATE_EDUCATION_INFO_STEP_SUCCESS,
//   FETCH_EDUCATION_CLASS_CONFIG_SUCCESS,
//   DELETE_EDUCATION_INFO_SUCCESS,
//   FETCH_EDUCATION_CLASS_CONFIG_FAILURE
// } from "../../../actions/educationInfoAction";
// import AcadmicClasses from "./CES3AcademicClassFormContainer";
// import { getYearOrMonth } from "../../../../../constants/constants";
// // import EditAcadmicClasses from "../../standardForms/sub-forms/editAcademicClass";
// import Loader from "../../../../common/components/loader";
// import AlertMessage from "../../../../common/components/alertMsg";
// import { mapValidationFunc } from "../../../../../validation/rules";
// import { ruleRunner } from "../../../../../validation/ruleRunner";
// import {
//   FETCH_SCHOLARSHIP_APPLY_SUCCESS,
//   FETCH_FILTER_RULES_SUCCEEDED
// } from "../../../../../constants/commonActions";
// import { FETCH_APPLICATION_STEP_SUCCESS } from "../../../actions/applicationFormAction";
// import ConfirmMessagePopup from "../../../../common/components/confirmMessagePopup";
// import {
//   FETCH_PERSONAL_INFO_CONFIG_SUCCEEDED,
//   APPLICATION_FETCH_PERSONAL_INFO_SUCCEEDED
// } from "../../../actions/personalInfoActions";
import {
  getEducationInfo as fetchEducationInfoAction,
  getDistrictInfo as getDistrictInfoAction,
  getSubjectInfo as getSubjectInfoAction,
  saveEducationData as saveEducationInfoAction,
  getEduConfig as getEducationConfigAction,
  updateStep as updateStepAction,
  getClassConfig as getClassConfigAction,
  deleteAcademicList as deleteAcademicListAction,
  saveLif9PresentClass as saveLife9PresentData
} from "../../../actions/educationInfoAction";
import { fetchUserApplicationPersonalInfo as fetchUserApplicationPersonalInfoAction } from "../../../actions/personalInfoActions";
import { applicationInstructionStep as applicationInstructionStepAction } from "../../../actions/applicationFormAction";
import { fetchFilterRule as fetchFilterRuleAction } from "../../../../../constants/commonActions";

// class EducationInfoCES3 extends Component {
//   constructor(props) {
//     super(props);

//     this.editEduction = this.editEduction.bind(this);
//     this.onSelectClassHandler = this.onSelectClassHandler.bind(this);
//     this.educationFormHandler = this.educationFormHandler.bind(this);
//     this.marksObtainedHandler = this.marksObtainedHandler.bind(this);
//     this.onEducationSubmitHandler = this.onEducationSubmitHandler.bind(this);
//     this.getValidationRulesObject = this.getValidationRulesObject.bind(this);
//     this.checkFormValidations = this.checkFormValidations.bind(this);
//     this.fireEducationConfigCall = this.fireEducationConfigCall.bind(this);
//     this.goToNextStep = this.goToNextStep.bind(this);
//     this.ifAllClassesFilled = this.ifAllClassesFilled.bind(this);
//     this.ifClassDetailsAreCompleted = this.ifClassDetailsAreCompleted.bind(
//       this
//     );
//     this.onSelectPresentClassHandler = this.onSelectPresentClassHandler.bind(
//       this
//     );

//     this.onDeleteEducation = this.onDeleteEducation.bind(this);
//     this.hideConfirmationPopup = this.hideConfirmationPopup.bind(this);
//     this.onDeleteEducationListHandler = this.onDeleteEducationListHandler.bind(
//       this
//     );
//     this.onEditEducationHandler = this.onEditEducationHandler.bind(this);
//     this.checkClassIdHandler = this.checkClassIdHandler.bind(this);
//     this.isStepsCompleted = this.isStepsCompleted.bind(this);

//     this.state = {
//       childVisible: true,
//       deleteClassId: null,
//       isClassIdDeleteable: false,
//       applicationEducationData: null,
//       classID: null,
//       presentClassID: null,
//       marksOrCgpa: true,
//       isEditable: false,
//       showLoader: false,
//       deleteClass: null,
//       isFormValid: true,
//       onSelectClassMode: true,
//       msgClassName: "",
//       msg: "",
//       status: false,
//       stepUpdated: false,
//       isStepCompleted: false,
//       classConfigList: [],
//       classConfigFields: null,
//       configFieldsData: {},
//       showConfirmationPopup: false,
//       deleteSuccessCallBack: null,
//       validations: {},
//       defultValidations: {},
//       editableConfig: null,
//       educationList: {},
//       eduState: {
//         customData: {},
//         userAcademicInfo: {
//           academicClass: {
//             id: "",
//             value: ""
//           },
//           academicClassName: "",
//           rollNo: "",
//           board: "",
//           boardName: "",
//           courseDuration: "",
//           courseStartDate: "",
//           currentDegreeYear: "",
//           degree: "",
//           fee: "",
//           grade: "",
//           id: "",
//           markingType: "1",
//           marksObtained: "",
//           otherBoard: "",
//           passingMonth: "",
//           passingYear: "",
//           percentage: "",
//           presentClass: 0,
//           stream: "",
//           totalMarks: ""
//         },
//         userInstituteInfo: {
//           academicDetailId: "",
//           address: "",
//           city: "",
//           country: "",
//           description: "",
//           district: "",
//           districtName: "",
//           id: "",
//           instituteEmail: "",
//           instituteName: "",
//           institutePhone: "",
//           pincode: "",
//           principalName: "",
//           state: "",
//           stateName: ""
//         }
//       },
//       eduInstructionTemplate: ""
//     };
//   }
//   onClick() {
//     this.setState(prevState => ({
//       childVisible: !prevState.childVisible
//     }));
//   }

//   componentDidMount() {
//     if (this.props.schApply) {
//       //If educatrion tab is not opened directly........@Pushpendra
//       this.fireEducationConfigCall(this.props);
//     }
//   }

//   fireEducationConfigCall(nextProps) {
//     this.props.getEducationConfig({
//       scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
//       step: "EDUCATION_INFO",
//       scholarshipType: nextProps.schApply
//         ? nextProps.schApply.scholarshipType
//         : ""
//     });
//   }

//   componentWillReceiveProps(nextProps) {
//     const { type } = nextProps;
//     switch (type) {
//       case FETCH_EDUCATION_INFO_CONFIG_SUCCESS:
//         this.props.fetchApplicationPersonalInfo({
//           userId: gblFunc.getStoreUserDetails()["userId"],
//           scholarshipId: gblFunc.getStoreApplicationScholarshipId()
//         });
//         break;
//       case APPLICATION_FETCH_PERSONAL_INFO_SUCCEEDED:
//         const userinfo = nextProps.personalInfoData;
//         const { academic_class } = nextProps.educationConfigInfo;
//         if (userinfo && userinfo.userRules && userinfo.userRules.length) {
//           const { userRules } = userinfo;
//           let presentClassInfo = userRules.filter(rule => rule.ruleTypeId == 1);
//           if (presentClassInfo && presentClassInfo.length) {
//             if (
//               academic_class &&
//               academic_class.dataOptions &&
//               academic_class.dataOptions.length
//             ) {
//               let filteredPresentClass = academic_class.dataOptions.filter(
//                 option => option.id == presentClassInfo[0].ruleId
//               );
//               if (filteredPresentClass && filteredPresentClass.length > 0) {
//                 let event = {
//                   target: {
//                     value: filteredPresentClass[0].id
//                   }
//                 };
//                 this.onSelectPresentClassHandler(event);
//               }
//             }
//           }
//         }

//         break;
//       case FETCH_EDUCATION_INFO_FORM_SUCCESS:
//         const { applicationEducationData } = nextProps;
//         let event = {
//           target: {
//             value: this.state.presentClassID
//           }
//         };
//         if (
//           applicationEducationData &&
//           applicationEducationData.userEducationList &&
//           applicationEducationData.userEducationList.length
//         ) {
//           const educations = applicationEducationData.userEducationList;
//           const classes = {};
//           educations.map(e => {
//             if (
//               e.userAcademicInfo &&
//               e.userAcademicInfo.presentClass == 1 &&
//               this.state.onSelectClassMode
//             ) {
//               classes[this.state.presentClassID] = {
//                 deleteClassId: e.userAcademicInfo.id,
//                 value: e.userAcademicInfo.academicClass.value,
//                 id: e.userAcademicInfo.academicClass.id,
//                 presentClass: e.userAcademicInfo.presentClass,
//                 eduList: e
//               };
//             }

//             if (
//               e.userAcademicInfo &&
//               e.userAcademicInfo.presentClass != 1 &&
//               e.userAcademicInfo.academicClass.id ==
//                 this.state.presentClassID &&
//               this.state.onSelectClassMode
//             ) {
//               classes[this.state.presentClassID] = {
//                 deleteClassId: e.userAcademicInfo.id,
//                 value: e.userAcademicInfo.academicClass.value,
//                 id: e.userAcademicInfo.academicClass.id,
//                 presentClass: e.userAcademicInfo.presentClass,
//                 eduList: e
//               };
//             }
//           });

//           if (classes && Object.keys(classes).length) {
//             this.onDeleteEducation(classes, this.state.presentClassID);
//           }

//           if (
//             classes &&
//             Object.keys(classes).length &&
//             classes[this.state.presentClassID].presentClass &&
//             classes[this.state.presentClassID].id ==
//               this.state.presentClassID &&
//             this.state.classConfigList.length == 1
//           ) {
//             this.editEduction(classes[this.state.presentClassID].eduList);
//           }
//         } else {
//           if (
//             this.state.onSelectClassMode &&
//             this.state.classConfigList.length == 1
//           ) {
//             this.onSelectClassHandler(event);
//           }
//         }
//         this.setState({
//           applicationEducationData
//         });

//         break;
//       case FETCH_SCHOLARSHIP_APPLY_SUCCESS: //If education tab is opened directly.....@Pushpendra
//         if (!this.props.educationConfigInfo) {
//           this.fireEducationConfigCall(nextProps);
//         }
//         break;

//       case UPDATE_EDUCATION_INFO_FORM_SUCCESS:
//         this.setState(
//           {
//             status: true,
//             msg: "Successfully Submitted",
//             showLoader: true,
//             eduState: {
//               customData: {},
//               userAcademicInfo: {
//                 academicClass: {
//                   id: "",
//                   value: ""
//                 },
//                 academicClassName: "",
//                 board: "",
//                 boardName: "",
//                 courseDuration: "",
//                 currentDegreeYear: "",
//                 degree: "",
//                 fee: "",
//                 grade: "",
//                 id: "",
//                 markingType: "1",
//                 marksObtained: "",
//                 otherBoard: "",
//                 passingMonth: "",
//                 passingYear: "",
//                 percentage: "",
//                 presentClass: 0,
//                 stream: "",
//                 totalMarks: ""
//               },
//               userInstituteInfo: {
//                 academicDetailId: "",
//                 address: "",
//                 city: "",
//                 country: "",
//                 description: "",
//                 district: "",
//                 districtName: "",
//                 id: "",
//                 instituteEmail: "",
//                 instituteName: "",
//                 institutePhone: "",
//                 pincode: "",
//                 principalName: "",
//                 state: "",
//                 stateName: ""
//               }
//             },
//             isEditable: false,
//             isStepCompleted: this.ifAllClassesFilled(true), //form submitted
//             classID: ""
//           },
//           () =>
//             this.props.fetchEduInfo({
//               scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
//               step: "EDUCATION_INFO",
//               presentClassId: this.state.presentClassID
//             })
//         );

//         break;
//       case UPDATE_EDUCATION_INFO_FORM_FAILURE:
//         this.setState({
//           status: false,
//           msg: "Something went wrong, please try again!",
//           showLoader: true,
//           isEditable: this.state.isEditable ? true : false
//         });
//         break;
//       case UPDATE_EDUCATION_INFO_STEP_SUCCESS:
//         if (this.state.stepUpdated) {
//           this.setState({ stepUpdated: false });

//           if (
//             nextProps.updatedStep &&
//             nextProps.updatedStep.step === "EDUCATION_INFO"
//           ) {
//             this.props.getCompletedSteps({
//               userId: gblFunc.getStoreUserDetails()["userId"],
//               scholarshipId: gblFunc.getStoreApplicationScholarshipId()
//             });
//           }

//           setTimeout(() => {
//             if (
//               this.props.match.params.bsid &&
//               this.props.redirectionSteps &&
//               this.props.redirectionSteps.length
//             ) {
//               this.props.history.push(
//                 `/application/${this.props.match.params.bsid}/form/${
//                   this.props.redirectionSteps[
//                     this.props.redirectionSteps.indexOf("educationInfo") + 1
//                   ]
//                 }`
//               );
//             }
//           }, 2000);
//         }
//         break;

//       case FETCH_EDUCATION_CLASS_CONFIG_SUCCESS:
//         if (!this.state.isEditable) {
//           this.setState(
//             {
//               classConfigList: nextProps.classConfig
//             },
//             () => {
//               this.props.fetchEduInfo({
//                 scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
//                 step: "EDUCATION_INFO",
//                 presentClassId: this.state.presentClassID
//               });
//             }
//           );
//           return;
//         } else {
//           const configFieldsData = { ...this.state.configFieldsData };
//           const validate = {};
//           const classConfig = nextProps.classConfig.find(
//             list => list.classId == this.state.classID
//           );

//           if (classConfig && Object.keys(classConfig).length > 0) {
//             const validateFields = classConfig.fields;
//             for (let key in validateFields) {
//               if (
//                 validateFields[key].validations !== null &&
//                 validateFields[key].active
//               ) {
//                 let clearUnderScore = gblFunc.replace_underScore(key);
//                 validate[clearUnderScore] = null;
//                 configFieldsData[clearUnderScore] = validateFields[key];
//               }
//             }

//             this.setState(
//               {
//                 eduState: this.state.educationList,
//                 showConfirmationPopup: false,
//                 defultValidations: validate,
//                 validations: validate,
//                 classConfigFields:
//                   classConfig && classConfig.fields ? classConfig.fields : [],
//                 classID: this.state.classID,
//                 configFieldsData
//               },
//               () => {
//                 if (
//                   this.state.educationList.userInstituteInfo &&
//                   this.state.educationList.userInstituteInfo.state
//                 ) {
//                   this.props.fetchDistrictList({
//                     id: this.state.educationList.userInstituteInfo.state
//                   });
//                 }
//               }
//             );
//           }
//         }

//         break;
//       case DELETE_EDUCATION_INFO_SUCCESS:
//         this.setState(
//           {
//             showConfirmationPopup: false
//           },
//           () => {
//             this.props.fetchEduInfo({
//               scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
//               step: "EDUCATION_INFO",
//               presentClassId: this.state.presentClassID
//             });
//             this.props.getCompletedSteps({
//               userId: gblFunc.getStoreUserDetails()["userId"],
//               scholarshipId: gblFunc.getStoreApplicationScholarshipId()
//             });
//           }
//         );
//         break;
//       case FETCH_EDUCATION_CLASS_CONFIG_FAILURE:
//         this.setState({
//           showConfirmationPopup: false,
//           presentClassID: this.state.deleteClass
//         });
//         break;

//       default:
//         break;
//     }
//   }

//   onDeleteEducation(classes, presentClssId, applicationEducationData) {
//     const { id, presentClass, deleteClassId, value, eduList } = classes[
//       presentClssId
//     ];

//     if (presentClass == 1 && id != presentClssId) {
//       this.setState({
//         showConfirmationPopup: true,
//         deleteClassId: deleteClassId,
//         isClassIdDeleteable: true,
//         deleteClass: id,
//         msg: `You have already saved ${value} as a present class, would like to delete it ?`
//       });
//     }

//     if (presentClass != 1) {
//       this.setState({
//         showConfirmationPopup: true,
//         isClassIdDeleteable: true,
//         deleteClassId: deleteClassId,
//         deleteClass: id,
//         msg: `You have already saved ${value}, would like to delete it ?`
//       });
//     }
//   }

//   onEditEducationHandler(list, value) {
//     const { userInstituteInfo, userAcademicInfo } = list;

//     const validations = { ...this.state.validations };

//     for (let key in validations) {
//       validations[key] = null;
//     }

//     // this.props.getSubject({ id: list.userAcademicInfo.academicClass.id });
//     this.setState({
//       eduState: list,
//       isEditable: true,
//       validations,
//       marksOrCgpa: list.userAcademicInfo.markingType == "1" ? true : false,
//       classID: list.userAcademicInfo.academicClass.id
//     });
//   }

//   onDeleteEducationListHandler() {
//     if (this.state.isClassIdDeleteable) {
//       this.props.deleteAcademic({
//         userId: gblFunc.getStoreUserDetails()["userId"],
//         scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
//         classId: this.state.deleteClassId
//       });
//     }
//   }

//   ifAllClassesFilled(formSubmitted = 0) {
//     //Called on form submission flag
//     let savedClass = [];
//     if (this.state.classConfigList && this.state.classConfigList.length) {
//       const isOptional = this.state.classConfigList.filter(
//         optional => optional.isOptional == true
//       );

//       switch (isOptional.length) {
//         case 0:
//           const isStepCompleted = this.isStepsCompleted(
//             this.state.classConfigList
//           );

//           return isStepCompleted.length > 0 ? false : true;
//         case 1:
//           return this.isStepsCompleted(this.state.classConfigList).length == 0
//             ? true
//             : false;
//         case 2:
//           const { presentClassID } = this.state;

//           if (presentClassID && [22, 23].includes(parseInt(presentClassID))) {
//             let saveClassArray = null;
//             let savedClass = [];
//             isOptional.map(optionObj => {
//               saveClassArray = this.checkClassIdHandler(optionObj.classId);
//               if (saveClassArray && saveClassArray.length) {
//                 savedClass.push(saveClassArray);
//               }
//             });

//             return savedClass &&
//               savedClass.length &&
//               this.isStepsCompleted(this.state.classConfigList).length == 0
//               ? true
//               : false;
//           }
//       }
//     } else {
//       return false;
//     }
//   }

//   isStepsCompleted(configListClass) {
//     const filteredDisableList = configListClass.filter(
//       list => !list.disabled && !list.isOptional
//     );
//     return filteredDisableList;
//   }

//   checkClassIdHandler(classID) {
//     const saveClass = [];
//     const { applicationEducationData } = this.state;

//     if (
//       applicationEducationData &&
//       applicationEducationData.userEducationList &&
//       applicationEducationData.userEducationList.length
//     ) {
//       applicationEducationData.userEducationList.map(list => {
//         if (
//           list.userAcademicInfo &&
//           list.userAcademicInfo.academicClass &&
//           list.userAcademicInfo.academicClass.id &&
//           list.userAcademicInfo.academicClass.id == classID
//         ) {
//           saveClass.push(true);
//         }
//       });
//       return saveClass;
//     }
//   }

//   getDistrictHandler(event) {
//     this.educationFormHandler(event, "userInstituteInfo", "state");
//   }

//   onSelectClassHandler(event) {
//     const { value } = event.target;
//     let updatedValidation = { ...this.state.validations };

//     const updatedConfigFieldsData = { ...this.state.configFieldsData };
//     const updatedClassConfig = this.state.classConfigList.find(
//       clss => clss.classId === parseInt(value, 10)
//     );
//     let e = {
//       target: {
//         id: updatedClassConfig && updatedClassConfig.classId,
//         value: updatedClassConfig && updatedClassConfig.className
//       }
//     };

//     // empty the previous validation if any
//     if (Object.keys(updatedValidation).length) {
//       updatedValidation = [];
//     }

//     if (updatedClassConfig && Object.keys(updatedClassConfig).length > 0) {
//       const updateFields = updatedClassConfig && updatedClassConfig.fields;
//       for (let key in updateFields) {
//         if (
//           updateFields[key].validations !== null &&
//           updateFields[key].active
//         ) {
//           let clearUnderScore = gblFunc.replace_underScore(key);
//           updatedValidation[clearUnderScore] = null;
//           updatedConfigFieldsData[clearUnderScore] = updateFields[key];
//         }
//       }
//     }

//     this.setState(
//       {
//         classID: value,
//         classConfigFields:
//           updatedClassConfig && updatedClassConfig.fields
//             ? updatedClassConfig.fields
//             : null,
//         isEditable: true,
//         validations: updatedValidation,
//         defultValidations: updatedValidation,
//         eduState: {
//           customData: {},
//           userAcademicInfo: {
//             academicClass: {
//               id: "",
//               value: ""
//             },
//             academicClassName: "",
//             rollNo: "",
//             board: "",
//             boardName: "",
//             courseDuration: "",
//             currentDegreeYear: "",
//             degree: "",
//             fee: "",
//             grade: "",
//             id: "",
//             markingType: "1",
//             marksObtained: "",
//             otherBoard: "",
//             passingMonth: "",
//             passingYear: "",
//             percentage: "",
//             presentClass: 0,
//             stream: "",
//             totalMarks: ""
//           },
//           userInstituteInfo: {
//             academicDetailId: "",
//             address: "",
//             city: "",
//             country: "",
//             description: "",
//             district: "",
//             districtName: "",
//             id: "",
//             instituteEmail: "",
//             instituteName: "",
//             institutePhone: "",
//             pincode: "",
//             principalName: "",
//             state: "",
//             stateName: ""
//           }
//         },
//         configFieldsData: updatedConfigFieldsData
//       },
//       () => {
//         this.props.getSubject({ id: this.state.classID });

//         this.props.fetchFilterRuleById(this.state.classID);
//         this.educationFormHandler(e, "userAcademicInfo", "academicClass");
//       }
//     );
//   }

//   onSelectPresentClassHandler(event) {
//     const { value } = event.target;
//     //gblFunc.getStoreApplicationScholarshipId()

//     this.setState(
//       {
//         isEditable: false,
//         onSelectClassMode: true,
//         presentClassID: value ? value : "",
//         classID: ""
//       },
//       () => {
//         if (this.state.presentClassID) {
//           this.props.getConfigOfClss({
//             scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
//             classID: value
//           });
//         }
//       }
//     );
//   }

//   marksObtainedHandler(event) {
//     const { value } = event.target;
//     const validations = { ...this.state.validations };
//     const { userAcademicInfo } = { ...this.state.eduState };
//     if (value && value == "1") {
//       userAcademicInfo.grade = "";
//       userAcademicInfo.marksObtained = "";
//       userAcademicInfo.totalMarks = "";
//       validations["marksObtained"] = null;
//       validations["totalMarks"] = null;
//       validations["grade"] = null;
//     } else {
//       userAcademicInfo.grade = "";
//       userAcademicInfo.marksObtained = "";
//       userAcademicInfo.totalMarks = 10;
//       validations["grade"] = null;
//       validations["cgpa"] = null;
//     }
//     userAcademicInfo.markingType = value;
//     this.setState({
//       ...this.state.eduState,
//       userAcademicInfo,
//       validations,
//       marksOrCgpa: value === "1" ? true : false
//     });
//   }

//   editEduction(eduList) {
//     const { userInstituteInfo, userAcademicInfo } = eduList;

//     const validations = { ...this.state.validations };

//     for (let key in validations) {
//       validations[key] = null;
//     }

//     // this.props.getSubject({ id: eduList.userAcademicInfo.academicClass.id });
//     this.setState(
//       {
//         educationList: eduList,
//         isEditable: true,
//         showConfirmationPopup: false,
//         validations,
//         marksOrCgpa: eduList.userAcademicInfo.markingType == "1" ? true : false,
//         classID: eduList.userAcademicInfo.academicClass.id
//       },
//       () => {
//         this.props.getConfigOfClss({
//           scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
//           classID: this.state.presentClassID,
//           query: userAcademicInfo.academicClass.id
//         });

//         this.props.getSubject({ id: this.state.classID });
//         this.props.fetchFilterRuleById(this.state.classID);
//       }
//     );
//   }

//   educationFormHandler(event, type, subType, validationName) {
//     // event.preventDefault();
//     const { value, id } = event.target;
//     let validations = { ...this.state.validations };

//     const updateEducationForm = {
//       ...this.state.eduState
//     };

//     if (validations.hasOwnProperty(id)) {
//       const { name, validationFunctions } = this.getValidationRulesObject(
//         id,
//         validationName
//       );
//       if (validationFunctions) {
//         const validationResult = ruleRunner(
//           value,
//           id,
//           name,
//           ...validationFunctions
//         );
//         validations[id] = validationResult[id];
//       }
//     }

//     // const updateFormData = { ...this.state.formData };
//     // updateFormData[id] = value;
//     // this.setState({
//     //   formData: updateFormData,
//     //   validations
//     // });

//     switch (type) {
//       case "userAcademicInfo":
//         if (subType && subType == "academicClass") {
//           updateEducationForm.userAcademicInfo.academicClass = {
//             id,
//             value
//           };
//         } else {
//           updateEducationForm.userAcademicInfo[subType] = value;
//         }
//         break;
//       case "userInstituteInfo":
//         if (subType == "state" && value) {
//           this.props.fetchDistrictList({ id: value });
//         }

//         updateEducationForm.userInstituteInfo[subType] = value;
//         break;
//       case "customInfo":
//         updateEducationForm.customData[subType] = value;
//         break;
//       default:
//         break;
//     }

//     this.setState({
//       eduState: updateEducationForm,
//       validations
//     });
//   }

//   getValidationRulesObject(fieldID, name = "*Field") {
//     let validationObject = {};
//     let validationRules = [];
//     if (this.state.defultValidations.hasOwnProperty(fieldID)) {
//       let configFields = this.state.configFieldsData[fieldID];
//       if (configFields && configFields.validations != null) {
//         let validationArr = configFields.validations;
//         if (educationSpecialValidations.indexOf(fieldID) > -1) {
//           addSpecialValidations(validationArr, fieldID, [this.state.eduState]);
//         }
//         validationArr.map(res => {
//           if (res != null) {
//             let validator = mapValidationFunc(res);
//             if (validator != undefined) {
//               validationRules.push(validator);
//             }
//           }
//         });
//         validationObject.name = name;
//         validationObject.validationFunctions = validationRules;
//       }
//     }
//     return validationObject;
//   }

//   checkFormValidations() {
//     let validations = { ...this.state.validations };

//     for (let key in validations) {
//       validations[key] = null;
//     }
//     /************* start Update conditional validations.........****/

//     if (
//       this.state.eduState.userAcademicInfo &&
//       this.state.eduState.userAcademicInfo.currentAcademicYear == 1 &&
//       this.state.eduState.userAcademicInfo.academicClass.id >= 21
//     ) {
//       validations = handleConditionalValidations(
//         true,
//         true,
//         null,
//         ["grade", "cgpa", "totalMarks", "marksObtained"],
//         validations
//       );
//     }
//     if (this.state.marksOrCgpa) {
//       validations = handleConditionalValidations(
//         this.state.marksOrCgpa,
//         true,
//         null,
//         ["grade", "cgpa"],
//         validations
//       );
//     } else {
//       validations = handleConditionalValidations(
//         this.state.marksOrCgpa,
//         false,
//         null,
//         ["totalMarks", "marksObtained"],
//         validations
//       );
//     }
//     if (this.state.classID == elvenId) {
//       validations = handleConditionalValidations(
//         this.state.classID,
//         elvenId,
//         null,
//         ["board", "otherBoard"],
//         validations
//       );
//     }
//     if (this.state.classID == graduationId) {
//       validations = handleConditionalValidations(
//         this.state.classID,
//         graduationId,
//         null,
//         ["board", "otherBoard"],
//         validations
//       );
//     }

//     if (this.state.classID == classTenthId) {
//       validations = handleConditionalValidations(
//         this.state.classID,
//         classTenthId,
//         null,
//         ["stream", "otherStream"],
//         validations
//       );
//     }

//     if (this.state.classID == postGradId) {
//       validations = handleConditionalValidations(
//         this.state.classID,
//         postGradId,
//         null,
//         ["board", "otherBoard"],
//         validations
//       );
//     }

//     if (
//       this.state.eduState.userAcademicInfo.degree != otherStreamIdGraduation &&
//       this.state.classID == graduationId
//     ) {
//       validations = handleConditionalValidations(
//         this.state.eduState.userAcademicInfo.degree,
//         otherStreamIdGraduation,
//         "notEqual",
//         ["otherStream"],
//         validations
//       );
//     }

//     if (
//       this.state.eduState.userAcademicInfo.degree !=
//         otherStreamIdPostGraduation &&
//       this.state.classID == postGradId
//     ) {
//       validations = handleConditionalValidations(
//         this.state.eduState.userAcademicInfo.degree,
//         otherStreamIdPostGraduation,
//         "notEqual",
//         ["otherStream"],
//         validations
//       );
//     }

//     if (
//       this.state.eduState.userAcademicInfo.stream != otherStreamIdTwelfth &&
//       this.state.classID == twelthId
//     ) {
//       validations = handleConditionalValidations(
//         this.state.eduState.userAcademicInfo.stream,
//         otherStreamIdTwelfth,
//         "notEqual",
//         ["otherStream"],
//         validations
//       );
//     }

//     if (
//       this.state.eduState.userAcademicInfo.stream != 753 &&
//       this.state.classID == twelthPassId
//     ) {
//       validations = handleConditionalValidations(
//         this.state.eduState.userAcademicInfo.stream,
//         753,
//         "notEqual",
//         ["otherStream"],
//         validations
//       );
//     }

//     // if (
//     //   this.state.eduState.userAcademicInfo.stream == 945 &&
//     //   this.state.classID == graduationId
//     // ) {
//     //   validations = handleConditionalValidations(
//     //     this.state.eduState.userAcademicInfo.stream,
//     //     945,
//     //     "notEqual",
//     //     ["otherStream"],
//     //     validations
//     //   );
//     // }

//     if (
//       this.state.eduState.userAcademicInfo.stream != otherStreamIdEleven &&
//       this.state.classID == elvenId
//     ) {
//       validations = handleConditionalValidations(
//         this.state.eduState.userAcademicInfo.stream,
//         otherStreamIdEleven,
//         "notEqual",
//         ["otherStream"],
//         validations
//       );
//     }

//     if (
//       this.state.eduState.userAcademicInfo.board != 31 &&
//       this.state.classID == twelthId
//     ) {
//       validations = handleConditionalValidations(
//         this.state.eduState.userAcademicInfo.board,
//         31,
//         "notEqual",
//         ["otherBoard"],
//         validations
//       );
//     }

//     if (
//       this.state.eduState.userAcademicInfo.board != 31 &&
//       this.state.classID == twelthPassId
//     ) {
//       validations = handleConditionalValidations(
//         this.state.eduState.userAcademicInfo.board,
//         31,
//         "notEqual",
//         ["otherBoard"],
//         validations
//       );
//     }

//     if (
//       this.state.eduState.userAcademicInfo.board != 31 &&
//       this.state.classID == classTenthId
//     ) {
//       validations = handleConditionalValidations(
//         this.state.eduState.userAcademicInfo.board,
//         31,
//         "notEqual",
//         ["otherBoard"],
//         validations
//       );
//     }

//     if (this.state.eduState.userInstituteInfo.state != otherStateId) {
//       validations = handleConditionalValidations(
//         this.state.eduState.userInstituteInfo.state,
//         otherStateId,
//         "notEqual",
//         ["otherState", "otherDistrict"],
//         validations
//       );
//     }

//     if (
//       this.state.eduState.userAcademicInfo.board != 31 &&
//       this.state.classID == twelthPassId
//     ) {
//       validations = handleConditionalValidations(
//         this.state.eduState.userAcademicInfo.board,
//         31,
//         "notEqual",
//         ["otherBoard"],
//         validations
//       );
//     }

//     if (
//       this.state.eduState.userAcademicInfo.stream != 753 &&
//       this.state.classID == twelthPassId
//     ) {
//       validations = handleConditionalValidations(
//         this.state.eduState.userAcademicInfo.stream,
//         753,
//         "notEqual",
//         ["otherStream"],
//         validations
//       );
//     }

//     // if (this.state.eduState.userAcademicInfo.stream == 244) {
//     //   validations = handleConditionalValidations(
//     //     this.state.eduState.userAcademicInfo.stream,
//     //     244,
//     //     null,
//     //     ["mathMarksObtained", "mathTotalMarks"],
//     //     validations
//     //   );
//     // }
//     // if (this.state.eduState.userAcademicInfo.stream == 248) {
//     //   validations = handleConditionalValidations(
//     //     this.state.eduState.userAcademicInfo.stream,
//     //     244,
//     //     null,
//     //     ["biologyMarksObtained", "biologyTotalMarks"],
//     //     validations
//     //   );
//     // }
//     // if (this.state.eduState.userAcademicInfo.stream == 753) {
//     //   validations = handleConditionalValidations(
//     //     this.state.eduState.userAcademicInfo.stream,
//     //     753,
//     //     null,
//     //     [
//     //       "biologyMarksObtained",
//     //       "biologyTotalMarks",
//     //       "mathMarksObtained",
//     //       "mathTotalMarks",
//     //       "chemistryTotalMarks",
//     //       "chemistryMarksObtained",
//     //       "physicsTotalMarks",
//     //       "physicsMarksObtained"
//     //     ],
//     //     validations
//     //   );
//     // }

//     validations = streamRelatedValidation(
//       this.state.eduState.userAcademicInfo.stream,
//       validations
//     );
//     /************* end Update conditional validations.........****/

//     let isFormValid = true;
//     for (let key in validations) {
//       let checkKey = key === "cgpa" ? "marksObtained" : key;
//       if (validations[key] !== undefined) {
//         let { name, validationFunctions } = this.getValidationRulesObject(key);
//         if (this.state.eduState["userAcademicInfo"]["markingType"] === "1") {
//           validationFunctions = validationFunctions.filter(
//             list => list.name !== "isCGPA"
//           );
//         }
//         let validationResult = ruleRunner(
//           this.state.eduState["userAcademicInfo"][checkKey] ||
//             this.state.eduState["userInstituteInfo"][checkKey] ||
//             this.state.eduState["customData"][checkKey],
//           key,
//           name,
//           ...validationFunctions
//         );

//         validations[key] = validationResult[key];
//         if (validationResult[key] !== null) {
//           isFormValid = false;
//         }
//       }
//     }
//     this.setState({
//       validations,
//       isFormValid
//     });
//     return isFormValid;
//   }

//   checkStreamBasedMarksValue() {
//     const updateCustomData = { ...this.state.eduState };

//     const { userAcademicInfo } = updateCustomData;

//     if (userAcademicInfo && userAcademicInfo.stream) {
//       const { stream } = userAcademicInfo;
//       switch (parseInt(stream)) {
//         case 244:
//           updateCustomData["customData"]["mathMarksObtained"] = "";
//           updateCustomData["customData"]["mathTotalMarks"] = "";
//           break;
//         case 248:
//           updateCustomData["customData"]["biologyMarksObtained"] = "";
//           updateCustomData["customData"]["biologyTotalMarks"] = "";
//           break;
//         case 245:
//           return updateCustomData["customData"];

//         default:
//           updateCustomData["customData"]["biologyMarksObtained"] = "";
//           updateCustomData["customData"]["biologyTotalMarks"] = "";
//           updateCustomData["customData"]["mathMarksObtained"] = "";
//           updateCustomData["customData"]["mathTotalMarks"] = "";
//           updateCustomData["customData"]["chemistryMarksObtained"] = "";
//           updateCustomData["customData"]["chemistryTotalMarks"] = "";
//           updateCustomData["customData"]["physicsMarksObtained"] = "";
//           updateCustomData["customData"]["physicsTotalMarks"] = "";
//       }
//     }
//     return updateCustomData["customData"];
//   }

//   onEducationSubmitHandler(e) {
//     e.preventDefault();
//     let isSubmit = this.checkFormValidations();
//     if (isSubmit) {
//       // for (let key in this.state.configFieldsData) {
//       //   if (this.state.configFieldsData[key].custom) {
//       //     this.state.eduState.customData[key] = this.state.eduState[key];
//       //   }
//       // }
//       const updatedEducation = { ...this.state.eduState };
//       if (
//         updatedEducation &&
//         updatedEducation.userAcademicInfo &&
//         updatedEducation.userAcademicInfo.academicClass
//       ) {
//         if (
//           updatedEducation.userAcademicInfo.academicClass.id ==
//           this.state.presentClassID
//         ) {
//           updatedEducation.userAcademicInfo["presentClass"] = 1;
//         } else {
//           updatedEducation.userAcademicInfo["presentClass"] = 0;
//         }
//       }

//       updatedEducation.customData = this.checkStreamBasedMarksValue();

//       this.setState(
//         {
//           onSelectClassMode: false,
//           isEditable: true
//         },
//         () =>
//           this.props.saveEducationInfo({
//             scholarshipId:
//               typeof window !== "undefined"
//                 ? localStorage.getItem("applicationSchID")
//                 : "",
//             education: updatedEducation
//           })
//       );
//     }
//   }
//   close() {
//     this.setState({
//       showLoader: false,
//       msg: "",
//       status: false
//     });
//   }
//   ifClassDetailsAreCompleted() {
//     const classesToCheck = []; //10th and graduation
//     const usedClasses = (this.state.applicationEducationData
//       ? this.state.applicationEducationData.userEducationList &&
//         Array.isArray(this.state.applicationEducationData.userEducationList)
//         ? this.state.applicationEducationData.userEducationList.filter(
//             item =>
//               classesToCheck.indexOf(
//                 item.userAcademicInfo
//                   ? item.userAcademicInfo.academicClass
//                     ? item.userAcademicInfo.academicClass.id
//                     : -1
//                   : -1
//               ) > -1
//           )
//         : []
//       : []
//     ).filter(
//       item =>
//         !item.userAcademicInfo.marksObtained && !item.userAcademicInfo.grade
//     );
//     return usedClasses.length;
//   }
//   goToNextStep() {
//     if (this.ifAllClassesFilled()) {
//       if (this.ifClassDetailsAreCompleted()) {
//         //Classes details are incomplete....
//         this.setState({
//           status: false,
//           msg: this.props.genericEduTitle ? this.props.genericEduTitle : "",
//           showLoader: true
//         });
//       } else {
//         this.setState(
//           {
//             stepUpdated: true,
//             status: false,
//             msg: "",
//             showLoader: false
//           },
//           () => {
//             this.props.updateStep({
//               scholarshipId: gblFunc.getStoreApplicationScholarshipId(),
//               step: "EDUCATION_INFO",
//               userId: gblFunc.getStoreUserDetails()["userId"]
//             });
//           }
//         );
//       }
//     }
//   }

//   markup(val) {
//     return { __html: val };
//   }
//   hideConfirmationPopup() {
//     this.setState({
//       showConfirmationPopup: false,
//       presentClassID: "",
//       classID: "",
//       classConfigList: [],
//       isEditable: false
//     });
//   }

//   render() {
//     const {
//       applicationEducationData,
//       classConfigList,
//       classID,
//       isEditable
//     } = this.state;

//     const {
//       allRules,
//       applicationStepInstruction,
//       educationConfigInfo,
//       scholarshipSteps,
//       match
//     } = this.props;
//     const year = getYearOrMonth("year", [1950, 2024]);
//     const month = getYearOrMonth("month", []);

//     let academicClassOptions = null,
//       eduInstructionTemplate = "";

//     if (
//       educationConfigInfo &&
//       educationConfigInfo.academic_class &&
//       educationConfigInfo.academic_class.dataOptions &&
//       educationConfigInfo.academic_class.dataOptions.length > 0
//     ) {
//       academicClassOptions = educationConfigInfo.academic_class.dataOptions;
//     }

//     if (
//       applicationEducationData &&
//       applicationEducationData.userEducationList &&
//       applicationEducationData.userEducationList.length > 0 &&
//       classConfigList &&
//       classConfigList.length
//     ) {
//       let classesId = applicationEducationData.userEducationList.map(
//         list => list.userAcademicInfo.academicClass.id
//       );

//       classesId
//         ? classConfigList.map(acdClss => {
//             if (
//               classesId &&
//               classesId.length > 0 &&
//               classesId.indexOf(parseInt(acdClss.classId)) > -1
//             ) {
//               acdClss.disabled = true;
//               return acdClss;
//             } else {
//               acdClss.disabled = false;
//               return acdClss;
//             }
//           })
//         : classConfigList.map(acdClss => {
//             acdClss.disabled = false;
//             return acdClss;
//           });
//     } else {
//       classConfigList.map(acdClss => {
//         acdClss.disabled = false;
//         return acdClss;
//       });
//     }

//     if (scholarshipSteps && scholarshipSteps.length) {
//       eduInstructionTemplate = extractStepTemplate(
//         scholarshipSteps,
//         "EDUCATION_INFO"
//       );
//     }

//     return (
//       <section className="sectionwhite">
//         <Loader isLoader={this.props.showLoader} />
//         <AlertMessage
//           close={this.close.bind(this)}
//           isShow={this.state.showLoader}
//           status={this.state.status}
//           msg={this.state.msg}
//         />
//         <ConfirmMessagePopup
//           message={this.state.msg}
//           showPopup={this.state.showConfirmationPopup}
//           onConfirmationSuccess={this.onDeleteEducationListHandler}
//           onConfirmationFailure={this.hideConfirmationPopup}
//         />
//         {academicClassOptions && academicClassOptions.length > 0 ? (
//           <article>
//             <article className="row">
//               <article className="col-md-4">
//                 <select
//                   className="icon"
//                   value={this.state.presentClassID}
//                   onChange={event => this.onSelectPresentClassHandler(event)}
//                 >
//                   <option value="">--Select Present Class--</option>
//                   {academicClassOptions.map(cls => (
//                     <option key={cls.id} value={cls.id}>
//                       {cls.rulevalue}
//                     </option>
//                   ))}
//                 </select>
//               </article>
//             </article>
//           </article>
//         ) : null}
//         <article className="form">
//           <article className="row ">
//             <article
//               dangerouslySetInnerHTML={this.markup(
//                 eduInstructionTemplate.length &&
//                   eduInstructionTemplate[0] &&
//                   eduInstructionTemplate[0].message
//                   ? eduInstructionTemplate[0].message
//                   : this.props.genericEduTitle
//               )}
//               className="col-md-12 subheadingerror"
//             />
//             {applicationEducationData &&
//             this.state.presentClassID &&
//             Object.keys(applicationEducationData).length > 0 &&
//             applicationEducationData.userEducationList &&
//             applicationEducationData.userEducationList.length > 0 ? (
//               <article className="col-md-12 table-responsive">
//                 <table className="table table-striped">
//                   <thead>
//                     {match.params && childEduBSID[match.params.bsid] ? (
//                       <tr>
//                         <th>Class</th>
//                         <th>Institute/School/College</th>
//                         <th>Annual Fee</th>
//                         <th>State</th>
//                         <th>Action</th>
//                       </tr>
//                     ) : (
//                       <tr>
//                         <th>Class</th>
//                         <th>Institute/School/College</th>
//                         <th>Percentage</th>
//                         <th>Grade</th>
//                         <th>Action</th>
//                       </tr>
//                     )}
//                   </thead>
//                   <tbody>
//                     <EducationLists
//                       editEduction={this.editEduction}
//                       applicationEducationData={applicationEducationData}
//                       match={match}
//                     />
//                   </tbody>
//                 </table>
//               </article>
//             ) : null}

//             {classConfigList && classConfigList.length > 0 ? (
//               <article>
//                 <article className="row">
//                   <article className="col-md-4">
//                     <select
//                       className="icon"
//                       disabled={isEditable}
//                       value={classID}
//                       onChange={event => this.onSelectClassHandler(event)}
//                     >
//                       <option value="">--Select Class--</option>
//                       {classConfigList.map(cls => (
//                         <option
//                           disabled={cls.disabled}
//                           key={cls.classId}
//                           value={cls.classId}
//                         >
//                           {cls.className}
//                         </option>
//                       ))}
//                     </select>
//                   </article>
//                 </article>
//               </article>
//             ) : null}
//             {this.state.classID ? (
//               <AcadmicClasses
//                 allRules={allRules}
//                 classID={this.state.classID}
//                 presentClassID={this.state.presentClassID}
//                 boardList={this.props.boardList}
//                 district={this.props.district}
//                 subject={this.props.subject}
//                 isEditable={this.state.isEditable}
//                 degree={this.props.filterRules}
//                 year={year}
//                 month={month}
//                 marksOrCgpa={this.state.marksOrCgpa}
//                 {...this.state.eduState}
//                 onDistrictHandler={this.getDistrictHandler.bind(this)}
//                 educationFormHandler={this.educationFormHandler}
//                 marksObtainedHandler={this.marksObtainedHandler}
//                 onEducationSubmitHandler={this.onEducationSubmitHandler}
//                 {...applicationEducationData}
//                 educationDataApi={this.state.classConfigFields}
//                 applicationStep={applicationStepInstruction}
//                 validations={this.state.validations}
//                 eduState={this.state.eduState}
//               />
//             ) : null}
//           </article>
//         </article>

//         <article className="border">&nbsp;</article>

//         {(this.state.isStepCompleted || this.ifAllClassesFilled()) &&
//         !this.state.isEditable ? (
//           <article className="row">
//             <article className="col-md-12">
//               <input
//                 type="button"
//                 value="Save & Continue"
//                 className="btn  pull-right"
//                 onClick={this.goToNextStep}
//               />
//             </article>
//           </article>
//         ) : null}
//       </section>
//     );
//   }
// }

// const EducationLists = ({ applicationEducationData, editEduction, match }) => {
//   let eduList = null;
//   if (
//     applicationEducationData &&
//     Object.keys(applicationEducationData).length > 0 &&
//     applicationEducationData.userEducationList &&
//     applicationEducationData.userEducationList.length > 0
//   ) {
//     const eduLists = applicationEducationData.userEducationList;
//     eduList = eduLists.map((list, index) =>
//       match.params && childEduBSID[match.params.bsid] ? (
//         <tr key={index}>
//           <td>{list.userAcademicInfo.academicClass.value}</td>
//           <td>
//             {list.userInstituteInfo.instituteName ? (
//               list.userInstituteInfo.instituteName
//             ) : (
//               <span>&mdash;</span>
//             )}
//           </td>
//           <td>
//             {list.userAcademicInfo.tuitionFee ? (
//               list.userAcademicInfo.tuitionFee
//             ) : (
//               <span>&mdash;</span>
//             )}
//           </td>

//           <td>
//             {list.userInstituteInfo.stateName ? (
//               list.userInstituteInfo.stateName
//             ) : (
//               <span>&mdash;</span>
//             )}
//           </td>

//           <td>
//             {" "}
//             <i
//               onClick={() => editEduction(list, true)}
//               className="fa fa-edit iconedit"
//             >
//               {" "}
//               &nbsp;
//             </i>
//           </td>
//         </tr>
//       ) : (
//         <tr key={index}>
//           <td>{list.userAcademicInfo.academicClass.value}</td>
//           <td>
//             {list.userInstituteInfo.instituteName ? (
//               list.userInstituteInfo.instituteName
//             ) : (
//               <span>&mdash;</span>
//             )}
//           </td>
//           <td>
//             {list.userAcademicInfo.percentage ? (
//               list.userAcademicInfo.percentage.toFixed(2) + " %"
//             ) : (
//               <span>&mdash;</span>
//             )}
//           </td>

//           <td>
//             {list.userAcademicInfo.grade ? (
//               list.userAcademicInfo.grade
//             ) : (
//               <span>&mdash;</span>
//             )}
//           </td>

//           <td>
//             {" "}
//             <i
//               onClick={() => editEduction(list, true)}
//               className="fa fa-edit iconedit"
//             >
//               {" "}
//               &nbsp;
//             </i>
//           </td>
//         </tr>
//       )
//     );
//   }
//   return eduList;
// };

const mapStateToProps = ({
  applicationEducation,
  applicationForm,
  applicationPersonalInfo,
  common
}) => ({
  ...applicationEducation,
  educationConfigInfo: applicationEducation.educationConfig,
  allRules: applicationForm,
  district: applicationEducation.district,
  type: applicationEducation.type,
  showLoader: applicationEducation.showLoader,
  classConfig: applicationEducation.classConfig,
  personalInfoData: applicationPersonalInfo.personalInfoData,
  filterRules: common.filterRules,
  updatedStep: applicationEducation.updateStep
});

const mapDispatchToProps = dispatch => ({
  fetchEduInfo: inputData => dispatch(fetchEducationInfoAction(inputData)),
  getSubject: inputData => dispatch(getSubjectInfoAction(inputData)),
  fetchDistrictList: inputData => dispatch(getDistrictInfoAction(inputData)),
  saveEducationInfo: inputData => dispatch(saveEducationInfoAction(inputData)),
  getEducationConfig: inputData =>
    dispatch(getEducationConfigAction(inputData)),
  updateStep: inputData => dispatch(updateStepAction(inputData)),
  getCompletedSteps: inputData =>
    dispatch(applicationInstructionStepAction(inputData)),
  getConfigOfClss: inputData => dispatch(getClassConfigAction(inputData)),
  deleteAcademic: inputData => dispatch(deleteAcademicListAction(inputData)),
  fetchApplicationPersonalInfo: inputData =>
    dispatch(fetchUserApplicationPersonalInfoAction(inputData)),
  fetchFilterRuleById: inputData => dispatch(fetchFilterRuleAction(inputData))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EducationInfo);
