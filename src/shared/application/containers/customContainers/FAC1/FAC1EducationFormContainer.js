import { connect } from "react-redux";
import educationInfo from "../../../components/customForms/FAC1/FAC1EducationInfo";
import {
  getEducationInfo as fetchEducationInfoAction,
  getDistrictInfo as getDistrictInfoAction,
  getSubjectInfo as getSubjectInfoAction,
  saveEducationData as saveEducationInfoAction,
  getEduConfig as getEducationConfigAction,
  updateStep as updateStepAction
} from "../../../actions/educationInfoAction";
import { applicationInstructionStep as applicationInstructionStepAction } from "../../../actions/applicationFormAction";

const mapStateToProps = ({ applicationEducation, applicationForm }) => ({
  ...applicationEducation,
  educationConfigInfo: applicationEducation.educationConfig,
  allRules: applicationForm,
  type: applicationEducation.type,
  showLoader: applicationEducation.showLoader
});

const mapDispatchToProps = dispatch => ({
  fetchEduInfo: inputData => dispatch(fetchEducationInfoAction(inputData)),
  getSubject: inputData => dispatch(getSubjectInfoAction(inputData)),
  fetchDistrictList: inputData => dispatch(getDistrictInfoAction(inputData)),
  saveEducationInfo: inputData => dispatch(saveEducationInfoAction(inputData)),
  getEducationConfig: inputData =>
    dispatch(getEducationConfigAction(inputData)),
  updateStep: inputData => dispatch(updateStepAction(inputData)),
  getCompletedSteps: inputData =>
    dispatch(applicationInstructionStepAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(educationInfo);
