import { connect } from "react-redux";
import EntranceExam from "../../../components/customForms/FAC1/FAC1EntranceExam";
import {
  saveExamData as saveExamDataAction,
  getExamData as getExamDataAction,
  deleteExamData as deleteExamDataAction
} from "../../../actions/applicationEntranceExamAction";

const mapStateToProps = ({ applicationEntranceExam }) => ({
  applicationExam: applicationEntranceExam,
  showLoader: applicationEntranceExam.showLoader
});

const mapDispatchToProps = dispatch => ({
  getExamData: inputData => dispatch(getExamDataAction(inputData)),
  saveExamData: inputData => dispatch(saveExamDataAction(inputData))
});

/* const mapStateToProps = ({ applicationAwardWon }) => ({
  applicationAwardWon: applicationAwardWon,
  showLoader: applicationAwardWon.showLoader
});

const mapDispatchToProps = dispatch => ({
  saveAwardWonData: inputData => dispatch(saveAwardWonDataAction(inputData)),
  getAwardWonData: inputData => dispatch(getAwardWonDataAction(inputData)),
  deleteAwardWonData: inputData => dispatch(deleteAwardWonDataAction(inputData))
});
 */
export default connect(mapStateToProps, mapDispatchToProps)(EntranceExam);
