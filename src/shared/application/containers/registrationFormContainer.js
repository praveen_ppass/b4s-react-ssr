import { connect } from "react-redux";

import RegistrationForm from "../components/registrationForm";
import { registerUser as registerUsersAction } from "../../login/actions";

const mapStateToProps = ({ loginOrRegister }) => ({
  type: loginOrRegister.type,
  serverErrorRegister: loginOrRegister.serverErrorRegister,
  serverErrorLogin: loginOrRegister.serverErrorLogin
});

const mapDispatchToProps = dispatch => ({
  registerUsers: inputData => dispatch(registerUsersAction(inputData))
});

export const RegistrationFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationForm);
