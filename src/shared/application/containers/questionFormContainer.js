import { connect } from "react-redux";

import {
  saveQuestionData as saveQuestionDataAction,
  getQuestionData as getQuestionDataAction
} from "../actions/questionAction";
import questions from "../components/standardForms/questions";
import { applicationInstructionStep as applicationInstructionStepAction } from "../actions/applicationFormAction";

const mapStateToProps = ({ applicationQuestion }) => ({
  QuestionDataList: applicationQuestion
});

const mapDispatchToProps = dispatch => ({
  saveQuestionData: inputData => dispatch(saveQuestionDataAction(inputData)),
  getQuestionData: inputData => dispatch(getQuestionDataAction(inputData)),
  applicationInstructionStep: inputData =>
    dispatch(applicationInstructionStepAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(questions);
