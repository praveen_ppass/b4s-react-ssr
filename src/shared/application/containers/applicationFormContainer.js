import { connect } from "react-redux";
import applicationForm from "../components/applicationForm";
import {
  getAllRules as getAllRulesAction,
  applicationInstruction as getApplicationInstructionsAction,
  applicationInstructionStep as applicationInstructionStepAction,
  scholarshipApplicableStep as scholarshipApplicableStepAction
} from "../actions/applicationFormAction";

import { fetchSchApply as fetchSchApplyAction } from "../../../constants/commonActions";

const mapStateToProps = ({ applicationForm, loginOrRegister }) => ({
  allRules: applicationForm,
  type: applicationForm.type,
  showLoader: applicationForm.showLoader,
  instructions: applicationForm.instructions,
  applicationStepInstruction: applicationForm.instructionsStep,
  scholarshipSteps: applicationForm.schApplicableStep,
  schApply: applicationForm.schApply,
  isAuthenticated: loginOrRegister.isAuthenticated,
  redirectionSteps: applicationForm.redirectionSteps
});

const mapDispatchToProps = dispatch => ({
  getAllRules: inputData => dispatch(getAllRulesAction(inputData)),
  loadScholarshipDetail: slug =>
    dispatch(getApplicationInstructionsAction(slug)),
  getSchApply: inputData => dispatch(fetchSchApplyAction(inputData)),
  getApplicationInstructionStep: inputData =>
    dispatch(applicationInstructionStepAction(inputData)),
  fetchScholarshipApplicableStep: scholarshipId =>
    dispatch(scholarshipApplicableStepAction(scholarshipId))
});

export default connect(mapStateToProps, mapDispatchToProps)(applicationForm);
