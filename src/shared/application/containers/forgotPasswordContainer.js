import { connect } from "react-redux";

import ForgotPasswordForm from "../components/forgotPasswordForm";
import { forgotUserPassword as forgotUserPasswordAction } from "../../login/actions";

const mapStateToProps = ({ bookSlot }) => ({
  showLoader: bookSlot.showLoader,
  serverErrorForgotPassword: bookSlot.serverErrorForgotPassword
});

const mapDispatchToProps = dispatch => ({
  forgotUserPassword: inputData => dispatch(forgotUserPasswordAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordForm);
