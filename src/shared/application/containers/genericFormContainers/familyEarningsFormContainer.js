import { connect } from "react-redux";

import {
  saveFamilyEarningsData as saveFamilyEarningDataAction,
  getFamilyEarningsData as getFamilyEarningDataAction,
  deleteFamilyEarningsData as deleteFamilyEarningsDataAction
} from "../../actions/familyEarningsActions";
import { updateStep as updateStepAction } from "../../actions/educationInfoAction";
import { applicationInstructionStep as applicationInstructionStepAction } from "../../actions/applicationFormAction";

import familyEarnings from "../../components/standardForms/familyEarnings";

const mapStateToProps = ({ applicationFamilyEarnings,applicationEducation }) => ({
  familyEarningsDataConfig: applicationFamilyEarnings,
  showLoader: applicationFamilyEarnings.showLoader,
  applicationEducation:applicationEducation
});

const mapDispatchToProps = dispatch => ({
  saveFamilyEarningData: inputData =>
    dispatch(saveFamilyEarningDataAction(inputData)),
  getFamilyEarningData: inputData =>
    dispatch(getFamilyEarningDataAction(inputData)),
  deleteFamilyEarningsData: inputData =>
    dispatch(deleteFamilyEarningsDataAction(inputData)),
  applicationInstructionStep: inputData =>
    dispatch(applicationInstructionStepAction(inputData)),
  updateStep: inputData => dispatch(updateStepAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(familyEarnings);
