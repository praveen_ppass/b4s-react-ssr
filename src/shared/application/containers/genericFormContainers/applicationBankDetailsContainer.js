import { connect } from "react-redux";
import bankDetails from "../../components/standardForms/bankDetails";
import {
  getBankDts as getBankDtsAction,
  saveBankDts as saveBankDtsAction,
  bankDtsConfig as bankDtsConfigAction,
  updateBankStep as updateBankStepAction
} from "../../actions/applicationBankDetailsAction";
import {
  fetchBankDetail as fetchBankDetailAction,
} from "../../../scholar/scholarAction";

import { applicationInstructionStep as applicationInstructionStepAction } from "../../actions/applicationFormAction";

const mapStateToProps = ({ applicationBankDts,scholarReducers }) => ({
  bankdetails: applicationBankDts.bankDetails,
  updateBankDetails: applicationBankDts.updateBankDetails,
  showLoader: applicationBankDts.showLoader,
  bankDetailsConfig: applicationBankDts.bankDtsConfig,
  type: applicationBankDts.type,
  type1:scholarReducers.type,
  bankIfscData:scholarReducers.bankIfscData
});
const mapDispatchToProps = dispatch => ({
  getBankDetails: input => dispatch(getBankDtsAction(input)),
  saveBankDetails: inputData => dispatch(saveBankDtsAction(inputData)),
  bankDtsConfig: input => dispatch(bankDtsConfigAction(input)),
  updateBankDtStep: inputData => dispatch(updateBankStepAction(inputData)),
  applicationInstructionStep: inputData =>
    dispatch(applicationInstructionStepAction(inputData)),
    fetchBankDetail: data => dispatch(fetchBankDetailAction(data)),

});

export default connect(mapStateToProps, mapDispatchToProps)(bankDetails);
