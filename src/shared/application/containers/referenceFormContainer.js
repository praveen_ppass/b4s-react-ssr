import { connect } from "react-redux";

import {
  saveReferenceData as saveReferenceDataAction,
  getReferenceData as getReferenceDataAction,
  deleteReferenceData as deleteReferenceDataAction,
  updateStepReference as updateReferenceStepAction
} from "../actions/referenceAction";
import reference from "../components/standardForms/reference";
import { applicationInstructionStep as applicationInstructionStepAction } from "../actions/applicationFormAction";

const mapStateToProps = ({ applicationReference }) => ({
  referenceDataConfig: applicationReference,
  showLoader: applicationReference.showLoader,
  instructionsStep: applicationReference.instructionsStep
});

const mapDispatchToProps = dispatch => ({
  saveReferenceData: inputData => dispatch(saveReferenceDataAction(inputData)),
  getReferenceData: inputData => dispatch(getReferenceDataAction(inputData)),
  deleteReferenceData: inputData =>
    dispatch(deleteReferenceDataAction(inputData)),
  applicationInstructionStep: inputData =>
    dispatch(applicationInstructionStepAction(inputData)),
  updateReferenceStep: inputData =>
    dispatch(updateReferenceStepAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(reference);
