import { connect } from "react-redux";

import applicationInstructions from "../components/applicationInstructions/applicationInstructions";
import {
  getApplicationInstructions as getApplicationInstructionsAction,
  getScholarshipCategory as getScholarshipCategoryAction,
  saveScholarshipType as saveScholarshipTypeAction,
  checkDisbursalStatus as checkDisbursalStatusAction
} from "../actions/applicationInstructionActions";
import {
  fetchUserRules as fetchUserRulesAction,
  fetchSchApply as fetchSchApplyAction
} from "../../../constants/commonActions";
import { applicationStatus as applicationStatusAction } from "../../dashboard/actions";
import { logUserOut as logUserOutAction } from "../../login/actions";

const mapStateToProps = ({
  loginOrRegister: {
    userData,
    type,
    userLoginData,
    serverErrorLogin,
    serverErrorRegister,
    serverErrorForgotPassword,
    uploadPicData,
    tokenRefreshed,
    showLoader
  },
  mediaUrl,
  appInstr
}) => ({
  // mediaUrl: mediaUrl.mediaData,
  // mediaSubmit: mediaUrl.mediaSubmit,
  schType: appInstr.schType,
  showLoader: appInstr.showLoader,
  type: appInstr.type,
  slug: appInstr.slug,
  schDtdata: appInstr.applicationInstruction,
  userRulesData: appInstr.userRulesData,
  schApply: appInstr.schApply,
  userLoginData,
  scholarshipCate: appInstr.scholarshipCategory,
  disbursalStatus: appInstr.disbursalStatus
  // serverError: mediaUrl.serverError,
  // isServerError: mediaUrl.isServerError
});

const mapDispatchToProps = dispatch => ({
  loadScholarshipDetail: slug =>
    dispatch(getApplicationInstructionsAction(slug)),
  getSchApply: inputData => dispatch(fetchSchApplyAction(inputData)),
  scholarshipCategory: schid => dispatch(getScholarshipCategoryAction(schid)),
  saveSchType: inputData => dispatch(saveScholarshipTypeAction(inputData)),
  fetchUserRules: inputData => dispatch(fetchUserRulesAction(inputData)),
  fetchApplicationStatus: inputData =>
    dispatch(applicationStatusAction(inputData)),
  logUserOut: inputData => dispatch(logUserOutAction(inputData)),
  checkDisbursal: inputData => dispatch(checkDisbursalStatusAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  applicationInstructions
);
