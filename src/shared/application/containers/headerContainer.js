import { connect } from "react-redux";

import header from "../components/header";
import {
  logUserOut as logUserOutAction,
  updateUserPassword as updateUserPasswordAction,
  openLoginPagePopup as openLoginPagePopupAction
} from "../../login/actions";

const mapStateToProps = ({
  loginOrRegister: {
    isAuthenticated,
    type,
    uploadPicData,
    userRulesData,
    updateUserInfo,
    showLoader,
    passwordUpdateError
  }
}) => ({
  isAuthenticated,
  type,
  uploadPicData,
  showLoader,
  userRulesData,
  updateUserInfo,
  passwordUpdateError
});

const mapDispatchToProps = dispatch => ({
  logUserOut: inputData => dispatch(logUserOutAction(inputData)),
  updateUserPassword: inputData =>
    dispatch(updateUserPasswordAction(inputData)),
  openLoginPagePopup: () => dispatch(openLoginPagePopupAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(header);
