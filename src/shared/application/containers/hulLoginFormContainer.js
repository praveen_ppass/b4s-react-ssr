import { connect } from "react-redux";

import HulLoginForm from "../components/auth/login/hulLoginForm";
import { loginUser as loginUserAction } from "../../login/actions";

import { fetchUserCode as fetchUserCodeAction } from "../actions/hulLoginFormAction";

const mapStateToProps = ({ hulLogin, bookSlot }) => ({
  showLoader: hulLogin.showLoader,
  type: hulLogin.type,
  loginType: bookSlot.type,
  serverUserCodeErrors: hulLogin.serverUserCodeErrors,
  serverErrorLogin: bookSlot.serverErrorLogin,
  userCode: hulLogin.userCode
});

const mapDispatchToProps = dispatch => ({
  loginUser: inputData => dispatch(loginUserAction(inputData)),
  validateUserCode: inputData => dispatch(fetchUserCodeAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(HulLoginForm);
