import { connect } from "react-redux";

import LoginSocialForm from "../components/loginSocialForm";
import RegistrationSocialForm from "../components/registerSocialForm";
import { socialLoginUser as socialLoginUserAction } from "../../login/actions";

const mapStateToProps = ({ bookSlot }) => ({});

const mapDispatchToProps = dispatch => ({
  socialLogin: inputData => dispatch(socialLoginUserAction(inputData))
});

export const LoginSocialFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginSocialForm);

export const RegisterSocialFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationSocialForm);
