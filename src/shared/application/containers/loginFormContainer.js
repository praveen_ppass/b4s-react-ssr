import { connect } from "react-redux";

import LoginForm from "../components/loginForm";
import {
  registerUser as registerUserAction,
  loginUser as loginUserAction,
  socialLogin as socialLoginUserAction,
  forgotUserPassword as forgotUserPasswordAction
} from "../../login/actions";

import { fetchUserRules as fetchUserRulesAction } from "../../../constants/commonActions";

const mapStateToProps = ({ bookSlot }) => ({
  showLoader: bookSlot.showLoader,
  userLoginData: bookSlot.userLoginData,
  userRulesData: bookSlot.userRulesData,
  type: bookSlot.type,
  serverErrorRegister: bookSlot.serverErrorRegister,
  serverErrorLogin: bookSlot.serverErrorLogin
});

const mapDispatchToProps = dispatch => ({
  registerUser: inputData => dispatch(registerUserAction(inputData)),
  socialLogin: inputData => dispatch(socialLoginUserAction(inputData)),
  fetchUserRules: inputData => dispatch(fetchUserRulesAction(inputData)),
  loginUser: inputData => dispatch(loginUserAction(inputData)),
  forgotUserPassword: inputData => dispatch(forgotUserPasswordAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
