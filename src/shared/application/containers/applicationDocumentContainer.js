import { connect } from "react-redux";
import documentNeeded from "../components/standardForms/documentNeeded";
import {
  getAllDocuement as getAllDocuementAction,
  uploadAllDocuement as uploadAllDocuementAction,
  deleteDocuement as deleteDocuementAction,
  completeDocument as completeDocumentAction
} from "../actions/applicationDocumentAction";
import { applicationInstructionStep as applicationInstructionStepAction } from "../actions/applicationFormAction";

const mapStateToProps = ({ applicationDocument }) => ({
  allDocument: applicationDocument,
  showLoader: applicationDocument.showLoader
});

const mapDispatchToProps = dispatch => ({
  getAllDocuement: inputData => dispatch(getAllDocuementAction(inputData)),
  uploadAllDocuement: inputData =>
    dispatch(uploadAllDocuementAction(inputData)),
  deleteDocuement: inputData => dispatch(deleteDocuementAction(inputData)),
  completeDocument: inputData => dispatch(completeDocumentAction(inputData)),
  applicationInstructionStep: inputData =>
    dispatch(applicationInstructionStepAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(documentNeeded);
