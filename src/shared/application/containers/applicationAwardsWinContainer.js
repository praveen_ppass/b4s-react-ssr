import { connect } from "react-redux";
import awardWon from "../components/standardForms/awardWon";
import {
  saveAwardWonData as saveAwardWonDataAction,
  getAwardWonData as getAwardWonDataAction,
  deleteAwardWonData as deleteAwardWonDataAction
} from "../actions/applicationAwardWonAction";

const mapStateToProps = ({ applicationAwardWon }) => ({
  applicationAwardWon: applicationAwardWon,
  showLoader: applicationAwardWon.showLoader
});

const mapDispatchToProps = dispatch => ({
  saveAwardWonData: inputData => dispatch(saveAwardWonDataAction(inputData)),
  getAwardWonData: inputData => dispatch(getAwardWonDataAction(inputData)),
  deleteAwardWonData: inputData => dispatch(deleteAwardWonDataAction(inputData))
});

export default connect(mapStateToProps, mapDispatchToProps)(awardWon);
