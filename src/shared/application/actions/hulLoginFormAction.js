export const FETCH_USER_CODE_REQUEST = "FETCH_USER_CODE_REQUEST";
export const FETCH_USER_CODE_SUCCESS = "FETCH_USER_CODE_SUCCESS";
export const FETCH_USER_CODE_FAIL = "FETCH_USER_CODE_FAIL";

export const fetchUserCode = data => ({
  type: FETCH_USER_CODE_REQUEST,
  payload: data
});
