import { mapValidationFunc } from "../../validation/rules";
import gblFunc from "../../globals/globalFunctions";
var jwtDecode = require("jwt-decode");
export const smsApplicationDetailConfig = {
  stepsToShow: () => {
    const userToken = gblFunc.getAuthToken();
    try {
      const accessTokenDecoded = jwtDecode(userToken);
      return (accessTokenDecoded.authorities || []).indexOf("sms-write") > -1
        ? null
        : ["summary"];
    } catch (error) {
      console.log("Invalid Token.");
      return null;
    }
  },
  isSMSWriteAdmin: () => {
    const userToken = gblFunc.getAuthToken();
    try {
      const accessTokenDecoded = jwtDecode(userToken);
      return (accessTokenDecoded.authorities || []).indexOf("sts-write") > -1 ||
        (accessTokenDecoded.authorities || []).indexOf("sms-write") > -1
        ? true
        : false;
    } catch (error) {
      console.log("Invalid Token.");
      return null;
    }
  },
  isSMSAdmin: () => {
    const userToken = gblFunc.getAuthToken();
    try {
      const accessTokenDecoded = jwtDecode(userToken);
      return (accessTokenDecoded.authorities || []).indexOf("sms-read") > -1 ||
        (accessTokenDecoded.authorities || []).indexOf("sts-read") > -1
        ? true
        : false;
    } catch (error) {
      console.log("Invalid Token.");
      return null;
    }
  }
};
export const config = {
  customInstruction: new Map([
    ["CES7", "Please enter the details of your child in the fields below."]
  ]),
  MIM4: {},

  CSS22: {
    //Need to change
    nextStepName: {
      personalInfo: "educationInfo",
      educationInfo: "documents",
      documents: "questions",
      questions: "summary"
    }
  },
  CSP1: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  FAC1: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "entranceExam",
      educationInfo: "documents",
      familyInfo: "references",
      references: "questions",
      questions: "awardsWon",
      awardsWon: "summary",
      entranceExam: "questions",
      questions: "summary"
    }
  },
  SGA3: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  IS73: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  CIS10: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  LFL2: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  COS1: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  CHG2: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  SDS5: {
    nextStepName: {
      personalInfo: "documents",
      documents: "questions",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  CHO1: {
    nextStepName: {
      personalInfo: "documents",
      documents: "questions",
      questions: "summary"
    }
  },
  SDS6: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  GGI6: {
    nextStepName: {
      personalInfo: "summary"
    }
  },
  HUS2: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "summary"
    }
  },
  CRS11: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  CRS9: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  SMS10: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  SBA1: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  CAL2: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  SCE3: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "bankDetails",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      bankDetails: "summary"
    }
  },
  BKS1: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  },
  UBS9: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "questions",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      questions: "summary"
    }
  },
  CBI2: {
    nextStepName: {
      personalInfo: "educationInfo",
      documents: "references",
      educationInfo: "familyInfo",
      familyInfo: "documents",
      references: "questions",
      questions: "summary"
    }
  }
};
export const formsLocation = {
  MIM2: ["personalInfo"],
  MIM4: ["personalInfo"],
  FAC1: ["personalInfo", "entranceExam", "educationInfo", "summary"],
  FAL9: ["personalInfo", "educationInfo", "documents", "questions", "references", "summary"],
  HCL2: ["personalInfo", "educationInfo", "documents", "questions", "references", "summary"],
  CSP1: ["personalInfo", "educationInfo", "documents", "summary"],
  CCS1: ["personalInfo", "educationInfo", "documents", "summary"],
  SWS3: ["personalInfo", "educationInfo", "documents", "summary"],
  SIHE1: ["personalInfo", "educationInfo", "documents", "summary"],
  CSS22: ["personalInfo", "educationInfo", "summary"],
  SGA3: ["personalInfo", "educationInfo", "documents", "summary"],
  IS73: ["personalInfo", "educationInfo", "documents", "summary"],
  CHO1: ["personalInfo", "documents", "summary"],
  RHS1: ["personalInfo", "educationInfo", "questions", "references", "summary"],
  CIS10: ["personalInfo", "educationInfo", "documents", "summary"],
  LFL2: ["personalInfo", "educationInfo", "documents", "summary"],
  COS1: ["personalInfo", "educationInfo", "documents", "summary"],
  CHG2: ["personalInfo", "educationInfo", "documents", "summary"],
  IFBMS1: ["personalInfo", "educationInfo", "documents", "summary"],
  SDS5: ["personalInfo", "educationInfo", "documents", "summary"],
  TGSP1: ["personalInfo", "educationInfo", "documents", "summary"],
  MIM5: ["personalInfo", "educationInfo", "questions", "documents", "summary"],
  SDS6: ["personalInfo", "educationInfo", "documents", "summary"],
  BCSMC1: ["personalInfo", "educationInfo", "documents", "summary"],
  BCSOC1: ["personalInfo", "educationInfo", "documents", "summary"],
  BCS01: ["personalInfo", "educationInfo", "documents", "summary"],
  RHS1: ["personalInfo", "educationInfo", "documents", "questions", "references", "summary"],
  HUS2: ["personalInfo", "educationInfo", "documents", "summary"],
  GGI6: ["personalInfo", "summary"],
  CRS11: ["personalInfo", "educationInfo", "documents", "questions", "summary"],
  CRS9: ["personalInfo", "educationInfo", "documents", "questions", "summary"],
  SMS10: ["personalInfo", "educationInfo", "documents", "summary"],
  SBA1: ["personalInfo", "educationInfo", "documents", "summary"],
  CBI1: ["personalInfo", "educationInfo", "documents", "summary"],
  CAL2: ["personalInfo", "educationInfo", "documents", "questions", "summary"],
  SCE3: ["personalInfo", "educationInfo", "documents", "questions", "bankDetails", "summary"],
  BKS1: ["personalInfo", "educationInfo", "documents", "summary"],
  CBI2: ["personalInfo", "educationInfo", "documents", "summary"],
  CBS3: ["personalInfo", "educationInfo", "summary"],
  HEC6: ["personalInfo", "educationInfo", "documents", "questions", "summary"],
  TSL1: ["personalInfo", "educationInfo", "documents", "questions", "summary"],
  CES7: ["personalInfo", "educationInfo", "documents", "questions", "bankDetails", "summary"],
  CES8: ["personalInfo", "educationInfo", "documents", "questions", "bankDetails", "summary"],
  CKS1: ["personalInfo", "educationInfo", "documents", "summary"],
  CES3: ["personalInfo", "educationInfo", "documents", "summary"],
  IGA1: ["personalInfo", "educationInfo", "documents", "questions", "summary"],
  GMM2: ["personalInfo", "educationInfo", "documents", "summary"],
  SSE4: ["personalInfo", "educationInfo", "documents", "summary"],
  LIF9: ["personalInfo", "educationInfo", "familyInfo", "documents", "summary"],
  LIF2: ["personalInfo", "educationInfo", "familyInfo", "documents", "references", "summary"],
  HUL1: ["personalInfo", "educationInfo", "documents", "summary"],
  CVS1: ["personalInfo", "educationInfo", "documents", "summary"],
  PMES01: ["personalInfo", "educationInfo", "questions", "summary"],
  DB1: ["personalInfo", "educationInfo", "documents", "questions", "bankDetails", "summary"],
  UBS9: ["personalInfo", "educationInfo", "familyInfo", "documents", "summary"],
  BML1: ["personalInfo", "educationInfo", "documents", "summary"],
  BML3: ["personalInfo", "educationInfo", "documents", "summary"],
  MIC9: ["personalInfo", "educationInfo", "questions", "documents", "summary"],
  FAL10: ["personalInfo", "educationInfo", "documents", "summary"],
  SIMT1: ["personalInfo", "educationInfo", "questions", "documents", "summary"],
  SIMSD1: ["personalInfo", "educationInfo", "questions", "documents", "summary"],
  SKKS1: ["personalInfo", "educationInfo", "questions", "documents", "summary"],
  AFIS1: ["personalInfo", "educationInfo", "questions", "documents", "summary"],
  AWFS2: ["personalInfo", "educationInfo", "questions", "documents", "summary"],
  TILS1: ["personalInfo", "educationInfo", "documents", "summary"],
  TISB1: ["personalInfo", "educationInfo", "documents", "summary"],
  TILE1: ["personalInfo", "educationInfo", "documents", "summary"]
};
export const formNames = {
  personalInfo: "personalInfo",
  documents: "documents",
  educationInfo: "educationInfo",
  familyInfo: "familyInfo",
  references: "references",
  questions: "questions",
  summary: "summary",
  awardsWon: "awardsWon",
  entranceExam: "entranceExam",
  bankDetail: "bankDetail",
  submitbutton: "submitbutton"
};
export const nextStepName = {
  personalInfo: "documents",
  documents: "educationInfo",
  educationInfo: "familyInfo",
  familyInfo: "references",
  references: "questions",
  questions: "awardsWon",
  awardsWon: "summary"
};
export const stepNamesToDisplay = {
  PERSONAL_INFO: "PERSONAL DETAILS",
  EDUCATION_INFO: "EDUCATION DETAILS",
  FAMILY_INFO: "FAMILY MEMBERS",
  DOCUMENTS: "DOCUMENTS",
  REFERENCES: "REFERENCES",
  QUESTIONS: "MORE ABOUT YOURSELF",
  SUMMARY: "SUMMARY",
  ENTRANCE_EXAM: "ENTRANCE EXAM",
  BANK_DETAIL: "BANK DETAIL"
};
export const userAddressFields = [
  "addressLine",
  "city",
  "state",
  "district",
  "pincode",
  "type",
  "otherState",
  "otherDistrict"
];
export const otherDistrictId = 677;
export const otherStateId = 483;
export const otherBoardId = [39, 31];
export const graduationId = 22;
export const postGradId = 23;
export const elvenId = 12;
export const twelthId = 16;
export const twelthPassId = 20;
export const classTenthId = 11;
export const classNineId = 10;
export const otherStreamIdTwelfth = 753;
export const otherStreamIdGraduation = 281;
export const otherStreamIdPostGraduation = 309;
export const otherStreamIdEleven = 753;
export const othersStreamIdDB1Graduation = 944;
export const otherStreams = [753, 281, 309, 944];
export const hasChildCall = new Map([
  ["state", "district"],
  ["caState", "district"]
]);
export const applicationAPIKeys = {
  personalInfo: "PERSONAL_INFO"
};
export const createValidationRule = function (validationsArray) {
  const updatedRulesArray = [];
  if (validationsArray && Array.isArray(validationsArray)) {
    for (let rule of validationsArray) {
      updatedRulesArray.push(mapValidationFunc(rule));
    }
  }
  return updatedRulesArray;
};
export const setValidationsInitialState = function (validationsObj) {
  //input as object...
  const updatedValidations = {};
  if (validationsObj) {
    for (let key in validationsObj) {
      if (
        validationsObj[key].validations != null &&
        validationsObj[key].active
      ) {
        updatedValidations[key] = null;
        updatedValidations[key + "Validation"] = createValidationRule(
          validationsObj[key].validations
        );
      }
    }
  }
  return updatedValidations;
};
export const handleConditionalValidations = function (
  source,
  value,
  condition,
  validations,
  validationsObj
) {
  if (!condition) {
    //If condition is not provided, assume it as equals...
    if (source == value && validations) {
      for (let key of validations) {
        validationsObj[key] = undefined;
      }
    } else {
      for (let key of validations) {
        validationsObj[key] = null;
      }
    }
  } else {
    switch (condition) {
      case "notEqual":
        if (source != value && validations) {
          for (let key of validations) {
            validationsObj[key] = undefined;
          }
        } else {
          for (let key of validations) {
            validationsObj[key] = null;
          }
        }
        break;
    }
  }
  return validationsObj;
};

export const streamRelatedValidation = function (stream, validations) {
  let validationObject;
  switch (stream) {
    case 244:
      validationObject = handleConditionalValidations(
        stream,
        244,
        null,
        ["mathMarksObtained", "mathTotalMarks"],
        validations
      );
      break;

    case 248:
      validationObject = handleConditionalValidations(
        stream,
        244,
        null,
        ["biologyMarksObtained", "biologyTotalMarks"],
        validations
      );
      break;

    case 245:
      validationObject = validations;
      break;

    default:
      validationObject = handleConditionalValidations(
        1,
        1,
        null,
        [
          "mathMarksObtained",
          "mathTotalMarks",
          "biologyMarksObtained",
          "biologyTotalMarks",
          "chemistryMarksObtained",
          "chemistryTotalMarks",
          "physicsMarksObtained",
          "physicsTotalMarks"
        ],
        validations
      );
  }
  return validationObject;
};

export const educationSpecialValidations = [
  "totalMarks",
  "biologyTotalMarks",
  "chemistryTotalMarks",
  "physicsTotalMarks",
  "mathTotalMarks",
];

export const subjectsMarkActive = function (educationDataApi, subject, stream) {
  switch (subject) {

    case "chemistryTotalMarks":
      if (
        educationDataApi &&
        educationDataApi["chemistryTotalMarks"] &&
        educationDataApi["chemistryTotalMarks"].active &&
        (stream == 245 || stream == 244 || stream == 248)
      ) {
        return true;
      }
      return false;

      break;



    case "physicsTotalMarks":
      if (
        educationDataApi &&
        educationDataApi["physicsTotalMarks"] &&
        educationDataApi["physicsTotalMarks"].active &&
        (stream == 245 || stream == 244 || stream == 248)
      ) {
        return true;
      }
      return false;

      break;



    case "mathTotalMarks":
      if (
        educationDataApi &&
        educationDataApi["mathTotalMarks"] &&
        educationDataApi["mathTotalMarks"].active &&
        (stream == 245 || stream == 248)
      ) {
        return true;
      }
      return false;

      break;



    case "biologyTotalMarks":
      if (
        educationDataApi &&
        educationDataApi["biologyTotalMarks"] &&
        educationDataApi["biologyTotalMarks"].active &&
        (stream == 245 || stream == 244)
      ) {
        return true;
      }
      return false;

      break;

    default:
      break;
  }
};

export const addSpecialValidations = function (
  validationArray,
  fieldId,
  extraArgsArray
) {
  if (extraArgsArray[0].userAcademicInfo) {
    switch (fieldId) {
      case "totalMarks":
        validationArray.push({
          args: [
            extraArgsArray[0].userAcademicInfo.marksObtained
              ? extraArgsArray[0].userAcademicInfo.marksObtained
              : 0
          ],
          func: "isGreaterThan"
        });
        break;

      case "biologyTotalMarks":
        validationArray.push({
          args: [
            extraArgsArray[0].customData.biologyMarksObtained
              ? extraArgsArray[0].customData.biologyMarksObtained
              : 0
          ],
          func: "isGreaterThan"
        });
        break;

      case "chemistryTotalMarks":
        validationArray.push({
          args: [
            extraArgsArray[0].customData.chemistryMarksObtained
              ? extraArgsArray[0].customData.chemistryMarksObtained
              : 0
          ],
          func: "isGreaterThan"
        });
        break;

      case "physicsTotalMarks":
        validationArray.push({
          args: [
            extraArgsArray[0].customData.physicsMarksObtained
              ? extraArgsArray[0].customData.physicsMarksObtained
              : 0
          ],
          func: "isGreaterThan"
        });
        break;

      case "mathTotalMarks":
        validationArray.push({
          args: [
            extraArgsArray[0].customData.mathMarksObtained
              ? extraArgsArray[0].customData.mathMarksObtained
              : 0
          ],
          func: "isGreaterThan"
        });
        break;

    }
  } else {
    switch (fieldId) {
      case "biologyMarksObtained":
        validationArray.push({
          args: [
            extraArgsArray[0].customData.biologyTotalMarks
              ? extraArgsArray[0].customData.biologyTotalMarks
              : 0
          ],
          func: "isLessThan"
        });
        break;
      case "chemistryMarksObtained":
        validationArray.push({
          args: [
            extraArgsArray[0].customData.chemistryTotalMarks
              ? extraArgsArray[0].customData.chemistryTotalMarks
              : 0
          ],
          func: "isLessThan"
        });
        break;
      case "physicsMarksObtained":
        validationArray.push({
          args: [
            extraArgsArray[0].customData.physicsTotalMarks
              ? extraArgsArray[0].customData.physicsTotalMarks
              : 0
          ],
          func: "isLessThan"
        });
        break;
      case "mathMarksObtained":
        validationArray.push({
          args: [
            extraArgsArray[0].customData.mathTotalMarks
              ? extraArgsArray[0].customData.mathTotalMarks
              : 0
          ],
          func: "isLessThan"
        });
        break;
      case "marksObtained":
        validationArray.push({
          args: [
            extraArgsArray[0].userAcademicInfo.totalMarks
              ? extraArgsArray[0].userAcademicInfo.totalMarks
              : 0
          ],
          func: "isLessThan"
        });
        break;
      case "totalMarks":
        validationArray.push({
          args: [
            extraArgsArray[0].marksObtained
              ? extraArgsArray[0].marksObtained
              : 0
          ],
          func: "isGreaterThan"
        });
        break;
      case "biologyTotalMarks":
        validationArray.push({
          args: [
            extraArgsArray[0].customData.biologyMarksObtained
              ? extraArgsArray[0].customData.biologyMarksObtained
              : 0
          ],
          func: "isGreaterThan"
        });
        break;
      case "chemistryTotalMarks":
        validationArray.push({
          args: [
            extraArgsArray[0].customData.chemistryMarksObtained
              ? extraArgsArray[0].customData.chemistryMarksObtained
              : 0
          ],
          func: "isGreaterThan"
        });
        break;
      case "physicsTotalMarks":
        validationArray.push({
          args: [
            extraArgsArray[0].customData.physicsMarksObtained
              ? extraArgsArray[0].customData.physicsMarksObtained
              : 0
          ],
          func: "isGreaterThan"
        });
        break;
      case "mathTotalMarks":
        validationArray.push({
          args: [
            extraArgsArray[0].customData.mathMarksObtained
              ? extraArgsArray[0].customData.mathMarksObtained
              : 0
          ],
          func: "isGreaterThan"
        });
        break;
    }
  }

  return validationArray;
};
export const idValueElement = "SELECT";
export const PIUserRules = new Map([
  ["gender", 5],
  ["disabled", 48],
  ["preferredLanguage", 6],
  ["quota", 6],
  ["religion", 4],
  ["otherPersonalDetails", 47]
]);

export const contentOfNeedSchoalrship = {
  CSS22: "I have completed: "
  // FAL9: "Currently I'm in: "
};
export const needHelpConfig = {
  FAL9: {
    name: "fal_nepal@buddy4study.com",
    mobile: "+977 9826768747"
  },
  CSP1: {
    name: "colgate@b4s.in",
    mobile: "8527484563"
  },
  CSS22: {
    name: "css@buddy4study.com",
    mobile: "8929016460"
  },
  SDS5: {
    name: "sarladevischolarship@buddy4study.com",
    mobile: "8929016464"
  },
  SDS6: {
    name: "sarladevischolarship@buddy4study.com",
    mobile: "8929016464"
  },
  CIS10: {
    name: "clp@buddy4study.com",
    mobile: "8527484563"
  },
  HUS2: {
    name: "hpudaan@hpindiacsr.com",
    mobile: "8448709545"
  },
  CRS11: {
    name: "dlf.foundation@buddy4study.com",
    mobile: "9667396078"
  }
};

export const extractStepTemplate = (steps, step) => {
  return steps.filter(stepObj => stepObj.step === step);
};

export const authorizeByCode = {
  HUS2: {
    exp: "15-03-2019",
    code: "XY03032019HUS2"
  }
};

export const formattedEmail = (front, middle) => `${front}${middle}@b4s.in`;

export const childEduBSID = {
  SCE3: "hul_sce3_",
  CES7: "hul_ces7_",
  CES8: "hul_ces8_",
  CKS1: "hul_cks1_",
  SSE4: "hul_sse4_",
  HUL1: "hul_hul1_",
  SKKS1: "skks1_"
};
// questions ids to exclude from word length vaildations
export const nonValidateIds = [
  31,
  82,
  84,
  88,
  89,
  102,
  103,
  104,
  105,
  106,
  107,
  108,
  109,
  134,
  136,
  138,
  167,
  172,
  176,
  177,
  192,
  212,
  194,
  228,
  229,
  196,
  234,
  251,
  254,
  206,
  278,
  279
];
