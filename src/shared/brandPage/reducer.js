import {
  GET_BRAND_PAGE_SCHOLARSHIPS_REQUESTED,
  GET_BRAND_PAGE_SCHOLARSHIPS_SUCCEEDED,
  GET_BRAND_PAGE_SCHOLARSHIPS_FAILED,
  GET_BRAND_PAGE_ABOUT_REQUESTED,
  GET_BRAND_PAGE_ABOUT_SUCCEEDED,
  GET_BRAND_PAGE_ABOUT_FAILED,
  GET_BRAND_PAGE_SCHOLARS_REQUESTED,
  GET_BRAND_PAGE_SCHOLARS_SUCCEEDED,
  GET_BRAND_PAGE_SCHOLARS_FAILED,
  GET_BRAND_PAGE_BLOGS_REQUESTED,
  GET_BRAND_PAGE_BLOGS_SUCCEEDED,
  GET_BRAND_PAGE_BLOGS_FAILED,
  GET_BRAND_PAGE_FOLLOW_REQUESTED,
  GET_BRAND_PAGE_FOLLOW_SUCCEEDED,
  GET_BRAND_PAGE_FOLLOW_FAILED,
  GET_BRAND_PAGE_FOLLOW_STATUS_REQUESTED,
  GET_BRAND_PAGE_FOLLOW_STATUS_SUCCEEDED,
  GET_BRAND_PAGE_FOLLOW_STATUS_FAILED,
  SEND_BRAND_PAGE_FOLLOW_REQUESTED,
  SEND_BRAND_PAGE_FOLLOW_SUCCEEDED,
  SEND_BRAND_PAGE_FOLLOW_FAILED,
  GET_BRAND_PAGE_FAQ_REQUESTED,
  GET_BRAND_PAGE_FAQ_SUCCEEDED,
  GET_BRAND_PAGE_FAQ_FAILED
} from "./action";

const initialState = {
  showLoader: false,
  isError: false,
  scholarshipData: "",
  aboutUsData: "",
  scholarsData: "",
  blogsData: "",
  followData: "",
  followerStatus: "",
  followDetail: "",
  faqData: ""
};

const brandPageReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_BRAND_PAGE_SCHOLARSHIPS_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        scholarshipData: ""
      };

    case GET_BRAND_PAGE_SCHOLARSHIPS_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        scholarshipData: payload
      };

    case GET_BRAND_PAGE_SCHOLARSHIPS_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        scholarshipData: payload
      };
    case GET_BRAND_PAGE_ABOUT_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        aboutUsData: ""
      };

    case GET_BRAND_PAGE_ABOUT_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        aboutUsData: payload
      };

    case GET_BRAND_PAGE_ABOUT_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        aboutUsData: payload
      };

    case GET_BRAND_PAGE_SCHOLARS_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        scholarsData: ""
      };

    case GET_BRAND_PAGE_SCHOLARS_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        scholarsData: payload
      };

    case GET_BRAND_PAGE_SCHOLARS_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        scholarsData: payload
      };
    case GET_BRAND_PAGE_BLOGS_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        blogsData: ""
      };

    case GET_BRAND_PAGE_BLOGS_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        blogsData: payload
      };

    case GET_BRAND_PAGE_BLOGS_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        blogsData: payload
      };
    case GET_BRAND_PAGE_FOLLOW_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        followData: ""
      };

    case GET_BRAND_PAGE_FOLLOW_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        followData: payload
      };

    case GET_BRAND_PAGE_FOLLOW_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        followData: payload
      };
    case GET_BRAND_PAGE_FOLLOW_STATUS_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        followerStatus: ""
      };

    case GET_BRAND_PAGE_FOLLOW_STATUS_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        followerStatus: payload
      };

    case GET_BRAND_PAGE_FOLLOW_STATUS_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        followerStatus: payload
      };
    case SEND_BRAND_PAGE_FOLLOW_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        followDetail: ""
      };

    case SEND_BRAND_PAGE_FOLLOW_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        followDetail: payload
      };

    case SEND_BRAND_PAGE_FOLLOW_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        followDetail: payload
      };

    case GET_BRAND_PAGE_FAQ_REQUESTED:
      return {
        ...state,
        showLoader: true,
        type,
        faqData: ""
      };
    case GET_BRAND_PAGE_FAQ_SUCCEEDED:
      return {
        ...state,
        showLoader: false,
        type,
        faqData: payload
      };
    case GET_BRAND_PAGE_FAQ_FAILED:
      return {
        ...state,
        showLoader: false,
        type,
        faqData: payload
      };
    default:
      return state;
  }
};
export default brandPageReducer;
