import React, { Component } from "react";
import BrandPage from "../components/brandPage";

import {
  getScholarshipsDetail as getScholarshipsDetailAction,
  getAboutUsDetail as getAboutUsDetailAction,
  getScholarsDetail as getScholarsDetailAction,
  getBlogsDetail as getBlogsDetailAction,
  getBrandFollower as getBrandFollowerAction,
  getFollowerStatus as getFollowerStatusAction,
  getFaqDetail as getFaqDetailAction,
  postBrandFollower as postBrandFollowerAction,
  deleteBrandFollow as deleteBrandFollowAction
} from "../action";

import { connect } from "react-redux";

const mapStateToProps = ({ brandPageReducer, loginOrRegister }) => ({
  showLoader: brandPageReducer.showLoader,
  actionType: brandPageReducer.type,
  aboutUsData: brandPageReducer.aboutUsData,
  scholarshipData: brandPageReducer.scholarshipData,
  scholarsData: brandPageReducer.scholarsData,
  blogsData: brandPageReducer.blogsData,
  followData: brandPageReducer.followData,
  followerStatus: brandPageReducer.followerStatus,
  followDetail: brandPageReducer.followDetail,
  loginType: loginOrRegister.type,
  loginSuccess: loginOrRegister,
  faqData: brandPageReducer.faqData
});

const mapDispatchToProps = dispatch => ({
  getScholarshipsDetail: data => dispatch(getScholarshipsDetailAction(data)),
  getAboutUsDetail: data => dispatch(getAboutUsDetailAction(data)),
  getScholarsDetail: data => dispatch(getScholarsDetailAction(data)),
  getBlogsDetail: data => dispatch(getBlogsDetailAction(data)),
  brandFollower: data => dispatch(getBrandFollowerAction(data)),
  getFollowerStatus: data => dispatch(getFollowerStatusAction(data)),
  postBrandFollower: data => dispatch(postBrandFollowerAction(data)),
  deleteBrandFollow: data => dispatch(deleteBrandFollowAction(data)),
  getFaqDetail: data => dispatch(getFaqDetailAction(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BrandPage);
