import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Colgate from "../../colgate/containers/colgateContainer";
import { BreadCrumDynemic } from "../../components/bread-crum/breadCrumDynemic";
import UserLoginRegistrationPopup from "../../login/containers/userLoginRegistrationContainer";
import gblFunc from "../../../globals/globalFunctions";
import AlertMessage from "../../common/components/alertMsg";
import Loader from "../../common/components/loader";
// import getNestedObjKey from "pushpendra-find-nested-obj-key";
import moment from "moment";
import {
  GET_BRAND_PAGE_SCHOLARSHIPS_SUCCEEDED,
  GET_BRAND_PAGE_SCHOLARSHIPS_FAILED,
  GET_BRAND_PAGE_ABOUT_SUCCEEDED,
  GET_BRAND_PAGE_ABOUT_FAILED,
  GET_BRAND_PAGE_SCHOLARS_SUCCEEDED,
  GET_BRAND_PAGE_SCHOLARS_FAILED,
  GET_BRAND_PAGE_BLOGS_SUCCEEDED,
  GET_BRAND_PAGE_BLOGS_FAILED,
  GET_BRAND_PAGE_FOLLOW_SUCCEEDED,
  GET_BRAND_PAGE_FOLLOW_FAILED,
  GET_BRAND_PAGE_FOLLOW_STATUS_SUCCEEDED,
  GET_BRAND_PAGE_FOLLOW_STATUS_FAILED,
  SEND_BRAND_PAGE_FOLLOW_SUCCEEDED,
  SEND_BRAND_PAGE_FOLLOW_FAILED,
  GET_BRAND_PAGE_FAQ_SUCCEEDED,
  GET_BRAND_PAGE_FAQ_FAILED
} from "../action";
import {
  LOGIN_USER_SUCCEEDED,
  LOG_USER_OUT_SUCCEEDED
} from "../../login/actions";
const defaultProfilePic =
  "https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/profile-img.png";

class BrandPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowAlert: false,
      alertMsg: "",
      alertStatus: "",
      PageStatus: "",
      scholarshipData: [],
      scholarsData: [],
      orgId: "",
      blogsData: [],
      aboutUsData: "",
      followData: "",
      followDetail: false,
      showItem: null,
      showLogin: false,
      hideCloseButton: true,
      hideCloseButton: false,
      showAllArticle: false,
      activeTab: 1,
      breadCrumList: []
    };
    this.flag = false;
    this.closeAlert = this.closeAlert.bind(this);
    this.frequestAskedQuesHandler = this.frequestAskedQuesHandler.bind(this);
    this.changeTab = this.changeTab.bind(this);
    this.increaseFollow = this.increaseFollow.bind(this);
    this.closeLoginPopup = this.closeLoginPopup.bind(this);
    this.showMoreArticle = this.showMoreArticle.bind(this);
  }

  componentDidMount() {
    this.props.getAboutUsDetail({ orgId: this.props.match.params.brandPage });
  }

  componentWillReceiveProps(nextProps) {
    if (
      !this.flag &&
      nextProps.aboutUsData &&
      nextProps.aboutUsData.orgId &&
      nextProps.aboutUsData.orgId !== ""
    ) {
      this.flag = true;
      this.props.getFaqDetail({ orgId: nextProps.aboutUsData.orgId });
    }
    const {
      aboutUsData,
      scholarshipData,
      scholarsData,
      blogsData,
      followData,
      followDetail,
      followerStatus,
      actionType,
      loginType,
      loginSuccess
    } = nextProps;
    let errorCode;
    switch (actionType) {
      case GET_BRAND_PAGE_SCHOLARSHIPS_SUCCEEDED:
        this.setState({
          scholarshipData: scholarshipData
        });
        this.props.getBlogsDetail({ orgId: this.state.orgId });
        break;
      case GET_BRAND_PAGE_SCHOLARSHIPS_FAILED:
        errorCode =
          scholarshipData &&
            scholarshipData.response &&
            scholarshipData.response.data
            ? scholarshipData.response.data.errorCode
            : "";
        // errorCode = getNestedObjKey(scholarshipData, [
        //   "response",
        //   "data",
        //   "errorCode"
        // ]);
        if (errorCode !== 801) {
          this.setState({
            isShowAlert: true,
            alertStatus: false,
            alertMsg: "Server error"
          });
        }
        this.setState({
          isShowAlert: true,
          alertStatus: false,
          alertMsg: "Server error"
        });
        break;
      case GET_BRAND_PAGE_ABOUT_SUCCEEDED:
        this.setState({
          aboutUsData: aboutUsData,
          orgId: aboutUsData.orgId,
          breadCrumList: [
            { pageUrl: "/", pageName: "Home" },
            { pageUrl: "", pageName: "Page" },
            { pageUrl: "", pageName: aboutUsData.name && aboutUsData.name }
          ]
        });
        this.props.getScholarshipsDetail({ orgId: aboutUsData.orgId });
        // this.props.getScholarsDetail({ orgId: aboutUsData.orgId });
        this.props.brandFollower({ orgId: aboutUsData.orgId });
        if (gblFunc.getStoreUserDetails()["userId"]) {
          this.props.getFollowerStatus({
            userId: gblFunc.getStoreUserDetails()["userId"],
            orgId: aboutUsData.orgId
          });
        }
        break;
      case GET_BRAND_PAGE_ABOUT_FAILED:
        if (
          aboutUsData &&
          aboutUsData.response &&
          aboutUsData.response.status === 404
        ) {
          this.setState({
            PageStatus: 404
          });
        } else {
          this.setState({
            isShowAlert: true,
            alertStatus: false,
            alertMsg: "Server error"
          });
        }
        break;
      case GET_BRAND_PAGE_SCHOLARS_SUCCEEDED:
        this.setState({
          scholarsData: scholarsData
        });
        break;
      case GET_BRAND_PAGE_SCHOLARS_FAILED:
        errorCode =
          scholarsData && scholarsData.response && scholarshipData.response.data
            ? scholarsData.response.data.errorCode
            : "";
        // errorCode = getNestedObjKey(scholarsData, [
        //   "response",
        //   "data",
        //   "errorCode"
        // ]);
        if (errorCode !== 801) {
          this.setState({
            isShowAlert: true,
            alertStatus: false,
            alertMsg: "Server error"
          });
        }
        break;
      case GET_BRAND_PAGE_BLOGS_SUCCEEDED:
        this.setState({
          blogsData: blogsData
        });
        break;
      case GET_BRAND_PAGE_BLOGS_FAILED:
        this.setState({
          isShowAlert: true,
          alertStatus: false,
          alertMsg: "Server error"
        });
        break;
      case GET_BRAND_PAGE_FOLLOW_SUCCEEDED:
        this.setState({
          followData: followData
        });
        break;
      case GET_BRAND_PAGE_FOLLOW_FAILED:
        this.setState({
          isShowAlert: true,
          alertStatus: false,
          alertMsg: "Server error"
        });
        break;
      case SEND_BRAND_PAGE_FOLLOW_SUCCEEDED:
        this.props.brandFollower({ orgId: this.state.orgId });
        this.setState({ followDetail: followerStatus.follow });
        break;
      case SEND_BRAND_PAGE_FOLLOW_FAILED:
        this.setState({
          isShowAlert: true,
          alertStatus: false,
          alertMsg: "Server error"
        });
        break;
      case GET_BRAND_PAGE_FOLLOW_STATUS_SUCCEEDED:
        this.setState({ followDetail: followerStatus.follow });
        break;
      case GET_BRAND_PAGE_FOLLOW_STATUS_FAILED:
        break;
    }
    switch (loginType) {
      case "VERIFY_TOKEN":
        if (
          nextProps.userLoginData &&
          nextProps.userLoginData.data &&
          nextProps.userLoginData.data.data &&
          nextProps.userLoginData.data.data.mobileVerified == "0"
        ) {
          this.props.history.push({
            pathname: "/otp",
            state: {
              userId: nextProps.userLoginData.data.data.userId,
              mobile: nextProps.userLoginData.data.data.mobile,
              countryCode: nextProps.userLoginData.data.data.countryCode,
              temp_Token: nextProps.userLoginData.data.data.token,
              location:
                this.props.location && this.props.location.pathname
                  ? this.props.location.pathname
                  : null
            }
          });
        }
        break;
      case LOGIN_USER_SUCCEEDED:
        const userId =
          loginSuccess && loginSuccess.userLoginData
            ? loginSuccess.userLoginData.userId
            : "";
        // const userId = getNestedObjKey(loginSuccess, [
        //   "userLoginData",
        //   "userId"
        // ]);
        if (userId || gblFunc.getStoreUserDetails()["userId"]) {
          this.props.getFollowerStatus({
            userId: userId || gblFunc.getStoreUserDetails()["userId"],
            orgId: aboutUsData.orgId
          });
        }
        break;
      case LOG_USER_OUT_SUCCEEDED:
        this.setState({
          followDetail: false
        });
        break;
    }
  }

  frequestAskedQuesHandler(index) {
    this.setState({
      showItem: index
    });
  }
  changeTab(tab) {
    switch (tab) {
      case 1:
        this.setState({
          activeTab: tab
        });
        break;
      // case 2:
      //   if (!this.state.scholarsData.length) {
      //     this.props.getScholarsDetail({ orgId: this.state.orgId });
      //   }
      //   this.setState({
      //     activeTab: tab
      //   });
      //   break;
    }
  }
  increaseFollow() {
    const user = {
      userId: gblFunc.getStoreUserDetails()["userId"],
      orgId: this.state.orgId,
      data: {
        flgDeleted: false,
        follow: !this.state.followDetail,
        orgId: this.state.orgId,
        userId: gblFunc.getStoreUserDetails()["userId"]
      }
    };
    if (gblFunc.getStoreUserDetails()["userId"]) {
      this.props.postBrandFollower(user);
    } else {
      this.setState({ showLogin: true });
    }
  }
  closeAlert() {
    this.setState({ isShowAlert: false });
  }
  closeLoginPopup(e) {
    e.preventDefault();
    if (e.target.innerHTML !== "OK") {
      this.setState({
        showLogin: false
      });
    } else {
      this.setState({
        showLogin: false
      });
    }
  }
  showMoreArticle() {
    this.setState({ showAllArticle: true });
  }

  render() {
    const { PageStatus } = this.state;

    if (PageStatus === 404) {
      return <Redirect to="/404" />;
    } else {
      return <Colgate {...this.props} />;
    }
  }
}

const Scolarships = ({ scholarshipData, showLoader }) => {
  return (
    <article
      className={
        scholarshipData.length > 4
          ? "sholarship-list sholarship-list-scroll"
          : "sholarship-list"
      }
    >
      {scholarshipData && scholarshipData.length ? (
        scholarshipData.map(s => (
          <article className="box" key={s.scholarshipId}>
            <article className="data-row flex-container">
              <article className="col-md-2 col-sm-12 flex-item">
                <article className="tablecell">
                  <article className="logotext">
                    <img
                      src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/b4scal.png"
                      alt="scholarship-icon"
                    />
                    {!Number.isNaN(parseInt(s.deadline)) ? (
                      <p>{moment(s.deadline).format("YYYY")}</p>
                    ) : (
                        ""
                      )}
                  </article>
                </article>
              </article>
              <article className="col-md-7 col-sm-9 flex-item">
                <article className="tablecell">
                  <article className="contentdisplay">
                    <h2>
                      <a
                        className="ellipsis"
                        href={`https://www.buddy4study.com/scholarship/${s.slug}`}
                      >
                        <span>{s.title}</span>
                      </a>
                      <article className="namelistTooltip">
                        <span>{s.title}</span>
                        <i className="arrow" />
                      </article>
                    </h2>
                    <p>
                      <img
                        src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/scholarship-icon.png"
                        alt="scholarship-icon"
                      />
                      {s.applicableFor}
                    </p>
                    <p>
                      <img
                        src="https://s3-ap-southeast-1.amazonaws.com/buddy4study/static/images/awards-icon.png"
                        alt="awards-icon"
                      />
                      {s.purposeAward}
                    </p>
                  </article>
                </article>
              </article>
              <ShowDeadlineDate deadline={s.deadline} />
            </article>
          </article>
        ))
      ) : (
          <div>
            {!showLoader ? (
              <h4 className="text-center">No scholarship found</h4>
            ) : (
                ""
              )}
          </div>
        )}
    </article>
  );
};

const ShowDeadlineDate = ({ deadline }) => {
  const daysToGO =
    parseInt(
      moment(deadline, "YYYY-MM-DD").diff(moment().startOf("day"), "days")
    ) + 1;

  let daysLabel;
  if (daysToGO === 1) {
    daysLabel = "Last";
  } else if (daysToGO === 2) {
    daysLabel = "1";
  } else if (daysToGO <= 0) {
    daysLabel = "Closed";
  } else {
    daysLabel = daysToGO;
  }
  return (
    <article className="col-md-3 col-sm-3 flex-item">
      {daysToGO >= 1 ? (
        <article>
          {daysToGO < 17 ? (
            <article className="tablecell">
              <article
                className={daysToGO < 9 ? "daysgo pink" : "daysgo yellow"}
              >
                <span>{daysLabel}</span>
                <p>{`day${daysToGO > 2 ? "s" : ""} to go`}</p>
              </article>
            </article>
          ) : (
              <article className="tablecell">
                <article className="calender">
                  <p className="text-center">Last Date to apply</p>
                  <dd>{moment(deadline).format("D")}</dd>
                  <p className="text-center date">
                    {moment(deadline).format("MMM, YY")}
                  </p>
                </article>
              </article>
            )}
        </article>
      ) : !Number.isNaN(daysToGO) ? (
        <article>
          <article className="tablecell">
            <article className="daysgo gray">
              <span>Closed</span>
            </article>
          </article>
        </article>
      ) : (
            <article>
              <article className="tablecell">
                <article className="daysgo pink always">
                  <p>Always Open</p>
                </article>
              </article>
            </article>
          )}
    </article>
  );
};

const Scholars = ({ scholarsData, showLoader }) => {
  return (
    <section
      className={
        scholarsData.length > 6
          ? "awardeesSlider awardeesSlider-scroll"
          : "awardeesSlider"
      }
    >
      {scholarsData && scholarsData.length ? (
        scholarsData.map((x, index) => (
          <article className="col-md-4" key={index}>
            <article className="box border">
              <article className="box-in">
                <img src={x.pic || defaultProfilePic} alt={x.name} />
                <h4>{x.name}</h4>
                <p>{x.scholarshipName}</p>
                <p>{x.instituteName}</p>
                <p>{x.presentClass}</p>
              </article>
            </article>
          </article>
        ))
      ) : (
          <article>
            {!showLoader ? <p className="text-center">No scholars found</p> : ""}
          </article>
        )}
    </section>
  );
};

export default BrandPage;
