import { call, put, takeEvery } from "redux-saga/effects";
import { apiUrl } from "../../constants/constants";
import fetchClient from "../../api/fetchClient";

import {
  GET_BRAND_PAGE_SCHOLARSHIPS_REQUESTED,
  GET_BRAND_PAGE_SCHOLARSHIPS_SUCCEEDED,
  GET_BRAND_PAGE_SCHOLARSHIPS_FAILED,
  GET_BRAND_PAGE_ABOUT_REQUESTED,
  GET_BRAND_PAGE_ABOUT_SUCCEEDED,
  GET_BRAND_PAGE_ABOUT_FAILED,
  GET_BRAND_PAGE_SCHOLARS_REQUESTED,
  GET_BRAND_PAGE_SCHOLARS_SUCCEEDED,
  GET_BRAND_PAGE_SCHOLARS_FAILED,
  GET_BRAND_PAGE_BLOGS_REQUESTED,
  GET_BRAND_PAGE_BLOGS_SUCCEEDED,
  GET_BRAND_PAGE_BLOGS_FAILED,
  GET_BRAND_PAGE_FOLLOW_REQUESTED,
  GET_BRAND_PAGE_FOLLOW_SUCCEEDED,
  GET_BRAND_PAGE_FOLLOW_FAILED,
  GET_BRAND_PAGE_FOLLOW_STATUS_REQUESTED,
  GET_BRAND_PAGE_FOLLOW_STATUS_SUCCEEDED,
  GET_BRAND_PAGE_FOLLOW_STATUS_FAILED,
  SEND_BRAND_PAGE_FOLLOW_REQUESTED,
  SEND_BRAND_PAGE_FOLLOW_SUCCEEDED,
  SEND_BRAND_PAGE_FOLLOW_FAILED,
  GET_BRAND_PAGE_FAQ_REQUESTED,
  GET_BRAND_PAGE_FAQ_SUCCEEDED,
  GET_BRAND_PAGE_FAQ_FAILED
} from "./action";

const getScholarshipApi = inputData => {
  return fetchClient
    .get(`${apiUrl.scholarshipUrl}/${inputData.orgId}/scholarship`)
    .then(res => {
      return res.data;
    });
};

function* getScholarship({ data }) {
  try {
    const response = yield call(getScholarshipApi, data);
    yield put({
      type: GET_BRAND_PAGE_SCHOLARSHIPS_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    yield put({
      type: GET_BRAND_PAGE_SCHOLARSHIPS_FAILED,
      payload: error
    });
  }
}

const getAboutUsApi = inputData => {
  // inputData.orgId
  return fetchClient
    .get(`${apiUrl.brandPageAboutUs}/${inputData.orgId}/aboutus`)
    .then(res => {
      return res.data;
    });
};

function* getAboutUs({ data }) {
  try {
    const response = yield call(getAboutUsApi, data);
    yield put({
      type: GET_BRAND_PAGE_ABOUT_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    yield put({
      type: GET_BRAND_PAGE_ABOUT_FAILED,
      payload: error
    });
  }
}
// https://devapi.buddy4study.com:8444/api/v1.0/ums/swagger-ui.html#!/organisation45controller/getOrganisationFaqUsingGET
// https://devapi.buddy4study.com:8444/api/v1.0/ums/organisation/MSDF/aboutus

const getFaqApi = inputData => {
  // inputData.orgId
  return fetchClient
    .get(`${apiUrl.faqPageUrl}/${inputData.orgId}/faq`)
    .then(res => {
      return res.data;
    });
};

function* getFaq({ data }) {
  try {
    const response = yield call(getFaqApi, data);
    yield put({
      type: GET_BRAND_PAGE_FAQ_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    yield put({
      type: GET_BRAND_PAGE_FAQ_FAILED,
      payload: error
    });
  }
}

const getScholarsApi = inputData => {
  return fetchClient
    .get(`${apiUrl.scholarshipUrlOrganisation}/${inputData.orgId}/scholar`)
    .then(res => {
      return res.data;
    });
};

function* getScholars({ data }) {
  try {
    const response = yield call(getScholarsApi, data);
    yield put({
      type: GET_BRAND_PAGE_SCHOLARS_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    yield put({
      type: GET_BRAND_PAGE_SCHOLARS_FAILED,
      payload: error
    });
  }
}

const getBlogsApi = inputData => {
  return fetchClient
    .get(`${apiUrl.brandPage}/${inputData.orgId}/related-article?size=10`)
    .then(res => {
      return res.data;
    });
};

function* getBlogs({ data }) {
  try {
    const response = yield call(getBlogsApi, data);
    yield put({
      type: GET_BRAND_PAGE_BLOGS_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    yield put({
      type: GET_BRAND_PAGE_BLOGS_FAILED,
      payload: error
    });
  }
}

const brandFollowApi = inputData => {
  return fetchClient
    .get(`${apiUrl.getFollower}/${inputData.orgId}/follow`)
    .then(res => {
      return res.data;
    });
};

function* brandFollow({ data }) {
  try {
    const response = yield call(brandFollowApi, data);
    yield put({
      type: GET_BRAND_PAGE_FOLLOW_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    yield put({
      type: GET_BRAND_PAGE_FOLLOW_FAILED,
      payload: error
    });
  }
}

const brandFollowStatusApi = inputData => {
  return fetchClient
    .get(
      `${apiUrl.brandFollower}/${inputData.userId}/organisation/${
        inputData.orgId
      }/follow`
    )
    .then(res => {
      return res.data;
    });
};

function* brandFollowStatus({ data }) {
  try {
    const response = yield call(brandFollowStatusApi, data);
    yield put({
      type: GET_BRAND_PAGE_FOLLOW_STATUS_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    yield put({
      type: GET_BRAND_PAGE_FOLLOW_STATUS_FAILED,
      payload: error
    });
  }
}

const postBrandFollowApi = inputData => {
  return fetchClient
    .post(
      `${apiUrl.brandFollower}/${inputData.data.userId}/organisation/${
        inputData.data.orgId
      }/follow`,
      inputData.data
    )
    .then(res => {
      return res.data;
    });
};

function* postBrandFollow({ data }) {
  try {
    const response = yield call(postBrandFollowApi, data);
    yield put({
      type: SEND_BRAND_PAGE_FOLLOW_SUCCEEDED,
      payload: response
    });
  } catch (error) {
    yield put({
      type: SEND_BRAND_PAGE_FOLLOW_FAILED,
      payload: error
    });
  }
}

export default function* interestFormSaga() {
  yield takeEvery(GET_BRAND_PAGE_SCHOLARSHIPS_REQUESTED, getScholarship);
  yield takeEvery(GET_BRAND_PAGE_ABOUT_REQUESTED, getAboutUs);
  yield takeEvery(GET_BRAND_PAGE_SCHOLARS_REQUESTED, getScholars);
  yield takeEvery(GET_BRAND_PAGE_BLOGS_REQUESTED, getBlogs);
  yield takeEvery(GET_BRAND_PAGE_FOLLOW_REQUESTED, brandFollow);
  yield takeEvery(GET_BRAND_PAGE_FOLLOW_STATUS_REQUESTED, brandFollowStatus);
  yield takeEvery(SEND_BRAND_PAGE_FOLLOW_REQUESTED, postBrandFollow);
  yield takeEvery(GET_BRAND_PAGE_FAQ_REQUESTED, getFaq);
}
