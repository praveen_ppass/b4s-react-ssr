import { render } from "react-dom";
import React, { Component } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { loadComponents } from "loadable-components";
import { Provider } from "react-redux";
import { combineReducers, createStore, compose, applyMiddleware } from "redux";
import {
  ConnectedRouter,
  routerReducer,
  routerMiddleware
} from "react-router-redux";
import { hotjar } from 'react-hotjar';
import createBrowserHistory from "history/createBrowserHistory";
import createMemoryHistory from "history/createMemoryHistory";
import createSagaMiddleware from "redux-saga";
// import { loadingBarReducer } from "react-redux-loading-bar";
import "babel-polyfill";
import "./../shared/b4s-theme/app.scss";
import App from "../shared/app";
import sagas from "../shared/app/sagas";
import { apiUrl } from "../constants/constants";
import fetchClient from "../api/fetchClient";
import axios from "axios";
import { error } from "util";
import HOC from "../shared/common/components/HOC";

import homeReducer from "../shared/home/reducer";
import scholarshipReducer from "../shared/scholarship/reducer";
import onlineApplicationFormListReducer from "../shared/online-application-form-list/reducer";
import termsConditionsReducer from "../shared/terms-conditions/reducer";
import contactUsReducer from "../shared/contact-us/reducer";
import privacyConditionReducer from "../shared/privacy-policy/reducer";
import scholarshipDetailReducer from "../shared/scholarship/reducer";
import commonReducer from "../constants/commonReducer";
import faqsReducer from "../shared/faqs/reducer";
import dashboardReducer from "../shared/dashboard/reducer";
import faqDetailReducer from "../shared/faqs/reducer";
import teamReducer from "../shared/team/reducer";
import loginRegistrationReducer from "../shared/login/reducer";
import careersReducer from "../shared/careers/reducer";
import aboutReducer from "../shared/about/reducer";
import mediaPartnerReducer from "../shared/media-partners/reducer";
import mediaUrlReducer from "../shared/media-url/reducer";
import educationReducer from "../shared/education-loan/reducer";

import cscReducer from "../shared/csc/reducer";
import oppReducer from "../shared/careers/reducer";
import bookSlotReducer from "../shared/book-slot/reducer";
import applicationPersonalInfoReducer from "../shared/application/reducers/applicationPersonalInfoReducer";
import applicationFamilyEarningsReducer from "../shared/application/reducers/applicationFamilyEarningsReducer";
import applicationReferenceReducer from "../shared/application/reducers/applicationReferenceReducer";
import applicationQuestionReducer from "../shared/application/reducers/applicationQuestionReducer";
import marchantReducer from "../shared/vle/reducer";
import applicationInstructionReducer from "../shared/application/reducers/applicationInstructionsReducer";
import applicationEducationReducer from "../shared/application/reducers/applicationEducationReducer";
import applicationFormReducer from "../shared/application/reducers/applicationFormReducer";
import applicationSummeryReducer from "../shared/application/reducers/applicationSummeryReducer";
import applicationAwardWonReducer from "../shared/application/reducers/applicationAwardWonReducer";
import applicationDocumentReducer from "../shared/application/reducers/applicationDocumentReducer";
import applicationEntranceExamReducer from "../shared/application/reducers/applicationExtranceExamReducer";
import internationalStudyReducer from "../shared/international-study/reducer";
import interestFormReducer from "../shared/interest-form/reducer";
import footerBannerReducer from "../shared/banners/reducer";
import quesAnsReducer from "../shared/question-answer/reducer";
import brandPageReducer1 from '../shared/colgate/reducer';
import scholarshipVideoReducer from "../shared/scholarship-video/reducer";
import userSubscribedReducer from "../shared/user-unsubscribed/reducer";
import brandPageReducer from "../shared/brandPage/reducer";
import scholarshipConclaveReducer from "../shared/scholarship-conclave/reducer";
import applicationPaymentReducer from "../shared/applicationPayment/reducers";
import applicationBankDtsReducer from "../shared/application/reducers/applicationBankDetailsReducer";
import resultsLookReducers from "../shared/scholarResults/reducers";
import HulLoginFormReducer from "../shared/application/reducers/hulLoginFormReducer";
import applicationShareLinkReducers from "../shared/applicationShareLink/reducers";
import collegeBoardReducer from "../shared/college-board/reducers";
import scholarReducers from "../shared/scholar/scholarReducer";
import contentReducer from "../shared/content/reducer";
import dontionReducer from "../shared/covid-19-donation/reducer";

if (typeof window !== "undefined") {
  window.onload = function () {
    document.getElementById("topLoader").style.display = "none";
    //document.getElementById("loading-bar-spinne").style.display = "none";
    document.getElementById("root").style.display = "block";
  };
}

// Grab the state from a global variable injected into the server-generated HTML
//const preloadedState = window.__PRELOADED_STATE__;

const reducer = combineReducers({
  routing: routerReducer,
  home: homeReducer,
  scholarship: scholarshipReducer,
  onlineApplicationFormList: onlineApplicationFormListReducer,
  termsConditions: termsConditionsReducer,
  contactUs: contactUsReducer,
  privacyConditions: privacyConditionReducer,
  team: teamReducer,
  scholarshipDetail: scholarshipDetailReducer,
  faqs: faqsReducer,
  faqDetail: faqDetailReducer,
  common: commonReducer,
  dashboard: dashboardReducer,
  loginOrRegister: loginRegistrationReducer,
  careers: careersReducer,
  about: aboutReducer,
  mediaPartner: mediaPartnerReducer,
  mediaUrl: mediaUrlReducer,
  educationLoan: educationReducer,
  csc: cscReducer,
  bookSlot: bookSlotReducer,
  applicationPersonalInfo: applicationPersonalInfoReducer,
  applicationFamilyEarnings: applicationFamilyEarningsReducer,
  marchant: marchantReducer,
  appInstr: applicationInstructionReducer,
  applicationReference: applicationReferenceReducer,
  applicationQuestion: applicationQuestionReducer,
  applicationEducation: applicationEducationReducer,
  applicationForm: applicationFormReducer,
  applicationSummery: applicationSummeryReducer,
  applicationAwardWon: applicationAwardWonReducer,
  applicationDocument: applicationDocumentReducer,
  applicationEntranceExam: applicationEntranceExamReducer,
  internationalStd: internationalStudyReducer,
  interestForm: interestFormReducer,
  footerBanner: footerBannerReducer,
  userSubscribed: userSubscribedReducer,
  quesAnswerData: quesAnsReducer,
  brandPageReducer1: brandPageReducer1,
  oppReducer: oppReducer,
  scholarshipVideo: scholarshipVideoReducer,
  applicationPayment: applicationPaymentReducer,
  brandPageReducer: brandPageReducer,
  scholarshipConclave: scholarshipConclaveReducer,
  applicationBankDts: applicationBankDtsReducer,
  userSubscribed: userSubscribedReducer,
  resultLookUp: resultsLookReducers,
  hulLogin: HulLoginFormReducer,
  applicationShareLink: applicationShareLinkReducers,
  collegeBoard: collegeBoardReducer,
  scholarReducers: scholarReducers,
  contentReducer: contentReducer,
  dontionReducer:dontionReducer
});

var history;
if (typeof window !== "undefined") {
  history = createBrowserHistory();
} else {
  history = createMemoryHistory(); //This kind of history is needed for server-side rendering.
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const sagaMiddleware = createSagaMiddleware();
// Allow the passed state to be garbage-collected
//delete window.__PRELOADED_STATE__;
let initialState = {};
export const store = createStore(
  reducer,
  initialState,
  //preloadedState,//will be used later will synced with server state
  composeEnhancers(applyMiddleware(routerMiddleware(history), sagaMiddleware))
);

// then run the saga
sagaMiddleware.run(sagas);

class Main extends Component {
  componentDidMount() { 
hotjar.initialize(766314, 6); // set hotjar
}

  render() {
    return (
      <Router>
        <HOC>
          <App {...this.props} />
        </HOC>
      </Router>
    );
  }
}

loadComponents().then(() => {
  render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Main />
      </ConnectedRouter>
    </Provider>,
    document.getElementById("root")
  );
});
