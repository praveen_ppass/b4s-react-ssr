import axios from "axios";
import { store } from "../client/index";
import utils from "../globals/globalFunctions";
import { apiUrl } from "../constants/constants";
import { messages } from "../constants/constants";
var jwtDecode = require("jwt-decode");
var renewTokenPromise;
const fetchClient = () => {
  const defaultOptions = {
    //baseURL: process.env.REACT_APP_API_PATH,//will set it later@pushpendra
    baseURL: "",
    method: "get",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json"
      //will set and get it from localstorage while implementing login@pushpendra
      //"B4SAUTH": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJidWRkeTRzdHVkeSIsImV4cCI6MTgzOTQ4OTc5Mn0.gu_6RvKoOB-eFTwSkGgMK7Mova7PRP9eKF9FteMpR1Izec4SbetsfH5qJZZjcFy9OVEWEgisE8i1tf0bt8l9mA"
    }
  };

  // Create instance
  let instance = axios.create(defaultOptions);

  /********************************** Start User token refresh managment@Pushpendra****************************** */
  var isTokenRefreshing = false;
  // Set the AUTH token for any request
  instance.interceptors.request.use(function (config) {
    config.headers.Authorization = "Bearer " + utils.getAuthToken(); //send token in every call............
    if (
      typeof window !== "undefined" &&
      parseInt(localStorage.getItem("isAuth")) &&
      !isTokenRefreshing
    ) {
      //Refresh token only if user is logged in
      const accessTokenDecoded = jwtDecode(utils.getAuthToken());
      const refreshTokenDecoded = jwtDecode(utils.getRefreshToken());

      //rfresh token only in case user is logged in and no request for refresh is running
      const isRefreshTokenExpired = refreshTokenDecoded.exp
        ? refreshTokenDecoded.exp * 1000 <= new Date().getTime()
        : true;
      const isAccessTokenExpired = accessTokenDecoded.exp
        ? accessTokenDecoded.exp * 1000 <= new Date().getTime()
        : true;
      const tokenExpireDelta =
        new Date().getTime() -
        (parseInt(localStorage.getItem("tokenGenerationTime")) +
          parseInt(localStorage.getItem("tokenExpiry")) * 1000); //make it ms and find delta
      if (isRefreshTokenExpired) {
        //If refresh token is expired then logout and go to Home Page
        localStorage.clear();
        console.warn("Refresh token Expired...");
        window.location.href = "/?sessionExpired=true";
        return;
      }

      if (tokenExpireDelta > -10000 || isAccessTokenExpired) {
        //refersh token before 10 seconds of expiration
        isTokenRefreshing = true;

        // renewToken performs authentication using username/password saved in sessionStorage/localStorage
        renewTokenPromise = new Promise((resolve, reject) => {
          utils.refreshToken(apiUrl.loginUser).then(response => {
            config.headers.Authorization =
              "Bearer " + response.data.access_token;
            // Get your config from the response
            utils.storeAuthDetails(response.data);

            isTokenRefreshing = false;
            // Resolve the promise
            resolve(config);
          }, reject);

          // Or when you don't need an HTTP request just resolve
          // resolve(config);
        });
        return renewTokenPromise;
      } //end if
    } else {
      if (isTokenRefreshing) {
        return renewTokenPromise.then(function () {
          config.headers.Authorization = "Bearer " + utils.getAuthToken();
          return config;
        });
      }
      //If user is not logged in.......
      const accessTokenDecoded = jwtDecode(utils.getAuthToken());
      const isAccessTokenExpired = accessTokenDecoded.exp
        ? accessTokenDecoded.exp * 1000 <= new Date().getTime()
        : true;

      if (isAccessTokenExpired && typeof window !== "undefined") {
        console.warn("Guest token expired");
        localStorage.clear();
        window.location.href = "/?sessionExpired=true";
        return;
      }
    }

    return config;
  });
  /********************************** End User token refresh managment@Pushpendra****************************** */

  /***********************************Start Response Interceptor for token handling*************************************** */
  instance.interceptors.response.use(
    response => response,
    error => {
      let value = error.response;
      //To handle network error thrown by axios(can be server OR CLIENT)
      if (!error.response.status) {
        error.errorMessage = messages.generic.networkError;
        return Promise.reject(error);
      }

      const httpCode = error.response.status;
      const errMessage = messages.generic.httpErrors[httpCode];

      error.errorMessage = errMessage;
      if (value.status === 401) {
        utils.getRefreshToken();
        return instance(error.config);
      }

      return Promise.reject(error);
    }
  );
  /************************************End Response Interceptor for token handling*************************************** */

  return instance;
};

export default fetchClient();
