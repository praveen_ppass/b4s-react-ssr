import axios from "axios";
import { apiUrl } from "../constants/constants";

export const guestAuthTokenCall = () => {
  let input = new FormData();
  input.append("grant_type", "client_credentials");
  input.append("portalId", 1);

  return axios({
    url: apiUrl.tokenGeneration,
    method: "post",
    headers: {
      "content-type":
        "multipart/form-data; boundary=---011000010111000001101001"
    },
    data: input,
    auth: {
      username: "b4s",
      password: "B$S!#"
    }
  });

  // return axios.post(apiUrl.tokenGeneration, guestAuthParams, requestConfig);
};

export const refreshTokenCall = token => {
  let input = new FormData();
  input.append("refresh_token", token);
  input.append("grant_type", "refresh_token");

  return axios({
    url: apiUrl.loginUser,
    method: "post",

    dataType: "jsonp",

    headers: {
      "content-type":
        "multipart/form-data; boundary=---011000010111000001101001"
    },
    data: input,
    auth: {
      username: "b4s",
      password: "B$S!#"
    }
  });

  // return axios.post(apiUrl.tokenGeneration, guestAuthParams, requestConfig);
};
