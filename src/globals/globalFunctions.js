/**************File header *********************/
//Purpose : All global and helper function would be here
//All methods will be enclosed in gblFunc object
//@author - Pushpendra
/**************File header *********************/
// import { apiUrl } from "../constants/constants";
import axios from "axios";
import "url-search-params-polyfill";
var pageTitle =
  "Largest Online Scholarship Portal in India [Become a Scholar] | Buddy4Study";
const consoleFlag =
  typeof process.env !== "undefined"
    ? process.env.NODE_ENV === "dev"
      ? true
      : false
    : false;

const checkIfVleStudentId = () => {
  if (window && window.localStorage) {
    const vleStudentId = localStorage.getItem("vleStudentId");
    const sid = sessionStorage.getItem("sid");
    const userid = localStorage.getItem("userId");
    const location = window.location.pathname;

    if (vleStudentId && location.includes("application")) {
      return vleStudentId;
    } else if (sid && location.includes("application")) {
      return sid;
    } else {
      return userid;
    }
  }
};

const isUndefinedOrNull = val => {
  if (!val || val !== null || val !== undefined) {
    return true;
  } else return false;
};

let gblFunc = {
  gaTrack: {
    //function to track page views as virtual and event also@Pushpendra
    trackPageView: function (pageName, title = pageTitle) {
      if (typeof window !== "undefined") {
        b4sGTM.push({ userMode: gblFunc.checkUserStatus() });
        b4sGTM.push({
          event: "virtualPageView",
          virtualPageURL:
            typeof window !== "undefined" ? window.location.pathname : "",
          virtualPageTitle: pageName
        });
      }
    },
    trackEvent: function (evArr) {
      if (typeof window !== "undefined") {
        b4sGTM.push({
          event: "Event",
          eventCategory: evArr[0],
          eventAction: evArr[2] ? evArr[2] : "Click",
          eventLabel: evArr[1]
        });
      }
    }
  },
  checkUserStatus: function () {
    return 1;
  },
  capitalText: function (str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  },
  replace_underScore: function (str) {
    return str
      .split("_")
      .map((l, i) => (i > 0 ? `${l.toUpperCase()[0]}${l.slice(1)}` : l))
      .join("");
  },
  camcelCase: function (str) {
    return str
      .split("_")
      .map(
        (l, i) =>
          i > 0
            ? `${l.toUpperCase()[0]}${l.toLowerCase().slice(1)}`
            : l.toLowerCase()
      )
      .join("");
  },
  replaceWithLoreal: function (input, noRender) {
    //second arg noRender - if this text is not rendering as html and parse only if it exists@pushpendra
    // let lorealFormat =
    //   input && input.includes("L'oreal")
    //     ? "L'oreal"
    //     : input && input.includes("loreal")
    //       ? "loreal"
    //       : "LOreal";

    return input
      ? noRender
        ? input.replace("LOreal", "L'Oréal")
        : input.replace("LOreal", "L'Or&eacute;al")
      : input;
  },
  hasProp(obj, prop) {
    return Object.prototype.hasOwnProperty.call(obj, prop);
  },
  refreshToken: (url, token) => {
    const refreshToken = !token ? gblFunc.getRefreshToken() : token;
    var form = new FormData();
    form.append("refresh_token", refreshToken);
    form.append("grant_type", "refresh_token");
    return new Promise(
      (resolve, reject) => {
        axios({
          //url: apiUrl.loginUser,
          url: url,
          method: "post",
          headers: {
            "content-type":
              "multipart/form-data; boundary=---011000010111000001101001"
          },
          data: form,
          auth: {
            username: "b4s",
            password: "B$S!#"
          }
        })
          .then(function (response) {
            resolve(response);
          })
          .catch(function (error) {
            reject(error);
          });
      },
      error => { }
    );
  },

  storeAuthDetails: loginDetails => {
    if (typeof window !== "undefined") {
      let {
        access_token,
        refresh_token,
        userId,
        expires_in,
        mobile,
        email,
        profileLocked
      } = loginDetails;
      localStorage.setItem("accessToken", access_token);
      localStorage.setItem("refreshToken", refresh_token);
      localStorage.setItem("tokenExpiry", expires_in);
      localStorage.setItem("tokenGenerationTime", new Date().getTime());
      localStorage.setItem("userId", userId);
      if (mobile) {
        localStorage.setItem("mobile", mobile);
      }
      if (email) {
        localStorage.setItem("email", email);
      }
      localStorage.setItem("isAuth", 1);
    }
  },
  storeRegAuthDetails: loginDetails => {
    if (typeof window !== "undefined") {
      let {
        access_token,
        refresh_token,
        userId,
        expires_in,
        mobile,
        email
      } = loginDetails;
      localStorage.setItem("accessToken", access_token);
      localStorage.setItem("refreshToken", refresh_token);
      localStorage.setItem("tokenExpiry", expires_in);
      localStorage.setItem("tokenGenerationTime", new Date().getTime());
      localStorage.setItem("userId", userId);
      if (mobile) {
        localStorage.setItem("mobile", mobile);
      }
      if (email) {
        localStorage.setItem("email", email);
      }
    }
  },

  storeAuthToken: token =>
    typeof window !== "undefined"
      ? localStorage.setItem("accessToken", token)
      : "",

  getAuthToken: () =>
    typeof window !== "undefined" ? localStorage.getItem("accessToken") : "",
  getRefreshToken: () =>
    typeof window !== "undefined" ? localStorage.getItem("refreshToken") : "",
  isAuthTokenPresent: () =>
    typeof window !== "undefined"
      ? localStorage.getItem("accessToken")
        ? true
        : false
      : "",
  isUserAuthenticated: () =>
    typeof window !== "undefined"
      ? parseInt(localStorage.getItem("isAuth"))
        ? true
        : false
      : "",

  removeUserDetails: () => {
    const userDetailKeys = [
      "pic",
      "firstName",
      "lastName",
      "email",
      "mobile",
      "membershipExpiry",
      "percentage",
      "vleUser",
      "cscId",
      "accessToken",
      "refreshToken",
      "tokenExpiry",
      "tokenGenerationTime",
      "userId",
      "applicationSchID",
      "vleStudentId",
      "hulType",
      "currentPath",
      "userInfo",
      "profileLocked"
    ];
    if (typeof window !== "undefined") {
      userDetailKeys.map(item => {
        localStorage.removeItem(item);
      });
    }

    if (typeof window !== "undefined") {
      // localStorage.removeItem("accessToken");
      // localStorage.removeItem("refreshToken");
      // localStorage.removeItem("tokenExpiry");
      // localStorage.removeItem("tokenGenerationTime");
      // localStorage.removeItem("email");
      // localStorage.removeItem("mobile");
      // localStorage.removeItem("userId");
      // localStorage.removeItem("pic");
      // localStorage.removeItem("firstName");
      // localStorage.removeItem("lastName");
      // localStorage.removeItem("percentage");
      // localStorage.removeItem("membershipExpiry");
      // localStorage.removeItem("applicationSchID");
      // localStorage.removeItem("vleStudentId");
      //Set isAuth to 0, remove rest.
      localStorage.setItem("isAuth", 0);

      document.cookie =
        "schDtCounter =; expires = Thu, 01 Jan 1970 00:00:00 UTC; path=/";
      // localStorage.removeItem("vleUser");
      // localStorage.removeItem("hulType");
    }
  },
  removeUserDetailsForRule: () => {
    const userDetailKeys = [
      "pic",
      "email",
      "mobile",
      "membershipExpiry",
      "percentage",
      "vleUser",
      "cscId",
      "refreshToken",
      "tokenExpiry",
      "tokenGenerationTime",
      "userId",
      "applicationSchID",
      "vleStudentId",
      "hulType",
      "userInfo",
      "profileLocked"
    ];
    if (typeof window !== "undefined") {
      userDetailKeys.map(item => {
        localStorage.removeItem(item);
      });
    }
    if (typeof window !== "undefined") {
      localStorage.setItem("isAuth", 0);
      document.cookie =
        "schDtCounter =; expires = Thu, 01 Jan 1970 00:00:00 UTC; path=/";
    }
  },

  removeCSCUserDetails: () => {
    const userDetailKeys = [
      "pic",
      "firstName",
      "lastName",
      "email",
      "mobile",
      "membershipExpiry",
      "percentage",
      "vleUser",
      "cscId",
      "accessToken",
      "refreshToken",
      "tokenExpiry",
      "tokenGenerationTime",
      "userId",
      "applicationSchID",
      "vleStudentId",
      "hulType",
      "profileLocked"
    ];
    if (typeof window !== "undefined") {
      userDetailKeys.map(item => {
        localStorage.removeItem(item);
      });
    }

    if (typeof window !== "undefined") {
      // localStorage.removeItem("accessToken");
      // localStorage.removeItem("userId");
      // localStorage.removeItem("pic");
      // localStorage.removeItem("firstName");
      // localStorage.removeItem("lastName");
      // localStorage.removeItem("percentage");
      // localStorage.removeItem("email");
      // localStorage.removeItem("mobile");
      // localStorage.removeItem("membershipExpiry");
      //Set isAuth to 0, remove rest.
      localStorage.setItem("isAuth", 0);
      // localStorage.removeItem("cscId");
      // localStorage.removeItem("vleUser");
      // localStorage.removeItem("hulType");
    }
  },
  storeUserDetails: userDetails => {
    const userDetailKeys = [
      "pic",
      "firstName",
      "lastName",
      "email",
      "mobile",
      "membershipExpiry",
      "percentage",
      "vleUser",
      "cscId",
      "profileLocked"
    ];
    if (typeof window !== "undefined") {
      userDetailKeys.map(item => {
        if (userDetails[item] && userDetails[item] != undefined && userDetails[item] != null) {
          localStorage.setItem(item, userDetails[item]);
        } else localStorage.setItem(item, "");
      });
      // localStorage.setItem(
      //   "pic",
      //   !isUndefinedOrNull(userDetails.pic) ? userDetails.pic : ""
      // );
      // localStorage.setItem("firstName", userDetails.firstName);
      // localStorage.setItem("lastName", userDetails.lastName);
      // localStorage.setItem("email", userDetails.email);
      // localStorage.setItem("mobile", userDetails.mobile);
      // localStorage.setItem("membershipExpiry", userDetails.membershipExpiry);
      // localStorage.setItem("percentage", userDetails.profilePercentage);
      localStorage.setItem("vleUser", Boolean(userDetails.vleUser));
      // localStorage.setItem("cscId", userDetails.cscId);
    }
  },

  storeCscUserDetails: userDetails => {
    const userDetailKeys = [
      "pic",
      "firstName",
      "lastName",
      "email",
      "mobile",
      "membershipExpiry",
      "percentage",
      "vleUser",
      "cscId",
      "profileLocked"
    ];
    if (typeof window !== "undefined") {
      userDetailKeys.map(item => {
        if (!isUndefinedOrNull(userDetails[item])) {
          localStorage.setItem(item, userDetails[item]);
        } else localStorage.setItem(item, "");
      });
    }
    // if (typeof window !== "undefined") {
    //   localStorage.setItem("pic", null);
    //   localStorage.setItem("firstName", userDetails.firstName);
    //   localStorage.setItem("email", userDetails.email);
    //   localStorage.setItem("mobile", userDetails.mobile);
    //   localStorage.setItem("membershipExpiry", userDetails.membershipExpiry);
    //   localStorage.setItem("lastName", userDetails.lastName);
    //   localStorage.setItem("percentage", userDetails.profilePercentage);
    //   localStorage.setItem("vleUser", Boolean(userDetails.vleUser));
    //   localStorage.setItem("cscId", userDetails.cscId);
    // }
  },

  isNull: value => value === "null",

  getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return "";
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  },

  getStoreUserDetails: () => {
    if (typeof window !== "undefined") {
      return {
        pic: localStorage.getItem("pic"),
        firstName: localStorage.getItem("firstName"),
        lastName: localStorage.getItem("lastName"),
        percentage: localStorage.getItem("percentage"),
        userId: checkIfVleStudentId(),
        email: localStorage.getItem("email"),
        mobile: localStorage.getItem("mobile"),
        profileLocked:localStorage.getItem("profileLocked"),
        vleUser: localStorage.getItem("vleUser"),
        cscId: localStorage.getItem("cscId"),
        sid: sessionStorage.getItem("sid"),
        membershipExpiry: localStorage.getItem("membershipExpiry"),
        applicationSchID: localStorage.getItem("applicationSchID"),
        admissionUserId: localStorage.getItem("admissionUserId")
      };
    } else {
      return {
        pic: "",
        firstName: "",
        lastName: "",
        percentage: "",
        userId: "",
        cscId: "",
        vleUser: "",
        membershipExpiry: "",
        profileLocked:""
      };
    }
  },

  getCscStoreUserDetails: () => {
    if (typeof window !== "undefined") {
      return {
        pic: localStorage.getItem("pic"),
        firstName: localStorage.getItem("firstName"),
        lastName: localStorage.getItem("lastName"),
        email: localStorage.getItem("email"),
        mobile: localStorage.getItem("mobile"),
        profileLocked:localStorage.getItem("profileLocked"),
        percentage: localStorage.getItem("percentage"),
        userId: localStorage.getItem("userId"),
        cscId: localStorage.getItem("cscId"),
        vleUser: localStorage.getItem("vleUser"),
        membershipExpiry: localStorage.getItem("membershipExpiry")
      };
    } else {
      return {
        pic: "",
        firstName: "",
        lastName: "",
        percentage: "",
        userId: "",
        cscId: "",
        vleUser: "",
        membershipExpiry: "",
        profileLocked:""
      };
    }
  },
  loadJsScript: (src, promise) => {
    if (promise) {
      return new Promise((resolve, reject) => {
        var tag = document.createElement("script");
        //tag.async = true;
        tag.src = src;
        tag.type = "text/javascript";
        document.body.appendChild(tag);
        tag.addEventListener("load", () => {
          resolve(tag);
        });
        tag.addEventListener("error", () => {
          reject(new Error("Unable to load"));
        });
      });
    } else {
      var tag = document.createElement("script");
      //tag.async = true;
      tag.src = src;
      tag.type = "text/javascript";
      document.body.appendChild(tag);
    }
  },

  storeApplicationScholarshipId: schId => {
    if (typeof window !== "undefined") {
      localStorage.setItem("applicationSchID", schId);
    }
  },

  getStoreApplicationScholarshipId: () => {
    if (typeof window !== "undefined") {
      return localStorage.getItem("applicationSchID");
    }
  },

  //Create global console.log as logMessage
  logMessage: msg => {
    if (!consoleFlag) {
      return false;
    } else {
      console.log(msg);
    }
  },
  storeHulAuthType(type) {
    if (typeof window !== "undefined") {
      localStorage.setItem("hulType", type);
    }
  },

  getStoreHulAuthType() {
    if (typeof window !== "undefined") {
      return localStorage.getItem("hulType");
    }
  },
  // Create a method for scrolling page according to element
  scrollPage(elem, refName = false) {
    const scrollToComponent = require("react-scroll-to-component"); //Need this package on clienbt only as it does not work on server@Pushpendra
    if (!refName) {
      const params = new URLSearchParams(location.search);
      const scroll = params.get("scroll");
      switch (scroll) {
        case "faq":
          setTimeout(() => {
            scrollToComponent(elem, {
              offset: -20,
              align: "top"
            });
          }, 400);
          break;
        case "downloadPDF":
          setTimeout(() => {
            scrollToComponent(elem, {
              align: "top"
            });
          }, 1200);
          break;
      }
    } else {
      switch (refName) {
        case "personalProfile":
          scrollToComponent(elem, {
            offset: 50,
            align: "top"
          });
          break;
      }
    }
  },
  getFullName: function () {
    const firstName = this.getStoreUserDetails()["firstName"];
    const lastName = this.getStoreUserDetails()["lastName"];

    let fullName = "";

    if (firstName && lastName) {
      fullName = firstName + " " + lastName;
    } else if (firstName && !lastName) {
      fullName = firstName;
    } else {
      fullName = "Buddy";
    }

    return fullName;
  },

  // getting number formatter name
  getNumberCaret: function (no) {
    let num = parseInt(no);
    switch (num) {
      case 1:
        return num + "st";
      case 2:
        return num + "nd";
      case 3:
        return num + "rd";
      default:
        return num + "th";
    }
  },
  checkPresentClass: (presentClassId, classId) => {
    switch(parseInt(presentClassId)) {
      case 1: return [2,3,4,5,6,7,8,9,10,11,12,16,20,21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 2: return [3,4,5,6,7,8,9,10,11,12,16,20,21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 3: return [4,5,6,7,8,9,10,11,12,16,20,21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 4: return [5,6,7,8,9,10,11,12,16,20,21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 5: return [6,7,8,9,10,11,12,16,20,21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 6: return [7,8,9,10,11,12,16,20,21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1; 
      case 7: return [8,9,10,11,12,16,20,21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 8: return [9,10,11,12,16,20,21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 9: return [10,11,12,16,20,21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 10: return [11,12,16,20,21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 11: return [12,16,20,21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 12: return [16,20,21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 16: return [20,21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 20: return [21,22,23,24,25,840,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 840: return [21,22,23,24,25,952,577,915,752].indexOf(parseInt(classId)) > -1;
      case 952: return [21,22,23,24,25,577,915,752].indexOf(parseInt(classId)) > -1;
      case 21: return [22,23,24,25,577,915,752].indexOf(parseInt(classId)) > -1;
      case 22: return [23,24,25,577,915,752].indexOf(parseInt(classId)) > -1;
      case 915: return [23,24,25,577,752].indexOf(parseInt(classId)) > -1;
      case 23: return [24,25,577,752,915].indexOf(parseInt(classId)) > -1;
      case 24: return [25,577,752].indexOf(parseInt(classId)) > -1;
      case 25: return [577,752].indexOf(parseInt(classId)) > -1;
      default: return true;
    }
  },
  getUserId: () => localStorage.getItem('userId'),
  /* Creates an array with given start value, end value, step */
  createRangeArray: (startValue, endValue, step = 1) =>
    Array(endValue - startValue)
      .fill(startValue)
      .map((v, index) => v + index + (step - 1))
};

export default gblFunc;
