export const ruleRunner = (fieldValue, field, name, ...validations) => {
  //example ("emailAddress", "Email Address", required)
  //("password1", "Password", required, minLength(6))
  for (let v of validations) {
    let errorMessageFunc = v(fieldValue);
    if (errorMessageFunc) {
      return {
        [field]: errorMessageFunc(name),
        [field + "Validation"]: validations
      };
    }
  }
  return { [field]: null };
};
