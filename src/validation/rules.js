import * as ErrorMessages from "./errorMessages.js";
import { regexChecker, regexCheckTypes } from "./regexChecker";

export const required = text => {
  if (text) {
    return null;
  } else {
    return ErrorMessages.isRequired;
  }
};

export const minLength = length => {
  return text => {
    if (text == null || text == "") {
      return null;
    } else {
      text = text.toString(); //make it string as number does not have length method...
      return text.length >= length ? null : ErrorMessages.minLength(length);
    }
  };
};
export const maxLength = length => {
  return text => {
    let text1 = text.toString();
    if (text1 == null || text1 == "") {
      return null;
    } else {
      return text1.length <= length ? null : ErrorMessages.maxLength(length);
    }
  };
};
export const lengthRange = (minLength, maxLength) => {
  return text => {
    if (text == null || text == "") {
      return ErrorMessages.tooShort;
    } else {
      text = text.toString(); //if number is coming.. make it string as number soes not have length function@Pushpendra
      return text.length >= minLength && text.length <= maxLength
        ? null
        : text.length < minLength
          ? ErrorMessages.tooShort
          : ErrorMessages.tooLong;
    }
  };
};

/* File based validations */
export const maxFileSize = maxSize => {
  return fileSize =>
    fileSize > maxSize ? ErrorMessages.fileTooLarge(maxSize) : null;
};

export const hasValidExtension = allowedExtensions => {
  return fileExtension =>
    allowedExtensions.includes(fileExtension)
      ? null
      : ErrorMessages.invalidFileExtension(allowedExtensions);
};

export const hasValidImageExtenstion = allowedExtensions => {
  return fileExtension =>
    allowedExtensions.includes(fileExtension)
      ? null
      : ErrorMessages.invalidImageExtension(allowedExtensions);
};

export const shouldMatch = frozenValue => {
  return value => (value === frozenValue ? null : ErrorMessages.shouldMatch);
};

export const isGreaterThan = frozenValue => {
  return value =>
    parseInt(value, 10) >= parseInt(frozenValue, 10)
      ? null
      : ErrorMessages.isNotGreater;
};

export const isLessThan = frozenValue => {
  return value =>
    parseInt(value, 10) <= parseInt(frozenValue, 10)
      ? null
      : ErrorMessages.isLessThen;
};

/* REGEX CHECKING SECTION
   For value based validations eg email, numeric etc
*/

const {
  NUMERIC,
  EMAIL,
  MOBILE_NUMBER,
  EMAIL_MOBILE,
  IFSC_CODE,
  PANCARD,
  CGPA,
  PINCODE,
  AADHARCARD,
  ALPHABETSONLY,
  NUMBERANDFLOAT,
  NONZERONUMBER,
  NEPALPINCODE,
  PERCENTAGE,
  ALPHANUMERICONLY,
  CHARONLY
} = regexCheckTypes;
export const isChar = value => {
  if (value) {
    return regexChecker(value, CHARONLY) ? null : ErrorMessages.isCharOnly
  }
}

export const isNumeric = value => {
  if (value)
    return regexChecker(value, NUMERIC) ? null : ErrorMessages.isNumeric;
};

export const isNumericAndFloat = value =>
  regexChecker(value, NUMBERANDFLOAT) ? null : ErrorMessages.isNumericAndFloat;

export const isEmail = value => {
  if (value) return regexChecker(value, EMAIL) ? null : ErrorMessages.isEmail;
};
export const isMobileNumber = value => {
  if (value)
    return regexChecker(value, MOBILE_NUMBER)
      ? null
      : ErrorMessages.isMobileNumber;
};
export const isEmailMobile = value => {
  if (value) {
    //var value2 = value.match(/\b(\w)/g);
    var value3 = value.includes('@');
    //console.log(value3, 'value3');
    if (!value3) {
      return regexChecker(value, MOBILE_NUMBER)
        ? null
        : ErrorMessages.isMobileNumber;
    }
    else {
      if (value3) {
        return regexChecker(value, EMAIL)
          ? null
          : ErrorMessages.isEmail;
      }

    }
  }
};

export const isIfscCode = value =>
  regexChecker(value, IFSC_CODE) ? null : ErrorMessages.isIfscCode;

export const isPanCard = value =>
  regexChecker(value, PANCARD) ? null : ErrorMessages.isPanCard;

export const isCGPA = value =>
  regexChecker(value, CGPA) ? null : ErrorMessages.isCGPA;

export const isPINCODE = value =>
  regexChecker(value, PINCODE) ? null : ErrorMessages.isPINCODE;

export const isNEPALPINCODE = value =>
  regexChecker(value, NEPALPINCODE) ? null : ErrorMessages.isNEPALPINCODE;
export const alphabetsOnly = value =>
  regexChecker(value, ALPHABETSONLY) ? null : ErrorMessages.alphabetsOnly;
export const isNonZeroNumber = value =>
  regexChecker(value, NONZERONUMBER) ? null : ErrorMessages.nonZeroNumber;

export const isPercentage = value =>
  regexChecker(value, PERCENTAGE) ? null : ErrorMessages.isPercentage;

export const isCorrectValue = value => {
  return value =>
    value < 11 && value > 0 ? null : ErrorMessages.isCorrectValue;
};

export const isAadhar = value =>
  regexChecker(value, AADHARCARD) ? null : ErrorMessages.isAadhar;
export const isAlphaNumericOnly = value =>
  regexChecker(value, ALPHANUMERICONLY)
    ? null
    : ErrorMessages.isAlphaNumericOnly;

export const mapValidationFunc = value => {
  switch (value.func) {
    case "required":
      return required;
      break;
    case "lengthRange":
      return lengthRange(value.args[0], value.args[1]);
      break;
    case "isEmail":
      return isEmail;
      break;
    case "isNumeric":
      return isNumeric;
      break;
    case "isNumericAndFloat":
      return isNumericAndFloat;
      break;
    case "isAadhar":
      return isAadhar;
      break;
    case "isMobileNumber":
      return isMobileNumber;
    case "alphabetsOnly":
      return alphabetsOnly;
      break;
    case "isPINCODE":
      return isPINCODE;
      break;
    case "isNEPALPINCODE":
      return isNEPALPINCODE;
      break;
    case "isGreaterThan":
      return isGreaterThan(value.args[0]);
      break;
    case "isLessThan":
      return isLessThan(value.args[0]);
      break;
    case "minLength":
      return minLength(value.args[0]);
      break;
    case "maxLength":
      return maxLength(value.args[0]);
      break;
    case "isNonZeroNumber":
      return isNonZeroNumber;
      break;
    case "isCGPA":
      return isCGPA;
    case "isPercentage":
      return isPercentage;
    case "isIFSC":
      return isIfscCode;
    case "ifscCode":
      return isIfscCode;
    default:
      return required;
  }
};
