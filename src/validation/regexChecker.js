const regexFactory = {
  numeric: /^[0-9]+$/,
  numberAndFloat: /^\d+(\.\d+)*$/,
  mobileNumber: /^(0|[6789][0-9]*)$/,
  aadharCard: /^([23456789][0-9]*)$/,
  email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/,
  emailMobile: /^((([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3})))| ((0|[6789][0-9]*))$/,
  ifscCode: /^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/,
  panCard: /(^([a-zA-Z]{5})([0-9]{4})([a-zA-Z]{1})$)/,
  cgpa: /^(([1-9](\.\d{0,1}[0-9])?)|(10))$/,
  pincode: /^[1-9][0-9]{5}$/,
  alphabetsOnly: /^[a-zA-Z ]*$/,
  nonZeroNumber: /^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$/,
  nepalpincode: /^[1-9][0-9]{4}$/,
  percentage: /^100$|^[0-9]{0,2}$|^[0-9]{0,2}[.][0-9]{1,2}?$/,
  alphaNumericOnly: /^[A-Za-z 0-9]+$/,
  charOnly: /^[a-zA-Z\s]*$/
};

export const regexChecker = (value, type) => regexFactory[type].test(value);

export const regexCheckTypes = {
  NUMERIC: "numeric",
  NUMBERANDFLOAT: "numberAndFloat",
  EMAIL: "email",
  MOBILE_NUMBER: "mobileNumber",
  EMAIL_MOBILE: "emailMobile",
  IFSC_CODE: "ifscCode",
  PANCARD: "panCard",
  CGPA: "cgpa",
  PINCODE: "pincode",
  AADHARCARD: "aadharCard",
  ALPHABETSONLY: "alphabetsOnly",
  NONZERONUMBER: "nonZeroNumber",
  NEPALPINCODE: "nepalpincode",
  PERCENTAGE: "percentage",
  ALPHANUMERICONLY: "alphaNumericOnly",
  CHARONLY: "charOnly"

};
