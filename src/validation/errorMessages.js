export const isRequired = fieldName => `${fieldName} is required`;

export const minLength = length => {
  return fieldName => `${fieldName} must be at least ${length} characters long`;
};
export const maxLength = length => {
  return fieldName => `${fieldName} must be ${length} characters long only`;
};

export const lengthRange = (minLength, maxLength) => {
  return fieldName =>
    `${fieldName} must be between ${minLength} to ${maxLength} characters long`;
};
export const tooShort = fieldName => `${fieldName} is too short`;

export const tooLong = fieldName => `${fieldName} is too long`;

export const isNumeric = fieldName => `${fieldName} must contain only digits`;

export const isNumericAndFloat = fieldName =>
  `${fieldName} must contain only decimal numbers`;

export const isEmail = fieldName => `Please enter a valid email address`;

export const shouldMatch = fieldName => `Passwords do not match!`;

export const isMobileNumber = fieldName => `Please enter a valid mobile number`;

export const isEmailMobile = fieldName => `Please enter either a valid email/ mobile number`;

export const isIfscCode = fieldName =>
  `Please enter a valid IFSC code (must be alphanumeric and 11 characters long)`;
export const isPanCard = fieldName =>
  `Please enter a valid pan card (must be alphanumeric and 10 characters long)`;
export const isCGPA = fieldName => `Please enter a valid cgpa number.`;

export const isValidMarks = fieldName =>
  `${fieldName} should be greater than obtain marks`;

export const isOneCheckBoxSelected = fieldName =>
  `Please select at least one ${fieldName}`;

export const fileTooLarge = maxSize => fieldName =>
  `${fieldName} size must be less than ${maxSize} MB`;

export const isCorrectValue = fieldName => `Please enter correct value`;

export const invalidFileExtension = allowedExtensions => fieldName =>
  `File must have a valid extension.`;

export const invalidImageExtension = allowedExtensions => fieldName =>
  `Image must have a valid extension. (.jpeg, .jpg, .png, .bmp, .tif)`;

export const isPINCODE = fieldName =>
  `Please enter a valid pincode (must be numeric and 6 digit long).`;

export const isNEPALPINCODE = fieldName =>
  `Please enter a valid pincode (must be numeric and 5 digit long).`;

export const isNotGreater = fieldName =>
  `Total marks must be greater than marks obtained.`;

export const isLessThen = fieldName =>
  `Marks obtained must be less than total marks.`;

export const isAadhar = fileName => `Please enter a valid aadhar card number.`;
export const alphabetsOnly = fileName => `Please enter alphabets only.`;
export const isAlphaNumericOnly = fileName => `Please enter alphanumeric only`;
export const nonZeroNumber = fieldName =>
  `${fieldName} must be greater than zero.`;

export const isPercentage = fileName => `Please enter a valid percentage.`;
export const isCharOnly = fieldName => `${fieldName} should not be alphanumeric.`;

export const SATerrors = {
  satRawScore: "Enter valid SAT raw score (1 to 9999999)",
  satMathScore: "Enter valid SAT math score (1 to 9999999)",
  satReadingScore: "Enter valid SAT reading score (1 to 9999999)",
  satWritingScore: "Enter valid SAT writing score (1 to 9999999)",
  satLanguageScore: "Enter valid SAT language score (1 to 9999999)"
};
export const GMATerrors = {
  gmatVerbalScore: "Enter valid GMAT verbal score (130 to 170)",
  gmatVerbal: "Enter valid GMAT verbal score percentage (1 to 100)",
  gmatQuantitativeScore: "Enter valid GMAT quantitative score (130 to 170)",
  gmatQuantitative: "Enter valid GMAT quantitative score percentage (1 to 100)",
  gmatAnalyticalWritingScore: "Enter valid GMAT analytical score (0 to 6)",
  gmatAnalyticalWriting:
    "Enter valid GMAT analytical score percentage (1 to 100)",
  gmatTotalScore: "Enter valid GMAT total score (1 to 999)",
  gmatTotalPercentage: "Enter valid GMAT total score percentage (1 to 100)"
};
export const GREerrors = {
  greVerbalScore: "Enter valid GRE verbal score (130 to 170)",
  greVerbal: "Enter valid GRE verbal score percentage (1 to 100)",
  greQuantitativeScore: "Enter valid GRE quantitative score (130 to 170)",
  greQuantitative: "Enter valid GRE quantitative score percentage (1 to 100)",
  greAnalyticalWriting: "Enter valid GRE analytical score (0 to 6)",
  greAnalyticalWritingScore:
    "Enter valid GRE analytical score percentage (1 to 100)"
};
