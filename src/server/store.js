import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import createSagaMiddleware, { END } from "redux-saga";
import createMemoryHistory from "history/createMemoryHistory";
import { routerMiddleware } from "react-router-redux";

import homeReducer from "../shared/home/reducer";
import scholarshipReducer from "../shared/scholarship/reducer";
import onlineApplicationFormListReducer from "../shared/online-application-form-list/reducer";
import termsConditionsReducer from "../shared/terms-conditions/reducer";
import contactUsReducer from "../shared/contact-us/reducer";
import privacyConditionReducer from "../shared/privacy-policy/reducer";
import scholarshipDetailReducer from "../shared/scholarship/reducer";
import commonReducer from "../constants/commonReducer";
import faqsReducer from "../shared/faqs/reducer";
import dashboardReducer from "../shared/dashboard/reducer";
import faqDetailReducer from "../shared/faqs/reducer";
import teamReducer from "../shared/team/reducer";
import loginRegistrationReducer from "../shared/login/reducer";
import careersReducer from "../shared/careers/reducer";
import aboutReducer from "../shared/about/reducer";
import mediaPartnerReducer from "../shared/media-partners/reducer";
import mediaUrlReducer from "../shared/media-url/reducer";
import educationReducer from "../shared/education-loan/reducer";
import cscReducer from "../shared/csc/reducer";
import bookSlotReducer from "../shared/book-slot/reducer";
import marchantReducer from "../shared/vle/reducer";
import applicationPersonalInfoReducer from "../shared/application/reducers/applicationPersonalInfoReducer";
import applicationFamilyEarningsReducer from "../shared/application/reducers/applicationFamilyEarningsReducer";
import applicationReferenceReducer from "../shared/application/reducers/applicationReferenceReducer";
import applicationQuestionReducer from "../shared/application/reducers/applicationQuestionReducer";
import applicationInstructionReducer from "../shared/application/reducers/applicationInstructionsReducer";
import applicationEducationReducer from "../shared/application/reducers/applicationEducationReducer";
import applicationFormReducer from "../shared/application/reducers/applicationFormReducer";
import applicationSummeryReducer from "../shared/application/reducers/applicationSummeryReducer";
import applicationAwardWonReducer from "../shared/application/reducers/applicationAwardWonReducer";
import applicationDocumentReducer from "../shared/application/reducers/applicationDocumentReducer";
import applicationExtranceExamReducer from "../shared/application/reducers/applicationExtranceExamReducer";
import internationalStudyReducer from "../shared/international-study/reducer";
import interestFormReducer from "../shared/interest-form/reducer";
import footerBannerReducer from "../shared/banners/reducer";
import quesAnsReducer from "../shared/question-answer/reducer";
import brandPageReducer1 from "../shared/colgate/reducer";
import scholarshipVideoReducer from "../shared/scholarship-video/reducer";
import userSubscribedReducer from "../shared/user-unsubscribed/reducer";
import brandPageReducer from "../shared/brandPage/reducer";

import scholarshipConclaveReducer from "../shared/scholarship-conclave/reducer";
import oppReducer from "../shared/careers/reducer";
import applicationPaymentReducer from "../shared/applicationPayment/reducers";
import applicationBankDtsReducer from "../shared/application/reducers/applicationBankDetailsReducer";
import resultsLookReducers from "../shared/scholarResults/reducers";
import HulLoginFormReducer from "../shared/application/reducers/hulLoginFormReducer";
import applicationShareLinkReducers from "../shared/applicationShareLink/reducers";

import collegeBoardReducer from "../shared/college-board/reducers";
import scholarReducers from "../shared/scholar/scholarReducer";
import contentReducer from "../shared/content/reducer";
import dontionReducer from "../shared/covid-19-donation/reducer";

const sagaMiddleware = createSagaMiddleware();
var history;

var history = createMemoryHistory(); //This kind of history is needed for server-side rendering.

const reduxMiddlewares = [routerMiddleware(history), sagaMiddleware];
export default initialState => {
  const store = createStore(
    combineReducers({
      home: homeReducer,
      scholarship: scholarshipReducer,
      onlineApplicationFormList: onlineApplicationFormListReducer,
      termsConditions: termsConditionsReducer,
      contactUs: contactUsReducer,
      privacyConditions: privacyConditionReducer,
      team: teamReducer,
      scholarshipDetail: scholarshipDetailReducer,
      faqs: faqsReducer,
      faqDetail: faqDetailReducer,
      common: commonReducer,
      dashboard: dashboardReducer,
      loginOrRegister: loginRegistrationReducer,
      careers: careersReducer,
      about: aboutReducer,
      mediaPartner: mediaPartnerReducer,
      mediaUrl: mediaUrlReducer,
      educationLoan: educationReducer,
      csc: cscReducer,
      bookSlot: bookSlotReducer,
      oppReducer: oppReducer,
      applicationPersonalInfo: applicationPersonalInfoReducer,
      applicationFamilyEarnings: applicationFamilyEarningsReducer,
      marchant: marchantReducer,
      appInstr: applicationInstructionReducer,
      applicationReference: applicationReferenceReducer,
      applicationQuestion: applicationQuestionReducer,
      applicationEducation: applicationEducationReducer,
      applicationForm: applicationFormReducer,
      applicationSummery: applicationSummeryReducer,
      applicationAwardWon: applicationAwardWonReducer,
      applicationDocument: applicationDocumentReducer,
      internationalStd: internationalStudyReducer,
      interestForm: interestFormReducer,
      applicationExtranceExam: applicationExtranceExamReducer,
      footerBanner: footerBannerReducer,
      scholarshipVideo: scholarshipVideoReducer,
      quesAnswerData: quesAnsReducer,
      brandPageReducer1: brandPageReducer1,
      scholarshipConclave: scholarshipConclaveReducer,
      applicationPayment: applicationPaymentReducer,
      brandPageReducer: brandPageReducer,
      applicationBankDts: applicationBankDtsReducer,
      userSubscribed: userSubscribedReducer,
      resultLookUp: resultsLookReducers,
      hulLogin: HulLoginFormReducer,
      applicationShareLink: applicationShareLinkReducers,
      collegeBoard: collegeBoardReducer,
      scholarReducers: scholarReducers,
      contentReducer: contentReducer,
     dontionReducer:dontionReducer
    }),
    initialState,
    compose(applyMiddleware(...reduxMiddlewares))
  );

  store.runSaga = sagaMiddleware.run;

  store.close = () => store.dispatch(END);

  return store;
};
