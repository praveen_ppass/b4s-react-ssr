import express from "express";
import React from "react";
import { renderToNodeStream } from "react-dom/server";
import { StaticRouter } from "react-router-dom";

import { getLoadableState } from "loadable-components/server";
import { Provider } from "react-redux";
import { Helmet } from "react-helmet";

import App from "../shared/app";
import configureStore from "./store";
import { renderHeader, renderFooter } from "./render";
import {
  defaultSeoTags,
  apiUrl,
  checkForSeoPage,
  UrlsToRedirect,
  pagesToNotFound
} from "../constants/constants";

import fetch from "isomorphic-fetch";
var compression = require("compression");
var slashes = require("connect-slashes");
var bodyParser = require("body-parser");
var request = require("request");

const path = require("path");
const app = express();
const port = process.env.PORT;
//test...
// var request = require("request").defaults({
//   timeout: 30000,
//   agentOptions: { secureProtocol: "TLSv1.2" }
// });
/******** SSL implementation **********/
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0; //for api calling from nodejs
if (process.env.NODE_ENV !== "production") {
  process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0; //for api calling from nodejs
  var fs = require("fs"),
    https = require("https");
  var options = {
    key: fs.readFileSync("../ssl/key.pem", "utf8"),
    cert: fs.readFileSync("../ssl/cert.pem", "utf8")
  };
}
/******** SSL implementation **********/

const optionsTXT = {
  root: path.join(__dirname, "/"),
  headers: {
    "Content-Type": "text/plain;charset=UTF-8"
  }
};
const optionsXML = {
  root: path.join(__dirname, "/"),
  headers: {
    "Content-Type": "text/xml;"
  }
};

app.get("*.js", function (req, res, next) {
  req.url = req.url + ".gz";
  res.set("Content-Encoding", "gzip");
  res.set("Content-Type", "text/javascript");
  next();
});

app.get("*.css", function (req, res, next) {
  req.url = req.url + ".gz";
  res.set("Content-Encoding", "gzip");
  res.set("Content-Type", "text/css");
  next();
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/assets", express.static("./dist"));
/** ========================================================================== */

app.post("/isIframeUrl", (req, res) => {
  var isBlocked = false,
    callStatus = 404;
  request(req.body.iframeUrl, function (err, response) {
    var isBlocked = false;
    try {
      callStatus = response.statusCode;

      // If the page was found...
      if (!err && response.statusCode == 200) {
        // Grab the headers
        var headers = response.headers;
        console.log("headers", headers);
        // Grab the x-frame-options header if it exists
        var xFrameOptions = headers["x-frame-options"] || "";

        // Normalize the header to lowercase
        xFrameOptions = xFrameOptions.toLowerCase();

        // Check if it's set to a blocking option
        if (xFrameOptions === "sameorigin" || xFrameOptions === "deny") {
          isBlocked = true;
        }
      }
    } catch (err) {
      console.log("error iframe..");
    }
    res.status(callStatus).send({ isBlocked: isBlocked });
    return;
  });
});
/** ========================================================================== */

app.get("*.txt", (req, res) => {
  res.status(200).sendFile(req.url.split("/").splice(1, 1)[0], optionsTXT);
});
app.get("*.xml", (req, res) => {
  res.status(200).sendFile(req.url.split("/").splice(1, 1)[0], optionsXML);
});
app.use(compression());
app.use(slashes(false)); //Remove trailing spaces......Moved permanently
var seoTagsGenerator = (seoTagsObj, pageType, req) => {
  seoTagsObj = seoTagsObj ? seoTagsObj : {};
  //method to create seo tags on the basis of json returned from server
  let metatags = "";
  metatags =
    '<meta name="p:domain_verify" content="dc3b4e4eaa47cdeb8848bf097b03c48f"/>';
  switch (pageType) {
    case "index":
      metatags +=
        '<meta property="og:title" content="' +
        (seoTagsObj["title"]
          ? seoTagsObj["title"]
          : defaultSeoTags.tags.title) +
        '" />';
      metatags +=
        '<meta property="og:description" content="' +
        (seoTagsObj["description"]
          ? seoTagsObj["description"]
          : defaultSeoTags.tags.description) +
        '" />';
      if (req.url) {
        metatags +=
          '<meta property="og:url" content="https://www.buddy4study.com' +
          req.url +
          '" />';
        '" />';
      }
      if (seoTagsObj && seoTagsObj["image"]) {
        metatags += '<meta property="og:image" content="' + seoTagsObj["image"] + '" />';
        metatags += '<meta property="og:image:url" content="' + seoTagsObj["image"] + '" />';
      }
      metatags +=
        "<title> " +
        (seoTagsObj["title"]
          ? seoTagsObj["title"]
          : defaultSeoTags.tags.title) +
        " </title>  ";
      metatags +=
        '<meta name="description" content="' +
        (seoTagsObj["description"]
          ? seoTagsObj["description"]
          : defaultSeoTags.tags.description) +
        '" />';

      metatags +=
        '<meta name="keywords" content="' +
        (seoTagsObj["keyword"]
          ? seoTagsObj["keyword"]
          : defaultSeoTags.tags.keywords) +
        '" />';

      metatags += '<meta name="robots" content="index, follow" />';

      let cUrl = "https://www.buddy4study.com";
      if (req.url !== "/") {
        if (seoTagsObj && seoTagsObj["canonicalUrl"]) {
          if (seoTagsObj["canonicalUrl"].includes("https://")) {
            cUrl = seoTagsObj["canonicalUrl"];
          } else {
            cUrl += "/" + seoTagsObj["canonicalUrl"];
          }
        } else {
          cUrl += req.url;
        }
      }
      metatags +=
        '<link rel="canonical" href="' + cUrl + '" />';
      break;
    case "noFollow":
      metatags += '<meta name="robots" content="noindex, nofollow" />';
      break;
    default:
      metatags = "";
  }
  return metatags;
};

function handleErrors(response) {
  if (!response.ok) {
    console.log("Error...", response.statusText);
    throw Error(response.statusText);
  }
  return response.json();
}
function timeout(ms, promise, page) {
  return new Promise(function (resolve, reject) {
    var timerId = setTimeout(function () {
      console.log("under timeout", page);
      reject(new Error("timeout"));
    }, ms);
    promise
      .then(function (res) {
        clearTimeout(timerId);
        resolve(res);
      })
      .catch(function (err) {
        clearTimeout(timerId);

        reject(err);
      });
  });
}

async function defaultHandling(
  pageInfo,
  loadableState,
  req,
  res,
  appWithRouter
) {
  //default seo handling in case of error or noindex page
  pageInfo.metatags = seoTagsGenerator({}, defaultSeoTags.robots(req.url), req);
  loadableState = await getLoadableState(appWithRouter);
  const helmet = Helmet.renderStatic();
  res.status(200).write(renderHeader(helmet, pageInfo));

  const preloadedState = {};
  const css = {};

  const htmlSteam = renderToNodeStream(appWithRouter);
  htmlSteam.pipe(res, { end: false });
  htmlSteam.on("end", () => {
    res.write(renderFooter(css, loadableState, preloadedState));
    return res.send();
  });
}

app.get("*", async (req, res) => {
  /*****************************Set headers ************************************* */
  res.header("Cache-Control", "no-cache, no-store");
  res.header("Expires", "-1");
  res.header("Pragma", "no-cache");
  res.header("content-type", "text/html; charset=UTF-8");
  res.header("x-frame-options", "SAMEORIGIN"); //Don't allow url to open in Iframe
  res.header("x-xss-protection", 1); //Security against crosite scripting attacks

  /*****************************Set headers ************************************* */
  const store = configureStore();
  const context = {};
  const pageSeoName = checkForSeoPage(req.url);
  var pageInfo = { reqUrl: req.url, userMode: 1 }; //user Mode will be checked on every request ....
  let loadableState = {};

  const appWithRouter = (
    <Provider store={store}>
      <StaticRouter location={req.url} context={context}>
        <App />
      </StaticRouter>
    </Provider>
  );
  if (context.url) {
    res.redirect(context.url);
    return;
  }
  if (UrlsToRedirect.has(req.url)) {
    res.redirect(301, UrlsToRedirect.get(req.url));
    return;
  }

  if (pageSeoName !== "noIndex") {
    //for indexed pages
    timeout(
      1000,
      fetch(apiUrl.seoTags, {
        method: "post",
        headers: {
          Authorization:
            "Bearer " +"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJyZWFkIl0sImV4cCI6MTYyMTg0MzM1NSwiYXV0aG9yaXRpZXMiOlsiVVNFUiJdLCJqdGkiOiI4YWE1NjNhZS1iMTFlLTQ4MmItYjZlNS01MjRiMWRhYThkMGMiLCJjbGllbnRfaWQiOiJiNHMifQ.OTodqbDjesRWwGnQZURsS53n_gs2AYsNhbAKWqXb1xc",
          Accept: "*/*",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ url: pageSeoName })
      }),
      pageSeoName
    )
      .then(handleErrors)
      .then(async function (response) {
        console.log("Under success...........", pageSeoName);
        if (response.redirectUrl) {
          if (
            response.redirectUrl.includes("https://") ||
            response.redirectUrl.includes("http://")
          ) {
            res.redirect(301, response.redirectUrl);
          } else {
            res.redirect(301, "/" + response.redirectUrl);
          }
          return;
        }
        const helmet = Helmet.renderStatic();
        pageInfo.metatags = seoTagsGenerator(response, "index", req); //to be used on render.js
        const pageTitle = response["title"];
        context.appData = pageTitle;
        if (pagesToNotFound.indexOf(req.url) > -1) {
          res.status(404).write(renderHeader(helmet, pageInfo));
        } else {
          res.status(200).write(renderHeader(helmet, pageInfo));
        }

        const preloadedState = {};
        const css = {};
        loadableState = await getLoadableState(appWithRouter);
        const htmlSteam = renderToNodeStream(appWithRouter);
        htmlSteam.pipe(res, { end: false });
        htmlSteam.on("end", () => {
          res.write(renderFooter(css, loadableState, preloadedState));
          return res.send();
        });
      })
      .catch(function (error) {
        defaultHandling(pageInfo, loadableState, req, res, appWithRouter);
      });
  } else {
    //if page is noindex
    defaultHandling(pageInfo, loadableState, req, res, appWithRouter);
  }
  // ${loadableState.getScriptTag()} - removed from render.js
  // Dispatch a close event so sagas stop listening after they're resolved
  store.close();
});

//app.listen(8085, () => {});

/******** SSL implementation **********/
if (process.env.NODE_ENV === "production") {
  app.listen(port, () => {
    console.log(
      `Server running on port ${port} with env ${process.env.NODE_ENV}`
    );
  });
} else {
  var server = https.createServer(options, app).listen(port, function () {
    console.log(
      `Server running on port ${port} with env ${process.env.NODE_ENV}`
    );
  });
}
/******** SSL implementation **********/
