const nodeExternals = require("webpack-node-externals");
const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const srcPath = path.resolve(__dirname, "src");
const distPath = path.resolve(__dirname, "dist");
const plugins = [];
require.extensions[".styl"] = () => {
  return;
};
module.exports = {
  context: srcPath,
  entry: "./server/index.js",
  output: {
    path: distPath,
    filename: "server.js",
    publicPath: "/assets/"
  },
  target: "node",
  node: {
    __dirname: false,
    __filename: false
  },
  resolve: {
    modules: ["node_modules", "src"],
    extensions: ["*", ".js", ".json"]
  },
  devtool: "inline-source-map",
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          presets: [
            [
              "env",
              {
                targets: {
                  node: 8
                }
              }
            ]
          ]
        }
      },
      { test: /\.scss$/, use: ["css-loader", "sass-loader"] },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(png|jp(e*)g|svg|ico)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 8000, // Convert images < 8kb to base64 strings
              name: "images/[hash]-[name].[ext]"
            }
          }
        ]
      }
    ]
  },
  externals: nodeExternals(),
  plugins
};
