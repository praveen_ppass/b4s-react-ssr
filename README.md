## Runnning React Router v4, SSR, Redux Saga and Code Splitting together

This repository contains the code of a POC seeking to run a React application including React Router v4, Redux Saga, providing splitting code and compatible with *server side rendering*.

### Installation

* clone the repository
* install the dependencies with `npm install`

### Start servers

You can start the servers with pm2 by running PM2 commands in make file.



 
You can follow the various steps by following the repository tags :

* step-1          Bootstrap application
* step-2          First simple code splitting
* step-3          SSR without code splitting
* step-4          Dynamic code splitting working client side
* step-5          Dynamic code splitting working server side

@In case of any query conatct   -  praveen.tripathi@buddy4study.com

