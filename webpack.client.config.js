const HTMLWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
var CompressionPlugin = require("compression-webpack-plugin");
const path = require("path");
const webpack = require("webpack");

const srcPath = path.resolve(__dirname, "src");
const distPath = path.resolve(__dirname, "dist");

const plugins = [
  new HTMLWebpackPlugin({
    title: "B4S Website",
    template: path.resolve(__dirname, "src/client/index.ejs")
  }),
  new webpack.optimize.CommonsChunkPlugin({
    name: "vendor",
    minChunks: Infinity
  }),
  new webpack.DefinePlugin({
    "process.env": {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      API_URL: JSON.stringify(process.env.API_URL),
      BROWSER: JSON.stringify(true)
    }
  }),
  new ExtractTextPlugin("dist/styles/main.css", {
    allChunks: true
  }),

  new CompressionPlugin({
    asset: "[path].gz[query]",
    algorithm: "gzip",
    test: /\.js$|\.css$|\.html$/
  })
];
if (process.env.NODE_ENV == "production" || process.env.NODE_ENV == "staging") {
  plugins.push(new webpack.optimize.AggressiveMergingPlugin());
  plugins.push(
    new UglifyJsPlugin({
      uglifyOptions: {
        sourceMap: true,
        mangle: true,
        compress: {
          warnings: false // Suppress uglification warnings
        },
        output: {
          comments: false
        },
        exclude: [/\.min\.js$/gi] // skip preminified libs
      }
    })
  );
}
if (process.env.NODE_ENV === "analyse") {
  plugins.push(new BundleAnalyzerPlugin());
}

module.exports = {
  context: srcPath,
  target: "web",
  entry: {
    client: `${srcPath}/client/index.js`,
    vendor: [
      "babel-polyfill",
      "react",
      "react-dom",
      "react-router-dom",
      "redux",
      "redux-saga",
      "react-redux"
    ]
  },
  output: {
    path: distPath,
    filename: "[name].js",
    publicPath: "/assets/"
  },
  resolve: {
    modules: ["node_modules", "src"],
    extensions: ["*", ".js", ".json"]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: "babel-loader",
        query: { compact: false }
      },
      {
        test: /\.(s*)css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader", "sass-loader"]
        })
      },
      {
        test: /\.(png|jp(e*)g|svg)$/,
        use: [
          {
            loader: "url-loader"
            // options: {
            //   limit: 8000, // Convert images < 8kb to base64 strings
            //   name: "images/[hash]-[name].[ext]"
            // }
          }
        ]
      }
    ]
  },
  plugins,
  devtool:
    process.env.NODE_ENV == "production" || process.env.NODE_ENV == "staging"
      ? "source-map"
      : "inline-eval-cheap-source-map",
  devServer: {
    historyApiFallback: true
  }
};
